﻿#region Default

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using FullDrive.Module.BusinessObjects;
using DevExpress.ExpressApp.Xpo;

#endregion Default

using FullDrive.Module.CustomProcess;
using System.Collections;
using DevExpress.Xpo;
using System.Web;
using System.IO;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class SalesOrderActionController : ViewController
    {
        #region Default

        private ChoiceActionItem _selectionListviewFilter;
        private ChoiceActionItem _selectionApprovalListviewFilter;
        private ChoiceActionItem _setApprovalLevel;

        public SalesOrderActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            #region FilterStatus
            SalesOrderListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                SalesOrderListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
            #endregion FilterStatus

            #region FilterApproval
            SalesOrderListviewFilterApprovalSelectionAction.Items.Clear();
            foreach (object _currApprovalFilter in Enum.GetValues(typeof(CustomProcess.ApprovalFilter)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.ApprovalFilter));
                _selectionApprovalListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApprovalFilter), _currApprovalFilter);
                SalesOrderListviewFilterApprovalSelectionAction.Items.Add(_selectionApprovalListviewFilter);
            }
            #endregion FilterApproval
        }
        protected override void OnActivated()
        {
            TypeOfView = typeof(ObjectView);
            base.OnActivated();
            // Perform various tasks depending on the target View.
            #region Approval
            var _locUser = SecuritySystem.CurrentUserName;
            GlobalFunction _globalFunction = new GlobalFunction();
            Session _currentSession = null;
            if (this.ObjectSpace != null)
            {
                _currentSession = ((XPObjectSpace)this.ObjectSpace).Session;
            }

            if (_currentSession != null)
            {
                UserAccess _locUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", _locUser));

                if (_locUserAccess != null)
                {
                    SalesOrderApprovalAction.Items.Clear();

                    XPCollection<ApplicationSetupDetail> _locAppSetupDetails = new XPCollection<ApplicationSetupDetail>
                                                                                 (_currentSession, new GroupOperator(GroupOperatorType.And,
                                                                                  new BinaryOperator("UserAccess", _locUserAccess),
                                                                                  new BinaryOperator("ObjectList", CustomProcess.ObjectList.SalesOrder),
                                                                                  new BinaryOperator("FunctionList", CustomProcess.FunctionList.Approval),
                                                                                  new BinaryOperator("Active", true)));

                    if (_locAppSetupDetails != null && _locAppSetupDetails.Count > 0)
                    {

                        foreach (ApplicationSetupDetail _locAppSetupDetail in _locAppSetupDetails)
                        {
                            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.ApprovalLevel)))
                            {
                                if (_locAppSetupDetail.ApprovalLevel != CustomProcess.ApprovalLevel.None)
                                {
                                    EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.ApprovalLevel));
                                    _setApprovalLevel = new ChoiceActionItem(_ed.GetCaption(_locAppSetupDetail.ApprovalLevel), _locAppSetupDetail.ApprovalLevel);
                                }
                            }
                            SalesOrderApprovalAction.Items.Add(_setApprovalLevel);
                        }
                    }
                }
            }
            #endregion Approval
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        #endregion Default

        private void SalesOrderProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                Status _locStatus = Status.None;
                DateTime _locNow;
                Status _locStatusWR = Status.None;
                DateTime _locNowWR;
                Status _locStatus2 = Status.None;
                DateTime _locNow2;
                Status _locStatusWR2 = Status.None;
                DateTime _locNowWR2;
                Status _locStatus3 = Status.None;
                DateTime _locNow3;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        SalesOrder _locSalesOrderOS = (SalesOrder)_objectSpace.GetObject(obj);

                        if (_locSalesOrderOS != null)
                        {
                            if (_locSalesOrderOS.Code != null)
                            {
                                _currObjectId = _locSalesOrderOS.Code;

                                SalesOrder _locSalesOrderXPO = _currSession.FindObject<SalesOrder>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locSalesOrderXPO != null)
                                {
                                    ApprovalLine _locApprovalLineXPO = _currSession.FindObject<ApprovalLine>
                                                                           (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("EndApproval", true),
                                                                            new BinaryOperator("SalesOrder", _locSalesOrderXPO)));
                                    if (_locApprovalLineXPO == null)
                                    {
                                        #region Status
                                        if (_locSalesOrderXPO.Status == Status.Open || _locSalesOrderXPO.Status == Status.Progress)
                                        {
                                            if (_locSalesOrderXPO.Status == Status.Open)
                                            {
                                                _locStatus = Status.Progress;
                                                _locNow = now;
                                            }
                                            else
                                            {
                                                _locStatus = _locSalesOrderXPO.Status;
                                                _locNow = _locSalesOrderXPO.StatusDate;

                                            }
                                            _locSalesOrderXPO.Status = _locStatus;
                                            _locSalesOrderXPO.StatusDate = _locNow;
                                            _locSalesOrderXPO.Save();
                                            _locSalesOrderXPO.Session.CommitTransaction();

                                            XPCollection<SalesOrderLine> _locSalesOrderLines = new XPCollection<SalesOrderLine>
                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("SalesOrder", _locSalesOrderXPO),
                                                                                    new GroupOperator(GroupOperatorType.Or,
                                                                                    new BinaryOperator("Status", Status.Open),
                                                                                    new BinaryOperator("Status", Status.Progress))));

                                            if (_locSalesOrderLines != null && _locSalesOrderLines.Count > 0)
                                            {
                                                foreach (SalesOrderLine _locSalesOrderLine in _locSalesOrderLines)
                                                {
                                                    if (_locSalesOrderLine.Status == Status.Open)
                                                    {
                                                        _locStatus2 = Status.Progress;
                                                        _locNow2 = now;
                                                    }
                                                    else
                                                    {
                                                        _locStatus2 = _locSalesOrderLine.Status;
                                                        _locNow2 = _locSalesOrderLine.StatusDate;

                                                    }
                                                    _locSalesOrderLine.Status = _locStatus2;
                                                    _locSalesOrderLine.StatusDate = _locNow2;
                                                    _locSalesOrderLine.Save();
                                                    _locSalesOrderLine.Session.CommitTransaction();                                                  

                                                }
                                            }

                                            XPCollection<SalesQuotationCollection> _locSalesQuotationCollections = new XPCollection<SalesQuotationCollection>
                                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                            new BinaryOperator("SalesOrder", _locSalesOrderXPO)));
                                            if(_locSalesQuotationCollections != null && _locSalesQuotationCollections.Count() > 0)
                                            {
                                                foreach(SalesQuotationCollection _locSalesQuotationCollection in _locSalesQuotationCollections)
                                                {
                                                    if (_locSalesQuotationCollection.Status == Status.Open)
                                                    {
                                                        _locStatus3 = Status.Progress;
                                                        _locNow3 = now;
                                                    }
                                                    else
                                                    {
                                                        _locStatus3 = _locSalesQuotationCollection.Status;
                                                        _locNow3 = _locSalesQuotationCollection.StatusDate;

                                                    }
                                                    _locSalesQuotationCollection.Status = _locStatus3;
                                                    _locSalesQuotationCollection.StatusDate = _locNow3;
                                                    _locSalesQuotationCollection.Save();
                                                    _locSalesQuotationCollection.Session.CommitTransaction();
                                                }
                                            }

                                        }
                                        #endregion Status
                                        #region StatusWR
                                        if (_locSalesOrderXPO.Status != Status.Close)
                                        {
                                            if (_locSalesOrderXPO.StatusWR == Status.Open || _locSalesOrderXPO.StatusWR == Status.Progress)
                                            {
                                                if (_locSalesOrderXPO.StatusWR == Status.Open)
                                                {
                                                    _locStatusWR = Status.Progress;
                                                    _locNowWR = now;
                                                }
                                                else
                                                {
                                                    _locStatusWR = _locSalesOrderXPO.StatusWR;
                                                    _locNowWR = _locSalesOrderXPO.StatusDateWR;

                                                }

                                                _locSalesOrderXPO.StatusWR = _locStatusWR;
                                                _locSalesOrderXPO.StatusDateWR = _locNowWR;
                                                _locSalesOrderXPO.Save();
                                                _locSalesOrderXPO.Session.CommitTransaction();

                                                XPCollection<SalesOrderLine> _locSalesOrderLines = new XPCollection<SalesOrderLine>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("SalesOrder", _locSalesOrderXPO),
                                                                                                new BinaryOperator("Status", Status.Open)));

                                                if (_locSalesOrderLines != null && _locSalesOrderLines.Count > 0)
                                                {
                                                    foreach (SalesOrderLine _locSalesOrderLine in _locSalesOrderLines)
                                                    {
                                                        if (_locSalesOrderLine.StatusWR == Status.Open || _locSalesOrderLine.StatusWR == Status.Progress)
                                                        {
                                                            if (_locSalesOrderLine.StatusWR == Status.Open)
                                                            {
                                                                _locStatusWR2 = Status.Progress;
                                                                _locNowWR2 = now;
                                                            }
                                                            else
                                                            {
                                                                _locStatusWR2 = _locSalesOrderLine.StatusWR;
                                                                _locNowWR2 = _locSalesOrderLine.StatusDateWR;

                                                            }

                                                            _locSalesOrderLine.StatusWR = _locStatusWR2;
                                                            _locSalesOrderLine.StatusDateWR = _locNowWR2;
                                                            _locSalesOrderLine.Save();
                                                            _locSalesOrderLine.Session.CommitTransaction();
                                                        }
                                                    }
                                                }
                                            }

                                        }
                                        #endregion StatusWR
                                    }

                                    SuccessMessageShow("Sales Order has been successfully updated to progress");
                                }
                                else
                                {
                                    ErrorMessageShow("Data Sales Order Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Sales Order Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrder " + ex.ToString());
            }
        }

        private void SalesOrderPostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        SalesOrder _locSalesOrderOS = (SalesOrder)_objectSpace.GetObject(obj);

                        if (_locSalesOrderOS != null)
                        {
                            if (_locSalesOrderOS.Code != null)
                            {
                                _currObjectId = _locSalesOrderOS.Code;

                                SalesOrder _locSalesOrderXPO = _currSession.FindObject<SalesOrder>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locSalesOrderXPO != null)
                                {
                                    if (_locSalesOrderXPO.Status == Status.Progress || _locSalesOrderXPO.Status == Status.Posted)
                                    {
                                        ApprovalLine _locApprovalLineXPO = _currSession.FindObject<ApprovalLine>
                                                                           (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("EndApproval", true),
                                                                            new BinaryOperator("SalesOrder", _locSalesOrderXPO)));

                                        if (_locApprovalLineXPO != null)
                                        {
                                            SetSalesOrderMonitoring(_currSession, _locSalesOrderXPO);
                                            SetRemainQty(_currSession, _locSalesOrderXPO);
                                            SetPostingQty(_currSession, _locSalesOrderXPO);
                                            SetProcessCount(_currSession, _locSalesOrderXPO);
                                            SetStatusSalesOrderLine(_currSession, _locSalesOrderXPO);
                                            SetNormalQuantity(_currSession, _locSalesOrderXPO);
                                            SetFinalStatusSalesOrder(_currSession, _locSalesOrderXPO);
                                            SetMaksBill(_currSession, _locSalesOrderXPO);
                                            SuccessMessageShow("Sales Order has been successfully post");
                                        }
                                    }      
                                }
                                else
                                {
                                    ErrorMessageShow("Data Sales Order Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Sales Order Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrder " + ex.ToString());
            }
        }

        private void WorkRequisitionAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        SalesOrder _locSalesOrderOS = (SalesOrder)_objectSpace.GetObject(obj);

                        if (_locSalesOrderOS != null)
                        {
                            if (_locSalesOrderOS.Code != null)
                            {
                                _currObjectId = _locSalesOrderOS.Code;

                                SalesOrder _locSalesOrderXPO = _currSession.FindObject<SalesOrder>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Code", _currObjectId)));

                                if (_locSalesOrderXPO != null)
                                {
                                    if (_locSalesOrderXPO.StatusWR == Status.Progress || _locSalesOrderXPO.StatusWR == Status.Posted)
                                    {
                                        SetWorkRequisition(_currSession, _locSalesOrderXPO);
                                        SetStatusSalesOrderLineWR(_currSession, _locSalesOrderXPO);
                                        SetPostedWRCount(_currSession, _locSalesOrderXPO);
                                        SetFinalStatusSalesOrderWR(_currSession, _locSalesOrderXPO);
                                        SuccessMessageShow("SalesOrder has successfully posted");
                                    }
                                    else
                                    {
                                        ErrorMessageShow("Data Status SalesOrder Not Available");
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data SalesOrder Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data SalesOrder Not Available");
                            }
                        }
                        else
                        {
                            ErrorMessageShow("Data SalesOrder Not Available");
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrder " + ex.ToString());
            }
        }

        private void SalesOrderGetSQAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        SalesOrder _locSalesOrderOS = (SalesOrder)_objectSpace.GetObject(obj);

                        if (_locSalesOrderOS != null)
                        {
                            if (_locSalesOrderOS.Code != null)
                            {
                                _currObjectId = _locSalesOrderOS.Code;

                                SalesOrder _locSalesOrderXPO = _currSession.FindObject<SalesOrder>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locSalesOrderXPO != null)
                                {
                                    if (_locSalesOrderXPO.Status == Status.Open || _locSalesOrderXPO.Status == Status.Progress)
                                    {
                                        XPCollection<SalesQuotationCollection> _locSalesQuotationCollections = new XPCollection<SalesQuotationCollection>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("SalesOrder", _locSalesOrderXPO),
                                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("Status", Status.Open),
                                                                                        new BinaryOperator("Status", Status.Progress))));

                                        if (_locSalesQuotationCollections != null && _locSalesQuotationCollections.Count() > 0)
                                        {
                                            foreach (SalesQuotationCollection _locSalesQuotationCollection in _locSalesQuotationCollections)
                                            {
                                                if ( _locSalesQuotationCollection.SalesQuotation != null )
                                                {
                                                    ApprovalLine _locApprovalLineXPO = _currSession.FindObject<ApprovalLine>
                                                                       (new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("EndApproval", true),
                                                                        new BinaryOperator("SalesQuotation", _locSalesQuotationCollection.SalesQuotation)));
                                                    if (_locApprovalLineXPO != null)
                                                    {
                                                        GetSalesQuotationMonitoring(_currSession, _locSalesQuotationCollection.SalesQuotation, _locSalesOrderXPO);
                                                    }
                                                }
                                            }
                                            SuccessMessageShow("Sales Quotation Has Been Successfully Getting into Sales Order");
                                        }
                                    } 
                                }
                                else
                                {
                                    ErrorMessageShow("Data Sales Order Not Available");
                                }

                            }
                            else
                            {
                                ErrorMessageShow("Data Sales Order Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrder " + ex.ToString());
            }
        }

        private void SalesOrderSalesReturnAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                string _currSignCode = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        SalesOrder _locSalesOrderOS = (SalesOrder)_objectSpace.GetObject(obj);

                        if (_locSalesOrderOS != null)
                        {
                            if (_locSalesOrderOS.Code != null)
                            {
                                _currObjectId = _locSalesOrderOS.Code;
                                SalesReturn _locSalesReturnBySO = null;

                                SalesOrder _locSalesOrderXPO = _currSession.FindObject<SalesOrder>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Code", _currObjectId)));
                                if (_locSalesOrderXPO != null)
                                {
                                    if (_locSalesOrderXPO.Status == Status.Progress || _locSalesOrderXPO.Status == Status.Posted)
                                    {
                                        _locSalesReturnBySO = _currSession.FindObject<SalesReturn>
                                                              (new GroupOperator(GroupOperatorType.And,
                                                               new BinaryOperator("SalesOrder", _locSalesOrderXPO)));

                                        if (_locSalesReturnBySO == null)
                                        {
                                            _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.SalesReturn);

                                            if (_currSignCode != null)
                                            {
                                                SalesReturn _saveDataSReturn = new SalesReturn(_currSession)
                                                {
                                                    SalesToCustomer = _locSalesOrderXPO.SalesToCustomer,
                                                    SalesToContact = _locSalesOrderXPO.SalesToContact,
                                                    SalesToCountry = _locSalesOrderXPO.SalesToCountry,
                                                    SalesToCity = _locSalesOrderXPO.SalesToCity,
                                                    SalesToAddress = _locSalesOrderXPO.SalesToAddress,
                                                    //SignCode = _locSalesOrderXPO.Code,
                                                    SalesOrder = _locSalesOrderXPO,
                                                };
                                                _saveDataSReturn.Save();
                                                _saveDataSReturn.Session.CommitTransaction();

                                                XPCollection<SalesOrderLine> _numLineSalesOrderLines = new XPCollection<SalesOrderLine>(_currSession,
                                                                                                       new GroupOperator(GroupOperatorType.And,
                                                                                                       new BinaryOperator("SalesOrder", _locSalesOrderXPO)));

                                                SalesReturn _locSalesReturn2 = _currSession.FindObject<SalesReturn>
                                                                               (new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("SalesOrder", _locSalesOrderXPO)));

                                                if (_locSalesReturn2 != null)
                                                {
                                                    if (_numLineSalesOrderLines != null && _numLineSalesOrderLines.Count > 0)
                                                    {
                                                        foreach (SalesOrderLine _numLineSalesOrderLine in _numLineSalesOrderLines)
                                                        {
                                                            if (_numLineSalesOrderLine.Status == Status.Progress || _numLineSalesOrderLine.Status == Status.Posted)
                                                            {
                                                                SalesReturnLine _saveDataSalesReturnLine = new SalesReturnLine(_currSession)
                                                                {
                                                                    Item = _numLineSalesOrderLine.Item,
                                                                    MxDQty = _numLineSalesOrderLine.MxDQty,
                                                                    MxDUOM = _numLineSalesOrderLine.MxDUOM,
                                                                    MxTQty = _numLineSalesOrderLine.MxTQty,
                                                                    MxUOM = _numLineSalesOrderLine.MxUOM,
                                                                    DQty = _numLineSalesOrderLine.DQty,
                                                                    DUOM = _numLineSalesOrderLine.DUOM,
                                                                    Qty = _numLineSalesOrderLine.Qty,
                                                                    UOM = _numLineSalesOrderLine.UOM,
                                                                    TQty = _numLineSalesOrderLine.TQty,
                                                                    UAmount = _numLineSalesOrderLine.UAmount,
                                                                    TUAmount = _numLineSalesOrderLine.TUAmount,
                                                                    EstimatedDate = _numLineSalesOrderLine.EstimatedDate,
                                                                    StatusDate = _numLineSalesOrderLine.StatusDate,
                                                                    SalesReturn = _locSalesReturn2,
                                                                };
                                                                _saveDataSalesReturnLine.Save();
                                                                _saveDataSalesReturnLine.Session.CommitTransaction();
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        ErrorMessageShow("Data Sales Return Not Available");
                                                    }
                                                }
                                                else
                                                {
                                                    ErrorMessageShow("Data Sales Return Not Available");
                                                }
                                            }
                                        }
                                        SuccessMessageShow("Sales Return has been successfully post");
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data Sales Return Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Sales Return Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesReturn " + ex.ToString());
            }
        }

        private void SalesOrderListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(SalesOrder)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrder " + ex.ToString());
            }
        }

        private void SalesOrderListviewFilterApprovalSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(SalesOrder)))
                {
                    if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.Approval)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("ActiveApproved3", false, BinaryOperatorType.Equal),
                                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("ActiveApproved1", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", true, BinaryOperatorType.Equal)));
                    }
                    else if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.EndApproval)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new BinaryOperator("ActiveApproved3", true, BinaryOperatorType.Equal);
                    }
                    else if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.AllData)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("ActiveApproved1", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved3", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved1", false, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", false, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved3", false, BinaryOperatorType.Equal));
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrder " + ex.ToString());
            }
        }

        private void SalesOrderApprovalAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                ApplicationSetupDetail _locAppSetDetail = null;
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                var User = SecuritySystem.CurrentUserName;
                Session _currentSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currentSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                UserAccess _locUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", User));

                foreach (Object obj in _objectsToProcess)
                {
                    SalesOrder _objInNewObjectSpace = (SalesOrder)_objectSpace.GetObject(obj);

                    if (_objInNewObjectSpace != null)
                    {
                        if (_objInNewObjectSpace.Code != null)
                        {
                            _currObjectId = _objInNewObjectSpace.Code;
                        }
                    }

                    if (_currObjectId != null)
                    {
                        SalesOrder _locSalesOrderXPO = _currentSession.FindObject<SalesOrder>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("Code", _currObjectId)));

                        if (_locSalesOrderXPO != null)
                        {
                            if (_locSalesOrderXPO.Status == Status.Progress)
                            {
                                ApprovalLine _locApprovalLine = _currentSession.FindObject<ApprovalLine>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("SalesOrder", _locSalesOrderXPO),
                                                                             new BinaryOperator("EndApproval", true)));
                                if (_locApprovalLine == null)
                                {
                                    #region Approval Level 1
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level1)
                                    {
                                        _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level1, ObjectList.SalesOrder);
                                        if (_locAppSetDetail != null)
                                        {
                                            //Buat bs input langsung ke approvalline
                                            ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("SalesOrder", _locSalesOrderXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level1)));
                                            if (_locApprovalLineXPO == null)
                                            {

                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level1,
                                                        EndApproval = true,
                                                        SalesOrder = _locSalesOrderXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locSalesOrderXPO.ActivationPosting = true;
                                                    _locSalesOrderXPO.ActiveApproved1 = false;
                                                    _locSalesOrderXPO.ActiveApproved2 = false;
                                                    _locSalesOrderXPO.ActiveApproved3 = true;
                                                    _locSalesOrderXPO.Save();
                                                    _locSalesOrderXPO.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level1,
                                                        SalesOrder = _locSalesOrderXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locSalesOrderXPO.ActiveApproved1 = true;
                                                    _locSalesOrderXPO.ActiveApproved2 = false;
                                                    _locSalesOrderXPO.ActiveApproved3 = false;
                                                    _locSalesOrderXPO.Save();
                                                    _locSalesOrderXPO.Session.CommitTransaction();
                                                }

                                                //Send Email
                                                ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("SalesOrder", _locSalesOrderXPO)));

                                                if (_locApprovalLineXPO2 != null)
                                                {
                                                    SendEmail(_currentSession, _locSalesOrderXPO, Status.Progress, _locApprovalLineXPO2);
                                                }

                                                SuccessMessageShow("SalesOrder has successfully Approve");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 1

                                    #region Approval Level 2
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level2)
                                    {
                                        _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level2, ObjectList.SalesOrder);

                                        if (_locAppSetDetail != null)
                                        {
                                            ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("SalesOrder", _locSalesOrderXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level2)));

                                            if (_locApprovalLineXPO == null)
                                            {
                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level2,
                                                        EndApproval = true,
                                                        SalesOrder = _locSalesOrderXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locSalesOrderXPO.ActivationPosting = true;
                                                    _locSalesOrderXPO.ActiveApproved1 = false;
                                                    _locSalesOrderXPO.ActiveApproved2 = false;
                                                    _locSalesOrderXPO.ActiveApproved3 = true;
                                                    _locSalesOrderXPO.Save();
                                                    _locSalesOrderXPO.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level2,
                                                        SalesOrder = _locSalesOrderXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    SetApprovalLine(_currentSession, _locSalesOrderXPO, ApprovalLevel.Level1);

                                                    _locSalesOrderXPO.ActiveApproved1 = false;
                                                    _locSalesOrderXPO.ActiveApproved2 = true;
                                                    _locSalesOrderXPO.ActiveApproved3 = false;
                                                    _locSalesOrderXPO.Save();
                                                    _locSalesOrderXPO.Session.CommitTransaction();
                                                }

                                                //Send Email
                                                ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("SalesOrder", _locSalesOrderXPO)));

                                                if (_locApprovalLineXPO2 != null)
                                                {
                                                    SendEmail(_currentSession, _locSalesOrderXPO, Status.Progress, _locApprovalLineXPO2);
                                                }

                                                SuccessMessageShow("SalesOrder has successfully Approve");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 2

                                    #region Approval Level 3
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level3)
                                    {
                                        _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level3, ObjectList.SalesOrder);

                                        if (_locAppSetDetail != null)
                                        {
                                            ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("SalesOrder", _locSalesOrderXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level3)));

                                            if (_locApprovalLineXPO == null)
                                            {
                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level3,
                                                        EndApproval = true,
                                                        SalesOrder = _locSalesOrderXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locSalesOrderXPO.ActivationPosting = true;
                                                    _locSalesOrderXPO.ActiveApproved1 = false;
                                                    _locSalesOrderXPO.ActiveApproved2 = false;
                                                    _locSalesOrderXPO.ActiveApproved3 = true;
                                                    _locSalesOrderXPO.Save();
                                                    _locSalesOrderXPO.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level3,
                                                        SalesOrder = _locSalesOrderXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    SetApprovalLine(_currentSession, _locSalesOrderXPO, ApprovalLevel.Level2);
                                                    SetApprovalLine(_currentSession, _locSalesOrderXPO, ApprovalLevel.Level1);

                                                    _locSalesOrderXPO.ActiveApproved1 = false;
                                                    _locSalesOrderXPO.ActiveApproved2 = false;
                                                    _locSalesOrderXPO.ActiveApproved3 = true;
                                                    _locSalesOrderXPO.Save();
                                                    _locSalesOrderXPO.Session.CommitTransaction();
                                                }

                                                //Send Email
                                                ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("SalesOrder", _locSalesOrderXPO)));

                                                if (_locApprovalLineXPO2 != null)
                                                {
                                                    SendEmail(_currentSession, _locSalesOrderXPO, Status.Progress, _locApprovalLineXPO2);
                                                }

                                                SuccessMessageShow("SalesOrder has successfully Approve");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 3
                                }

                            }
                            else
                            {
                                ErrorMessageShow("SalesOrder Status Not Available");
                            }
                        }
                        else
                        {
                            ErrorMessageShow("SalesOrder Not Available");
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrder " + ex.ToString());
            }
        }

        //=================================== Code Only =====================================================

        #region PostingMethod

        private void SetSalesOrderMonitoring(Session _currSession, SalesOrder _locSalesOrderXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                
                if (_locSalesOrderXPO != null)
                {
                    if(_locSalesOrderXPO.Status == Status.Progress || _locSalesOrderXPO.Status == Status.Posted)
                    {
                        XPCollection<SalesOrderLine> _locSalesOrderLines = new XPCollection<SalesOrderLine>
                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("SalesOrder", _locSalesOrderXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Progress),
                                                            new BinaryOperator("Status", Status.Posted))));

                        if (_locSalesOrderLines != null && _locSalesOrderLines.Count() > 0)
                        {
                            foreach (SalesOrderLine _locSalesOrderLine in _locSalesOrderLines)
                            {
                                SalesOrderMonitoring _saveDataSalesOrderMonitoring = new SalesOrderMonitoring(_currSession)
                                {
                                    SalesOrder = _locSalesOrderXPO,
                                    SalesOrderLine = _locSalesOrderLine,
                                    SalesType = _locSalesOrderLine.SalesType,
                                    Item = _locSalesOrderLine.Item,
                                    DQty = _locSalesOrderLine.DQty,
                                    DUOM = _locSalesOrderLine.DUOM,
                                    Qty = _locSalesOrderLine.Qty,
                                    UOM = _locSalesOrderLine.UOM,
                                    TQty = _locSalesOrderLine.TQty,
                                    Currency = _locSalesOrderLine.Currency,
                                    PriceGroup = _locSalesOrderLine.PriceGroup,
                                    Price = _locSalesOrderLine.Price,
                                    PriceLine = _locSalesOrderLine.PriceLine,
                                    UAmount = _locSalesOrderLine.UAmount,
                                    TUAmount = _locSalesOrderLine.TUAmount,
                                    MultiTax = _locSalesOrderLine.MultiTax,
                                    Tax = _locSalesOrderLine.Tax,
                                    TxValue = _locSalesOrderLine.TxValue,
                                    TxAmount = _locSalesOrderLine.TxAmount,
                                    MultiDiscount = _locSalesOrderLine.MultiDiscount,
                                    Discount = _locSalesOrderLine.Discount,
                                    Disc = _locSalesOrderLine.Disc,
                                    DiscAmount = _locSalesOrderLine.DiscAmount,
                                    TAmount = _locSalesOrderLine.TAmount,
                                };
                                _saveDataSalesOrderMonitoring.Save();
                                _saveDataSalesOrderMonitoring.Session.CommitTransaction();  
                            }
                        }
                    }  
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = SalesOrder ", ex.ToString());
            }
        }

        private void SetRemainQty(Session _currSession, SalesOrder _locSalesOrderXPO)
        {
            try
            {
                if (_locSalesOrderXPO != null)
                {
                    XPCollection<SalesOrderLine> _locSalesOrderLines = new XPCollection<SalesOrderLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("SalesOrder", _locSalesOrderXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locSalesOrderLines != null && _locSalesOrderLines.Count > 0)
                    {
                        double _locRmDQty = 0;
                        double _locRmQty = 0;
                        double _locInvLineTotal = 0;
                        ItemUnitOfMeasure _locItemUOM = null;

                        foreach (SalesOrderLine _locSalesOrderLine in _locSalesOrderLines)
                        {
                            #region ProcessCount=0
                            if (_locSalesOrderLine.PostedCount == 0)
                            {
                                #region MaxQuantity
                                if (_locSalesOrderLine.MxDQty > 0)
                                {
                                    if (_locSalesOrderLine.DQty > 0 && _locSalesOrderLine.DQty <= _locSalesOrderLine.MxDQty)
                                    {
                                        _locRmDQty = _locSalesOrderLine.MxDQty - _locSalesOrderLine.DQty;
                                    }

                                    if (_locSalesOrderLine.Qty > 0 && _locSalesOrderLine.Qty <= _locSalesOrderLine.MxQty)
                                    {
                                        _locRmQty = _locSalesOrderLine.MxQty - _locSalesOrderLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locSalesOrderLine.Item),
                                                                new BinaryOperator("UOM", _locSalesOrderLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locSalesOrderLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty + _locRmDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }

                                }
                                #endregion MaxQuantity
                                #region NonMaxQuantity
                                else
                                {
                                    if (_locSalesOrderLine.DQty > 0)
                                    {
                                        _locRmDQty = 0;
                                    }

                                    if (_locSalesOrderLine.Qty > 0)
                                    {
                                        _locRmQty = 0;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locSalesOrderLine.Item),
                                                                new BinaryOperator("UOM", _locSalesOrderLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locSalesOrderLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty + _locRmDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion ProcessCount=0

                            #region ProcessCount>0
                            if (_locSalesOrderLine.PostedCount > 0)
                            {
                                if (_locSalesOrderLine.RmDQty > 0)
                                {
                                    _locRmDQty = _locSalesOrderLine.RmDQty - _locSalesOrderLine.DQty;
                                }

                                if (_locSalesOrderLine.RmQty > 0)
                                {
                                    _locRmQty = _locSalesOrderLine.RmQty - _locSalesOrderLine.Qty;
                                }

                                if (_locSalesOrderLine.MxDQty > 0 || _locSalesOrderLine.MxQty > 0)
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locSalesOrderLine.Item),
                                                                new BinaryOperator("UOM", _locSalesOrderLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locSalesOrderLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                }
                                else
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locSalesOrderLine.Item),
                                                                new BinaryOperator("UOM", _locSalesOrderLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locSalesOrderLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                }


                                if (_locItemUOM != null)
                                {
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locRmQty + _locRmDQty;
                                }

                            }
                            #endregion ProcessCount>0

                            _locSalesOrderLine.RmDQty = _locRmDQty;
                            _locSalesOrderLine.RmQty = _locRmQty;
                            _locSalesOrderLine.RmTQty = _locInvLineTotal;
                            _locSalesOrderLine.Save();
                            _locSalesOrderLine.Session.CommitTransaction();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrder " + ex.ToString());
            }
        }

        private void SetPostingQty(Session _currSession, SalesOrder _locSalesOrderXPO)
        {
            try
            {
                if (_locSalesOrderXPO != null)
                {
                    XPCollection<SalesOrderLine> _locSalesOrderLines = new XPCollection<SalesOrderLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("SalesOrder", _locSalesOrderXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locSalesOrderLines != null && _locSalesOrderLines.Count > 0)
                    {
                        double _locPDQty = 0;
                        double _locPQty = 0;
                        double _locInvLineTotal = 0;
                        ItemUnitOfMeasure _locItemUOM = null;

                        foreach (SalesOrderLine _locSalesOrderLine in _locSalesOrderLines)
                        {
                            #region ProcessCount=0
                            if (_locSalesOrderLine.PostedCount == 0)
                            {
                                #region MaxQuantity
                                if (_locSalesOrderLine.MxDQty > 0)
                                {
                                    if (_locSalesOrderLine.DQty > 0 && _locSalesOrderLine.DQty <= _locSalesOrderLine.MxDQty)
                                    {
                                        _locPDQty = _locSalesOrderLine.DQty;
                                    }

                                    if (_locSalesOrderLine.Qty > 0 && _locSalesOrderLine.Qty <= _locSalesOrderLine.MxQty)
                                    {
                                        _locPQty = _locSalesOrderLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locSalesOrderLine.Item),
                                                                new BinaryOperator("UOM", _locSalesOrderLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locSalesOrderLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty + _locPDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }

                                }
                                #endregion MaxQuantity
                                #region NonMaxQuantity
                                else
                                {
                                    if (_locSalesOrderLine.DQty > 0)
                                    {
                                        _locPDQty = _locSalesOrderLine.DQty;
                                    }

                                    if (_locSalesOrderLine.Qty > 0)
                                    {
                                        _locPQty = _locSalesOrderLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locSalesOrderLine.Item),
                                                                new BinaryOperator("UOM", _locSalesOrderLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locSalesOrderLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty + _locPDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion ProcessCount=0

                            #region ProcessCount>0
                            if (_locSalesOrderLine.PostedCount > 0)
                            {
                                if (_locSalesOrderLine.PDQty > 0)
                                {
                                    _locPDQty = _locSalesOrderLine.PDQty + _locSalesOrderLine.DQty;
                                }

                                if (_locSalesOrderLine.PQty > 0)
                                {
                                    _locPQty = _locSalesOrderLine.PQty + _locSalesOrderLine.Qty;
                                }

                                if (_locSalesOrderLine.MxDQty > 0 || _locSalesOrderLine.MxQty > 0)
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                           (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Item", _locSalesOrderLine.Item),
                                                            new BinaryOperator("UOM", _locSalesOrderLine.MxUOM),
                                                            new BinaryOperator("DefaultUOM", _locSalesOrderLine.MxDUOM),
                                                            new BinaryOperator("Active", true)));
                                }
                                else
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                           (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Item", _locSalesOrderLine.Item),
                                                            new BinaryOperator("UOM", _locSalesOrderLine.UOM),
                                                            new BinaryOperator("DefaultUOM", _locSalesOrderLine.DUOM),
                                                            new BinaryOperator("Active", true)));
                                }

                                if (_locItemUOM != null)
                                {
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locPQty + _locPDQty;
                                }

                            }
                            #endregion ProcessCount>0

                            _locSalesOrderLine.PDQty = _locPDQty;
                            _locSalesOrderLine.PDUOM = _locSalesOrderLine.DUOM;
                            _locSalesOrderLine.PQty = _locPQty;
                            _locSalesOrderLine.PUOM = _locSalesOrderLine.UOM;
                            _locSalesOrderLine.PTQty = _locInvLineTotal;
                            _locSalesOrderLine.Save();
                            _locSalesOrderLine.Session.CommitTransaction();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrder " + ex.ToString());
            }

        }

        private void SetProcessCount(Session _currSession, SalesOrder _locSalesOrderXPO)
        {
            try
            {
                if (_locSalesOrderXPO != null)
                {
                    XPCollection<SalesOrderLine> _locSalesOrderLines = new XPCollection<SalesOrderLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("SalesOrder", _locSalesOrderXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locSalesOrderLines != null && _locSalesOrderLines.Count > 0)
                    {

                        foreach (SalesOrderLine _locSalesOrderLine in _locSalesOrderLines)
                        {
                            if (_locSalesOrderLine.Status == Status.Progress || _locSalesOrderLine.Status == Status.Posted)
                            {
                                _locSalesOrderLine.PostedCount = _locSalesOrderLine.PostedCount + 1;
                                _locSalesOrderLine.Save();
                                _locSalesOrderLine.Session.CommitTransaction();
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrder " + ex.ToString());
            }
        }

        private void SetStatusSalesOrderLine(Session _currSession, SalesOrder _locSalesOrderXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locSalesOrderXPO != null)
                {
                    XPCollection<SalesOrderLine> _locSalesOrderLines = new XPCollection<SalesOrderLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("SalesOrder", _locSalesOrderXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locSalesOrderLines != null && _locSalesOrderLines.Count > 0)
                    {

                        foreach (SalesOrderLine _locSalesOrderLine in _locSalesOrderLines)
                        {
                            if (_locSalesOrderLine.Status == Status.Progress || _locSalesOrderLine.Status == Status.Posted)
                            {
                                if (_locSalesOrderLine.RmDQty == 0 && _locSalesOrderLine.RmQty == 0 && _locSalesOrderLine.RmTQty == 0)
                                {
                                    _locSalesOrderLine.Status = Status.Close;
                                    _locSalesOrderLine.ActivationQuantity = true;
                                    _locSalesOrderLine.ActivationPosting = true;
                                    _locSalesOrderLine.StatusDate = now;
                                }
                                else
                                {
                                    _locSalesOrderLine.Status = Status.Posted;
                                    _locSalesOrderLine.StatusDate = now;
                                }
                                _locSalesOrderLine.Save();
                                _locSalesOrderLine.Session.CommitTransaction();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrder " + ex.ToString());
            }
        }

        private void SetNormalQuantity(Session _currSession, SalesOrder _locSalesOrderXPO)
        {
            try
            {
                if (_locSalesOrderXPO != null)
                {
                    XPCollection<SalesOrderLine> _locSalesOrderLines = new XPCollection<SalesOrderLine>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("SalesOrder", _locSalesOrderXPO),
                                                                                     new BinaryOperator("Select", true)));

                    if (_locSalesOrderLines != null && _locSalesOrderLines.Count > 0)
                    {
                        foreach (SalesOrderLine _locSalesOrderLine in _locSalesOrderLines)
                        {
                            if (_locSalesOrderLine.Status == Status.Progress || _locSalesOrderLine.Status == Status.Posted || _locSalesOrderLine.Status == Status.Close)
                            {
                                if (_locSalesOrderLine.DQty > 0 || _locSalesOrderLine.Qty > 0)
                                {
                                    _locSalesOrderLine.Select = false;
                                    _locSalesOrderLine.DQty = 0;
                                    _locSalesOrderLine.Qty = 0;
                                    _locSalesOrderLine.Save();
                                    _locSalesOrderLine.Session.CommitTransaction();
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrder " + ex.ToString());
            }
        }

        private void SetFinalStatusSalesOrder(Session _currSession, SalesOrder _locSalesOrderXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locStatusCount = 0;
                int _locPurchaseRequisitionLineCount = 0;

                if (_locSalesOrderXPO != null)
                {

                    XPCollection<SalesOrderLine> _locSalesOrderLines = new XPCollection<SalesOrderLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("SalesOrder", _locSalesOrderXPO)));

                    if (_locSalesOrderLines != null && _locSalesOrderLines.Count() > 0)
                    {
                        _locPurchaseRequisitionLineCount = _locSalesOrderLines.Count();

                        foreach (SalesOrderLine _locSalesOrderLine in _locSalesOrderLines)
                        {
                            if (_locSalesOrderLine.Status == Status.Close)
                            {
                                _locStatusCount = _locStatusCount + 1;
                            }
                        }
                    }

                    if (_locStatusCount == _locPurchaseRequisitionLineCount)
                    {
                        _locSalesOrderXPO.ActivationPosting = true;
                        _locSalesOrderXPO.Status = Status.Posted;
                        _locSalesOrderXPO.StatusDate = now;
                        _locSalesOrderXPO.Save();
                        _locSalesOrderXPO.Session.CommitTransaction();
                    }
                    else
                    {
                        _locSalesOrderXPO.Status = Status.Posted;
                        _locSalesOrderXPO.StatusDate = now;
                        _locSalesOrderXPO.Save();
                        _locSalesOrderXPO.Session.CommitTransaction();
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrder " + ex.ToString());
            }
        }

        private void SetMaksBill(Session _currSession, SalesOrder _locSalesOrderXPO)
        {
            try
            {
                double _locTxValue = 0;
                double _locTxAmount = 0;
                double _locDisc = 0;
                double _locDiscAmount = 0;
                double _locTAmount = 0;
                double _locGTAmount = 0;

                if (_locSalesOrderXPO != null)
                {
                    XPCollection<SalesOrderLine> _locSalesOrderLines = new XPCollection<SalesOrderLine>
                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("SalesOrder", _locSalesOrderXPO)));

                    if(_locSalesOrderLines != null && _locSalesOrderLines.Count() > 0)
                    {
                        foreach(SalesOrderLine _locSalesOrderLine in _locSalesOrderLines)
                        {
                            //Count Tax
                            if (_locSalesOrderLine.MultiTax == true)
                            {
                                _locTxAmount = GetSumTotalTaxAmount(_currSession, _locSalesOrderLine);
                            }
                            else
                            {
                                if (_locSalesOrderLine.TxValue > 0)
                                {
                                    _locTxValue = _locSalesOrderLine.TxValue;
                                    _locTxAmount = (_locTxValue / 100) * (_locSalesOrderLine.MxTUAmount);
                                }
                            }

                            //Count Discount
                            if (_locSalesOrderLine.MultiDiscount == true)
                            {
                                _locDiscAmount = GetSumTotalDiscAmount(_currSession, _locSalesOrderLine);
                            }
                            else
                            {
                                if (_locSalesOrderLine.Disc > 0)
                                {
                                    _locDisc = _locSalesOrderLine.Disc;
                                    _locDiscAmount = (_locDisc / 100) * (_locSalesOrderLine.MxTUAmount);
                                }
                            }

                            _locTAmount = (_locSalesOrderLine.MxTUAmount + _locTxAmount) - _locDiscAmount;

                            _locGTAmount = _locGTAmount + _locTAmount;
                        }

                        if(_locGTAmount > 0)
                        {
                            _locSalesOrderXPO.MaxBill = _locGTAmount;
                            _locSalesOrderXPO.Save();
                            _locSalesOrderXPO.Session.CommitTransaction();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrder " + ex.ToString());
            }
        }

        private double GetSumTotalDiscAmount(Session _currSession, SalesOrderLine _salesOrderLineXPO )
        {
            double _result = 0;
            try
            {
                double _locDiscAmount = 0;

                if (_salesOrderLineXPO != null)
                {
                    XPCollection<DiscountLine> _locDiscountLines = new XPCollection<DiscountLine>(_currSession,
                                                               new BinaryOperator("SalesOrderLine", _salesOrderLineXPO));

                    if (_locDiscountLines.Count() > 0)
                    {
                        foreach (DiscountLine _locDiscountLine in _locDiscountLines)
                        {
                            if (_locDiscountLine.Disc > 0)
                            {
                                _locDiscAmount = _locDiscAmount + (_salesOrderLineXPO.MxTUAmount * _locDiscountLine.Disc / 100);
                            }
                        }
                        _result = _locDiscAmount;
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = SalesInvoice" + ex.ToString());
            }
            return _result;
        }

        private double GetSumTotalTaxAmount(Session _currSession, SalesOrderLine _salesOrderLineXPO)
        {
            double _return = 0;
            try
            {
                double _locTxAmount = 0;

                if (_salesOrderLineXPO != null)
                {
                    XPCollection<TaxLine> _locTaxLines = new XPCollection<TaxLine>(_currSession,
                                                     new BinaryOperator("SalesOrderLine", _salesOrderLineXPO));

                    if (_locTaxLines.Count() > 0)
                    {
                        foreach (TaxLine _locTaxLine in _locTaxLines)
                        {
                            if (_locTaxLine.TxValue > 0)
                            {
                                if (_locTaxLine.TaxNature == TaxNature.Increase)
                                {
                                    _locTxAmount = _locTxAmount + (_salesOrderLineXPO.MxTUAmount * _locTaxLine.TxValue / 100);
                                }
                                else if (_locTaxLine.TaxNature == TaxNature.Decrease)
                                {
                                    _locTxAmount = _locTxAmount - (_salesOrderLineXPO.MxTUAmount * _locTaxLine.TxValue / 100);
                                }
                            }
                        }
                        _return = _locTxAmount;
                    }
                }


            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = SalesInvoice" + ex.ToString());
            }
            return _return;
        }

        #endregion PostingMethod

        #region GetSQ

        private void GetSalesQuotationMonitoring(Session _currSession, SalesQuotation _locSalesQuotationXPO, SalesOrder _locSalesOrderXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _currSignCode = null;

                if (_locSalesQuotationXPO != null && _locSalesOrderXPO != null)
                {
                    //Hanya memindahkan SalesQuotationMonitoring ke SalesOrderLine
                    XPCollection<SalesQuotationMonitoring> _locSalesQuotationMonitorings = new XPCollection<SalesQuotationMonitoring>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("SalesQuotation", _locSalesQuotationXPO),
                                                                                        new BinaryOperator("SalesOrder", _locSalesOrderXPO),
                                                                                        new BinaryOperator("Select", true),
                                                                                        new BinaryOperator("PostedCount", 0)));

                    if(_locSalesQuotationMonitorings != null && _locSalesQuotationMonitorings.Count() > 0)
                    {
                        foreach(SalesQuotationMonitoring _locSalesQuotationMonitoring in _locSalesQuotationMonitorings)
                        {
                            if(_locSalesQuotationMonitoring.SalesQuotationLine != null)
                            {
                                _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.SalesOrderLine);

                                if (_currSignCode != null)
                                {
                                    SalesOrderLine _saveDataSalesOrderLine = new SalesOrderLine(_currSession)
                                    {
                                        SignCode = _currSignCode,
                                        SalesType = _locSalesQuotationMonitoring.SalesType,
                                        Item = _locSalesQuotationMonitoring.Item,
                                        Description = _locSalesQuotationMonitoring.SalesQuotationLine.Description,
                                        MxDQty = _locSalesQuotationMonitoring.DQty,
                                        MxDUOM = _locSalesQuotationMonitoring.DUOM,
                                        MxQty = _locSalesQuotationMonitoring.Qty,
                                        MxUOM = _locSalesQuotationMonitoring.UOM,
                                        MxTQty = _locSalesQuotationMonitoring.TQty,
                                        MxUAmount = _locSalesQuotationMonitoring.UAmount,
                                        MxTUAmount = _locSalesQuotationMonitoring.TQty * _locSalesQuotationMonitoring.UAmount,
                                        DQty = _locSalesQuotationMonitoring.DQty,
                                        DUOM = _locSalesQuotationMonitoring.DUOM,
                                        Qty = _locSalesQuotationMonitoring.Qty,
                                        UOM = _locSalesQuotationMonitoring.UOM,
                                        TQty = _locSalesQuotationMonitoring.TQty,
                                        Currency = _locSalesQuotationMonitoring.Currency,
                                        PriceGroup = _locSalesQuotationMonitoring.PriceGroup,
                                        Price = _locSalesQuotationMonitoring.Price,
                                        PriceLine = _locSalesQuotationMonitoring.PriceLine,
                                        UAmount = _locSalesQuotationMonitoring.UAmount,
                                        TUAmount = _locSalesQuotationMonitoring.TUAmount,
                                        MultiTax = _locSalesQuotationMonitoring.MultiTax,
                                        Tax = _locSalesQuotationMonitoring.Tax,
                                        TxValue = _locSalesQuotationMonitoring.TxValue,
                                        TxAmount = _locSalesQuotationMonitoring.TxAmount,
                                        MultiDiscount = _locSalesQuotationMonitoring.MultiDiscount,
                                        Disc = _locSalesQuotationMonitoring.Disc,
                                        Discount = _locSalesQuotationMonitoring.Discount,
                                        DiscAmount = _locSalesQuotationMonitoring.DiscAmount,
                                        TAmount = _locSalesQuotationMonitoring.TAmount,
                                        SalesQuotationMonitoring = _locSalesQuotationMonitoring,
                                        SalesOrder = _locSalesOrderXPO,
                                    };
                                    _saveDataSalesOrderLine.Save();
                                    _saveDataSalesOrderLine.Session.CommitTransaction();

                                    SalesOrderLine _locSalOrderLine = _currSession.FindObject<SalesOrderLine>
                                                                (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("SignCode", _currSignCode)));
                                    if (_locSalOrderLine != null)
                                    {
                                        if (_locSalesQuotationMonitoring.MultiTax == true)
                                        {
                                            XPCollection<TaxLine> _locTaxLines = new XPCollection<TaxLine>(_currSession, new GroupOperator(
                                                                                GroupOperatorType.And,
                                                                                new BinaryOperator("SalesQuotationLine", _locSalesQuotationMonitoring.SalesQuotationLine)));
                                            if (_locTaxLines != null && _locTaxLines.Count() > 0)
                                            {
                                                foreach (TaxLine _locTaxLine in _locTaxLines)
                                                {
                                                    TaxLine _saveDataTaxLine = new TaxLine(_currSession)
                                                    {
                                                        Tax = _locTaxLine.Tax,
                                                        Name = _locTaxLine.Name,
                                                        TaxType = _locTaxLine.TaxType,
                                                        TaxGroup = _locTaxLine.TaxGroup,
                                                        TaxGroupType = _locTaxLine.TaxGroupType,
                                                        TaxAccountGroup = _locTaxLine.TaxAccountGroup,
                                                        TaxNature = _locTaxLine.TaxNature,
                                                        TaxMode = _locTaxLine.TaxMode,
                                                        TxValue = _locTaxLine.TxValue,
                                                        Description = _locTaxLine.Description,
                                                        UAmount = _locSalOrderLine.UAmount,
                                                        TUAmount = _locSalOrderLine.TUAmount,
                                                        TxAmount = _locSalOrderLine.TxAmount,
                                                        SalesOrderLine = _locSalOrderLine,
                                                    };
                                                    _saveDataTaxLine.Save();
                                                    _saveDataTaxLine.Session.CommitTransaction();
                                                }
                                            }
                                        }

                                        if (_locSalesQuotationMonitoring.MultiDiscount == true)
                                        {
                                            XPCollection<DiscountLine> _locDiscountLines = new XPCollection<DiscountLine>(_currSession,
                                                                                new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("SalesQuotationLine", _locSalesQuotationMonitoring.SalesQuotationLine)));
                                            if (_locDiscountLines != null && _locDiscountLines.Count() > 0)
                                            {
                                                foreach (DiscountLine _locDiscountLine in _locDiscountLines)
                                                {
                                                    DiscountLine _saveDataDiscountLine = new DiscountLine(_currSession)
                                                    {
                                                        Discount = _locDiscountLine.Discount,
                                                        Name = _locDiscountLine.Name,
                                                        DiscountMode = _locDiscountLine.DiscountMode,
                                                        Description = _locDiscountLine.Description,
                                                        UAmount = _locSalOrderLine.UAmount,
                                                        Disc = _locDiscountLine.Disc,
                                                        DiscAmount = _locSalOrderLine.DiscAmount,
                                                        TUAmount = _locSalOrderLine.TUAmount,
                                                        SalesOrderLine = _locSalOrderLine,
                                                    };
                                                    _saveDataDiscountLine.Save();
                                                    _saveDataDiscountLine.Session.CommitTransaction();
                                                }
                                            }
                                        }

                                        //Masukkan MultiDiscount

                                        _locSalesQuotationMonitoring.SalesOrderLine = _locSalOrderLine;
                                        _locSalesQuotationMonitoring.Status = Status.Close;
                                        _locSalesQuotationMonitoring.StatusDate = now;
                                        _locSalesQuotationMonitoring.PostedCount = _locSalesQuotationMonitoring.PostedCount + 1;
                                        _locSalesQuotationMonitoring.Save();
                                        _locSalesQuotationMonitoring.Session.CommitTransaction();
                                    }
                                }
                            }     
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = SalesOrder ", ex.ToString());
            }
        }

        #endregion GetSQ

        #region PostingWR

        private void SetWorkRequisition(Session _currSession, SalesOrder _locSalesOrderXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _currSignCode = null;

                if (_locSalesOrderXPO != null)
                {
                    if (_locSalesOrderXPO.Status == Status.Progress)
                    {
                        _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.WorkRequisition);

                        if (_currSignCode != null)
                        {
                            WorkRequisition _saveDataWorkRequisition = new WorkRequisition(_currSession)
                            {
                                Name = _locSalesOrderXPO.SalesToCustomer.Name,
                                Company = _locSalesOrderXPO.Company,
                                SalesOrder = _locSalesOrderXPO,
                            };
                            _saveDataWorkRequisition.Save();
                            _saveDataWorkRequisition.Session.CommitTransaction();
                        }
                    }

                    XPCollection<SalesOrderLine> _numLineSalesOrderLines = new XPCollection<SalesOrderLine>(_currSession,
                                                                           new GroupOperator(GroupOperatorType.And,
                                                                           new BinaryOperator("SalesOrder", _locSalesOrderXPO)));

                    if (_numLineSalesOrderLines != null && _numLineSalesOrderLines.Count > 0)
                    {
                        WorkRequisition _locWorkRequisition2 = _currSession.FindObject<WorkRequisition>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("SalesOrder", _locSalesOrderXPO)));

                        if (_locWorkRequisition2 != null)
                        {
                            if (_numLineSalesOrderLines != null && _numLineSalesOrderLines.Count > 0)
                            {
                                foreach (SalesOrderLine _numLineSalesOrderLine in _numLineSalesOrderLines)
                                {
                                    if (_numLineSalesOrderLine.Status == Status.Progress || _numLineSalesOrderLine.Status == Status.Posted)
                                    {
                                        WorkRequisitionLine _saveDataWorkRequisitionLine = new WorkRequisitionLine(_currSession)
                                        {
                                            Name = _numLineSalesOrderLine.Name,
                                            Item = _numLineSalesOrderLine.Item,
                                            Description = _numLineSalesOrderLine.Description,
                                            MxDQty = _numLineSalesOrderLine.DQty,
                                            MxDUOM = _numLineSalesOrderLine.DUOM,
                                            MxQty = _numLineSalesOrderLine.Qty,
                                            MxUOM = _numLineSalesOrderLine.UOM,
                                            MxTQty = _numLineSalesOrderLine.MxTQty,
                                            DQty = _numLineSalesOrderLine.DQty,
                                            DUOM = _numLineSalesOrderLine.DUOM,
                                            Qty = _numLineSalesOrderLine.Qty,
                                            UOM = _numLineSalesOrderLine.UOM,
                                            TQty = _numLineSalesOrderLine.TQty,
                                            WorkRequisition = _locWorkRequisition2,
                                        };
                                        _saveDataWorkRequisitionLine.Save();
                                        _saveDataWorkRequisitionLine.Session.CommitTransaction();
                                    }
                                    else
                                    {
                                        ErrorMessageShow("Data Status SalesOrderLine Not Available");
                                    }
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data SalesOrderLine Not Available");
                            }
                        }
                        else
                        {
                            ErrorMessageShow("Data SalesOrder Not Available");
                        }
                    }
                    else
                    {
                        ErrorMessageShow("Data SalesOrderLine Not Available");
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = SalesOrder ", ex.ToString());
            }
        }

        private void SetStatusSalesOrderLineWR(Session _currSession, SalesOrder _locSalesOrderXPO)
        {
            try
            {
                DateTime now = DateTime.Now;

                if (_locSalesOrderXPO != null)
                {
                    XPCollection<SalesOrderLine> _locSalesOrderLines = new XPCollection<SalesOrderLine>(_currSession,
                                                                       new GroupOperator(GroupOperatorType.And,
                                                                       new BinaryOperator("SalesOrder", _locSalesOrderXPO)));

                    if (_locSalesOrderLines != null && _locSalesOrderLines.Count > 0)
                    {
                        foreach (SalesOrderLine _locSalesOrderLine in _locSalesOrderLines)
                        {
                            if (_locSalesOrderLine.StatusWR == Status.Progress || _locSalesOrderLine.StatusWR == Status.Posted)
                            {
                                _locSalesOrderLine.StatusWR = Status.Posted;
                                _locSalesOrderLine.StatusDateWR = now;
                                _locSalesOrderLine.Save();
                                _locSalesOrderLine.Session.CommitTransaction();
                            }
                            else
                            {
                                ErrorMessageShow("Data Status SalesOrderLine Not Available");
                            }
                        }
                    }
                    else
                    {
                        ErrorMessageShow("Data SalesOrderLine Not Available");
                    }
                }
                else
                {
                    ErrorMessageShow("Data Status SalesOrder Not Available");
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrder " + ex.ToString());
            }
        }

        private void SetPostedWRCount(Session _currSession, SalesOrder _locSalesOrderXPO)
        {
            try
            {
                if (_locSalesOrderXPO != null)
                {
                    XPCollection<SalesOrderLine> _locSalesOrderLines = new XPCollection<SalesOrderLine>(_currSession,
                                                                       new GroupOperator(GroupOperatorType.And,
                                                                       new BinaryOperator("SalesOrder", _locSalesOrderXPO)));

                    if (_locSalesOrderLines != null && _locSalesOrderLines.Count > 0)
                    {
                        foreach (SalesOrderLine _locSalesOrderLine in _locSalesOrderLines)
                        {
                            if (_locSalesOrderLine.Status == Status.Progress || _locSalesOrderLine.Status == Status.Posted)
                            {
                                if (_locSalesOrderLine.DQty > 0 || _locSalesOrderLine.Qty > 0)
                                {
                                    _locSalesOrderLine.PostedCountWR = _locSalesOrderLine.PostedCountWR + 1;
                                    _locSalesOrderLine.Save();
                                    _locSalesOrderLine.Session.CommitTransaction();
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Status WorkRequisitionLine Not Available");
                            }
                        }
                    }
                    else
                    {
                        ErrorMessageShow("Data WorkRequisitionLine Not Available");
                    }
                }
                else
                {
                    ErrorMessageShow("Data SalesOrder Not Available");
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrder " + ex.ToString());
            }
        }

        private void SetFinalStatusSalesOrderWR(Session _currSession, SalesOrder _locSalesOrderXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locPostedCountWR = 0;
                int _locSalesOrderLineCount = 0;

                if (_locSalesOrderXPO != null)
                {
                    XPCollection<SalesOrderLine> _locSalesOrderLines = new XPCollection<SalesOrderLine>(_currSession,
                                                                       new GroupOperator(GroupOperatorType.And,
                                                                       new BinaryOperator("SalesOrder", _locSalesOrderXPO)));

                    if (_locSalesOrderLines != null && _locSalesOrderLines.Count() > 0)
                    {
                        _locSalesOrderLineCount = _locSalesOrderLines.Count();

                        foreach (SalesOrderLine _locSalesOrderLine in _locSalesOrderLines)
                        {
                            if (_locSalesOrderLine.StatusWR == Status.Close)
                            {
                                _locPostedCountWR = _locPostedCountWR + 1;
                            }
                        }
                    }

                    if (_locPostedCountWR == _locSalesOrderLineCount)
                    {
                        _locSalesOrderXPO.ActivationPosting = true;
                        _locSalesOrderXPO.StatusWR = Status.Close;
                        _locSalesOrderXPO.StatusDateWR = now;
                        _locSalesOrderXPO.Save();
                        _locSalesOrderXPO.Session.CommitTransaction();
                    }
                    else
                    {
                        _locSalesOrderXPO.StatusWR = Status.Posted;
                        _locSalesOrderXPO.StatusDateWR = now;
                        _locSalesOrderXPO.Save();
                        _locSalesOrderXPO.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrder " + ex.ToString());
            }
        }

        #endregion PostingWR

        #region Approval Process

        private void SetApprovalLine(Session _currentSession, SalesOrder _locSalesOrderXPO, ApprovalLevel _locApprovalLevel)
        {
            try
            {
                DateTime now = DateTime.Now;

                ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("SalesOrder", _locSalesOrderXPO),
                                                                         new BinaryOperator("ApprovalLevel", _locApprovalLevel)));

                if (_locApprovalLineXPO == null)
                {
                    ApprovalLine _saveDataAL2 = new ApprovalLine(_currentSession)
                    {
                        ApprovalDate = now,
                        ApprovalStatus = Status.Approved,
                        ApprovalLevel = _locApprovalLevel,
                        SalesOrder = _locSalesOrderXPO,
                    };
                    _saveDataAL2.Save();
                    _saveDataAL2.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesQuotation " + ex.ToString());
            }
        }

        #endregion Approval Process

        #region Email

        private string BackgroundBody(Session _currentSession, SalesOrder _locSalesOrderXPO, MailSetupDetail _locMailSetDetail)
        {
            string body = string.Empty;

            SalesOrder _locSalesOrders = _currentSession.FindObject<SalesOrder>
                                           (new GroupOperator(GroupOperatorType.And,
                                            new BinaryOperator("Code", _locSalesOrderXPO.Code)));

            ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                               (new GroupOperator(GroupOperatorType.And,
                                                new BinaryOperator("SalesOrder", _locSalesOrders)));

            string Status = _locApprovalLineXPO.ApprovalStatus.ToString();

            using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Email/EmailTemplate.html")))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{Code}", _locSalesOrderXPO.Code);

            #region Level
            if (_locSalesOrders.ActiveApproved1 == true)
            {
                string Active = "Level 1";
                body = body.Replace("{Level}", Active);
            }
            else if (_locSalesOrders.ActiveApproved2 == true)
            {
                string Active = "Level 2";
                body = body.Replace("{Level}", Active);
            }
            else if (_locSalesOrders.ActiveApproved3 == true)
            {
                string Active = "Level 3";
                body = body.Replace("{Level}", Active);
            }
            else
            {
                string Active = "Not Available";
                body = body.Replace("{Level}", Active);
            }
            #endregion Level

            body = body.Replace("{Status}", Status);
            body = body.Replace("{Message}", _locMailSetDetail.MessageBody);
            return body;
        }

        protected void SendEmail(Session _currentSession, SalesOrder _locSalesOrderXPO, Status _locStatus, ApprovalLine _locApprovalLineXPO2)
        {
            DateTime now = DateTime.Now;
            var User = SecuritySystem.CurrentUserName;
            GlobalFunction _globFunc = new GlobalFunction();
            UserAccess _numLineUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", User));
            string _locMessageBody = null;

            try
            {
                if (_locSalesOrderXPO != null)
                {
                    XPCollection<MailSetupDetail> _locMailSetDetails = new XPCollection<MailSetupDetail>
                                                                       (_currentSession, new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("ObjectList", ObjectList.SalesOrder),
                                                                        new BinaryOperator("UserAccess", _numLineUserAccess),
                                                                        new BinaryOperator("Active", true)));

                    if (_locMailSetDetails != null && _locMailSetDetails.Count > 0)
                    {
                        foreach (MailSetupDetail _locMailSetDetail in _locMailSetDetails)
                        {
                            if (_locMailSetDetail.MessageBody != null)
                            {
                                if (_locApprovalLineXPO2.EndApproval == true)
                                {
                                    _locMessageBody = this.BackgroundBody(_currentSession, _locSalesOrderXPO, _locMailSetDetail);
                                }
                                else
                                {
                                    _locMessageBody = this.BackgroundBody(_currentSession, _locSalesOrderXPO, _locMailSetDetail);
                                }
                            }

                            _globFunc.SetAndSendMail(_locMailSetDetail.SmtpHost, _locMailSetDetail.SmtpPort,
                            _locMailSetDetail.MailFrom, _locMailSetDetail.MailFromPassword, _locMailSetDetail.MailTo, _locMailSetDetail.MailSubject, _locMessageBody);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrder " + ex.ToString());
            }
        }

        #endregion Email

        #region Global

        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 4000;
            options.Message = string.Format("{0} !", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global

        
    }
}
