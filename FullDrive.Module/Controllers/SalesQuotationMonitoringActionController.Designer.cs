﻿namespace FullDrive.Module.Controllers
{
    partial class SalesQuotationMonitoringActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.SalesQuotationMonitoringSelectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.SalesQuotationMonitoringUnselectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // SalesQuotationMonitoringSelectAction
            // 
            this.SalesQuotationMonitoringSelectAction.Caption = "Select";
            this.SalesQuotationMonitoringSelectAction.ConfirmationMessage = null;
            this.SalesQuotationMonitoringSelectAction.Id = "SalesQuotationMonitoringSelectActionId";
            this.SalesQuotationMonitoringSelectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesQuotationMonitoring);
            this.SalesQuotationMonitoringSelectAction.ToolTip = null;
            this.SalesQuotationMonitoringSelectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SalesQuotationMonitoringSelectAction_Execute);
            // 
            // SalesQuotationMonitoringUnselectAction
            // 
            this.SalesQuotationMonitoringUnselectAction.Caption = "Unselect";
            this.SalesQuotationMonitoringUnselectAction.ConfirmationMessage = null;
            this.SalesQuotationMonitoringUnselectAction.Id = "SalesQuotationMonitoringUnselectActionId";
            this.SalesQuotationMonitoringUnselectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesQuotationMonitoring);
            this.SalesQuotationMonitoringUnselectAction.ToolTip = null;
            this.SalesQuotationMonitoringUnselectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SalesQuotationMonitoringUnselectAction_Execute);
            // 
            // SalesQuotationMonitoringActionController
            // 
            this.Actions.Add(this.SalesQuotationMonitoringSelectAction);
            this.Actions.Add(this.SalesQuotationMonitoringUnselectAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction SalesQuotationMonitoringSelectAction;
        private DevExpress.ExpressApp.Actions.SimpleAction SalesQuotationMonitoringUnselectAction;
    }
}
