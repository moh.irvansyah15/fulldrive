﻿namespace FullDrive.Module.Controllers
{
    partial class InventoryProductionLineActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.InventoryProductionLineSelectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.InventoryProductionLineUnselectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // InventoryProductionLineSelectAction
            // 
            this.InventoryProductionLineSelectAction.Caption = "Select";
            this.InventoryProductionLineSelectAction.ConfirmationMessage = null;
            this.InventoryProductionLineSelectAction.Id = "InventoryProductionLineSelectActionId";
            this.InventoryProductionLineSelectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.InventoryProductionLine);
            this.InventoryProductionLineSelectAction.ToolTip = null;
            this.InventoryProductionLineSelectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.InventoryProductionLineSelectAction_Execute);
            // 
            // InventoryProductionLineUnselectAction
            // 
            this.InventoryProductionLineUnselectAction.Caption = "Unselect";
            this.InventoryProductionLineUnselectAction.ConfirmationMessage = null;
            this.InventoryProductionLineUnselectAction.Id = "InventoryProductionLineUnselecrActionId";
            this.InventoryProductionLineUnselectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.InventoryProductionLine);
            this.InventoryProductionLineUnselectAction.ToolTip = null;
            this.InventoryProductionLineUnselectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.InventoryProductionLineUnselectAction_Execute);
            // 
            // InventoryProductionLineActionController
            // 
            this.Actions.Add(this.InventoryProductionLineSelectAction);
            this.Actions.Add(this.InventoryProductionLineUnselectAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction InventoryProductionLineSelectAction;
        private DevExpress.ExpressApp.Actions.SimpleAction InventoryProductionLineUnselectAction;
    }
}
