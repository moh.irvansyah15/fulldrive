﻿#region Default

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

#endregion Default

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;
using System.Web;
using System.IO;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class CreditMemoActionController : ViewController
    {
        #region Default

        private ChoiceActionItem _selectionListviewFilter;
        private ChoiceActionItem _setApprovalLevel;

        public CreditMemoActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            CreditMemoListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {

                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                CreditMemoListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
        }

        protected override void OnActivated()
        {
            TypeOfView = typeof(ObjectView);
            base.OnActivated();
            // Perform various tasks depending on the target View.
            #region Approval
            var _locUser = SecuritySystem.CurrentUserName;
            GlobalFunction _globalFunction = new GlobalFunction();
            Session _currentSession = null;
            if (this.ObjectSpace != null)
            {
                _currentSession = ((XPObjectSpace)this.ObjectSpace).Session;
            }

            if (_currentSession != null)
            {
                UserAccess _locUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", _locUser));

                if (_locUserAccess != null)
                {
                    CreditMemoApprovalAction.Items.Clear();

                    XPCollection<ApplicationSetupDetail> _locAppSetupDetails = new XPCollection<ApplicationSetupDetail>
                                                                                 (_currentSession, new GroupOperator(GroupOperatorType.And,
                                                                                  new BinaryOperator("UserAccess", _locUserAccess),
                                                                                  new BinaryOperator("ObjectList", CustomProcess.ObjectList.CreditMemo),
                                                                                  new BinaryOperator("FunctionList", CustomProcess.FunctionList.Approval),
                                                                                  new BinaryOperator("Active", true)));

                    if (_locAppSetupDetails != null && _locAppSetupDetails.Count > 0)
                    {
                        foreach (ApplicationSetupDetail _locAppSetupDetail in _locAppSetupDetails)
                        {
                            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.ApprovalLevel)))
                            {
                                if (_locAppSetupDetail.ApprovalLevel != CustomProcess.ApprovalLevel.None)
                                {
                                    EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.ApprovalLevel));
                                    _setApprovalLevel = new ChoiceActionItem(_ed.GetCaption(_locAppSetupDetail.ApprovalLevel), _locAppSetupDetail.ApprovalLevel);
                                }
                            }
                            CreditMemoApprovalAction.Items.Add(_setApprovalLevel);
                        }
                    }
                }
            }
            #endregion Approval
        }

        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }

        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        #endregion Default

        private void CreditMemoProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        CreditMemo _locCreditMemoOS = (CreditMemo)_objectSpace.GetObject(obj);

                        if (_locCreditMemoOS != null)
                        {
                            if (_locCreditMemoOS.Code != null)
                            {
                                _currObjectId = _locCreditMemoOS.Code;

                                CreditMemo _locCreditMemoXPO = _currSession.FindObject<CreditMemo>
                                                             (new GroupOperator(GroupOperatorType.And,
                                                              new BinaryOperator("Code", _currObjectId)));

                                if (_locCreditMemoXPO != null)
                                {
                                    if (_locCreditMemoXPO.Status == Status.Open || _locCreditMemoXPO.Status == Status.Progress)
                                    {
                                        _locCreditMemoXPO.Status = Status.Progress;
                                        _locCreditMemoXPO.StatusDate = now;
                                        _locCreditMemoXPO.Save();
                                        _locCreditMemoXPO.Session.CommitTransaction();

                                        XPCollection<CreditMemoLine> _locCreditMemoLines = new XPCollection<CreditMemoLine>
                                                                                         (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                          new BinaryOperator("CreditMemo", _locCreditMemoXPO),
                                                                                          new BinaryOperator("Status", Status.Open)));

                                        if (_locCreditMemoLines != null && _locCreditMemoLines.Count > 0)
                                        {
                                            foreach (CreditMemoLine _locCreditMemoLine in _locCreditMemoLines)
                                            {
                                                _locCreditMemoLine.Status = Status.Progress;
                                                _locCreditMemoLine.StatusDate = now;
                                                _locCreditMemoLine.Save();
                                                _locCreditMemoLine.Session.CommitTransaction();
                                            }
                                        }

                                        XPCollection<CreditPurchaseCollection> _locCreditPurchaseCollections = new XPCollection<CreditPurchaseCollection>
                                                                                         (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                          new BinaryOperator("CreditMemo", _locCreditMemoXPO),
                                                                                          new BinaryOperator("Status", Status.Open)));
                                        if (_locCreditPurchaseCollections != null && _locCreditPurchaseCollections.Count() > 0)
                                        {
                                            foreach (CreditPurchaseCollection _locCreditPurchaseCollection in _locCreditPurchaseCollections)
                                            {
                                                _locCreditPurchaseCollection.Status = Status.Progress;
                                                _locCreditPurchaseCollection.StatusDate = now;
                                                _locCreditPurchaseCollection.Save();
                                                _locCreditPurchaseCollection.Session.CommitTransaction();
                                            }
                                        }
                                        SuccessMessageShow(_locCreditMemoXPO.Code + " has been change successfully to Progress");
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data Credit Memo Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Credit Memo Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CreditMemo " + ex.ToString());
            }
        }

        private void CreditMemoPostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        CreditMemo _locCreditMemoOS = (CreditMemo)_objectSpace.GetObject(obj);

                        if (_locCreditMemoOS != null)
                        {
                            if (_locCreditMemoOS.Code != null)
                            {
                                _currObjectId = _locCreditMemoOS.Code;

                                CreditMemo _locCreditMemoXPO = _currSession.FindObject<CreditMemo>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locCreditMemoXPO != null)
                                {
                                    if (_locCreditMemoXPO.Status == Status.Progress || _locCreditMemoXPO.Status == Status.Posted)
                                    {
                                        SetReverseGoodsDeliverReturnJournal(_currSession, _locCreditMemoXPO);
                                        SetReturnJournal(_currSession, _locCreditMemoXPO);
                                        SetPurchaseReturnMonitoring(_currSession, _locCreditMemoXPO);
                                        SetRemainQty(_currSession, _locCreditMemoXPO);
                                        SetPostingQty(_currSession, _locCreditMemoXPO);
                                        SetProcessCount(_currSession, _locCreditMemoXPO);
                                        SetStatusPurchaseReturnLine(_currSession, _locCreditMemoXPO);
                                        SetNormalQuantity(_currSession, _locCreditMemoXPO);
                                        SetFinalStatusPurchaseReturn(_currSession, _locCreditMemoXPO);
                                    }

                                    SuccessMessageShow("Credit Memo has been successfully Posted");
                                }
                                else
                                {
                                    ErrorMessageShow("Data Purchase Return Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Purchase Return Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CreditMemo " + ex.ToString());
            }
        }

        private void CreditMemoGetPRMAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        CreditMemo _locCreditMemoOS = (CreditMemo)_objectSpace.GetObject(obj);

                        if (_locCreditMemoOS != null)
                        {
                            if (_locCreditMemoOS.Code != null)
                            {
                                _currObjectId = _locCreditMemoOS.Code;

                                CreditMemo _locCreditMemoXPO = _currSession.FindObject<CreditMemo>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locCreditMemoXPO != null)
                                {
                                    if (_locCreditMemoXPO.Status == Status.Open || _locCreditMemoXPO.Status == Status.Progress)
                                    {
                                        XPCollection<CreditPurchaseCollection> _locCreditPurchaseCollections = new XPCollection<CreditPurchaseCollection>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("CreditMemo", _locCreditMemoXPO),
                                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("Status", Status.Open),
                                                                                        new BinaryOperator("Status", Status.Progress))));

                                        if (_locCreditPurchaseCollections != null && _locCreditPurchaseCollections.Count() > 0)
                                        {
                                            foreach (CreditPurchaseCollection _locCreditPurchaseCollection in _locCreditPurchaseCollections)
                                            {
                                                if (_locCreditPurchaseCollection.CreditMemo != null)
                                                {
                                                    ApprovalLine _locApprovalLineXPO = _currSession.FindObject<ApprovalLine>
                                                                       (new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("EndApproval", true),
                                                                        new BinaryOperator("CreditMemo", _locCreditPurchaseCollection.CreditMemo)));
                                                    if (_locApprovalLineXPO != null)
                                                    {
                                                        GetPurchaseReturnMonitoring(_currSession, _locCreditPurchaseCollection.PurchaseReturn, _locCreditMemoXPO);
                                                    }
                                                }
                                            }
                                            SuccessMessageShow("Purchase Return Monitoring Has Been Successfully Getting into Credit Memo");
                                        }
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data Credit Memo Not Available");
                                }

                            }
                            else
                            {
                                ErrorMessageShow("Data Credit Memo Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CreditMemo " + ex.ToString());
            }
        }

        private void CreditMemoApprovalAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                ApplicationSetupDetail _locAppSetDetail = null;
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                var User = SecuritySystem.CurrentUserName;
                Session _currentSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currentSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                UserAccess _locUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", User));

                foreach (Object obj in _objectsToProcess)
                {
                    CreditMemo _objInNewObjectSpace = (CreditMemo)_objectSpace.GetObject(obj);

                    if (_objInNewObjectSpace != null)
                    {
                        if (_objInNewObjectSpace.Code != null)
                        {
                            _currObjectId = _objInNewObjectSpace.Code;
                        }
                    }

                    if (_currObjectId != null)
                    {
                        CreditMemo _locCreditMemoXPO = _currentSession.FindObject<CreditMemo>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("Code", _currObjectId)));

                        if (_locCreditMemoXPO != null)
                        {
                            if (_locCreditMemoXPO.Status == Status.Progress)
                            {
                                ApprovalLine _locApprovalLine = _currentSession.FindObject<ApprovalLine>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("CreditMemo", _locCreditMemoXPO),
                                                                             new BinaryOperator("EndApproval", true)));
                                if (_locApprovalLine == null)
                                {
                                    #region Approval Level 1
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level1)
                                    {
                                        _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level1, ObjectList.CreditMemo);
                                        if (_locAppSetDetail != null)
                                        {
                                            //Buat bs input langsung ke approvalline
                                            ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("CreditMemo", _locCreditMemoXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level1)));
                                            if (_locApprovalLineXPO == null)
                                            {
                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level1,
                                                        EndApproval = true,
                                                        CreditMemo = _locCreditMemoXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locCreditMemoXPO.ActivationPosting = true;
                                                    _locCreditMemoXPO.ActiveApproved1 = false;
                                                    _locCreditMemoXPO.ActiveApproved2 = false;
                                                    _locCreditMemoXPO.ActiveApproved3 = true;
                                                    _locCreditMemoXPO.Save();
                                                    _locCreditMemoXPO.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level1,
                                                        CreditMemo = _locCreditMemoXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locCreditMemoXPO.ActiveApproved1 = true;
                                                    _locCreditMemoXPO.ActiveApproved2 = false;
                                                    _locCreditMemoXPO.ActiveApproved3 = false;
                                                    _locCreditMemoXPO.Save();
                                                    _locCreditMemoXPO.Session.CommitTransaction();
                                                }

                                                //Send Email
                                                ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("CreditMemo", _locCreditMemoXPO)));

                                                if (_locApprovalLineXPO2 != null)
                                                {
                                                    SendEmail(_currentSession, _locCreditMemoXPO, Status.Progress, _locApprovalLineXPO2);
                                                }

                                                SuccessMessageShow("Credit Memo has successfully Approve");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 1

                                    #region Approval Level 2
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level2)
                                    {
                                        _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level2, ObjectList.CreditMemo);

                                        if (_locAppSetDetail != null)
                                        {
                                            ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("CreditMemo", _locCreditMemoXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level2)));

                                            if (_locApprovalLineXPO == null)
                                            {
                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level2,
                                                        EndApproval = true,
                                                        CreditMemo = _locCreditMemoXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locCreditMemoXPO.ActivationPosting = true;
                                                    _locCreditMemoXPO.ActiveApproved1 = false;
                                                    _locCreditMemoXPO.ActiveApproved2 = false;
                                                    _locCreditMemoXPO.ActiveApproved3 = true;
                                                    _locCreditMemoXPO.Save();
                                                    _locCreditMemoXPO.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level2,
                                                        CreditMemo = _locCreditMemoXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    SetApprovalLine(_currentSession, _locCreditMemoXPO, ApprovalLevel.Level1);

                                                    _locCreditMemoXPO.ActiveApproved1 = false;
                                                    _locCreditMemoXPO.ActiveApproved2 = true;
                                                    _locCreditMemoXPO.ActiveApproved3 = false;
                                                    _locCreditMemoXPO.Save();
                                                    _locCreditMemoXPO.Session.CommitTransaction();
                                                }

                                                //Send Email
                                                ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("CreditMemo", _locCreditMemoXPO)));

                                                if (_locApprovalLineXPO2 != null)
                                                {
                                                    SendEmail(_currentSession, _locCreditMemoXPO, Status.Progress, _locApprovalLineXPO2);
                                                }

                                                SuccessMessageShow("Credit Memo has successfully Approve");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 2

                                    #region Approval Level 3
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level3)
                                    {
                                        _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level3, ObjectList.CreditMemo);

                                        if (_locAppSetDetail != null)
                                        {
                                            ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("CreditMemo", _locCreditMemoXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level3)));

                                            if (_locApprovalLineXPO == null)
                                            {
                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level3,
                                                        EndApproval = true,
                                                        CreditMemo = _locCreditMemoXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locCreditMemoXPO.ActivationPosting = true;
                                                    _locCreditMemoXPO.ActiveApproved1 = false;
                                                    _locCreditMemoXPO.ActiveApproved2 = false;
                                                    _locCreditMemoXPO.ActiveApproved3 = true;
                                                    _locCreditMemoXPO.Save();
                                                    _locCreditMemoXPO.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level3,
                                                        CreditMemo = _locCreditMemoXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    SetApprovalLine(_currentSession, _locCreditMemoXPO, ApprovalLevel.Level2);
                                                    SetApprovalLine(_currentSession, _locCreditMemoXPO, ApprovalLevel.Level1);

                                                    _locCreditMemoXPO.ActiveApproved1 = false;
                                                    _locCreditMemoXPO.ActiveApproved2 = false;
                                                    _locCreditMemoXPO.ActiveApproved3 = true;
                                                    _locCreditMemoXPO.Save();
                                                    _locCreditMemoXPO.Session.CommitTransaction();
                                                }

                                                //Send Email
                                                ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("CreditMemo", _locCreditMemoXPO)));

                                                if (_locApprovalLineXPO2 != null)
                                                {
                                                    SendEmail(_currentSession, _locCreditMemoXPO, Status.Progress, _locApprovalLineXPO2);
                                                }

                                                SuccessMessageShow("Credit Memo has successfully Approve");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 3
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Purchase Return Status Not Available");
                            }
                        }
                        else
                        {
                            ErrorMessageShow("Purchase Return Not Available");
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CreditMemo " + ex.ToString());
            }
        }

        private void CreditMemoListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(CreditMemo)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CreditMemo " + ex.ToString());
            }
        }

        //=========================================== Code Only ===========================================

        #region GetPRM

        private void GetPurchaseReturnMonitoring(Session _currSession, PurchaseReturn _locPurchaseReturnXPO, CreditMemo _locCreditMemoXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _currSignCode = null;
                double _locTxValue = 0;
                double _locTxAmount = 0;
                double _locDisc = 0;
                double _locDiscAmount = 0;
                double _locTAmount = 0;


                if (_locPurchaseReturnXPO != null)
                {
                    XPCollection<PurchaseReturnMonitoring> _locPurchaseReturnMonitorings = new XPCollection<PurchaseReturnMonitoring>
                                                                                 (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                  new BinaryOperator("PurchaseReturn", _locPurchaseReturnXPO),
                                                                                  new BinaryOperator("CreditMemo", _locCreditMemoXPO),
                                                                                  new BinaryOperator("Select", true),
                                                                                  new BinaryOperator("ReturnType", ReturnType.CreditMemo)
                                                                                  ));
                    if (_locPurchaseReturnMonitorings != null && _locPurchaseReturnMonitorings.Count() > 0)
                    {
                        foreach (PurchaseReturnMonitoring _locPurchaseReturnMonitoring in _locPurchaseReturnMonitorings)
                        {
                            if (_locPurchaseReturnMonitoring.PurchaseReturnLine != null && _locPurchaseReturnMonitoring.PurchaseOrderMonitoring != null)
                            {
                                _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.CreditMemoLine);

                                if (_currSignCode != null)
                                {
                                    #region CountTax
                                    if (_locPurchaseReturnMonitoring.PurchaseOrderMonitoring.MultiTax == true)
                                    {
                                        _locTxValue = GetSumTotalTaxValue(_currSession, _locPurchaseReturnMonitoring.PurchaseReturnLine);
                                        _locTxAmount = GetSumTotalTaxAmount(_currSession, _locPurchaseReturnMonitoring.PurchaseReturnLine, _locPurchaseReturnMonitoring);
                                    }
                                    else
                                    {
                                        if (_locPurchaseReturnMonitoring.TxValue > 0)
                                        {
                                            _locTxValue = _locPurchaseReturnMonitoring.TxValue;
                                            _locTxAmount = (_locTxValue / 100) * (_locPurchaseReturnMonitoring.TQty * _locPurchaseReturnMonitoring.UAmount);
                                        }
                                    }
                                    #endregion CountTax

                                    #region CountDiscount
                                    if (_locPurchaseReturnMonitoring.MultiDiscount == true)
                                    {
                                        _locDisc = GetSumTotalDisc(_currSession, _locPurchaseReturnMonitoring.PurchaseReturnLine);
                                        _locDiscAmount = GetSumTotalDiscAmount(_currSession, _locPurchaseReturnMonitoring.PurchaseReturnLine, _locPurchaseReturnMonitoring);
                                    }
                                    else
                                    {
                                        if (_locPurchaseReturnMonitoring.PurchaseOrderMonitoring.Disc > 0)
                                        {
                                            _locDisc = _locPurchaseReturnMonitoring.Disc;
                                            _locDiscAmount = (_locDisc / 100) * (_locPurchaseReturnMonitoring.TQty * _locPurchaseReturnMonitoring.UAmount);
                                        }
                                    }
                                    #endregion CountDiscount

                                    _locTAmount = (_locPurchaseReturnMonitoring.PurchaseOrderMonitoring.UAmount * _locPurchaseReturnMonitoring.TQty) + _locTxAmount - _locDiscAmount;
                                    //Tempat untuk menghitung multitax dan multidiscount
                                    CreditMemoLine _saveDataCreditMemoLine = new CreditMemoLine(_currSession)
                                    {
                                        SignCode = _currSignCode,
                                        OrderType = _locPurchaseReturnMonitoring.PurchaseReturnLine.OrderType,
                                        Item = _locPurchaseReturnMonitoring.Item,
                                        Description = _locPurchaseReturnMonitoring.PurchaseReturnLine.Description,
                                        MxDQty = _locPurchaseReturnMonitoring.DQty,
                                        MxDUOM = _locPurchaseReturnMonitoring.DUOM,
                                        MxQty = _locPurchaseReturnMonitoring.Qty,
                                        MxUOM = _locPurchaseReturnMonitoring.UOM,
                                        MxTQty = _locPurchaseReturnMonitoring.TQty,
                                        MxUAmount = _locPurchaseReturnMonitoring.UAmount,
                                        DQty = _locPurchaseReturnMonitoring.DQty,
                                        DUOM = _locPurchaseReturnMonitoring.DUOM,
                                        Qty = _locPurchaseReturnMonitoring.Qty,
                                        UOM = _locPurchaseReturnMonitoring.UOM,
                                        TQty = _locPurchaseReturnMonitoring.TQty,
                                        Currency = _locPurchaseReturnMonitoring.Currency,
                                        UAmount = _locPurchaseReturnMonitoring.UAmount,
                                        TUAmount = _locPurchaseReturnMonitoring.TQty * _locPurchaseReturnMonitoring.UAmount,
                                        MultiTax = _locPurchaseReturnMonitoring.MultiTax,
                                        Tax = _locPurchaseReturnMonitoring.Tax,
                                        TxValue = _locTxValue,
                                        TxAmount = _locTxAmount,
                                        MultiDiscount = _locPurchaseReturnMonitoring.MultiDiscount,
                                        Discount = _locPurchaseReturnMonitoring.Discount,
                                        Disc = _locDisc,
                                        DiscAmount = _locDiscAmount,
                                        TAmount = _locTAmount,
                                        PurchaseReturnMonitoring = _locPurchaseReturnMonitoring,
                                        ReturnType = _locPurchaseReturnMonitoring.ReturnType,
                                    };
                                    _saveDataCreditMemoLine.Save();
                                    _saveDataCreditMemoLine.Session.CommitTransaction();

                                    CreditMemoLine _locCreditMemoLine = _currSession.FindObject<CreditMemoLine>
                                                                (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("SignCode", _currSignCode)));
                                    if (_locCreditMemoLine != null)
                                    {
                                        #region TaxLine
                                        if (_locCreditMemoLine.MultiTax == true && _locPurchaseReturnMonitoring.PurchaseReturnLine != null)
                                        {
                                            XPCollection<TaxLine> _locTaxLines = new XPCollection<TaxLine>(_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("PurchaseReturnLine", _locPurchaseReturnMonitoring.PurchaseReturnLine)));
                                            if (_locTaxLines != null && _locTaxLines.Count() > 0)
                                            {
                                                foreach (TaxLine _locTaxLine in _locTaxLines)
                                                {
                                                    TaxLine _saveDataTaxLine = new TaxLine(_currSession)
                                                    {
                                                        Tax = _locTaxLine.Tax,
                                                        Name = _locTaxLine.Name,
                                                        TaxType = _locTaxLine.TaxType,
                                                        TaxGroup = _locTaxLine.TaxGroup,
                                                        TaxGroupType = _locTaxLine.TaxGroupType,
                                                        TaxAccountGroup = _locTaxLine.TaxAccountGroup,
                                                        TaxNature = _locTaxLine.TaxNature,
                                                        TaxMode = _locTaxLine.TaxMode,
                                                        TxValue = _locTaxLine.TxValue,
                                                        Description = _locTaxLine.Description,
                                                        UAmount = _locCreditMemoLine.UAmount,
                                                        TUAmount = _locCreditMemoLine.TUAmount,
                                                        TxAmount = _locCreditMemoLine.TxAmount,
                                                        CreditMemoLine = _locCreditMemoLine,
                                                    };
                                                    _saveDataTaxLine.Save();
                                                    _saveDataTaxLine.Session.CommitTransaction();
                                                }
                                            }
                                        }
                                        #endregion TaxLine

                                        #region DiscountLine
                                        if (_locCreditMemoLine.MultiDiscount == true)
                                        {
                                            XPCollection<DiscountLine> _locDiscountLines = new XPCollection<DiscountLine>(_currSession,
                                                                                new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("PurchaseReturnLine", _locPurchaseReturnMonitoring.PurchaseReturnLine)));
                                            if (_locDiscountLines != null && _locDiscountLines.Count() > 0)
                                            {
                                                foreach (DiscountLine _locDiscountLine in _locDiscountLines)
                                                {
                                                    DiscountLine _saveDataDiscountLine = new DiscountLine(_currSession)
                                                    {
                                                        Discount = _locDiscountLine.Discount,
                                                        Name = _locDiscountLine.Name,
                                                        DiscountMode = _locDiscountLine.DiscountMode,
                                                        Description = _locDiscountLine.Description,
                                                        UAmount = _locCreditMemoLine.UAmount,
                                                        Disc = _locDiscountLine.Disc,
                                                        DiscAmount = _locCreditMemoLine.DiscAmount,
                                                        TUAmount = _locCreditMemoLine.TUAmount,
                                                        CreditMemoLine = _locCreditMemoLine,
                                                    };
                                                    _saveDataDiscountLine.Save();
                                                    _saveDataDiscountLine.Session.CommitTransaction();
                                                }
                                            }
                                        }
                                        #endregion DiscountLine

                                        _locPurchaseReturnMonitoring.CreditMemoLine = _locCreditMemoLine;
                                        _locPurchaseReturnMonitoring.MemoStatus = Status.Close;
                                        _locPurchaseReturnMonitoring.MemoStatusDate = now;
                                        _locPurchaseReturnMonitoring.Save();
                                        _locPurchaseReturnMonitoring.Session.CommitTransaction();
                                    }
                                }
                            }
                        }
                    }

                    //================================================================================================================
                    //Harus mampu menghitung otomatis buat MultiTax dan MultiDiscount

                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = CreditMemo ", ex.ToString());
            }
        }

        private double GetSumTotalTaxValue(Session _currSession, PurchaseReturnLine _purchaseReturnLineXPO)
        {
            double _result = 0;
            try
            {
                double _locTxValue = 0;

                if (_purchaseReturnLineXPO != null)
                {
                    XPCollection<TaxLine> _locTaxLines = new XPCollection<TaxLine>(_currSession,
                                                     new BinaryOperator("PurchaseReturnLine", _purchaseReturnLineXPO));

                    if (_locTaxLines.Count() > 0)
                    {
                        foreach (TaxLine _locTaxLine in _locTaxLines)
                        {
                            if (_locTaxLine.TxValue > 0 && _locTaxLine.TxAmount > 0)
                            {
                                if (_locTaxLine.TaxNature == TaxNature.Increase)
                                {
                                    _locTxValue = _locTxValue + _locTaxLine.TxValue;
                                }
                                else if (_locTaxLine.TaxNature == TaxNature.Decrease)
                                {
                                    _locTxValue = _locTxValue - _locTaxLine.TxValue;
                                }
                            }
                        }
                        _result = _locTxValue;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = CreditMemo " + ex.ToString());
            }
            return _result;
        }

        private double GetSumTotalTaxAmount(Session _currSession, PurchaseReturnLine _purchaseReturnLineXPO, PurchaseReturnMonitoring _purchaseReturnMonitoringXPO)
        {
            double _return = 0;
            try
            {
                double _locTxAmount = 0;

                if (_purchaseReturnLineXPO != null)
                {
                    XPCollection<TaxLine> _locTaxLines = new XPCollection<TaxLine>(_currSession,
                                                     new BinaryOperator("PurchaseReturnLine", _purchaseReturnLineXPO));

                    if (_locTaxLines.Count() > 0)
                    {
                        foreach (TaxLine _locTaxLine in _locTaxLines)
                        {
                            if (_locTaxLine.TxValue > 0)
                            {
                                if (_locTaxLine.TaxNature == TaxNature.Increase)
                                {
                                    _locTxAmount = _locTxAmount + (_purchaseReturnLineXPO.UAmount * _purchaseReturnMonitoringXPO.TQty * _locTaxLine.TxValue / 100);
                                }
                                else if (_locTaxLine.TaxNature == TaxNature.Decrease)
                                {
                                    _locTxAmount = _locTxAmount - (_purchaseReturnLineXPO.UAmount * _purchaseReturnMonitoringXPO.TQty * _locTaxLine.TxValue / 100);
                                }
                            }
                        }
                        _return = _locTxAmount;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = CreditMemo " + ex.ToString());
            }
            return _return;
        }

        private double GetSumTotalDisc(Session _currSession, PurchaseReturnLine _purchaseReturnLineXPO)
        {
            double _result = 0;
            try
            {
                double _locDisc = 0;

                if (_purchaseReturnLineXPO != null)
                {
                    XPCollection<DiscountLine> _locDiscountLines = new XPCollection<DiscountLine>(_currSession,
                                                               new BinaryOperator("PurchaseReturnLine", _purchaseReturnLineXPO));

                    if (_locDiscountLines.Count() > 0)
                    {
                        foreach (DiscountLine _locDiscountLine in _locDiscountLines)
                        {
                            if (_locDiscountLine.Disc > 0 && _locDiscountLine.DiscAmount > 0)
                            {
                                if (_locDiscountLine.Disc > 0 && _locDiscountLine.DiscAmount > 0)
                                {
                                    _locDisc = _locDisc + _locDiscountLine.Disc;
                                }
                            }
                        }
                        _result = _locDisc;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = CreditMemo " + ex.ToString());
            }
            return _result;
        }

        private double GetSumTotalDiscAmount(Session _currSession, PurchaseReturnLine _purchaseReturnLineXPO, PurchaseReturnMonitoring _purchaseReturnMonitoringXPO)
        {
            double _result = 0;
            try
            {
                double _locDiscAmount = 0;

                if (_purchaseReturnLineXPO != null)
                {
                    XPCollection<DiscountLine> _locDiscountLines = new XPCollection<DiscountLine>(_currSession,
                                                               new BinaryOperator("PurchaseReturnLine", _purchaseReturnLineXPO));

                    if (_locDiscountLines.Count() > 0)
                    {
                        foreach (DiscountLine _locDiscountLine in _locDiscountLines)
                        {
                            if (_locDiscountLine.Disc > 0)
                            {
                                _locDiscAmount = _locDiscAmount + (_purchaseReturnLineXPO.UAmount * _purchaseReturnMonitoringXPO.TQty * _locDiscountLine.Disc / 100);
                            }
                        }
                        _result = _locDiscAmount;
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = CreditMemo " + ex.ToString());
            }
            return _result;
        }

        #endregion GetPRM

        #region Posting

        private void SetReverseGoodsDeliverReturnJournal(Session _currSession, CreditMemo _locCreditMemoInXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                double _locTotalUnitAmount = 0;

                if (_locCreditMemoInXPO != null)
                {
                    XPCollection<CreditMemoLine> _locCreditMemoLines = new XPCollection<CreditMemoLine>
                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("CreditMemoLine", _locCreditMemoInXPO),
                                                                     new BinaryOperator("Select", true),
                                                                     new BinaryOperator("ReturnType", ReturnType.CreditMemo),
                                                                     new GroupOperator(GroupOperatorType.Or,
                                                                     new BinaryOperator("Status", Status.Progress),
                                                                     new BinaryOperator("Status", Status.Posted)
                                                                     )));
                    if (_locCreditMemoLines != null && _locCreditMemoLines.Count() > 0)
                    {
                        foreach (CreditMemoLine _locCreditMemoLine in _locCreditMemoLines)
                        {
                            XPQuery<PurchaseReturnMonitoring> _purchaseReturnMonitoringsQuery = new XPQuery<PurchaseReturnMonitoring>(_currSession);

                            var _purchaseReturnMonitorings = from prm in _purchaseReturnMonitoringsQuery
                                                          where (prm.CreditMemo == _locCreditMemoInXPO
                                                          && prm.CreditMemoLine == _locCreditMemoLine
                                                          )
                                                          group prm by prm.PurchaseReturn into g
                                                          select new { PurchaseReturn = g.Key };

                            if (_purchaseReturnMonitorings != null && _purchaseReturnMonitorings.Count() > 0)
                            {
                                foreach (var _purchaseReturnMonitoring in _purchaseReturnMonitorings)
                                {
                                    if (_purchaseReturnMonitoring.PurchaseReturn != null)
                                    {
                                        XPCollection<PurchaseReturnMonitoring> _locPurchaseReturnMonitorings = new XPCollection<PurchaseReturnMonitoring>
                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                           new BinaryOperator("CreditMemo", _locCreditMemoInXPO),
                                                                                           new BinaryOperator("CreditMemoLine", _locCreditMemoLine),
                                                                                           new BinaryOperator("PurchaseReturn", _purchaseReturnMonitoring.PurchaseReturn)));

                                        if (_locPurchaseReturnMonitorings != null && _locPurchaseReturnMonitorings.Count() > 0)
                                        {
                                            foreach (PurchaseReturnMonitoring _locPurchaseReturnMonitoring in _locPurchaseReturnMonitorings)
                                            {
                                                if (_locPurchaseReturnMonitoring.PurchaseReturnLine != null)
                                                {
                                                    //Create purchaseorderline based Item   
                                                    _locTotalUnitAmount = _locPurchaseReturnMonitoring.TUAmount;

                                                }

                                                #region CreateGoodsDeliverJournal

                                                #region JournalMapByItems
                                                if (_locCreditMemoLine.Item != null)
                                                {
                                                    if (_locCreditMemoLine.Item.ItemAccountGroup != null)
                                                    {
                                                        XPCollection<JournalMap> _locJournalMapItems = new XPCollection<JournalMap>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("ItemAccountGroup", _locCreditMemoLine.Item.ItemAccountGroup)));

                                                        if (_locJournalMapItems != null && _locJournalMapItems.Count() > 0)
                                                        {
                                                            foreach (JournalMap _locJournalMapItem in _locJournalMapItems)
                                                            {
                                                                XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                    new BinaryOperator("JournalMap", _locJournalMapItem)));

                                                                if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                                {
                                                                    foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                                    {
                                                                        AccountMap _locAccountMap = _currSession.FindObject<AccountMap>
                                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                                     new BinaryOperator("Code", _locJournalMapLine.AccountMap.Code),
                                                                                                     new BinaryOperator("PostingType", PostingType.Purchase),
                                                                                                     new BinaryOperator("OrderType", _locPurchaseReturnMonitoring.OrderType),
                                                                                                     new BinaryOperator("PostingMethod", PostingMethod.Deliver),
                                                                                                     new BinaryOperator("PostingMethodType", PostingMethodType.Return)));

                                                                        if (_locAccountMap != null)
                                                                        {
                                                                            XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                                new BinaryOperator("AccountMap", _locAccountMap),
                                                                                                                                new BinaryOperator("Active", true)));

                                                                            if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                            {
                                                                                double _locTotalAmountDebit = 0;
                                                                                double _locTotalAmountCredit = 0;
                                                                                double _locTotalBalance = 0;

                                                                                foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                                {
                                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                                    {
                                                                                        _locTotalAmountCredit = _locTotalUnitAmount;
                                                                                    }
                                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                                    {
                                                                                        _locTotalAmountDebit = _locTotalUnitAmount;
                                                                                    }

                                                                                    GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                                    {
                                                                                        PostingDate = now,
                                                                                        PostingType = PostingType.Sales,
                                                                                        OrderType = _locPurchaseReturnMonitoring.OrderType,
                                                                                        PostingMethod = PostingMethod.Receipt,
                                                                                        PostingMethodType = PostingMethodType.Return,
                                                                                        Account = _locAccountMapLine.Account,
                                                                                        Debit = _locTotalAmountDebit,
                                                                                        Credit = _locTotalAmountCredit,
                                                                                        PurchaseReturn = _locPurchaseReturnMonitoring.PurchaseReturn,
                                                                                        PurchaseReturnLine = _locPurchaseReturnMonitoring.PurchaseReturnLine,
                                                                                        CreditMemo = _locCreditMemoInXPO,
                                                                                        CreditMemoLine = _locCreditMemoLine,
                                                                                        Company = _locCreditMemoInXPO.Company,
                                                                                    };

                                                                                    _saveGeneralJournal.Save();
                                                                                    _saveGeneralJournal.Session.CommitTransaction();

                                                                                    #region COA
                                                                                    if (_locAccountMapLine.Account.Code != null)
                                                                                    {
                                                                                        ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                                 new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                                                                                        if (_locCOA != null)
                                                                                        {
                                                                                            if (_locCOA.BalanceType == BalanceType.Change)
                                                                                            {
                                                                                                if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                                                {
                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                                                    }
                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                                                    }
                                                                                                }
                                                                                                if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                                                {
                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                                                    }
                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                                                    }
                                                                                                }
                                                                                            }

                                                                                            _locCOA.Balance = _locTotalBalance;
                                                                                            _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                                            _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                                            _locCOA.Save();
                                                                                            _locCOA.Session.CommitTransaction();
                                                                                        }
                                                                                    }
                                                                                    #endregion COA
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }

                                                #endregion JournalMapByItems

                                                #region JournalMapByBusinessPartner
                                                if (_locCreditMemoInXPO.BuyFromVendor != null)
                                                {
                                                    if (_locCreditMemoInXPO.BuyFromVendor.BusinessPartnerAccountGroup != null)
                                                    {
                                                        XPCollection<JournalMap> _locJournalMapBusinessPartners = new XPCollection<JournalMap>
                                                                                                                  (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                   new BinaryOperator("BusinessPartnerAccountGroup", _locCreditMemoInXPO.BuyFromVendor.BusinessPartnerAccountGroup)));

                                                        if (_locJournalMapBusinessPartners != null && _locJournalMapBusinessPartners.Count() > 0)
                                                        {
                                                            foreach (JournalMap _locJournalMapBusinessPartner in _locJournalMapBusinessPartners)
                                                            {
                                                                XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                    new BinaryOperator("JournalMap", _locJournalMapBusinessPartner)));

                                                                if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                                {
                                                                    foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                                    {
                                                                        AccountMap _locAccountMap = _currSession.FindObject<AccountMap>
                                                                                                     (new GroupOperator(GroupOperatorType.And,
                                                                                                      new BinaryOperator("Code", _locJournalMapLine.AccountMap.Code),
                                                                                                      new BinaryOperator("PostingType", PostingType.Purchase),
                                                                                                      new BinaryOperator("OrderType", _locPurchaseReturnMonitoring.OrderType),
                                                                                                      new BinaryOperator("PostingMethod", PostingMethod.Deliver),
                                                                                                      new BinaryOperator("PostingMethodType", PostingMethodType.Return)));

                                                                        if (_locAccountMap != null)
                                                                        {
                                                                            XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                             new BinaryOperator("AccountMap", _locAccountMap),
                                                                                                             new BinaryOperator("Active", true)));

                                                                            if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                            {
                                                                                double _locTotalAmountDebit = 0;
                                                                                double _locTotalAmountCredit = 0;
                                                                                double _locTotalBalance = 0;

                                                                                foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                                {
                                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                                    {
                                                                                        _locTotalAmountCredit = _locTotalUnitAmount;
                                                                                    }
                                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                                    {
                                                                                        _locTotalAmountDebit = _locTotalUnitAmount;
                                                                                    }

                                                                                    GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                                    {
                                                                                        PostingDate = now,
                                                                                        PostingType = PostingType.Purchase,
                                                                                        OrderType = _locPurchaseReturnMonitoring.OrderType,
                                                                                        PostingMethod = PostingMethod.Deliver,
                                                                                        PostingMethodType = PostingMethodType.Return,
                                                                                        Account = _locAccountMapLine.Account,
                                                                                        Debit = _locTotalAmountDebit,
                                                                                        Credit = _locTotalAmountCredit,
                                                                                        PurchaseReturn = _locPurchaseReturnMonitoring.PurchaseReturn,
                                                                                        PurchaseReturnLine = _locPurchaseReturnMonitoring.PurchaseReturnLine,
                                                                                        CreditMemo = _locCreditMemoInXPO,
                                                                                        CreditMemoLine = _locCreditMemoLine,
                                                                                        Company = _locCreditMemoInXPO.Company,
                                                                                    };

                                                                                    _saveGeneralJournal.Save();
                                                                                    _saveGeneralJournal.Session.CommitTransaction();

                                                                                    #region COA
                                                                                    if (_locAccountMapLine.Account.Code != null)
                                                                                    {
                                                                                        ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                                 new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                                                                                        if (_locCOA != null)
                                                                                        {
                                                                                            if (_locCOA.BalanceType == BalanceType.Change)
                                                                                            {
                                                                                                if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                                                {
                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                                                    }
                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                                                    }
                                                                                                }
                                                                                                if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                                                {
                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                                                    }
                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                                                    }
                                                                                                }
                                                                                            }

                                                                                            _locCOA.Balance = _locTotalBalance;
                                                                                            _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                                            _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                                            _locCOA.Save();
                                                                                            _locCOA.Session.CommitTransaction();
                                                                                        }
                                                                                    }
                                                                                    #endregion COA
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                #endregion JournalMapByBusinessPartner

                                                #endregion CreateGoodsDeliverJournal
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = CreditMemo ", ex.ToString());
            }
        }

        private void SetReturnJournal(Session _currSession, CreditMemo _locCreditMemoInXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                double _locTotalUnitAmount = 0;

                if (_locCreditMemoInXPO != null)
                {
                    XPCollection<CreditMemoLine> _locCreditMemoLines = new XPCollection<CreditMemoLine>
                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("CreditMemo", _locCreditMemoInXPO),
                                                                     new BinaryOperator("Select", true),
                                                                     new BinaryOperator("ReturnType", ReturnType.CreditMemo),
                                                                     new GroupOperator(GroupOperatorType.Or,
                                                                     new BinaryOperator("Status", Status.Progress),
                                                                     new BinaryOperator("Status", Status.Posted)
                                                                     )));
                    if (_locCreditMemoLines != null && _locCreditMemoLines.Count() > 0)
                    {
                        foreach (CreditMemoLine _locCreditMemoLine in _locCreditMemoLines)
                        {
                            XPQuery<PurchaseReturnMonitoring> _purchaseReturnMonitoringsQuery = new XPQuery<PurchaseReturnMonitoring>(_currSession);

                            var _purchaseReturnMonitorings = from prm in _purchaseReturnMonitoringsQuery
                                                          where (prm.CreditMemo == _locCreditMemoInXPO
                                                          && prm.CreditMemoLine == _locCreditMemoLine
                                                          )
                                                          group prm by prm.PurchaseReturn into g
                                                          select new { PurchaseReturn = g.Key };

                            if (_purchaseReturnMonitorings != null && _purchaseReturnMonitorings.Count() > 0)
                            {
                                foreach (var _purchaseReturnMonitoring in _purchaseReturnMonitorings)
                                {
                                    if (_purchaseReturnMonitoring.PurchaseReturn != null)
                                    {
                                        XPCollection<PurchaseReturnMonitoring> _locPurchaseReturnMonitorings = new XPCollection<PurchaseReturnMonitoring>
                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                           new BinaryOperator("CreditMemo", _locCreditMemoInXPO),
                                                                                           new BinaryOperator("CreditMemoLine", _locCreditMemoLine),
                                                                                           new BinaryOperator("PurchaseReturn", _purchaseReturnMonitoring.PurchaseReturn)));

                                        if (_locPurchaseReturnMonitorings != null && _locPurchaseReturnMonitorings.Count() > 0)
                                        {
                                            foreach (PurchaseReturnMonitoring _locPurchaseReturnMonitoring in _locPurchaseReturnMonitorings)
                                            {
                                                if (_locPurchaseReturnMonitoring.PurchaseReturnLine != null)
                                                {
                                                    //Create purchaseorderline based Item   
                                                    _locTotalUnitAmount = _locPurchaseReturnMonitoring.TUAmount;

                                                }

                                                #region CreateGoodsDeliverJournal

                                                #region JournalMapByItems
                                                if (_locCreditMemoLine.Item != null)
                                                {
                                                    if (_locCreditMemoLine.Item.ItemAccountGroup != null)
                                                    {
                                                        XPCollection<JournalMap> _locJournalMapItems = new XPCollection<JournalMap>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("ItemAccountGroup", _locCreditMemoLine.Item.ItemAccountGroup)));

                                                        if (_locJournalMapItems != null && _locJournalMapItems.Count() > 0)
                                                        {
                                                            foreach (JournalMap _locJournalMapItem in _locJournalMapItems)
                                                            {
                                                                XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                    new BinaryOperator("JournalMap", _locJournalMapItem)));

                                                                if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                                {
                                                                    foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                                    {
                                                                        AccountMap _locAccountMap = _currSession.FindObject<AccountMap>
                                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                                     new BinaryOperator("Code", _locJournalMapLine.AccountMap.Code),
                                                                                                     new BinaryOperator("PostingType", PostingType.Purchase),
                                                                                                     new BinaryOperator("OrderType", _locPurchaseReturnMonitoring.OrderType),
                                                                                                     new BinaryOperator("PostingMethod", PostingMethod.Return),
                                                                                                     new BinaryOperator("PostingMethodType", PostingMethodType.Normal)));

                                                                        if (_locAccountMap != null)
                                                                        {
                                                                            XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                                new BinaryOperator("AccountMap", _locAccountMap),
                                                                                                                                new BinaryOperator("Active", true)));

                                                                            if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                            {
                                                                                double _locTotalAmountDebit = 0;
                                                                                double _locTotalAmountCredit = 0;
                                                                                double _locTotalBalance = 0;

                                                                                foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                                {
                                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                                    {
                                                                                        _locTotalAmountDebit = _locTotalUnitAmount;
                                                                                    }
                                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                                    {
                                                                                        _locTotalAmountCredit = _locTotalUnitAmount;
                                                                                    }

                                                                                    GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                                    {
                                                                                        PostingDate = now,
                                                                                        PostingType = PostingType.Purchase,
                                                                                        OrderType = _locPurchaseReturnMonitoring.OrderType,
                                                                                        PostingMethod = PostingMethod.Return,
                                                                                        PostingMethodType = PostingMethodType.Normal,
                                                                                        Account = _locAccountMapLine.Account,
                                                                                        Debit = _locTotalAmountDebit,
                                                                                        Credit = _locTotalAmountCredit,
                                                                                        PurchaseReturn = _locPurchaseReturnMonitoring.PurchaseReturn,
                                                                                        PurchaseReturnLine = _locPurchaseReturnMonitoring.PurchaseReturnLine,
                                                                                        CreditMemo = _locCreditMemoInXPO,
                                                                                        CreditMemoLine = _locCreditMemoLine,
                                                                                        Company = _locCreditMemoInXPO.Company,
                                                                                    };

                                                                                    _saveGeneralJournal.Save();
                                                                                    _saveGeneralJournal.Session.CommitTransaction();

                                                                                    #region COA
                                                                                    if (_locAccountMapLine.Account.Code != null)
                                                                                    {
                                                                                        ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                                 new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                                                                                        if (_locCOA != null)
                                                                                        {
                                                                                            if (_locCOA.BalanceType == BalanceType.Change)
                                                                                            {
                                                                                                if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                                                {
                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                                                    }
                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                                                    }
                                                                                                }
                                                                                                if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                                                {
                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                                                    }
                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                                                    }
                                                                                                }
                                                                                            }

                                                                                            _locCOA.Balance = _locTotalBalance;
                                                                                            _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                                            _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                                            _locCOA.Save();
                                                                                            _locCOA.Session.CommitTransaction();
                                                                                        }
                                                                                    }
                                                                                    #endregion COA
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }

                                                #endregion JournalMapByItems

                                                #region JournalMapByBusinessPartner
                                                if (_locCreditMemoInXPO.BuyFromVendor != null)
                                                {
                                                    if (_locCreditMemoInXPO.BuyFromVendor.BusinessPartnerAccountGroup != null)
                                                    {
                                                        XPCollection<JournalMap> _locJournalMapBusinessPartners = new XPCollection<JournalMap>
                                                                                                                  (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                   new BinaryOperator("BusinessPartnerAccountGroup", _locCreditMemoInXPO.BuyFromVendor.BusinessPartnerAccountGroup)));

                                                        if (_locJournalMapBusinessPartners != null && _locJournalMapBusinessPartners.Count() > 0)
                                                        {
                                                            foreach (JournalMap _locJournalMapBusinessPartner in _locJournalMapBusinessPartners)
                                                            {
                                                                XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                    new BinaryOperator("JournalMap", _locJournalMapBusinessPartner)));

                                                                if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                                {
                                                                    foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                                    {
                                                                        AccountMap _locAccountMap = _currSession.FindObject<AccountMap>
                                                                                                     (new GroupOperator(GroupOperatorType.And,
                                                                                                      new BinaryOperator("Code", _locJournalMapLine.AccountMap.Code),
                                                                                                      new BinaryOperator("PostingType", PostingType.Purchase),
                                                                                                      new BinaryOperator("OrderType", _locPurchaseReturnMonitoring.OrderType),
                                                                                                      new BinaryOperator("PostingMethod", PostingMethod.Return),
                                                                                                      new BinaryOperator("PostingMethodType", PostingMethodType.Normal)));

                                                                        if (_locAccountMap != null)
                                                                        {
                                                                            XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                             new BinaryOperator("AccountMap", _locAccountMap),
                                                                                                             new BinaryOperator("Active", true)));

                                                                            if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                            {
                                                                                double _locTotalAmountDebit = 0;
                                                                                double _locTotalAmountCredit = 0;
                                                                                double _locTotalBalance = 0;

                                                                                foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                                {
                                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                                    {
                                                                                        _locTotalAmountDebit = _locTotalUnitAmount;
                                                                                    }
                                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                                    {
                                                                                        _locTotalAmountCredit = _locTotalUnitAmount;
                                                                                    }

                                                                                    GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                                    {
                                                                                        PostingDate = now,
                                                                                        PostingType = PostingType.Purchase,
                                                                                        OrderType = _locPurchaseReturnMonitoring.OrderType,
                                                                                        PostingMethod = PostingMethod.Return,
                                                                                        PostingMethodType = PostingMethodType.Normal,
                                                                                        Account = _locAccountMapLine.Account,
                                                                                        Debit = _locTotalAmountDebit,
                                                                                        Credit = _locTotalAmountCredit,
                                                                                        PurchaseReturn = _locPurchaseReturnMonitoring.PurchaseReturn,
                                                                                        PurchaseReturnLine = _locPurchaseReturnMonitoring.PurchaseReturnLine,
                                                                                        CreditMemo = _locCreditMemoInXPO,
                                                                                        CreditMemoLine = _locCreditMemoLine,
                                                                                        Company = _locCreditMemoInXPO.Company,
                                                                                    };

                                                                                    _saveGeneralJournal.Save();
                                                                                    _saveGeneralJournal.Session.CommitTransaction();

                                                                                    #region COA
                                                                                    if (_locAccountMapLine.Account.Code != null)
                                                                                    {
                                                                                        ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                                 new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                                                                                        if (_locCOA != null)
                                                                                        {
                                                                                            if (_locCOA.BalanceType == BalanceType.Change)
                                                                                            {
                                                                                                if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                                                {
                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                                                    }
                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                                                    }
                                                                                                }
                                                                                                if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                                                {
                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                                                    }
                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                                                    }
                                                                                                }
                                                                                            }

                                                                                            _locCOA.Balance = _locTotalBalance;
                                                                                            _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                                            _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                                            _locCOA.Save();
                                                                                            _locCOA.Session.CommitTransaction();
                                                                                        }
                                                                                    }
                                                                                    #endregion COA
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                #endregion JournalMapByBusinessPartner

                                                #endregion CreateGoodsDeliverJournal
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = CreditMemo ", ex.ToString());
            }
        }

        private void SetInvoiceAPReturnJournal(Session _currSession, CreditMemo _locCreditMemoXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                double _locTotalUnitPrice = 0;

                #region CreateInvoiceAPReturnJournal

                if (_locCreditMemoXPO != null)
                {
                    XPQuery<CreditMemoLine> _creditMemoLinesQuery = new XPQuery<CreditMemoLine>(_currSession);

                    var _creditMemoLines = from cml in _creditMemoLinesQuery
                                          where (cml.CreditMemo == _locCreditMemoXPO && (cml.Status == Status.Progress || cml.Status == Status.Posted))
                                          group cml by cml.Item.ItemAccountGroup into g
                                          select new { ItemAccountGroup = g.Key };

                    if (_creditMemoLines != null && _creditMemoLines.Count() > 0)
                    {
                        foreach (var _creditMemoLine in _creditMemoLines)
                        {
                            XPCollection<CreditMemoLine> _locCreditMemoLines = new XPCollection<CreditMemoLine>(_currSession,
                                                                         new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("CreditMemo", _locCreditMemoXPO),
                                                                         new BinaryOperator("Item.ItemAccountGroup", _creditMemoLine.ItemAccountGroup)));

                            if (_locCreditMemoLines != null && _locCreditMemoLines.Count() > 0)
                            {
                                #region MenghitungTotalUnitPriceBasedOnItemAccountGroup
                                foreach (CreditMemoLine _locCreditMemoLine in _locCreditMemoLines)
                                {
                                    if (_locCreditMemoLine.Status == Status.Progress || _locCreditMemoLine.Status == Status.Posted)
                                    {
                                        _locTotalUnitPrice = _locTotalUnitPrice + _locCreditMemoLine.TUAmount;
                                    }
                                }
                                #endregion MenghitungTotalUnitPriceBasedOnItemAccountGroup

                                #region CreateInvoiceARReturnJournalByItems

                                XPCollection<JournalMap> _locJournalMapByItems = new XPCollection<JournalMap>
                                                                                 (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                  new BinaryOperator("ItemAccountGroup", _creditMemoLine.ItemAccountGroup)));

                                if (_locJournalMapByItems != null && _locJournalMapByItems.Count() > 0)
                                {
                                    foreach (JournalMap _locJournalMapByItem in _locJournalMapByItems)
                                    {
                                        XPCollection<JournalMapLine> _locJournalMapLineByItems = new XPCollection<JournalMapLine>
                                                                                                 (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                  new BinaryOperator("JournalMap", _locJournalMapByItem)));

                                        if (_locJournalMapLineByItems != null && _locJournalMapLineByItems.Count() > 0)
                                        {
                                            foreach (JournalMapLine _locJournalMapLineByItem in _locJournalMapLineByItems)
                                            {
                                                AccountMap _locAccountMapByItem = _currSession.FindObject<AccountMap>
                                                                                  (new GroupOperator(GroupOperatorType.And,
                                                                                   new BinaryOperator("Code", _locJournalMapLineByItem.AccountMap.Code),
                                                                                   new BinaryOperator("PostingType", PostingType.Purchase),
                                                                                   new BinaryOperator("OrderType", OrderType.Item),
                                                                                   new BinaryOperator("PostingMethod", PostingMethod.CreditMemo),
                                                                                   new BinaryOperator("PostingMethodType", PostingMethodType.Normal)));

                                                if (_locAccountMapByItem != null)
                                                {
                                                    XPCollection<AccountMapLine> _locAccountMapLineByItems = new XPCollection<AccountMapLine>
                                                                                                             (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                              new BinaryOperator("AccountMap", _locAccountMapByItem),
                                                                                                              new BinaryOperator("Active", true)));

                                                    if (_locAccountMapLineByItems != null && _locAccountMapLineByItems.Count() > 0)
                                                    {
                                                        double _locTotalAmountDebit = 0;
                                                        double _locTotalAmountCredit = 0;
                                                        double _locTotalBalance = 0;

                                                        foreach (AccountMapLine _locAccountMapLineByItem in _locAccountMapLineByItems)
                                                        {
                                                            if (_locAccountMapLineByItem.AccountCharge == AccountCharge.Debit)
                                                            {
                                                                _locTotalAmountDebit = _locTotalUnitPrice;
                                                            }
                                                            if (_locAccountMapLineByItem.AccountCharge == AccountCharge.Credit)
                                                            {
                                                                _locTotalAmountCredit = _locTotalUnitPrice;
                                                            }

                                                            GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                            {
                                                                PostingDate = now,
                                                                PostingType = PostingType.Purchase,
                                                                PostingMethod = PostingMethod.InvoiceAR,
                                                                Account = _locAccountMapLineByItem.Account,
                                                                Debit = _locTotalAmountDebit,
                                                                Credit = _locTotalAmountCredit,
                                                                PurchaseOrder = _locCreditMemoXPO.PurchaseOrder,
                                                                CreditMemo = _locCreditMemoXPO,
                                                            };
                                                            _saveGeneralJournal.Save();
                                                            _saveGeneralJournal.Session.CommitTransaction();

                                                            if (_locAccountMapLineByItem.Account.Code != null)
                                                            {
                                                                ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("Code", _locAccountMapLineByItem.Account.Code)));
                                                                if (_locCOA != null)
                                                                {
                                                                    if (_locCOA.BalanceType == BalanceType.Change)
                                                                    {
                                                                        if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                        {
                                                                            if (_locTotalAmountDebit > 0)
                                                                            {
                                                                                _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                            }
                                                                            if (_locTotalAmountCredit > 0)
                                                                            {
                                                                                _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                            }
                                                                        }
                                                                        if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                        {
                                                                            if (_locTotalAmountDebit > 0)
                                                                            {
                                                                                _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                            }
                                                                            if (_locTotalAmountCredit > 0)
                                                                            {
                                                                                _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                            }
                                                                        }
                                                                    }

                                                                    _locCOA.Balance = _locTotalBalance;
                                                                    _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                    _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                    _locCOA.Save();
                                                                    _locCOA.Session.CommitTransaction();
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                #endregion CreateInvoiceARReturnJournalByItems

                                #region CreateInvoiceARJournalByBusinessPartner
                                if (_locCreditMemoXPO.BuyFromVendor != null)
                                {
                                    if (_locCreditMemoXPO.BuyFromVendor.BusinessPartnerAccountGroup != null)
                                    {
                                        XPCollection<JournalMap> _locJournalMapByBusinessPartners = new XPCollection<JournalMap>
                                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                     new BinaryOperator("BusinessPartnerAccountGroup", _locCreditMemoXPO.BuyFromVendor.BusinessPartnerAccountGroup)));

                                        if (_locJournalMapByBusinessPartners != null && _locJournalMapByBusinessPartners.Count() > 0)
                                        {
                                            foreach (JournalMap _locJournalMapByBusinessPartner in _locJournalMapByBusinessPartners)
                                            {
                                                XPCollection<JournalMapLine> _locJournalMapLineByBusinessPartners = new XPCollection<JournalMapLine>
                                                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                     new BinaryOperator("JournalMap", _locJournalMapByBusinessPartner)));

                                                if (_locJournalMapLineByBusinessPartners != null && _locJournalMapLineByBusinessPartners.Count() > 0)
                                                {
                                                    foreach (JournalMapLine _locJournalMapLineByBusinessPartner in _locJournalMapLineByBusinessPartners)
                                                    {
                                                        AccountMap _locAccountMapByBusinessPartner = _currSession.FindObject<AccountMap>
                                                                                                     (new GroupOperator(GroupOperatorType.And,
                                                                                                      new BinaryOperator("Code", _locJournalMapLineByBusinessPartner.AccountMap.Code),
                                                                                                      new BinaryOperator("PostingType", PostingType.Purchase),
                                                                                                      new BinaryOperator("OrderType", OrderType.Item),
                                                                                                      new BinaryOperator("PostingMethod", PostingMethod.InvoiceAR),
                                                                                                      new BinaryOperator("PostingMethodType", PostingMethodType.Normal)));

                                                        if (_locAccountMapByBusinessPartner != null)
                                                        {
                                                            XPCollection<AccountMapLine> _locAccountMapLineByBusinessPartners = new XPCollection<AccountMapLine>
                                                                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                                 new BinaryOperator("AccountMap", _locAccountMapByBusinessPartner),
                                                                                                                                 new BinaryOperator("Active", true)));

                                                            if (_locAccountMapLineByBusinessPartners != null && _locAccountMapLineByBusinessPartners.Count() > 0)
                                                            {
                                                                double _locTotalAmountDebit = 0;
                                                                double _locTotalAmountCredit = 0;
                                                                double _locTotalBalance = 0;

                                                                foreach (AccountMapLine _locAccountMapLineByBusinessPartner in _locAccountMapLineByBusinessPartners)
                                                                {
                                                                    if (_locAccountMapLineByBusinessPartner.AccountCharge == AccountCharge.Debit)
                                                                    {
                                                                        _locTotalAmountDebit = _locTotalUnitPrice;
                                                                    }
                                                                    if (_locAccountMapLineByBusinessPartner.AccountCharge == AccountCharge.Credit)
                                                                    {
                                                                        _locTotalAmountCredit = _locTotalUnitPrice;
                                                                    }

                                                                    GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                    {
                                                                        PostingDate = now,
                                                                        PostingType = PostingType.Purchase,
                                                                        PostingMethod = PostingMethod.InvoiceAR,
                                                                        Account = _locAccountMapLineByBusinessPartner.Account,
                                                                        Debit = _locTotalAmountDebit,
                                                                        Credit = _locTotalAmountCredit,
                                                                        PurchaseOrder = _locCreditMemoXPO.PurchaseOrder,
                                                                        CreditMemo = _locCreditMemoXPO,
                                                                    };
                                                                    _saveGeneralJournal.Save();
                                                                    _saveGeneralJournal.Session.CommitTransaction();

                                                                    if (_locAccountMapLineByBusinessPartner.Account.Code != null)
                                                                    {
                                                                        ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("Code", _locAccountMapLineByBusinessPartner.Account.Code)));
                                                                        if (_locCOA != null)
                                                                        {
                                                                            if (_locCOA.BalanceType == BalanceType.Change)
                                                                            {
                                                                                if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                                {
                                                                                    if (_locTotalAmountDebit > 0)
                                                                                    {
                                                                                        _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                                    }
                                                                                    if (_locTotalAmountCredit > 0)
                                                                                    {
                                                                                        _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                                    }
                                                                                }
                                                                                if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                                {
                                                                                    if (_locTotalAmountDebit > 0)
                                                                                    {
                                                                                        _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                                    }
                                                                                    if (_locTotalAmountCredit > 0)
                                                                                    {
                                                                                        _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                                    }
                                                                                }
                                                                            }

                                                                            _locCOA.Balance = _locTotalBalance;
                                                                            _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                            _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                            _locCOA.Save();
                                                                            _locCOA.Session.CommitTransaction();
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                #endregion CreateInvoiceARJournalByBusinessPartner
                            }
                        }
                    }
                }

                #endregion CreateInvoiceARReturnJournal
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = CreditMemo ", ex.ToString());
            }
        }

        private void SetPurchaseReturnMonitoring(Session _currSession, CreditMemo _locCreditMemoXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locCreditMemoXPO != null)
                {
                    XPCollection<CreditMemoLine> _locCreditMemoLines = new XPCollection<CreditMemoLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("CreditMemo", _locCreditMemoXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Progress),
                                                            new BinaryOperator("Status", Status.Posted))));

                    if (_locCreditMemoLines != null && _locCreditMemoLines.Count() > 0)
                    {
                        foreach (CreditMemoLine _locCreditMemoLine in _locCreditMemoLines)
                        {
                            CreditMemoMonitoring _saveDataCreditMemoMonitoring = new CreditMemoMonitoring(_currSession)
                            {
                                CreditMemo = _locCreditMemoXPO,
                                CreditMemoLine = _locCreditMemoLine,
                                OrderType = _locCreditMemoLine.OrderType,
                                Item = _locCreditMemoLine.Item,
                                DQty = _locCreditMemoLine.DQty,
                                DUOM = _locCreditMemoLine.DUOM,
                                Qty = _locCreditMemoLine.Qty,
                                TQty = _locCreditMemoLine.TQty,
                                UOM = _locCreditMemoLine.UOM,
                                Currency = _locCreditMemoLine.Currency,
                                UAmount = _locCreditMemoLine.UAmount,
                                TUAmount = _locCreditMemoLine.TUAmount,
                                MultiTax = _locCreditMemoLine.MultiTax,
                                Tax = _locCreditMemoLine.Tax,
                                TxValue = _locCreditMemoLine.TxValue,
                                TxAmount = _locCreditMemoLine.TxAmount,
                                MultiDiscount = _locCreditMemoLine.MultiDiscount,
                                Discount = _locCreditMemoLine.Discount,
                                Disc = _locCreditMemoLine.Disc,
                                DiscAmount = _locCreditMemoLine.DiscAmount,
                                TAmount = _locCreditMemoLine.TAmount,
                                ReturnType = _locCreditMemoLine.ReturnType,
                            };
                            _saveDataCreditMemoMonitoring.Save();
                            _saveDataCreditMemoMonitoring.Session.CommitTransaction();

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CreditMemo " + ex.ToString());
            }
        }

        private void SetRemainQty(Session _currSession, CreditMemo _locCreditMemoXPO)
        {
            try
            {
                if (_locCreditMemoXPO != null)
                {
                    XPCollection<CreditMemoLine> _locCreditMemoLines = new XPCollection<CreditMemoLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("CreditMemo", _locCreditMemoXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locCreditMemoLines != null && _locCreditMemoLines.Count > 0)
                    {
                        double _locRmDQty = 0;
                        double _locRmQty = 0;
                        double _locInvLineTotal = 0;
                        ItemUnitOfMeasure _locItemUOM = null;

                        foreach (CreditMemoLine _locCreditMemoLine in _locCreditMemoLines)
                        {
                            #region ProcessCount=0
                            if (_locCreditMemoLine.PostedCount == 0)
                            {
                                #region MaxQuantity
                                if (_locCreditMemoLine.MxDQty > 0)
                                {
                                    if (_locCreditMemoLine.DQty > 0 && _locCreditMemoLine.DQty <= _locCreditMemoLine.MxDQty)
                                    {
                                        _locRmDQty = _locCreditMemoLine.MxDQty - _locCreditMemoLine.DQty;
                                    }

                                    if (_locCreditMemoLine.Qty > 0 && _locCreditMemoLine.Qty <= _locCreditMemoLine.MxQty)
                                    {
                                        _locRmQty = _locCreditMemoLine.MxQty - _locCreditMemoLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locCreditMemoLine.Item),
                                                                new BinaryOperator("UOM", _locCreditMemoLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locCreditMemoLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty + _locRmDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }

                                }
                                #endregion MaxQuantity

                                #region NonMaxQuantity
                                else
                                {
                                    if (_locCreditMemoLine.DQty > 0)
                                    {
                                        _locRmDQty = 0;
                                    }

                                    if (_locCreditMemoLine.Qty > 0)
                                    {
                                        _locRmQty = 0;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locCreditMemoLine.Item),
                                                                new BinaryOperator("UOM", _locCreditMemoLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locCreditMemoLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty + _locRmDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion ProcessCount=0

                            #region ProcessCount>0
                            if (_locCreditMemoLine.PostedCount > 0)
                            {
                                if (_locCreditMemoLine.RmDQty > 0)
                                {
                                    _locRmDQty = _locCreditMemoLine.RmDQty - _locCreditMemoLine.DQty;
                                }

                                if (_locCreditMemoLine.RmQty > 0)
                                {
                                    _locRmQty = _locCreditMemoLine.RmQty - _locCreditMemoLine.Qty;
                                }

                                if (_locCreditMemoLine.MxDQty > 0 || _locCreditMemoLine.MxQty > 0)
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locCreditMemoLine.Item),
                                                                new BinaryOperator("UOM", _locCreditMemoLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locCreditMemoLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                }
                                else
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locCreditMemoLine.Item),
                                                                new BinaryOperator("UOM", _locCreditMemoLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locCreditMemoLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                }


                                if (_locItemUOM != null)
                                {
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locRmQty + _locRmDQty;
                                }

                            }
                            #endregion ProcessCount>0

                            _locCreditMemoLine.RmDQty = _locRmDQty;
                            _locCreditMemoLine.RmQty = _locRmQty;
                            _locCreditMemoLine.RmTQty = _locInvLineTotal;
                            _locCreditMemoLine.Save();
                            _locCreditMemoLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CreditMemo " + ex.ToString());
            }
        }

        private void SetPostingQty(Session _currSession, CreditMemo _locCreditMemoXPO)
        {
            try
            {
                if (_locCreditMemoXPO != null)
                {
                    XPCollection<CreditMemoLine> _locCreditMemoLines = new XPCollection<CreditMemoLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("CreditMemo", _locCreditMemoXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locCreditMemoLines != null && _locCreditMemoLines.Count > 0)
                    {
                        double _locPDQty = 0;
                        double _locPQty = 0;
                        double _locInvLineTotal = 0;
                        ItemUnitOfMeasure _locItemUOM = null;

                        foreach (CreditMemoLine _locCreditMemoLine in _locCreditMemoLines)
                        {
                            #region ProcessCount=0
                            if (_locCreditMemoLine.PostedCount == 0)
                            {
                                #region MaxQuantity
                                if (_locCreditMemoLine.MxDQty > 0)
                                {
                                    if (_locCreditMemoLine.DQty > 0 && _locCreditMemoLine.DQty <= _locCreditMemoLine.MxDQty)
                                    {
                                        _locPDQty = _locCreditMemoLine.DQty;
                                    }

                                    if (_locCreditMemoLine.Qty > 0 && _locCreditMemoLine.Qty <= _locCreditMemoLine.MxQty)
                                    {
                                        _locPQty = _locCreditMemoLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locCreditMemoLine.Item),
                                                                new BinaryOperator("UOM", _locCreditMemoLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locCreditMemoLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty + _locPDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }

                                }
                                #endregion MaxQuantity

                                #region NonMaxQuantity
                                else
                                {
                                    if (_locCreditMemoLine.DQty > 0)
                                    {
                                        _locPDQty = _locCreditMemoLine.DQty;
                                    }

                                    if (_locCreditMemoLine.Qty > 0)
                                    {
                                        _locPQty = _locCreditMemoLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locCreditMemoLine.Item),
                                                                new BinaryOperator("UOM", _locCreditMemoLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locCreditMemoLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty + _locPDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion ProcessCount=0

                            #region ProcessCount>0
                            if (_locCreditMemoLine.PostedCount > 0)
                            {
                                if (_locCreditMemoLine.PDQty > 0)
                                {
                                    _locPDQty = _locCreditMemoLine.PDQty + _locCreditMemoLine.DQty;
                                }

                                if (_locCreditMemoLine.PQty > 0)
                                {
                                    _locPQty = _locCreditMemoLine.PQty + _locCreditMemoLine.Qty;
                                }

                                if (_locCreditMemoLine.MxDQty > 0 || _locCreditMemoLine.MxQty > 0)
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                           (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Item", _locCreditMemoLine.Item),
                                                            new BinaryOperator("UOM", _locCreditMemoLine.MxUOM),
                                                            new BinaryOperator("DefaultUOM", _locCreditMemoLine.MxDUOM),
                                                            new BinaryOperator("Active", true)));
                                }
                                else
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                           (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Item", _locCreditMemoLine.Item),
                                                            new BinaryOperator("UOM", _locCreditMemoLine.UOM),
                                                            new BinaryOperator("DefaultUOM", _locCreditMemoLine.DUOM),
                                                            new BinaryOperator("Active", true)));
                                }

                                if (_locItemUOM != null)
                                {
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locPQty + _locPDQty;
                                }

                            }
                            #endregion ProcessCount>0

                            _locCreditMemoLine.PDQty = _locPDQty;
                            _locCreditMemoLine.PDUOM = _locCreditMemoLine.DUOM;
                            _locCreditMemoLine.PQty = _locPQty;
                            _locCreditMemoLine.PUOM = _locCreditMemoLine.UOM;
                            _locCreditMemoLine.PTQty = _locInvLineTotal;
                            _locCreditMemoLine.Save();
                            _locCreditMemoLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CreditMemo " + ex.ToString());
            }
        }

        private void SetProcessCount(Session _currSession, CreditMemo _locCreditMemoXPO)
        {
            try
            {
                if (_locCreditMemoXPO != null)
                {
                    XPCollection<CreditMemoLine> _locCreditMemoLines = new XPCollection<CreditMemoLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("CreditMemo", _locCreditMemoXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locCreditMemoLines != null && _locCreditMemoLines.Count > 0)
                    {

                        foreach (CreditMemoLine _locCreditMemoLine in _locCreditMemoLines)
                        {
                            if (_locCreditMemoLine.Status == Status.Progress || _locCreditMemoLine.Status == Status.Posted)
                            {
                                _locCreditMemoLine.PostedCount = _locCreditMemoLine.PostedCount + 1;
                                _locCreditMemoLine.Save();
                                _locCreditMemoLine.Session.CommitTransaction();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CreditMemo " + ex.ToString());
            }
        }

        private void SetStatusPurchaseReturnLine(Session _currSession, CreditMemo _locCreditMemoXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locCreditMemoXPO != null)
                {
                    XPCollection<CreditMemoLine> _locCreditMemoLines = new XPCollection<CreditMemoLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("CreditMemo", _locCreditMemoXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locCreditMemoLines != null && _locCreditMemoLines.Count > 0)
                    {
                        foreach (CreditMemoLine _locCreditMemoLine in _locCreditMemoLines)
                        {
                            if (_locCreditMemoLine.Status == Status.Progress || _locCreditMemoLine.Status == Status.Posted)
                            {
                                if (_locCreditMemoLine.RmDQty == 0 && _locCreditMemoLine.RmQty == 0 && _locCreditMemoLine.RmTQty == 0)
                                {
                                    _locCreditMemoLine.Status = Status.Close;
                                    _locCreditMemoLine.ActivationPosting = true;
                                    _locCreditMemoLine.StatusDate = now;
                                }
                                else
                                {
                                    _locCreditMemoLine.Status = Status.Posted;
                                    _locCreditMemoLine.StatusDate = now;
                                }
                                _locCreditMemoLine.Save();
                                _locCreditMemoLine.Session.CommitTransaction();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CreditMemo " + ex.ToString());
            }
        }

        private void SetNormalQuantity(Session _currSession, CreditMemo _locCreditMemoXPO)
        {
            try
            {
                if (_locCreditMemoXPO != null)
                {
                    XPCollection<CreditMemoLine> _locCreditMemoLines = new XPCollection<CreditMemoLine>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("CreditMemo", _locCreditMemoXPO),
                                                                                     new BinaryOperator("Select", true)));

                    if (_locCreditMemoLines != null && _locCreditMemoLines.Count > 0)
                    {
                        foreach (CreditMemoLine _locCreditMemoLine in _locCreditMemoLines)
                        {
                            if (_locCreditMemoLine.Status == Status.Progress || _locCreditMemoLine.Status == Status.Posted || _locCreditMemoLine.Status == Status.Close)
                            {
                                if (_locCreditMemoLine.DQty > 0 || _locCreditMemoLine.Qty > 0)
                                {
                                    _locCreditMemoLine.Select = false;
                                    _locCreditMemoLine.DQty = 0;
                                    _locCreditMemoLine.Qty = 0;
                                    _locCreditMemoLine.Save();
                                    _locCreditMemoLine.Session.CommitTransaction();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CreditMemo " + ex.ToString());
            }
        }

        private void SetFinalStatusPurchaseReturn(Session _currSession, CreditMemo _locCreditMemoXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locStatusCount = 0;
                int _locCreditMemoLineCount = 0;

                if (_locCreditMemoXPO != null)
                {

                    XPCollection<CreditMemoLine> _locCreditMemoLines = new XPCollection<CreditMemoLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("CreditMemo", _locCreditMemoXPO)));

                    if (_locCreditMemoLines != null && _locCreditMemoLines.Count() > 0)
                    {
                        _locCreditMemoLineCount = _locCreditMemoLines.Count();

                        foreach (CreditMemoLine _locCreditMemoLine in _locCreditMemoLines)
                        {
                            if (_locCreditMemoLine.Status == Status.Close)
                            {
                                _locStatusCount = _locStatusCount + 1;
                            }
                        }
                    }

                    if (_locStatusCount == _locCreditMemoLineCount)
                    {
                        _locCreditMemoXPO.ActivationPosting = true;
                        _locCreditMemoXPO.Status = Status.Close;
                        _locCreditMemoXPO.StatusDate = now;
                        _locCreditMemoXPO.Save();
                        _locCreditMemoXPO.Session.CommitTransaction();
                    }
                    else
                    {
                        _locCreditMemoXPO.Status = Status.Posted;
                        _locCreditMemoXPO.StatusDate = now;
                        _locCreditMemoXPO.Save();
                        _locCreditMemoXPO.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CreditMemo " + ex.ToString());
            }
        }

        #endregion Posting

        #region Approval Process

        private void SetApprovalLine(Session _currentSession, CreditMemo _locCreditMemoXPO, ApprovalLevel _locApprovalLevel)
        {
            try
            {
                DateTime now = DateTime.Now;

                ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("CreditMemo", _locCreditMemoXPO),
                                                                         new BinaryOperator("ApprovalLevel", _locApprovalLevel)));

                if (_locApprovalLineXPO == null)
                {
                    ApprovalLine _saveDataAL2 = new ApprovalLine(_currentSession)
                    {
                        ApprovalDate = now,
                        ApprovalStatus = Status.Approved,
                        ApprovalLevel = _locApprovalLevel,
                        CreditMemo = _locCreditMemoXPO,
                    };
                    _saveDataAL2.Save();
                    _saveDataAL2.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CreditMemo " + ex.ToString());
            }
        }

        #endregion Approval Process

        #region Email

        private string BackgroundBody(Session _currentSession, CreditMemo _locCreditMemoXPO, MailSetupDetail _locMailSetDetail)
        {
            string body = string.Empty;

            if (_locCreditMemoXPO != null)
            {
                ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                               (new GroupOperator(GroupOperatorType.And,
                                                new BinaryOperator("CreditMemo", _locCreditMemoXPO)));

                string Status = _locApprovalLineXPO.ApprovalStatus.ToString();

                using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Email/EmailTemplate.html")))
                {
                    body = reader.ReadToEnd();
                }
                body = body.Replace("{Code}", _locCreditMemoXPO.Code);

                #region Level
                if (_locCreditMemoXPO.ActiveApproved1 == true)
                {
                    string Active = "Level 1";
                    body = body.Replace("{Level}", Active);
                }
                else if (_locCreditMemoXPO.ActiveApproved2 == true)
                {
                    string Active = "Level 2";
                    body = body.Replace("{Level}", Active);
                }
                else if (_locCreditMemoXPO.ActiveApproved3 == true)
                {
                    string Active = "Level 3";
                    body = body.Replace("{Level}", Active);
                }
                else
                {
                    string Active = "Not Available";
                    body = body.Replace("{Level}", Active);
                }
                #endregion Level

                body = body.Replace("{Status}", Status);
                body = body.Replace("{Message}", _locMailSetDetail.MessageBody);
            }

            return body;
        }

        protected void SendEmail(Session _currentSession, CreditMemo _locCreditMemoXPO, Status _locStatus, ApprovalLine _locApprovalLineXPO2)
        {
            DateTime now = DateTime.Now;
            var User = SecuritySystem.CurrentUserName;
            GlobalFunction _globFunc = new GlobalFunction();
            UserAccess _numLineUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", User));
            string _locMessageBody = null;

            try
            {
                if (_locCreditMemoXPO != null)
                {
                    XPCollection<MailSetupDetail> _locMailSetDetails = new XPCollection<MailSetupDetail>
                                                                       (_currentSession, new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("ObjectList", ObjectList.CreditMemo),
                                                                        new BinaryOperator("UserAccess", _numLineUserAccess),
                                                                        new BinaryOperator("Active", true)));

                    if (_locMailSetDetails != null && _locMailSetDetails.Count > 0)
                    {
                        foreach (MailSetupDetail _locMailSetDetail in _locMailSetDetails)
                        {
                            if (_locMailSetDetail.MessageBody != null)
                            {
                                if (_locApprovalLineXPO2.EndApproval == true)
                                {
                                    _locMessageBody = this.BackgroundBody(_currentSession, _locCreditMemoXPO, _locMailSetDetail);
                                }
                                else
                                {
                                    _locMessageBody = this.BackgroundBody(_currentSession, _locCreditMemoXPO, _locMailSetDetail);
                                }
                            }

                            _globFunc.SetAndSendMail(_locMailSetDetail.SmtpHost, _locMailSetDetail.SmtpPort,
                            _locMailSetDetail.MailFrom, _locMailSetDetail.MailFromPassword, _locMailSetDetail.MailTo, _locMailSetDetail.MailSubject, _locMessageBody);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CreditMemo " + ex.ToString());
            }
        }

        #endregion Email

        #region Global Method

        private void SuccessMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 4000;
            options.Message = string.Format("{0}", _locMessage);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method

        

        
    }
}
