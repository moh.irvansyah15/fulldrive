﻿namespace FullDrive.Module.Controllers
{
    partial class PurchasePrePaymentInvoiceActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.PurchasePrePaymentInvoiceProgressAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PurchasePrePaymentInvoicePostingAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PurchasePrePaymentInvoiceGetPayAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PurchasePrePaymentInvoiceSelectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PurchasePrePaymentInvoiceUnselectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PurchasePrePaymentInvoiceApprovalAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.PurchasePrePaymentInvoiceListviewFilterSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // PurchasePrePaymentInvoiceProgressAction
            // 
            this.PurchasePrePaymentInvoiceProgressAction.Caption = "Progress";
            this.PurchasePrePaymentInvoiceProgressAction.ConfirmationMessage = null;
            this.PurchasePrePaymentInvoiceProgressAction.Id = "PurchasePrePaymentInvoiceProgressActionId";
            this.PurchasePrePaymentInvoiceProgressAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchasePrePaymentInvoice);
            this.PurchasePrePaymentInvoiceProgressAction.ToolTip = null;
            this.PurchasePrePaymentInvoiceProgressAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PurchasePrePaymentInvoiceProgressAction_Execute);
            // 
            // PurchasePrePaymentInvoicePostingAction
            // 
            this.PurchasePrePaymentInvoicePostingAction.Caption = "Posting";
            this.PurchasePrePaymentInvoicePostingAction.ConfirmationMessage = null;
            this.PurchasePrePaymentInvoicePostingAction.Id = "PurchasePrePaymentInvoicePostingActionId";
            this.PurchasePrePaymentInvoicePostingAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchasePrePaymentInvoice);
            this.PurchasePrePaymentInvoicePostingAction.ToolTip = null;
            this.PurchasePrePaymentInvoicePostingAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PurchasePrePaymentInvoicePostingAction_Execute);
            // 
            // PurchasePrePaymentInvoiceGetPayAction
            // 
            this.PurchasePrePaymentInvoiceGetPayAction.Caption = "Get Pay";
            this.PurchasePrePaymentInvoiceGetPayAction.ConfirmationMessage = null;
            this.PurchasePrePaymentInvoiceGetPayAction.Id = "PurchasePrePaymentInvoiceGetPayActionId";
            this.PurchasePrePaymentInvoiceGetPayAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchasePrePaymentInvoice);
            this.PurchasePrePaymentInvoiceGetPayAction.ToolTip = null;
            this.PurchasePrePaymentInvoiceGetPayAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PurchasePrePaymentInvoiceGetPayAction_Execute);
            // 
            // PurchasePrePaymentInvoiceSelectAction
            // 
            this.PurchasePrePaymentInvoiceSelectAction.Caption = "Select";
            this.PurchasePrePaymentInvoiceSelectAction.ConfirmationMessage = null;
            this.PurchasePrePaymentInvoiceSelectAction.Id = "PurchasePrePaymentInvoiceSelectActionId";
            this.PurchasePrePaymentInvoiceSelectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchasePrePaymentInvoice);
            this.PurchasePrePaymentInvoiceSelectAction.ToolTip = null;
            this.PurchasePrePaymentInvoiceSelectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PurchasePrePaymentInvoiceSelectAction_Execute);
            // 
            // PurchasePrePaymentInvoiceUnselectAction
            // 
            this.PurchasePrePaymentInvoiceUnselectAction.Caption = "Unselect";
            this.PurchasePrePaymentInvoiceUnselectAction.ConfirmationMessage = null;
            this.PurchasePrePaymentInvoiceUnselectAction.Id = "PurchasePrePaymentInvoiceUnselectActionId";
            this.PurchasePrePaymentInvoiceUnselectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchasePrePaymentInvoice);
            this.PurchasePrePaymentInvoiceUnselectAction.ToolTip = null;
            this.PurchasePrePaymentInvoiceUnselectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PurchasePrePaymentInvoiceUnselectAction_Execute);
            // 
            // PurchasePrePaymentInvoiceApprovalAction
            // 
            this.PurchasePrePaymentInvoiceApprovalAction.Caption = "Approval";
            this.PurchasePrePaymentInvoiceApprovalAction.ConfirmationMessage = null;
            this.PurchasePrePaymentInvoiceApprovalAction.Id = "PurchasePrePaymentInvoiceApprovalActionId";
            this.PurchasePrePaymentInvoiceApprovalAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.PurchasePrePaymentInvoiceApprovalAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchasePrePaymentInvoice);
            this.PurchasePrePaymentInvoiceApprovalAction.ToolTip = null;
            this.PurchasePrePaymentInvoiceApprovalAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.PurchasePrePaymentInvoiceApprovalAction_Execute);
            // 
            // PurchasePrePaymentInvoiceListviewFilterSelectionAction
            // 
            this.PurchasePrePaymentInvoiceListviewFilterSelectionAction.Caption = "Filter";
            this.PurchasePrePaymentInvoiceListviewFilterSelectionAction.ConfirmationMessage = null;
            this.PurchasePrePaymentInvoiceListviewFilterSelectionAction.Id = "PurchasePrePaymentInvoiceListviewFilterSelectionActionId";
            this.PurchasePrePaymentInvoiceListviewFilterSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.PurchasePrePaymentInvoiceListviewFilterSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchasePrePaymentInvoice);
            this.PurchasePrePaymentInvoiceListviewFilterSelectionAction.ToolTip = null;
            this.PurchasePrePaymentInvoiceListviewFilterSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.PurchasePrePaymentInvoiceListviewFilterSelectionAction_Execute);
            // 
            // PurchasePrePaymentInvoiceActionController
            // 
            this.Actions.Add(this.PurchasePrePaymentInvoiceProgressAction);
            this.Actions.Add(this.PurchasePrePaymentInvoicePostingAction);
            this.Actions.Add(this.PurchasePrePaymentInvoiceGetPayAction);
            this.Actions.Add(this.PurchasePrePaymentInvoiceSelectAction);
            this.Actions.Add(this.PurchasePrePaymentInvoiceUnselectAction);
            this.Actions.Add(this.PurchasePrePaymentInvoiceApprovalAction);
            this.Actions.Add(this.PurchasePrePaymentInvoiceListviewFilterSelectionAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction PurchasePrePaymentInvoiceProgressAction;
        private DevExpress.ExpressApp.Actions.SimpleAction PurchasePrePaymentInvoicePostingAction;
        private DevExpress.ExpressApp.Actions.SimpleAction PurchasePrePaymentInvoiceGetPayAction;
        private DevExpress.ExpressApp.Actions.SimpleAction PurchasePrePaymentInvoiceSelectAction;
        private DevExpress.ExpressApp.Actions.SimpleAction PurchasePrePaymentInvoiceUnselectAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction PurchasePrePaymentInvoiceApprovalAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction PurchasePrePaymentInvoiceListviewFilterSelectionAction;
    }
}
