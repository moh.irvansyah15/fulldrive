﻿namespace FullDrive.Module.Controllers
{
    partial class RoutingLineActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.RoutingLineDeliveryAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.RoutingLineSelectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.RoutingLineUnselectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // RoutingLineDeliveryAction
            // 
            this.RoutingLineDeliveryAction.Caption = "Delivery";
            this.RoutingLineDeliveryAction.ConfirmationMessage = null;
            this.RoutingLineDeliveryAction.Id = "RoutingLineDeliveryActionId";
            this.RoutingLineDeliveryAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.RoutingLine);
            this.RoutingLineDeliveryAction.ToolTip = null;
            this.RoutingLineDeliveryAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.RoutingLineDeliveryAction_Execute);
            // 
            // RoutingLineSelectAction
            // 
            this.RoutingLineSelectAction.Caption = "Select";
            this.RoutingLineSelectAction.ConfirmationMessage = null;
            this.RoutingLineSelectAction.Id = "RoutingLineSelectActionId";
            this.RoutingLineSelectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.RoutingLine);
            this.RoutingLineSelectAction.ToolTip = null;
            this.RoutingLineSelectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.RoutingLineSelectAction_Execute);
            // 
            // RoutingLineUnselectAction
            // 
            this.RoutingLineUnselectAction.Caption = "Unselect";
            this.RoutingLineUnselectAction.ConfirmationMessage = null;
            this.RoutingLineUnselectAction.Id = "RoutingLineUnselectActionId";
            this.RoutingLineUnselectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.RoutingLine);
            this.RoutingLineUnselectAction.ToolTip = null;
            this.RoutingLineUnselectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.RoutingLineUnselectAction_Execute);
            // 
            // RoutingLineActionController
            // 
            this.Actions.Add(this.RoutingLineDeliveryAction);
            this.Actions.Add(this.RoutingLineSelectAction);
            this.Actions.Add(this.RoutingLineUnselectAction);

        }

        #endregion
        private DevExpress.ExpressApp.Actions.SimpleAction RoutingLineDeliveryAction;
        private DevExpress.ExpressApp.Actions.SimpleAction RoutingLineSelectAction;
        private DevExpress.ExpressApp.Actions.SimpleAction RoutingLineUnselectAction;
    }
}
