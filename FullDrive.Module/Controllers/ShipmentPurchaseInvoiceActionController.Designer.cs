﻿namespace FullDrive.Module.Controllers
{
    partial class ShipmentPurchaseInvoiceActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ShipmentPurchaseInvoiceGetSPMAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.ShipmentPurchaseInvoiceProgressAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.ShipmentPurchaseInvoiceGetPayAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.ShipmentPurchaseInvoiceApprovalAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.ShipmentPurchaseInvoiceListviewFilterSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.ShipmentPurchaseInvoiceListviewFilterApprovalSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.ShipmentPurchaseInvoicePostingAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // ShipmentPurchaseInvoiceGetSPMAction
            // 
            this.ShipmentPurchaseInvoiceGetSPMAction.Caption = "Get SPM";
            this.ShipmentPurchaseInvoiceGetSPMAction.ConfirmationMessage = null;
            this.ShipmentPurchaseInvoiceGetSPMAction.Id = "ShipmentPurchaseInvoiceGetSPMActionId";
            this.ShipmentPurchaseInvoiceGetSPMAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.ShipmentPurchaseInvoice);
            this.ShipmentPurchaseInvoiceGetSPMAction.ToolTip = null;
            this.ShipmentPurchaseInvoiceGetSPMAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.ShipmentPurchaseInvoiceGetSPMAction_Execute);
            // 
            // ShipmentPurchaseInvoiceProgressAction
            // 
            this.ShipmentPurchaseInvoiceProgressAction.Caption = "Progress";
            this.ShipmentPurchaseInvoiceProgressAction.ConfirmationMessage = null;
            this.ShipmentPurchaseInvoiceProgressAction.Id = "ShipmentPurchaseInvoiceProgressActionId";
            this.ShipmentPurchaseInvoiceProgressAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.ShipmentPurchaseInvoice);
            this.ShipmentPurchaseInvoiceProgressAction.ToolTip = null;
            this.ShipmentPurchaseInvoiceProgressAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.ShipmentPurchaseInvoiceProgressAction_Execute);
            // 
            // ShipmentPurchaseInvoiceGetPayAction
            // 
            this.ShipmentPurchaseInvoiceGetPayAction.Caption = "Get Pay";
            this.ShipmentPurchaseInvoiceGetPayAction.ConfirmationMessage = null;
            this.ShipmentPurchaseInvoiceGetPayAction.Id = "ShipmentPurchaseInvoiceGetPayActionId";
            this.ShipmentPurchaseInvoiceGetPayAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.ShipmentPurchaseInvoice);
            this.ShipmentPurchaseInvoiceGetPayAction.ToolTip = null;
            this.ShipmentPurchaseInvoiceGetPayAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.ShipmentPurchaseInvoiceGetPayAction_Execute);
            // 
            // ShipmentPurchaseInvoiceApprovalAction
            // 
            this.ShipmentPurchaseInvoiceApprovalAction.Caption = "Approval";
            this.ShipmentPurchaseInvoiceApprovalAction.ConfirmationMessage = null;
            this.ShipmentPurchaseInvoiceApprovalAction.Id = "ShipmentPurchaseInvoiceApprovalActionId";
            this.ShipmentPurchaseInvoiceApprovalAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.ShipmentPurchaseInvoiceApprovalAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.ShipmentPurchaseInvoice);
            this.ShipmentPurchaseInvoiceApprovalAction.ToolTip = null;
            this.ShipmentPurchaseInvoiceApprovalAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.ShipmentPurchaseInvoiceApprovalAction_Execute);
            // 
            // ShipmentPurchaseInvoiceListviewFilterSelectionAction
            // 
            this.ShipmentPurchaseInvoiceListviewFilterSelectionAction.Caption = "Filter";
            this.ShipmentPurchaseInvoiceListviewFilterSelectionAction.ConfirmationMessage = null;
            this.ShipmentPurchaseInvoiceListviewFilterSelectionAction.Id = "ShipmentPurchaseInvoiceListviewFilterSelectionActionId";
            this.ShipmentPurchaseInvoiceListviewFilterSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.ShipmentPurchaseInvoiceListviewFilterSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.ShipmentPurchaseInvoice);
            this.ShipmentPurchaseInvoiceListviewFilterSelectionAction.ToolTip = null;
            this.ShipmentPurchaseInvoiceListviewFilterSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.ShipmentPurchaseInvoiceListviewFilterSelectionAction_Execute);
            // 
            // ShipmentPurchaseInvoiceListviewFilterApprovalSelectionAction
            // 
            this.ShipmentPurchaseInvoiceListviewFilterApprovalSelectionAction.Caption = "Filter Approval";
            this.ShipmentPurchaseInvoiceListviewFilterApprovalSelectionAction.ConfirmationMessage = null;
            this.ShipmentPurchaseInvoiceListviewFilterApprovalSelectionAction.Id = "ShipmentPurchaseInvoiceListviewFilterApprovalSelectionActionId";
            this.ShipmentPurchaseInvoiceListviewFilterApprovalSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.ShipmentPurchaseInvoiceListviewFilterApprovalSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.ShipmentPurchaseInvoice);
            this.ShipmentPurchaseInvoiceListviewFilterApprovalSelectionAction.ToolTip = null;
            this.ShipmentPurchaseInvoiceListviewFilterApprovalSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.ShipmentPurchaseInvoiceListviewFilterApprovalSelectionAction_Execute);
            // 
            // ShipmentPurchaseInvoicePostingAction
            // 
            this.ShipmentPurchaseInvoicePostingAction.Caption = "Posting";
            this.ShipmentPurchaseInvoicePostingAction.ConfirmationMessage = null;
            this.ShipmentPurchaseInvoicePostingAction.Id = "ShipmentPurchaseInvoicePostingActionId";
            this.ShipmentPurchaseInvoicePostingAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.ShipmentPurchaseInvoice);
            this.ShipmentPurchaseInvoicePostingAction.ToolTip = null;
            this.ShipmentPurchaseInvoicePostingAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.ShipmentPurchaseInvoicePostingAction_Execute);
            // 
            // ShipmentPurchaseInvoiceActionController
            // 
            this.Actions.Add(this.ShipmentPurchaseInvoiceGetSPMAction);
            this.Actions.Add(this.ShipmentPurchaseInvoiceProgressAction);
            this.Actions.Add(this.ShipmentPurchaseInvoiceGetPayAction);
            this.Actions.Add(this.ShipmentPurchaseInvoiceApprovalAction);
            this.Actions.Add(this.ShipmentPurchaseInvoiceListviewFilterSelectionAction);
            this.Actions.Add(this.ShipmentPurchaseInvoiceListviewFilterApprovalSelectionAction);
            this.Actions.Add(this.ShipmentPurchaseInvoicePostingAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction ShipmentPurchaseInvoiceGetSPMAction;
        private DevExpress.ExpressApp.Actions.SimpleAction ShipmentPurchaseInvoiceProgressAction;
        private DevExpress.ExpressApp.Actions.SimpleAction ShipmentPurchaseInvoiceGetPayAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction ShipmentPurchaseInvoiceApprovalAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction ShipmentPurchaseInvoiceListviewFilterSelectionAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction ShipmentPurchaseInvoiceListviewFilterApprovalSelectionAction;
        private DevExpress.ExpressApp.Actions.SimpleAction ShipmentPurchaseInvoicePostingAction;
    }
}
