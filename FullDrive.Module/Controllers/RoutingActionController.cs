﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using FullDrive.Module.BusinessObjects;
using DevExpress.ExpressApp.Xpo;
using FullDrive.Module.CustomProcess;
using System.Collections;
using DevExpress.Xpo;

using System.Web;
using System.IO;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class RoutingActionController : ViewController
    {
        public RoutingActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void RoutingProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        Routing _locRoutingOS = (Routing)_objectSpace.GetObject(obj);

                        if (_locRoutingOS != null)
                        {
                            if (_locRoutingOS.Code != null)
                            {
                                _currObjectId = _locRoutingOS.Code;

                                Routing _locRoutingXPO = _currSession.FindObject<Routing>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locRoutingXPO != null)
                                {
                                    #region Status
                                    if (_locRoutingXPO.Status == Status.Open || _locRoutingXPO.Status == Status.Progress || _locRoutingXPO.Status == Status.Posted)
                                    {
                                        if (_locRoutingXPO.Status == Status.Open)
                                        {
                                            _locRoutingXPO.Status = Status.Progress;
                                            _locRoutingXPO.StatusDate = now;
                                            _locRoutingXPO.Save();
                                            _locRoutingXPO.Session.CommitTransaction();
                                        }

                                        #region RoutingPackage
                                        XPCollection<RoutingPackage> _locRoutingPackages = new XPCollection<RoutingPackage>
                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Routing", _locRoutingXPO),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Open),
                                                            new BinaryOperator("Status", Status.Progress))
                                                            ));

                                        if (_locRoutingPackages != null && _locRoutingPackages.Count > 0)
                                        {
                                            foreach (RoutingPackage _locRoutingPackage in _locRoutingPackages)
                                            {
                                                _locRoutingPackage.Status = Status.Progress;
                                                _locRoutingPackage.StatusDate = now;
                                                _locRoutingPackage.Save();
                                                _locRoutingPackage.Session.CommitTransaction();
                                            }
                                        }
                                        #endregion Package
                                        #region PackageCollection
                                        XPCollection<PackageCollection> _locPackageCollections = new XPCollection<PackageCollection>
                                                                                 (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("Routing", _locRoutingXPO),
                                                                                 new GroupOperator(GroupOperatorType.Or,
                                                                                 new BinaryOperator("Status", Status.Open),
                                                                                 new BinaryOperator("Status", Status.Progress))));
                                        if (_locPackageCollections != null && _locPackageCollections.Count() > 0)
                                        {
                                            foreach (PackageCollection _locPackageCollection in _locPackageCollections)
                                            {
                                                _locPackageCollection.Status = Status.Progress;
                                                _locPackageCollection.StatusDate = now;
                                                _locPackageCollection.Save();
                                                _locPackageCollection.Session.CommitTransaction();
                                            }
                                        }
                                        #endregion PackageCollection
                                        #region RoutingLine
                                        XPCollection<PackageCollection> _locRoutingLines = new XPCollection<PackageCollection>
                                                                                 (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("Routing", _locRoutingXPO),
                                                                                 new BinaryOperator("Select", true),
                                                                                 new GroupOperator(GroupOperatorType.Or,
                                                                                 new BinaryOperator("Status", Status.Open),
                                                                                 new BinaryOperator("Status", Status.Progress))));
                                        if (_locPackageCollections != null && _locPackageCollections.Count() > 0)
                                        {
                                            foreach (PackageCollection _locRoutingLine in _locRoutingLines)
                                            {
                                                _locRoutingLine.Status = Status.Progress;
                                                _locRoutingLine.StatusDate = now;
                                                _locRoutingLine.Save();
                                                _locRoutingLine.Session.CommitTransaction();
                                            }
                                        }
                                        #endregion RoutingLine
                                    }
                                    #endregion Status

                                    SuccessMessageShow("Routing has successfully updated to progress");
                                }
                                else
                                {
                                    ErrorMessageShow("Data Routing Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Routing Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = Routing " + ex.ToString());
            }
        }

        private void RoutingPostingRouteAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        Routing _locRoutingOS = (Routing)_objectSpace.GetObject(obj);

                        if (_locRoutingOS != null)
                        {
                            if (_locRoutingOS.Status == Status.Progress || _locRoutingOS.Status == Status.Posted)
                            {
                                if (_locRoutingOS.Code != null)
                                {
                                    _currObjectId = _locRoutingOS.Code;

                                    Routing _locRoutingXPO = _currSession.FindObject<Routing>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("Code", _currObjectId)));

                                    if (_locRoutingXPO != null)
                                    {
                                        SetShipmentPurchaseMonitoring(_currSession, _locRoutingXPO);
                                    }
                                    else
                                    {
                                        ErrorMessageShow("Routing Data Not Available");
                                    }
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Routing Data Not Available");
                            }
                        }
                    }
                }

                //auto refresh
                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = RoutingLine " + ex.ToString());
            }
        }

        private void RoutingPostingPackageAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        Routing _locRoutingOS = (Routing)_objectSpace.GetObject(obj);

                        if (_locRoutingOS != null)
                        {
                            if (_locRoutingOS.Status == Status.Progress || _locRoutingOS.Status == Status.Posted)
                            {
                                if (_locRoutingOS.Code != null)
                                {
                                    _currObjectId = _locRoutingOS.Code;

                                    Routing _locRoutingXPO = _currSession.FindObject<Routing>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("Code", _currObjectId)));

                                    if (_locRoutingXPO != null)
                                    {
                                        SetShipmentSalesMonitoring(_currSession, _locRoutingXPO);
                                    }
                                    else
                                    {
                                        ErrorMessageShow("Routing Data Not Available");
                                    }
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Routing Data Not Available");
                            }
                        }
                    }
                }

                //auto refresh
                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = RoutingLine " + ex.ToString());
            }
        }

        //================================================================== Code Only =======================================================

        #region PostingRoute

        private void SetShipmentPurchaseMonitoring(Session _currSession, Routing _locRoutingXPO)
        {
            try
            {
                
                XPQuery<RoutingLine> _routingLineQueries = new XPQuery<RoutingLine>(_currSession);
                XPCollection<RoutingLine> _locRoutingLines = null;
                Routing _locRouting = null;
                ShipmentType _locShipmentType = ShipmentType.None;
                BusinessPartner _locCarrier = null;
                String _locWorkOrder = null;
                DeliveryType _locDeliveryType = null;
                PriceType _locPriceType = PriceType.None;
                PriceType _locFinPriceType = PriceType.None;
                bool _locBoolPriceType = true;
                double _locActualQuantity = 0;
                double _locFinActualQuantity = 0;
                bool _locBoolActualQuantity = true;
                VehicleType _locVehicleType = null;
                VehicleType _locFinVehicleType = null;
                bool _locBoolVehicleType = true;
                TransactionTerm _locTransactionTerm = TransactionTerm.None;
                TransactionTerm _locFinTransactionTerm = TransactionTerm.None;
                bool _locBoolTransactionTerm = true;
                ContainerType _locContainerType = null;
                ContainerType _locFinContainerType = null;
                bool _locBoolContainerType = true;
                TransportType _locTransportType = null;
                TransportType _locFinTransportType = null;
                bool _locBoolTransportType = true;
                UnitOfMeasure _locUOM = null;
                UnitOfMeasure _locFinUOM = null;
                bool _locBoolUOM = true;
                double _amountPayable = 0;
                VehicleMode _locVehicleMode = VehicleMode.None;
                VehicleMode _locFinVehicleMode = VehicleMode.None;
                bool _locBoolVehicleMode = true;
                Vehicle _locVehicle = null;
                Vehicle _locFinVehicle = null;
                bool _locBoolVehicle = true;

                GlobalFunction _globFunc = new GlobalFunction();

                if(_locRoutingXPO != null)
                {
                    _locRouting = _locRoutingXPO;

                    #region GroupByShipmentType

                    var _routingLineByShipmentTypes = from _rl in _routingLineQueries
                                                      where (_rl.Routing == _locRouting 
                                                      && _rl.Select == true)
                                                      group _rl by _rl.ShipmentType into g
                                                      select new { ShipmentType = g.Key };

                    if (_routingLineByShipmentTypes != null)
                    {
                        foreach (var _routingLineByShipmentType in _routingLineByShipmentTypes)
                        {
                            if (_routingLineByShipmentType.ShipmentType != ShipmentType.None || _routingLineByShipmentType.ShipmentType == ShipmentType.ExportAndImport)
                            {
                                _locShipmentType = _routingLineByShipmentType.ShipmentType;

                                #region GroupByCarrier

                                var _routingLineByCarriers = from _rlc in _routingLineQueries
                                                             where (_rlc.Routing == _locRouting
                                                             && _rlc.ShipmentType == _locShipmentType
                                                             && _rlc.Select == true)
                                                             group _rlc by _rlc.Carrier into g
                                                             select new { Carrier = g.Key };
                                if (_routingLineByCarriers != null)
                                {
                                    foreach (var _routingLineByCarrier in _routingLineByCarriers)
                                    {
                                        if (_routingLineByCarrier.Carrier != null)
                                        {
                                            _locCarrier = _routingLineByCarrier.Carrier;

                                            #region GroupByWorkOrderOrSPK

                                            var _routingLineByWorkOrders = from _rlw in _routingLineQueries
                                                                           where (_rlw.Routing == _locRouting
                                                                           && _rlw.ShipmentType == _locShipmentType
                                                                           && _rlw.Carrier == _locCarrier
                                                                           && _rlw.Select == true)
                                                                           group _rlw by _rlw.WorkOrder into g
                                                                           select new { WorkOrder = g.Key };

                                            if (_routingLineByWorkOrders != null)
                                            {
                                                foreach (var _routingLineByWorkOrder in _routingLineByWorkOrders)
                                                {
                                                    if (_routingLineByWorkOrder.WorkOrder != null)
                                                    {
                                                        _locWorkOrder = _routingLineByWorkOrder.WorkOrder;

                                                        #region GroupByDeliveryType

                                                        var _routingLineByDeliveryTypes = from _rldt in _routingLineQueries
                                                                                          where (_rldt.Routing == _locRouting
                                                                                          && _rldt.ShipmentType == _locShipmentType
                                                                                          && _rldt.Carrier == _locCarrier
                                                                                          && _rldt.WorkOrder == _locWorkOrder
                                                                                          && _rldt.Select == true)
                                                                                          group _rldt by _rldt.DeliveryType into g
                                                                                          select new { DeliveryType = g.Key };

                                                        if (_routingLineByDeliveryTypes != null)
                                                        {
                                                            foreach (var _routingLineByDeliveryType in _routingLineByDeliveryTypes)
                                                            {
                                                                if (_routingLineByDeliveryType.DeliveryType != null)
                                                                {
                                                                    _locDeliveryType = _routingLineByDeliveryType.DeliveryType;

                                                                    _locRoutingLines = new XPCollection<RoutingLine>
                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("ShipmentType", _locShipmentType),
                                                                                    new BinaryOperator("Routing", _locRouting),
                                                                                    new BinaryOperator("Carrier", _locCarrier),
                                                                                    new BinaryOperator("WorkOrder", _locWorkOrder),
                                                                                    new BinaryOperator("Status", CustomProcess.Status.Delivered),
                                                                                    new BinaryOperator("DeliveryType", _locDeliveryType),
                                                                                    new BinaryOperator("Select", true)),
                                                                                    new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));

                                                                    if (_locRoutingLines != null && _locRoutingLines.Count() > 0)
                                                                    {
                                                                        #region GetVariable
                                                                        foreach (RoutingLine _locRoutingLine in _locRoutingLines)
                                                                        {
                                                                            #region MainGetVariable
                                                                            if(_locPriceType == PriceType.None && _locRoutingLine.PriceType != PriceType.None)
                                                                            {
                                                                                _locPriceType = _locRoutingLine.PriceType;
                                                                            }
                                                                            else
                                                                            {
                                                                                if (_locPriceType != _locRoutingLine.PriceType)
                                                                                {
                                                                                    _locBoolPriceType = false;
                                                                                }
                                                                            }

                                                                            if (_locActualQuantity == 0 && _locRoutingLine.ActualQuantity > 0)
                                                                            {
                                                                                _locActualQuantity = _locRoutingLine.ActualQuantity;
                                                                            }
                                                                            else
                                                                            {
                                                                                if (_locActualQuantity != _locRoutingLine.ActualQuantity)
                                                                                {
                                                                                    _locBoolActualQuantity = false;
                                                                                }
                                                                            }

                                                                            if (_locUOM == null && _locRoutingLine.UOM != null)
                                                                            {
                                                                                _locUOM = _locRoutingLine.UOM;
                                                                            }
                                                                            else
                                                                            {
                                                                                if (_locUOM != _locRoutingLine.UOM)
                                                                                {
                                                                                    _locBoolUOM = false;
                                                                                }
                                                                            }

                                                                            if (_locVehicleType == null && _locRoutingLine.VehicleType != null)
                                                                            {
                                                                                _locVehicleType = _locRoutingLine.VehicleType;
                                                                            }
                                                                            else
                                                                            {
                                                                                if (_locVehicleType != _locRoutingLine.VehicleType)
                                                                                {
                                                                                    _locBoolVehicleType = false;
                                                                                }
                                                                            }

                                                                            if (_locTransactionTerm == TransactionTerm.None && _locRoutingLine.TransactionTerm != TransactionTerm.None)
                                                                            {
                                                                                _locTransactionTerm = _locRoutingLine.TransactionTerm;
                                                                            }
                                                                            else
                                                                            {
                                                                                if (_locTransactionTerm != _locRoutingLine.TransactionTerm)
                                                                                {
                                                                                    _locBoolTransactionTerm = false;
                                                                                }
                                                                            }

                                                                            if (_locTransportType == null && _locRoutingLine.TransportType != null)
                                                                            {
                                                                                _locTransportType = _locRoutingLine.TransportType;
                                                                            }
                                                                            else
                                                                            {
                                                                                if (_locTransportType != _locRoutingLine.TransportType)
                                                                                {
                                                                                    _locBoolTransportType = false;
                                                                                }
                                                                            }

                                                                            if (_locContainerType == null && _locRoutingLine.ContainerType != null)
                                                                            {
                                                                                _locContainerType = _locRoutingLine.ContainerType;
                                                                            }
                                                                            else
                                                                            {
                                                                                if (_locContainerType != _locRoutingLine.ContainerType)
                                                                                {
                                                                                    _locBoolContainerType = false;
                                                                                }
                                                                            }
                                                                            #endregion MainGetVariable
                                                                            #region SubmainGetVariable
                                                                            if (_locVehicleMode == VehicleMode.None && _locRoutingLine.VehicleMode != VehicleMode.None)
                                                                            {
                                                                                _locVehicleMode = _locRoutingLine.VehicleMode;
                                                                            }
                                                                            else
                                                                            {
                                                                                if (_locVehicleMode != _locRoutingLine.VehicleMode)
                                                                                {
                                                                                    _locBoolVehicleMode = false;
                                                                                }
                                                                            }

                                                                            if (_locVehicle == null && _locRoutingLine.Vehicle != null)
                                                                            {
                                                                                _locVehicle = _locRoutingLine.Vehicle;
                                                                            }
                                                                            else
                                                                            {
                                                                                if (_locVehicle != _locRoutingLine.Vehicle)
                                                                                {
                                                                                    _locBoolVehicle = false;
                                                                                }
                                                                            }
                                                                            #endregion SubmainGetVariable
                                                                        }
                                                                        #endregion GetVariable

                                                                        #region SetVariable

                                                                        //main
                                                                        if (_locBoolPriceType == true) { _locFinPriceType = _locPriceType; }
                                                                        if (_locBoolActualQuantity == true) { _locFinActualQuantity = _locActualQuantity; }
                                                                        if (_locBoolUOM == true) { _locFinUOM = _locUOM; }
                                                                        if (_locBoolVehicleType == true) { _locFinVehicleType = _locVehicleType; }
                                                                        if (_locBoolTransactionTerm == true) { _locFinTransactionTerm = _locTransactionTerm; }
                                                                        if (_locBoolTransportType == true) { _locFinTransportType = _locTransportType; }
                                                                        if (_locBoolContainerType == true) { _locFinContainerType = _locContainerType; }
                                                                        if (_locBoolTransactionTerm == true) { _locFinTransactionTerm = _locTransactionTerm; }
                                                                        //submain
                                                                        if (_locBoolVehicleMode == true) { _locFinVehicleMode = _locVehicleMode; }
                                                                        if (_locBoolVehicle == true) { _locFinVehicle = _locVehicle; }
                                                                        #endregion SetVariable

                                                                        _amountPayable = GetVendorBuyingRate(_currSession, _locShipmentType, _locRouting, _locCarrier, _locWorkOrder, _locDeliveryType,
                                                                                        _locFinTransportType, _locFinContainerType, _locFinVehicleType, _locFinTransactionTerm,
                                                                                        _locFinPriceType, _locFinActualQuantity, _locFinUOM).Price;

                                                                        //Membuat ShipmentPurchaseInvoice

                                                                        if(_amountPayable > 0)
                                                                        {
                                                                            ShipmentPurchaseMonitoring _saveDataShipmentPurchaseMonitoring = new ShipmentPurchaseMonitoring(_currSession)
                                                                            {
                                                                                Routing = _locRouting,
                                                                                ShipmentType = _locShipmentType,
                                                                                TransactionTerm = _locFinTransactionTerm,
                                                                                TransportType = _locFinTransportType,
                                                                                ContainerType = _locFinContainerType,
                                                                                PriceType = _locFinPriceType,
                                                                                DeliveryType = _locDeliveryType,
                                                                                WorkOrder = _locWorkOrder,
                                                                                Carrier = _locCarrier,
                                                                                VehicleMode = _locFinVehicleMode,
                                                                                Vehicle = _locFinVehicle,
                                                                                VehicleType = _locFinVehicleType,
                                                                                ActualQuantity = _locFinActualQuantity,
                                                                                UOM = _locFinUOM,
                                                                                CountryFrom = _locRouting.CountryFrom,
                                                                                CityFrom = _locRouting.CityFrom,
                                                                                AreaFrom = _locRouting.AreaFrom,
                                                                                LocationFrom = _locRouting.LocationFrom,
                                                                                TransportLocationFrom = _locRouting.TransportLocationFrom,
                                                                                CountryTo = _locRouting.CountryTo,
                                                                                CityTo = _locRouting.CityTo,
                                                                                AreaTo = _locRouting.AreaTo,
                                                                                LocationTo = _locRouting.LocationTo,
                                                                                TransportLocationTo = _locRouting.TransportLocationTo,
                                                                                Amount = _amountPayable,
                                                                                MaxAmount = _amountPayable,
                                                                            };
                                                                            _saveDataShipmentPurchaseMonitoring.Save();
                                                                            _saveDataShipmentPurchaseMonitoring.Session.CommitTransaction();
                                                                        }
                                                                        
                                                                    }
                                                                }
                                                            }
                                                        }

                                                        #endregion GroupByDeliveryType
                                                    }
                                                }
                                            }

                                            #endregion GroupByWorkOrderOrSPK
                                        }
                                    }
                                }

                                #endregion GroupByCarrier
                            }
                        }
                    }


                    #endregion GroupByShipmentType
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = Routing " + ex.ToString());
            }
        }

        private Rate GetVendorBuyingRate(Session _currSession, ShipmentType _locShipmentType, Routing _locRouting, BusinessPartner _locCarrier,
                                         String _locWorkOrder, DeliveryType _locDeliveryType, TransportType _locTransportType, ContainerType _locContainerType,
                                         VehicleType _locVehicleType, TransactionTerm _locTransactionTerm,
                                         PriceType _currLocPriceType, double _locActualQuantity, UnitOfMeasure _locUOM)
        {
            string _fullNameRoutes = null;
            Rate _currLocRateLine = null;
            BuyingRate _currLocBuyingRate = null;
            Rate _result = null;
            try
            {
                if (_locShipmentType != ShipmentType.None && _locRouting != null && _locCarrier != null && _locWorkOrder != null && _locDeliveryType != null &&
                    _locTransportType != null && _locContainerType != null && _locVehicleType != null &&
                    _locTransactionTerm != TransactionTerm.None && _currLocPriceType != CustomProcess.PriceType.None && _locActualQuantity > 0
                    && _locUOM != null)
                {
                    _fullNameRoutes = GetFullRoute(_currSession, _locShipmentType, _locRouting, _locCarrier, _locWorkOrder, _locDeliveryType,
                                                   _locTransportType, _locContainerType, _locVehicleType, _locTransactionTerm,
                                                   _currLocPriceType, _locActualQuantity, _locUOM);
                    if (_fullNameRoutes != null)
                    {
                        _currLocBuyingRate = _currSession.FindObject<BuyingRate>(new GroupOperator(GroupOperatorType.And,
                                                                          new BinaryOperator("Vendor", _locCarrier)));
                        if (_currLocBuyingRate != null)
                        {
                            _currLocRateLine = _currSession.FindObject<Rate>
                                                   (new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("ShipmentType", _locShipmentType),
                                                    new BinaryOperator("PriceType", _currLocPriceType),
                                                    new BinaryOperator("BuyingRate", _currLocBuyingRate),
                                                    new BinaryOperator("TransportType", _locTransportType),
                                                    new BinaryOperator("DeliveryType", _locDeliveryType),
                                                    new BinaryOperator("TransactionType", _locTransactionTerm),
                                                    new BinaryOperator("ContainerType", _locContainerType),
                                                    new BinaryOperator("VehicleType", _locVehicleType),
                                                    new BinaryOperator("Quantity", _locActualQuantity),
                                                    new BinaryOperator("UOM", _locUOM),
                                                    new BinaryOperator("Active", true),
                                                    new BinaryOperator("FullName", _fullNameRoutes)));

                            if (_currLocRateLine != null)
                            {
                                _result = _currLocRateLine;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = Routing " + ex.ToString());
            }
            return _result;
        }

        private string GetFullRoute(Session _currSession, ShipmentType _locShipmentType, Routing _locRouting, BusinessPartner _locCarrier,
                                    String _locWorkOrder, DeliveryType _locDeliveryType, TransportType _locTransportType, ContainerType _locContainerType,
                                    VehicleType _locVehicleType, TransactionTerm _locTransactionTerm,
                                    PriceType _locPriceType, double _locActualQuantity, UnitOfMeasure _locUOM)
        {
            string _locFullString = "";
            string _locBegin = "";
            string _locMiddle = "";
            string _loEnding = "";
            int _locMinRecord = 0;
            int _locMaxRecord = 0;

            try
            {

                XPCollection<RoutingLine> _dataLines = null;

                if ((_locShipmentType != ShipmentType.None || _locShipmentType != ShipmentType.ExportAndImport) &&_locRouting != null && _locCarrier != null && 
                    _locWorkOrder != null && _locDeliveryType != null && _locTransportType != null && _locContainerType != null && _locVehicleType != null &&
                    _locTransactionTerm != TransactionTerm.None && _locPriceType != CustomProcess.PriceType.None && _locActualQuantity > 0
                    && _locUOM != null)
                {
                    _dataLines = GetRoutings(_currSession, _locShipmentType, _locRouting, _locCarrier, _locWorkOrder, _locDeliveryType,
                                             _locTransportType, _locContainerType, _locVehicleType,
                                             _locPriceType, _locActualQuantity, _locUOM);

                    if (_dataLines != null)
                    {

                        _locMinRecord = GetMinNo(_currSession, _locShipmentType, _locRouting, _locCarrier, _locWorkOrder, _locDeliveryType,
                                                        _locTransportType, _locContainerType, _locVehicleType);
                        _locMaxRecord = GetMaxNo(_currSession, _locShipmentType, _locRouting, _locCarrier, _locWorkOrder, _locDeliveryType,
                                                        _locTransportType, _locContainerType, _locVehicleType);

                        foreach (RoutingLine _dataLine in _dataLines)
                        {
                            if (_dataLine.No == _locMinRecord)
                            {
                                _locBegin = _dataLine.AreaFrom.Name;
                                _locMiddle = _dataLine.AreaTo.Name;
                            }
                            else if (_dataLine.No <= _locMaxRecord)
                            {
                                _loEnding = _loEnding + _dataLine.AreaTo.Name;
                            }
                        }

                        _locFullString = String.Format("{0}{1}{2}", _locBegin, _locMiddle, _loEnding).Trim().Replace(" ", "").ToLower();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = Routing " + ex.ToString());
            }
            return _locFullString;
        }

      
        private XPCollection<RoutingLine> GetRoutings(Session _currSession, ShipmentType _locShipmentType, Routing _locRouting, BusinessPartner _locCarrier,
                                         String _locWorkOrder, DeliveryType _locDeliveryType, TransportType _locTransportType,
                                         ContainerType _locContainerType, VehicleType _locVehicleType, PriceType _locPriceType,
                                         double _locActualQuantity, UnitOfMeasure _locUOM)
        {
            XPCollection<RoutingLine> _results = null;
            XPCollection<RoutingLine> _dataLines = null;
            try
            {
                string _fullString = null;
                string _locStrShipmentType = null;
                string _locStrRouting = null;
                string _locStrCarrier = null;
                string _locStrWorkOrder = null;
                string _locStrDeliveryType = null;
                string _locStrTransportType = null;
                string _locStrContainerType = null;
                string _locStrVehicleType = null;
                string _locStrPriceType = null;
                string _locStrActualQuantity = null;
                string _locStrUOM = null;


                if (_locShipmentType != ShipmentType.None)
                { _locStrShipmentType = "[ShipmentType.Code]=='" + GetShipmentType(_locShipmentType).ToString() + "'"; }
                else { _locStrShipmentType = ""; }

                if ( _locRouting != null && _locShipmentType != ShipmentType.None)
                { _locStrRouting = "AND [Routing.Code]=='" + _locRouting.Code + "'"; }
                else if (_locRouting != null && _locShipmentType == ShipmentType.None)
                { _locStrRouting = "[Routing.Code]=='" + _locRouting.Code + "'"; }
                else { _locStrRouting = ""; }

                if (_locCarrier != null && (_locShipmentType != ShipmentType.None || _locRouting != null))
                { _locStrCarrier = "AND [Carrier.Code]=='" + _locCarrier.Code + "'"; }
                else if (_locCarrier != null && _locShipmentType == ShipmentType.None && _locRouting == null)
                { _locStrCarrier = " [Carrier.Code]=='" + _locCarrier.Code + "'"; }
                else { _locStrCarrier = ""; }

                if (_locWorkOrder != null && (_locShipmentType != ShipmentType.None || _locRouting != null || _locCarrier != null))
                { _locStrWorkOrder = "AND [WorkOrder]=='" + _locWorkOrder + "'"; }
                else if (_locWorkOrder != null && _locShipmentType == ShipmentType.None && _locRouting == null && _locCarrier == null)
                { _locStrWorkOrder = " [WorkOrder]=='" + _locWorkOrder + "'"; }
                else { _locStrWorkOrder = ""; }

                if (_locDeliveryType != null && (_locShipmentType != ShipmentType.None || _locRouting != null || _locCarrier != null || _locWorkOrder != null))
                { _locStrDeliveryType = "AND [DeliveryType.Code]=='" + _locDeliveryType.Code + "'"; }
                else if (_locDeliveryType != null && _locShipmentType == ShipmentType.None && _locRouting == null && _locCarrier == null && _locWorkOrder == null)
                { _locStrDeliveryType = " [DeliveryType.Code]=='" + _locDeliveryType.Code + "'"; }
                else { _locStrDeliveryType = ""; }

                if (_locTransportType != null && (_locShipmentType != ShipmentType.None || _locRouting != null || _locCarrier != null || _locWorkOrder != null || _locDeliveryType != null))
                { _locStrTransportType = "AND [TransportType.Code]=='" + _locTransportType.Code + "'"; }
                else if (_locTransportType != null && _locShipmentType == ShipmentType.None && _locRouting == null && _locCarrier == null && _locWorkOrder == null && _locDeliveryType == null)
                { _locStrTransportType = " [TransportType.Code]=='" + _locTransportType.Code + "'"; }
                else { _locStrTransportType = ""; }

                if (_locContainerType != null && (_locShipmentType != ShipmentType.None || _locRouting != null || _locCarrier != null || _locWorkOrder != null || _locDeliveryType != null
                    || _locTransportType != null))
                { _locStrContainerType = "AND [ContainerType.Code]=='" + _locContainerType.Code + "'"; }
                else if (_locContainerType != null && _locShipmentType == ShipmentType.None && _locRouting == null && _locCarrier == null && _locWorkOrder == null && _locDeliveryType == null
                    && _locTransportType == null)
                { _locStrContainerType = " [ContainerType.Code]=='" + _locContainerType.Code + "'"; }
                else { _locStrContainerType = ""; }

                if (_locVehicleType != null && (_locShipmentType != ShipmentType.None || _locRouting != null || _locCarrier != null || _locWorkOrder != null || _locDeliveryType != null
                    || _locTransportType != null || _locContainerType != null))
                { _locStrVehicleType = "AND [VehicleType.Code]=='" + _locVehicleType.Code + "'"; }
                else if (_locVehicleType != null && _locShipmentType == ShipmentType.None && _locRouting == null && _locCarrier == null && _locWorkOrder == null && _locDeliveryType == null
                    && _locTransportType == null && _locContainerType == null)
                { _locStrVehicleType = " [VehicleType.Code]=='" + _locVehicleType.Code + "'"; }
                else { _locStrVehicleType = ""; }

                if (_locPriceType != PriceType.None && (_locShipmentType != ShipmentType.None || _locRouting != null || _locCarrier != null || _locWorkOrder != null || _locDeliveryType != null
                    || _locTransportType != null || _locContainerType != null || _locVehicleType != null))
                { _locStrPriceType = "AND [PriceType]=='" + GetPriceType(_locPriceType).ToString() + "'"; }
                else if (_locPriceType != PriceType.None && _locShipmentType == ShipmentType.None && _locRouting == null && _locCarrier == null && _locWorkOrder == null && _locDeliveryType == null
                    && _locTransportType == null && _locContainerType == null && _locVehicleType == null)
                { _locStrPriceType = " [PriceType]=='" + GetPriceType(_locPriceType).ToString() + "'"; }
                else { _locStrPriceType = ""; }

                if (_locActualQuantity >= 0 && (_locShipmentType != ShipmentType.None || _locRouting != null || _locCarrier != null || _locWorkOrder != null || _locDeliveryType != null
                    || _locTransportType != null || _locContainerType != null || _locVehicleType != null || _locPriceType != PriceType.None))
                { _locStrActualQuantity = "AND [ActualQuantity]=='" + _locActualQuantity.ToString() + "'"; }
                else if (_locActualQuantity >= 0 && _locShipmentType == ShipmentType.None && _locRouting == null && _locCarrier == null && _locWorkOrder == null && _locDeliveryType == null
                    && _locTransportType == null && _locContainerType == null && _locVehicleType == null && _locPriceType == PriceType.None)
                { _locStrActualQuantity = " [ActualQuantity]=='" + _locActualQuantity.ToString() + "'"; }
                else { _locStrActualQuantity = ""; }

                if (_locUOM != null && (_locShipmentType != ShipmentType.None || _locRouting != null || _locCarrier != null || _locWorkOrder != null || _locDeliveryType != null
                    || _locTransportType != null || _locContainerType != null || _locVehicleType != null || _locPriceType != PriceType.None || _locActualQuantity > 0))
                { _locStrUOM = "AND [UOM.Code]=='" + _locUOM.Code + "'"; }
                else if (_locUOM != null && _locShipmentType == ShipmentType.None && _locRouting == null && _locCarrier == null && _locWorkOrder == null && _locDeliveryType == null
                    && _locTransportType == null && _locContainerType == null && _locVehicleType == null && _locPriceType == PriceType.None && _locActualQuantity == 0)
                { _locStrUOM = " [UOM.Code]=='" + _locUOM.Code + "'"; }
                else { _locStrUOM = ""; }

                
                //harus membuat function GetPriceType
                if (_locStrShipmentType != null || _locStrRouting != null || _locStrCarrier != null || _locStrWorkOrder != null || _locStrDeliveryType != null 
                    || _locStrTransportType != null || _locStrContainerType != null || _locStrVehicleType != null || _locStrPriceType != null 
                    || _locStrActualQuantity != null || _locStrUOM != null)
                {
                    _fullString = _locStrShipmentType + _locStrRouting + _locStrCarrier + _locStrWorkOrder + _locStrDeliveryType + _locStrTransportType + 
                        _locStrContainerType + _locStrVehicleType + _locStrPriceType + _locStrActualQuantity + _locStrUOM +" [Select]== '1'";
                }


                _dataLines = new XPCollection<RoutingLine>(_currSession, CriteriaOperator.Parse(_fullString));

                if (_dataLines != null && _dataLines.Count > 0)
                {
                    _results = _dataLines;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = Routing " + ex.ToString());
            }
            return _results;
        }

        private int GetMaxNo(Session _currSession, ShipmentType _locShipmentType ,Routing _locRouting, BusinessPartner _locCarrier,
                                    String _locWorkOrder, DeliveryType _locDeliveryType,
                                    TransportType _locTransportType, ContainerType _locContainerType,
                                    VehicleType _locVehicleType)
        {
            int result = 0;
            try
            {
                object _maxRecord = null;

                string _locQueryCriteriaOperator = "ShipmentType=? and Routing=? and Carrier=? and WorkOrder=? and DeliveryType=? and TransportType=? and ContainerType=? and VehicleType=? and Select=?";

                if (_locRouting != null && _locTransportType != null && _locDeliveryType != null)
                {
                    _maxRecord = _currSession.Evaluate<RoutingLine>(CriteriaOperator.Parse("Max(No)"),
                                    CriteriaOperator.Parse(_locQueryCriteriaOperator, _locShipmentType, _locRouting, _locCarrier, _locWorkOrder,
                                                           _locDeliveryType, _locTransportType, _locContainerType, _locVehicleType, true));

                    if (_maxRecord != null)
                    {
                        result = Convert.ToInt32(_maxRecord);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = Routing " + ex.ToString());
            }
            return result;
        }

        private int GetMinNo(Session _currSession, ShipmentType _locShipmentType ,Routing _locRouting, BusinessPartner _locCarrier,
                                    String _locWorkOrder, DeliveryType _locDeliveryType,
                                    TransportType _locTransportType, ContainerType _locContainerType,
                                    VehicleType _locVehicleType)
        {
            int result = 0;
            try
            {
                object _minRecord = null;
                string _locQueryCriteriaOperator = "ShipmentType=? and Routing=? and Carrier=? and WorkOrder=? and DeliveryType=? and TransportType=? and ContainerType=? and VehicleType=? and Select=?";

                if (_locRouting != null && _locTransportType != null && _locDeliveryType != null)
                {
                    _minRecord = _currSession.Evaluate<RoutingLine>(CriteriaOperator.Parse("Min(No)"),
                                    CriteriaOperator.Parse(_locQueryCriteriaOperator, _locShipmentType, _locRouting, _locCarrier, _locWorkOrder,
                                                           _locDeliveryType, _locTransportType, _locContainerType, _locVehicleType, true));

                    if (_minRecord != null)
                    {
                        result = Convert.ToInt32(_minRecord);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = Routing " + ex.ToString());
            }
            return result;
        }

        public int GetPriceType(PriceType objectName)
        {
            int _result = 0;
            try
            {
                if (objectName == PriceType.Normal)
                {
                    _result = 1;
                }
                else if (objectName == PriceType.Weight)
                {
                    _result = 2;
                }
                else if (objectName == PriceType.Distance)
                {
                    _result = 3;
                }
                else
                {
                    _result = 0;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = Routing " + ex.ToString());
            }
            return _result;
        }

        public int GetShipmentType(ShipmentType objectName)
        {
            int _result = 0;
            try
            {
                if (objectName == ShipmentType.Import)
                {
                    _result = 1;
                }
                else if (objectName == ShipmentType.Export)
                {
                    _result = 2;
                }
                else if (objectName == ShipmentType.ExportAndImport)
                {
                    _result = 3;
                }
                else if (objectName == ShipmentType.DomesticDelivery)
                {
                    _result = 3;
                }
                else
                {
                    _result = 0;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = Routing " + ex.ToString());
            }
            return _result;
        }

        #endregion PostingRoute

        #region PostingPackage

        private void SetShipmentSalesMonitoring(Session _currSession, Routing _locRoutingXPO)
        {
            try
            {
                if(_locRoutingXPO != null)
                {
                    XPCollection<RoutingPackage> _locRoutingPackages = new XPCollection<RoutingPackage>
                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Routing", _locRoutingXPO),
                                                                    new BinaryOperator("Select", true),
                                                                    new BinaryOperator("Status", Status.Chargeable)));
                    if(_locRoutingPackages != null && _locRoutingPackages.Count() > 0)
                    {
                        foreach(RoutingPackage _locRoutingPackage in _locRoutingPackages)
                        {
                            ShipmentSalesMonitoring _saveDataShipmentSalesInvoice = new ShipmentSalesMonitoring(_currSession)
                            {
                                Routing = _locRoutingXPO,
                                RoutingPackage = _locRoutingPackage,
                                ShipmentType = _locRoutingPackage.ShipmentType,
                                TransportType = _locRoutingPackage.TransportType,
                                DeliveryType = _locRoutingPackage.DeliveryType,
                                ContainerType = _locRoutingPackage.ContainerType,
                                TransactionTerm = _locRoutingPackage.TransactionTerm,
                                CountryFrom = _locRoutingPackage.CountryFrom,
                                CityFrom = _locRoutingPackage.CityFrom,
                                AreaFrom = _locRoutingPackage.AreaFrom,
                                LocationFrom = _locRoutingPackage.LocationFrom,
                                TransportLocationFrom = _locRoutingPackage.TransportLocationFrom,
                                CountryTo = _locRoutingPackage.CountryTo,
                                CityTo = _locRoutingPackage.CityTo,
                                AreaTo = _locRoutingPackage.AreaTo,
                                LocationTo = _locRoutingPackage.LocationTo,
                                TransportLocationTo = _locRoutingPackage.TransportLocationTo,
                                Shipper = _locRoutingPackage.Shipper,
                                Consignee = _locRoutingPackage.Consignee,
                                BillToBusinessPartner = _locRoutingPackage.BillToCustomer,
                                PriceGroup = _locRoutingPackage.PriceGroup,
                                PriceType = _locRoutingPackage.PriceType,
                                ItemGroup = _locRoutingPackage.ItemGroup,
                                Item = _locRoutingPackage.Item,
                                Name = _locRoutingPackage.Name,
                                Description = _locRoutingPackage.Description,
                                DQty = _locRoutingPackage.DQty,
                                DUOM = _locRoutingPackage.DUOM,
                                Qty = _locRoutingPackage.Qty,
                                UOM = _locRoutingPackage.UOM,
                                TQty = _locRoutingPackage.TQty,
                                Amount = _locRoutingPackage.Amount,

                            };
                            _saveDataShipmentSalesInvoice.Save();
                            _saveDataShipmentSalesInvoice.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = Routing " + ex.ToString());
            }
            
        }

        #endregion PostingPackage

        #region Global

        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0} !", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }


        #endregion Global

        

        
    }
}
