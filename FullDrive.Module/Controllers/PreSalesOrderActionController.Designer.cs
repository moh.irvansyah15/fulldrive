﻿namespace FullDrive.Module.Controllers
{
    partial class PreSalesOrderActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.PreSalesOrderProgressAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PreSalesOrderPostingAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PreSalesOrderListviewFilterSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // PreSalesOrderProgressAction
            // 
            this.PreSalesOrderProgressAction.Caption = "Progress";
            this.PreSalesOrderProgressAction.ConfirmationMessage = null;
            this.PreSalesOrderProgressAction.Id = "PreSalesOrderProgressActionId";
            this.PreSalesOrderProgressAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PreSalesOrder);
            this.PreSalesOrderProgressAction.ToolTip = null;
            this.PreSalesOrderProgressAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PreSalesOrderProgressAction_Execute);
            // 
            // PreSalesOrderPostingAction
            // 
            this.PreSalesOrderPostingAction.Caption = "Posting";
            this.PreSalesOrderPostingAction.ConfirmationMessage = null;
            this.PreSalesOrderPostingAction.Id = "PreSalesOrderPostingActionId";
            this.PreSalesOrderPostingAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PreSalesOrder);
            this.PreSalesOrderPostingAction.ToolTip = null;
            this.PreSalesOrderPostingAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PreSalesOrderPostingAction_Execute);
            // 
            // PreSalesOrderListviewFilterSelectionAction
            // 
            this.PreSalesOrderListviewFilterSelectionAction.Caption = "Filter";
            this.PreSalesOrderListviewFilterSelectionAction.ConfirmationMessage = null;
            this.PreSalesOrderListviewFilterSelectionAction.Id = "PreSalesOrderListviewFilterSelectionActionId";
            this.PreSalesOrderListviewFilterSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.PreSalesOrderListviewFilterSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PreSalesOrder);
            this.PreSalesOrderListviewFilterSelectionAction.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.PreSalesOrderListviewFilterSelectionAction.ToolTip = null;
            this.PreSalesOrderListviewFilterSelectionAction.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.PreSalesOrderListviewFilterSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.PreSalesOrderListviewFilterSelectionAction_Execute);
            // 
            // PreSalesOrderActionController
            // 
            this.Actions.Add(this.PreSalesOrderProgressAction);
            this.Actions.Add(this.PreSalesOrderPostingAction);
            this.Actions.Add(this.PreSalesOrderListviewFilterSelectionAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction PreSalesOrderProgressAction;
        private DevExpress.ExpressApp.Actions.SimpleAction PreSalesOrderPostingAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction PreSalesOrderListviewFilterSelectionAction;
    }
}
