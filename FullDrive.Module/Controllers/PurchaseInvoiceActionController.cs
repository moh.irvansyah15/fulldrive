﻿#region Default

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

#endregion Default

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;
using System.IO;
using System.Web;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class PurchaseInvoiceActionController : ViewController
    {
        #region Default

        private ChoiceActionItem _selectionListviewFilter;
        private ChoiceActionItem _setApprovalLevel;

        public PurchaseInvoiceActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            PurchaseInvoiceListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                PurchaseInvoiceListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
        }

        protected override void OnActivated()
        {
            TypeOfView = typeof(ObjectView);
            base.OnActivated();
            // Perform various tasks depending on the target View.
            #region Approval
            var _locUser = SecuritySystem.CurrentUserName;
            GlobalFunction _globalFunction = new GlobalFunction();
            Session _currentSession = null;
            if (this.ObjectSpace != null)
            {
                _currentSession = ((XPObjectSpace)this.ObjectSpace).Session;
            }

            if (_currentSession != null)
            {
                UserAccess _locUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", _locUser));

                if (_locUserAccess != null)
                {
                    PurchaseInvoiceApprovalAction.Items.Clear();

                    XPCollection<ApplicationSetupDetail> _locAppSetupDetails = new XPCollection<ApplicationSetupDetail>
                                                                                 (_currentSession, new GroupOperator(GroupOperatorType.And,
                                                                                  new BinaryOperator("UserAccess", _locUserAccess),
                                                                                  new BinaryOperator("ObjectList", CustomProcess.ObjectList.PurchaseInvoice),
                                                                                  new BinaryOperator("FunctionList", CustomProcess.FunctionList.Approval),
                                                                                  new BinaryOperator("Active", true)));

                    if (_locAppSetupDetails != null && _locAppSetupDetails.Count > 0)
                    {

                        foreach (ApplicationSetupDetail _locAppSetupDetail in _locAppSetupDetails)
                        {
                            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.ApprovalLevel)))
                            {
                                if (_locAppSetupDetail.ApprovalLevel != CustomProcess.ApprovalLevel.None)
                                {
                                    EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.ApprovalLevel));
                                    _setApprovalLevel = new ChoiceActionItem(_ed.GetCaption(_locAppSetupDetail.ApprovalLevel), _locAppSetupDetail.ApprovalLevel);
                                }
                            }
                            PurchaseInvoiceApprovalAction.Items.Add(_setApprovalLevel);
                        }
                    }
                }
            }
            #endregion Approval
        }

        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }

        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        #endregion Default

        private void PurchaseInvoiceProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                Status _locStatus = Status.Open;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        PurchaseInvoice _locPurchaseInvoiceOS = (PurchaseInvoice)_objectSpace.GetObject(obj);

                        if (_locPurchaseInvoiceOS != null)
                        {
                            if (_locPurchaseInvoiceOS.Code != null)
                            {
                                _currObjectId = _locPurchaseInvoiceOS.Code;

                                PurchaseInvoice _locPurchaseInvoiceXPO = _currSession.FindObject<PurchaseInvoice>
                                                                   (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId)));

                                if (_locPurchaseInvoiceXPO != null)
                                {
                                    if (_locPurchaseInvoiceXPO.Status == Status.Open || _locPurchaseInvoiceXPO.Status == Status.Progress || _locPurchaseInvoiceXPO.Status == Status.Posted)
                                    {
                                        if (_locPurchaseInvoiceXPO.Status == Status.Open || _locPurchaseInvoiceXPO.Status == Status.Progress)
                                        {
                                            _locStatus = Status.Progress;

                                        }
                                        else if (_locPurchaseInvoiceXPO.Status == Status.Posted)
                                        {
                                            _locStatus = Status.Posted;
                                        }

                                        _locPurchaseInvoiceXPO.Status = _locStatus;
                                        _locPurchaseInvoiceXPO.StatusDate = now;
                                        _locPurchaseInvoiceXPO.Save();
                                        _locPurchaseInvoiceXPO.Session.CommitTransaction();

                                        #region PurchaseInvoiceLine
                                        XPCollection<PurchaseInvoiceLine> _locPurchaseInvoiceLines = new XPCollection<PurchaseInvoiceLine>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO)));

                                        if (_locPurchaseInvoiceLines != null && _locPurchaseInvoiceLines.Count > 0)
                                        {
                                            foreach (PurchaseInvoiceLine _locPurchaseInvoiceLine in _locPurchaseInvoiceLines)
                                            {
                                                if (_locPurchaseInvoiceLine.Status == Status.Open)
                                                {
                                                    _locPurchaseInvoiceLine.Status = Status.Progress;
                                                    _locPurchaseInvoiceLine.StatusDate = now;
                                                    _locPurchaseInvoiceLine.Save();
                                                    _locPurchaseInvoiceLine.Session.CommitTransaction();
                                                }
                                            }
                                        }
                                        #endregion PurchaseInvoiceLine

                                        #region InvoicePurchaseCollection
                                        {
                                            XPCollection<InvoicePurchaseCollection> _locInvoicePurchaseCollections = new XPCollection<BusinessObjects.InvoicePurchaseCollection>
                                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                            new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO)));
                                            if (_locInvoicePurchaseCollections != null && _locInvoicePurchaseCollections.Count() > 0)
                                            {
                                                foreach (InvoicePurchaseCollection _locInvoicePurchaseCollection in _locInvoicePurchaseCollections)
                                                {
                                                    _locInvoicePurchaseCollection.Status = Status.Progress;
                                                    _locInvoicePurchaseCollection.StatusDate = now;
                                                    _locInvoicePurchaseCollection.Save();
                                                    _locInvoicePurchaseCollection.Session.CommitTransaction();
                                                }
                                            }
                                        }
                                        #endregion InvoicePurchaseCollection

                                        SuccessMessageShow(_locPurchaseInvoiceXPO.Code + " has been change successfully to Progress ");
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data Purchase Invoice Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Purchase Invoice Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseInvoice " + ex.ToString());
            }
        }

        private void PurchaseInvoiceGetPayAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                double _locMaxPay = 0;
                double _locTotalPay = 0;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        PurchaseInvoice _locPurchaseInvoiceOS = (PurchaseInvoice)_objectSpace.GetObject(obj);

                        if (_locPurchaseInvoiceOS != null)
                        {
                            if (_locPurchaseInvoiceOS.Code != null)
                            {
                                _currObjectId = _locPurchaseInvoiceOS.Code;

                                PurchaseInvoice _locPurchaseInvoiceXPO = _currSession.FindObject<PurchaseInvoice>
                                                                   (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId)));

                                if (_locPurchaseInvoiceXPO != null)
                                {
                                    _locMaxPay = _locPurchaseInvoiceXPO.Outstanding;

                                    if (_locPurchaseInvoiceXPO.Status == Status.Progress || _locPurchaseInvoiceXPO.Status == Status.Posted)
                                    {
                                        ApprovalLine _locApprovalLineXPO = _currSession.FindObject<ApprovalLine>
                                                                           (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("EndApproval", true),
                                                                            new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO)));

                                        if (_locApprovalLineXPO == null)
                                        {
                                            GetPay(_currSession, _locPurchaseInvoiceXPO);

                                            XPCollection<PurchaseInvoiceLine> _locPurchaseInvoiceLines = new XPCollection<PurchaseInvoiceLine>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO),
                                                                                                new BinaryOperator("Select", true)));

                                            if (_locPurchaseInvoiceLines != null && _locPurchaseInvoiceLines.Count > 0)
                                            {
                                                foreach (PurchaseInvoiceLine _locPurchaseInvoiceLine in _locPurchaseInvoiceLines)
                                                {
                                                    if (_locPurchaseInvoiceLine.Status == Status.Lock)
                                                    {
                                                        _locTotalPay = _locTotalPay + _locPurchaseInvoiceLine.Pay;
                                                    }
                                                }
                                            }
                                        }

                                        SuccessMessageShow(_locPurchaseInvoiceXPO.Code + "Has been change successfully to Get Max Pay");
                                    }



                                    _locPurchaseInvoiceXPO.MaxPay = _locTotalPay;
                                    _locPurchaseInvoiceXPO.Pay = _locTotalPay;
                                    _locPurchaseInvoiceXPO.Status = Status.Lock;
                                    _locPurchaseInvoiceXPO.StatusDate = now;
                                    _locPurchaseInvoiceXPO.Save();
                                    _locPurchaseInvoiceXPO.Session.CommitTransaction();
                                }
                                else
                                {
                                    ErrorMessageShow("Data Purchase Invoice Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Purchase Invoice Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseInvoice " + ex.ToString());
            }
        }

        private void PurchaseInvoicePostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                //Mengirim informasi ke Payable Transaction --HutangDagang Dan --BankAccount Perusahaan yg aktif default
                //Meng-akumulasi semua hal perhitungan lokal Purchase Invoice termasuk close status

                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        PurchaseInvoice _locPurchaseInvoiceOS = (PurchaseInvoice)_objectSpace.GetObject(obj);

                        if (_locPurchaseInvoiceOS != null)
                        {
                            if (_locPurchaseInvoiceOS.Code != null)
                            {
                                _currObjectId = _locPurchaseInvoiceOS.Code;

                                PurchaseInvoice _locPurchaseInvoiceXPO = _currSession.FindObject<PurchaseInvoice>
                                                                   (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId)));

                                if (_locPurchaseInvoiceXPO != null)
                                {
                                    if (_locPurchaseInvoiceXPO.Status == Status.Lock || _locPurchaseInvoiceXPO.Status == Status.Posted)
                                    {
                                        ApprovalLine _locApprovalLineXPO = _currSession.FindObject<ApprovalLine>
                                                                           (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("EndApproval", true),
                                                                            new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO)));

                                        if (_locApprovalLineXPO != null)
                                        {
                                            if (_locPurchaseInvoiceXPO.Pay > 0)
                                            {
                                                //Berdasarkan Outstanding dari PaymentOutPlan yg mana di hitung berdasarkan jumlah mak Outstanding 
                                                if (_locPurchaseInvoiceXPO.Pay == GetPurchaseInvoiceLineTotalAmount(_currSession, _locPurchaseInvoiceXPO))
                                                {
                                                    SetPurchaseInvoiceMonitoring(_currSession, _locPurchaseInvoiceXPO);
                                                    SetReverseGoodsReceiveJournal(_currSession, _locPurchaseInvoiceXPO);
                                                    SetInvoiceAPJournal(_currSession, _locPurchaseInvoiceXPO);
                                                    SetAfterPostingReport(_currSession, _locPurchaseInvoiceXPO);
                                                    SetRemainQty(_currSession, _locPurchaseInvoiceXPO);
                                                    SetPostingQty(_currSession, _locPurchaseInvoiceXPO);
                                                    SetProcessCount(_currSession, _locPurchaseInvoiceXPO);
                                                    SetStatusPurchaseInvoiceLine(_currSession, _locPurchaseInvoiceXPO);
                                                    SetNormalPay(_currSession, _locPurchaseInvoiceXPO);
                                                    SetNormalQuantity(_currSession, _locPurchaseInvoiceXPO);
                                                    SetFinalPurchaseInvoice(_currSession, _locPurchaseInvoiceXPO);
                                                    SuccessMessageShow(_locPurchaseInvoiceXPO.Code + " has been change successfully to Posted");
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data Purchase Invoice Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Purchase Invoice Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseInvoice " + ex.ToString());
            }
        }

        private void PurchaseInvoiceGetITIAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;


                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        PurchaseInvoice _locPurchaseInvoiceOS = (PurchaseInvoice)_objectSpace.GetObject(obj);

                        if (_locPurchaseInvoiceOS != null)
                        {
                            if (_locPurchaseInvoiceOS.Code != null)
                            {
                                _currObjectId = _locPurchaseInvoiceOS.Code;

                                PurchaseInvoice _locPurchaseInvoiceXPO = _currSession.FindObject<PurchaseInvoice>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locPurchaseInvoiceXPO != null)
                                {
                                    if (_locPurchaseInvoiceXPO.Status == Status.Open || _locPurchaseInvoiceXPO.Status == Status.Progress)
                                    {
                                        XPCollection<InvoicePurchaseCollection> _locInvoicePurchaseCollections = new XPCollection<InvoicePurchaseCollection>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO),
                                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("Status", Status.Open),
                                                                                        new BinaryOperator("Status", Status.Progress))));

                                        if (_locInvoicePurchaseCollections != null && _locInvoicePurchaseCollections.Count() > 0)
                                        {
                                            foreach (InvoicePurchaseCollection _locInvoicePurchaseCollection in _locInvoicePurchaseCollections)
                                            {
                                                if (_locInvoicePurchaseCollection.PurchaseInvoice != null && _locInvoicePurchaseCollection.InventoryTransferIn != null)
                                                {
                                                    GetInventoryTransferInMonitoring(_currSession, _locInvoicePurchaseCollection.InventoryTransferIn, _locPurchaseInvoiceXPO);
                                                }
                                            }
                                            SuccessMessageShow("Inventory Transfer In Has Been Successfully Getting into Purchase Invoice");
                                        }
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Inventory Transfer In Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Inventory Transfer In Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseInvoice " + ex.ToString());
            }
        }

        private void PurchaseInvoiceListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(PurchaseInvoice)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseInvoice " + ex.ToString());
            }
        }

        private void PurchaseInvoiceApprovalAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                ApplicationSetupDetail _locAppSetDetail = null;
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                var User = SecuritySystem.CurrentUserName;
                Session _currentSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currentSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                UserAccess _locUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", User));

                foreach (Object obj in _objectsToProcess)
                {
                    PurchaseInvoice _objInNewObjectSpace = (PurchaseInvoice)_objectSpace.GetObject(obj);

                    if (_objInNewObjectSpace != null)
                    {
                        if (_objInNewObjectSpace.Code != null)
                        {
                            _currObjectId = _objInNewObjectSpace.Code;
                        }
                    }

                    if (_currObjectId != null)
                    {
                        PurchaseInvoice _locPurchaseInvoiceXPO = _currentSession.FindObject<PurchaseInvoice>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("Code", _currObjectId)));

                        if (_locPurchaseInvoiceXPO != null)
                        {
                            if (_locPurchaseInvoiceXPO.Status == Status.Lock)
                            {
                                ApprovalLine _locApprovalLine = _currentSession.FindObject<ApprovalLine>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO),
                                                                             new BinaryOperator("EndApproval", true)));
                                if (_locApprovalLine == null)
                                {
                                    #region Approval Level 1
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level1)
                                    {
                                        _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level1, ObjectList.PurchaseInvoice);
                                        if (_locAppSetDetail != null)
                                        {
                                            //Buat bs input langsung ke approvalline
                                            ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level1)));
                                            if (_locApprovalLineXPO == null)
                                            {

                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level1,
                                                        EndApproval = true,
                                                        PurchaseInvoice = _locPurchaseInvoiceXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locPurchaseInvoiceXPO.ActivationPosting = true;
                                                    _locPurchaseInvoiceXPO.ActiveApproved1 = false;
                                                    _locPurchaseInvoiceXPO.ActiveApproved2 = false;
                                                    _locPurchaseInvoiceXPO.ActiveApproved3 = true;
                                                    _locPurchaseInvoiceXPO.Save();
                                                    _locPurchaseInvoiceXPO.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level1,
                                                        PurchaseInvoice = _locPurchaseInvoiceXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locPurchaseInvoiceXPO.ActiveApproved1 = true;
                                                    _locPurchaseInvoiceXPO.ActiveApproved2 = false;
                                                    _locPurchaseInvoiceXPO.ActiveApproved3 = false;
                                                    _locPurchaseInvoiceXPO.Save();
                                                    _locPurchaseInvoiceXPO.Session.CommitTransaction();
                                                }

                                                //Send Email
                                                ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO)));

                                                if (_locApprovalLineXPO2 != null)
                                                {
                                                    SendEmail(_currentSession, _locPurchaseInvoiceXPO, Status.Lock, _locApprovalLineXPO2);
                                                }

                                                SuccessMessageShow("Purchase Invoice has successfully Approve");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 1

                                    #region Approval Level 2
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level2)
                                    {
                                        _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level2, ObjectList.PurchaseInvoice);

                                        if (_locAppSetDetail != null)
                                        {
                                            ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level2)));

                                            if (_locApprovalLineXPO == null)
                                            {
                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level2,
                                                        EndApproval = true,
                                                        PurchaseInvoice = _locPurchaseInvoiceXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locPurchaseInvoiceXPO.ActivationPosting = true;
                                                    _locPurchaseInvoiceXPO.ActiveApproved1 = false;
                                                    _locPurchaseInvoiceXPO.ActiveApproved2 = false;
                                                    _locPurchaseInvoiceXPO.ActiveApproved3 = true;
                                                    _locPurchaseInvoiceXPO.Save();
                                                    _locPurchaseInvoiceXPO.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level2,
                                                        PurchaseInvoice = _locPurchaseInvoiceXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    SetApprovalLine(_currentSession, _locPurchaseInvoiceXPO, ApprovalLevel.Level1);

                                                    _locPurchaseInvoiceXPO.ActiveApproved1 = false;
                                                    _locPurchaseInvoiceXPO.ActiveApproved2 = true;
                                                    _locPurchaseInvoiceXPO.ActiveApproved3 = false;
                                                    _locPurchaseInvoiceXPO.Save();
                                                    _locPurchaseInvoiceXPO.Session.CommitTransaction();
                                                }

                                                //Send Email
                                                ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO)));

                                                if (_locApprovalLineXPO2 != null)
                                                {
                                                    SendEmail(_currentSession, _locPurchaseInvoiceXPO, Status.Lock, _locApprovalLineXPO2);
                                                }

                                                SuccessMessageShow("Purchase Invoice has successfully Approve");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 2

                                    #region Approval Level 3
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level3)
                                    {
                                        _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level3, ObjectList.PurchaseInvoice);

                                        if (_locAppSetDetail != null)
                                        {
                                            ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level3)));

                                            if (_locApprovalLineXPO == null)
                                            {
                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level3,
                                                        EndApproval = true,
                                                        PurchaseInvoice = _locPurchaseInvoiceXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locPurchaseInvoiceXPO.ActivationPosting = true;
                                                    _locPurchaseInvoiceXPO.ActiveApproved1 = false;
                                                    _locPurchaseInvoiceXPO.ActiveApproved2 = false;
                                                    _locPurchaseInvoiceXPO.ActiveApproved3 = true;
                                                    _locPurchaseInvoiceXPO.Save();
                                                    _locPurchaseInvoiceXPO.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level3,
                                                        PurchaseInvoice = _locPurchaseInvoiceXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    SetApprovalLine(_currentSession, _locPurchaseInvoiceXPO, ApprovalLevel.Level2);
                                                    SetApprovalLine(_currentSession, _locPurchaseInvoiceXPO, ApprovalLevel.Level1);

                                                    _locPurchaseInvoiceXPO.ActiveApproved1 = false;
                                                    _locPurchaseInvoiceXPO.ActiveApproved2 = false;
                                                    _locPurchaseInvoiceXPO.ActiveApproved3 = true;
                                                    _locPurchaseInvoiceXPO.Save();
                                                    _locPurchaseInvoiceXPO.Session.CommitTransaction();
                                                }

                                                //Send Email
                                                ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO)));

                                                if (_locApprovalLineXPO2 != null)
                                                {
                                                    SendEmail(_currentSession, _locPurchaseInvoiceXPO, Status.Lock, _locApprovalLineXPO2);
                                                }

                                                SuccessMessageShow("Purchase Invoice has successfully Approve");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 3
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Purchase Invoice Status Not Available");
                            }
                        }
                        else
                        {
                            ErrorMessageShow("Purchase Invoice Not Available");
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseInvoice " + ex.ToString());
            }
        }


        #region GetITI

        private void GetInventoryTransferInMonitoring(Session _currSession, InventoryTransferIn _locInventoryTransferInXPO, PurchaseInvoice _locPurchaseInvoiceXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _currSignCode = null;
                double _locTxValue = 0;
                double _locTxAmount = 0;
                double _locDisc = 0;
                double _locDiscAmount = 0;
                double _locTAmount = 0;

                if (_locInventoryTransferInXPO != null && _locPurchaseInvoiceXPO != null)
                {
                    //Harus mampu menghitung otomatis buat MultiTax dan MultiDiscount
                    XPCollection<InventoryTransferInMonitoring> _locInventoryTransferInMonitorings = new XPCollection<InventoryTransferInMonitoring>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("InventoryTransferIn", _locInventoryTransferInXPO),
                                                                                        new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO),
                                                                                        new BinaryOperator("Select", true),
                                                                                        new BinaryOperator("PostedCount", 0)));
                    if (_locInventoryTransferInMonitorings != null && _locInventoryTransferInMonitorings.Count() > 0)
                    {
                        foreach (InventoryTransferInMonitoring _locInventoryTransferInMonitoring in _locInventoryTransferInMonitorings)
                        {
                            if (_locInventoryTransferInMonitoring.InventoryTransferInLine != null && _locInventoryTransferInMonitoring.PurchaseOrderMonitoring != null
                                && _locInventoryTransferInMonitoring.PurchaseOrderMonitoring.PurchaseOrderLine != null)
                            {
                                _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.InventoryTransferInLine);

                                if (_currSignCode != null)
                                {
                                    //Count Tax
                                    if (_locInventoryTransferInMonitoring.PurchaseOrderMonitoring.MultiTax == true)
                                    {
                                        _locTxValue = GetSumTotalTaxValue(_currSession, _locInventoryTransferInMonitoring.PurchaseOrderMonitoring.PurchaseOrderLine);
                                        _locTxAmount = GetSumTotalTaxAmount(_currSession, _locInventoryTransferInMonitoring.PurchaseOrderMonitoring.PurchaseOrderLine, _locInventoryTransferInMonitoring);
                                    }
                                    else
                                    {
                                        if (_locInventoryTransferInMonitoring.PurchaseOrderMonitoring.TxValue > 0)
                                        {
                                            _locTxValue = _locInventoryTransferInMonitoring.PurchaseOrderMonitoring.PurchaseOrderLine.TxValue;
                                            _locTxAmount = (_locTxValue / 100) * (_locInventoryTransferInMonitoring.TQty * _locInventoryTransferInMonitoring.PurchaseOrderMonitoring.UAmount);
                                        }
                                    }

                                    //Count Discount
                                    if (_locInventoryTransferInMonitoring.PurchaseOrderMonitoring.MultiDiscount == true)
                                    {
                                        _locDisc = GetSumTotalDisc(_currSession, _locInventoryTransferInMonitoring.PurchaseOrderMonitoring.PurchaseOrderLine);
                                        _locDiscAmount = GetSumTotalDiscAmount(_currSession, _locInventoryTransferInMonitoring.PurchaseOrderMonitoring.PurchaseOrderLine, _locInventoryTransferInMonitoring);
                                    }
                                    else
                                    {
                                        if (_locInventoryTransferInMonitoring.PurchaseOrderMonitoring.Disc > 0)
                                        {
                                            _locDisc = _locInventoryTransferInMonitoring.PurchaseOrderMonitoring.PurchaseOrderLine.Disc;
                                            _locDiscAmount = (_locDisc / 100) * (_locInventoryTransferInMonitoring.TQty * _locInventoryTransferInMonitoring.PurchaseOrderMonitoring.UAmount);
                                        }
                                    }

                                    _locTAmount = (_locInventoryTransferInMonitoring.PurchaseOrderMonitoring.UAmount * _locInventoryTransferInMonitoring.TQty) + _locTxAmount - _locDiscAmount;
                                    //Tempat untuk menghitung multitax dan multidiscount
                                    PurchaseInvoiceLine _saveDataPurchaseInvoiceLine = new PurchaseInvoiceLine(_currSession)
                                    {
                                        SignCode = _currSignCode,
                                        PurchaseType = _locInventoryTransferInMonitoring.PurchaseOrderMonitoring.PurchaseOrderLine.PurchaseType,
                                        Item = _locInventoryTransferInMonitoring.Item,
                                        Description = _locInventoryTransferInMonitoring.InventoryTransferInLine.Description,
                                        MxDQty = _locInventoryTransferInMonitoring.DQty,
                                        MxDUOM = _locInventoryTransferInMonitoring.DUOM,
                                        MxQty = _locInventoryTransferInMonitoring.Qty,
                                        MxUOM = _locInventoryTransferInMonitoring.UOM,
                                        MxTQty = _locInventoryTransferInMonitoring.TQty,
                                        MxUAmount = _locInventoryTransferInMonitoring.PurchaseOrderMonitoring.UAmount,
                                        MxTUAmount = _locInventoryTransferInMonitoring.TQty * _locInventoryTransferInMonitoring.PurchaseOrderMonitoring.UAmount,
                                        DQty = _locInventoryTransferInMonitoring.DQty,
                                        DUOM = _locInventoryTransferInMonitoring.DUOM,
                                        Qty = _locInventoryTransferInMonitoring.Qty,
                                        UOM = _locInventoryTransferInMonitoring.UOM,
                                        TQty = _locInventoryTransferInMonitoring.TQty,
                                        Currency = _locInventoryTransferInMonitoring.PurchaseOrderMonitoring.Currency,
                                        UAmount = _locInventoryTransferInMonitoring.PurchaseOrderMonitoring.UAmount,
                                        TUAmount = _locInventoryTransferInMonitoring.TQty * _locInventoryTransferInMonitoring.PurchaseOrderMonitoring.UAmount,
                                        MultiTax = _locInventoryTransferInMonitoring.PurchaseOrderMonitoring.MultiTax,
                                        Tax = _locInventoryTransferInMonitoring.PurchaseOrderMonitoring.Tax,
                                        TxValue = _locTxValue,
                                        TxAmount = _locTxAmount,
                                        MultiDiscount = _locInventoryTransferInMonitoring.PurchaseOrderMonitoring.MultiDiscount,
                                        Discount = _locInventoryTransferInMonitoring.PurchaseOrderMonitoring.Discount,
                                        Disc = _locDisc,
                                        DiscAmount = _locDiscAmount,
                                        TAmount = _locTAmount,
                                        InventoryTransferInMonitoring = _locInventoryTransferInMonitoring,
                                        PurchaseOrderMonitoring = _locInventoryTransferInMonitoring.PurchaseOrderMonitoring,
                                        PurchaseInvoice = _locPurchaseInvoiceXPO,
                                    };
                                    _saveDataPurchaseInvoiceLine.Save();
                                    _saveDataPurchaseInvoiceLine.Session.CommitTransaction();

                                    PurchaseInvoiceLine _locPurchaseInvoiceLine = _currSession.FindObject<PurchaseInvoiceLine>
                                                                (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("SignCode", _currSignCode)));
                                    if (_locPurchaseInvoiceLine != null)
                                    {
                                        #region TaxLine
                                        if (_locInventoryTransferInMonitoring.PurchaseOrderMonitoring.PurchaseOrderLine.MultiTax == true)
                                        {
                                            XPCollection<TaxLine> _locTaxLines = new XPCollection<TaxLine>(_currSession, new GroupOperator(
                                                                                GroupOperatorType.And,
                                                                                new BinaryOperator("PurchaseOrderLine", _locInventoryTransferInMonitoring.PurchaseOrderMonitoring.PurchaseOrderLine)));
                                            if (_locTaxLines != null && _locTaxLines.Count() > 0)
                                            {
                                                foreach (TaxLine _locTaxLine in _locTaxLines)
                                                {
                                                    TaxLine _saveDataTaxLine = new TaxLine(_currSession)
                                                    {
                                                        Tax = _locTaxLine.Tax,
                                                        Name = _locTaxLine.Name,
                                                        TaxType = _locTaxLine.TaxType,
                                                        TaxGroup = _locTaxLine.TaxGroup,
                                                        TaxGroupType = _locTaxLine.TaxGroupType,
                                                        TaxAccountGroup = _locTaxLine.TaxAccountGroup,
                                                        TaxNature = _locTaxLine.TaxNature,
                                                        TaxMode = _locTaxLine.TaxMode,
                                                        TxValue = _locTaxLine.TxValue,
                                                        Description = _locTaxLine.Description,
                                                        UAmount = _locPurchaseInvoiceLine.UAmount,
                                                        TUAmount = _locPurchaseInvoiceLine.TUAmount,
                                                        TxAmount = _locPurchaseInvoiceLine.TxAmount,
                                                        PurchaseInvoiceLine = _locPurchaseInvoiceLine,
                                                    };
                                                    _saveDataTaxLine.Save();
                                                    _saveDataTaxLine.Session.CommitTransaction();
                                                }
                                            }
                                        }
                                        #endregion TaxLine

                                        #region DiscountLine
                                        if (_locInventoryTransferInMonitoring.PurchaseOrderMonitoring.PurchaseOrderLine.MultiDiscount == true)
                                        {
                                            XPCollection<DiscountLine> _locDiscountLines = new XPCollection<DiscountLine>(_currSession,
                                                                                new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("PurchaseOrderLine", _locInventoryTransferInMonitoring.PurchaseOrderMonitoring.PurchaseOrderLine)));
                                            if (_locDiscountLines != null && _locDiscountLines.Count() > 0)
                                            {
                                                foreach (DiscountLine _locDiscountLine in _locDiscountLines)
                                                {
                                                    DiscountLine _saveDataDiscountLine = new DiscountLine(_currSession)
                                                    {
                                                        Discount = _locDiscountLine.Discount,
                                                        Name = _locDiscountLine.Name,
                                                        DiscountMode = _locDiscountLine.DiscountMode,
                                                        Description = _locDiscountLine.Description,
                                                        UAmount = _locPurchaseInvoiceLine.UAmount,
                                                        Disc = _locDiscountLine.Disc,
                                                        DiscAmount = _locPurchaseInvoiceLine.DiscAmount,
                                                        TUAmount = _locPurchaseInvoiceLine.TUAmount,
                                                        PurchaseInvoiceLine = _locPurchaseInvoiceLine,
                                                    };
                                                    _saveDataDiscountLine.Save();
                                                    _saveDataDiscountLine.Session.CommitTransaction();
                                                }
                                            }
                                        }
                                        #endregion DiscountLine

                                        _locInventoryTransferInMonitoring.PurchaseInvoiceLine = _locPurchaseInvoiceLine;
                                        _locInventoryTransferInMonitoring.Status = Status.Close;
                                        _locInventoryTransferInMonitoring.StatusDate = now;
                                        _locInventoryTransferInMonitoring.PostedCount = _locInventoryTransferInMonitoring.PostedCount + 1;
                                        _locInventoryTransferInMonitoring.Save();
                                        _locInventoryTransferInMonitoring.Session.CommitTransaction();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = SalesInvoice ", ex.ToString());
            }
        }

        private double GetSumTotalTaxValue(Session _currSession, PurchaseOrderLine _purchaseOrderLineXPO)
        {
            double _result = 0;
            try
            {
                double _locTxValue = 0;

                if (_purchaseOrderLineXPO != null)
                {
                    XPCollection<TaxLine> _locTaxLines = new XPCollection<TaxLine>(_currSession,
                                                     new BinaryOperator("PurchaseOrderLine", _purchaseOrderLineXPO));

                    if (_locTaxLines.Count() > 0)
                    {
                        foreach (TaxLine _locTaxLine in _locTaxLines)
                        {
                            if (_locTaxLine.TxValue > 0 && _locTaxLine.TxAmount > 0)
                            {
                                if (_locTaxLine.TaxNature == TaxNature.Increase)
                                {
                                    _locTxValue = _locTxValue + _locTaxLine.TxValue;
                                }
                                else if (_locTaxLine.TaxNature == TaxNature.Decrease)
                                {
                                    _locTxValue = _locTxValue - _locTaxLine.TxValue;
                                }
                            }
                        }
                        _result = _locTxValue;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PurchaseInvoice " + ex.ToString());
            }
            return _result;
        }

        private double GetSumTotalTaxAmount(Session _currSession, PurchaseOrderLine _purchaseOrderLineXPO, InventoryTransferInMonitoring _inventoryTransferInMonitoringXPO)
        {
            double _return = 0;
            try
            {
                double _locTxAmount = 0;

                if (_purchaseOrderLineXPO != null)
                {
                    XPCollection<TaxLine> _locTaxLines = new XPCollection<TaxLine>(_currSession,
                                                     new BinaryOperator("PurchaseOrderLine", _purchaseOrderLineXPO));

                    if (_locTaxLines.Count() > 0)
                    {
                        foreach (TaxLine _locTaxLine in _locTaxLines)
                        {
                            if (_locTaxLine.TxValue > 0)
                            {
                                if (_locTaxLine.TaxNature == TaxNature.Increase)
                                {
                                    _locTxAmount = _locTxAmount + (_purchaseOrderLineXPO.UAmount * _inventoryTransferInMonitoringXPO.TQty * _locTaxLine.TxValue / 100);
                                }
                                else if (_locTaxLine.TaxNature == TaxNature.Decrease)
                                {
                                    _locTxAmount = _locTxAmount - (_purchaseOrderLineXPO.UAmount * _inventoryTransferInMonitoringXPO.TQty * _locTaxLine.TxValue / 100);
                                }
                            }
                        }
                        _return = _locTxAmount;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PurchaseInvoice" + ex.ToString());
            }
            return _return;
        }

        private double GetSumTotalDisc(Session _currSession, PurchaseOrderLine _purchaseOrderLineXPO)
        {
            double _result = 0;
            try
            {
                double _locDisc = 0;

                if (_purchaseOrderLineXPO != null)
                {
                    XPCollection<DiscountLine> _locDiscountLines = new XPCollection<DiscountLine>(_currSession,
                                                               new BinaryOperator("PurchaseOrderLine", _purchaseOrderLineXPO));

                    if (_locDiscountLines.Count() > 0)
                    {
                        foreach (DiscountLine _locDiscountLine in _locDiscountLines)
                        {
                            if (_locDiscountLine.Disc > 0 && _locDiscountLine.DiscAmount > 0)
                            {
                                if (_locDiscountLine.Disc > 0 && _locDiscountLine.DiscAmount > 0)
                                {
                                    _locDisc = _locDisc + _locDiscountLine.Disc;
                                }
                            }
                        }
                        _result = _locDisc;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PurchaseInvoice" + ex.ToString());
            }
            return _result;
        }

        private double GetSumTotalDiscAmount(Session _currSession, PurchaseOrderLine _salesOrderLineXPO, InventoryTransferInMonitoring _inventoryTransferInMonitoringXPO)
        {
            double _result = 0;
            try
            {
                double _locDiscAmount = 0;

                if (_salesOrderLineXPO != null)
                {
                    XPCollection<DiscountLine> _locDiscountLines = new XPCollection<DiscountLine>(_currSession,
                                                               new BinaryOperator("PurchaseOrderLine", _salesOrderLineXPO));

                    if (_locDiscountLines.Count() > 0)
                    {
                        foreach (DiscountLine _locDiscountLine in _locDiscountLines)
                        {
                            if (_locDiscountLine.Disc > 0)
                            {
                                _locDiscAmount = _locDiscAmount + (_salesOrderLineXPO.UAmount * _inventoryTransferInMonitoringXPO.TQty * _locDiscountLine.Disc / 100);
                            }
                        }
                        _result = _locDiscAmount;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PurchaseInvoice" + ex.ToString());
            }
            return _result;
        }

        #endregion GetITI

        #region GetPay

        private void GetPay(Session _currSession, PurchaseInvoice _purchaseInvoiceXPO)
        {
            try
            {
                double _totPayPOP = 0;
                double _totPayPO = 0;
                DateTime now = DateTime.Now;

                if (_purchaseInvoiceXPO != null)
                {
                    List<string> _stringPO = new List<string>();


                    XPCollection<PurchaseInvoiceLine> _locPurchaseInvoiceLines = new XPCollection<PurchaseInvoiceLine>
                                                                          (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("PurchaseInvoice", _purchaseInvoiceXPO),
                                                                            new BinaryOperator("Select", true),
                                                                            new BinaryOperator("Status", Status.Progress)));

                    if (_locPurchaseInvoiceLines != null && _locPurchaseInvoiceLines.Count() > 0)
                    {
                        foreach (PurchaseInvoiceLine _locPurchaseInvoiceLine in _locPurchaseInvoiceLines)
                        {
                            if (_locPurchaseInvoiceLine.PurchaseOrderMonitoring != null)
                            {
                                if (_locPurchaseInvoiceLine.PurchaseOrderMonitoring.PurchaseOrder != null)
                                {
                                    if (_locPurchaseInvoiceLine.PurchaseOrderMonitoring.PurchaseOrder.Code != null)
                                    {
                                        _stringPO.Add(_locPurchaseInvoiceLine.PurchaseOrderMonitoring.PurchaseOrder.Code);
                                    }
                                }
                            }
                        }
                    }


                    IEnumerable<string> _stringArrayPODistinct = _stringPO.Distinct();
                    string[] _stringArrayPOList = _stringArrayPODistinct.ToArray();
                    if (_stringArrayPOList.Length > 0)
                    {
                        for (int i = 0; i < _stringArrayPOList.Length; i++)
                        {
                            PurchaseOrder _locPurchaseOrder = _currSession.FindObject<PurchaseOrder>(new BinaryOperator("Code", _stringArrayPOList[i]));
                            if (_locPurchaseOrder != null)
                            {
                                XPCollection<PaymentOutPlan> _locPaymentOutPlans = new XPCollection<PaymentOutPlan>
                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("PurchaseOrder", _locPurchaseOrder)));

                                if (_locPaymentOutPlans != null && _locPaymentOutPlans.Count() > 0)
                                {
                                    foreach (PaymentOutPlan _locPaymentOutPlan in _locPaymentOutPlans)
                                    {
                                        _totPayPOP = _totPayPOP + _locPaymentOutPlan.Plan;
                                    }
                                }

                                if (_totPayPOP > 0)
                                {
                                    if (_totPayPOP < _locPurchaseOrder.MaxPay)
                                    {
                                        XPCollection<PurchaseInvoiceLine> _locPurchaseInvLines = new XPCollection<PurchaseInvoiceLine>
                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("PurchaseInvoice", _purchaseInvoiceXPO),
                                                                                new BinaryOperator("Select", true),
                                                                                new BinaryOperator("PurchaseOrderMonitoring.PurchaseOrder", _locPurchaseOrder),
                                                                                new GroupOperator(GroupOperatorType.Or,
                                                                                new BinaryOperator("Status", Status.Progress))));
                                        if (_locPurchaseInvLines != null && _locPurchaseInvLines.Count() > 0)
                                        {
                                            _totPayPO = (_locPurchaseOrder.MaxPay - _totPayPOP) / _locPurchaseInvLines.Count();

                                            foreach (PurchaseInvoiceLine _locPurchaseInvLine in _locPurchaseInvLines)
                                            {
                                                if (_locPurchaseInvLine.PurchaseOrderMonitoring != null)
                                                {
                                                    if (_locPurchaseInvLine.PurchaseOrderMonitoring.PurchaseOrder == _locPurchaseOrder)
                                                    {
                                                        if (_locPurchaseInvLine.TAmount >= _totPayPO)
                                                        {
                                                            _locPurchaseInvLine.Pay = _totPayPO;
                                                            _locPurchaseInvLine.Status = Status.Lock;
                                                            _locPurchaseInvLine.StatusDate = now;
                                                            _locPurchaseInvLine.Save();
                                                            _locPurchaseInvLine.Session.CommitTransaction();
                                                        }
                                                        else
                                                        {
                                                            _locPurchaseInvLine.Pay = _locPurchaseInvLine.TAmount;
                                                            _locPurchaseInvLine.Status = Status.Lock;
                                                            _locPurchaseInvLine.StatusDate = now;
                                                            _locPurchaseInvLine.Save();
                                                            _locPurchaseInvLine.Session.CommitTransaction();
                                                        }
                                                    }
                                                }
                                            }
                                            _totPayPO = 0;
                                        }
                                    }
                                    _totPayPOP = 0;
                                }
                                else
                                {
                                    XPCollection<PurchaseInvoiceLine> _locPurchaseInvLines = new XPCollection<PurchaseInvoiceLine>
                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("PurchaseInvoice", _purchaseInvoiceXPO),
                                                                                new BinaryOperator("Select", true),
                                                                                new GroupOperator(GroupOperatorType.Or,
                                                                                new BinaryOperator("Status", Status.Progress))));
                                    if (_locPurchaseInvLines != null && _locPurchaseInvLines.Count() > 0)
                                    {
                                        foreach (PurchaseInvoiceLine _locPurchaseInvLine in _locPurchaseInvLines)
                                        {
                                            _locPurchaseInvLine.Pay = _locPurchaseInvLine.TAmount;
                                            _locPurchaseInvLine.Status = Status.Lock;
                                            _locPurchaseInvLine.StatusDate = now;
                                            _locPurchaseInvLine.Save();
                                            _locPurchaseInvLine.Session.CommitTransaction();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PurchaseInvoice ", ex.ToString());
            }
        }

        #endregion GetPay

        #region Posting Method

        private double GetPurchaseInvoiceLineTotalAmount(Session _currSession, PurchaseInvoice _locPurchaseInvoiceXPO)
        {
            double _return = 0;
            try
            {
                if (_locPurchaseInvoiceXPO != null)
                {
                    XPCollection<PurchaseInvoiceLine> _locPurchaseInvoiceLines = new XPCollection<PurchaseInvoiceLine>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO),
                                                                                                new BinaryOperator("Select", true),
                                                                                                new GroupOperator(GroupOperatorType.Or,
                                                                                                new BinaryOperator("Status", Status.Lock),
                                                                                                new BinaryOperator("Status", Status.Posted))));

                    if (_locPurchaseInvoiceLines != null && _locPurchaseInvoiceLines.Count > 0)
                    {
                        foreach (PurchaseInvoiceLine _locPurchaseInvoiceLine in _locPurchaseInvoiceLines)
                        {
                            _return = _return + _locPurchaseInvoiceLine.Pay;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PurchaseInvoice" + ex.ToString());
            }
            return _return;
        }

        private void SetPurchaseInvoiceMonitoring(Session _currSession, PurchaseInvoice _locPurchaseInvoiceXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _currSignCode = null;

                if (_locPurchaseInvoiceXPO != null)
                {
                    if (_locPurchaseInvoiceXPO.Status == Status.Lock || _locPurchaseInvoiceXPO.Status == Status.Posted)
                    {
                        XPCollection<PurchaseInvoiceLine> _locPurchaseInvoiceLines = new XPCollection<PurchaseInvoiceLine>
                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Lock),
                                                            new BinaryOperator("Status", Status.Posted))));

                        if (_locPurchaseInvoiceLines != null && _locPurchaseInvoiceLines.Count() > 0)
                        {
                            foreach (PurchaseInvoiceLine _locPurchaseInvoiceLine in _locPurchaseInvoiceLines)
                            {
                                if (_locPurchaseInvoiceLine.Pay > 0)
                                {
                                    _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.PurchaseInvoiceMonitoring);

                                    if (_currSignCode != null)
                                    {
                                        PurchaseInvoiceMonitoring _saveDataPurchaseInvoiceMonitoring = new PurchaseInvoiceMonitoring(_currSession)
                                        {
                                            SignCode = _currSignCode,
                                            PurchaseInvoice = _locPurchaseInvoiceXPO,
                                            PurchaseInvoiceLine = _locPurchaseInvoiceLine,
                                            PurchaseType = _locPurchaseInvoiceLine.PurchaseType,
                                            Item = _locPurchaseInvoiceLine.Item,
                                            DQty = _locPurchaseInvoiceLine.DQty,
                                            DUOM = _locPurchaseInvoiceLine.DUOM,
                                            Qty = _locPurchaseInvoiceLine.Qty,
                                            UOM = _locPurchaseInvoiceLine.UOM,
                                            TQty = _locPurchaseInvoiceLine.TQty,
                                            Currency = _locPurchaseInvoiceLine.Currency,
                                            UAmount = _locPurchaseInvoiceLine.UAmount,
                                            TUAmount = _locPurchaseInvoiceLine.TUAmount,
                                            MultiTax = _locPurchaseInvoiceLine.MultiTax,
                                            Tax = _locPurchaseInvoiceLine.Tax,
                                            TxValue = _locPurchaseInvoiceLine.TxValue,
                                            TxAmount = _locPurchaseInvoiceLine.TxAmount,
                                            MultiDiscount = _locPurchaseInvoiceLine.MultiDiscount,
                                            Discount = _locPurchaseInvoiceLine.Discount,
                                            Disc = _locPurchaseInvoiceLine.Disc,
                                            DiscAmount = _locPurchaseInvoiceLine.DiscAmount,
                                            TAmount = _locPurchaseInvoiceLine.TAmount,
                                            Pay = _locPurchaseInvoiceLine.Pay,
                                            PurchaseOrderMonitoring = _locPurchaseInvoiceLine.PurchaseOrderMonitoring,
                                            InventoryTransferInMonitoring = _locPurchaseInvoiceLine.InventoryTransferInMonitoring,
                                        };
                                        _saveDataPurchaseInvoiceMonitoring.Save();
                                        _saveDataPurchaseInvoiceMonitoring.Session.CommitTransaction();

                                        PurchaseInvoiceMonitoring _locPurchaseInvoiceMonitoring = _currSession.FindObject<PurchaseInvoiceMonitoring>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("SignCode", _currSignCode)));
                                        if (_locPurchaseInvoiceMonitoring != null)
                                        {
                                            SetPaymentOutPlan(_currSession, _locPurchaseInvoiceXPO, _locPurchaseInvoiceMonitoring);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PurchaseInvoice ", ex.ToString());
            }
        }

        private void SetPaymentOutPlan(Session _currSession, PurchaseInvoice _locPurchaseInvoiceXPO, PurchaseInvoiceMonitoring _locPurchaseInvoiceMonitoringXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;

                if (_locPurchaseInvoiceXPO != null && _locPurchaseInvoiceMonitoringXPO != null)
                {
                    PaymentOutPlan _locPaymentOutPlan = _currSession.FindObject<PaymentOutPlan>
                                                    (new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("PurchaseInvoiceMonitoring", _locPurchaseInvoiceMonitoringXPO)));
                    if (_locPaymentOutPlan == null)
                    {
                        PaymentOutPlan _savePaymentOutPlan = new PaymentOutPlan(_currSession)
                        {
                            PaymentMethod = _locPurchaseInvoiceXPO.PaymentMethod,
                            PaymentMethodType = _locPurchaseInvoiceXPO.PaymentMethodType,
                            PaymentType = _locPurchaseInvoiceXPO.PaymentType,
                            EstimatedDate = _locPurchaseInvoiceXPO.EstimatedDate,
                            Plan = _locPurchaseInvoiceMonitoringXPO.Pay,
                            Outstanding = _locPurchaseInvoiceMonitoringXPO.Pay,
                            PurchaseOrder = _locPurchaseInvoiceMonitoringXPO.PurchaseOrderMonitoring.PurchaseOrder,
                            PurchaseInvoice = _locPurchaseInvoiceXPO,
                            PurchaseInvoiceMonitoring = _locPurchaseInvoiceMonitoringXPO,
                            Company = _locPurchaseInvoiceXPO.Company,
                        };
                        _savePaymentOutPlan.Save();
                        _savePaymentOutPlan.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PurchaseInvoice ", ex.ToString());
            }
        }

        private void SetReverseGoodsReceiveJournal(Session _currSession, PurchaseInvoice _locPurchaseInvoiceXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                double _locTotalUnitPrice = 0;

                if (_locPurchaseInvoiceXPO != null)
                {
                    //PurchaseInvoice yg di reverse
                    XPCollection<PurchaseInvoiceLine> _locPurchaseInvoiceLines = new XPCollection<PurchaseInvoiceLine>
                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO),
                                                                        new BinaryOperator("Select", true),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("Status", Status.Lock),
                                                                        new BinaryOperator("Status", Status.Posted))));

                    if (_locPurchaseInvoiceLines != null && _locPurchaseInvoiceLines.Count() > 0)
                    {
                        foreach (PurchaseInvoiceLine _locPurchaseInvoiceLine in _locPurchaseInvoiceLines)
                        {
                            //ngambilnya nanti dari COGS
                            _locTotalUnitPrice = _locPurchaseInvoiceLine.TUAmount;

                            if (_locPurchaseInvoiceLine.PurchaseOrderMonitoring != null && _locPurchaseInvoiceLine.InventoryTransferInMonitoring != null)
                            {
                                if (_locPurchaseInvoiceLine.PurchaseOrderMonitoring.PurchaseOrder != null && _locPurchaseInvoiceLine.InventoryTransferInMonitoring.InventoryTransferIn != null)
                                {
                                    #region CreateReverseGoodsReceiveJournalByItems
                                    if (_locPurchaseInvoiceLine.Item.ItemAccountGroup != null)
                                    {
                                        XPCollection<JournalMap> _locJournalMapItems = new XPCollection<JournalMap>
                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("ItemAccountGroup", _locPurchaseInvoiceLine.Item.ItemAccountGroup)));

                                        if (_locJournalMapItems != null && _locJournalMapItems.Count() > 0)
                                        {
                                            foreach (JournalMap _locJournalMapItem in _locJournalMapItems)
                                            {
                                                XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                    new BinaryOperator("JournalMap", _locJournalMapItem)));

                                                if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                {
                                                    foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                    {
                                                        AccountMap _locAccountMap = _currSession.FindObject<AccountMap>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("Code", _locJournalMapLine.AccountMap.Code),
                                                                                     new BinaryOperator("PostingType", PostingType.Purchase),
                                                                                     new BinaryOperator("OrderType", _locPurchaseInvoiceLine.PurchaseType),
                                                                                     new BinaryOperator("PostingMethod", PostingMethod.Receipt),
                                                                                     new BinaryOperator("PostingMethodType", PostingMethodType.Normal)));

                                                        if (_locAccountMap != null)
                                                        {
                                                            XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                new BinaryOperator("AccountMap", _locAccountMap),
                                                                                                                new BinaryOperator("Active", true)));

                                                            if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                            {
                                                                double _locTotalAmountDebit = 0;
                                                                double _locTotalAmountCredit = 0;
                                                                double _locTotalBalance = 0;

                                                                foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                {
                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                    {
                                                                        _locTotalAmountCredit = _locTotalUnitPrice;
                                                                    }
                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                    {
                                                                        _locTotalAmountDebit = _locTotalUnitPrice;
                                                                    }

                                                                    GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                    {
                                                                        PostingDate = now,
                                                                        PostingType = PostingType.Purchase,
                                                                        PostingMethod = PostingMethod.InvoiceAP,
                                                                        Account = _locAccountMapLine.Account,
                                                                        Debit = _locTotalAmountDebit,
                                                                        Credit = _locTotalAmountCredit,
                                                                        PurchaseOrder = _locPurchaseInvoiceLine.PurchaseOrderMonitoring.PurchaseOrder,
                                                                        InventoryTransferIn = _locPurchaseInvoiceLine.InventoryTransferInMonitoring.InventoryTransferIn,
                                                                        PurchaseInvoice = _locPurchaseInvoiceXPO,
                                                                        Company = _locPurchaseInvoiceXPO.Company,
                                                                    };
                                                                    _saveGeneralJournal.Save();
                                                                    _saveGeneralJournal.Session.CommitTransaction();

                                                                    if (_locAccountMapLine.Account.Code != null)
                                                                    {
                                                                        ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                                                                        if (_locCOA != null)
                                                                        {
                                                                            if (_locCOA.BalanceType == BalanceType.Change)
                                                                            {
                                                                                if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                                {
                                                                                    if (_locTotalAmountDebit > 0)
                                                                                    {
                                                                                        _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                                    }
                                                                                    if (_locTotalAmountCredit > 0)
                                                                                    {
                                                                                        _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                                    }
                                                                                }
                                                                                if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                                {
                                                                                    if (_locTotalAmountDebit > 0)
                                                                                    {
                                                                                        _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                                    }
                                                                                    if (_locTotalAmountCredit > 0)
                                                                                    {
                                                                                        _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                                    }
                                                                                }
                                                                            }

                                                                            _locCOA.Balance = _locTotalBalance;
                                                                            _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                            _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                            _locCOA.Save();
                                                                            _locCOA.Session.CommitTransaction();
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    #endregion CreateReverseGoodsReceiptJournalByItems

                                    //Nanti di hapus kalau sudah ada COGS sistemnya
                                    #region CreateReverseGoodsReceiptJournalByBusinessPartner
                                    if (_locPurchaseInvoiceXPO.BuyFromVendor != null)
                                    {
                                        if (_locPurchaseInvoiceXPO.BuyFromVendor.BusinessPartnerAccountGroup != null)
                                        {
                                            XPCollection<JournalMap> _locJournalMapBusinessPartners = new XPCollection<JournalMap>
                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                               new BinaryOperator("BusinessPartnerAccountGroup", _locPurchaseInvoiceXPO.BuyFromVendor.BusinessPartnerAccountGroup)));

                                            if (_locJournalMapBusinessPartners != null && _locJournalMapBusinessPartners.Count() > 0)
                                            {
                                                foreach (JournalMap _locJournalMapBusinessPartner in _locJournalMapBusinessPartners)
                                                {
                                                    XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("JournalMap", _locJournalMapBusinessPartner)));

                                                    if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                    {
                                                        foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                        {
                                                            AccountMap _locAccountMap = _currSession.FindObject<AccountMap>
                                                                                         (new GroupOperator(GroupOperatorType.And,
                                                                                         new BinaryOperator("Code", _locJournalMapLine.AccountMap.Code),
                                                                                         new BinaryOperator("PostingType", PostingType.Purchase),
                                                                                         new BinaryOperator("OrderType", _locPurchaseInvoiceLine.PurchaseType),
                                                                                         new BinaryOperator("PostingMethod", PostingMethod.Receipt),
                                                                                         new BinaryOperator("PostingMethodType", PostingMethodType.Normal)));

                                                            if (_locAccountMap != null)
                                                            {
                                                                XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("AccountMap", _locAccountMap),
                                                                                                new BinaryOperator("Active", true)));

                                                                if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                {
                                                                    double _locTotalAmountDebit = 0;
                                                                    double _locTotalAmountCredit = 0;
                                                                    double _locTotalBalance = 0;

                                                                    foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                    {
                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                        {
                                                                            _locTotalAmountCredit = _locTotalUnitPrice;
                                                                        }
                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                        {
                                                                            _locTotalAmountDebit = _locTotalUnitPrice;
                                                                        }

                                                                        GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                        {
                                                                            PostingDate = now,
                                                                            PostingType = PostingType.Purchase,
                                                                            PostingMethod = PostingMethod.InvoiceAP,
                                                                            Account = _locAccountMapLine.Account,
                                                                            Debit = _locTotalAmountDebit,
                                                                            Credit = _locTotalAmountCredit,
                                                                            PurchaseOrder = _locPurchaseInvoiceLine.PurchaseOrderMonitoring.PurchaseOrder,
                                                                            InventoryTransferIn = _locPurchaseInvoiceLine.InventoryTransferInMonitoring.InventoryTransferIn,
                                                                            PurchaseInvoice = _locPurchaseInvoiceXPO,
                                                                            Company = _locPurchaseInvoiceXPO.Company,
                                                                        };
                                                                        _saveGeneralJournal.Save();
                                                                        _saveGeneralJournal.Session.CommitTransaction();

                                                                        if (_locAccountMapLine.Account.Code != null)
                                                                        {
                                                                            ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                                                                            if (_locCOA != null)
                                                                            {
                                                                                if (_locCOA.BalanceType == BalanceType.Change)
                                                                                {
                                                                                    if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                                    {
                                                                                        if (_locTotalAmountDebit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                                        }
                                                                                        if (_locTotalAmountCredit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                                        }
                                                                                    }
                                                                                    if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                                    {
                                                                                        if (_locTotalAmountDebit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                                        }
                                                                                        if (_locTotalAmountCredit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                                        }
                                                                                    }
                                                                                }

                                                                                _locCOA.Balance = _locTotalBalance;
                                                                                _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                                _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                                _locCOA.Save();
                                                                                _locCOA.Session.CommitTransaction();
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    #endregion CreateReverseGoodsReceiptJournalByBusinessPartner
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = SalesInvoice ", ex.ToString());
            }
        }

        private void SetInvoiceAPJournal(Session _currSession, PurchaseInvoice _locPurchaseInvoiceXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                double _locBillTUAmount = 0;
                double _locTxOfAmount = 0;
                double _locDiscOfAmount = 0;

                if (_locPurchaseInvoiceXPO != null)
                {
                    #region CreateInvoiceAPJournal

                    XPCollection<PurchaseInvoiceLine> _locPurchaseInvoiceLines = new XPCollection<PurchaseInvoiceLine>
                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO),
                                                                                new BinaryOperator("Select", true),
                                                                                new GroupOperator(GroupOperatorType.Or,
                                                                                new BinaryOperator("Status", Status.Lock),
                                                                                new BinaryOperator("Status", Status.Posted))));
                    if (_locPurchaseInvoiceLines != null && _locPurchaseInvoiceLines.Count() > 0)
                    {
                        foreach (PurchaseInvoiceLine _locPurchaseInvoiceLine in _locPurchaseInvoiceLines)
                        {
                            if (_locPurchaseInvoiceLine.PurchaseOrderMonitoring != null)
                            {
                                if (_locPurchaseInvoiceLine.PurchaseOrderMonitoring.PurchaseOrder != null)
                                {
                                    #region CreateNormalInvoiceAPJournal

                                    _locBillTUAmount = _locPurchaseInvoiceLine.Pay;

                                    #region JournalMapItemAccountGroup
                                    if (_locPurchaseInvoiceLine.Item != null)
                                    {
                                        if (_locPurchaseInvoiceLine.Item.ItemAccountGroup != null)
                                        {
                                            XPCollection<JournalMap> _locJournalMaps = new XPCollection<JournalMap>
                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                               new BinaryOperator("ItemAccountGroup", _locPurchaseInvoiceLine.Item.ItemAccountGroup)));

                                            if (_locJournalMaps != null && _locJournalMaps.Count() > 0)
                                            {
                                                foreach (JournalMap _locJournalMap in _locJournalMaps)
                                                {
                                                    XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                    new BinaryOperator("JournalMap", _locJournalMap)));

                                                    if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                    {
                                                        foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                        {
                                                            AccountMap _locAccountMap = _currSession.FindObject<AccountMap>
                                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                                         new BinaryOperator("Code", _locJournalMapLine.AccountMap.Code),
                                                                                         new BinaryOperator("PostingType", PostingType.Purchase),
                                                                                         new BinaryOperator("OrderType", _locPurchaseInvoiceLine.PurchaseType),
                                                                                         new BinaryOperator("PostingMethod", PostingMethod.InvoiceAP),
                                                                                         new BinaryOperator("PostingMethodType", PostingMethodType.Normal)));

                                                            if (_locAccountMap != null)
                                                            {
                                                                XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                    new BinaryOperator("AccountMap", _locAccountMap),
                                                                                                                    new BinaryOperator("Active", true)));

                                                                if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                {
                                                                    double _locTotalAmountDebit = 0;
                                                                    double _locTotalAmountCredit = 0;
                                                                    double _locTotalBalance = 0;

                                                                    foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                    {
                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                        {
                                                                            _locTotalAmountDebit = _locBillTUAmount;
                                                                        }
                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                        {
                                                                            _locTotalAmountCredit = _locBillTUAmount;
                                                                        }

                                                                        GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                        {
                                                                            PostingDate = now,
                                                                            PostingType = PostingType.Purchase,
                                                                            OrderType = _locPurchaseInvoiceLine.PurchaseType,
                                                                            PostingMethod = PostingMethod.InvoiceAP,
                                                                            PostingMethodType = PostingMethodType.Normal,
                                                                            Account = _locAccountMapLine.Account,
                                                                            Debit = _locTotalAmountDebit,
                                                                            Credit = _locTotalAmountCredit,
                                                                            PurchaseOrder = _locPurchaseInvoiceLine.PurchaseOrderMonitoring.PurchaseOrder,
                                                                            PurchaseInvoice = _locPurchaseInvoiceXPO,
                                                                            Company = _locPurchaseInvoiceXPO.Company,
                                                                        };
                                                                        _saveGeneralJournal.Save();
                                                                        _saveGeneralJournal.Session.CommitTransaction();

                                                                        if (_locAccountMapLine.Account.Code != null)
                                                                        {
                                                                            ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                                                                            if (_locCOA != null)
                                                                            {
                                                                                if (_locCOA.BalanceType == BalanceType.Change)
                                                                                {
                                                                                    if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                                    {
                                                                                        if (_locTotalAmountDebit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                                        }
                                                                                        if (_locTotalAmountCredit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                                        }
                                                                                    }
                                                                                    if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                                    {
                                                                                        if (_locTotalAmountDebit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                                        }
                                                                                        if (_locTotalAmountCredit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                                        }
                                                                                    }
                                                                                }

                                                                                _locCOA.Balance = _locTotalBalance;
                                                                                _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                                _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                                _locCOA.Save();
                                                                                _locCOA.Session.CommitTransaction();
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    #endregion JournalMapItemAccountGroup

                                    #region JournalMapBusinessPartnerAccountGroup
                                    if (_locPurchaseInvoiceXPO.BuyFromVendor != null)
                                    {
                                        if (_locPurchaseInvoiceXPO.BuyFromVendor.BusinessPartnerAccountGroup != null)
                                        {
                                            XPCollection<JournalMap> _locJournalMapByBusinessPartners = new XPCollection<JournalMap>
                                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                         new BinaryOperator("BusinessPartnerAccountGroup", _locPurchaseInvoiceXPO.BuyFromVendor.BusinessPartnerAccountGroup)));

                                            if (_locJournalMapByBusinessPartners != null && _locJournalMapByBusinessPartners.Count() > 0)
                                            {
                                                foreach (JournalMap _locJournalMapByBusinessPartner in _locJournalMapByBusinessPartners)
                                                {
                                                    XPCollection<JournalMapLine> _locJournalMapLineByBusinessPartners = new XPCollection<JournalMapLine>
                                                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                         new BinaryOperator("JournalMap", _locJournalMapByBusinessPartner)));

                                                    if (_locJournalMapLineByBusinessPartners != null && _locJournalMapLineByBusinessPartners.Count() > 0)
                                                    {
                                                        foreach (JournalMapLine _locJournalMapLineByBusinessPartner in _locJournalMapLineByBusinessPartners)
                                                        {
                                                            AccountMap _locAccountMapByBusinessPartner = _currSession.FindObject<AccountMap>
                                                                                                         (new GroupOperator(GroupOperatorType.And,
                                                                                                          new BinaryOperator("Code", _locJournalMapLineByBusinessPartner.AccountMap.Code),
                                                                                                          new BinaryOperator("PostingType", PostingType.Purchase),
                                                                                                          new BinaryOperator("OrderType", _locPurchaseInvoiceLine.PurchaseType),
                                                                                                          new BinaryOperator("PostingMethod", PostingMethod.InvoiceAP),
                                                                                                          new BinaryOperator("PostingMethodType", PostingMethodType.Normal)));

                                                            if (_locAccountMapByBusinessPartner != null)
                                                            {
                                                                XPCollection<AccountMapLine> _locAccountMapLineByBusinessPartners = new XPCollection<AccountMapLine>
                                                                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                                     new BinaryOperator("AccountMap", _locAccountMapByBusinessPartner),
                                                                                                                                     new BinaryOperator("Active", true)));

                                                                if (_locAccountMapLineByBusinessPartners != null && _locAccountMapLineByBusinessPartners.Count() > 0)
                                                                {
                                                                    double _locTotalAmountDebit = 0;
                                                                    double _locTotalAmountCredit = 0;
                                                                    double _locTotalBalance = 0;

                                                                    foreach (AccountMapLine _locAccountMapLine in _locAccountMapLineByBusinessPartners)
                                                                    {
                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                        {
                                                                            _locTotalAmountDebit = _locBillTUAmount;
                                                                        }
                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                        {
                                                                            _locTotalAmountCredit = _locBillTUAmount;
                                                                        }

                                                                        GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                        {
                                                                            PostingDate = now,
                                                                            PostingType = PostingType.Purchase,
                                                                            OrderType = _locPurchaseInvoiceLine.PurchaseType,
                                                                            PostingMethod = PostingMethod.InvoiceAP,
                                                                            PostingMethodType = PostingMethodType.Normal,
                                                                            Account = _locAccountMapLine.Account,
                                                                            Debit = _locTotalAmountDebit,
                                                                            Credit = _locTotalAmountCredit,
                                                                            PurchaseOrder = _locPurchaseInvoiceLine.PurchaseOrderMonitoring.PurchaseOrder,
                                                                            PurchaseInvoice = _locPurchaseInvoiceXPO,
                                                                            Company = _locPurchaseInvoiceXPO.Company,
                                                                        };
                                                                        _saveGeneralJournal.Save();
                                                                        _saveGeneralJournal.Session.CommitTransaction();

                                                                        if (_locAccountMapLine.Account.Code != null)
                                                                        {
                                                                            ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                                     (new GroupOperator(GroupOperatorType.And,
                                                                                                      new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                                                                            if (_locCOA != null)
                                                                            {
                                                                                if (_locCOA.BalanceType == BalanceType.Change)
                                                                                {
                                                                                    if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                                    {
                                                                                        if (_locTotalAmountDebit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                                        }
                                                                                        if (_locTotalAmountCredit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                                        }
                                                                                    }
                                                                                    if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                                    {
                                                                                        if (_locTotalAmountDebit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                                        }
                                                                                        if (_locTotalAmountCredit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                                        }
                                                                                    }
                                                                                }

                                                                                _locCOA.Balance = _locTotalBalance;
                                                                                _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                                _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                                _locCOA.Save();
                                                                                _locCOA.Session.CommitTransaction();
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    #endregion JournalMapBusinessPartnerAccountGroup

                                    #endregion CreateNormalInvoiceAPJournal

                                    #region CreateInvoiceAPWithTax
                                    if (_locPurchaseInvoiceLine.MultiTax == true)
                                    {
                                        XPCollection<TaxLine> _locTaxLines = new XPCollection<TaxLine>
                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("PurchaseInvoiceLine", _locPurchaseInvoiceLine)));
                                        if (_locTaxLines != null && _locTaxLines.Count() > 0)
                                        {
                                            foreach (TaxLine _locTaxLine in _locTaxLines)
                                            {
                                                if (_locTaxLine.Tax != null)
                                                {
                                                    _locTxOfAmount = (_locTaxLine.TxValue * _locPurchaseInvoiceLine.TUAmount) / 100;

                                                    #region JournalMapTaxAccountGroup
                                                    if (_locTaxLine.Tax.TaxAccountGroup != null)
                                                    {
                                                        XPCollection<JournalMap> _locJournalMapByTaxs = new XPCollection<JournalMap>
                                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                     new BinaryOperator("TaxAccountGroup", _locTaxLine.Tax.TaxAccountGroup)));

                                                        if (_locJournalMapByTaxs != null && _locJournalMapByTaxs.Count() > 0)
                                                        {
                                                            foreach (JournalMap _locJournalMapByTax in _locJournalMapByTaxs)
                                                            {
                                                                XPCollection<JournalMapLine> _locJournalMapLineByTaxs = new XPCollection<JournalMapLine>
                                                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                         new BinaryOperator("JournalMap", _locJournalMapByTax)));

                                                                if (_locJournalMapLineByTaxs != null && _locJournalMapLineByTaxs.Count() > 0)
                                                                {
                                                                    foreach (JournalMapLine _locJournalMapLineByTax in _locJournalMapLineByTaxs)
                                                                    {
                                                                        AccountMap _locAccountMapByTax = _currSession.FindObject<AccountMap>
                                                                                                         (new GroupOperator(GroupOperatorType.And,
                                                                                                          new BinaryOperator("Code", _locJournalMapLineByTax.AccountMap.Code),
                                                                                                          new BinaryOperator("PostingType", PostingType.Purchase),
                                                                                                          new BinaryOperator("OrderType", _locPurchaseInvoiceLine.PurchaseType),
                                                                                                          new BinaryOperator("PostingMethod", PostingMethod.InvoiceAP),
                                                                                                          new BinaryOperator("PostingMethodType", PostingMethodType.Tax)));

                                                                        if (_locAccountMapByTax != null)
                                                                        {
                                                                            XPCollection<AccountMapLine> _locAccountMapLineByTaxs = new XPCollection<AccountMapLine>
                                                                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                                     new BinaryOperator("AccountMap", _locAccountMapByTax),
                                                                                                                                     new BinaryOperator("Active", true)));

                                                                            if (_locAccountMapLineByTaxs != null && _locAccountMapLineByTaxs.Count() > 0)
                                                                            {
                                                                                double _locTotalAmountDebit = 0;
                                                                                double _locTotalAmountCredit = 0;
                                                                                double _locTotalBalance = 0;

                                                                                foreach (AccountMapLine _locAccountMapLineByTax in _locAccountMapLineByTaxs)
                                                                                {
                                                                                    if (_locAccountMapLineByTax.AccountCharge == AccountCharge.Debit)
                                                                                    {
                                                                                        _locTotalAmountDebit = _locTxOfAmount;
                                                                                    }
                                                                                    if (_locAccountMapLineByTax.AccountCharge == AccountCharge.Credit)
                                                                                    {
                                                                                        _locTotalAmountCredit = _locTxOfAmount;
                                                                                    }

                                                                                    GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                                    {
                                                                                        PostingDate = now,
                                                                                        PostingType = PostingType.Purchase,
                                                                                        OrderType = _locPurchaseInvoiceLine.PurchaseType,
                                                                                        PostingMethod = PostingMethod.InvoiceAP,
                                                                                        PostingMethodType = PostingMethodType.Tax,
                                                                                        Account = _locAccountMapLineByTax.Account,
                                                                                        Debit = _locTotalAmountDebit,
                                                                                        Credit = _locTotalAmountCredit,
                                                                                        PurchaseOrder = _locPurchaseInvoiceLine.PurchaseOrderMonitoring.PurchaseOrder,
                                                                                        PurchaseInvoice = _locPurchaseInvoiceXPO,
                                                                                        Company = _locPurchaseInvoiceXPO.Company,
                                                                                    };
                                                                                    _saveGeneralJournal.Save();
                                                                                    _saveGeneralJournal.Session.CommitTransaction();

                                                                                    if (_locAccountMapLineByTax.Account.Code != null)
                                                                                    {
                                                                                        ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                                                 (new GroupOperator(GroupOperatorType.And,
                                                                                                                  new BinaryOperator("Code", _locAccountMapLineByTax.Account.Code)));
                                                                                        if (_locCOA != null)
                                                                                        {
                                                                                            if (_locCOA.BalanceType == BalanceType.Change)
                                                                                            {
                                                                                                if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                                                {
                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                                                    }
                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                                                    }
                                                                                                }
                                                                                                if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                                                {
                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                                                    }
                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                                                    }
                                                                                                }
                                                                                            }

                                                                                            _locCOA.Balance = _locTotalBalance;
                                                                                            _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                                            _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                                            _locCOA.Save();
                                                                                            _locCOA.Session.CommitTransaction();
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }

                                                    #endregion JournalMapTaxAccountGroup

                                                    #region JournalMapBusinessPartnerAccountGroup
                                                    if (_locPurchaseInvoiceXPO.BuyFromVendor != null)
                                                    {
                                                        if (_locPurchaseInvoiceXPO.BuyFromVendor.BusinessPartnerAccountGroup != null)
                                                        {
                                                            XPCollection<JournalMap> _locJournalMapByBusinessPartners = new XPCollection<JournalMap>
                                                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                         new BinaryOperator("BusinessPartnerAccountGroup", _locPurchaseInvoiceXPO.BuyFromVendor.BusinessPartnerAccountGroup)));

                                                            if (_locJournalMapByBusinessPartners != null && _locJournalMapByBusinessPartners.Count() > 0)
                                                            {
                                                                foreach (JournalMap _locJournalMapByBusinessPartner in _locJournalMapByBusinessPartners)
                                                                {
                                                                    XPCollection<JournalMapLine> _locJournalMapLineByBusinessPartners = new XPCollection<JournalMapLine>
                                                                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                                         new BinaryOperator("JournalMap", _locJournalMapByBusinessPartner)));

                                                                    if (_locJournalMapLineByBusinessPartners != null && _locJournalMapLineByBusinessPartners.Count() > 0)
                                                                    {
                                                                        foreach (JournalMapLine _locJournalMapLineByBusinessPartner in _locJournalMapLineByBusinessPartners)
                                                                        {
                                                                            //Wahab 15-06-2020
                                                                            AccountMap _locAccountMapByBusinessPartner = _currSession.FindObject<AccountMap>
                                                                                                                         (new GroupOperator(GroupOperatorType.And,
                                                                                                                          new BinaryOperator("Code", _locJournalMapLineByBusinessPartner.AccountMap.Code),
                                                                                                                          new BinaryOperator("PostingType", PostingType.Purchase),
                                                                                                                          new BinaryOperator("OrderType", OrderType.Item),
                                                                                                                          new BinaryOperator("PostingMethod", PostingMethod.InvoiceAP),
                                                                                                                          new BinaryOperator("PostingMethodType", PostingMethodType.Tax)));

                                                                            if (_locAccountMapByBusinessPartner != null)
                                                                            {
                                                                                XPCollection<AccountMapLine> _locAccountMapLineByBusinessPartners = new XPCollection<AccountMapLine>
                                                                                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                                                     new BinaryOperator("AccountMap", _locAccountMapByBusinessPartner),
                                                                                                                                                     new BinaryOperator("Active", true)));

                                                                                if (_locAccountMapLineByBusinessPartners != null && _locAccountMapLineByBusinessPartners.Count() > 0)
                                                                                {
                                                                                    double _locTotalAmountDebit = 0;
                                                                                    double _locTotalAmountCredit = 0;
                                                                                    double _locTotalBalance = 0;

                                                                                    foreach (AccountMapLine _locAccountMapLine in _locAccountMapLineByBusinessPartners)
                                                                                    {
                                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                                        {
                                                                                            _locTotalAmountDebit = _locTxOfAmount;
                                                                                        }
                                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                                        {
                                                                                            _locTotalAmountCredit = _locTxOfAmount;
                                                                                        }

                                                                                        GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                                        {
                                                                                            PostingDate = now,
                                                                                            PostingType = PostingType.Purchase,
                                                                                            PostingMethod = PostingMethod.InvoiceAP,
                                                                                            Account = _locAccountMapLine.Account,
                                                                                            Debit = _locTotalAmountDebit,
                                                                                            Credit = _locTotalAmountCredit,
                                                                                            PurchaseOrder = _locPurchaseInvoiceLine.PurchaseOrderMonitoring.PurchaseOrder,
                                                                                            PurchaseInvoice = _locPurchaseInvoiceXPO,
                                                                                            Company = _locPurchaseInvoiceXPO.Company,
                                                                                        };
                                                                                        _saveGeneralJournal.Save();
                                                                                        _saveGeneralJournal.Session.CommitTransaction();

                                                                                        if (_locAccountMapLine.Account.Code != null)
                                                                                        {
                                                                                            ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                                                            new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                                                                                            if (_locCOA != null)
                                                                                            {
                                                                                                if (_locCOA.BalanceType == BalanceType.Change)
                                                                                                {
                                                                                                    if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                                                    {
                                                                                                        if (_locTotalAmountDebit > 0)
                                                                                                        {
                                                                                                            _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                                                        }
                                                                                                        if (_locTotalAmountCredit > 0)
                                                                                                        {
                                                                                                            _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                                                        }
                                                                                                    }
                                                                                                    if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                                                    {
                                                                                                        if (_locTotalAmountDebit > 0)
                                                                                                        {
                                                                                                            _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                                                        }
                                                                                                        if (_locTotalAmountCredit > 0)
                                                                                                        {
                                                                                                            _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                                                        }
                                                                                                    }
                                                                                                }

                                                                                                _locCOA.Balance = _locTotalBalance;
                                                                                                _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                                                _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                                                _locCOA.Save();
                                                                                                _locCOA.Session.CommitTransaction();
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    #endregion JournalMapBusinessPartnerAccountGroup
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {

                                        if (_locPurchaseInvoiceLine.Tax != null)
                                        {
                                            _locTxOfAmount = _locPurchaseInvoiceLine.TxAmount;

                                            #region JournalMapTaxAccountGroup
                                            if (_locPurchaseInvoiceLine.Tax.TaxAccountGroup != null)
                                            {
                                                XPCollection<JournalMap> _locJournalMapByTaxs = new XPCollection<JournalMap>
                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                             new BinaryOperator("TaxAccountGroup", _locPurchaseInvoiceLine.Tax.TaxAccountGroup)));

                                                if (_locJournalMapByTaxs != null && _locJournalMapByTaxs.Count() > 0)
                                                {
                                                    foreach (JournalMap _locJournalMapByTax in _locJournalMapByTaxs)
                                                    {
                                                        XPCollection<JournalMapLine> _locJournalMapLineByTaxs = new XPCollection<JournalMapLine>
                                                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                 new BinaryOperator("JournalMap", _locJournalMapByTax)));

                                                        if (_locJournalMapLineByTaxs != null && _locJournalMapLineByTaxs.Count() > 0)
                                                        {
                                                            foreach (JournalMapLine _locJournalMapLineByTax in _locJournalMapLineByTaxs)
                                                            {
                                                                //Wahab 15-06-2020
                                                                AccountMap _locAccountMapByTax = _currSession.FindObject<AccountMap>
                                                                                                 (new GroupOperator(GroupOperatorType.And,
                                                                                                  new BinaryOperator("Code", _locJournalMapLineByTax.AccountMap.Code),
                                                                                                  new BinaryOperator("PostingType", PostingType.Purchase),
                                                                                                  new BinaryOperator("OrderType", OrderType.Item),
                                                                                                  new BinaryOperator("PostingMethod", PostingMethod.InvoiceAP),
                                                                                                  new BinaryOperator("PostingMethodType", PostingMethodType.Tax)));

                                                                if (_locAccountMapByTax != null)
                                                                {
                                                                    XPCollection<AccountMapLine> _locAccountMapLineByTaxs = new XPCollection<AccountMapLine>
                                                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                             new BinaryOperator("AccountMap", _locAccountMapByTax),
                                                                                                                             new BinaryOperator("Active", true)));

                                                                    if (_locAccountMapLineByTaxs != null && _locAccountMapLineByTaxs.Count() > 0)
                                                                    {
                                                                        double _locTotalAmountDebit = 0;
                                                                        double _locTotalAmountCredit = 0;
                                                                        double _locTotalBalance = 0;

                                                                        foreach (AccountMapLine _locAccountMapLineByTax in _locAccountMapLineByTaxs)
                                                                        {
                                                                            if (_locAccountMapLineByTax.AccountCharge == AccountCharge.Debit)
                                                                            {
                                                                                _locTotalAmountDebit = _locTxOfAmount;
                                                                            }
                                                                            if (_locAccountMapLineByTax.AccountCharge == AccountCharge.Credit)
                                                                            {
                                                                                _locTotalAmountCredit = _locTxOfAmount;
                                                                            }

                                                                            GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                            {
                                                                                PostingDate = now,
                                                                                PostingType = PostingType.Purchase,
                                                                                PostingMethod = PostingMethod.InvoiceAP,
                                                                                Account = _locAccountMapLineByTax.Account,
                                                                                Debit = _locTotalAmountDebit,
                                                                                Credit = _locTotalAmountCredit,
                                                                                PurchaseOrder = _locPurchaseInvoiceLine.PurchaseOrderMonitoring.PurchaseOrder,
                                                                                PurchaseInvoice = _locPurchaseInvoiceXPO,
                                                                                Company = _locPurchaseInvoiceXPO.Company,
                                                                            };
                                                                            _saveGeneralJournal.Save();
                                                                            _saveGeneralJournal.Session.CommitTransaction();

                                                                            if (_locAccountMapLineByTax.Account.Code != null)
                                                                            {
                                                                                ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                                         (new GroupOperator(GroupOperatorType.And,
                                                                                                          new BinaryOperator("Code", _locAccountMapLineByTax.Account.Code)));
                                                                                if (_locCOA != null)
                                                                                {
                                                                                    if (_locCOA.BalanceType == BalanceType.Change)
                                                                                    {
                                                                                        if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                                        {
                                                                                            if (_locTotalAmountDebit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                                            }
                                                                                            if (_locTotalAmountCredit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                                            }
                                                                                        }
                                                                                        if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                                        {
                                                                                            if (_locTotalAmountDebit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                                            }
                                                                                            if (_locTotalAmountCredit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                                            }
                                                                                        }
                                                                                    }

                                                                                    _locCOA.Balance = _locTotalBalance;
                                                                                    _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                                    _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                                    _locCOA.Save();
                                                                                    _locCOA.Session.CommitTransaction();
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }

                                            #endregion JournalMapTaxAccountGroup

                                            #region JournalMapBusinessPartnerAccountGroup
                                            if (_locPurchaseInvoiceXPO.BuyFromVendor != null)
                                            {
                                                if (_locPurchaseInvoiceXPO.BuyFromVendor.BusinessPartnerAccountGroup != null)
                                                {
                                                    XPCollection<JournalMap> _locJournalMapByBusinessPartners = new XPCollection<JournalMap>
                                                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                 new BinaryOperator("BusinessPartnerAccountGroup", _locPurchaseInvoiceXPO.BuyFromVendor.BusinessPartnerAccountGroup)));

                                                    if (_locJournalMapByBusinessPartners != null && _locJournalMapByBusinessPartners.Count() > 0)
                                                    {
                                                        foreach (JournalMap _locJournalMapByBusinessPartner in _locJournalMapByBusinessPartners)
                                                        {
                                                            XPCollection<JournalMapLine> _locJournalMapLineByBusinessPartners = new XPCollection<JournalMapLine>
                                                                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                                 new BinaryOperator("JournalMap", _locJournalMapByBusinessPartner)));

                                                            if (_locJournalMapLineByBusinessPartners != null && _locJournalMapLineByBusinessPartners.Count() > 0)
                                                            {
                                                                foreach (JournalMapLine _locJournalMapLineByBusinessPartner in _locJournalMapLineByBusinessPartners)
                                                                {
                                                                    AccountMap _locAccountMapByBusinessPartner = _currSession.FindObject<AccountMap>
                                                                                                                 (new GroupOperator(GroupOperatorType.And,
                                                                                                                  new BinaryOperator("Code", _locJournalMapLineByBusinessPartner.AccountMap.Code),
                                                                                                                  new BinaryOperator("PostingType", PostingType.Purchase),
                                                                                                                  new BinaryOperator("OrderType", OrderType.Item),
                                                                                                                  new BinaryOperator("PostingMethod", PostingMethod.InvoiceAP),
                                                                                                                  new BinaryOperator("PostingMethodType", PostingMethodType.Tax)));

                                                                    if (_locAccountMapByBusinessPartner != null)
                                                                    {
                                                                        XPCollection<AccountMapLine> _locAccountMapLineByBusinessPartners = new XPCollection<AccountMapLine>
                                                                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                                             new BinaryOperator("AccountMap", _locAccountMapByBusinessPartner),
                                                                                                                                             new BinaryOperator("Active", true)));

                                                                        if (_locAccountMapLineByBusinessPartners != null && _locAccountMapLineByBusinessPartners.Count() > 0)
                                                                        {
                                                                            double _locTotalAmountDebit = 0;
                                                                            double _locTotalAmountCredit = 0;
                                                                            double _locTotalBalance = 0;

                                                                            foreach (AccountMapLine _locAccountMapLine in _locAccountMapLineByBusinessPartners)
                                                                            {
                                                                                if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                                {
                                                                                    _locTotalAmountDebit = _locTxOfAmount;
                                                                                }
                                                                                if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                                {
                                                                                    _locTotalAmountCredit = _locTxOfAmount;
                                                                                }

                                                                                GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                                {
                                                                                    PostingDate = now,
                                                                                    PostingType = PostingType.Purchase,
                                                                                    PostingMethod = PostingMethod.InvoiceAP,
                                                                                    Account = _locAccountMapLine.Account,
                                                                                    Debit = _locTotalAmountDebit,
                                                                                    Credit = _locTotalAmountCredit,
                                                                                    PurchaseOrder = _locPurchaseInvoiceLine.PurchaseOrderMonitoring.PurchaseOrder,
                                                                                    PurchaseInvoice = _locPurchaseInvoiceXPO,
                                                                                    Company = _locPurchaseInvoiceXPO.Company,
                                                                                };
                                                                                _saveGeneralJournal.Save();
                                                                                _saveGeneralJournal.Session.CommitTransaction();

                                                                                if (_locAccountMapLine.Account.Code != null)
                                                                                {
                                                                                    ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                                    new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                                                                                    if (_locCOA != null)
                                                                                    {
                                                                                        if (_locCOA.BalanceType == BalanceType.Change)
                                                                                        {
                                                                                            if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                                            {
                                                                                                if (_locTotalAmountDebit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                                                }
                                                                                                if (_locTotalAmountCredit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                                                }
                                                                                            }
                                                                                            if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                                            {
                                                                                                if (_locTotalAmountDebit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                                                }
                                                                                                if (_locTotalAmountCredit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                                                }
                                                                                            }
                                                                                        }

                                                                                        _locCOA.Balance = _locTotalBalance;
                                                                                        _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                                        _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                                        _locCOA.Save();
                                                                                        _locCOA.Session.CommitTransaction();
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            #endregion JournalMapBusinessPartnerAccountGroup
                                        }
                                    }
                                    #endregion CreateInvoiceAPWithTax

                                    #region CreateInvoiceAPWithDiscount
                                    if (_locPurchaseInvoiceLine.MultiDiscount == true)
                                    {
                                        XPCollection<DiscountLine> _locDiscountLines = new XPCollection<DiscountLine>
                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("PurchaseInvoiceLine", _locPurchaseInvoiceLine)));
                                        if (_locDiscountLines != null && _locDiscountLines.Count() > 0)
                                        {
                                            foreach (DiscountLine _locDiscountLine in _locDiscountLines)
                                            {
                                                if (_locDiscountLine.Discount != null)
                                                {
                                                    _locDiscOfAmount = (_locDiscountLine.Disc * _locPurchaseInvoiceLine.TUAmount) / 100;

                                                    #region JournalMapDiscountAccountGroup
                                                    if (_locDiscountLine.Discount.DiscountAccountGroup != null)
                                                    {
                                                        XPCollection<JournalMap> _locJournalMapByDiscounts = new XPCollection<JournalMap>
                                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                     new BinaryOperator("DiscountAccountGroup", _locDiscountLine.Discount.DiscountAccountGroup)));

                                                        if (_locJournalMapByDiscounts != null && _locJournalMapByDiscounts.Count() > 0)
                                                        {
                                                            foreach (JournalMap _locJournalMapByDiscount in _locJournalMapByDiscounts)
                                                            {
                                                                XPCollection<JournalMapLine> _locJournalMapLineByDiscounts = new XPCollection<JournalMapLine>
                                                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                         new BinaryOperator("JournalMap", _locJournalMapByDiscount)));

                                                                if (_locJournalMapLineByDiscounts != null && _locJournalMapLineByDiscounts.Count() > 0)
                                                                {
                                                                    foreach (JournalMapLine _locJournalMapLineByDiscount in _locJournalMapLineByDiscounts)
                                                                    {
                                                                        AccountMap _locAccountMapByDiscount = _currSession.FindObject<AccountMap>
                                                                                                         (new GroupOperator(GroupOperatorType.And,
                                                                                                          new BinaryOperator("Code", _locJournalMapLineByDiscount.AccountMap.Code),
                                                                                                          new BinaryOperator("PostingType", PostingType.Purchase),
                                                                                                          new BinaryOperator("OrderType", _locPurchaseInvoiceLine.PurchaseType),
                                                                                                          new BinaryOperator("PostingMethod", PostingMethod.InvoiceAP),
                                                                                                          new BinaryOperator("PostingMethodType", PostingMethodType.Discount)));

                                                                        if (_locAccountMapByDiscount != null)
                                                                        {
                                                                            XPCollection<AccountMapLine> _locAccountMapLineByDiscounts = new XPCollection<AccountMapLine>
                                                                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                                     new BinaryOperator("AccountMap", _locAccountMapByDiscount),
                                                                                                                                     new BinaryOperator("Active", true)));

                                                                            if (_locAccountMapLineByDiscounts != null && _locAccountMapLineByDiscounts.Count() > 0)
                                                                            {
                                                                                double _locTotalAmountDebit = 0;
                                                                                double _locTotalAmountCredit = 0;
                                                                                double _locTotalBalance = 0;

                                                                                foreach (AccountMapLine _locAccountMapLineByDiscount in _locAccountMapLineByDiscounts)
                                                                                {
                                                                                    if (_locAccountMapLineByDiscount.AccountCharge == AccountCharge.Debit)
                                                                                    {
                                                                                        _locTotalAmountDebit = _locDiscOfAmount;
                                                                                    }
                                                                                    if (_locAccountMapLineByDiscount.AccountCharge == AccountCharge.Credit)
                                                                                    {
                                                                                        _locTotalAmountCredit = _locDiscOfAmount;
                                                                                    }

                                                                                    GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                                    {
                                                                                        PostingDate = now,
                                                                                        PostingType = PostingType.Purchase,
                                                                                        OrderType = _locPurchaseInvoiceLine.PurchaseType,
                                                                                        PostingMethod = PostingMethod.InvoiceAP,
                                                                                        PostingMethodType = PostingMethodType.Discount,
                                                                                        Account = _locAccountMapLineByDiscount.Account,
                                                                                        Debit = _locTotalAmountDebit,
                                                                                        Credit = _locTotalAmountCredit,
                                                                                        PurchaseOrder = _locPurchaseInvoiceLine.PurchaseOrderMonitoring.PurchaseOrder,
                                                                                        PurchaseInvoice = _locPurchaseInvoiceXPO,
                                                                                        Company = _locPurchaseInvoiceXPO.Company,
                                                                                    };
                                                                                    _saveGeneralJournal.Save();
                                                                                    _saveGeneralJournal.Session.CommitTransaction();

                                                                                    if (_locAccountMapLineByDiscount.Account.Code != null)
                                                                                    {
                                                                                        ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                                                 (new GroupOperator(GroupOperatorType.And,
                                                                                                                  new BinaryOperator("Code", _locAccountMapLineByDiscount.Account.Code)));
                                                                                        if (_locCOA != null)
                                                                                        {
                                                                                            if (_locCOA.BalanceType == BalanceType.Change)
                                                                                            {
                                                                                                if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                                                {
                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                                                    }
                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                                                    }
                                                                                                }
                                                                                                if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                                                {
                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                                                    }
                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                                                    }
                                                                                                }
                                                                                            }

                                                                                            _locCOA.Balance = _locTotalBalance;
                                                                                            _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                                            _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                                            _locCOA.Save();
                                                                                            _locCOA.Session.CommitTransaction();
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    #endregion JournalMapDiscountAccountGroup

                                                    #region JournalMapBusinessPartnerAccountGroup
                                                    if (_locPurchaseInvoiceXPO.BuyFromVendor != null)
                                                    {
                                                        if (_locPurchaseInvoiceXPO.BuyFromVendor.BusinessPartnerAccountGroup != null)
                                                        {
                                                            XPCollection<JournalMap> _locJournalMapByBusinessPartners = new XPCollection<JournalMap>
                                                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                         new BinaryOperator("BusinessPartnerAccountGroup", _locPurchaseInvoiceXPO.BuyFromVendor.BusinessPartnerAccountGroup)));

                                                            if (_locJournalMapByBusinessPartners != null && _locJournalMapByBusinessPartners.Count() > 0)
                                                            {
                                                                foreach (JournalMap _locJournalMapByBusinessPartner in _locJournalMapByBusinessPartners)
                                                                {
                                                                    XPCollection<JournalMapLine> _locJournalMapLineByBusinessPartners = new XPCollection<JournalMapLine>
                                                                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                                         new BinaryOperator("JournalMap", _locJournalMapByBusinessPartner)));

                                                                    if (_locJournalMapLineByBusinessPartners != null && _locJournalMapLineByBusinessPartners.Count() > 0)
                                                                    {
                                                                        foreach (JournalMapLine _locJournalMapLineByBusinessPartner in _locJournalMapLineByBusinessPartners)
                                                                        {
                                                                            AccountMap _locAccountMapByBusinessPartner = _currSession.FindObject<AccountMap>
                                                                                                                         (new GroupOperator(GroupOperatorType.And,
                                                                                                                          new BinaryOperator("Code", _locJournalMapLineByBusinessPartner.AccountMap.Code),
                                                                                                                          new BinaryOperator("PostingType", PostingType.Purchase),
                                                                                                                          new BinaryOperator("OrderType", _locPurchaseInvoiceLine.PurchaseType),
                                                                                                                          new BinaryOperator("PostingMethod", PostingMethod.InvoiceAP),
                                                                                                                          new BinaryOperator("PostingMethodType", PostingMethodType.Discount)));

                                                                            if (_locAccountMapByBusinessPartner != null)
                                                                            {
                                                                                XPCollection<AccountMapLine> _locAccountMapLineByBusinessPartners = new XPCollection<AccountMapLine>
                                                                                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                                                     new BinaryOperator("AccountMap", _locAccountMapByBusinessPartner),
                                                                                                                                                     new BinaryOperator("Active", true)));

                                                                                if (_locAccountMapLineByBusinessPartners != null && _locAccountMapLineByBusinessPartners.Count() > 0)
                                                                                {
                                                                                    double _locTotalAmountDebit = 0;
                                                                                    double _locTotalAmountCredit = 0;
                                                                                    double _locTotalBalance = 0;

                                                                                    foreach (AccountMapLine _locAccountMapLine in _locAccountMapLineByBusinessPartners)
                                                                                    {
                                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                                        {
                                                                                            _locTotalAmountDebit = _locDiscOfAmount;
                                                                                        }
                                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                                        {
                                                                                            _locTotalAmountCredit = _locDiscOfAmount;
                                                                                        }

                                                                                        GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                                        {
                                                                                            PostingDate = now,
                                                                                            PostingType = PostingType.Purchase,
                                                                                            OrderType = _locPurchaseInvoiceLine.PurchaseType,
                                                                                            PostingMethod = PostingMethod.InvoiceAP,
                                                                                            PostingMethodType = PostingMethodType.Discount,
                                                                                            Account = _locAccountMapLine.Account,
                                                                                            Debit = _locTotalAmountDebit,
                                                                                            Credit = _locTotalAmountCredit,
                                                                                            PurchaseOrder = _locPurchaseInvoiceLine.PurchaseOrderMonitoring.PurchaseOrder,
                                                                                            PurchaseInvoice = _locPurchaseInvoiceXPO,
                                                                                            Company = _locPurchaseInvoiceXPO.Company,
                                                                                        };
                                                                                        _saveGeneralJournal.Save();
                                                                                        _saveGeneralJournal.Session.CommitTransaction();

                                                                                        if (_locAccountMapLine.Account.Code != null)
                                                                                        {
                                                                                            ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                                                     (new GroupOperator(GroupOperatorType.And,
                                                                                                                      new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                                                                                            if (_locCOA != null)
                                                                                            {
                                                                                                if (_locCOA.BalanceType == BalanceType.Change)
                                                                                                {
                                                                                                    if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                                                    {
                                                                                                        if (_locTotalAmountDebit > 0)
                                                                                                        {
                                                                                                            _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                                                        }
                                                                                                        if (_locTotalAmountCredit > 0)
                                                                                                        {
                                                                                                            _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                                                        }
                                                                                                    }
                                                                                                    if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                                                    {
                                                                                                        if (_locTotalAmountDebit > 0)
                                                                                                        {
                                                                                                            _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                                                        }
                                                                                                        if (_locTotalAmountCredit > 0)
                                                                                                        {
                                                                                                            _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                                                        }
                                                                                                    }
                                                                                                }

                                                                                                _locCOA.Balance = _locTotalBalance;
                                                                                                _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                                                _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                                                _locCOA.Save();
                                                                                                _locCOA.Session.CommitTransaction();
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    #endregion JournalMapBusinessPartnerAccountGroup
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (_locPurchaseInvoiceLine.Discount != null)
                                        {
                                            _locDiscOfAmount = _locPurchaseInvoiceLine.DiscAmount;

                                            #region JournalMapDiscountAccountGroup
                                            if (_locPurchaseInvoiceLine.Discount.DiscountAccountGroup != null)
                                            {
                                                XPCollection<JournalMap> _locJournalMapByDiscounts = new XPCollection<JournalMap>
                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                             new BinaryOperator("DiscountAccountGroup", _locPurchaseInvoiceLine.Discount.DiscountAccountGroup)));

                                                if (_locJournalMapByDiscounts != null && _locJournalMapByDiscounts.Count() > 0)
                                                {
                                                    foreach (JournalMap _locJournalMapByDiscount in _locJournalMapByDiscounts)
                                                    {
                                                        XPCollection<JournalMapLine> _locJournalMapLineByDiscounts = new XPCollection<JournalMapLine>
                                                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                 new BinaryOperator("JournalMap", _locJournalMapByDiscount)));

                                                        if (_locJournalMapLineByDiscounts != null && _locJournalMapLineByDiscounts.Count() > 0)
                                                        {
                                                            foreach (JournalMapLine _locJournalMapLineByDiscount in _locJournalMapLineByDiscounts)
                                                            {
                                                                AccountMap _locAccountMapByDiscount = _currSession.FindObject<AccountMap>
                                                                                                 (new GroupOperator(GroupOperatorType.And,
                                                                                                  new BinaryOperator("Code", _locJournalMapLineByDiscount.AccountMap.Code),
                                                                                                  new BinaryOperator("PostingType", PostingType.Purchase),
                                                                                                  new BinaryOperator("OrderType", _locPurchaseInvoiceLine.PurchaseType),
                                                                                                  new BinaryOperator("PostingMethod", PostingMethod.InvoiceAP),
                                                                                                  new BinaryOperator("PostingMethodType", PostingMethodType.Discount)));

                                                                if (_locAccountMapByDiscount != null)
                                                                {
                                                                    XPCollection<AccountMapLine> _locAccountMapLineByDiscounts = new XPCollection<AccountMapLine>
                                                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                             new BinaryOperator("AccountMap", _locAccountMapByDiscount),
                                                                                                                             new BinaryOperator("Active", true)));

                                                                    if (_locAccountMapLineByDiscounts != null && _locAccountMapLineByDiscounts.Count() > 0)
                                                                    {
                                                                        double _locTotalAmountDebit = 0;
                                                                        double _locTotalAmountCredit = 0;
                                                                        double _locTotalBalance = 0;

                                                                        foreach (AccountMapLine _locAccountMapLineByDiscount in _locAccountMapLineByDiscounts)
                                                                        {
                                                                            if (_locAccountMapLineByDiscount.AccountCharge == AccountCharge.Debit)
                                                                            {
                                                                                _locTotalAmountDebit = _locDiscOfAmount;
                                                                            }
                                                                            if (_locAccountMapLineByDiscount.AccountCharge == AccountCharge.Credit)
                                                                            {
                                                                                _locTotalAmountCredit = _locDiscOfAmount;
                                                                            }

                                                                            GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                            {
                                                                                PostingDate = now,
                                                                                PostingType = PostingType.Purchase,
                                                                                OrderType = _locPurchaseInvoiceLine.PurchaseType,
                                                                                PostingMethod = PostingMethod.InvoiceAP,
                                                                                PostingMethodType = PostingMethodType.Discount,
                                                                                Account = _locAccountMapLineByDiscount.Account,
                                                                                Debit = _locTotalAmountDebit,
                                                                                Credit = _locTotalAmountCredit,
                                                                                PurchaseOrder = _locPurchaseInvoiceLine.PurchaseOrderMonitoring.PurchaseOrder,
                                                                                PurchaseInvoice = _locPurchaseInvoiceXPO,
                                                                                Company = _locPurchaseInvoiceXPO.Company,
                                                                            };
                                                                            _saveGeneralJournal.Save();
                                                                            _saveGeneralJournal.Session.CommitTransaction();

                                                                            if (_locAccountMapLineByDiscount.Account.Code != null)
                                                                            {
                                                                                ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                                         (new GroupOperator(GroupOperatorType.And,
                                                                                                          new BinaryOperator("Code", _locAccountMapLineByDiscount.Account.Code)));
                                                                                if (_locCOA != null)
                                                                                {
                                                                                    if (_locCOA.BalanceType == BalanceType.Change)
                                                                                    {
                                                                                        if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                                        {
                                                                                            if (_locTotalAmountDebit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                                            }
                                                                                            if (_locTotalAmountCredit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                                            }
                                                                                        }
                                                                                        if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                                        {
                                                                                            if (_locTotalAmountDebit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                                            }
                                                                                            if (_locTotalAmountCredit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                                            }
                                                                                        }
                                                                                    }

                                                                                    _locCOA.Balance = _locTotalBalance;
                                                                                    _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                                    _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                                    _locCOA.Save();
                                                                                    _locCOA.Session.CommitTransaction();
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }

                                            #endregion JournalMapDiscountAccountGroup

                                            #region JournalMapBusinessPartnerAccountGroup
                                            if (_locPurchaseInvoiceXPO.BuyFromVendor != null)
                                            {
                                                if (_locPurchaseInvoiceXPO.BuyFromVendor.BusinessPartnerAccountGroup != null)
                                                {
                                                    XPCollection<JournalMap> _locJournalMapByBusinessPartners = new XPCollection<JournalMap>
                                                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                 new BinaryOperator("BusinessPartnerAccountGroup", _locPurchaseInvoiceXPO.BuyFromVendor.BusinessPartnerAccountGroup)));

                                                    if (_locJournalMapByBusinessPartners != null && _locJournalMapByBusinessPartners.Count() > 0)
                                                    {
                                                        foreach (JournalMap _locJournalMapByBusinessPartner in _locJournalMapByBusinessPartners)
                                                        {
                                                            XPCollection<JournalMapLine> _locJournalMapLineByBusinessPartners = new XPCollection<JournalMapLine>
                                                                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                                 new BinaryOperator("JournalMap", _locJournalMapByBusinessPartner)));

                                                            if (_locJournalMapLineByBusinessPartners != null && _locJournalMapLineByBusinessPartners.Count() > 0)
                                                            {
                                                                foreach (JournalMapLine _locJournalMapLineByBusinessPartner in _locJournalMapLineByBusinessPartners)
                                                                {
                                                                    AccountMap _locAccountMapByBusinessPartner = _currSession.FindObject<AccountMap>
                                                                                                                 (new GroupOperator(GroupOperatorType.And,
                                                                                                                  new BinaryOperator("Code", _locJournalMapLineByBusinessPartner.AccountMap.Code),
                                                                                                                  new BinaryOperator("PostingType", PostingType.Purchase),
                                                                                                                  new BinaryOperator("OrderType", _locPurchaseInvoiceLine.PurchaseType),
                                                                                                                  new BinaryOperator("PostingMethod", PostingMethod.InvoiceAP),
                                                                                                                  new BinaryOperator("PostingMethodType", PostingMethodType.Discount)));

                                                                    if (_locAccountMapByBusinessPartner != null)
                                                                    {
                                                                        XPCollection<AccountMapLine> _locAccountMapLineByBusinessPartners = new XPCollection<AccountMapLine>
                                                                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                                             new BinaryOperator("AccountMap", _locAccountMapByBusinessPartner),
                                                                                                                                             new BinaryOperator("Active", true)));

                                                                        if (_locAccountMapLineByBusinessPartners != null && _locAccountMapLineByBusinessPartners.Count() > 0)
                                                                        {
                                                                            double _locTotalAmountDebit = 0;
                                                                            double _locTotalAmountCredit = 0;
                                                                            double _locTotalBalance = 0;

                                                                            foreach (AccountMapLine _locAccountMapLine in _locAccountMapLineByBusinessPartners)
                                                                            {
                                                                                if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                                {
                                                                                    _locTotalAmountDebit = _locDiscOfAmount;
                                                                                }
                                                                                if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                                {
                                                                                    _locTotalAmountCredit = _locDiscOfAmount;
                                                                                }

                                                                                GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                                {
                                                                                    PostingDate = now,
                                                                                    PostingType = PostingType.Purchase,
                                                                                    OrderType = _locPurchaseInvoiceLine.PurchaseType,
                                                                                    PostingMethod = PostingMethod.InvoiceAP,
                                                                                    PostingMethodType = PostingMethodType.Discount,
                                                                                    Account = _locAccountMapLine.Account,
                                                                                    Debit = _locTotalAmountDebit,
                                                                                    Credit = _locTotalAmountCredit,
                                                                                    PurchaseOrder = _locPurchaseInvoiceLine.PurchaseOrderMonitoring.PurchaseOrder,
                                                                                    PurchaseInvoice = _locPurchaseInvoiceXPO,
                                                                                    Company = _locPurchaseInvoiceXPO.Company,
                                                                                };
                                                                                _saveGeneralJournal.Save();
                                                                                _saveGeneralJournal.Session.CommitTransaction();

                                                                                if (_locAccountMapLine.Account.Code != null)
                                                                                {
                                                                                    ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                                             (new GroupOperator(GroupOperatorType.And,
                                                                                                              new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                                                                                    if (_locCOA != null)
                                                                                    {
                                                                                        if (_locCOA.BalanceType == BalanceType.Change)
                                                                                        {
                                                                                            if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                                            {
                                                                                                if (_locTotalAmountDebit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                                                }
                                                                                                if (_locTotalAmountCredit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                                                }
                                                                                            }
                                                                                            if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                                            {
                                                                                                if (_locTotalAmountDebit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                                                }
                                                                                                if (_locTotalAmountCredit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                                                }
                                                                                            }
                                                                                        }

                                                                                        _locCOA.Balance = _locTotalBalance;
                                                                                        _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                                        _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                                        _locCOA.Save();
                                                                                        _locCOA.Session.CommitTransaction();
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            #endregion JournalMapBusinessPartnerAccountGroup
                                        }

                                    }
                                    #endregion CreateInvoiceAPWithDiscount
                                }
                            }
                        }
                    }

                    #endregion CreateInvoiceARJournal
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PurchaseInvoice ", ex.ToString());
            }
        }

        private void SetNormalPay(Session _currSession, PurchaseInvoice _locPurchaseInvoiceXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;

                if (_locPurchaseInvoiceXPO != null)
                {
                    _locPurchaseInvoiceXPO.Pay = 0;
                    _locPurchaseInvoiceXPO.Save();
                    _locPurchaseInvoiceXPO.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PurchaseInvoice ", ex.ToString());
            }
        }

        private void SetRemainQty(Session _currSession, PurchaseInvoice _locPurchaseInvoiceXPO)
        {
            try
            {
                if (_locPurchaseInvoiceXPO != null)
                {
                    XPCollection<PurchaseInvoiceLine> _locPurchaseInvoiceLines = new XPCollection<PurchaseInvoiceLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Lock),
                                                            new BinaryOperator("Status", Status.Posted))));

                    if (_locPurchaseInvoiceLines != null && _locPurchaseInvoiceLines.Count > 0)
                    {
                        double _locRmDQty = 0;
                        double _locRmQty = 0;
                        double _locInvLineTotal = 0;
                        ItemUnitOfMeasure _locItemUOM = null;

                        foreach (PurchaseInvoiceLine _locPurchaseInvoiceLine in _locPurchaseInvoiceLines)
                        {
                            #region ProcessCount=0
                            if (_locPurchaseInvoiceLine.ProcessCount == 0)
                            {
                                #region MaxQuantity
                                if (_locPurchaseInvoiceLine.MxDQty > 0)
                                {
                                    if (_locPurchaseInvoiceLine.DQty > 0 && _locPurchaseInvoiceLine.DQty <= _locPurchaseInvoiceLine.MxDQty)
                                    {
                                        _locRmDQty = _locPurchaseInvoiceLine.MxDQty - _locPurchaseInvoiceLine.DQty;
                                    }

                                    if (_locPurchaseInvoiceLine.Qty > 0 && _locPurchaseInvoiceLine.Qty <= _locPurchaseInvoiceLine.MxQty)
                                    {
                                        _locRmQty = _locPurchaseInvoiceLine.MxQty - _locPurchaseInvoiceLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locPurchaseInvoiceLine.Item),
                                                                new BinaryOperator("UOM", _locPurchaseInvoiceLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locPurchaseInvoiceLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty + _locRmDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }

                                }
                                #endregion MaxQuantity

                                #region NonMaxQuantity
                                else
                                {
                                    if (_locPurchaseInvoiceLine.DQty > 0)
                                    {
                                        _locRmDQty = 0;
                                    }

                                    if (_locPurchaseInvoiceLine.Qty > 0)
                                    {
                                        _locRmQty = 0;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locPurchaseInvoiceLine.Item),
                                                                new BinaryOperator("UOM", _locPurchaseInvoiceLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locPurchaseInvoiceLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty + _locRmDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion ProcessCount=0

                            #region ProcessCount>0
                            if (_locPurchaseInvoiceLine.ProcessCount > 0)
                            {
                                if (_locPurchaseInvoiceLine.RmDQty > 0)
                                {
                                    _locRmDQty = _locPurchaseInvoiceLine.RmDQty - _locPurchaseInvoiceLine.DQty;
                                }

                                if (_locPurchaseInvoiceLine.RmQty > 0)
                                {
                                    _locRmQty = _locPurchaseInvoiceLine.RmQty - _locPurchaseInvoiceLine.Qty;
                                }

                                if (_locPurchaseInvoiceLine.MxDQty > 0 || _locPurchaseInvoiceLine.MxQty > 0)
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locPurchaseInvoiceLine.Item),
                                                                new BinaryOperator("UOM", _locPurchaseInvoiceLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locPurchaseInvoiceLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                }
                                else
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locPurchaseInvoiceLine.Item),
                                                                new BinaryOperator("UOM", _locPurchaseInvoiceLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locPurchaseInvoiceLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                }


                                if (_locItemUOM != null)
                                {
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locRmQty + _locRmDQty;
                                }

                            }
                            #endregion ProcessCount>0

                            _locPurchaseInvoiceLine.RmDQty = _locRmDQty;
                            _locPurchaseInvoiceLine.RmQty = _locRmQty;
                            _locPurchaseInvoiceLine.RmTQty = _locInvLineTotal;
                            _locPurchaseInvoiceLine.Save();
                            _locPurchaseInvoiceLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseInvoice " + ex.ToString());
            }
        }

        private void SetPostingQty(Session _currSession, PurchaseInvoice _locPurchaseInvoiceXPO)
        {
            try
            {
                if (_locPurchaseInvoiceXPO != null)
                {
                    XPCollection<PurchaseInvoiceLine> _locPurchaseInvoiceLines = new XPCollection<PurchaseInvoiceLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Lock),
                                                            new BinaryOperator("Status", Status.Posted))));

                    if (_locPurchaseInvoiceLines != null && _locPurchaseInvoiceLines.Count > 0)
                    {
                        double _locPDQty = 0;
                        double _locPQty = 0;
                        double _locInvLineTotal = 0;
                        ItemUnitOfMeasure _locItemUOM = null;

                        foreach (PurchaseInvoiceLine _locPurchaseInvoiceLine in _locPurchaseInvoiceLines)
                        {
                            #region ProcessCount=0
                            if (_locPurchaseInvoiceLine.ProcessCount == 0)
                            {
                                #region MaxQuantity
                                if (_locPurchaseInvoiceLine.MxDQty > 0)
                                {
                                    if (_locPurchaseInvoiceLine.DQty > 0 && _locPurchaseInvoiceLine.DQty <= _locPurchaseInvoiceLine.MxDQty)
                                    {
                                        _locPDQty = _locPurchaseInvoiceLine.DQty;
                                    }

                                    if (_locPurchaseInvoiceLine.Qty > 0 && _locPurchaseInvoiceLine.Qty <= _locPurchaseInvoiceLine.MxQty)
                                    {
                                        _locPQty = _locPurchaseInvoiceLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locPurchaseInvoiceLine.Item),
                                                                new BinaryOperator("UOM", _locPurchaseInvoiceLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locPurchaseInvoiceLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty + _locPDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }

                                }
                                #endregion MaxQuantity

                                #region NonMaxQuantity
                                else
                                {
                                    if (_locPurchaseInvoiceLine.DQty > 0)
                                    {
                                        _locPDQty = _locPurchaseInvoiceLine.DQty;
                                    }

                                    if (_locPurchaseInvoiceLine.Qty > 0)
                                    {
                                        _locPQty = _locPurchaseInvoiceLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locPurchaseInvoiceLine.Item),
                                                                new BinaryOperator("UOM", _locPurchaseInvoiceLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locPurchaseInvoiceLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty + _locPDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion ProcessCount=0

                            #region ProcessCount>0
                            if (_locPurchaseInvoiceLine.ProcessCount > 0)
                            {
                                if (_locPurchaseInvoiceLine.PDQty > 0)
                                {
                                    _locPDQty = _locPurchaseInvoiceLine.PDQty + _locPurchaseInvoiceLine.DQty;
                                }

                                if (_locPurchaseInvoiceLine.PQty > 0)
                                {
                                    _locPQty = _locPurchaseInvoiceLine.PQty + _locPurchaseInvoiceLine.Qty;
                                }

                                if (_locPurchaseInvoiceLine.MxDQty > 0 || _locPurchaseInvoiceLine.MxQty > 0)
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                           (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Item", _locPurchaseInvoiceLine.Item),
                                                            new BinaryOperator("UOM", _locPurchaseInvoiceLine.MxUOM),
                                                            new BinaryOperator("DefaultUOM", _locPurchaseInvoiceLine.MxDUOM),
                                                            new BinaryOperator("Active", true)));
                                }
                                else
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                           (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Item", _locPurchaseInvoiceLine.Item),
                                                            new BinaryOperator("UOM", _locPurchaseInvoiceLine.UOM),
                                                            new BinaryOperator("DefaultUOM", _locPurchaseInvoiceLine.DUOM),
                                                            new BinaryOperator("Active", true)));
                                }

                                if (_locItemUOM != null)
                                {
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locPQty + _locPDQty;
                                }

                            }
                            #endregion ProcessCount>0

                            _locPurchaseInvoiceLine.PDQty = _locPDQty;
                            _locPurchaseInvoiceLine.PDUOM = _locPurchaseInvoiceLine.DUOM;
                            _locPurchaseInvoiceLine.PQty = _locPQty;
                            _locPurchaseInvoiceLine.PUOM = _locPurchaseInvoiceLine.UOM;
                            _locPurchaseInvoiceLine.PTQty = _locInvLineTotal;
                            _locPurchaseInvoiceLine.Save();
                            _locPurchaseInvoiceLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseInvoice " + ex.ToString());
            }
        }

        private void SetProcessCount(Session _currSession, PurchaseInvoice _locPurchaseInvoiceXPO)
        {
            try
            {
                if (_locPurchaseInvoiceXPO != null)
                {
                    XPCollection<PurchaseInvoiceLine> _locPurchaseInvoiceLines = new XPCollection<PurchaseInvoiceLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Lock),
                                                            new BinaryOperator("Status", Status.Posted))));

                    if (_locPurchaseInvoiceLines != null && _locPurchaseInvoiceLines.Count > 0)
                    {

                        foreach (PurchaseInvoiceLine _locPurchaseInvoiceLine in _locPurchaseInvoiceLines)
                        {
                            _locPurchaseInvoiceLine.ProcessCount = _locPurchaseInvoiceLine.ProcessCount + 1;
                            _locPurchaseInvoiceLine.Save();
                            _locPurchaseInvoiceLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseInvoice " + ex.ToString());
            }
        }

        private void SetStatusPurchaseInvoiceLine(Session _currSession, PurchaseInvoice _locPurchaseInvoiceXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locPurchaseInvoiceXPO != null)
                {
                    XPCollection<PurchaseInvoiceLine> _locPurchaseInvoiceLines = new XPCollection<PurchaseInvoiceLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Lock),
                                                            new BinaryOperator("Status", Status.Posted))));

                    if (_locPurchaseInvoiceLines != null && _locPurchaseInvoiceLines.Count > 0)
                    {

                        foreach (PurchaseInvoiceLine _locPurchaseInvoiceLine in _locPurchaseInvoiceLines)
                        {
                            if (_locPurchaseInvoiceLine.RmDQty == 0 && _locPurchaseInvoiceLine.RmQty == 0 && _locPurchaseInvoiceLine.RmTQty == 0)
                            {
                                _locPurchaseInvoiceLine.Status = Status.Close;
                                _locPurchaseInvoiceLine.ActivationPosting = true;
                                _locPurchaseInvoiceLine.StatusDate = now;
                            }
                            else
                            {
                                _locPurchaseInvoiceLine.Status = Status.Posted;
                                _locPurchaseInvoiceLine.StatusDate = now;
                            }
                            _locPurchaseInvoiceLine.Save();
                            _locPurchaseInvoiceLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseInvoice " + ex.ToString());
            }
        }

        private void SetNormalQuantity(Session _currSession, PurchaseInvoice _locPurchaseInvoiceXPO)
        {
            try
            {
                if (_locPurchaseInvoiceXPO != null)
                {
                    XPCollection<PurchaseInvoiceLine> _locPurchaseInvoiceLines = new XPCollection<PurchaseInvoiceLine>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO),
                                                                                     new BinaryOperator("Select", true),
                                                                                     new GroupOperator(GroupOperatorType.Or,
                                                                                     new BinaryOperator("Status", Status.Lock),
                                                                                     new BinaryOperator("Status", Status.Posted),
                                                                                     new BinaryOperator("Status", Status.Close))));

                    if (_locPurchaseInvoiceLines != null && _locPurchaseInvoiceLines.Count > 0)
                    {
                        foreach (PurchaseInvoiceLine _locPurchaseInvoiceLine in _locPurchaseInvoiceLines)
                        {
                            if (_locPurchaseInvoiceLine.DQty > 0 || _locPurchaseInvoiceLine.Qty > 0)
                            {
                                _locPurchaseInvoiceLine.Select = false;
                                _locPurchaseInvoiceLine.DQty = 0;
                                _locPurchaseInvoiceLine.Qty = 0;
                                _locPurchaseInvoiceLine.Save();
                                _locPurchaseInvoiceLine.Session.CommitTransaction();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseInvoice " + ex.ToString());
            }
        }

        private void SetFinalPurchaseInvoice(Session _currSession, PurchaseInvoice _locPurchaseInvoiceXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locStatusCount = 0;
                int _locPurchInvLineCount = 0;

                if (_locPurchaseInvoiceXPO != null)
                {

                    XPCollection<PurchaseInvoiceLine> _locPurchaseInvoiceLines = new XPCollection<PurchaseInvoiceLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO)));

                    if (_locPurchaseInvoiceLines != null && _locPurchaseInvoiceLines.Count() > 0)
                    {
                        _locPurchInvLineCount = _locPurchaseInvoiceLines.Count();

                        foreach (PurchaseInvoiceLine _locPurchaseInvoiceLine in _locPurchaseInvoiceLines)
                        {
                            if (_locPurchaseInvoiceLine.Status == Status.Close)
                            {
                                _locStatusCount = _locStatusCount + 1;
                            }
                        }
                    }

                    if (_locStatusCount == _locPurchInvLineCount)
                    {
                        _locPurchaseInvoiceXPO.ActivationPosting = true;
                        _locPurchaseInvoiceXPO.Status = Status.Posted;
                        _locPurchaseInvoiceXPO.StatusDate = now;
                        _locPurchaseInvoiceXPO.Save();
                        _locPurchaseInvoiceXPO.Session.CommitTransaction();
                    }
                    else
                    {
                        _locPurchaseInvoiceXPO.Status = Status.Posted;
                        _locPurchaseInvoiceXPO.StatusDate = now;
                        _locPurchaseInvoiceXPO.Save();
                        _locPurchaseInvoiceXPO.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseInvoice " + ex.ToString());
            }
        }

        private void SetAfterPostingReport(Session _currSession, PurchaseInvoice _locPurchaseInvoiceXPO)
        {
            //Memindahkan Nilai TotalPIL, TUAmount, ReportTPIL, ReportTDisc, ReportTaxPIL ke field Posting Untuk Report After Posting
            try
            {
                DateTime now = DateTime.Now;

                if (_locPurchaseInvoiceXPO != null)
                {
                    XPCollection<PurchaseInvoiceLine> _locPurchaseInvoiceLines = new XPCollection<PurchaseInvoiceLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO)));

                    if (_locPurchaseInvoiceLines != null && _locPurchaseInvoiceLines.Count() > 0)
                    {
                        foreach (PurchaseInvoiceLine _locPurchaseInvoiceLine in _locPurchaseInvoiceLines)
                        {
                            _locPurchaseInvoiceLine.PTotalPIL = _locPurchaseInvoiceLine.TotalPIL;
                            _locPurchaseInvoiceLine.PTUAmount = _locPurchaseInvoiceLine.TUAmount;
                            _locPurchaseInvoiceLine.PTAmountPIL = _locPurchaseInvoiceLine.TAmountPIL;
                            _locPurchaseInvoiceLine.PTPIL = _locPurchaseInvoiceLine.ReportTPIL;
                            _locPurchaseInvoiceLine.PDisc = _locPurchaseInvoiceLine.ReportDisc;
                            _locPurchaseInvoiceLine.PTax = _locPurchaseInvoiceLine.ReportTaxPIL;
                            _locPurchaseInvoiceLine.Save();
                            _locPurchaseInvoiceLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseInvoice " + ex.ToString());
            }
        }

        #endregion Posting Method

        #region PostingMethodOld
        //24 Jan 2020

        //24 Jan 2020


        private void SetInvoiceAPJournalOld(Session _currSession, PurchaseInvoice _locPurchaseInvoiceXPO)
        {
            try
            {
                DateTime now = DateTime.Now;

                #region CreateInvoiceAPJournal

                //Check Nullable TaxAccountGroup
                if (GetNullPurchaseInvoiceLineTaxOld(_currSession, _locPurchaseInvoiceXPO) == false)
                {
                    #region CreateInvoiceAPJournalByItemsWithTax
                    XPQuery<PurchaseInvoiceLine> _purchaseInvoiceLinesQueryBasedTax = new XPQuery<PurchaseInvoiceLine>(_currSession);

                    var _purchaseInvoiceLineBasedTaxs = from pil in _purchaseInvoiceLinesQueryBasedTax
                                                        where (pil.PurchaseInvoice == _locPurchaseInvoiceXPO
                                                        && (pil.Status == Status.Progress || pil.Status == Status.Posted)
                                                        && pil.Select == true)
                                                        group pil by pil.Tax.TaxAccountGroup into g
                                                        select new { TaxAccountGroup = g.Key };

                    if (_purchaseInvoiceLineBasedTaxs != null && _purchaseInvoiceLineBasedTaxs.Count() > 0)
                    {
                        foreach (var _purchaseInvoiceLineBasedTax in _purchaseInvoiceLineBasedTaxs)
                        {
                            #region CreateInvoiceAPJournal
                            //Kalau discountnya pergroup yang berbeda maka tinggal buat Discount.DiscountAccountGroup di bawah ItemAccountGroup
                            XPQuery<PurchaseInvoiceLine> _purchaseInvoiceLinesQuery = new XPQuery<PurchaseInvoiceLine>(_currSession);

                            var _purchaseInvoiceLines = from pil in _purchaseInvoiceLinesQuery
                                                        where (pil.PurchaseInvoice == _locPurchaseInvoiceXPO
                                                        && pil.Tax.TaxAccountGroup == _purchaseInvoiceLineBasedTax.TaxAccountGroup
                                                        && (pil.Status == Status.Progress || pil.Status == Status.Posted))
                                                        group pil by pil.Item.ItemAccountGroup into g
                                                        select new { ItemAccountGroup = g.Key };

                            if (_purchaseInvoiceLines != null && _purchaseInvoiceLines.Count() > 0)
                            {
                                foreach (var _purchaseInvoiceLine in _purchaseInvoiceLines)
                                {
                                    //Membuat Perhitungan jumlah TUPrice dan Total TxOfPrice

                                    double _locTUAmount = 0;
                                    double _locTxOfAmount = 0;
                                    double _locDiscOfAmount = 0;
                                    XPCollection<PurchaseInvoiceLine> _locPurchaseInvoiceLines = new XPCollection<PurchaseInvoiceLine>(_currSession,
                                                                                            new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("Item.ItemAccountGroup", _purchaseInvoiceLine.ItemAccountGroup),
                                                                                            new BinaryOperator("Tax.TaxAccountGroup", _purchaseInvoiceLineBasedTax.TaxAccountGroup),
                                                                                            new BinaryOperator("Select", true)));

                                    if (_locPurchaseInvoiceLines != null && _locPurchaseInvoiceLines.Count() > 0)
                                    {
                                        foreach (PurchaseInvoiceLine _locPurchaseInvoiceLine in _locPurchaseInvoiceLines)
                                        {
                                            _locTUAmount = _locTUAmount + _locPurchaseInvoiceLine.TUAmount;
                                            //Membuat TaxLine
                                            _locTxOfAmount = _locTxOfAmount + _locPurchaseInvoiceLine.TxAmount;
                                            _locDiscOfAmount = _locDiscOfAmount + _locPurchaseInvoiceLine.DiscAmount;
                                        }
                                    }

                                    if (_locTUAmount > 0)
                                    {
                                        #region JournalMapItemAccountGroup
                                        XPCollection<JournalMap> _locJournalMaps = new XPCollection<JournalMap>
                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                   new BinaryOperator("ItemAccountGroup", _purchaseInvoiceLine.ItemAccountGroup)));

                                        if (_locJournalMaps != null && _locJournalMaps.Count() > 0)
                                        {
                                            foreach (JournalMap _locJournalMap in _locJournalMaps)
                                            {
                                                XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("JournalMap", _locJournalMap)));

                                                if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                {
                                                    foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                    {
                                                        AccountMap _locAccountMap = _currSession.FindObject<AccountMap>
                                                                                     (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("Code", _locJournalMapLine.AccountMap.Code),
                                                                                     new BinaryOperator("PostingType", PostingType.Purchase),
                                                                                     new BinaryOperator("OrderType", OrderType.Item),
                                                                                     new BinaryOperator("PostingMethod", PostingMethod.InvoiceAP),
                                                                                     new BinaryOperator("PostingMethodType", PostingMethodType.Normal)));

                                                        if (_locAccountMap != null)
                                                        {
                                                            XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("AccountMap", _locAccountMap),
                                                                                            new BinaryOperator("Active", true)));

                                                            if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                            {
                                                                double _locTotalPriceDebit = 0;
                                                                double _locTotalPriceCredit = 0;

                                                                foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                {
                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                    {
                                                                        _locTotalPriceDebit = _locTUAmount;
                                                                    }
                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                    {
                                                                        _locTotalPriceCredit = _locTUAmount;
                                                                    }

                                                                    GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                    {
                                                                        PostingDate = now,
                                                                        PostingType = PostingType.Purchase,
                                                                        PostingMethod = PostingMethod.InvoiceAP,
                                                                        Account = _locAccountMapLine.Account,
                                                                        Debit = _locTotalPriceDebit,
                                                                        Credit = _locTotalPriceCredit,
                                                                        PurchaseOrder = _locPurchaseInvoiceXPO.PurchaseOrder,
                                                                        PurchaseInvoice = _locPurchaseInvoiceXPO,
                                                                        Company = _locPurchaseInvoiceXPO.Company,
                                                                    };
                                                                    _saveGeneralJournal.Save();
                                                                    _saveGeneralJournal.Session.CommitTransaction();

                                                                    if (_locAccountMapLine.Account.Code != null)
                                                                    {
                                                                        ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                                                                        if (_locCOA != null)
                                                                        {
                                                                            _locCOA.Debit = _locCOA.Debit + _locTotalPriceDebit;
                                                                            _locCOA.Credit = _locCOA.Credit + _locTotalPriceCredit;
                                                                            _locCOA.Save();
                                                                            _locCOA.Session.CommitTransaction();
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        #endregion JournalMapItemAccountGroup

                                        #region JournalMapBusinessPartnerAccountGroup
                                        if (_locPurchaseInvoiceXPO.PayToVendor != null)
                                        {
                                            if (_locPurchaseInvoiceXPO.PayToVendor.BusinessPartnerAccountGroup != null)
                                            {
                                                XPCollection<JournalMap> _locJournalMapByBusinessPartners = new XPCollection<JournalMap>
                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                           new BinaryOperator("BusinessPartnerAccountGroup", _locPurchaseInvoiceXPO.PayToVendor.BusinessPartnerAccountGroup)));

                                                if (_locJournalMapByBusinessPartners != null && _locJournalMapByBusinessPartners.Count() > 0)
                                                {
                                                    foreach (JournalMap _locJournalMapByBusinessPartner in _locJournalMapByBusinessPartners)
                                                    {
                                                        XPCollection<JournalMapLine> _locJournalMapLineByBusinessPartners = new XPCollection<JournalMapLine>
                                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                        new BinaryOperator("JournalMap", _locJournalMapByBusinessPartner)));

                                                        if (_locJournalMapLineByBusinessPartners != null && _locJournalMapLineByBusinessPartners.Count() > 0)
                                                        {
                                                            foreach (JournalMapLine _locJournalMapLineByBusinessPartner in _locJournalMapLineByBusinessPartners)
                                                            {
                                                                AccountMap _locAccountMapByBusinessPartner = _currSession.FindObject<AccountMap>
                                                                                             (new GroupOperator(GroupOperatorType.And,
                                                                                             new BinaryOperator("Code", _locJournalMapLineByBusinessPartner.AccountMap.Code),
                                                                                             new BinaryOperator("PostingType", PostingType.Purchase),
                                                                                             new BinaryOperator("OrderType", OrderType.Item),
                                                                                             new BinaryOperator("PostingMethod", PostingMethod.InvoiceAP),
                                                                                             new BinaryOperator("PostingMethodType", PostingMethodType.Normal)));

                                                                if (_locAccountMapByBusinessPartner != null)
                                                                {
                                                                    XPCollection<AccountMapLine> _locAccountMapLineByBusinessPartners = new XPCollection<AccountMapLine>
                                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                    new BinaryOperator("AccountMap", _locAccountMapByBusinessPartner),
                                                                                                    new BinaryOperator("Active", true)));

                                                                    if (_locAccountMapLineByBusinessPartners != null && _locAccountMapLineByBusinessPartners.Count() > 0)
                                                                    {
                                                                        double _locTotalPriceDebit = 0;
                                                                        double _locTotalPriceCredit = 0;

                                                                        foreach (AccountMapLine _locAccountMapLine in _locAccountMapLineByBusinessPartners)
                                                                        {
                                                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                            {
                                                                                _locTotalPriceDebit = _locTUAmount;
                                                                            }
                                                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                            {
                                                                                _locTotalPriceCredit = _locTUAmount;
                                                                            }

                                                                            GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                            {
                                                                                PostingDate = now,
                                                                                PostingType = PostingType.Purchase,
                                                                                PostingMethod = PostingMethod.InvoiceAP,
                                                                                Account = _locAccountMapLine.Account,
                                                                                Debit = _locTotalPriceDebit,
                                                                                Credit = _locTotalPriceCredit,
                                                                                PurchaseOrder = _locPurchaseInvoiceXPO.PurchaseOrder,
                                                                                PurchaseInvoice = _locPurchaseInvoiceXPO,
                                                                                Company = _locPurchaseInvoiceXPO.Company,
                                                                            };
                                                                            _saveGeneralJournal.Save();
                                                                            _saveGeneralJournal.Session.CommitTransaction();

                                                                            if (_locAccountMapLine.Account.Code != null)
                                                                            {
                                                                                ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                                                                                if (_locCOA != null)
                                                                                {
                                                                                    _locCOA.Debit = _locCOA.Debit + _locTotalPriceDebit;
                                                                                    _locCOA.Credit = _locCOA.Credit + _locTotalPriceCredit;
                                                                                    _locCOA.Save();
                                                                                    _locCOA.Session.CommitTransaction();
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        #endregion JournalMapBusinessPartnerAccountGroup
                                    }

                                    if (_locTxOfAmount > 0)
                                    {
                                        #region JournalMapTaxAccountGroup
                                        XPCollection<JournalMap> _locJournalMapByTaxs = new XPCollection<JournalMap>
                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                               new BinaryOperator("TaxAccountGroup", _purchaseInvoiceLineBasedTax.TaxAccountGroup)));

                                        if (_locJournalMapByTaxs != null && _locJournalMapByTaxs.Count() > 0)
                                        {
                                            foreach (JournalMap _locJournalMapByTax in _locJournalMapByTaxs)
                                            {
                                                XPCollection<JournalMapLine> _locJournalMapLineByTaxs = new XPCollection<JournalMapLine>
                                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("JournalMap", _locJournalMapByTax)));

                                                if (_locJournalMapLineByTaxs != null && _locJournalMapLineByTaxs.Count() > 0)
                                                {
                                                    foreach (JournalMapLine _locJournalMapLineByTax in _locJournalMapLineByTaxs)
                                                    {
                                                        AccountMap _locAccountMapByTax = _currSession.FindObject<AccountMap>
                                                                                     (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("Code", _locJournalMapLineByTax.AccountMap.Code),
                                                                                     new BinaryOperator("PostingType", PostingType.Purchase),
                                                                                     new BinaryOperator("OrderType", OrderType.Item),
                                                                                     new BinaryOperator("PostingMethod", PostingMethod.InvoiceAP),
                                                                                     new BinaryOperator("PostingMethodType", PostingMethodType.Tax)));

                                                        if (_locAccountMapByTax != null)
                                                        {
                                                            XPCollection<AccountMapLine> _locAccountMapLineByTaxs = new XPCollection<AccountMapLine>
                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("AccountMap", _locAccountMapByTax),
                                                                                            new BinaryOperator("Active", true)));

                                                            if (_locAccountMapLineByTaxs != null && _locAccountMapLineByTaxs.Count() > 0)
                                                            {
                                                                double _locTotalPriceDebit = 0;
                                                                double _locTotalPriceCredit = 0;

                                                                foreach (AccountMapLine _locAccountMapLineByTax in _locAccountMapLineByTaxs)
                                                                {
                                                                    if (_locAccountMapLineByTax.AccountCharge == AccountCharge.Debit)
                                                                    {
                                                                        _locTotalPriceDebit = _locTxOfAmount;
                                                                    }
                                                                    if (_locAccountMapLineByTax.AccountCharge == AccountCharge.Credit)
                                                                    {
                                                                        _locTotalPriceCredit = _locTxOfAmount;
                                                                    }

                                                                    GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                    {
                                                                        PostingDate = now,
                                                                        PostingType = PostingType.Purchase,
                                                                        PostingMethod = PostingMethod.InvoiceAP,
                                                                        Account = _locAccountMapLineByTax.Account,
                                                                        Debit = _locTotalPriceDebit,
                                                                        Credit = _locTotalPriceCredit,
                                                                        PurchaseOrder = _locPurchaseInvoiceXPO.PurchaseOrder,
                                                                        PurchaseInvoice = _locPurchaseInvoiceXPO,
                                                                        Company = _locPurchaseInvoiceXPO.Company,
                                                                    };
                                                                    _saveGeneralJournal.Save();
                                                                    _saveGeneralJournal.Session.CommitTransaction();

                                                                    if (_locAccountMapLineByTax.Account.Code != null)
                                                                    {
                                                                        ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("Code", _locAccountMapLineByTax.Account.Code)));
                                                                        if (_locCOA != null)
                                                                        {
                                                                            _locCOA.Debit = _locCOA.Debit + _locTotalPriceDebit;
                                                                            _locCOA.Credit = _locCOA.Credit + _locTotalPriceCredit;
                                                                            _locCOA.Save();
                                                                            _locCOA.Session.CommitTransaction();
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        #endregion JournalMapTaxAccountGroup

                                        #region JournalMapBusinessPartnerAccountGroup
                                        if (_locPurchaseInvoiceXPO.PayToVendor != null)
                                        {
                                            if (_locPurchaseInvoiceXPO.PayToVendor.BusinessPartnerAccountGroup != null)
                                            {
                                                XPCollection<JournalMap> _locJournalMapByBusinessPartners = new XPCollection<JournalMap>
                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                           new BinaryOperator("BusinessPartnerAccountGroup", _locPurchaseInvoiceXPO.PayToVendor.BusinessPartnerAccountGroup)));

                                                if (_locJournalMapByBusinessPartners != null && _locJournalMapByBusinessPartners.Count() > 0)
                                                {
                                                    foreach (JournalMap _locJournalMapByBusinessPartner in _locJournalMapByBusinessPartners)
                                                    {
                                                        XPCollection<JournalMapLine> _locJournalMapLineByBusinessPartners = new XPCollection<JournalMapLine>
                                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                        new BinaryOperator("JournalMap", _locJournalMapByBusinessPartner)));

                                                        if (_locJournalMapLineByBusinessPartners != null && _locJournalMapLineByBusinessPartners.Count() > 0)
                                                        {
                                                            foreach (JournalMapLine _locJournalMapLineByBusinessPartner in _locJournalMapLineByBusinessPartners)
                                                            {
                                                                AccountMap _locAccountMapByBusinessPartner = _currSession.FindObject<AccountMap>
                                                                                             (new GroupOperator(GroupOperatorType.And,
                                                                                             new BinaryOperator("Code", _locJournalMapLineByBusinessPartner.AccountMap.Code),
                                                                                             new BinaryOperator("PostingType", PostingType.Purchase),
                                                                                             new BinaryOperator("OrderType", OrderType.Item),
                                                                                             new BinaryOperator("PostingMethod", PostingMethod.InvoiceAP),
                                                                                             new BinaryOperator("PostingMethodType", PostingMethodType.Tax)));

                                                                if (_locAccountMapByBusinessPartner != null)
                                                                {
                                                                    XPCollection<AccountMapLine> _locAccountMapLineByBusinessPartners = new XPCollection<AccountMapLine>
                                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                    new BinaryOperator("AccountMap", _locAccountMapByBusinessPartner),
                                                                                                    new BinaryOperator("Active", true)));

                                                                    if (_locAccountMapLineByBusinessPartners != null && _locAccountMapLineByBusinessPartners.Count() > 0)
                                                                    {
                                                                        double _locTotalPriceDebit = 0;
                                                                        double _locTotalPriceCredit = 0;

                                                                        foreach (AccountMapLine _locAccountMapLine in _locAccountMapLineByBusinessPartners)
                                                                        {
                                                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                            {
                                                                                _locTotalPriceDebit = _locTxOfAmount;
                                                                            }
                                                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                            {
                                                                                _locTotalPriceCredit = _locTxOfAmount;
                                                                            }

                                                                            GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                            {
                                                                                PostingDate = now,
                                                                                PostingType = PostingType.Purchase,
                                                                                PostingMethod = PostingMethod.InvoiceAP,
                                                                                Account = _locAccountMapLine.Account,
                                                                                Debit = _locTotalPriceDebit,
                                                                                Credit = _locTotalPriceCredit,
                                                                                PurchaseOrder = _locPurchaseInvoiceXPO.PurchaseOrder,
                                                                                PurchaseInvoice = _locPurchaseInvoiceXPO,
                                                                                Company = _locPurchaseInvoiceXPO.Company,
                                                                            };
                                                                            _saveGeneralJournal.Save();
                                                                            _saveGeneralJournal.Session.CommitTransaction();

                                                                            if (_locAccountMapLine.Account.Code != null)
                                                                            {
                                                                                ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                                                                                if (_locCOA != null)
                                                                                {
                                                                                    _locCOA.Debit = _locCOA.Debit + _locTotalPriceDebit;
                                                                                    _locCOA.Credit = _locCOA.Credit + _locTotalPriceCredit;
                                                                                    _locCOA.Save();
                                                                                    _locCOA.Session.CommitTransaction();
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        #endregion JournalMapBusinessPartnerAccountGroup
                                    }

                                    if (_locDiscOfAmount > 0)
                                    {
                                        #region JournalMapDefaultDiscount
                                        XPQuery<PurchaseInvoiceLine> _purchaseInvoiceLinesQueryBasedDiscount = new XPQuery<PurchaseInvoiceLine>(_currSession);

                                        var _purchaseInvoiceLinesBasedDiscounts = from pil in _purchaseInvoiceLinesQueryBasedDiscount
                                                                                  where (pil.PurchaseInvoice == _locPurchaseInvoiceXPO && (pil.Status == Status.Progress || pil.Status == Status.Posted))
                                                                                  group pil by pil.Discount.DiscountAccountGroup into g
                                                                                  select new { DiscountAccountGroup = g.Key };

                                        if (_purchaseInvoiceLinesBasedDiscounts != null && _purchaseInvoiceLinesBasedDiscounts.Count() > 0)
                                        {
                                            foreach (var _purchaseInvoiceLinesBasedDiscount in _purchaseInvoiceLinesBasedDiscounts)
                                            {
                                                XPCollection<JournalMap> _locJournalMapByDiscounts = new XPCollection<JournalMap>
                                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                       new BinaryOperator("BusinessPartnerAccountGroup", _locPurchaseInvoiceXPO.PayToVendor.BusinessPartnerAccountGroup)));

                                                if (_locJournalMapByDiscounts != null && _locJournalMapByDiscounts.Count() > 0)
                                                {
                                                    foreach (JournalMap _locJournalMapByBusinessPartner in _locJournalMapByDiscounts)
                                                    {
                                                        XPCollection<JournalMapLine> _locJournalMapLineByDiscounts = new XPCollection<JournalMapLine>
                                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                        new BinaryOperator("JournalMap", _locJournalMapByBusinessPartner)));

                                                        if (_locJournalMapLineByDiscounts != null && _locJournalMapLineByDiscounts.Count() > 0)
                                                        {
                                                            foreach (JournalMapLine _locJournalMapLineByDiscount in _locJournalMapLineByDiscounts)
                                                            {
                                                                AccountMap _locAccountMapByDiscount = _currSession.FindObject<AccountMap>
                                                                                             (new GroupOperator(GroupOperatorType.And,
                                                                                             new BinaryOperator("Code", _locJournalMapLineByDiscount.AccountMap.Code),
                                                                                             new BinaryOperator("PostingType", PostingType.Purchase),
                                                                                             new BinaryOperator("OrderType", OrderType.Item),
                                                                                             new BinaryOperator("PostingMethod", PostingMethod.InvoiceAP),
                                                                                             new BinaryOperator("PostingMethodType", PostingMethodType.Discount)));

                                                                if (_locAccountMapByDiscount != null)
                                                                {
                                                                    XPCollection<AccountMapLine> _locAccountMapLineByDiscounts = new XPCollection<AccountMapLine>
                                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                    new BinaryOperator("AccountMap", _locAccountMapByDiscount),
                                                                                                    new BinaryOperator("Active", true)));

                                                                    if (_locAccountMapLineByDiscounts != null && _locAccountMapLineByDiscounts.Count() > 0)
                                                                    {
                                                                        double _locTotalPriceDebit = 0;
                                                                        double _locTotalPriceCredit = 0;

                                                                        foreach (AccountMapLine _locAccountMapLineByDiscount in _locAccountMapLineByDiscounts)
                                                                        {
                                                                            if (_locAccountMapLineByDiscount.AccountCharge == AccountCharge.Debit)
                                                                            {
                                                                                _locTotalPriceDebit = _locDiscOfAmount;
                                                                            }
                                                                            if (_locAccountMapLineByDiscount.AccountCharge == AccountCharge.Credit)
                                                                            {
                                                                                _locTotalPriceCredit = _locDiscOfAmount;
                                                                            }

                                                                            GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                            {
                                                                                PostingDate = now,
                                                                                PostingType = PostingType.Purchase,
                                                                                PostingMethod = PostingMethod.InvoiceAP,
                                                                                Account = _locAccountMapLineByDiscount.Account,
                                                                                Debit = _locTotalPriceDebit,
                                                                                Credit = _locTotalPriceCredit,
                                                                                PurchaseOrder = _locPurchaseInvoiceXPO.PurchaseOrder,
                                                                                PurchaseInvoice = _locPurchaseInvoiceXPO,
                                                                                Company = _locPurchaseInvoiceXPO.Company,
                                                                            };
                                                                            _saveGeneralJournal.Save();
                                                                            _saveGeneralJournal.Session.CommitTransaction();

                                                                            if (_locAccountMapLineByDiscount.Account.Code != null)
                                                                            {
                                                                                ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("Code", _locAccountMapLineByDiscount.Account.Code)));
                                                                                if (_locCOA != null)
                                                                                {
                                                                                    _locCOA.Debit = _locCOA.Debit + _locTotalPriceDebit;
                                                                                    _locCOA.Credit = _locCOA.Credit + _locTotalPriceCredit;
                                                                                    _locCOA.Save();
                                                                                    _locCOA.Session.CommitTransaction();
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        #endregion JournalMapDefaultDiscount
                                    }
                                }
                            }
                            #endregion CreateInvoiceAPJournal

                        }
                    }
                    #endregion CreateInvoiceAPJournalByItemsWithTax
                }

                #endregion CreateInvoiceAPJournal
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PurchaseInvoice ", ex.ToString());
            }
        }

        private void SetPaymentOutPlanOld(Session _currSession, PurchaseInvoice _locPurchaseInvoiceXPO, PayableTransaction _payableTransaction)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                double _locTotalUnitPrice = _locPurchaseInvoiceXPO.Pay;
                if (_locPurchaseInvoiceXPO != null && _payableTransaction != null)
                {
                    //Membuat PaymentOutBasedOn PO dan PI
                    if (_locPurchaseInvoiceXPO.Outstanding > 0)
                    {
                        PaymentOutPlan _savePaymentOutPlan = new PaymentOutPlan(_currSession)
                        {
                            PaymentMethod = _locPurchaseInvoiceXPO.PaymentMethod,
                            EstimatedDate = _locPurchaseInvoiceXPO.EstimatedDate,
                            Plan = _locPurchaseInvoiceXPO.Pay,
                            Outstanding = _locPurchaseInvoiceXPO.Outstanding - _locPurchaseInvoiceXPO.Pay,
                            PurchaseOrder = _locPurchaseInvoiceXPO.PurchaseOrder,
                            PurchaseInvoice = _locPurchaseInvoiceXPO,
                            PayableTransaction = _payableTransaction,
                            Company = _locPurchaseInvoiceXPO.Company,
                        };
                        _savePaymentOutPlan.Save();
                        _savePaymentOutPlan.Session.CommitTransaction();
                    }
                    else
                    {
                        PaymentOutPlan _savePaymentOutPlan = new PaymentOutPlan(_currSession)
                        {
                            PaymentMethod = _locPurchaseInvoiceXPO.PaymentMethod,
                            EstimatedDate = _locPurchaseInvoiceXPO.EstimatedDate,
                            Plan = _locPurchaseInvoiceXPO.Pay,
                            Outstanding = _locPurchaseInvoiceXPO.MaxPay - _locPurchaseInvoiceXPO.Pay,
                            PurchaseOrder = _locPurchaseInvoiceXPO.PurchaseOrder,
                            PurchaseInvoice = _locPurchaseInvoiceXPO,
                            PayableTransaction = _payableTransaction,
                            Company = _locPurchaseInvoiceXPO.Company,
                        };
                        _savePaymentOutPlan.Save();
                        _savePaymentOutPlan.Session.CommitTransaction();
                    }

                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PurchaseInvoice ", ex.ToString());
            }
        }

        private void SetRemainAmountOld(Session _currSession, PurchaseInvoice _locPurchaseInvoiceXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;

                if (_locPurchaseInvoiceXPO != null)
                {
                    double _locPlan = 0;
                    double _locOutstanding = 0;
                    double _locMaxPay = 0;
                    bool _locActivationPosting = false;
                    Status _locStatus = Status.Posted;

                    //#region RemainAmountWithPostedCount!=0

                    //if (_locPurchaseInvoiceXPO.PostedCount > 0)
                    //{
                    //    _locMaxPay = _locPurchaseInvoiceXPO.MaxPay - (_locPurchaseInvoiceXPO.DP_Amount + _locPurchaseInvoiceXPO.CM_Amount);
                    //    if (_locPurchaseInvoiceXPO.Outstanding > 0 && _locMaxPay != _locPurchaseInvoiceXPO.Plan)
                    //    {
                    //        if (_locPurchaseInvoiceXPO.Pay > 0)
                    //        {
                    //            _locPlan = _locPurchaseInvoiceXPO.Plan + _locPurchaseInvoiceXPO.Pay;
                    //            _locOutstanding = _locPurchaseInvoiceXPO.Outstanding - _locPurchaseInvoiceXPO.Pay;

                    //            if (_locOutstanding == 0 && _locPlan == _locMaxPay)
                    //            {
                    //                _locActivationPosting = true;
                    //                _locStatus = Status.Posted;
                    //            }

                    //            _locPurchaseInvoiceXPO.Plan = _locPlan;
                    //            _locPurchaseInvoiceXPO.Outstanding = _locOutstanding;
                    //            _locPurchaseInvoiceXPO.Status = _locStatus;
                    //            _locPurchaseInvoiceXPO.StatusDate = now;
                    //            _locPurchaseInvoiceXPO.ActivationPosting = _locActivationPosting;
                    //            _locPurchaseInvoiceXPO.PostedCount = _locPurchaseInvoiceXPO.PostedCount + 1;
                    //            _locPurchaseInvoiceXPO.Save();
                    //            _locPurchaseInvoiceXPO.Session.CommitTransaction();

                    //            if (_locPurchaseInvoiceXPO.Code != null)
                    //            {
                    //                PurchaseInvoice _locPurchaseInvoice = _currSession.FindObject<PurchaseInvoice>
                    //                                                (new GroupOperator(GroupOperatorType.And,
                    //                                                new BinaryOperator("Code", _locPurchaseInvoiceXPO.Code)));
                    //                if (_locPurchaseInvoice != null)
                    //                {
                    //                    if (_locPurchaseInvoice.Status == Status.Posted && _locPurchaseInvoice.ActivationPosting == true)
                    //                    {
                    //                        XPCollection<PurchaseInvoiceLine> _locPurchaseInvoiceLines = new XPCollection<PurchaseInvoiceLine>
                    //                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                    //                                                                new BinaryOperator("PurchaseInvoice", _locPurchaseInvoice),
                    //                                                                new BinaryOperator("Select", true)));

                    //                        if (_locPurchaseInvoiceLines != null && _locPurchaseInvoiceLines.Count() > 0)
                    //                        {
                    //                            foreach (PurchaseInvoiceLine _locPurchaseInvoiceLine in _locPurchaseInvoiceLines)
                    //                            {
                    //                                _locPurchaseInvoiceLine.ActivationPosting = true;
                    //                                _locPurchaseInvoiceLine.Status = Status.Close;
                    //                                _locPurchaseInvoiceLine.Save();
                    //                                _locPurchaseInvoiceLine.Session.CommitTransaction();
                    //                            }
                    //                        }
                    //                    }
                    //                    else
                    //                    {
                    //                        XPCollection<PurchaseInvoiceLine> _locPurchaseInvoiceLines = new XPCollection<PurchaseInvoiceLine>
                    //                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                    //                                                                new BinaryOperator("PurchaseInvoice", _locPurchaseInvoice),
                    //                                                                new BinaryOperator("Select", true)));

                    //                        if (_locPurchaseInvoiceLines != null && _locPurchaseInvoiceLines.Count() > 0)
                    //                        {
                    //                            foreach (PurchaseInvoiceLine _locPurchaseInvoiceLine in _locPurchaseInvoiceLines)
                    //                            {
                    //                                _locPurchaseInvoiceLine.ActivationPosting = true;
                    //                                _locPurchaseInvoiceLine.Status = Status.Posted;
                    //                                _locPurchaseInvoiceLine.Save();
                    //                                _locPurchaseInvoiceLine.Session.CommitTransaction();
                    //                            }
                    //                        }
                    //                    }
                    //                }
                    //            }
                    //        }
                    //    }
                    //}
                    //#endregion RemainAmountWithPostedCount!=0

                    //#region RemainAmountWithPostedCount=0
                    //else
                    //{
                    //    if (_locPurchaseInvoiceXPO.Pay > 0)
                    //    {
                    //        _locPlan = _locPurchaseInvoiceXPO.Pay;
                    //        _locMaxPay = _locPurchaseInvoiceXPO.MaxPay - (_locPurchaseInvoiceXPO.DP_Amount + _locPurchaseInvoiceXPO.CM_Amount);
                    //        _locOutstanding = _locMaxPay - _locPlan;

                    //        if (_locPlan == _locMaxPay && _locOutstanding == 0)
                    //        {
                    //            _locStatus = Status.Posted;
                    //            _locActivationPosting = true;
                    //        }
                    //        //_locPurchaseInvoiceXPO.DP_Posting = true;
                    //        _locPurchaseInvoiceXPO.Plan = _locPlan;
                    //        _locPurchaseInvoiceXPO.Outstanding = _locOutstanding;
                    //        _locPurchaseInvoiceXPO.Status = _locStatus;
                    //        _locPurchaseInvoiceXPO.StatusDate = now;
                    //        _locPurchaseInvoiceXPO.ActivationPosting = _locActivationPosting;
                    //        _locPurchaseInvoiceXPO.PostedCount = _locPurchaseInvoiceXPO.PostedCount + 1;
                    //        _locPurchaseInvoiceXPO.Save();
                    //        _locPurchaseInvoiceXPO.Session.CommitTransaction();

                    //        if (_locPurchaseInvoiceXPO.Code != null)
                    //        {
                    //            PurchaseInvoice _locPurchaseInvoice = _currSession.FindObject<PurchaseInvoice>
                    //                                            (new GroupOperator(GroupOperatorType.And,
                    //                                            new BinaryOperator("Code", _locPurchaseInvoiceXPO.Code)));
                    //            if (_locPurchaseInvoice != null)
                    //            {
                    //                if (_locPurchaseInvoice.Status == Status.Posted && _locPurchaseInvoice.ActivationPosting == true)
                    //                {
                    //                    XPCollection<PurchaseInvoiceLine> _locPurchaseInvoiceLines = new XPCollection<PurchaseInvoiceLine>
                    //                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                    //                                                            new BinaryOperator("PurchaseInvoice", _locPurchaseInvoice),
                    //                                                            new BinaryOperator("Select", true)));

                    //                    if (_locPurchaseInvoiceLines != null && _locPurchaseInvoiceLines.Count() > 0)
                    //                    {
                    //                        foreach (PurchaseInvoiceLine _locPurchaseInvoiceLine in _locPurchaseInvoiceLines)
                    //                        {
                    //                            _locPurchaseInvoiceLine.ActivationPosting = true;
                    //                            _locPurchaseInvoiceLine.Status = Status.Close;
                    //                            _locPurchaseInvoiceLine.Save();
                    //                            _locPurchaseInvoiceLine.Session.CommitTransaction();
                    //                        }
                    //                    }
                    //                }
                    //                else
                    //                {
                    //                    XPCollection<PurchaseInvoiceLine> _locPurchaseInvoiceLines = new XPCollection<PurchaseInvoiceLine>
                    //                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                    //                                                            new BinaryOperator("PurchaseInvoice", _locPurchaseInvoice),
                    //                                                            new BinaryOperator("Select", true)));

                    //                    if (_locPurchaseInvoiceLines != null && _locPurchaseInvoiceLines.Count() > 0)
                    //                    {
                    //                        foreach (PurchaseInvoiceLine _locPurchaseInvoiceLine in _locPurchaseInvoiceLines)
                    //                        {
                    //                            _locPurchaseInvoiceLine.ActivationPosting = false;
                    //                            _locPurchaseInvoiceLine.Status = Status.Posted;
                    //                            _locPurchaseInvoiceLine.Save();
                    //                            _locPurchaseInvoiceLine.Session.CommitTransaction();
                    //                        }
                    //                    }
                    //                }
                    //            }
                    //        }
                    //    }

                    //}
                    //#endregion RemainAmountWithPostedCount=0
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PurchaseInvoice ", ex.ToString());
            }
        }

        private bool GetNullPurchaseInvoiceLineTaxOld(Session _currSession, PurchaseInvoice _locPurchaseInvoiceXPO)
        {
            bool _result = false;
            try
            {
                double _currCount = 0;
                XPCollection<PurchaseInvoiceLine> _locPurchInvLines = new XPCollection<PurchaseInvoiceLine>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO)));

                if (_locPurchInvLines != null && _locPurchInvLines.Count() > 0)
                {
                    foreach (PurchaseInvoiceLine _locPurchInvLine in _locPurchInvLines)
                    {
                        if (_locPurchInvLine.Tax == null)
                        {
                            _currCount = _currCount + 1;

                        }
                        else
                        {
                            if (_locPurchInvLine.Tax != null)
                            {
                                if (_locPurchInvLine.Tax.TaxAccountGroup == null)
                                {
                                    _currCount = _currCount + 1;
                                }
                            }
                        }
                    }

                    if (_currCount > 0)
                    {
                        _result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PurchaseInvoice ", ex.ToString());
            }

            return _result;
        }

        private void SetInvoiceAPJournal20Feb2020(Session _currSession, PurchaseInvoice _locPurchaseInvoiceXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                double _locPayTUAmount = 0;
                double _locTUAmount = 0;
                double _locTxOfAmount = 0;
                double _locDiscOfAmount = 0;
                double _locDP = 0;
                PaymentOutPlan _locPaymentPlan = null;

                #region CreateInvoiceAPJournal

                //if (_locPurchaseInvoiceXPO.DP_Posting == true && _locPurchaseInvoiceXPO.DP_Amount > 0)
                //{

                //    #region CreateInvoiceAPJournalDownPayment

                //    if (_locPurchaseInvoiceXPO.Pay > 0)
                //    {

                //        if ((_locPurchaseInvoiceXPO.Pay <= _locPurchaseInvoiceXPO.DP_Amount))
                //        {
                //            _locDP = _locPurchaseInvoiceXPO.Pay;
                //        }
                //        else
                //        {
                //            _locDP = _locPurchaseInvoiceXPO.Pay - _locPurchaseInvoiceXPO.DP_Amount;
                //        }

                //        #region JournalMapCompanyAccountGroup
                //        if (_locPurchaseInvoiceXPO.Company != null)
                //        {
                //            if (_locPurchaseInvoiceXPO.Company.CompanyAccountGroup != null)
                //            {
                //                XPCollection<JournalMap> _locJournalMapByCompanys = new XPCollection<JournalMap>
                //                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                //                                                                             new BinaryOperator("CompanyAccountGroup", _locPurchaseInvoiceXPO.Company.CompanyAccountGroup)));

                //                if (_locJournalMapByCompanys != null && _locJournalMapByCompanys.Count() > 0)
                //                {
                //                    foreach (JournalMap _locJournalMapByCompany in _locJournalMapByCompanys)
                //                    {
                //                        XPCollection<JournalMapLine> _locJournalMapLineByCompanys = new XPCollection<JournalMapLine>
                //                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                //                                                                                             new BinaryOperator("JournalMap", _locJournalMapByCompany)));

                //                        if (_locJournalMapLineByCompanys != null && _locJournalMapLineByCompanys.Count() > 0)
                //                        {
                //                            foreach (JournalMapLine _locJournalMapLineByCompany in _locJournalMapLineByCompanys)
                //                            {
                //                                AccountMap _locAccountMapByBusinessPartner = _currSession.FindObject<AccountMap>
                //                                                                             (new GroupOperator(GroupOperatorType.And,
                //                                                                              new BinaryOperator("Code", _locJournalMapLineByCompany.AccountMap.Code),
                //                                                                              new BinaryOperator("PostingType", PostingType.Purchase),
                //                                                                              new BinaryOperator("OrderType", OrderType.Item),
                //                                                                              new BinaryOperator("PostingMethod", PostingMethod.InvoiceAP),
                //                                                                              new BinaryOperator("PostingMethodType", PostingMethodType.DownPayment)));

                //                                if (_locAccountMapByBusinessPartner != null)
                //                                {
                //                                    XPCollection<AccountMapLine> _locAccountMapLineByBusinessPartners = new XPCollection<AccountMapLine>
                //                                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                //                                                                                                         new BinaryOperator("AccountMap", _locAccountMapByBusinessPartner),
                //                                                                                                         new BinaryOperator("Active", true)));

                //                                    if (_locAccountMapLineByBusinessPartners != null && _locAccountMapLineByBusinessPartners.Count() > 0)
                //                                    {
                //                                        double _locTotalAmountDebit = 0;
                //                                        double _locTotalAmountCredit = 0;
                //                                        double _locTotalBalance = 0;

                //                                        foreach (AccountMapLine _locAccountMapLine in _locAccountMapLineByBusinessPartners)
                //                                        {
                //                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                //                                            {
                //                                                _locTotalAmountDebit = _locDP;
                //                                            }
                //                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                //                                            {
                //                                                _locTotalAmountCredit = _locDP;
                //                                            }

                //                                            GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                //                                            {
                //                                                PostingDate = now,
                //                                                PostingType = PostingType.Purchase,
                //                                                PostingMethod = PostingMethod.InvoiceAP,
                //                                                Account = _locAccountMapLine.Account,
                //                                                Debit = _locTotalAmountDebit,
                //                                                Credit = _locTotalAmountCredit,
                //                                                PurchaseOrder = _locPurchaseInvoiceXPO.PurchaseOrder,
                //                                                PurchaseInvoice = _locPurchaseInvoiceXPO,
                //                                                Company = _locPurchaseInvoiceXPO.Company,
                //                                            };
                //                                            _saveGeneralJournal.Save();
                //                                            _saveGeneralJournal.Session.CommitTransaction();

                //                                            if (_locAccountMapLine.Account.Code != null)
                //                                            {
                //                                                ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                //                                                                         (new GroupOperator(GroupOperatorType.And,
                //                                                                          new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                //                                                if (_locCOA != null)
                //                                                {
                //                                                    if (_locCOA.BalanceType == BalanceType.Change)
                //                                                    {
                //                                                        if (_locCOA.AccountCharge == AccountCharge.Debit)
                //                                                        {
                //                                                            if (_locTotalAmountDebit > 0)
                //                                                            {
                //                                                                _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                //                                                            }
                //                                                            if (_locTotalAmountCredit > 0)
                //                                                            {
                //                                                                _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                //                                                            }
                //                                                        }
                //                                                        if (_locCOA.AccountCharge == AccountCharge.Credit)
                //                                                        {
                //                                                            if (_locTotalAmountDebit > 0)
                //                                                            {
                //                                                                _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                //                                                            }
                //                                                            if (_locTotalAmountCredit > 0)
                //                                                            {
                //                                                                _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                //                                                            }
                //                                                        }
                //                                                    }

                //                                                    _locCOA.Balance = _locTotalBalance;
                //                                                    _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                //                                                    _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                //                                                    _locCOA.Save();
                //                                                    _locCOA.Session.CommitTransaction();
                //                                                }
                //                                            }
                //                                        }
                //                                    }
                //                                }
                //                            }
                //                        }
                //                    }
                //                }
                //            }
                //        }
                //        #endregion JournalMapCompanyAccountGroup
                //        #region JournalMapBusinessPartnerAccountGroup
                //        if (_locPurchaseInvoiceXPO.PayToVendor != null)
                //        {
                //            if (_locPurchaseInvoiceXPO.PayToVendor.BusinessPartnerAccountGroup != null)
                //            {
                //                XPCollection<JournalMap> _locJournalMapByBusinessPartners = new XPCollection<JournalMap>
                //                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                //                                                                             new BinaryOperator("BusinessPartnerAccountGroup", _locPurchaseInvoiceXPO.PayToVendor.BusinessPartnerAccountGroup)));

                //                if (_locJournalMapByBusinessPartners != null && _locJournalMapByBusinessPartners.Count() > 0)
                //                {
                //                    foreach (JournalMap _locJournalMapByBusinessPartner in _locJournalMapByBusinessPartners)
                //                    {
                //                        XPCollection<JournalMapLine> _locJournalMapLineByBusinessPartners = new XPCollection<JournalMapLine>
                //                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                //                                                                                             new BinaryOperator("JournalMap", _locJournalMapByBusinessPartner)));

                //                        if (_locJournalMapLineByBusinessPartners != null && _locJournalMapLineByBusinessPartners.Count() > 0)
                //                        {
                //                            foreach (JournalMapLine _locJournalMapLineByBusinessPartner in _locJournalMapLineByBusinessPartners)
                //                            {
                //                                AccountMap _locAccountMapByBusinessPartner = _currSession.FindObject<AccountMap>
                //                                                                             (new GroupOperator(GroupOperatorType.And,
                //                                                                              new BinaryOperator("Code", _locJournalMapLineByBusinessPartner.AccountMap.Code),
                //                                                                              new BinaryOperator("PostingType", PostingType.Purchase),
                //                                                                              new BinaryOperator("OrderType", OrderType.Item),
                //                                                                              new BinaryOperator("PostingMethod", PostingMethod.InvoiceAP),
                //                                                                              new BinaryOperator("PostingMethodType", PostingMethodType.DownPayment)));

                //                                if (_locAccountMapByBusinessPartner != null)
                //                                {
                //                                    XPCollection<AccountMapLine> _locAccountMapLineByBusinessPartners = new XPCollection<AccountMapLine>
                //                                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                //                                                                                                         new BinaryOperator("AccountMap", _locAccountMapByBusinessPartner),
                //                                                                                                         new BinaryOperator("Active", true)));

                //                                    if (_locAccountMapLineByBusinessPartners != null && _locAccountMapLineByBusinessPartners.Count() > 0)
                //                                    {
                //                                        double _locTotalAmountDebit = 0;
                //                                        double _locTotalAmountCredit = 0;
                //                                        double _locTotalBalance = 0;

                //                                        foreach (AccountMapLine _locAccountMapLine in _locAccountMapLineByBusinessPartners)
                //                                        {
                //                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                //                                            {
                //                                                _locTotalAmountDebit = _locDP;
                //                                            }
                //                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                //                                            {
                //                                                _locTotalAmountCredit = _locDP;
                //                                            }

                //                                            GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                //                                            {
                //                                                PostingDate = now,
                //                                                PostingType = PostingType.Purchase,
                //                                                PostingMethod = PostingMethod.InvoiceAP,
                //                                                Account = _locAccountMapLine.Account,
                //                                                Debit = _locTotalAmountDebit,
                //                                                Credit = _locTotalAmountCredit,
                //                                                PurchaseOrder = _locPurchaseInvoiceXPO.PurchaseOrder,
                //                                                PurchaseInvoice = _locPurchaseInvoiceXPO,
                //                                                Company = _locPurchaseInvoiceXPO.Company,
                //                                            };
                //                                            _saveGeneralJournal.Save();
                //                                            _saveGeneralJournal.Session.CommitTransaction();

                //                                            if (_locAccountMapLine.Account.Code != null)
                //                                            {
                //                                                ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                //                                                                         (new GroupOperator(GroupOperatorType.And,
                //                                                                          new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                //                                                if (_locCOA != null)
                //                                                {
                //                                                    if (_locCOA.BalanceType == BalanceType.Change)
                //                                                    {
                //                                                        if (_locCOA.AccountCharge == AccountCharge.Debit)
                //                                                        {
                //                                                            if (_locTotalAmountDebit > 0)
                //                                                            {
                //                                                                _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                //                                                            }
                //                                                            if (_locTotalAmountCredit > 0)
                //                                                            {
                //                                                                _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                //                                                            }
                //                                                        }
                //                                                        if (_locCOA.AccountCharge == AccountCharge.Credit)
                //                                                        {
                //                                                            if (_locTotalAmountDebit > 0)
                //                                                            {
                //                                                                _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                //                                                            }
                //                                                            if (_locTotalAmountCredit > 0)
                //                                                            {
                //                                                                _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                //                                                            }
                //                                                        }
                //                                                    }

                //                                                    _locCOA.Balance = _locTotalBalance;
                //                                                    _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                //                                                    _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                //                                                    _locCOA.Save();
                //                                                    _locCOA.Session.CommitTransaction();
                //                                                }
                //                                            }
                //                                        }
                //                                    }
                //                                }
                //                            }
                //                        }
                //                    }
                //                }
                //            }
                //        }
                //        #endregion JournalMapBusinessPartnerAccountGroup
                //    }

                //    #endregion CreateInvoiceAPJournalDownPayment
                //}
                //else
                //{
                //    #region CreateInvoiceAPJournalNormal
                //    XPQuery<PurchaseInvoiceLine> _PurchaseInvoiceLinesQueryBasedNormal = new XPQuery<PurchaseInvoiceLine>(_currSession);

                //    var _PurchaseInvoiceLineBasedNormals = from pil in _PurchaseInvoiceLinesQueryBasedNormal
                //                                           where (pil.PurchaseInvoice == _locPurchaseInvoiceXPO
                //                                           && (pil.Status == Status.Progress || pil.Status == Status.Posted)
                //                                           && pil.Select == true)
                //                                           group pil by pil.Item.ItemAccountGroup into g
                //                                           select new { ItemAccountGroup = g.Key };

                //    if (_PurchaseInvoiceLineBasedNormals != null && _PurchaseInvoiceLineBasedNormals.Count() > 0)
                //    {
                //        foreach (var _PurchaseInvoiceLineBasedNormal in _PurchaseInvoiceLineBasedNormals)
                //        {
                //            XPCollection<PurchaseInvoiceLine> _locPurchaseInvoiceLines = new XPCollection<PurchaseInvoiceLine>(_currSession,
                //                                                                            new GroupOperator(GroupOperatorType.And,
                //                                                                            new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO),
                //                                                                            new BinaryOperator("Item.ItemAccountGroup", _PurchaseInvoiceLineBasedNormal.ItemAccountGroup),
                //                                                                            new BinaryOperator("Select", true)));

                //            if (_locPurchaseInvoiceLines != null && _locPurchaseInvoiceLines.Count() > 0)
                //            {
                //                foreach (PurchaseInvoiceLine _locPurchaseInvoiceLine in _locPurchaseInvoiceLines)
                //                {
                //                    _locTUAmount = _locTUAmount + _locPurchaseInvoiceLine.TUAmount;
                //                }
                //            }

                //            #region PerhitunganPay
                //            if (_locPurchaseInvoiceXPO.PurchaseOrder != null)
                //            {
                //                if (_locPurchaseInvoiceXPO.PurchaseOrder.SplitInvoice == true)
                //                {

                //                    _locPaymentPlan = _currSession.FindObject<PaymentOutPlan>
                //                                                    (new GroupOperator(GroupOperatorType.And,
                //                                                     new BinaryOperator("PurchaseOrder", _locPurchaseInvoiceXPO.PurchaseOrder),
                //                                                     new BinaryOperator("DownPayment", true),
                //                                                     new BinaryOperator("Deduction", false)));
                //                    if (_locPaymentPlan != null)
                //                    {
                //                        if (_locPurchaseInvoiceXPO.PostedCount == 0)
                //                        {
                //                            if (_locTUAmount > _locPaymentPlan.Plan)
                //                            {
                //                                _locPayTUAmount = _locTUAmount - _locPaymentPlan.Plan;
                //                            }
                //                            else
                //                            {

                //                            }
                //                            //Buat else sisa 50% dari plan di kurangkan ke LocTUAmount

                //                        }
                //                        else
                //                        {
                //                            if (_locTUAmount <= _locPurchaseInvoiceXPO.Outstanding)
                //                            {
                //                                if (_locTUAmount > _locPaymentPlan.Plan)
                //                                {
                //                                    _locPayTUAmount = _locTUAmount - _locPaymentPlan.Plan;
                //                                }
                //                            }
                //                            else
                //                            {
                //                                _locPayTUAmount = _locPurchaseInvoiceXPO.Outstanding - _locPaymentPlan.Plan;
                //                            }
                //                        }

                //                    }
                //                    else
                //                    {
                //                        _locPayTUAmount = _locTUAmount;
                //                    }

                //                }
                //                else
                //                {
                //                    if (_locTUAmount > _locPurchaseInvoiceXPO.Outstanding)
                //                    {
                //                        _locPayTUAmount = _locTUAmount;
                //                    }
                //                    else
                //                    {
                //                        _locPayTUAmount = _locPurchaseInvoiceXPO.Outstanding;

                //                    }
                //                }
                //            }
                //            #endregion PerhitunganPay

                //            if (_locPayTUAmount > 0)
                //            {
                //                #region JournalMapItemAccountGroup
                //                XPCollection<JournalMap> _locJournalMaps = new XPCollection<JournalMap>
                //                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                //                                                           new BinaryOperator("ItemAccountGroup", _PurchaseInvoiceLineBasedNormal.ItemAccountGroup)));

                //                if (_locJournalMaps != null && _locJournalMaps.Count() > 0)
                //                {
                //                    foreach (JournalMap _locJournalMap in _locJournalMaps)
                //                    {
                //                        XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                //                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                //                                                                        new BinaryOperator("JournalMap", _locJournalMap)));

                //                        if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                //                        {
                //                            foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                //                            {
                //                                AccountMap _locAccountMap = _currSession.FindObject<AccountMap>
                //                                                            (new GroupOperator(GroupOperatorType.And,
                //                                                             new BinaryOperator("Code", _locJournalMapLine.AccountMap.Code),
                //                                                             new BinaryOperator("PostingType", PostingType.Purchase),
                //                                                             new BinaryOperator("OrderType", OrderType.Item),
                //                                                             new BinaryOperator("PostingMethod", PostingMethod.InvoiceAP),
                //                                                             new BinaryOperator("PostingMethodType", PostingMethodType.Normal)));

                //                                if (_locAccountMap != null)
                //                                {
                //                                    XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                //                                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                //                                                                                        new BinaryOperator("AccountMap", _locAccountMap),
                //                                                                                        new BinaryOperator("Active", true)));

                //                                    if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                //                                    {
                //                                        double _locTotalAmountDebit = 0;
                //                                        double _locTotalAmountCredit = 0;
                //                                        double _locTotalBalance = 0;

                //                                        foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                //                                        {
                //                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                //                                            {
                //                                                _locTotalAmountDebit = _locPayTUAmount;
                //                                            }
                //                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                //                                            {
                //                                                _locTotalAmountCredit = _locPayTUAmount;
                //                                            }

                //                                            GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                //                                            {
                //                                                PostingDate = now,
                //                                                PostingType = PostingType.Purchase,
                //                                                PostingMethod = PostingMethod.InvoiceAP,
                //                                                Account = _locAccountMapLine.Account,
                //                                                Debit = _locTotalAmountDebit,
                //                                                Credit = _locTotalAmountCredit,
                //                                                PurchaseOrder = _locPurchaseInvoiceXPO.PurchaseOrder,
                //                                                PurchaseInvoice = _locPurchaseInvoiceXPO,
                //                                                Company = _locPurchaseInvoiceXPO.Company,
                //                                            };
                //                                            _saveGeneralJournal.Save();
                //                                            _saveGeneralJournal.Session.CommitTransaction();

                //                                            if (_locAccountMapLine.Account.Code != null)
                //                                            {
                //                                                ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                //                                                                (new GroupOperator(GroupOperatorType.And,
                //                                                                new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                //                                                if (_locCOA != null)
                //                                                {
                //                                                    if (_locCOA.BalanceType == BalanceType.Change)
                //                                                    {
                //                                                        if (_locCOA.AccountCharge == AccountCharge.Debit)
                //                                                        {
                //                                                            if (_locTotalAmountDebit > 0)
                //                                                            {
                //                                                                _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                //                                                            }
                //                                                            if (_locTotalAmountCredit > 0)
                //                                                            {
                //                                                                _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                //                                                            }
                //                                                        }
                //                                                        if (_locCOA.AccountCharge == AccountCharge.Credit)
                //                                                        {
                //                                                            if (_locTotalAmountDebit > 0)
                //                                                            {
                //                                                                _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                //                                                            }
                //                                                            if (_locTotalAmountCredit > 0)
                //                                                            {
                //                                                                _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                //                                                            }
                //                                                        }
                //                                                    }

                //                                                    _locCOA.Balance = _locTotalBalance;
                //                                                    _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                //                                                    _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                //                                                    _locCOA.Save();
                //                                                    _locCOA.Session.CommitTransaction();
                //                                                }
                //                                            }
                //                                        }
                //                                    }
                //                                }
                //                            }
                //                        }
                //                    }
                //                }
                //                #endregion JournalMapItemAccountGroup

                //                #region JournalMapBusinessPartnerAccountGroup
                //                if (_locPurchaseInvoiceXPO.PayToVendor != null)
                //                {
                //                    if (_locPurchaseInvoiceXPO.PayToVendor.BusinessPartnerAccountGroup != null)
                //                    {
                //                        XPCollection<JournalMap> _locJournalMapByBusinessPartners = new XPCollection<JournalMap>
                //                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                //                                                                                     new BinaryOperator("BusinessPartnerAccountGroup", _locPurchaseInvoiceXPO.PayToVendor.BusinessPartnerAccountGroup)));

                //                        if (_locJournalMapByBusinessPartners != null && _locJournalMapByBusinessPartners.Count() > 0)
                //                        {
                //                            foreach (JournalMap _locJournalMapByBusinessPartner in _locJournalMapByBusinessPartners)
                //                            {
                //                                XPCollection<JournalMapLine> _locJournalMapLineByBusinessPartners = new XPCollection<JournalMapLine>
                //                                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                //                                                                                                     new BinaryOperator("JournalMap", _locJournalMapByBusinessPartner)));

                //                                if (_locJournalMapLineByBusinessPartners != null && _locJournalMapLineByBusinessPartners.Count() > 0)
                //                                {
                //                                    foreach (JournalMapLine _locJournalMapLineByBusinessPartner in _locJournalMapLineByBusinessPartners)
                //                                    {
                //                                        AccountMap _locAccountMapByBusinessPartner = _currSession.FindObject<AccountMap>
                //                                                                                     (new GroupOperator(GroupOperatorType.And,
                //                                                                                      new BinaryOperator("Code", _locJournalMapLineByBusinessPartner.AccountMap.Code),
                //                                                                                      new BinaryOperator("PostingType", PostingType.Purchase),
                //                                                                                      new BinaryOperator("OrderType", OrderType.Item),
                //                                                                                      new BinaryOperator("PostingMethod", PostingMethod.InvoiceAP),
                //                                                                                      new BinaryOperator("PostingMethodType", PostingMethodType.Normal)));

                //                                        if (_locAccountMapByBusinessPartner != null)
                //                                        {
                //                                            XPCollection<AccountMapLine> _locAccountMapLineByBusinessPartners = new XPCollection<AccountMapLine>
                //                                                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                //                                                                                                                 new BinaryOperator("AccountMap", _locAccountMapByBusinessPartner),
                //                                                                                                                 new BinaryOperator("Active", true)));

                //                                            if (_locAccountMapLineByBusinessPartners != null && _locAccountMapLineByBusinessPartners.Count() > 0)
                //                                            {
                //                                                double _locTotalAmountDebit = 0;
                //                                                double _locTotalAmountCredit = 0;
                //                                                double _locTotalBalance = 0;

                //                                                foreach (AccountMapLine _locAccountMapLine in _locAccountMapLineByBusinessPartners)
                //                                                {
                //                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                //                                                    {
                //                                                        _locTotalAmountDebit = _locPayTUAmount;
                //                                                    }
                //                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                //                                                    {
                //                                                        _locTotalAmountCredit = _locPayTUAmount;
                //                                                    }

                //                                                    GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                //                                                    {
                //                                                        PostingDate = now,
                //                                                        PostingType = PostingType.Purchase,
                //                                                        PostingMethod = PostingMethod.InvoiceAP,
                //                                                        Account = _locAccountMapLine.Account,
                //                                                        Debit = _locTotalAmountDebit,
                //                                                        Credit = _locTotalAmountCredit,
                //                                                        PurchaseOrder = _locPurchaseInvoiceXPO.PurchaseOrder,
                //                                                        PurchaseInvoice = _locPurchaseInvoiceXPO,
                //                                                        Company = _locPurchaseInvoiceXPO.Company,
                //                                                    };
                //                                                    _saveGeneralJournal.Save();
                //                                                    _saveGeneralJournal.Session.CommitTransaction();

                //                                                    if (_locAccountMapLine.Account.Code != null)
                //                                                    {
                //                                                        ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                //                                                                                 (new GroupOperator(GroupOperatorType.And,
                //                                                                                  new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                //                                                        if (_locCOA != null)
                //                                                        {
                //                                                            if (_locCOA.BalanceType == BalanceType.Change)
                //                                                            {
                //                                                                if (_locCOA.AccountCharge == AccountCharge.Debit)
                //                                                                {
                //                                                                    if (_locTotalAmountDebit > 0)
                //                                                                    {
                //                                                                        _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                //                                                                    }
                //                                                                    if (_locTotalAmountCredit > 0)
                //                                                                    {
                //                                                                        _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                //                                                                    }
                //                                                                }
                //                                                                if (_locCOA.AccountCharge == AccountCharge.Credit)
                //                                                                {
                //                                                                    if (_locTotalAmountDebit > 0)
                //                                                                    {
                //                                                                        _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                //                                                                    }
                //                                                                    if (_locTotalAmountCredit > 0)
                //                                                                    {
                //                                                                        _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                //                                                                    }
                //                                                                }
                //                                                            }

                //                                                            _locCOA.Balance = _locTotalBalance;
                //                                                            _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                //                                                            _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                //                                                            _locCOA.Save();
                //                                                            _locCOA.Session.CommitTransaction();
                //                                                        }
                //                                                    }
                //                                                }
                //                                            }
                //                                        }
                //                                    }
                //                                }
                //                            }
                //                        }
                //                    }
                //                }
                //                #endregion JournalMapBusinessPartnerAccountGroup


                //            }
                //        }
                //    }
                //    #endregion CreateInvoiceAPJournalNormal

                //    #region CreateInvoiceARWithTax
                //    XPQuery<PurchaseInvoiceLine> _PurchaseInvoiceLinesQueryBasedTax = new XPQuery<PurchaseInvoiceLine>(_currSession);

                //    var _PurchaseInvoiceLineBasedTaxs = from pil in _PurchaseInvoiceLinesQueryBasedNormal
                //                                        where (pil.PurchaseInvoice == _locPurchaseInvoiceXPO
                //                                        && (pil.Status == Status.Progress || pil.Status == Status.Posted)
                //                                        && pil.Select == true)
                //                                        group pil by pil.Tax.TaxAccountGroup into g
                //                                        select new { TaxAccountGroup = g.Key };

                //    if (_PurchaseInvoiceLineBasedNormals != null && _PurchaseInvoiceLineBasedNormals.Count() > 0)
                //    {
                //        foreach (var _PurchaseInvoiceLineBasedTax in _PurchaseInvoiceLineBasedTaxs)
                //        {
                //            XPCollection<PurchaseInvoiceLine> _locPurchaseInvoiceLines = new XPCollection<PurchaseInvoiceLine>(_currSession,
                //                                                                            new GroupOperator(GroupOperatorType.And,
                //                                                                            new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO),
                //                                                                            new BinaryOperator("Tax.TaxAccountGroup", _PurchaseInvoiceLineBasedTax.TaxAccountGroup),
                //                                                                            new BinaryOperator("Select", true)));

                //            if (_locPurchaseInvoiceLines != null && _locPurchaseInvoiceLines.Count() > 0)
                //            {
                //                foreach (PurchaseInvoiceLine _locPurchaseInvoiceLine in _locPurchaseInvoiceLines)
                //                {
                //                    _locTxOfAmount = _locTxOfAmount + _locPurchaseInvoiceLine.TxAmount;
                //                }
                //            }

                //            if (_locTxOfAmount > 0)
                //            {
                //                #region JournalMapTaxAccountGroup
                //                XPCollection<JournalMap> _locJournalMapByTaxs = new XPCollection<JournalMap>
                //                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                //                                                                 new BinaryOperator("TaxAccountGroup", _PurchaseInvoiceLineBasedTax.TaxAccountGroup)));

                //                if (_locJournalMapByTaxs != null && _locJournalMapByTaxs.Count() > 0)
                //                {
                //                    foreach (JournalMap _locJournalMapByTax in _locJournalMapByTaxs)
                //                    {
                //                        XPCollection<JournalMapLine> _locJournalMapLineByTaxs = new XPCollection<JournalMapLine>
                //                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                //                                                                                 new BinaryOperator("JournalMap", _locJournalMapByTax)));

                //                        if (_locJournalMapLineByTaxs != null && _locJournalMapLineByTaxs.Count() > 0)
                //                        {
                //                            foreach (JournalMapLine _locJournalMapLineByTax in _locJournalMapLineByTaxs)
                //                            {
                //                                AccountMap _locAccountMapByTax = _currSession.FindObject<AccountMap>
                //                                                                 (new GroupOperator(GroupOperatorType.And,
                //                                                                  new BinaryOperator("Code", _locJournalMapLineByTax.AccountMap.Code),
                //                                                                  new BinaryOperator("PostingType", PostingType.Purchase),
                //                                                                  new BinaryOperator("OrderType", OrderType.Item),
                //                                                                  new BinaryOperator("PostingMethod", PostingMethod.InvoiceAP),
                //                                                                  new BinaryOperator("PostingMethodType", PostingMethodType.Tax)));

                //                                if (_locAccountMapByTax != null)
                //                                {
                //                                    XPCollection<AccountMapLine> _locAccountMapLineByTaxs = new XPCollection<AccountMapLine>
                //                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                //                                                                                             new BinaryOperator("AccountMap", _locAccountMapByTax),
                //                                                                                             new BinaryOperator("Active", true)));

                //                                    if (_locAccountMapLineByTaxs != null && _locAccountMapLineByTaxs.Count() > 0)
                //                                    {
                //                                        double _locTotalPriceDebit = 0;
                //                                        double _locTotalPriceCredit = 0;

                //                                        foreach (AccountMapLine _locAccountMapLineByTax in _locAccountMapLineByTaxs)
                //                                        {
                //                                            if (_locAccountMapLineByTax.AccountCharge == AccountCharge.Debit)
                //                                            {
                //                                                _locTotalPriceDebit = _locTxOfAmount;
                //                                            }
                //                                            if (_locAccountMapLineByTax.AccountCharge == AccountCharge.Credit)
                //                                            {
                //                                                _locTotalPriceCredit = _locTxOfAmount;
                //                                            }

                //                                            GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                //                                            {
                //                                                PostingDate = now,
                //                                                PostingType = PostingType.Purchase,
                //                                                PostingMethod = PostingMethod.InvoiceAP,
                //                                                Account = _locAccountMapLineByTax.Account,
                //                                                Debit = _locTotalPriceDebit,
                //                                                Credit = _locTotalPriceCredit,
                //                                                PurchaseOrder = _locPurchaseInvoiceXPO.PurchaseOrder,
                //                                                PurchaseInvoice = _locPurchaseInvoiceXPO,
                //                                                Company = _locPurchaseInvoiceXPO.Company,
                //                                            };
                //                                            _saveGeneralJournal.Save();
                //                                            _saveGeneralJournal.Session.CommitTransaction();

                //                                            if (_locAccountMapLineByTax.Account.Code != null)
                //                                            {
                //                                                ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                //                                                                         (new GroupOperator(GroupOperatorType.And,
                //                                                                          new BinaryOperator("Code", _locAccountMapLineByTax.Account.Code)));
                //                                                if (_locCOA != null)
                //                                                {
                //                                                    _locCOA.Debit = _locCOA.Debit + _locTotalPriceDebit;
                //                                                    _locCOA.Credit = _locCOA.Credit + _locTotalPriceCredit;
                //                                                    _locCOA.Save();
                //                                                    _locCOA.Session.CommitTransaction();
                //                                                }
                //                                            }
                //                                        }
                //                                    }
                //                                }
                //                            }
                //                        }
                //                    }
                //                }
                //                #endregion JournalMapTaxAccountGroup

                //                #region JournalMapBusinessPartnerAccountGroup
                //                if (_locPurchaseInvoiceXPO.PayToVendor != null)
                //                {
                //                    if (_locPurchaseInvoiceXPO.PayToVendor.BusinessPartnerAccountGroup != null)
                //                    {
                //                        XPCollection<JournalMap> _locJournalMapByBusinessPartners = new XPCollection<JournalMap>
                //                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                //                                                                                     new BinaryOperator("BusinessPartnerAccountGroup", _locPurchaseInvoiceXPO.PayToVendor.BusinessPartnerAccountGroup)));

                //                        if (_locJournalMapByBusinessPartners != null && _locJournalMapByBusinessPartners.Count() > 0)
                //                        {
                //                            foreach (JournalMap _locJournalMapByBusinessPartner in _locJournalMapByBusinessPartners)
                //                            {
                //                                XPCollection<JournalMapLine> _locJournalMapLineByBusinessPartners = new XPCollection<JournalMapLine>
                //                                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                //                                                                                                     new BinaryOperator("JournalMap", _locJournalMapByBusinessPartner)));

                //                                if (_locJournalMapLineByBusinessPartners != null && _locJournalMapLineByBusinessPartners.Count() > 0)
                //                                {
                //                                    foreach (JournalMapLine _locJournalMapLineByBusinessPartner in _locJournalMapLineByBusinessPartners)
                //                                    {
                //                                        AccountMap _locAccountMapByBusinessPartner = _currSession.FindObject<AccountMap>
                //                                                                                     (new GroupOperator(GroupOperatorType.And,
                //                                                                                      new BinaryOperator("Code", _locJournalMapLineByBusinessPartner.AccountMap.Code),
                //                                                                                      new BinaryOperator("PostingType", PostingType.Purchase),
                //                                                                                      new BinaryOperator("OrderType", OrderType.Item),
                //                                                                                      new BinaryOperator("PostingMethod", PostingMethod.InvoiceAP),
                //                                                                                      new BinaryOperator("PostingMethodType", PostingMethodType.Tax)));

                //                                        if (_locAccountMapByBusinessPartner != null)
                //                                        {
                //                                            XPCollection<AccountMapLine> _locAccountMapLineByBusinessPartners = new XPCollection<AccountMapLine>
                //                                                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                //                                                                                                                 new BinaryOperator("AccountMap", _locAccountMapByBusinessPartner),
                //                                                                                                                 new BinaryOperator("Active", true)));

                //                                            if (_locAccountMapLineByBusinessPartners != null && _locAccountMapLineByBusinessPartners.Count() > 0)
                //                                            {
                //                                                double _locTotalPriceDebit = 0;
                //                                                double _locTotalPriceCredit = 0;

                //                                                foreach (AccountMapLine _locAccountMapLine in _locAccountMapLineByBusinessPartners)
                //                                                {
                //                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                //                                                    {
                //                                                        _locTotalPriceDebit = _locTxOfAmount;
                //                                                    }
                //                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                //                                                    {
                //                                                        _locTotalPriceCredit = _locTxOfAmount;
                //                                                    }

                //                                                    GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                //                                                    {
                //                                                        PostingDate = now,
                //                                                        PostingType = PostingType.Purchase,
                //                                                        PostingMethod = PostingMethod.InvoiceAP,
                //                                                        Account = _locAccountMapLine.Account,
                //                                                        Debit = _locTotalPriceDebit,
                //                                                        Credit = _locTotalPriceCredit,
                //                                                        PurchaseOrder = _locPurchaseInvoiceXPO.PurchaseOrder,
                //                                                        PurchaseInvoice = _locPurchaseInvoiceXPO,
                //                                                        Company = _locPurchaseInvoiceXPO.Company,
                //                                                    };
                //                                                    _saveGeneralJournal.Save();
                //                                                    _saveGeneralJournal.Session.CommitTransaction();

                //                                                    if (_locAccountMapLine.Account.Code != null)
                //                                                    {
                //                                                        ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                //                                                                        (new GroupOperator(GroupOperatorType.And,
                //                                                                        new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                //                                                        if (_locCOA != null)
                //                                                        {
                //                                                            _locCOA.Debit = _locCOA.Debit + _locTotalPriceDebit;
                //                                                            _locCOA.Credit = _locCOA.Credit + _locTotalPriceCredit;
                //                                                            _locCOA.Save();
                //                                                            _locCOA.Session.CommitTransaction();
                //                                                        }
                //                                                    }
                //                                                }
                //                                            }
                //                                        }
                //                                    }
                //                                }
                //                            }
                //                        }
                //                    }
                //                }
                //                #endregion JournalMapBusinessPartnerAccountGroup
                //            }
                //        }
                //    }
                //    #endregion CreateInvoiceARWithTax

                //    #region CreateInvoiceARWithDiscount
                //    XPQuery<PurchaseInvoiceLine> _PurchaseInvoiceLinesQueryBasedDiscount = new XPQuery<PurchaseInvoiceLine>(_currSession);

                //    var _PurchaseInvoiceLineBasedDiscounts = from pil in _PurchaseInvoiceLinesQueryBasedTax
                //                                             where (pil.PurchaseInvoice == _locPurchaseInvoiceXPO
                //                                             && (pil.Status == Status.Progress || pil.Status == Status.Posted)
                //                                             && pil.Select == true)
                //                                             group pil by pil.Discount.DiscountAccountGroup into g
                //                                             select new { DiscountAccountGroup = g.Key };

                //    if (_PurchaseInvoiceLineBasedDiscounts != null && _PurchaseInvoiceLineBasedDiscounts.Count() > 0)
                //    {
                //        foreach (var _PurchaseInvoiceLineBasedDiscount in _PurchaseInvoiceLineBasedDiscounts)
                //        {
                //            XPCollection<PurchaseInvoiceLine> _locPurchaseInvoiceLines = new XPCollection<PurchaseInvoiceLine>(_currSession,
                //                                                                            new GroupOperator(GroupOperatorType.And,
                //                                                                            new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO),
                //                                                                            new BinaryOperator("Discount.DiscountAccountGroup", _PurchaseInvoiceLineBasedDiscount.DiscountAccountGroup),
                //                                                                            new BinaryOperator("Select", true)));

                //            if (_locPurchaseInvoiceLines != null && _locPurchaseInvoiceLines.Count() > 0)
                //            {
                //                foreach (PurchaseInvoiceLine _locPurchaseInvoiceLine in _locPurchaseInvoiceLines)
                //                {
                //                    _locDiscOfAmount = _locDiscOfAmount + _locPurchaseInvoiceLine.DiscAmount;
                //                }
                //            }

                //            if (_locDiscOfAmount > 0)
                //            {
                //                #region JournalMapDiscountAccountGroup
                //                XPCollection<JournalMap> _locJournalMapByDiscounts = new XPCollection<JournalMap>
                //                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                //                                                                 new BinaryOperator("DiscountAccountGroup", _PurchaseInvoiceLineBasedDiscount.DiscountAccountGroup)));

                //                if (_locJournalMapByDiscounts != null && _locJournalMapByDiscounts.Count() > 0)
                //                {
                //                    foreach (JournalMap _locJournalMapByDiscount in _locJournalMapByDiscounts)
                //                    {
                //                        XPCollection<JournalMapLine> _locJournalMapLineByDiscounts = new XPCollection<JournalMapLine>
                //                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                //                                                                                 new BinaryOperator("JournalMap", _locJournalMapByDiscount)));

                //                        if (_locJournalMapLineByDiscounts != null && _locJournalMapLineByDiscounts.Count() > 0)
                //                        {
                //                            foreach (JournalMapLine _locJournalMapLineByDiscount in _locJournalMapLineByDiscounts)
                //                            {
                //                                AccountMap _locAccountMapByDiscount = _currSession.FindObject<AccountMap>
                //                                                                 (new GroupOperator(GroupOperatorType.And,
                //                                                                  new BinaryOperator("Code", _locJournalMapLineByDiscount.AccountMap.Code),
                //                                                                  new BinaryOperator("PostingType", PostingType.Purchase),
                //                                                                  new BinaryOperator("OrderType", OrderType.Item),
                //                                                                  new BinaryOperator("PostingMethod", PostingMethod.InvoiceAP),
                //                                                                  new BinaryOperator("PostingMethodType", PostingMethodType.Discount)));

                //                                if (_locAccountMapByDiscount != null)
                //                                {
                //                                    XPCollection<AccountMapLine> _locAccountMapLineByDiscounts = new XPCollection<AccountMapLine>
                //                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                //                                                                                             new BinaryOperator("AccountMap", _locAccountMapByDiscount),
                //                                                                                             new BinaryOperator("Active", true)));

                //                                    if (_locAccountMapLineByDiscounts != null && _locAccountMapLineByDiscounts.Count() > 0)
                //                                    {
                //                                        double _locTotalPriceDebit = 0;
                //                                        double _locTotalPriceCredit = 0;

                //                                        foreach (AccountMapLine _locAccountMapLineByDiscount in _locAccountMapLineByDiscounts)
                //                                        {
                //                                            if (_locAccountMapLineByDiscount.AccountCharge == AccountCharge.Debit)
                //                                            {
                //                                                _locTotalPriceDebit = _locDiscOfAmount;
                //                                            }
                //                                            if (_locAccountMapLineByDiscount.AccountCharge == AccountCharge.Credit)
                //                                            {
                //                                                _locTotalPriceCredit = _locDiscOfAmount;
                //                                            }

                //                                            GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                //                                            {
                //                                                PostingDate = now,
                //                                                PostingType = PostingType.Purchase,
                //                                                PostingMethod = PostingMethod.InvoiceAP,
                //                                                Account = _locAccountMapLineByDiscount.Account,
                //                                                Debit = _locTotalPriceDebit,
                //                                                Credit = _locTotalPriceCredit,
                //                                                PurchaseOrder = _locPurchaseInvoiceXPO.PurchaseOrder,
                //                                                PurchaseInvoice = _locPurchaseInvoiceXPO,
                //                                                Company = _locPurchaseInvoiceXPO.Company,
                //                                            };
                //                                            _saveGeneralJournal.Save();
                //                                            _saveGeneralJournal.Session.CommitTransaction();

                //                                            if (_locAccountMapLineByDiscount.Account.Code != null)
                //                                            {
                //                                                ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                //                                                                         (new GroupOperator(GroupOperatorType.And,
                //                                                                          new BinaryOperator("Code", _locAccountMapLineByDiscount.Account.Code)));
                //                                                if (_locCOA != null)
                //                                                {
                //                                                    _locCOA.Debit = _locCOA.Debit + _locTotalPriceDebit;
                //                                                    _locCOA.Credit = _locCOA.Credit + _locTotalPriceCredit;
                //                                                    _locCOA.Save();
                //                                                    _locCOA.Session.CommitTransaction();
                //                                                }
                //                                            }
                //                                        }
                //                                    }
                //                                }
                //                            }
                //                        }
                //                    }
                //                }
                //                #endregion JournalMapDiscountAccountGroup

                //                #region JournalMapBusinessPartnerAccountGroup
                //                if (_locPurchaseInvoiceXPO.PayToVendor != null)
                //                {
                //                    if (_locPurchaseInvoiceXPO.PayToVendor.BusinessPartnerAccountGroup != null)
                //                    {
                //                        XPCollection<JournalMap> _locJournalMapByBusinessPartners = new XPCollection<JournalMap>
                //                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                //                                                                                     new BinaryOperator("BusinessPartnerAccountGroup", _locPurchaseInvoiceXPO.PayToVendor.BusinessPartnerAccountGroup)));

                //                        if (_locJournalMapByBusinessPartners != null && _locJournalMapByBusinessPartners.Count() > 0)
                //                        {
                //                            foreach (JournalMap _locJournalMapByBusinessPartner in _locJournalMapByBusinessPartners)
                //                            {
                //                                XPCollection<JournalMapLine> _locJournalMapLineByBusinessPartners = new XPCollection<JournalMapLine>
                //                                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                //                                                                                                     new BinaryOperator("JournalMap", _locJournalMapByBusinessPartner)));

                //                                if (_locJournalMapLineByBusinessPartners != null && _locJournalMapLineByBusinessPartners.Count() > 0)
                //                                {
                //                                    foreach (JournalMapLine _locJournalMapLineByBusinessPartner in _locJournalMapLineByBusinessPartners)
                //                                    {
                //                                        AccountMap _locAccountMapByBusinessPartner = _currSession.FindObject<AccountMap>
                //                                                                                     (new GroupOperator(GroupOperatorType.And,
                //                                                                                      new BinaryOperator("Code", _locJournalMapLineByBusinessPartner.AccountMap.Code),
                //                                                                                      new BinaryOperator("PostingType", PostingType.Purchase),
                //                                                                                      new BinaryOperator("OrderType", OrderType.Item),
                //                                                                                      new BinaryOperator("PostingMethod", PostingMethod.InvoiceAP),
                //                                                                                      new BinaryOperator("PostingMethodType", PostingMethodType.Discount)));

                //                                        if (_locAccountMapByBusinessPartner != null)
                //                                        {
                //                                            XPCollection<AccountMapLine> _locAccountMapLineByBusinessPartners = new XPCollection<AccountMapLine>
                //                                                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                //                                                                                                                 new BinaryOperator("AccountMap", _locAccountMapByBusinessPartner),
                //                                                                                                                 new BinaryOperator("Active", true)));

                //                                            if (_locAccountMapLineByBusinessPartners != null && _locAccountMapLineByBusinessPartners.Count() > 0)
                //                                            {
                //                                                double _locTotalPriceDebit = 0;
                //                                                double _locTotalPriceCredit = 0;

                //                                                foreach (AccountMapLine _locAccountMapLine in _locAccountMapLineByBusinessPartners)
                //                                                {
                //                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                //                                                    {
                //                                                        _locTotalPriceDebit = _locDiscOfAmount;
                //                                                    }
                //                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                //                                                    {
                //                                                        _locTotalPriceCredit = _locDiscOfAmount;
                //                                                    }

                //                                                    GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                //                                                    {
                //                                                        PostingDate = now,
                //                                                        PostingType = PostingType.Purchase,
                //                                                        PostingMethod = PostingMethod.InvoiceAP,
                //                                                        Account = _locAccountMapLine.Account,
                //                                                        Debit = _locTotalPriceDebit,
                //                                                        Credit = _locTotalPriceCredit,
                //                                                        PurchaseOrder = _locPurchaseInvoiceXPO.PurchaseOrder,
                //                                                        PurchaseInvoice = _locPurchaseInvoiceXPO,
                //                                                        Company = _locPurchaseInvoiceXPO.Company,
                //                                                    };
                //                                                    _saveGeneralJournal.Save();
                //                                                    _saveGeneralJournal.Session.CommitTransaction();

                //                                                    if (_locAccountMapLine.Account.Code != null)
                //                                                    {
                //                                                        ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                //                                                                                 (new GroupOperator(GroupOperatorType.And,
                //                                                                                  new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                //                                                        if (_locCOA != null)
                //                                                        {
                //                                                            _locCOA.Debit = _locCOA.Debit + _locTotalPriceDebit;
                //                                                            _locCOA.Credit = _locCOA.Credit + _locTotalPriceCredit;
                //                                                            _locCOA.Save();
                //                                                            _locCOA.Session.CommitTransaction();
                //                                                        }
                //                                                    }
                //                                                }
                //                                            }
                //                                        }
                //                                    }
                //                                }
                //                            }
                //                        }
                //                    }
                //                }
                //                #endregion JournalMapBusinessPartnerAccountGroup

                //            }
                //        }
                //    }
                //    #endregion CreateInvoiceARWithDiscount
                //}

                #endregion CreateInvoiceAPJournal
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PurchaseInvoice ", ex.ToString());
            }
        }

        private void SetPayableTransaction20Feb2020(Session _currSession, PurchaseInvoice _locPurchaseInvoiceXPO)
        {
            //try
            //{
            //    GlobalFunction _globFunc = new GlobalFunction();
            //    DateTime now = DateTime.Now;
            //    string _locSignCode = null;
            //    double _locTotalUnitPrice = _locPurchaseInvoiceXPO.Pay;
            //    if (_locPurchaseInvoiceXPO != null)
            //    {
            //        if (_locPurchaseInvoiceXPO.PayToVendor != null)
            //        {
            //            if (_locPurchaseInvoiceXPO.PayToVendor.Code != null)
            //            {
            //                BusinessPartner _locBusinessPartner = _currSession.FindObject<BusinessPartner>
            //                                                (new GroupOperator(GroupOperatorType.And,
            //                                                new BinaryOperator("Code", _locPurchaseInvoiceXPO.PayToVendor.Code)));


            //                if (_locBusinessPartner != null)
            //                {
            //                    _locSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.PayableTransaction);
            //                    if (_locSignCode != null)
            //                    {
            //                        PayableTransaction _savePayableTransaction = new PayableTransaction(_currSession)
            //                        {
            //                            PostingType = PostingType.Purchase,
            //                            PostingMethod = PostingMethod.Payment,
            //                            PaymentMethod = _locPurchaseInvoiceXPO.PaymentMethod,
            //                            EstimatedDate = _locPurchaseInvoiceXPO.EstimatedDate,
            //                            SignCode = _locSignCode,
            //                            PurchaseOrder = _locPurchaseInvoiceXPO.PurchaseOrder,
            //                            PurchaseInvoice = _locPurchaseInvoiceXPO,
            //                            CompanyDefault = _locPurchaseInvoiceXPO.Company,
            //                        };
            //                        _savePayableTransaction.Save();
            //                        _savePayableTransaction.Session.CommitTransaction();

            //                        PayableTransaction _locPayableTransaction = _currSession.FindObject<PayableTransaction>
            //                                                            (new GroupOperator(GroupOperatorType.And,
            //                                                            new BinaryOperator("SignCode", _locSignCode)));
            //                        if (_locPayableTransaction != null)
            //                        {
            //                            //Create Mapping for BusinessPartner
            //                            if (_locBusinessPartner.BusinessPartnerAccountGroup != null)
            //                            {
            //                                PayableTransactionLine _savePayableTransactionLine = new PayableTransactionLine(_currSession)
            //                                {
            //                                    PostingType = PostingType.Purchase,
            //                                    PostingMethod = PostingMethod.Payment,
            //                                    OpenVendor = true,
            //                                    Vendor = _locBusinessPartner,
            //                                    BankAccount = _locPurchaseInvoiceXPO.BankAccount,
            //                                    PayableTransaction = _locPayableTransaction,
            //                                    CloseCredit = true,
            //                                    Debit = _locPurchaseInvoiceXPO.Pay,
            //                                    CompanyDefault = _locPayableTransaction.CompanyDefault,
            //                                };
            //                                _savePayableTransactionLine.Save();
            //                                _savePayableTransactionLine.Session.CommitTransaction();
            //                            }

            //                            //Create Mapping For CompanyDefault
            //                            if (_locPayableTransaction.CompanyDefault != null)
            //                            {
            //                                BankAccount _locBankAccount = _currSession.FindObject<BankAccount>
            //                                                                (new GroupOperator(GroupOperatorType.And,
            //                                                                new BinaryOperator("Company", _locPayableTransaction.CompanyDefault),
            //                                                                new BinaryOperator("Default", true),
            //                                                                new BinaryOperator("Active", true)));
            //                                if (_locBankAccount != null)
            //                                {
            //                                    PayableTransactionLine _savePayableTransactionLine = new PayableTransactionLine(_currSession)
            //                                    {
            //                                        PostingType = PostingType.Purchase,
            //                                        PostingMethod = PostingMethod.Payment,
            //                                        OpenCompany = true,
            //                                        Company = _locPayableTransaction.CompanyDefault,
            //                                        BankAccount = _locBankAccount,
            //                                        PayableTransaction = _locPayableTransaction,
            //                                        CloseDebit = true,
            //                                        Credit = _locPurchaseInvoiceXPO.Pay,
            //                                        CompanyDefault = _locPayableTransaction.CompanyDefault,
            //                                    };
            //                                    _savePayableTransactionLine.Save();
            //                                    _savePayableTransactionLine.Session.CommitTransaction();
            //                                }

            //                            }

            //                            //Tambahkan PaymentOutPlan
            //                            SetPaymentOutPlan(_currSession, _locPurchaseInvoiceXPO, _locPayableTransaction);
            //                        }
            //                    }
            //                }
            //            }

            //        }
            //    }

            //}
            //catch (Exception ex)
            //{
            //    Tracing.Tracer.LogError("Business Object = PurchaseInvoice ", ex.ToString());
            //}
        }

        private void SetRemainAmount20Feb2020(Session _currSession, PurchaseInvoice _locPurchaseInvoiceXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;

                if (_locPurchaseInvoiceXPO != null)
                {
                    double _locPlan = 0;
                    double _locOutstanding = 0;
                    double _locMaxPay = 0;
                    double _locDP_Amount = 0;
                    bool _locDP_Posting = false;
                    bool _locActivationPosting = false;
                    string _locNotification = null;
                    Status _locStatus = Status.Posted;

                    //#region RemainAmountWithPostedCount!=0

                    //if (_locPurchaseInvoiceXPO.PostedCount > 0)
                    //{
                    //    _locMaxPay = _locPurchaseInvoiceXPO.MaxPay - _locPurchaseInvoiceXPO.CM_Amount;
                    //    if (_locPurchaseInvoiceXPO.Outstanding > 0 && _locMaxPay != _locPurchaseInvoiceXPO.Plan)
                    //    {
                    //        if (_locPurchaseInvoiceXPO.Pay > 0)
                    //        {
                    //            if (_locPurchaseInvoiceXPO.PurchaseOrder != null)
                    //            {
                    //                if (_locPurchaseInvoiceXPO.PurchaseOrder.SplitInvoice == true)
                    //                {
                    //                    double _locPay = 0;

                    //                    if (_locPurchaseInvoiceXPO.DP_Posting == true && _locPurchaseInvoiceXPO.DP_Amount > 0)
                    //                    {
                    //                        if (_locPurchaseInvoiceXPO.Pay <= _locPurchaseInvoiceXPO.DP_Amount)
                    //                        {
                    //                            _locDP_Amount = _locPurchaseInvoiceXPO.DP_Amount - _locPurchaseInvoiceXPO.Pay;
                    //                        }
                    //                        else
                    //                        {
                    //                            _locDP_Amount = 0;
                    //                        }

                    //                        if (_locDP_Amount > 0)
                    //                        {
                    //                            _locDP_Posting = true;
                    //                        }
                    //                    }

                    //                    PaymentOutPlan _locPaymentPlan = _currSession.FindObject<PaymentOutPlan>
                    //                                                (new GroupOperator(GroupOperatorType.And,
                    //                                                 new BinaryOperator("PurchaseOrder", _locPurchaseInvoiceXPO.PurchaseOrder),
                    //                                                 new BinaryOperator("DownPayment", true),
                    //                                                 new BinaryOperator("Deduction", false)));
                    //                    if (_locPaymentPlan != null)
                    //                    {
                    //                        if (_locPurchaseInvoiceXPO.Pay > _locPaymentPlan.Plan)
                    //                        {
                    //                            _locPay = _locPurchaseInvoiceXPO.Pay - _locPaymentPlan.Plan;
                    //                        }
                    //                        _locPlan = _locPurchaseInvoiceXPO.Plan + _locPay;
                    //                        _locOutstanding = _locPurchaseInvoiceXPO.Outstanding - _locPay;
                    //                    }
                    //                    else
                    //                    {
                    //                        _locPlan = _locPurchaseInvoiceXPO.Plan + _locPurchaseInvoiceXPO.Pay;
                    //                        _locOutstanding = _locPurchaseInvoiceXPO.Outstanding - _locPurchaseInvoiceXPO.Pay;
                    //                    }
                    //                }
                    //                else
                    //                {
                    //                    if (_locPurchaseInvoiceXPO.DP_Posting == true && _locPurchaseInvoiceXPO.DP_Amount > 0)
                    //                    {
                    //                        if (_locPurchaseInvoiceXPO.Pay <= _locPurchaseInvoiceXPO.DP_Amount)
                    //                        {
                    //                            _locDP_Amount = _locPurchaseInvoiceXPO.DP_Amount - _locPurchaseInvoiceXPO.Pay;
                    //                        }
                    //                        else
                    //                        {
                    //                            _locDP_Amount = 0;
                    //                        }

                    //                        if (_locDP_Amount > 0)
                    //                        {
                    //                            _locDP_Posting = true;
                    //                        }
                    //                    }

                    //                    _locPlan = _locPurchaseInvoiceXPO.Plan + _locPurchaseInvoiceXPO.Pay;
                    //                    _locOutstanding = _locPurchaseInvoiceXPO.Outstanding - _locPurchaseInvoiceXPO.Pay;
                    //                }
                    //            }


                    //            if (_locOutstanding == 0 && _locPlan == _locMaxPay)
                    //            {
                    //                _locActivationPosting = true;
                    //                _locStatus = Status.Posted;
                    //            }

                    //            _locPurchaseInvoiceXPO.DP_Posting = _locDP_Posting;
                    //            _locPurchaseInvoiceXPO.DP_Amount = _locDP_Amount;
                    //            _locPurchaseInvoiceXPO.Plan = _locPlan;
                    //            _locPurchaseInvoiceXPO.Outstanding = _locOutstanding;
                    //            _locPurchaseInvoiceXPO.Status = _locStatus;
                    //            _locPurchaseInvoiceXPO.StatusDate = now;
                    //            _locPurchaseInvoiceXPO.ActivationPosting = _locActivationPosting;
                    //            _locPurchaseInvoiceXPO.PostedCount = _locPurchaseInvoiceXPO.PostedCount + 1;
                    //            _locPurchaseInvoiceXPO.Save();
                    //            _locPurchaseInvoiceXPO.Session.CommitTransaction();
                    //        }
                    //    }
                    //}
                    //#endregion RemainAmountWithPostedCount!=0

                    //#region RemainAmountWithPostedCount=0
                    //else
                    //{
                    //    if (_locPurchaseInvoiceXPO.Pay > 0)
                    //    {
                    //        if (_locPurchaseInvoiceXPO.PurchaseOrder != null)
                    //        {
                    //            if (_locPurchaseInvoiceXPO.PurchaseOrder.SplitInvoice == true)
                    //            {
                    //                double _locPay = 0;

                    //                if (_locPurchaseInvoiceXPO.DP_Posting == true && _locPurchaseInvoiceXPO.DP_Amount > 0)
                    //                {
                    //                    if (_locPurchaseInvoiceXPO.Pay <= _locPurchaseInvoiceXPO.DP_Amount)
                    //                    {
                    //                        _locDP_Amount = _locPurchaseInvoiceXPO.DP_Amount - _locPurchaseInvoiceXPO.Pay;
                    //                    }
                    //                    else
                    //                    {
                    //                        _locDP_Amount = 0;
                    //                    }

                    //                    if (_locDP_Amount > 0)
                    //                    {
                    //                        _locDP_Posting = true;
                    //                    }
                    //                }

                    //                PaymentOutPlan _locPaymentPlan = _currSession.FindObject<PaymentOutPlan>
                    //                                                (new GroupOperator(GroupOperatorType.And,
                    //                                                 new BinaryOperator("PurchaseOrder", _locPurchaseInvoiceXPO.PurchaseOrder),
                    //                                                 new BinaryOperator("DownPayment", true),
                    //                                                 new BinaryOperator("Deduction", false)));

                    //                if (_locPaymentPlan != null)
                    //                {
                    //                    if (_locPurchaseInvoiceXPO.Pay > _locPaymentPlan.Plan)
                    //                    {
                    //                        _locPay = _locPurchaseInvoiceXPO.Pay - _locPaymentPlan.Plan;
                    //                    }

                    //                    _locPlan = _locPay;
                    //                    _locMaxPay = _locPurchaseInvoiceXPO.MaxPay - (_locPurchaseInvoiceXPO.CM_Amount + _locPaymentPlan.Plan);
                    //                    _locOutstanding = _locMaxPay - _locPlan;
                    //                    _locNotification = "Deduction By DownPayment";
                    //                }
                    //                else
                    //                {
                    //                    _locPlan = _locPurchaseInvoiceXPO.Pay;
                    //                    _locMaxPay = _locPurchaseInvoiceXPO.MaxPay - _locPurchaseInvoiceXPO.CM_Amount;
                    //                    _locOutstanding = _locMaxPay - _locPlan;
                    //                }

                    //            }
                    //            else
                    //            {
                    //                if (_locPurchaseInvoiceXPO.DP_Posting == true && _locPurchaseInvoiceXPO.DP_Amount > 0)
                    //                {
                    //                    if (_locPurchaseInvoiceXPO.Pay <= _locPurchaseInvoiceXPO.DP_Amount)
                    //                    {
                    //                        _locDP_Amount = _locPurchaseInvoiceXPO.DP_Amount - _locPurchaseInvoiceXPO.Pay;
                    //                    }
                    //                    else
                    //                    {
                    //                        _locDP_Amount = 0;
                    //                    }

                    //                    if (_locDP_Amount > 0)
                    //                    {
                    //                        _locDP_Posting = true;
                    //                    }
                    //                }

                    //                _locPlan = _locPurchaseInvoiceXPO.Pay;
                    //                _locMaxPay = _locPurchaseInvoiceXPO.MaxPay - _locPurchaseInvoiceXPO.CM_Amount;
                    //                _locOutstanding = _locMaxPay - _locPlan;
                    //            }
                    //        }


                    //        if (_locPlan == _locMaxPay && _locOutstanding == 0)
                    //        {
                    //            _locStatus = Status.Posted;
                    //            _locActivationPosting = true;
                    //        }

                    //        _locPurchaseInvoiceXPO.DP_Posting = _locDP_Posting;
                    //        _locPurchaseInvoiceXPO.DP_Amount = _locDP_Amount;
                    //        _locPurchaseInvoiceXPO.Plan = _locPlan;
                    //        _locPurchaseInvoiceXPO.Outstanding = _locOutstanding;
                    //        _locPurchaseInvoiceXPO.Status = _locStatus;
                    //        _locPurchaseInvoiceXPO.StatusDate = now;
                    //        _locPurchaseInvoiceXPO.Notification = _locNotification;
                    //        _locPurchaseInvoiceXPO.ActivationPosting = _locActivationPosting;
                    //        _locPurchaseInvoiceXPO.PostedCount = _locPurchaseInvoiceXPO.PostedCount + 1;
                    //        _locPurchaseInvoiceXPO.Save();
                    //        _locPurchaseInvoiceXPO.Session.CommitTransaction();

                    //    }
                    //}
                    //#endregion RemainAmountWithPostedCount=0
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PurchaseInvoice ", ex.ToString());
            }
        }

        #endregion PostingMethodOld

        #region Approval

        private void SetApprovalLine(Session _currentSession, PurchaseInvoice _locPurchaseInvoiceXPO, ApprovalLevel _locApprovalLevel)
        {
            try
            {
                DateTime now = DateTime.Now;

                ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO),
                                                                         new BinaryOperator("ApprovalLevel", _locApprovalLevel)));

                if (_locApprovalLineXPO == null)
                {
                    ApprovalLine _saveDataAL2 = new ApprovalLine(_currentSession)
                    {
                        ApprovalDate = now,
                        ApprovalStatus = Status.Approved,
                        ApprovalLevel = _locApprovalLevel,
                        PurchaseInvoice = _locPurchaseInvoiceXPO,
                    };
                    _saveDataAL2.Save();
                    _saveDataAL2.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseInvoice " + ex.ToString());
            }
        }

        #endregion Approval

        #region Email

        private string BackgroundBody(Session _currentSession, PurchaseInvoice _locPurchaseInvoiceXPO, MailSetupDetail _locMailSetDetail)
        {
            string body = string.Empty;

            if (_locPurchaseInvoiceXPO != null)
            {

                ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                   (new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO)));

                string Status = _locApprovalLineXPO.ApprovalStatus.ToString();

                using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Email/EmailTemplate.html")))
                {
                    body = reader.ReadToEnd();
                }
                body = body.Replace("{Code}", _locPurchaseInvoiceXPO.Code);

                #region Level
                if (_locPurchaseInvoiceXPO.ActiveApproved1 == true)
                {
                    string Active = "Level 1";
                    body = body.Replace("{Level}", Active);
                }
                else if (_locPurchaseInvoiceXPO.ActiveApproved2 == true)
                {
                    string Active = "Level 2";
                    body = body.Replace("{Level}", Active);
                }
                else if (_locPurchaseInvoiceXPO.ActiveApproved3 == true)
                {
                    string Active = "Level 3";
                    body = body.Replace("{Level}", Active);
                }
                else
                {
                    string Active = "Not Available";
                    body = body.Replace("{Level}", Active);
                }
                #endregion Level

                body = body.Replace("{Status}", Status);
                body = body.Replace("{Message}", _locMailSetDetail.MessageBody);

            }

            return body;

        }

        protected void SendEmail(Session _currentSession, PurchaseInvoice _locPurchaseInvoiceXPO, Status _locStatus, ApprovalLine _locApprovalLineXPO2)
        {
            DateTime now = DateTime.Now;
            var User = SecuritySystem.CurrentUserName;
            GlobalFunction _globFunc = new GlobalFunction();
            UserAccess _numLineUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", User));
            string _locMessageBody = null;

            try
            {
                if (_locPurchaseInvoiceXPO != null)
                {
                    XPCollection<MailSetupDetail> _locMailSetDetails = new XPCollection<MailSetupDetail>
                                                                       (_currentSession, new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("ObjectList", ObjectList.PurchaseInvoice),
                                                                        new BinaryOperator("UserAccess", _numLineUserAccess),
                                                                        new BinaryOperator("Active", true)));

                    if (_locMailSetDetails != null && _locMailSetDetails.Count > 0)
                    {
                        foreach (MailSetupDetail _locMailSetDetail in _locMailSetDetails)
                        {
                            if (_locMailSetDetail.MessageBody != null)
                            {
                                if (_locApprovalLineXPO2.EndApproval == true)
                                {
                                    _locMessageBody = this.BackgroundBody(_currentSession, _locPurchaseInvoiceXPO, _locMailSetDetail);
                                }
                                else
                                {
                                    _locMessageBody = this.BackgroundBody(_currentSession, _locPurchaseInvoiceXPO, _locMailSetDetail);
                                }
                            }

                            _globFunc.SetAndSendMail(_locMailSetDetail.SmtpHost, _locMailSetDetail.SmtpPort,
                            _locMailSetDetail.MailFrom, _locMailSetDetail.MailFromPassword, _locMailSetDetail.MailTo, _locMailSetDetail.MailSubject, _locMessageBody);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseInvoice " + ex.ToString());
            }
        }

        #endregion Email

        #region Global Method

        private void SuccessMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 4000;
            options.Message = string.Format("{0}", _locMessage);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }



        #endregion Global Method

        
    }
}
