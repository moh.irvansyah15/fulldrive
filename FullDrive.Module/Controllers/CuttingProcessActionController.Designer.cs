﻿namespace FullDrive.Module.Controllers
{
    partial class CuttingProcessActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.CuttingProcessProgressAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.CuttingProcessPostingAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.CuttingProcessListviewFilterSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // CuttingProcessProgressAction
            // 
            this.CuttingProcessProgressAction.Caption = "Progress";
            this.CuttingProcessProgressAction.ConfirmationMessage = null;
            this.CuttingProcessProgressAction.Id = "CuttingProcessProgressActionId";
            this.CuttingProcessProgressAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.CuttingProcess);
            this.CuttingProcessProgressAction.ToolTip = null;
            this.CuttingProcessProgressAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.CuttingProcessProgressAction_Execute);
            // 
            // CuttingProcessPostingAction
            // 
            this.CuttingProcessPostingAction.Caption = "Posting";
            this.CuttingProcessPostingAction.ConfirmationMessage = null;
            this.CuttingProcessPostingAction.Id = "CuttingProcessPostingActionId";
            this.CuttingProcessPostingAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.CuttingProcess);
            this.CuttingProcessPostingAction.ToolTip = null;
            this.CuttingProcessPostingAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.CuttingProcessPostingAction_Execute);
            // 
            // CuttingProcessListviewFilterSelectionAction
            // 
            this.CuttingProcessListviewFilterSelectionAction.Caption = "Filter";
            this.CuttingProcessListviewFilterSelectionAction.ConfirmationMessage = null;
            this.CuttingProcessListviewFilterSelectionAction.Id = "CuttingProcessListviewFilterSelectionActionId";
            this.CuttingProcessListviewFilterSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.CuttingProcessListviewFilterSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.CuttingProcess);
            this.CuttingProcessListviewFilterSelectionAction.ToolTip = null;
            this.CuttingProcessListviewFilterSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.CuttingProcessListviewFilterSelectionAction_Execute);
            // 
            // CuttingProcessActionController
            // 
            this.Actions.Add(this.CuttingProcessProgressAction);
            this.Actions.Add(this.CuttingProcessPostingAction);
            this.Actions.Add(this.CuttingProcessListviewFilterSelectionAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction CuttingProcessProgressAction;
        private DevExpress.ExpressApp.Actions.SimpleAction CuttingProcessPostingAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction CuttingProcessListviewFilterSelectionAction;
    }
}
