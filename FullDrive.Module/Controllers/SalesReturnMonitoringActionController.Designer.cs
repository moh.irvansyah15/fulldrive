﻿namespace FullDrive.Module.Controllers
{
    partial class SalesReturnMonitoringActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.SalesReturnMonitoringUnselectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.SalesReturnMonitoringSelectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // SalesReturnMonitoringUnselectAction
            // 
            this.SalesReturnMonitoringUnselectAction.Caption = "Unselect";
            this.SalesReturnMonitoringUnselectAction.ConfirmationMessage = null;
            this.SalesReturnMonitoringUnselectAction.Id = "SalesReturnMonitoringUnselectActionId";
            this.SalesReturnMonitoringUnselectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesReturnMonitoring);
            this.SalesReturnMonitoringUnselectAction.ToolTip = null;
            this.SalesReturnMonitoringUnselectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SalesReturnMonitoringUnselectAction_Execute);
            // 
            // SalesReturnMonitoringSelectAction
            // 
            this.SalesReturnMonitoringSelectAction.Caption = "Select";
            this.SalesReturnMonitoringSelectAction.ConfirmationMessage = null;
            this.SalesReturnMonitoringSelectAction.Id = "SalesReturnMonitoringSelectActionId";
            this.SalesReturnMonitoringSelectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesReturnMonitoring);
            this.SalesReturnMonitoringSelectAction.ToolTip = null;
            this.SalesReturnMonitoringSelectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SalesReturnMonitoringSelectAction_Execute);
            // 
            // SalesReturnMonitoringActionController
            // 
            this.Actions.Add(this.SalesReturnMonitoringUnselectAction);
            this.Actions.Add(this.SalesReturnMonitoringSelectAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction SalesReturnMonitoringUnselectAction;
        private DevExpress.ExpressApp.Actions.SimpleAction SalesReturnMonitoringSelectAction;
    }
}
