﻿namespace FullDrive.Module.Controllers
{
    partial class TransferOrderOutCollectionActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.TransferOrderOutCollectionShowTOMAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // TransferOrderOutCollectionShowTOMAction
            // 
            this.TransferOrderOutCollectionShowTOMAction.Caption = "Show TOM";
            this.TransferOrderOutCollectionShowTOMAction.ConfirmationMessage = null;
            this.TransferOrderOutCollectionShowTOMAction.Id = "TransferOrderOutCollectionShowTOMAction";
            this.TransferOrderOutCollectionShowTOMAction.ToolTip = null;
            this.TransferOrderOutCollectionShowTOMAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.TransferOrderOutCollectionShowTOMAction_Execute);
            // 
            // TransferOrderOutCollectionActionController
            // 
            this.Actions.Add(this.TransferOrderOutCollectionShowTOMAction);
            this.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.TransferOrderOutCollection);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction TransferOrderOutCollectionShowTOMAction;
    }
}
