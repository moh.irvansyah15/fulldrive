﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using FullDrive.Module.BusinessObjects;
using DevExpress.ExpressApp.Xpo;
using FullDrive.Module.CustomProcess;
using System.Collections;
using DevExpress.Xpo;

using System.Web;
using System.IO;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class FreightForwardingDestinationActionController : ViewController
    {
        private ChoiceActionItem _selectionListviewFilter;
        private ChoiceActionItem _selectionApprovalListviewFilter;
        private ChoiceActionItem _setApprovalLevel;

        public FreightForwardingDestinationActionController()
        {
            InitializeComponent();
            #region FilterStatus
            FreightForwardinDestinationListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                FreightForwardinDestinationListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
            #endregion FilterStatus

            #region FilterApproval
            FreightForwardingDestinationListviewFilterApprovalSelectionAction.Items.Clear();
            foreach (object _currApprovalFilter in Enum.GetValues(typeof(CustomProcess.ApprovalFilter)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.ApprovalFilter));
                _selectionApprovalListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApprovalFilter), _currApprovalFilter);
                FreightForwardingDestinationListviewFilterApprovalSelectionAction.Items.Add(_selectionApprovalListviewFilter);
            }
            #endregion FilterApproval
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }

        protected override void OnActivated()
        {
            TypeOfView = typeof(ObjectView);
            base.OnActivated();
            // Perform various tasks depending on the target View.
            #region Approval

            var _locUser = SecuritySystem.CurrentUserName;
            GlobalFunction _globalFunction = new GlobalFunction();
            Session _currentSession = null;
            if (this.ObjectSpace != null)
            {
                _currentSession = ((XPObjectSpace)this.ObjectSpace).Session;
            }

            if (_currentSession != null)
            {
                UserAccess _locUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", _locUser));

                if (_locUserAccess != null)
                {
                    FreightForwardingDestinationApprovalAction.Items.Clear();

                    XPCollection<ApplicationSetupDetail> _locAppSetupDetails = new XPCollection<ApplicationSetupDetail>
                                                                                 (_currentSession, new GroupOperator(GroupOperatorType.And,
                                                                                  new BinaryOperator("UserAccess", _locUserAccess),
                                                                                  new BinaryOperator("ObjectList", CustomProcess.ObjectList.FreightForwardingDestination),
                                                                                  new BinaryOperator("FunctionList", CustomProcess.FunctionList.Approval),
                                                                                  new BinaryOperator("Active", true)));

                    if (_locAppSetupDetails != null && _locAppSetupDetails.Count > 0)
                    {

                        foreach (ApplicationSetupDetail _locAppSetupDetail in _locAppSetupDetails)
                        {
                            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.ApprovalLevel)))
                            {
                                if (_locAppSetupDetail.ApprovalLevel != CustomProcess.ApprovalLevel.None)
                                {
                                    EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.ApprovalLevel));
                                    _setApprovalLevel = new ChoiceActionItem(_ed.GetCaption(_locAppSetupDetail.ApprovalLevel), _locAppSetupDetail.ApprovalLevel);
                                }
                            }
                            FreightForwardingDestinationApprovalAction.Items.Add(_setApprovalLevel);
                        }
                    }
                }
            }
            #endregion Approval
        }

        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }

        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
            
        }

        private void FreightForwardingDestinationProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        FreightForwardingDestination _locFreightForwardingDestinationOS = (FreightForwardingDestination)_objectSpace.GetObject(obj);

                        if (_locFreightForwardingDestinationOS != null)
                        {
                            if (_locFreightForwardingDestinationOS.Code != null)
                            {
                                _currObjectId = _locFreightForwardingDestinationOS.Code;

                                FreightForwardingDestination _locFreightForwardingDestinationXPO = _currSession.FindObject<FreightForwardingDestination>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locFreightForwardingDestinationXPO != null)
                                {
                                    #region Status
                                    if (_locFreightForwardingDestinationXPO.Status == Status.Open || _locFreightForwardingDestinationXPO.Status == Status.Progress || _locFreightForwardingDestinationXPO.Status == Status.Posted)
                                    {
                                        if (_locFreightForwardingDestinationXPO.Status == Status.Open)
                                        {
                                            _locFreightForwardingDestinationXPO.Status = Status.Progress;
                                            _locFreightForwardingDestinationXPO.StatusDate = now;
                                            _locFreightForwardingDestinationXPO.Save();
                                            _locFreightForwardingDestinationXPO.Session.CommitTransaction();
                                        }

                                        XPCollection<FreightForwardingDestinationLine> _locFreightForwardingDestinationLines = new XPCollection<FreightForwardingDestinationLine>
                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("FreightForwardingDestination", _locFreightForwardingDestinationXPO),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Open),
                                                            new BinaryOperator("Status", Status.Progress))
                                                            ));

                                        if (_locFreightForwardingDestinationLines != null && _locFreightForwardingDestinationLines.Count > 0)
                                        {
                                            foreach (FreightForwardingDestinationLine _locFreightForwardingDestinationLine in _locFreightForwardingDestinationLines)
                                            {
                                                _locFreightForwardingDestinationLine.Status = Status.Progress;
                                                _locFreightForwardingDestinationLine.StatusDate = now;
                                                _locFreightForwardingDestinationLine.Save();
                                                _locFreightForwardingDestinationLine.Session.CommitTransaction();
                                            }
                                        }

                                        XPCollection<ShipmentBookingDestinationCollection> _locShipmentBookingDestinationCollections = new XPCollection<ShipmentBookingDestinationCollection>
                                                                                 (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("FreightForwardingDestination", _locFreightForwardingDestinationXPO),
                                                                                 new GroupOperator(GroupOperatorType.Or,
                                                                                 new BinaryOperator("Status", Status.Open),
                                                                                 new BinaryOperator("Status", Status.Progress))));
                                        if (_locShipmentBookingDestinationCollections != null && _locShipmentBookingDestinationCollections.Count() > 0)
                                        {
                                            foreach (ShipmentBookingDestinationCollection _locShipmentBookingDestinationCollection in _locShipmentBookingDestinationCollections)
                                            {
                                                _locShipmentBookingDestinationCollection.Status = Status.Progress;
                                                _locShipmentBookingDestinationCollection.StatusDate = now;
                                                _locShipmentBookingDestinationCollection.Save();
                                                _locShipmentBookingDestinationCollection.Session.CommitTransaction();
                                            }
                                        }
                                    }
                                    #endregion Status

                                    SuccessMessageShow("FreightForwardingDestination has successfully updated to progress");
                                }
                                else
                                {
                                    ErrorMessageShow("Data FreightForwardingDestination Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data FreightForwardingDestination Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FreightForwardingDestination " + ex.ToString());
            }
        }

        private void FreightForwardinDestinationGetSBMAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        FreightForwardingDestination _locFreightForwardingDestinationOS = (FreightForwardingDestination)_objectSpace.GetObject(obj);

                        if (_locFreightForwardingDestinationOS != null)
                        {
                            if (_locFreightForwardingDestinationOS.Code != null)
                            {
                                _currObjectId = _locFreightForwardingDestinationOS.Code;

                                FreightForwardingDestination _locFreightForwardingDestinationXPO = _currSession.FindObject<FreightForwardingDestination>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locFreightForwardingDestinationXPO != null)
                                {
                                    if (_locFreightForwardingDestinationXPO.Status == Status.Open || _locFreightForwardingDestinationXPO.Status == Status.Progress)
                                    {
                                        XPCollection<ShipmentBookingDestinationCollection> _locShipmentBookingDestinationCollections = new XPCollection<ShipmentBookingDestinationCollection>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("FreightForwardingDestination", _locFreightForwardingDestinationXPO),
                                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("Status", Status.Open),
                                                                                        new BinaryOperator("Status", Status.Progress))));

                                        if (_locShipmentBookingDestinationCollections != null && _locShipmentBookingDestinationCollections.Count() > 0)
                                        {
                                            foreach (ShipmentBookingDestinationCollection _locShipmentBookingDestinationCollection in _locShipmentBookingDestinationCollections)
                                            {
                                                if (_locShipmentBookingDestinationCollection.FreightForwardingDestination != null)
                                                {
                                                    ShipmentApprovalLine _locShipmentApprovalLineXPO = _currSession.FindObject<ShipmentApprovalLine>
                                                                       (new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("EndApproval", true),
                                                                        new BinaryOperator("FreightForwardingDestination", _locShipmentBookingDestinationCollection.FreightForwardingDestination)));
                                                    if (_locShipmentApprovalLineXPO != null)
                                                    {
                                                        GetShipmentBookingMonitoring(_currSession, _locShipmentBookingDestinationCollection.ShipmentBooking, _locFreightForwardingDestinationXPO);
                                                    }
                                                }
                                            }
                                            SuccessMessageShow("ShipmentBookingMonitoring Has Been Successfully Getting into FreightForwardingDestination");
                                        }
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data FreightForwardingDestination Not Available");
                                }

                            }
                            else
                            {
                                ErrorMessageShow("Data FreightForwardingDestination Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FreightForwardingDestination " + ex.ToString());
            }
        }

        private void FreightForwardingDestinationApprovalAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                ApplicationSetupDetail _locAppSetDetail = null;
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                var User = SecuritySystem.CurrentUserName;
                Session _currentSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currentSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                UserAccess _locUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", User));

                foreach (Object obj in _objectsToProcess)
                {
                    FreightForwardingDestination _objInNewObjectSpace = (FreightForwardingDestination)_objectSpace.GetObject(obj);

                    if (_objInNewObjectSpace != null)
                    {
                        if (_objInNewObjectSpace.Code != null)
                        {
                            _currObjectId = _objInNewObjectSpace.Code;
                        }
                    }

                    if (_currObjectId != null)
                    {
                        FreightForwardingDestination _locFreightForwardingDestinationXPO = _currentSession.FindObject<FreightForwardingDestination>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("Code", _currObjectId)));

                        if (_locFreightForwardingDestinationXPO != null)
                        {
                            if (_locFreightForwardingDestinationXPO.Status == Status.Progress)
                            {
                                ShipmentApprovalLine _locShipmentApprovalLine = _currentSession.FindObject<ShipmentApprovalLine>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("FreightForwardingDestination", _locFreightForwardingDestinationXPO),
                                                                             new BinaryOperator("EndApproval", true)));
                                if (_locShipmentApprovalLine == null)
                                {
                                    #region Approval Level 1
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level1)
                                    {
                                        _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level1, ObjectList.FreightForwardingDestination);

                                        if (_locAppSetDetail != null)
                                        {
                                            //Buat bs input langsung ke approvalline
                                            ShipmentApprovalLine _locShipmentApprovalLineXPO = _currentSession.FindObject<ShipmentApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("FreightForwardingDestination", _locFreightForwardingDestinationXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level1)));
                                            if (_locShipmentApprovalLineXPO == null)
                                            {

                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ShipmentApprovalLine _saveDataAL = new ShipmentApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level1,
                                                        EndApproval = true,
                                                        FreightForwardingDestination = _locFreightForwardingDestinationXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locFreightForwardingDestinationXPO.ActivationPosting = true;
                                                    _locFreightForwardingDestinationXPO.ActiveApproved1 = false;
                                                    _locFreightForwardingDestinationXPO.ActiveApproved2 = false;
                                                    _locFreightForwardingDestinationXPO.ActiveApproved3 = true;
                                                    _locFreightForwardingDestinationXPO.Save();
                                                    _locFreightForwardingDestinationXPO.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ShipmentApprovalLine _saveDataAL = new ShipmentApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level1,
                                                        FreightForwardingDestination = _locFreightForwardingDestinationXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locFreightForwardingDestinationXPO.ActiveApproved1 = true;
                                                    _locFreightForwardingDestinationXPO.ActiveApproved2 = false;
                                                    _locFreightForwardingDestinationXPO.ActiveApproved3 = false;
                                                    _locFreightForwardingDestinationXPO.Save();
                                                    _locFreightForwardingDestinationXPO.Session.CommitTransaction();
                                                }

                                                //Send Email
                                                ShipmentApprovalLine _locShipmentApprovalLineXPO2 = _currentSession.FindObject<ShipmentApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("FreightForwardingDestination", _locFreightForwardingDestinationXPO)));
                                                if (_locShipmentApprovalLineXPO2 != null)
                                                {
                                                    SendEmail(_currentSession, _locFreightForwardingDestinationXPO, Status.Progress, _locShipmentApprovalLineXPO2);
                                                }

                                                SuccessMessageShow("FreightForwardingDestination has successfully Approve");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 1

                                    #region Approval Level 2
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level2)
                                    {
                                        _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level2, ObjectList.FreightForwardingDestination);

                                        if (_locAppSetDetail != null)
                                        {
                                            ShipmentApprovalLine _locShipmentApprovalLineXPO = _currentSession.FindObject<ShipmentApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("FreightForwardingDestination", _locFreightForwardingDestinationXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level2)));

                                            if (_locShipmentApprovalLineXPO == null)
                                            {
                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ShipmentApprovalLine _saveDataAL = new ShipmentApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level2,
                                                        EndApproval = true,
                                                        FreightForwardingDestination = _locFreightForwardingDestinationXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locFreightForwardingDestinationXPO.ActivationPosting = true;
                                                    _locFreightForwardingDestinationXPO.ActiveApproved1 = false;
                                                    _locFreightForwardingDestinationXPO.ActiveApproved2 = false;
                                                    _locFreightForwardingDestinationXPO.ActiveApproved3 = true;
                                                    _locFreightForwardingDestinationXPO.Save();
                                                    _locFreightForwardingDestinationXPO.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ShipmentApprovalLine _saveDataAL = new ShipmentApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level2,
                                                        FreightForwardingDestination = _locFreightForwardingDestinationXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    SetApprovalLine(_currentSession, _locFreightForwardingDestinationXPO, ApprovalLevel.Level1);

                                                    _locFreightForwardingDestinationXPO.ActiveApproved1 = false;
                                                    _locFreightForwardingDestinationXPO.ActiveApproved2 = true;
                                                    _locFreightForwardingDestinationXPO.ActiveApproved3 = false;
                                                    _locFreightForwardingDestinationXPO.Save();
                                                    _locFreightForwardingDestinationXPO.Session.CommitTransaction();
                                                }

                                                //Send Email
                                                ShipmentApprovalLine _locShipmentApprovalLineXPO2 = _currentSession.FindObject<ShipmentApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("FreightForwardingDestination", _locFreightForwardingDestinationXPO)));
                                                if (_locShipmentApprovalLineXPO2 != null)
                                                {
                                                    SendEmail(_currentSession, _locFreightForwardingDestinationXPO, Status.Progress, _locShipmentApprovalLineXPO2);
                                                }

                                                SuccessMessageShow("FreightForwardingDestination has successfully Approve");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 2

                                    #region Approval Level 3
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level3)
                                    {
                                        _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level3, ObjectList.FreightForwardingDestination);

                                        if (_locAppSetDetail != null)
                                        {
                                            ShipmentApprovalLine _locShipmentApprovalLineXPO = _currentSession.FindObject<ShipmentApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("FreightForwardingDestination", _locFreightForwardingDestinationXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level3)));

                                            if (_locShipmentApprovalLineXPO == null)
                                            {
                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ShipmentApprovalLine _saveDataAL = new ShipmentApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level3,
                                                        EndApproval = true,
                                                        FreightForwardingDestination = _locFreightForwardingDestinationXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locFreightForwardingDestinationXPO.ActivationPosting = true;
                                                    _locFreightForwardingDestinationXPO.ActiveApproved1 = false;
                                                    _locFreightForwardingDestinationXPO.ActiveApproved2 = false;
                                                    _locFreightForwardingDestinationXPO.ActiveApproved3 = true;
                                                    _locFreightForwardingDestinationXPO.Save();
                                                    _locFreightForwardingDestinationXPO.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ShipmentApprovalLine _saveDataAL = new ShipmentApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level3,
                                                        FreightForwardingDestination = _locFreightForwardingDestinationXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    SetApprovalLine(_currentSession, _locFreightForwardingDestinationXPO, ApprovalLevel.Level2);
                                                    SetApprovalLine(_currentSession, _locFreightForwardingDestinationXPO, ApprovalLevel.Level1);

                                                    _locFreightForwardingDestinationXPO.ActiveApproved1 = false;
                                                    _locFreightForwardingDestinationXPO.ActiveApproved2 = false;
                                                    _locFreightForwardingDestinationXPO.ActiveApproved3 = true;
                                                    _locFreightForwardingDestinationXPO.Save();
                                                    _locFreightForwardingDestinationXPO.Session.CommitTransaction();
                                                }

                                                //Send Email
                                                ShipmentApprovalLine _locShipmentApprovalLineXPO2 = _currentSession.FindObject<ShipmentApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("FreightForwardingDestination", _locFreightForwardingDestinationXPO)));
                                                if (_locShipmentApprovalLineXPO2 != null)
                                                {
                                                    SendEmail(_currentSession, _locFreightForwardingDestinationXPO, Status.Progress, _locShipmentApprovalLineXPO2);
                                                }

                                                SuccessMessageShow("FreightForwardingDestination has successfully Approve");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 3
                                }

                            }
                            else
                            {
                                ErrorMessageShow("FreightForwardingDestination Status Not Available");
                            }
                        }
                        else
                        {
                            ErrorMessageShow("FreightForwardingDestination Not Available");
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FreightForwardingDestination " + ex.ToString());
            }
        }

        private void FreightForwardingDestinationPostingPackageAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        FreightForwardingDestination _locFreightForwardingDestinationOS = (FreightForwardingDestination)_objectSpace.GetObject(obj);

                        if (_locFreightForwardingDestinationOS != null)
                        {
                            if (_locFreightForwardingDestinationOS.Code != null)
                            {
                                _currObjectId = _locFreightForwardingDestinationOS.Code;

                                FreightForwardingDestination _locFreightForwardingDestinationXPO = _currSession.FindObject<FreightForwardingDestination>
                                                                       (new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Code", _currObjectId)));

                                if (_locFreightForwardingDestinationXPO != null)
                                {
                                    if (_locFreightForwardingDestinationXPO.Status == Status.Progress || _locFreightForwardingDestinationXPO.Status == Status.Posted)
                                    {
                                        ShipmentApprovalLine _locShipmentApprovalLineXPO = _currSession.FindObject<ShipmentApprovalLine>
                                                                           (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("EndApproval", true),
                                                                            new BinaryOperator("FreightForwardingDestination", _locFreightForwardingDestinationXPO)));
                                        if (_locShipmentApprovalLineXPO != null)
                                        {
                                            SetDestinationPackageMonitoring(_currSession, _locFreightForwardingDestinationXPO);
                                            SetRemainQtyInPostPackage(_currSession, _locFreightForwardingDestinationXPO);
                                            SetPostingQtyInPostPackage(_currSession, _locFreightForwardingDestinationXPO);
                                            SetProcessCountInPostPackage(_currSession, _locFreightForwardingDestinationXPO);
                                            SetStatusFreightForwardinDestinationLine(_currSession, _locFreightForwardingDestinationXPO);
                                            SetNormalQuantityInPostPackage(_currSession, _locFreightForwardingDestinationXPO);
                                            SetFinalStatusFreightForwardinDestination(_currSession, _locFreightForwardingDestinationXPO);
                                            SuccessMessageShow("FreightForwardingDestination has successfully posted into DestinationPackageMonitoring");
                                        }
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data FreightForwardingDestination Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data FreightForwardingDestination Not Available");
                            }
                        }
                    }
                }
                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FreightForwardingDestination " + ex.ToString());
            }
        }

        private void FreightForwardinDestinationListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(FreightForwardingDestination)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FreightForwardingDestination " + ex.ToString());
            }
        }

        private void FreightForwardingDestinationListviewFilterApprovalSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(FreightForwardingDestination)))
                {
                    if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.Approval)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("ActiveApproved3", false, BinaryOperatorType.Equal),
                                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("ActiveApproved1", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", true, BinaryOperatorType.Equal)));
                    }
                    else if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.EndApproval)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new BinaryOperator("ActiveApproved3", true, BinaryOperatorType.Equal);
                    }
                    else if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.AllData)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("ActiveApproved1", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved3", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved1", false, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", false, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved3", false, BinaryOperatorType.Equal));
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FreightForwardingDestination " + ex.ToString());
            }
        }

        //======================================================= Code Only ======================================================

        #region GetSBM

        private void GetShipmentBookingMonitoring(Session _currSession, ShipmentBooking _locShipmentBookingXPO, FreightForwardingDestination _locFreightForwardingDestinationXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _currSignCode = null;

                if (_locShipmentBookingXPO != null && _locFreightForwardingDestinationXPO != null)
                {
                    //Hanya memindahkan SalesQuotationMonitoring ke SalesOrderLine
                    XPCollection<ShipmentBookingMonitoring> _locShipmentBookingMonitorings = new XPCollection<ShipmentBookingMonitoring>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("ShipmentBooking", _locShipmentBookingXPO),
                                                                                        new BinaryOperator("FreightForwardingDestination", _locFreightForwardingDestinationXPO),
                                                                                        new BinaryOperator("Select", true),
                                                                                        new BinaryOperator("Status", Status.Close, BinaryOperatorType.NotEqual)));

                    if (_locShipmentBookingMonitorings != null && _locShipmentBookingMonitorings.Count() > 0)
                    {
                        foreach (ShipmentBookingMonitoring _locShipmentBookingMonitoring in _locShipmentBookingMonitorings)
                        {
                            if (_locShipmentBookingMonitoring.ShipmentBookingLine != null)
                            {
                                _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.FreightForwardingDestinationLine);

                                if (_currSignCode != null)
                                {
                                    #region SaveFreightForwardingDestinationLine

                                    FreightForwardingDestinationLine _saveDataFreightForwardingDestinationLine = new FreightForwardingDestinationLine(_currSession)
                                    {
                                        SignCode = _currSignCode,
                                        ShipmentType = _locShipmentBookingMonitoring.ShipmentType,
                                        ServiceModa = _locShipmentBookingMonitoring.ServiceModa,
                                        TransportType = _locShipmentBookingMonitoring.TransportType,
                                        DeliveryType = _locShipmentBookingMonitoring.DeliveryType,
                                        TransactionTerm = _locShipmentBookingMonitoring.TransactionTerm,
                                        ItemGroup = _locShipmentBookingMonitoring.ItemGroup,
                                        Item = _locShipmentBookingMonitoring.Item,
                                        Description = _locShipmentBookingMonitoring.Description,
                                        MxDQty = _locShipmentBookingMonitoring.DQty,
                                        MxDUOM = _locShipmentBookingMonitoring.DUOM,
                                        MxQty = _locShipmentBookingMonitoring.Qty,
                                        MxUOM = _locShipmentBookingMonitoring.UOM,
                                        MxTQty = _locShipmentBookingMonitoring.TQty,
                                        MxUAmount = _locShipmentBookingMonitoring.UAmount,
                                        MxTUAmount = _locShipmentBookingMonitoring.TQty * _locShipmentBookingMonitoring.UAmount,
                                        DQty = _locShipmentBookingMonitoring.DQty,
                                        DUOM = _locShipmentBookingMonitoring.DUOM,
                                        Qty = _locShipmentBookingMonitoring.Qty,
                                        UOM = _locShipmentBookingMonitoring.UOM,
                                        TQty = _locShipmentBookingMonitoring.TQty,
                                        Currency = _locShipmentBookingMonitoring.Currency,
                                        PriceGroup = _locShipmentBookingMonitoring.PriceGroup,
                                        Price = _locShipmentBookingMonitoring.Price,
                                        PriceLine = _locShipmentBookingMonitoring.PriceLine,
                                        UAmount = _locShipmentBookingMonitoring.UAmount,
                                        TUAmount = _locShipmentBookingMonitoring.TUAmount,
                                        MultiTax = _locShipmentBookingMonitoring.MultiTax,
                                        Tax = _locShipmentBookingMonitoring.Tax,
                                        TxValue = _locShipmentBookingMonitoring.TxValue,
                                        TxAmount = _locShipmentBookingMonitoring.TxAmount,
                                        MultiDiscount = _locShipmentBookingMonitoring.MultiDiscount,
                                        Disc = _locShipmentBookingMonitoring.Disc,
                                        Discount = _locShipmentBookingMonitoring.Discount,
                                        DiscAmount = _locShipmentBookingMonitoring.DiscAmount,
                                        TAmount = _locShipmentBookingMonitoring.TAmount,
                                        CountryFrom = _locShipmentBookingMonitoring.CountryFrom,
                                        CityFrom = _locShipmentBookingMonitoring.CityFrom,
                                        AreaFrom = _locShipmentBookingMonitoring.AreaFrom,
                                        LocationFrom = _locShipmentBookingMonitoring.LocationFrom,
                                        TransportLocationFrom = _locShipmentBookingMonitoring.TransportLocationFrom,
                                        CountryTo = _locShipmentBookingMonitoring.CountryTo,
                                        CityTo = _locShipmentBookingMonitoring.CityTo,
                                        AreaTo = _locShipmentBookingMonitoring.AreaTo,
                                        LocationTo = _locShipmentBookingMonitoring.LocationTo,
                                        TransportLocationTo = _locShipmentBookingMonitoring.TransportLocationTo,
                                        ShipmentBookingMonitoring = _locShipmentBookingMonitoring,
                                        FreightForwardingDestination = _locFreightForwardingDestinationXPO,
                                    };
                                    _saveDataFreightForwardingDestinationLine.Save();
                                    _saveDataFreightForwardingDestinationLine.Session.CommitTransaction();

                                    #endregion SaveFreightForwardingDestinationLine

                                    FreightForwardingDestinationLine _locFreForwardingDestinationLine = _currSession.FindObject<FreightForwardingDestinationLine>
                                                                (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("SignCode", _currSignCode)));
                                    if (_locFreForwardingDestinationLine != null)
                                    {
                                        SetRemainQty(_currSession, _locShipmentBookingMonitoring);
                                        SetPostingQty(_currSession, _locShipmentBookingMonitoring);
                                        SetProcessCount(_currSession, _locShipmentBookingMonitoring);
                                        SetStatusShipmentBookingMonitoring(_currSession, _locShipmentBookingMonitoring);
                                        SetNormalQuantity(_currSession, _locShipmentBookingMonitoring);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = FreightForwardingDestination ", ex.ToString());
            }
        }

        private void SetRemainQty(Session _currSession, ShipmentBookingMonitoring _locShipmentBookingMonitoringXPO)
        {
            try
            {
                if (_locShipmentBookingMonitoringXPO != null)
                {
                    double _locRmDQty = 0;
                    double _locRmQty = 0;
                    double _locInvLineTotal = 0;
                    ItemUnitOfMeasure _locItemUOM = null;

                    #region ProcessCount=0
                    if (_locShipmentBookingMonitoringXPO.PostedCount == 0)
                    {
                        #region MaxQuantity
                        if (_locShipmentBookingMonitoringXPO.MxDQty > 0)
                        {
                            if (_locShipmentBookingMonitoringXPO.DQty > 0 && _locShipmentBookingMonitoringXPO.DQty <= _locShipmentBookingMonitoringXPO.MxDQty)
                            {
                                _locRmDQty = _locShipmentBookingMonitoringXPO.MxDQty - _locShipmentBookingMonitoringXPO.DQty;
                            }

                            if (_locShipmentBookingMonitoringXPO.Qty > 0 && _locShipmentBookingMonitoringXPO.Qty <= _locShipmentBookingMonitoringXPO.MxQty)
                            {
                                _locRmQty = _locShipmentBookingMonitoringXPO.MxQty - _locShipmentBookingMonitoringXPO.Qty;
                            }


                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                       (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Item", _locShipmentBookingMonitoringXPO.Item),
                                                        new BinaryOperator("UOM", _locShipmentBookingMonitoringXPO.MxUOM),
                                                        new BinaryOperator("DefaultUOM", _locShipmentBookingMonitoringXPO.MxDUOM),
                                                        new BinaryOperator("Active", true)));
                            if (_locItemUOM != null)
                            {
                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                }
                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                }
                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty + _locRmDQty;
                                }
                            }
                            else
                            {
                                _locInvLineTotal = _locRmQty + _locRmDQty;
                            }

                        }
                        #endregion MaxQuantity
                        #region NonMaxQuantity
                        else
                        {
                            if (_locShipmentBookingMonitoringXPO.DQty > 0)
                            {
                                _locRmDQty = 0;
                            }

                            if (_locShipmentBookingMonitoringXPO.Qty > 0)
                            {
                                _locRmQty = 0;
                            }


                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                       (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Item", _locShipmentBookingMonitoringXPO.Item),
                                                        new BinaryOperator("UOM", _locShipmentBookingMonitoringXPO.UOM),
                                                        new BinaryOperator("DefaultUOM", _locShipmentBookingMonitoringXPO.DUOM),
                                                        new BinaryOperator("Active", true)));
                            if (_locItemUOM != null)
                            {
                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                }
                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                }
                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty + _locRmDQty;
                                }
                            }
                            else
                            {
                                _locInvLineTotal = _locRmQty + _locRmDQty;
                            }
                        }
                        #endregion NonMaxQuantity
                    }
                    #endregion ProcessCount=0

                    #region ProcessCount>0
                    if (_locShipmentBookingMonitoringXPO.PostedCount > 0)
                    {
                        if (_locShipmentBookingMonitoringXPO.RmDQty > 0)
                        {
                            _locRmDQty = _locShipmentBookingMonitoringXPO.RmDQty - _locShipmentBookingMonitoringXPO.DQty;
                        }

                        if (_locShipmentBookingMonitoringXPO.RmQty > 0)
                        {
                            _locRmQty = _locShipmentBookingMonitoringXPO.RmQty - _locShipmentBookingMonitoringXPO.Qty;
                        }

                        if (_locShipmentBookingMonitoringXPO.MxDQty > 0 || _locShipmentBookingMonitoringXPO.MxQty > 0)
                        {
                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                       (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Item", _locShipmentBookingMonitoringXPO.Item),
                                                        new BinaryOperator("UOM", _locShipmentBookingMonitoringXPO.MxUOM),
                                                        new BinaryOperator("DefaultUOM", _locShipmentBookingMonitoringXPO.MxDUOM),
                                                        new BinaryOperator("Active", true)));
                        }
                        else
                        {
                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                       (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Item", _locShipmentBookingMonitoringXPO.Item),
                                                        new BinaryOperator("UOM", _locShipmentBookingMonitoringXPO.UOM),
                                                        new BinaryOperator("DefaultUOM", _locShipmentBookingMonitoringXPO.DUOM),
                                                        new BinaryOperator("Active", true)));
                        }


                        if (_locItemUOM != null)
                        {
                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                            }
                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                            }
                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = _locRmQty + _locRmDQty;
                            }
                        }
                        else
                        {
                            _locInvLineTotal = _locRmQty + _locRmDQty;
                        }

                    }
                    #endregion ProcessCount>0

                    _locShipmentBookingMonitoringXPO.RmDQty = _locRmDQty;
                    _locShipmentBookingMonitoringXPO.RmQty = _locRmQty;
                    _locShipmentBookingMonitoringXPO.RmTQty = _locInvLineTotal;
                    _locShipmentBookingMonitoringXPO.Save();
                    _locShipmentBookingMonitoringXPO.Session.CommitTransaction();

                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FreightForwardingDestination " + ex.ToString());
            }
        }

        private void SetPostingQty(Session _currSession, ShipmentBookingMonitoring _locShipmentBookingMonitoringXPO)
        {
            try
            {
                if (_locShipmentBookingMonitoringXPO != null)
                {
                    double _locPDQty = 0;
                    double _locPQty = 0;
                    double _locInvLineTotal = 0;
                    ItemUnitOfMeasure _locItemUOM = null;

                    #region ProcessCount=0
                    if (_locShipmentBookingMonitoringXPO.PostedCount == 0)
                    {
                        #region MaxQuantity
                        if (_locShipmentBookingMonitoringXPO.MxDQty > 0)
                        {
                            if (_locShipmentBookingMonitoringXPO.DQty > 0 && _locShipmentBookingMonitoringXPO.DQty <= _locShipmentBookingMonitoringXPO.MxDQty)
                            {
                                _locPDQty = _locShipmentBookingMonitoringXPO.DQty;
                            }

                            if (_locShipmentBookingMonitoringXPO.Qty > 0 && _locShipmentBookingMonitoringXPO.Qty <= _locShipmentBookingMonitoringXPO.MxQty)
                            {
                                _locPQty = _locShipmentBookingMonitoringXPO.Qty;
                            }


                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                       (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Item", _locShipmentBookingMonitoringXPO.Item),
                                                        new BinaryOperator("UOM", _locShipmentBookingMonitoringXPO.MxUOM),
                                                        new BinaryOperator("DefaultUOM", _locShipmentBookingMonitoringXPO.MxDUOM),
                                                        new BinaryOperator("Active", true)));
                            if (_locItemUOM != null)
                            {
                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                }
                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                }
                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locPQty + _locPDQty;
                                }
                            }
                            else
                            {
                                _locInvLineTotal = _locPQty + _locPDQty;
                            }

                        }
                        #endregion MaxQuantity
                        #region NonMaxQuantity
                        else
                        {
                            if (_locShipmentBookingMonitoringXPO.DQty > 0)
                            {
                                _locPDQty = _locShipmentBookingMonitoringXPO.DQty;
                            }

                            if (_locShipmentBookingMonitoringXPO.Qty > 0)
                            {
                                _locPQty = _locShipmentBookingMonitoringXPO.Qty;
                            }


                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                       (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Item", _locShipmentBookingMonitoringXPO.Item),
                                                        new BinaryOperator("UOM", _locShipmentBookingMonitoringXPO.UOM),
                                                        new BinaryOperator("DefaultUOM", _locShipmentBookingMonitoringXPO.DUOM),
                                                        new BinaryOperator("Active", true)));
                            if (_locItemUOM != null)
                            {
                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                }
                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                }
                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locPQty + _locPDQty;
                                }
                            }
                            else
                            {
                                _locInvLineTotal = _locPQty + _locPDQty;
                            }
                        }
                        #endregion NonMaxQuantity
                    }
                    #endregion ProcessCount=0

                    #region ProcessCount>0
                    if (_locShipmentBookingMonitoringXPO.PostedCount > 0)
                    {
                        if (_locShipmentBookingMonitoringXPO.PDQty > 0)
                        {
                            _locPDQty = _locShipmentBookingMonitoringXPO.PDQty + _locShipmentBookingMonitoringXPO.DQty;
                        }

                        if (_locShipmentBookingMonitoringXPO.PQty > 0)
                        {
                            _locPQty = _locShipmentBookingMonitoringXPO.PQty + _locShipmentBookingMonitoringXPO.Qty;
                        }

                        if (_locShipmentBookingMonitoringXPO.MxDQty > 0 || _locShipmentBookingMonitoringXPO.MxQty > 0)
                        {
                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                   (new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("Item", _locShipmentBookingMonitoringXPO.Item),
                                                    new BinaryOperator("UOM", _locShipmentBookingMonitoringXPO.MxUOM),
                                                    new BinaryOperator("DefaultUOM", _locShipmentBookingMonitoringXPO.MxDUOM),
                                                    new BinaryOperator("Active", true)));
                        }
                        else
                        {
                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                   (new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("Item", _locShipmentBookingMonitoringXPO.Item),
                                                    new BinaryOperator("UOM", _locShipmentBookingMonitoringXPO.UOM),
                                                    new BinaryOperator("DefaultUOM", _locShipmentBookingMonitoringXPO.DUOM),
                                                    new BinaryOperator("Active", true)));
                        }

                        if (_locItemUOM != null)
                        {
                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                            }
                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                            }
                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = _locPQty + _locPDQty;
                            }
                        }
                        else
                        {
                            _locInvLineTotal = _locPQty + _locPDQty;
                        }

                    }
                    #endregion ProcessCount>0

                    _locShipmentBookingMonitoringXPO.PDQty = _locPDQty;
                    _locShipmentBookingMonitoringXPO.PDUOM = _locShipmentBookingMonitoringXPO.DUOM;
                    _locShipmentBookingMonitoringXPO.PQty = _locPQty;
                    _locShipmentBookingMonitoringXPO.PUOM = _locShipmentBookingMonitoringXPO.UOM;
                    _locShipmentBookingMonitoringXPO.PTQty = _locInvLineTotal;
                    _locShipmentBookingMonitoringXPO.Save();
                    _locShipmentBookingMonitoringXPO.Session.CommitTransaction();

                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FreightForwardingDestination " + ex.ToString());
            }

        }

        private void SetProcessCount(Session _currSession, ShipmentBookingMonitoring _locShipmentBookingMonitoringXPO)
        {
            try
            {
                if (_locShipmentBookingMonitoringXPO != null)
                {
                    if (_locShipmentBookingMonitoringXPO.Status == Status.Progress || _locShipmentBookingMonitoringXPO.Status == Status.Posted)
                    {
                        _locShipmentBookingMonitoringXPO.PostedCount = _locShipmentBookingMonitoringXPO.PostedCount + 1;
                        _locShipmentBookingMonitoringXPO.Save();
                        _locShipmentBookingMonitoringXPO.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FreightForwardingDestination " + ex.ToString());
            }
        }

        private void SetStatusShipmentBookingMonitoring(Session _currSession, ShipmentBookingMonitoring _locShipmentBookingMonitoringXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locShipmentBookingMonitoringXPO != null)
                {
                    if (_locShipmentBookingMonitoringXPO.Status == Status.Progress || _locShipmentBookingMonitoringXPO.Status == Status.Posted)
                    {
                        if (_locShipmentBookingMonitoringXPO.RmDQty == 0 && _locShipmentBookingMonitoringXPO.RmQty == 0 && _locShipmentBookingMonitoringXPO.RmTQty == 0)
                        {
                            _locShipmentBookingMonitoringXPO.Status = Status.Close;
                            _locShipmentBookingMonitoringXPO.ActivationPosting = true;
                            _locShipmentBookingMonitoringXPO.StatusDate = now;
                        }
                        else
                        {
                            _locShipmentBookingMonitoringXPO.Status = Status.Posted;
                            _locShipmentBookingMonitoringXPO.StatusDate = now;
                        }
                        _locShipmentBookingMonitoringXPO.Save();
                        _locShipmentBookingMonitoringXPO.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FreightForwardingDestination " + ex.ToString());
            }
        }

        private void SetNormalQuantity(Session _currSession, ShipmentBookingMonitoring _locShipmentBookingMonitoringXPO)
        {
            try
            {
                if (_locShipmentBookingMonitoringXPO != null)
                {
                    if (_locShipmentBookingMonitoringXPO.Status == Status.Progress || _locShipmentBookingMonitoringXPO.Status == Status.Posted || _locShipmentBookingMonitoringXPO.Status == Status.Close)
                    {
                        if (_locShipmentBookingMonitoringXPO.DQty > 0 || _locShipmentBookingMonitoringXPO.Qty > 0)
                        {
                            _locShipmentBookingMonitoringXPO.Select = false;
                            _locShipmentBookingMonitoringXPO.DQty = 0;
                            _locShipmentBookingMonitoringXPO.Qty = 0;
                            _locShipmentBookingMonitoringXPO.FreightForwardingDestination = null;
                            _locShipmentBookingMonitoringXPO.Save();
                            _locShipmentBookingMonitoringXPO.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FreightForwardingDestination " + ex.ToString());
            }
        }

        #endregion GetSBM

        #region PostingPackage

        private void SetDestinationPackageMonitoring(Session _currSession, FreightForwardingDestination _locFreightForwardingDestinationXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locFreightForwardingDestinationXPO != null)
                {
                    XPCollection<FreightForwardingDestinationLine> _locFreightForwardingDestinationLines = new XPCollection<FreightForwardingDestinationLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("FreightForwardingDestination", _locFreightForwardingDestinationXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Progress),
                                                            new BinaryOperator("Status", Status.Posted))));

                    if (_locFreightForwardingDestinationLines != null && _locFreightForwardingDestinationLines.Count() > 0)
                    {

                        foreach (FreightForwardingDestinationLine _locFreightForwardingDestinationLine in _locFreightForwardingDestinationLines)
                        {
                            DestinationPackageMonitoring _saveDataDestinationPackageMonitoring = new DestinationPackageMonitoring(_currSession)
                            {
                                FreightForwardingDestination = _locFreightForwardingDestinationXPO,
                                FreightForwardingDestinationLine = _locFreightForwardingDestinationLine,
                                ShipmentType = _locFreightForwardingDestinationLine.ShipmentType,
                                ServiceModa = _locFreightForwardingDestinationLine.ServiceModa,
                                TransportType = _locFreightForwardingDestinationLine.TransportType,
                                DeliveryType = _locFreightForwardingDestinationLine.DeliveryType,
                                TransactionTerm = _locFreightForwardingDestinationLine.TransactionTerm,
                                ItemGroup = _locFreightForwardingDestinationLine.ItemGroup,
                                Item = _locFreightForwardingDestinationLine.Item,
                                MxDQty = _locFreightForwardingDestinationLine.DQty,
                                MxDUOM = _locFreightForwardingDestinationLine.DUOM,
                                MxQty = _locFreightForwardingDestinationLine.Qty,
                                MxUOM = _locFreightForwardingDestinationLine.UOM,
                                MxTQty = _locFreightForwardingDestinationLine.TQty,
                                DQty = _locFreightForwardingDestinationLine.DQty,
                                DUOM = _locFreightForwardingDestinationLine.DUOM,
                                Qty = _locFreightForwardingDestinationLine.Qty,
                                UOM = _locFreightForwardingDestinationLine.UOM,
                                TQty = _locFreightForwardingDestinationLine.TQty,
                                Currency = _locFreightForwardingDestinationLine.Currency,
                                PriceGroup = _locFreightForwardingDestinationLine.PriceGroup,
                                Price = _locFreightForwardingDestinationLine.Price,
                                PriceLine = _locFreightForwardingDestinationLine.PriceLine,
                                UAmount = _locFreightForwardingDestinationLine.UAmount,
                                TUAmount = _locFreightForwardingDestinationLine.TUAmount,
                                MultiTax = _locFreightForwardingDestinationLine.MultiTax,
                                Tax = _locFreightForwardingDestinationLine.Tax,
                                TxValue = _locFreightForwardingDestinationLine.TxValue,
                                TxAmount = _locFreightForwardingDestinationLine.TxAmount,
                                MultiDiscount = _locFreightForwardingDestinationLine.MultiDiscount,
                                Discount = _locFreightForwardingDestinationLine.Discount,
                                Disc = _locFreightForwardingDestinationLine.Disc,
                                DiscAmount = _locFreightForwardingDestinationLine.DiscAmount,
                                TAmount = _locFreightForwardingDestinationLine.TAmount,
                                CountryFrom = _locFreightForwardingDestinationLine.CountryFrom,
                                CityFrom = _locFreightForwardingDestinationLine.CityFrom,
                                AreaFrom = _locFreightForwardingDestinationLine.AreaFrom,
                                LocationFrom = _locFreightForwardingDestinationLine.LocationFrom,
                                TransportLocationFrom = _locFreightForwardingDestinationLine.TransportLocationFrom,
                                CountryTo = _locFreightForwardingDestinationLine.CountryTo,
                                CityTo = _locFreightForwardingDestinationLine.CityTo,
                                AreaTo = _locFreightForwardingDestinationLine.AreaTo,
                                LocationTo = _locFreightForwardingDestinationLine.LocationTo,
                                TransportLocationTo = _locFreightForwardingDestinationLine.TransportLocationTo,
                            };
                            _saveDataDestinationPackageMonitoring.Save();
                            _saveDataDestinationPackageMonitoring.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FreightForwardingDestination " + ex.ToString());
            }
        }

        private void SetRemainQtyInPostPackage(Session _currSession, FreightForwardingDestination _locFreightForwardingDestinationXPO)
        {
            try
            {
                if (_locFreightForwardingDestinationXPO != null)
                {
                    XPCollection<FreightForwardingDestinationLine> _locFreightForwardingDestinationLines = new XPCollection<FreightForwardingDestinationLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("FreightForwardingDestination", _locFreightForwardingDestinationXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Progress),
                                                            new BinaryOperator("Status", Status.Posted))));

                    if (_locFreightForwardingDestinationLines != null && _locFreightForwardingDestinationLines.Count > 0)
                    {
                        double _locRmDQty = 0;
                        double _locRmQty = 0;
                        double _locInvLineTotal = 0;
                        ItemUnitOfMeasure _locItemUOM = null;

                        foreach (FreightForwardingDestinationLine _locFreightForwardingDestinationLine in _locFreightForwardingDestinationLines)
                        {
                            #region ProcessCount=0
                            if (_locFreightForwardingDestinationLine.PostedCount == 0)
                            {
                                #region MaxQuantity
                                if (_locFreightForwardingDestinationLine.MxDQty > 0)
                                {
                                    if (_locFreightForwardingDestinationLine.DQty > 0 && _locFreightForwardingDestinationLine.DQty <= _locFreightForwardingDestinationLine.MxDQty)
                                    {
                                        _locRmDQty = _locFreightForwardingDestinationLine.MxDQty - _locFreightForwardingDestinationLine.DQty;
                                    }

                                    if (_locFreightForwardingDestinationLine.Qty > 0 && _locFreightForwardingDestinationLine.Qty <= _locFreightForwardingDestinationLine.MxQty)
                                    {
                                        _locRmQty = _locFreightForwardingDestinationLine.MxQty - _locFreightForwardingDestinationLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locFreightForwardingDestinationLine.Item),
                                                                new BinaryOperator("UOM", _locFreightForwardingDestinationLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locFreightForwardingDestinationLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty + _locRmDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }

                                }
                                #endregion MaxQuantity
                                #region NonMaxQuantity
                                else
                                {
                                    if (_locFreightForwardingDestinationLine.DQty > 0)
                                    {
                                        _locRmDQty = 0;
                                    }

                                    if (_locFreightForwardingDestinationLine.Qty > 0)
                                    {
                                        _locRmQty = 0;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locFreightForwardingDestinationLine.Item),
                                                                new BinaryOperator("UOM", _locFreightForwardingDestinationLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locFreightForwardingDestinationLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty + _locRmDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion ProcessCount=0

                            #region ProcessCount>0
                            if (_locFreightForwardingDestinationLine.PostedCount > 0)
                            {
                                if (_locFreightForwardingDestinationLine.RmDQty > 0)
                                {
                                    _locRmDQty = _locFreightForwardingDestinationLine.RmDQty - _locFreightForwardingDestinationLine.DQty;
                                }

                                if (_locFreightForwardingDestinationLine.RmQty > 0)
                                {
                                    _locRmQty = _locFreightForwardingDestinationLine.RmQty - _locFreightForwardingDestinationLine.Qty;
                                }

                                if (_locFreightForwardingDestinationLine.MxDQty > 0 || _locFreightForwardingDestinationLine.MxQty > 0)
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locFreightForwardingDestinationLine.Item),
                                                                new BinaryOperator("UOM", _locFreightForwardingDestinationLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locFreightForwardingDestinationLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                }
                                else
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locFreightForwardingDestinationLine.Item),
                                                                new BinaryOperator("UOM", _locFreightForwardingDestinationLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locFreightForwardingDestinationLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                }


                                if (_locItemUOM != null)
                                {
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locRmQty + _locRmDQty;
                                }

                            }
                            #endregion ProcessCount>0

                            _locFreightForwardingDestinationLine.RmDQty = _locRmDQty;
                            _locFreightForwardingDestinationLine.RmQty = _locRmQty;
                            _locFreightForwardingDestinationLine.RmTQty = _locInvLineTotal;
                            _locFreightForwardingDestinationLine.Save();
                            _locFreightForwardingDestinationLine.Session.CommitTransaction();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FreightForwardingDestination " + ex.ToString());
            }
        }

        private void SetPostingQtyInPostPackage(Session _currSession, FreightForwardingDestination _locFreightForwardingDestinationXPO)
        {
            try
            {
                if (_locFreightForwardingDestinationXPO != null)
                {
                    XPCollection<FreightForwardingDestinationLine> _locFreightForwardingDestinationLines = new XPCollection<FreightForwardingDestinationLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("FreightForwardingDestination", _locFreightForwardingDestinationXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Progress),
                                                            new BinaryOperator("Status", Status.Posted))));

                    if (_locFreightForwardingDestinationLines != null && _locFreightForwardingDestinationLines.Count > 0)
                    {
                        double _locPDQty = 0;
                        double _locPQty = 0;
                        double _locInvLineTotal = 0;
                        ItemUnitOfMeasure _locItemUOM = null;

                        foreach (FreightForwardingDestinationLine _locFreightForwardingDestinationLine in _locFreightForwardingDestinationLines)
                        {
                            #region ProcessCount=0
                            if (_locFreightForwardingDestinationLine.PostedCount == 0)
                            {
                                #region MaxQuantity
                                if (_locFreightForwardingDestinationLine.MxDQty > 0)
                                {
                                    if (_locFreightForwardingDestinationLine.DQty > 0 && _locFreightForwardingDestinationLine.DQty <= _locFreightForwardingDestinationLine.MxDQty)
                                    {
                                        _locPDQty = _locFreightForwardingDestinationLine.DQty;
                                    }

                                    if (_locFreightForwardingDestinationLine.Qty > 0 && _locFreightForwardingDestinationLine.Qty <= _locFreightForwardingDestinationLine.MxQty)
                                    {
                                        _locPQty = _locFreightForwardingDestinationLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locFreightForwardingDestinationLine.Item),
                                                                new BinaryOperator("UOM", _locFreightForwardingDestinationLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locFreightForwardingDestinationLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty + _locPDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }

                                }
                                #endregion MaxQuantity
                                #region NonMaxQuantity
                                else
                                {
                                    if (_locFreightForwardingDestinationLine.DQty > 0)
                                    {
                                        _locPDQty = _locFreightForwardingDestinationLine.DQty;
                                    }

                                    if (_locFreightForwardingDestinationLine.Qty > 0)
                                    {
                                        _locPQty = _locFreightForwardingDestinationLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locFreightForwardingDestinationLine.Item),
                                                                new BinaryOperator("UOM", _locFreightForwardingDestinationLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locFreightForwardingDestinationLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty + _locPDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion ProcessCount=0

                            #region ProcessCount>0
                            if (_locFreightForwardingDestinationLine.PostedCount > 0)
                            {
                                if (_locFreightForwardingDestinationLine.PDQty > 0)
                                {
                                    _locPDQty = _locFreightForwardingDestinationLine.PDQty + _locFreightForwardingDestinationLine.DQty;
                                }

                                if (_locFreightForwardingDestinationLine.PQty > 0)
                                {
                                    _locPQty = _locFreightForwardingDestinationLine.PQty + _locFreightForwardingDestinationLine.Qty;
                                }

                                if (_locFreightForwardingDestinationLine.MxDQty > 0 || _locFreightForwardingDestinationLine.MxQty > 0)
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                           (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Item", _locFreightForwardingDestinationLine.Item),
                                                            new BinaryOperator("UOM", _locFreightForwardingDestinationLine.MxUOM),
                                                            new BinaryOperator("DefaultUOM", _locFreightForwardingDestinationLine.MxDUOM),
                                                            new BinaryOperator("Active", true)));
                                }
                                else
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                           (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Item", _locFreightForwardingDestinationLine.Item),
                                                            new BinaryOperator("UOM", _locFreightForwardingDestinationLine.UOM),
                                                            new BinaryOperator("DefaultUOM", _locFreightForwardingDestinationLine.DUOM),
                                                            new BinaryOperator("Active", true)));
                                }

                                if (_locItemUOM != null)
                                {
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locPQty + _locPDQty;
                                }

                            }
                            #endregion ProcessCount>0

                            _locFreightForwardingDestinationLine.PDQty = _locPDQty;
                            _locFreightForwardingDestinationLine.PDUOM = _locFreightForwardingDestinationLine.DUOM;
                            _locFreightForwardingDestinationLine.PQty = _locPQty;
                            _locFreightForwardingDestinationLine.PUOM = _locFreightForwardingDestinationLine.UOM;
                            _locFreightForwardingDestinationLine.PTQty = _locInvLineTotal;
                            _locFreightForwardingDestinationLine.Save();
                            _locFreightForwardingDestinationLine.Session.CommitTransaction();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FreightForwardingDestination " + ex.ToString());
            }

        }

        private void SetProcessCountInPostPackage(Session _currSession, FreightForwardingDestination _locFreightForwardingDestinationXPO)
        {
            try
            {
                if (_locFreightForwardingDestinationXPO != null)
                {
                    XPCollection<FreightForwardingDestinationLine> _locFreightForwardingDestinationLines = new XPCollection<FreightForwardingDestinationLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("FreightForwardingDestination", _locFreightForwardingDestinationXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Progress),
                                                            new BinaryOperator("Status", Status.Posted))));

                    if (_locFreightForwardingDestinationLines != null && _locFreightForwardingDestinationLines.Count > 0)
                    {

                        foreach (FreightForwardingDestinationLine _locFreightForwardingDestinationLine in _locFreightForwardingDestinationLines)
                        {
                            _locFreightForwardingDestinationLine.PostedCount = _locFreightForwardingDestinationLine.PostedCount + 1;
                            _locFreightForwardingDestinationLine.Save();
                            _locFreightForwardingDestinationLine.Session.CommitTransaction();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FreightForwardingDestination " + ex.ToString());
            }
        }

        private void SetStatusFreightForwardinDestinationLine(Session _currSession, FreightForwardingDestination _locFreightForwardingDestinationXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locFreightForwardingDestinationXPO != null)
                {
                    XPCollection<FreightForwardingDestinationLine> _locFreightForwardingDestinationLines = new XPCollection<FreightForwardingDestinationLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("FreightForwardingDestination", _locFreightForwardingDestinationXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Progress),
                                                            new BinaryOperator("Status", Status.Posted))));

                    if (_locFreightForwardingDestinationLines != null && _locFreightForwardingDestinationLines.Count > 0)
                    {

                        foreach (FreightForwardingDestinationLine _locFreightForwardingDestinationLine in _locFreightForwardingDestinationLines)
                        {
                            if (_locFreightForwardingDestinationLine.RmDQty == 0 && _locFreightForwardingDestinationLine.RmQty == 0 && _locFreightForwardingDestinationLine.RmTQty == 0)
                            {
                                _locFreightForwardingDestinationLine.Status = Status.Close;
                                _locFreightForwardingDestinationLine.ActivationPosting = true;
                                _locFreightForwardingDestinationLine.StatusDate = now;
                            }
                            else
                            {
                                _locFreightForwardingDestinationLine.Status = Status.Posted;
                                _locFreightForwardingDestinationLine.StatusDate = now;
                            }
                            _locFreightForwardingDestinationLine.Save();
                            _locFreightForwardingDestinationLine.Session.CommitTransaction();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FreightForwardingDestination " + ex.ToString());
            }
        }

        private void SetNormalQuantityInPostPackage(Session _currSession, FreightForwardingDestination _locFreightForwardingDestinationXPO)
        {
            try
            {
                if (_locFreightForwardingDestinationXPO != null)
                {
                    XPCollection<FreightForwardingDestinationLine> _locFreightForwardingDestinationLines = new XPCollection<FreightForwardingDestinationLine>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("FreightForwardingDestination", _locFreightForwardingDestinationXPO),
                                                                                     new BinaryOperator("Select", true)));

                    if (_locFreightForwardingDestinationLines != null && _locFreightForwardingDestinationLines.Count > 0)
                    {
                        foreach (FreightForwardingDestinationLine _locFreightForwardingDestinationLine in _locFreightForwardingDestinationLines)
                        {
                            if (_locFreightForwardingDestinationLine.Status == Status.Progress || _locFreightForwardingDestinationLine.Status == Status.Posted || _locFreightForwardingDestinationLine.Status == Status.Close)
                            {
                                if (_locFreightForwardingDestinationLine.DQty > 0 || _locFreightForwardingDestinationLine.Qty > 0)
                                {
                                    _locFreightForwardingDestinationLine.Select = false;
                                    _locFreightForwardingDestinationLine.DQty = 0;
                                    _locFreightForwardingDestinationLine.Qty = 0;
                                    _locFreightForwardingDestinationLine.Save();
                                    _locFreightForwardingDestinationLine.Session.CommitTransaction();
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FreightForwardingDestination " + ex.ToString());
            }
        }

        private void SetFinalStatusFreightForwardinDestination(Session _currSession, FreightForwardingDestination _locFreightForwardingDestinationXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locStatusCount = 0;
                int _locFreightForwardingineCount = 0;

                if (_locFreightForwardingDestinationXPO != null)
                {

                    XPCollection<FreightForwardingDestinationLine> _locFreightForwardingDestinationLines = new XPCollection<FreightForwardingDestinationLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("FreightForwardingDestination", _locFreightForwardingDestinationXPO)));

                    if (_locFreightForwardingDestinationLines != null && _locFreightForwardingDestinationLines.Count() > 0)
                    {
                        _locFreightForwardingineCount = _locFreightForwardingDestinationLines.Count();

                        foreach (FreightForwardingDestinationLine _locFreightForwardingDestinationLine in _locFreightForwardingDestinationLines)
                        {
                            if (_locFreightForwardingDestinationLine.Status == Status.Close)
                            {
                                _locStatusCount = _locStatusCount + 1;
                            }
                        }
                    }

                    if (_locStatusCount == _locFreightForwardingineCount)
                    {
                        _locFreightForwardingDestinationXPO.ActivationPosting = true;
                        _locFreightForwardingDestinationXPO.Status = Status.Close;
                        _locFreightForwardingDestinationXPO.StatusDate = now;
                        _locFreightForwardingDestinationXPO.Save();
                        _locFreightForwardingDestinationXPO.Session.CommitTransaction();
                    }
                    else
                    {
                        _locFreightForwardingDestinationXPO.Status = Status.Posted;
                        _locFreightForwardingDestinationXPO.StatusDate = now;
                        _locFreightForwardingDestinationXPO.Save();
                        _locFreightForwardingDestinationXPO.Session.CommitTransaction();
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FreightForwardingDestination " + ex.ToString());
            }
        }

        #endregion PostingPackage

        #region Approval Process

        private void SetApprovalLine(Session _currentSession, FreightForwardingDestination _locFreightForwardingDestinationXPO, ApprovalLevel _locApprovalLevel)
        {
            try
            {
                DateTime now = DateTime.Now;

                ShipmentApprovalLine _locShipmentApprovalLineXPO = _currentSession.FindObject<ShipmentApprovalLine>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("FreightForwardingDestination", _locFreightForwardingDestinationXPO),
                                                                         new BinaryOperator("ApprovalLevel", _locApprovalLevel)));

                if (_locShipmentApprovalLineXPO == null)
                {
                    ShipmentApprovalLine _saveDataAL2 = new ShipmentApprovalLine(_currentSession)
                    {
                        ApprovalDate = now,
                        ApprovalStatus = Status.Approved,
                        ApprovalLevel = _locApprovalLevel,
                        FreightForwardingDestination = _locFreightForwardingDestinationXPO,
                    };
                    _saveDataAL2.Save();
                    _saveDataAL2.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FreightForwardingDestination " + ex.ToString());
            }
        }

        #endregion Approval Process

        #region Email

        private string BackgroundBody(Session _currentSession, FreightForwardingDestination _locFreightForwardingDestinationXPO, MailSetupDetail _locMailSetDetail)
        {
            string body = string.Empty;

            if (_locFreightForwardingDestinationXPO != null)
            {

                ShipmentApprovalLine _locShipmentApprovalLineXPO = _currentSession.FindObject<ShipmentApprovalLine>
                                                   (new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("FreightForwardingDestination", _locFreightForwardingDestinationXPO)));

                string Status = _locShipmentApprovalLineXPO.ApprovalStatus.ToString();

                using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Email/EmailTemplate.html")))
                {
                    body = reader.ReadToEnd();
                }
                body = body.Replace("{Code}", _locFreightForwardingDestinationXPO.Code);

                #region Level
                if (_locFreightForwardingDestinationXPO.ActiveApproved1 == true)
                {
                    string Active = "Level 1";
                    body = body.Replace("{Level}", Active);
                }
                else if (_locFreightForwardingDestinationXPO.ActiveApproved2 == true)
                {
                    string Active = "Level 2";
                    body = body.Replace("{Level}", Active);
                }
                else if (_locFreightForwardingDestinationXPO.ActiveApproved3 == true)
                {
                    string Active = "Level 3";
                    body = body.Replace("{Level}", Active);
                }
                else
                {
                    string Active = "Not Available";
                    body = body.Replace("{Level}", Active);
                }
                #endregion Level

                body = body.Replace("{Status}", Status);
                body = body.Replace("{Message}", _locMailSetDetail.MessageBody);
            }

            return body;
        }

        protected void SendEmail(Session _currentSession, FreightForwardingDestination _locFreightForwardinDestinationXPO, Status _locStatus, ShipmentApprovalLine _locShipmentApprovalLineXPO2)
        {
            DateTime now = DateTime.Now;
            var User = SecuritySystem.CurrentUserName;
            GlobalFunction _globFunc = new GlobalFunction();
            UserAccess _numLineUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", User));
            string _locMessageBody = null;

            try
            {
                if (_locFreightForwardinDestinationXPO != null)
                {
                    XPCollection<MailSetupDetail> _locMailSetDetails = new XPCollection<MailSetupDetail>
                                                                       (_currentSession, new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("ObjectList", ObjectList.FreightForwardingDestination),
                                                                        new BinaryOperator("UserAccess", _numLineUserAccess),
                                                                        new BinaryOperator("Active", true)));

                    if (_locMailSetDetails != null && _locMailSetDetails.Count > 0)
                    {
                        foreach (MailSetupDetail _locMailSetDetail in _locMailSetDetails)
                        {
                            if (_locMailSetDetail.MessageBody != null)
                            {
                                if (_locShipmentApprovalLineXPO2.EndApproval == true)
                                {
                                    _locMessageBody = this.BackgroundBody(_currentSession, _locFreightForwardinDestinationXPO, _locMailSetDetail);
                                }
                                else
                                {
                                    _locMessageBody = this.BackgroundBody(_currentSession, _locFreightForwardinDestinationXPO, _locMailSetDetail);
                                }
                            }

                            _globFunc.SetAndSendMail(_locMailSetDetail.SmtpHost, _locMailSetDetail.SmtpPort,
                            _locMailSetDetail.MailFrom, _locMailSetDetail.MailFromPassword, _locMailSetDetail.MailTo, _locMailSetDetail.MailSubject, _locMessageBody);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FreightForwardingDestination " + ex.ToString());
            }
        }

        #endregion Email

        #region Global

        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0} !", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }






        #endregion Global

        
    }
}
