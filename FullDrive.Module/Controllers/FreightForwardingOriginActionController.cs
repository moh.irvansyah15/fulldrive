﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using FullDrive.Module.BusinessObjects;
using DevExpress.ExpressApp.Xpo;
using FullDrive.Module.CustomProcess;
using System.Collections;
using DevExpress.Xpo;

using System.Web;
using System.IO;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class FreightForwardingOriginActionController : ViewController
    {
        private ChoiceActionItem _selectionListviewFilter;
        private ChoiceActionItem _selectionApprovalListviewFilter;
        private ChoiceActionItem _setApprovalLevel;

        public FreightForwardingOriginActionController()
        {
            InitializeComponent();
            #region FilterStatus
            FreightForwardinOriginListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                FreightForwardinOriginListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
            #endregion FilterStatus

            #region FilterApproval
            FreightForwardingOriginListviewFilterApprovalSelectionAction.Items.Clear();
            foreach (object _currApprovalFilter in Enum.GetValues(typeof(CustomProcess.ApprovalFilter)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.ApprovalFilter));
                _selectionApprovalListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApprovalFilter), _currApprovalFilter);
                FreightForwardingOriginListviewFilterApprovalSelectionAction.Items.Add(_selectionApprovalListviewFilter);
            }
            #endregion FilterApproval
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            TypeOfView = typeof(ObjectView);
            base.OnActivated();
            // Perform various tasks depending on the target View.
            #region Approval

            var _locUser = SecuritySystem.CurrentUserName;
            GlobalFunction _globalFunction = new GlobalFunction();
            Session _currentSession = null;
            if (this.ObjectSpace != null)
            {
                _currentSession = ((XPObjectSpace)this.ObjectSpace).Session;
            }

            if (_currentSession != null)
            {
                UserAccess _locUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", _locUser));

                if (_locUserAccess != null)
                {
                    FreightForwardingOriginApprovalAction.Items.Clear();

                    XPCollection<ApplicationSetupDetail> _locAppSetupDetails = new XPCollection<ApplicationSetupDetail>
                                                                                 (_currentSession, new GroupOperator(GroupOperatorType.And,
                                                                                  new BinaryOperator("UserAccess", _locUserAccess),
                                                                                  new BinaryOperator("ObjectList", CustomProcess.ObjectList.FreightForwardingOrigin),
                                                                                  new BinaryOperator("FunctionList", CustomProcess.FunctionList.Approval),
                                                                                  new BinaryOperator("Active", true)));

                    if (_locAppSetupDetails != null && _locAppSetupDetails.Count > 0)
                    {

                        foreach (ApplicationSetupDetail _locAppSetupDetail in _locAppSetupDetails)
                        {
                            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.ApprovalLevel)))
                            {
                                if (_locAppSetupDetail.ApprovalLevel != CustomProcess.ApprovalLevel.None)
                                {
                                    EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.ApprovalLevel));
                                    _setApprovalLevel = new ChoiceActionItem(_ed.GetCaption(_locAppSetupDetail.ApprovalLevel), _locAppSetupDetail.ApprovalLevel);
                                }
                            }
                            FreightForwardingOriginApprovalAction.Items.Add(_setApprovalLevel);
                        }
                    }
                }
            }
            #endregion Approval
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void FreightForwardingOriginProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        FreightForwardingOrigin _locFreightForwardingOriginOS = (FreightForwardingOrigin)_objectSpace.GetObject(obj);

                        if (_locFreightForwardingOriginOS != null)
                        {
                            if (_locFreightForwardingOriginOS.Code != null)
                            {
                                _currObjectId = _locFreightForwardingOriginOS.Code;

                                FreightForwardingOrigin _locFreightForwardingOriginXPO = _currSession.FindObject<FreightForwardingOrigin>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locFreightForwardingOriginXPO != null)
                                {
                                    #region Status
                                    if (_locFreightForwardingOriginXPO.Status == Status.Open || _locFreightForwardingOriginXPO.Status == Status.Progress || _locFreightForwardingOriginXPO.Status == Status.Posted)
                                    {
                                        if (_locFreightForwardingOriginXPO.Status == Status.Open)
                                        {
                                            _locFreightForwardingOriginXPO.Status = Status.Progress;
                                            _locFreightForwardingOriginXPO.StatusDate = now;
                                            _locFreightForwardingOriginXPO.Save();
                                            _locFreightForwardingOriginXPO.Session.CommitTransaction();
                                        }

                                        XPCollection<FreightForwardingOriginLine> _locFreightForwardingOriginLines = new XPCollection<FreightForwardingOriginLine>
                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("FreightForwardingOrigin", _locFreightForwardingOriginXPO),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Open),
                                                            new BinaryOperator("Status", Status.Progress))
                                                            ));

                                        if (_locFreightForwardingOriginLines != null && _locFreightForwardingOriginLines.Count > 0)
                                        {
                                            foreach (FreightForwardingOriginLine _locFreightForwardingOriginLine in _locFreightForwardingOriginLines)
                                            {
                                                _locFreightForwardingOriginLine.Status = Status.Progress;
                                                _locFreightForwardingOriginLine.StatusDate = now;
                                                _locFreightForwardingOriginLine.Save();
                                                _locFreightForwardingOriginLine.Session.CommitTransaction();
                                            }
                                        }

                                        XPCollection<ShipmentBookingOriginCollection> _locShipmentBookingOriginCollections = new XPCollection<ShipmentBookingOriginCollection>
                                                                                 (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("FreightForwardingOrigin", _locFreightForwardingOriginXPO),
                                                                                 new GroupOperator(GroupOperatorType.Or,
                                                                                 new BinaryOperator("Status", Status.Open),
                                                                                 new BinaryOperator("Status", Status.Progress))));
                                        if(_locShipmentBookingOriginCollections != null && _locShipmentBookingOriginCollections.Count() > 0)
                                        {
                                            foreach(ShipmentBookingOriginCollection _locShipmentBookingOriginCollection in _locShipmentBookingOriginCollections)
                                            {
                                                _locShipmentBookingOriginCollection.Status = Status.Progress;
                                                _locShipmentBookingOriginCollection.StatusDate = now;
                                                _locShipmentBookingOriginCollection.Save();
                                                _locShipmentBookingOriginCollection.Session.CommitTransaction();
                                            }
                                        }
                                    }
                                    #endregion Status

                                    SuccessMessageShow("FreightForwardingOrigin has successfully updated to progress");
                                }
                                else
                                {
                                    ErrorMessageShow("Data FreightForwardingOrigin Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data FreightForwardingOrigin Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FreightForwardingOrigin " + ex.ToString());
            }
        }

        private void FreightForwardinOriginGetSBMAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        FreightForwardingOrigin _locFreightForwardingOriginOS = (FreightForwardingOrigin)_objectSpace.GetObject(obj);

                        if (_locFreightForwardingOriginOS != null)
                        {
                            if (_locFreightForwardingOriginOS.Code != null)
                            {
                                _currObjectId = _locFreightForwardingOriginOS.Code;

                                FreightForwardingOrigin _locFreightForwardingOriginXPO = _currSession.FindObject<FreightForwardingOrigin>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locFreightForwardingOriginXPO != null)
                                {
                                    if (_locFreightForwardingOriginXPO.Status == Status.Open || _locFreightForwardingOriginXPO.Status == Status.Progress)
                                    {
                                        XPCollection<ShipmentBookingOriginCollection> _locShipmentBookingOriginCollections = new XPCollection<ShipmentBookingOriginCollection>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("FreightForwardingOrigin", _locFreightForwardingOriginXPO),
                                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("Status", Status.Open),
                                                                                        new BinaryOperator("Status", Status.Progress))));

                                        if (_locShipmentBookingOriginCollections != null && _locShipmentBookingOriginCollections.Count() > 0)
                                        {
                                            foreach (ShipmentBookingOriginCollection _locShipmentBookingOriginCollection in _locShipmentBookingOriginCollections)
                                            {
                                                if (_locShipmentBookingOriginCollection.FreightForwardingOrigin != null)
                                                {
                                                    ShipmentApprovalLine _locShipmentApprovalLineXPO = _currSession.FindObject<ShipmentApprovalLine>
                                                                       (new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("EndApproval", true),
                                                                        new BinaryOperator("FreightForwardingOrigin", _locShipmentBookingOriginCollection.FreightForwardingOrigin)));
                                                    if (_locShipmentApprovalLineXPO != null)
                                                    {
                                                        GetShipmentBookingMonitoring(_currSession, _locShipmentBookingOriginCollection.ShipmentBooking, _locFreightForwardingOriginXPO);
                                                    }
                                                }
                                            }
                                            SuccessMessageShow("ShipmentBookingMonitoring Has Been Successfully Getting into FreightForwardingOrigin");
                                        }
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data FreightForwardingOrigin Not Available");
                                }

                            }
                            else
                            {
                                ErrorMessageShow("Data FreightForwardingOrigin Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FreightForwardingOrigin " + ex.ToString());
            }
        }

        private void FreightForwardingOriginApprovalAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                ApplicationSetupDetail _locAppSetDetail = null;
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                var User = SecuritySystem.CurrentUserName;
                Session _currentSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currentSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                UserAccess _locUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", User));

                foreach (Object obj in _objectsToProcess)
                {
                    FreightForwardingOrigin _objInNewObjectSpace = (FreightForwardingOrigin)_objectSpace.GetObject(obj);

                    if (_objInNewObjectSpace != null)
                    {
                        if (_objInNewObjectSpace.Code != null)
                        {
                            _currObjectId = _objInNewObjectSpace.Code;
                        }
                    }

                    if (_currObjectId != null)
                    {
                        FreightForwardingOrigin _locFreightForwardingOriginXPO = _currentSession.FindObject<FreightForwardingOrigin>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("Code", _currObjectId)));

                        if (_locFreightForwardingOriginXPO != null)
                        {
                            if (_locFreightForwardingOriginXPO.Status == Status.Progress)
                            {
                                ShipmentApprovalLine _locShipmentApprovalLine = _currentSession.FindObject<ShipmentApprovalLine>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("FreightForwardingOrigin", _locFreightForwardingOriginXPO),
                                                                             new BinaryOperator("EndApproval", true)));
                                if (_locShipmentApprovalLine == null)
                                {
                                    #region Approval Level 1
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level1)
                                    {
                                        _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level1, ObjectList.FreightForwardingOrigin);

                                        if (_locAppSetDetail != null)
                                        {
                                            //Buat bs input langsung ke approvalline
                                            ShipmentApprovalLine _locShipmentApprovalLineXPO = _currentSession.FindObject<ShipmentApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("FreightForwardingOrigin", _locFreightForwardingOriginXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level1)));
                                            if (_locShipmentApprovalLineXPO == null)
                                            {

                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ShipmentApprovalLine _saveDataAL = new ShipmentApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level1,
                                                        EndApproval = true,
                                                        FreightForwardingOrigin = _locFreightForwardingOriginXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locFreightForwardingOriginXPO.ActivationPosting = true;
                                                    _locFreightForwardingOriginXPO.ActiveApproved1 = false;
                                                    _locFreightForwardingOriginXPO.ActiveApproved2 = false;
                                                    _locFreightForwardingOriginXPO.ActiveApproved3 = true;
                                                    _locFreightForwardingOriginXPO.Save();
                                                    _locFreightForwardingOriginXPO.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ShipmentApprovalLine _saveDataAL = new ShipmentApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level1,
                                                        FreightForwardingOrigin = _locFreightForwardingOriginXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locFreightForwardingOriginXPO.ActiveApproved1 = true;
                                                    _locFreightForwardingOriginXPO.ActiveApproved2 = false;
                                                    _locFreightForwardingOriginXPO.ActiveApproved3 = false;
                                                    _locFreightForwardingOriginXPO.Save();
                                                    _locFreightForwardingOriginXPO.Session.CommitTransaction();
                                                }

                                                //Send Email
                                                ShipmentApprovalLine _locShipmentApprovalLineXPO2 = _currentSession.FindObject<ShipmentApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("FreightForwardingOrigin", _locFreightForwardingOriginXPO)));
                                                if (_locShipmentApprovalLineXPO2 != null)
                                                {
                                                    SendEmail(_currentSession, _locFreightForwardingOriginXPO, Status.Progress, _locShipmentApprovalLineXPO2);
                                                }

                                                SuccessMessageShow("FreightForwardingOrigin has successfully Approve");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 1

                                    #region Approval Level 2
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level2)
                                    {
                                        _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level2, ObjectList.FreightForwardingOrigin);

                                        if (_locAppSetDetail != null)
                                        {
                                            ShipmentApprovalLine _locShipmentApprovalLineXPO = _currentSession.FindObject<ShipmentApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("FreightForwardingOrigin", _locFreightForwardingOriginXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level2)));

                                            if (_locShipmentApprovalLineXPO == null)
                                            {
                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ShipmentApprovalLine _saveDataAL = new ShipmentApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level2,
                                                        EndApproval = true,
                                                        FreightForwardingOrigin = _locFreightForwardingOriginXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locFreightForwardingOriginXPO.ActivationPosting = true;
                                                    _locFreightForwardingOriginXPO.ActiveApproved1 = false;
                                                    _locFreightForwardingOriginXPO.ActiveApproved2 = false;
                                                    _locFreightForwardingOriginXPO.ActiveApproved3 = true;
                                                    _locFreightForwardingOriginXPO.Save();
                                                    _locFreightForwardingOriginXPO.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ShipmentApprovalLine _saveDataAL = new ShipmentApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level2,
                                                        FreightForwardingOrigin = _locFreightForwardingOriginXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    SetApprovalLine(_currentSession, _locFreightForwardingOriginXPO, ApprovalLevel.Level1);

                                                    _locFreightForwardingOriginXPO.ActiveApproved1 = false;
                                                    _locFreightForwardingOriginXPO.ActiveApproved2 = true;
                                                    _locFreightForwardingOriginXPO.ActiveApproved3 = false;
                                                    _locFreightForwardingOriginXPO.Save();
                                                    _locFreightForwardingOriginXPO.Session.CommitTransaction();
                                                }

                                                //Send Email
                                                ShipmentApprovalLine _locShipmentApprovalLineXPO2 = _currentSession.FindObject<ShipmentApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("FreightForwardingOrigin", _locFreightForwardingOriginXPO)));
                                                if (_locShipmentApprovalLineXPO2 != null)
                                                {
                                                    SendEmail(_currentSession, _locFreightForwardingOriginXPO, Status.Progress, _locShipmentApprovalLineXPO2);
                                                }

                                                SuccessMessageShow("FreightForwardingOrigin has successfully Approve");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 2

                                    #region Approval Level 3
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level3)
                                    {
                                        _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level3, ObjectList.FreightForwardingOrigin);

                                        if (_locAppSetDetail != null)
                                        {
                                            ShipmentApprovalLine _locShipmentApprovalLineXPO = _currentSession.FindObject<ShipmentApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("FreightForwardingOrigin", _locFreightForwardingOriginXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level3)));

                                            if (_locShipmentApprovalLineXPO == null)
                                            {
                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ShipmentApprovalLine _saveDataAL = new ShipmentApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level3,
                                                        EndApproval = true,
                                                        FreightForwardingOrigin = _locFreightForwardingOriginXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locFreightForwardingOriginXPO.ActivationPosting = true;
                                                    _locFreightForwardingOriginXPO.ActiveApproved1 = false;
                                                    _locFreightForwardingOriginXPO.ActiveApproved2 = false;
                                                    _locFreightForwardingOriginXPO.ActiveApproved3 = true;
                                                    _locFreightForwardingOriginXPO.Save();
                                                    _locFreightForwardingOriginXPO.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ShipmentApprovalLine _saveDataAL = new ShipmentApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level3,
                                                        FreightForwardingOrigin = _locFreightForwardingOriginXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    SetApprovalLine(_currentSession, _locFreightForwardingOriginXPO, ApprovalLevel.Level2);
                                                    SetApprovalLine(_currentSession, _locFreightForwardingOriginXPO, ApprovalLevel.Level1);

                                                    _locFreightForwardingOriginXPO.ActiveApproved1 = false;
                                                    _locFreightForwardingOriginXPO.ActiveApproved2 = false;
                                                    _locFreightForwardingOriginXPO.ActiveApproved3 = true;
                                                    _locFreightForwardingOriginXPO.Save();
                                                    _locFreightForwardingOriginXPO.Session.CommitTransaction();
                                                }

                                                //Send Email
                                                ShipmentApprovalLine _locShipmentApprovalLineXPO2 = _currentSession.FindObject<ShipmentApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("FreightForwardingOrigin", _locFreightForwardingOriginXPO)));
                                                if (_locShipmentApprovalLineXPO2 != null)
                                                {
                                                    SendEmail(_currentSession, _locFreightForwardingOriginXPO, Status.Progress, _locShipmentApprovalLineXPO2);
                                                }

                                                SuccessMessageShow("FreightForwardingOrigin has successfully Approve");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 3
                                }

                            }
                            else
                            {
                                ErrorMessageShow("FreightForwardingOrigin Status Not Available");
                            }
                        }
                        else
                        {
                            ErrorMessageShow("FreightForwardingOrigin Not Available");
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FreightForwardingOrigin " + ex.ToString());
            }
        }

        private void FreightForwardingOriginPostingPackageAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        FreightForwardingOrigin _locFreightForwardingOriginOS = (FreightForwardingOrigin)_objectSpace.GetObject(obj);

                        if (_locFreightForwardingOriginOS != null)
                        {
                            if (_locFreightForwardingOriginOS.Code != null)
                            {
                                _currObjectId = _locFreightForwardingOriginOS.Code;

                                FreightForwardingOrigin _locFreightForwardingOriginXPO = _currSession.FindObject<FreightForwardingOrigin>
                                                                       (new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Code", _currObjectId)));

                                if (_locFreightForwardingOriginXPO != null)
                                {
                                    if (_locFreightForwardingOriginXPO.Status == Status.Progress || _locFreightForwardingOriginXPO.Status == Status.Posted)
                                    {
                                        ShipmentApprovalLine _locShipmentApprovalLineXPO = _currSession.FindObject<ShipmentApprovalLine>
                                                                           (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("EndApproval", true),
                                                                            new BinaryOperator("FreightForwardingOrigin", _locFreightForwardingOriginXPO)));
                                        if (_locShipmentApprovalLineXPO != null)
                                        {
                                            SetOriginPackageMonitoring(_currSession, _locFreightForwardingOriginXPO);
                                            SetRemainQtyInPostPackage(_currSession, _locFreightForwardingOriginXPO);
                                            SetPostingQtyInPostPackage(_currSession, _locFreightForwardingOriginXPO);
                                            SetProcessCountInPostPackage(_currSession, _locFreightForwardingOriginXPO);
                                            SetStatusFreightForwardinOriginLine(_currSession, _locFreightForwardingOriginXPO);
                                            SetNormalQuantityInPostPackage(_currSession, _locFreightForwardingOriginXPO);
                                            SetFinalStatusFreightForwardinOrigin(_currSession, _locFreightForwardingOriginXPO);
                                            SuccessMessageShow("FreightForwardingOrigin has successfully posted into OriginPackageMonitoring");
                                        }
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data FreightForwardingOrigin Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data FreightForwardingOrigin Not Available");
                            }
                        }
                    }
                }
                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FreightForwardingOrigin " + ex.ToString());
            }
        }

        private void FreightForwardinOriginListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(FreightForwardingOrigin)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FreightForwardingOrigin " + ex.ToString());
            }
        }

        private void FreightForwardingOriginListviewFilterApprovalSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(FreightForwardingOrigin)))
                {
                    if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.Approval)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("ActiveApproved3", false, BinaryOperatorType.Equal),
                                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("ActiveApproved1", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", true, BinaryOperatorType.Equal)));
                    }
                    else if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.EndApproval)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new BinaryOperator("ActiveApproved3", true, BinaryOperatorType.Equal);
                    }
                    else if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.AllData)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("ActiveApproved1", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved3", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved1", false, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", false, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved3", false, BinaryOperatorType.Equal));
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FreightForwardingOrigin " + ex.ToString());
            }
        }

        //===================================================== Code Only ======================================================

        #region GetSBM

        private void GetShipmentBookingMonitoring(Session _currSession, ShipmentBooking _locShipmentBookingXPO, FreightForwardingOrigin _locFreightForwardingOriginXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _currSignCode = null;

                if (_locShipmentBookingXPO != null && _locFreightForwardingOriginXPO != null)
                {
                    //Hanya memindahkan SalesQuotationMonitoring ke SalesOrderLine
                    XPCollection<ShipmentBookingMonitoring> _locShipmentBookingMonitorings = new XPCollection<ShipmentBookingMonitoring>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("ShipmentBooking", _locShipmentBookingXPO),
                                                                                        new BinaryOperator("FreightForwardingOrigin", _locFreightForwardingOriginXPO),
                                                                                        new BinaryOperator("Select", true),
                                                                                        new BinaryOperator("Status", Status.Close, BinaryOperatorType.NotEqual)));

                    if (_locShipmentBookingMonitorings != null && _locShipmentBookingMonitorings.Count() > 0)
                    {
                        foreach (ShipmentBookingMonitoring _locShipmentBookingMonitoring in _locShipmentBookingMonitorings)
                        {
                            if (_locShipmentBookingMonitoring.ShipmentBookingLine != null)
                            {
                                _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.FreightForwardingOriginLine);

                                if (_currSignCode != null)
                                {
                                    #region SaveFreightForwardingOriginLine

                                    FreightForwardingOriginLine _saveDataFreightForwardingOriginLine = new FreightForwardingOriginLine(_currSession)
                                    {
                                        SignCode = _currSignCode,
                                        ShipmentType = _locShipmentBookingMonitoring.ShipmentType,
                                        ServiceModa = _locShipmentBookingMonitoring.ServiceModa,
                                        TransportType = _locShipmentBookingMonitoring.TransportType,
                                        DeliveryType = _locShipmentBookingMonitoring.DeliveryType,
                                        TransactionTerm = _locShipmentBookingMonitoring.TransactionTerm,
                                        ItemGroup = _locShipmentBookingMonitoring.ItemGroup,
                                        Item = _locShipmentBookingMonitoring.Item,
                                        Description = _locShipmentBookingMonitoring.Description,
                                        MxDQty = _locShipmentBookingMonitoring.DQty,
                                        MxDUOM = _locShipmentBookingMonitoring.DUOM,
                                        MxQty = _locShipmentBookingMonitoring.Qty,
                                        MxUOM = _locShipmentBookingMonitoring.UOM,
                                        MxTQty = _locShipmentBookingMonitoring.TQty,
                                        MxUAmount = _locShipmentBookingMonitoring.UAmount,
                                        MxTUAmount = _locShipmentBookingMonitoring.TQty * _locShipmentBookingMonitoring.UAmount,
                                        DQty = _locShipmentBookingMonitoring.DQty,
                                        DUOM = _locShipmentBookingMonitoring.DUOM,
                                        Qty = _locShipmentBookingMonitoring.Qty,
                                        UOM = _locShipmentBookingMonitoring.UOM,
                                        TQty = _locShipmentBookingMonitoring.TQty,
                                        Currency = _locShipmentBookingMonitoring.Currency,
                                        PriceGroup = _locShipmentBookingMonitoring.PriceGroup,
                                        Price = _locShipmentBookingMonitoring.Price,
                                        PriceLine = _locShipmentBookingMonitoring.PriceLine,
                                        UAmount = _locShipmentBookingMonitoring.UAmount,
                                        TUAmount = _locShipmentBookingMonitoring.TUAmount,
                                        MultiTax = _locShipmentBookingMonitoring.MultiTax,
                                        Tax = _locShipmentBookingMonitoring.Tax,
                                        TxValue = _locShipmentBookingMonitoring.TxValue,
                                        TxAmount = _locShipmentBookingMonitoring.TxAmount,
                                        MultiDiscount = _locShipmentBookingMonitoring.MultiDiscount,
                                        Disc = _locShipmentBookingMonitoring.Disc,
                                        Discount = _locShipmentBookingMonitoring.Discount,
                                        DiscAmount = _locShipmentBookingMonitoring.DiscAmount,
                                        TAmount = _locShipmentBookingMonitoring.TAmount,
                                        CountryFrom = _locShipmentBookingMonitoring.CountryFrom,
                                        CityFrom = _locShipmentBookingMonitoring.CityFrom,
                                        AreaFrom = _locShipmentBookingMonitoring.AreaFrom,
                                        LocationFrom = _locShipmentBookingMonitoring.LocationFrom,
                                        TransportLocationFrom = _locShipmentBookingMonitoring.TransportLocationFrom,
                                        CountryTo = _locShipmentBookingMonitoring.CountryTo,
                                        CityTo = _locShipmentBookingMonitoring.CityTo,
                                        AreaTo = _locShipmentBookingMonitoring.AreaTo,
                                        LocationTo = _locShipmentBookingMonitoring.LocationTo,
                                        TransportLocationTo = _locShipmentBookingMonitoring.TransportLocationTo,
                                        ShipmentBookingMonitoring = _locShipmentBookingMonitoring,
                                        FreightForwardingOrigin = _locFreightForwardingOriginXPO,
                                    };
                                    _saveDataFreightForwardingOriginLine.Save();
                                    _saveDataFreightForwardingOriginLine.Session.CommitTransaction();

                                    #endregion SaveFreightForwardingOriginLine

                                    FreightForwardingOriginLine _locFreForwardingOriginLine = _currSession.FindObject<FreightForwardingOriginLine>
                                                                (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("SignCode", _currSignCode)));
                                    if (_locFreForwardingOriginLine != null)
                                    {
                                        SetRemainQty(_currSession, _locShipmentBookingMonitoring);
                                        SetPostingQty(_currSession, _locShipmentBookingMonitoring);
                                        SetProcessCount(_currSession, _locShipmentBookingMonitoring);
                                        SetStatusShipmentBookingMonitoring(_currSession, _locShipmentBookingMonitoring);
                                        SetNormalQuantity(_currSession, _locShipmentBookingMonitoring);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = FreightForwardingOrigin ", ex.ToString());
            }
        }

        private void SetRemainQty(Session _currSession, ShipmentBookingMonitoring _locShipmentBookingMonitoringXPO)
        {
            try
            {
                if (_locShipmentBookingMonitoringXPO != null)
                {
                    double _locRmDQty = 0;
                    double _locRmQty = 0;
                    double _locInvLineTotal = 0;
                    ItemUnitOfMeasure _locItemUOM = null;

                    #region ProcessCount=0
                    if (_locShipmentBookingMonitoringXPO.PostedCount == 0)
                    {
                        #region MaxQuantity
                        if (_locShipmentBookingMonitoringXPO.MxDQty > 0)
                        {
                            if (_locShipmentBookingMonitoringXPO.DQty > 0 && _locShipmentBookingMonitoringXPO.DQty <= _locShipmentBookingMonitoringXPO.MxDQty)
                            {
                                _locRmDQty = _locShipmentBookingMonitoringXPO.MxDQty - _locShipmentBookingMonitoringXPO.DQty;
                            }

                            if (_locShipmentBookingMonitoringXPO.Qty > 0 && _locShipmentBookingMonitoringXPO.Qty <= _locShipmentBookingMonitoringXPO.MxQty)
                            {
                                _locRmQty = _locShipmentBookingMonitoringXPO.MxQty - _locShipmentBookingMonitoringXPO.Qty;
                            }


                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                       (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Item", _locShipmentBookingMonitoringXPO.Item),
                                                        new BinaryOperator("UOM", _locShipmentBookingMonitoringXPO.MxUOM),
                                                        new BinaryOperator("DefaultUOM", _locShipmentBookingMonitoringXPO.MxDUOM),
                                                        new BinaryOperator("Active", true)));
                            if (_locItemUOM != null)
                            {
                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                }
                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                }
                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty + _locRmDQty;
                                }
                            }
                            else
                            {
                                _locInvLineTotal = _locRmQty + _locRmDQty;
                            }

                        }
                        #endregion MaxQuantity
                        #region NonMaxQuantity
                        else
                        {
                            if (_locShipmentBookingMonitoringXPO.DQty > 0)
                            {
                                _locRmDQty = 0;
                            }

                            if (_locShipmentBookingMonitoringXPO.Qty > 0)
                            {
                                _locRmQty = 0;
                            }


                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                       (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Item", _locShipmentBookingMonitoringXPO.Item),
                                                        new BinaryOperator("UOM", _locShipmentBookingMonitoringXPO.UOM),
                                                        new BinaryOperator("DefaultUOM", _locShipmentBookingMonitoringXPO.DUOM),
                                                        new BinaryOperator("Active", true)));
                            if (_locItemUOM != null)
                            {
                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                }
                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                }
                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty + _locRmDQty;
                                }
                            }
                            else
                            {
                                _locInvLineTotal = _locRmQty + _locRmDQty;
                            }
                        }
                        #endregion NonMaxQuantity
                    }
                    #endregion ProcessCount=0

                    #region ProcessCount>0
                    if (_locShipmentBookingMonitoringXPO.PostedCount > 0)
                    {
                        if (_locShipmentBookingMonitoringXPO.RmDQty > 0)
                        {
                            _locRmDQty = _locShipmentBookingMonitoringXPO.RmDQty - _locShipmentBookingMonitoringXPO.DQty;
                        }

                        if (_locShipmentBookingMonitoringXPO.RmQty > 0)
                        {
                            _locRmQty = _locShipmentBookingMonitoringXPO.RmQty - _locShipmentBookingMonitoringXPO.Qty;
                        }

                        if (_locShipmentBookingMonitoringXPO.MxDQty > 0 || _locShipmentBookingMonitoringXPO.MxQty > 0)
                        {
                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                       (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Item", _locShipmentBookingMonitoringXPO.Item),
                                                        new BinaryOperator("UOM", _locShipmentBookingMonitoringXPO.MxUOM),
                                                        new BinaryOperator("DefaultUOM", _locShipmentBookingMonitoringXPO.MxDUOM),
                                                        new BinaryOperator("Active", true)));
                        }
                        else
                        {
                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                       (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Item", _locShipmentBookingMonitoringXPO.Item),
                                                        new BinaryOperator("UOM", _locShipmentBookingMonitoringXPO.UOM),
                                                        new BinaryOperator("DefaultUOM", _locShipmentBookingMonitoringXPO.DUOM),
                                                        new BinaryOperator("Active", true)));
                        }


                        if (_locItemUOM != null)
                        {
                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                            }
                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                            }
                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = _locRmQty + _locRmDQty;
                            }
                        }
                        else
                        {
                            _locInvLineTotal = _locRmQty + _locRmDQty;
                        }

                    }
                    #endregion ProcessCount>0

                    _locShipmentBookingMonitoringXPO.RmDQty = _locRmDQty;
                    _locShipmentBookingMonitoringXPO.RmQty = _locRmQty;
                    _locShipmentBookingMonitoringXPO.RmTQty = _locInvLineTotal;
                    _locShipmentBookingMonitoringXPO.Save();
                    _locShipmentBookingMonitoringXPO.Session.CommitTransaction();
                    
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FreightForwardingOrigin " + ex.ToString());
            }
        }

        private void SetPostingQty(Session _currSession, ShipmentBookingMonitoring _locShipmentBookingMonitoringXPO)
        {
            try
            {
                if (_locShipmentBookingMonitoringXPO != null)
                {
                    double _locPDQty = 0;
                    double _locPQty = 0;
                    double _locInvLineTotal = 0;
                    ItemUnitOfMeasure _locItemUOM = null;

                    #region ProcessCount=0
                    if (_locShipmentBookingMonitoringXPO.PostedCount == 0)
                    {
                        #region MaxQuantity
                        if (_locShipmentBookingMonitoringXPO.MxDQty > 0)
                        {
                            if (_locShipmentBookingMonitoringXPO.DQty > 0 && _locShipmentBookingMonitoringXPO.DQty <= _locShipmentBookingMonitoringXPO.MxDQty)
                            {
                                _locPDQty = _locShipmentBookingMonitoringXPO.DQty;
                            }

                            if (_locShipmentBookingMonitoringXPO.Qty > 0 && _locShipmentBookingMonitoringXPO.Qty <= _locShipmentBookingMonitoringXPO.MxQty)
                            {
                                _locPQty = _locShipmentBookingMonitoringXPO.Qty;
                            }


                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                       (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Item", _locShipmentBookingMonitoringXPO.Item),
                                                        new BinaryOperator("UOM", _locShipmentBookingMonitoringXPO.MxUOM),
                                                        new BinaryOperator("DefaultUOM", _locShipmentBookingMonitoringXPO.MxDUOM),
                                                        new BinaryOperator("Active", true)));
                            if (_locItemUOM != null)
                            {
                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                }
                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                }
                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locPQty + _locPDQty;
                                }
                            }
                            else
                            {
                                _locInvLineTotal = _locPQty + _locPDQty;
                            }

                        }
                        #endregion MaxQuantity
                        #region NonMaxQuantity
                        else
                        {
                            if (_locShipmentBookingMonitoringXPO.DQty > 0)
                            {
                                _locPDQty = _locShipmentBookingMonitoringXPO.DQty;
                            }

                            if (_locShipmentBookingMonitoringXPO.Qty > 0)
                            {
                                _locPQty = _locShipmentBookingMonitoringXPO.Qty;
                            }


                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                       (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Item", _locShipmentBookingMonitoringXPO.Item),
                                                        new BinaryOperator("UOM", _locShipmentBookingMonitoringXPO.UOM),
                                                        new BinaryOperator("DefaultUOM", _locShipmentBookingMonitoringXPO.DUOM),
                                                        new BinaryOperator("Active", true)));
                            if (_locItemUOM != null)
                            {
                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                }
                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                }
                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locPQty + _locPDQty;
                                }
                            }
                            else
                            {
                                _locInvLineTotal = _locPQty + _locPDQty;
                            }
                        }
                        #endregion NonMaxQuantity
                    }
                    #endregion ProcessCount=0

                    #region ProcessCount>0
                    if (_locShipmentBookingMonitoringXPO.PostedCount > 0)
                    {
                        if (_locShipmentBookingMonitoringXPO.PDQty > 0)
                        {
                            _locPDQty = _locShipmentBookingMonitoringXPO.PDQty + _locShipmentBookingMonitoringXPO.DQty;
                        }

                        if (_locShipmentBookingMonitoringXPO.PQty > 0)
                        {
                            _locPQty = _locShipmentBookingMonitoringXPO.PQty + _locShipmentBookingMonitoringXPO.Qty;
                        }

                        if (_locShipmentBookingMonitoringXPO.MxDQty > 0 || _locShipmentBookingMonitoringXPO.MxQty > 0)
                        {
                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                   (new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("Item", _locShipmentBookingMonitoringXPO.Item),
                                                    new BinaryOperator("UOM", _locShipmentBookingMonitoringXPO.MxUOM),
                                                    new BinaryOperator("DefaultUOM", _locShipmentBookingMonitoringXPO.MxDUOM),
                                                    new BinaryOperator("Active", true)));
                        }
                        else
                        {
                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                   (new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("Item", _locShipmentBookingMonitoringXPO.Item),
                                                    new BinaryOperator("UOM", _locShipmentBookingMonitoringXPO.UOM),
                                                    new BinaryOperator("DefaultUOM", _locShipmentBookingMonitoringXPO.DUOM),
                                                    new BinaryOperator("Active", true)));
                        }

                        if (_locItemUOM != null)
                        {
                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                            }
                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                            }
                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = _locPQty + _locPDQty;
                            }
                        }
                        else
                        {
                            _locInvLineTotal = _locPQty + _locPDQty;
                        }

                    }
                    #endregion ProcessCount>0

                    _locShipmentBookingMonitoringXPO.PDQty = _locPDQty;
                    _locShipmentBookingMonitoringXPO.PDUOM = _locShipmentBookingMonitoringXPO.DUOM;
                    _locShipmentBookingMonitoringXPO.PQty = _locPQty;
                    _locShipmentBookingMonitoringXPO.PUOM = _locShipmentBookingMonitoringXPO.UOM;
                    _locShipmentBookingMonitoringXPO.PTQty = _locInvLineTotal;
                    _locShipmentBookingMonitoringXPO.Save();
                    _locShipmentBookingMonitoringXPO.Session.CommitTransaction();

                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FreightForwardingOrigin " + ex.ToString());
            }

        }

        private void SetProcessCount(Session _currSession, ShipmentBookingMonitoring _locShipmentBookingMonitoringXPO)
        {
            try
            {
                if (_locShipmentBookingMonitoringXPO != null)
                {
                    if (_locShipmentBookingMonitoringXPO.Status == Status.Progress || _locShipmentBookingMonitoringXPO.Status == Status.Posted)
                    {
                        _locShipmentBookingMonitoringXPO.PostedCount = _locShipmentBookingMonitoringXPO.PostedCount + 1;
                        _locShipmentBookingMonitoringXPO.Save();
                        _locShipmentBookingMonitoringXPO.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FreightForwardingOrigin " + ex.ToString());
            }
        }

        private void SetStatusShipmentBookingMonitoring(Session _currSession, ShipmentBookingMonitoring _locShipmentBookingMonitoringXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locShipmentBookingMonitoringXPO != null)
                {
                    if (_locShipmentBookingMonitoringXPO.Status == Status.Progress || _locShipmentBookingMonitoringXPO.Status == Status.Posted)
                    {
                        if (_locShipmentBookingMonitoringXPO.RmDQty == 0 && _locShipmentBookingMonitoringXPO.RmQty == 0 && _locShipmentBookingMonitoringXPO.RmTQty == 0)
                        {
                            _locShipmentBookingMonitoringXPO.Status = Status.Close;
                            _locShipmentBookingMonitoringXPO.ActivationPosting = true;
                            _locShipmentBookingMonitoringXPO.StatusDate = now;
                        }
                        else
                        {
                            _locShipmentBookingMonitoringXPO.Status = Status.Posted;
                            _locShipmentBookingMonitoringXPO.StatusDate = now;
                        }
                        _locShipmentBookingMonitoringXPO.Save();
                        _locShipmentBookingMonitoringXPO.Session.CommitTransaction();
                    }  
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FreightForwardingOrigin " + ex.ToString());
            }
        }

        private void SetNormalQuantity(Session _currSession, ShipmentBookingMonitoring _locShipmentBookingMonitoringXPO)
        {
            try
            {
                if (_locShipmentBookingMonitoringXPO != null)
                {
                    if (_locShipmentBookingMonitoringXPO.Status == Status.Progress || _locShipmentBookingMonitoringXPO.Status == Status.Posted || _locShipmentBookingMonitoringXPO.Status == Status.Close)
                    {
                        if (_locShipmentBookingMonitoringXPO.DQty > 0 || _locShipmentBookingMonitoringXPO.Qty > 0)
                        {
                            _locShipmentBookingMonitoringXPO.Select = false;
                            _locShipmentBookingMonitoringXPO.DQty = 0;
                            _locShipmentBookingMonitoringXPO.Qty = 0;
                            _locShipmentBookingMonitoringXPO.FreightForwardingOrigin = null;
                            _locShipmentBookingMonitoringXPO.Save();
                            _locShipmentBookingMonitoringXPO.Session.CommitTransaction();
                        }
                    }  
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FreightForwardingOrigin " + ex.ToString());
            }
        }

        #endregion GetSBM

        #region PostingPackage

        private void SetOriginPackageMonitoring(Session _currSession, FreightForwardingOrigin _locFreightForwardingOriginXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locFreightForwardingOriginXPO != null)
                {
                    XPCollection<FreightForwardingOriginLine> _locFreightForwardingOriginLines = new XPCollection<FreightForwardingOriginLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("FreightForwardingOrigin", _locFreightForwardingOriginXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Progress),
                                                            new BinaryOperator("Status", Status.Posted))));

                    if (_locFreightForwardingOriginLines != null && _locFreightForwardingOriginLines.Count() > 0)
                    {

                        foreach (FreightForwardingOriginLine _locFreightForwardingOriginLine in _locFreightForwardingOriginLines)
                        {
                            OriginPackageMonitoring _saveDataOriginPackageMonitoring = new OriginPackageMonitoring(_currSession)
                            {
                                FreightForwardingOrigin = _locFreightForwardingOriginXPO,
                                FreightForwardingOriginLine = _locFreightForwardingOriginLine,
                                ShipmentType = _locFreightForwardingOriginLine.ShipmentType,
                                ServiceModa = _locFreightForwardingOriginLine.ServiceModa,
                                TransportType = _locFreightForwardingOriginLine.TransportType,
                                DeliveryType = _locFreightForwardingOriginLine.DeliveryType,
                                TransactionTerm = _locFreightForwardingOriginLine.TransactionTerm,
                                ItemGroup = _locFreightForwardingOriginLine.ItemGroup,
                                Item = _locFreightForwardingOriginLine.Item,
                                MxDQty = _locFreightForwardingOriginLine.DQty,
                                MxDUOM = _locFreightForwardingOriginLine.DUOM,
                                MxQty = _locFreightForwardingOriginLine.Qty,
                                MxUOM = _locFreightForwardingOriginLine.UOM,
                                MxTQty = _locFreightForwardingOriginLine.TQty,
                                DQty = _locFreightForwardingOriginLine.DQty,
                                DUOM = _locFreightForwardingOriginLine.DUOM,
                                Qty = _locFreightForwardingOriginLine.Qty,
                                UOM = _locFreightForwardingOriginLine.UOM,
                                TQty = _locFreightForwardingOriginLine.TQty,
                                Currency = _locFreightForwardingOriginLine.Currency,
                                PriceGroup = _locFreightForwardingOriginLine.PriceGroup,
                                Price = _locFreightForwardingOriginLine.Price,
                                PriceLine = _locFreightForwardingOriginLine.PriceLine,
                                UAmount = _locFreightForwardingOriginLine.UAmount,
                                TUAmount = _locFreightForwardingOriginLine.TUAmount,
                                MultiTax = _locFreightForwardingOriginLine.MultiTax,
                                Tax = _locFreightForwardingOriginLine.Tax,
                                TxValue = _locFreightForwardingOriginLine.TxValue,
                                TxAmount = _locFreightForwardingOriginLine.TxAmount,
                                MultiDiscount = _locFreightForwardingOriginLine.MultiDiscount,
                                Discount = _locFreightForwardingOriginLine.Discount,
                                Disc = _locFreightForwardingOriginLine.Disc,
                                DiscAmount = _locFreightForwardingOriginLine.DiscAmount,
                                TAmount = _locFreightForwardingOriginLine.TAmount,
                                CountryFrom = _locFreightForwardingOriginLine.CountryFrom,
                                CityFrom = _locFreightForwardingOriginLine.CityFrom,
                                AreaFrom = _locFreightForwardingOriginLine.AreaFrom,
                                LocationFrom = _locFreightForwardingOriginLine.LocationFrom,
                                TransportLocationFrom = _locFreightForwardingOriginLine.TransportLocationFrom,
                                CountryTo = _locFreightForwardingOriginLine.CountryTo,
                                CityTo = _locFreightForwardingOriginLine.CityTo,
                                AreaTo = _locFreightForwardingOriginLine.AreaTo,
                                LocationTo = _locFreightForwardingOriginLine.LocationTo,
                                TransportLocationTo = _locFreightForwardingOriginLine.TransportLocationTo,
                            };
                            _saveDataOriginPackageMonitoring.Save();
                            _saveDataOriginPackageMonitoring.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FreightForwardingOrigin " + ex.ToString());
            }
        }

        private void SetRemainQtyInPostPackage(Session _currSession, FreightForwardingOrigin _locFreightForwardingOriginXPO)
        {
            try
            {
                if (_locFreightForwardingOriginXPO != null)
                {
                    XPCollection<FreightForwardingOriginLine> _locFreightForwardingOriginLines = new XPCollection<FreightForwardingOriginLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("FreightForwardingOrigin", _locFreightForwardingOriginXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Progress),
                                                            new BinaryOperator("Status", Status.Posted))));

                    if (_locFreightForwardingOriginLines != null && _locFreightForwardingOriginLines.Count > 0)
                    {
                        double _locRmDQty = 0;
                        double _locRmQty = 0;
                        double _locInvLineTotal = 0;
                        ItemUnitOfMeasure _locItemUOM = null;

                        foreach (FreightForwardingOriginLine _locFreightForwardingOriginLine in _locFreightForwardingOriginLines)
                        {
                            #region ProcessCount=0
                            if (_locFreightForwardingOriginLine.PostedCount == 0)
                            {
                                #region MaxQuantity
                                if (_locFreightForwardingOriginLine.MxDQty > 0)
                                {
                                    if (_locFreightForwardingOriginLine.DQty > 0 && _locFreightForwardingOriginLine.DQty <= _locFreightForwardingOriginLine.MxDQty)
                                    {
                                        _locRmDQty = _locFreightForwardingOriginLine.MxDQty - _locFreightForwardingOriginLine.DQty;
                                    }

                                    if (_locFreightForwardingOriginLine.Qty > 0 && _locFreightForwardingOriginLine.Qty <= _locFreightForwardingOriginLine.MxQty)
                                    {
                                        _locRmQty = _locFreightForwardingOriginLine.MxQty - _locFreightForwardingOriginLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locFreightForwardingOriginLine.Item),
                                                                new BinaryOperator("UOM", _locFreightForwardingOriginLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locFreightForwardingOriginLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty + _locRmDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }

                                }
                                #endregion MaxQuantity
                                #region NonMaxQuantity
                                else
                                {
                                    if (_locFreightForwardingOriginLine.DQty > 0)
                                    {
                                        _locRmDQty = 0;
                                    }

                                    if (_locFreightForwardingOriginLine.Qty > 0)
                                    {
                                        _locRmQty = 0;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locFreightForwardingOriginLine.Item),
                                                                new BinaryOperator("UOM", _locFreightForwardingOriginLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locFreightForwardingOriginLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty + _locRmDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion ProcessCount=0

                            #region ProcessCount>0
                            if (_locFreightForwardingOriginLine.PostedCount > 0)
                            {
                                if (_locFreightForwardingOriginLine.RmDQty > 0)
                                {
                                    _locRmDQty = _locFreightForwardingOriginLine.RmDQty - _locFreightForwardingOriginLine.DQty;
                                }

                                if (_locFreightForwardingOriginLine.RmQty > 0)
                                {
                                    _locRmQty = _locFreightForwardingOriginLine.RmQty - _locFreightForwardingOriginLine.Qty;
                                }

                                if (_locFreightForwardingOriginLine.MxDQty > 0 || _locFreightForwardingOriginLine.MxQty > 0)
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locFreightForwardingOriginLine.Item),
                                                                new BinaryOperator("UOM", _locFreightForwardingOriginLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locFreightForwardingOriginLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                }
                                else
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locFreightForwardingOriginLine.Item),
                                                                new BinaryOperator("UOM", _locFreightForwardingOriginLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locFreightForwardingOriginLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                }


                                if (_locItemUOM != null)
                                {
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locRmQty + _locRmDQty;
                                }

                            }
                            #endregion ProcessCount>0

                            _locFreightForwardingOriginLine.RmDQty = _locRmDQty;
                            _locFreightForwardingOriginLine.RmQty = _locRmQty;
                            _locFreightForwardingOriginLine.RmTQty = _locInvLineTotal;
                            _locFreightForwardingOriginLine.Save();
                            _locFreightForwardingOriginLine.Session.CommitTransaction();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FreightForwardingOrigin " + ex.ToString());
            }
        }

        private void SetPostingQtyInPostPackage(Session _currSession, FreightForwardingOrigin _locFreightForwardingOriginXPO)
        {
            try
            {
                if (_locFreightForwardingOriginXPO != null)
                {
                    XPCollection<FreightForwardingOriginLine> _locFreightForwardingOriginLines = new XPCollection<FreightForwardingOriginLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("FreightForwardingOrigin", _locFreightForwardingOriginXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Progress),
                                                            new BinaryOperator("Status", Status.Posted))));

                    if (_locFreightForwardingOriginLines != null && _locFreightForwardingOriginLines.Count > 0)
                    {
                        double _locPDQty = 0;
                        double _locPQty = 0;
                        double _locInvLineTotal = 0;
                        ItemUnitOfMeasure _locItemUOM = null;

                        foreach (FreightForwardingOriginLine _locFreightForwardingOriginLine in _locFreightForwardingOriginLines)
                        {
                            #region ProcessCount=0
                            if (_locFreightForwardingOriginLine.PostedCount == 0)
                            {
                                #region MaxQuantity
                                if (_locFreightForwardingOriginLine.MxDQty > 0)
                                {
                                    if (_locFreightForwardingOriginLine.DQty > 0 && _locFreightForwardingOriginLine.DQty <= _locFreightForwardingOriginLine.MxDQty)
                                    {
                                        _locPDQty = _locFreightForwardingOriginLine.DQty;
                                    }

                                    if (_locFreightForwardingOriginLine.Qty > 0 && _locFreightForwardingOriginLine.Qty <= _locFreightForwardingOriginLine.MxQty)
                                    {
                                        _locPQty = _locFreightForwardingOriginLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locFreightForwardingOriginLine.Item),
                                                                new BinaryOperator("UOM", _locFreightForwardingOriginLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locFreightForwardingOriginLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty + _locPDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }

                                }
                                #endregion MaxQuantity
                                #region NonMaxQuantity
                                else
                                {
                                    if (_locFreightForwardingOriginLine.DQty > 0)
                                    {
                                        _locPDQty = _locFreightForwardingOriginLine.DQty;
                                    }

                                    if (_locFreightForwardingOriginLine.Qty > 0)
                                    {
                                        _locPQty = _locFreightForwardingOriginLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locFreightForwardingOriginLine.Item),
                                                                new BinaryOperator("UOM", _locFreightForwardingOriginLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locFreightForwardingOriginLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty + _locPDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion ProcessCount=0

                            #region ProcessCount>0
                            if (_locFreightForwardingOriginLine.PostedCount > 0)
                            {
                                if (_locFreightForwardingOriginLine.PDQty > 0)
                                {
                                    _locPDQty = _locFreightForwardingOriginLine.PDQty + _locFreightForwardingOriginLine.DQty;
                                }

                                if (_locFreightForwardingOriginLine.PQty > 0)
                                {
                                    _locPQty = _locFreightForwardingOriginLine.PQty + _locFreightForwardingOriginLine.Qty;
                                }

                                if (_locFreightForwardingOriginLine.MxDQty > 0 || _locFreightForwardingOriginLine.MxQty > 0)
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                           (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Item", _locFreightForwardingOriginLine.Item),
                                                            new BinaryOperator("UOM", _locFreightForwardingOriginLine.MxUOM),
                                                            new BinaryOperator("DefaultUOM", _locFreightForwardingOriginLine.MxDUOM),
                                                            new BinaryOperator("Active", true)));
                                }
                                else
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                           (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Item", _locFreightForwardingOriginLine.Item),
                                                            new BinaryOperator("UOM", _locFreightForwardingOriginLine.UOM),
                                                            new BinaryOperator("DefaultUOM", _locFreightForwardingOriginLine.DUOM),
                                                            new BinaryOperator("Active", true)));
                                }

                                if (_locItemUOM != null)
                                {
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locPQty + _locPDQty;
                                }

                            }
                            #endregion ProcessCount>0

                            _locFreightForwardingOriginLine.PDQty = _locPDQty;
                            _locFreightForwardingOriginLine.PDUOM = _locFreightForwardingOriginLine.DUOM;
                            _locFreightForwardingOriginLine.PQty = _locPQty;
                            _locFreightForwardingOriginLine.PUOM = _locFreightForwardingOriginLine.UOM;
                            _locFreightForwardingOriginLine.PTQty = _locInvLineTotal;
                            _locFreightForwardingOriginLine.Save();
                            _locFreightForwardingOriginLine.Session.CommitTransaction();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FreightForwardingOrigin " + ex.ToString());
            }

        }

        private void SetProcessCountInPostPackage(Session _currSession, FreightForwardingOrigin _locFreightForwardingOriginXPO)
        {
            try
            {
                if (_locFreightForwardingOriginXPO != null)
                {
                    XPCollection<FreightForwardingOriginLine> _locFreightForwardingOriginLines = new XPCollection<FreightForwardingOriginLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("FreightForwardingOrigin", _locFreightForwardingOriginXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Progress),
                                                            new BinaryOperator("Status", Status.Posted))));

                    if (_locFreightForwardingOriginLines != null && _locFreightForwardingOriginLines.Count > 0)
                    {

                        foreach (FreightForwardingOriginLine _locFreightForwardingOriginLine in _locFreightForwardingOriginLines)
                        {
                            _locFreightForwardingOriginLine.PostedCount = _locFreightForwardingOriginLine.PostedCount + 1;
                            _locFreightForwardingOriginLine.Save();
                            _locFreightForwardingOriginLine.Session.CommitTransaction();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FreightForwardingOrigin " + ex.ToString());
            }
        }

        private void SetStatusFreightForwardinOriginLine(Session _currSession, FreightForwardingOrigin _locFreightForwardingOriginXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locFreightForwardingOriginXPO != null)
                {
                    XPCollection<FreightForwardingOriginLine> _locFreightForwardingOriginLines = new XPCollection<FreightForwardingOriginLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("FreightForwardingOrigin", _locFreightForwardingOriginXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Progress),
                                                            new BinaryOperator("Status", Status.Posted))));

                    if (_locFreightForwardingOriginLines != null && _locFreightForwardingOriginLines.Count > 0)
                    {

                        foreach (FreightForwardingOriginLine _locFreightForwardingOriginLine in _locFreightForwardingOriginLines)
                        {
                            if (_locFreightForwardingOriginLine.RmDQty == 0 && _locFreightForwardingOriginLine.RmQty == 0 && _locFreightForwardingOriginLine.RmTQty == 0)
                            {
                                _locFreightForwardingOriginLine.Status = Status.Close;
                                _locFreightForwardingOriginLine.ActivationPosting = true;
                                _locFreightForwardingOriginLine.StatusDate = now;
                            }
                            else
                            {
                                _locFreightForwardingOriginLine.Status = Status.Posted;
                                _locFreightForwardingOriginLine.StatusDate = now;
                            }
                            _locFreightForwardingOriginLine.Save();
                            _locFreightForwardingOriginLine.Session.CommitTransaction();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FreightForwardingOrigin " + ex.ToString());
            }
        }

        private void SetNormalQuantityInPostPackage(Session _currSession, FreightForwardingOrigin _locFreightForwardingOriginXPO)
        {
            try
            {
                if (_locFreightForwardingOriginXPO != null)
                {
                    XPCollection<FreightForwardingOriginLine> _locFreightForwardingOriginLines = new XPCollection<FreightForwardingOriginLine>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("FreightForwardingOrigin", _locFreightForwardingOriginXPO),
                                                                                     new BinaryOperator("Select", true)));

                    if (_locFreightForwardingOriginLines != null && _locFreightForwardingOriginLines.Count > 0)
                    {
                        foreach (FreightForwardingOriginLine _locFreightForwardingOriginLine in _locFreightForwardingOriginLines)
                        {
                            if (_locFreightForwardingOriginLine.Status == Status.Progress || _locFreightForwardingOriginLine.Status == Status.Posted || _locFreightForwardingOriginLine.Status == Status.Close)
                            {
                                if (_locFreightForwardingOriginLine.DQty > 0 || _locFreightForwardingOriginLine.Qty > 0)
                                {
                                    _locFreightForwardingOriginLine.Select = false;
                                    _locFreightForwardingOriginLine.DQty = 0;
                                    _locFreightForwardingOriginLine.Qty = 0;
                                    _locFreightForwardingOriginLine.Save();
                                    _locFreightForwardingOriginLine.Session.CommitTransaction();
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FreightForwardingOrigin " + ex.ToString());
            }
        }

        private void SetFinalStatusFreightForwardinOrigin(Session _currSession, FreightForwardingOrigin _locFreightForwardingOriginXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locStatusCount = 0;
                int _locFreightForwardingineCount = 0;

                if (_locFreightForwardingOriginXPO != null)
                {

                    XPCollection<FreightForwardingOriginLine> _locFreightForwardingOriginLines = new XPCollection<FreightForwardingOriginLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("FreightForwardingOrigin", _locFreightForwardingOriginXPO)));

                    if (_locFreightForwardingOriginLines != null && _locFreightForwardingOriginLines.Count() > 0)
                    {
                        _locFreightForwardingineCount = _locFreightForwardingOriginLines.Count();

                        foreach (FreightForwardingOriginLine _locFreightForwardingOriginLine in _locFreightForwardingOriginLines)
                        {
                            if (_locFreightForwardingOriginLine.Status == Status.Close)
                            {
                                _locStatusCount = _locStatusCount + 1;
                            }
                        }
                    }

                    if (_locStatusCount == _locFreightForwardingineCount)
                    {
                        _locFreightForwardingOriginXPO.ActivationPosting = true;
                        _locFreightForwardingOriginXPO.Status = Status.Close;
                        _locFreightForwardingOriginXPO.StatusDate = now;
                        _locFreightForwardingOriginXPO.Save();
                        _locFreightForwardingOriginXPO.Session.CommitTransaction();
                    }
                    else
                    {
                        _locFreightForwardingOriginXPO.Status = Status.Posted;
                        _locFreightForwardingOriginXPO.StatusDate = now;
                        _locFreightForwardingOriginXPO.Save();
                        _locFreightForwardingOriginXPO.Session.CommitTransaction();
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FreightForwardingOrigin " + ex.ToString());
            }
        }

        #endregion PostingPackage

        #region Approval Process

        private void SetApprovalLine(Session _currentSession, FreightForwardingOrigin _locFreightForwardingOriginXPO, ApprovalLevel _locApprovalLevel)
        {
            try
            {
                DateTime now = DateTime.Now;

                ShipmentApprovalLine _locShipmentApprovalLineXPO = _currentSession.FindObject<ShipmentApprovalLine>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("FreightForwardingOrigin", _locFreightForwardingOriginXPO),
                                                                         new BinaryOperator("ApprovalLevel", _locApprovalLevel)));

                if (_locShipmentApprovalLineXPO == null)
                {
                    ShipmentApprovalLine _saveDataAL2 = new ShipmentApprovalLine(_currentSession)
                    {
                        ApprovalDate = now,
                        ApprovalStatus = Status.Approved,
                        ApprovalLevel = _locApprovalLevel,
                        FreightForwardingOrigin = _locFreightForwardingOriginXPO,
                    };
                    _saveDataAL2.Save();
                    _saveDataAL2.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FreightForwardingOrigin " + ex.ToString());
            }
        }

        #endregion Approval Process

        #region Email

        private string BackgroundBody(Session _currentSession, FreightForwardingOrigin _locFreightForwardingOriginXPO, MailSetupDetail _locMailSetDetail)
        {
            string body = string.Empty;

            if (_locFreightForwardingOriginXPO != null)
            {

                ShipmentApprovalLine _locShipmentApprovalLineXPO = _currentSession.FindObject<ShipmentApprovalLine>
                                                   (new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("FreightForwardingOrigin", _locFreightForwardingOriginXPO)));

                string Status = _locShipmentApprovalLineXPO.ApprovalStatus.ToString();

                using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Email/EmailTemplate.html")))
                {
                    body = reader.ReadToEnd();
                }
                body = body.Replace("{Code}", _locFreightForwardingOriginXPO.Code);

                #region Level
                if (_locFreightForwardingOriginXPO.ActiveApproved1 == true)
                {
                    string Active = "Level 1";
                    body = body.Replace("{Level}", Active);
                }
                else if (_locFreightForwardingOriginXPO.ActiveApproved2 == true)
                {
                    string Active = "Level 2";
                    body = body.Replace("{Level}", Active);
                }
                else if (_locFreightForwardingOriginXPO.ActiveApproved3 == true)
                {
                    string Active = "Level 3";
                    body = body.Replace("{Level}", Active);
                }
                else
                {
                    string Active = "Not Available";
                    body = body.Replace("{Level}", Active);
                }
                #endregion Level

                body = body.Replace("{Status}", Status);
                body = body.Replace("{Message}", _locMailSetDetail.MessageBody);
            }

            return body;
        }

        protected void SendEmail(Session _currentSession, FreightForwardingOrigin _locFreightForwardinOriginXPO, Status _locStatus, ShipmentApprovalLine _locShipmentApprovalLineXPO2)
        {
            DateTime now = DateTime.Now;
            var User = SecuritySystem.CurrentUserName;
            GlobalFunction _globFunc = new GlobalFunction();
            UserAccess _numLineUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", User));
            string _locMessageBody = null;

            try
            {
                if (_locFreightForwardinOriginXPO != null)
                {
                    XPCollection<MailSetupDetail> _locMailSetDetails = new XPCollection<MailSetupDetail>
                                                                       (_currentSession, new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("ObjectList", ObjectList.FreightForwardingOrigin),
                                                                        new BinaryOperator("UserAccess", _numLineUserAccess),
                                                                        new BinaryOperator("Active", true)));

                    if (_locMailSetDetails != null && _locMailSetDetails.Count > 0)
                    {
                        foreach (MailSetupDetail _locMailSetDetail in _locMailSetDetails)
                        {
                            if (_locMailSetDetail.MessageBody != null)
                            {
                                if (_locShipmentApprovalLineXPO2.EndApproval == true)
                                {
                                    _locMessageBody = this.BackgroundBody(_currentSession, _locFreightForwardinOriginXPO, _locMailSetDetail);
                                }
                                else
                                {
                                    _locMessageBody = this.BackgroundBody(_currentSession, _locFreightForwardinOriginXPO, _locMailSetDetail);
                                }
                            }

                            _globFunc.SetAndSendMail(_locMailSetDetail.SmtpHost, _locMailSetDetail.SmtpPort,
                            _locMailSetDetail.MailFrom, _locMailSetDetail.MailFromPassword, _locMailSetDetail.MailTo, _locMailSetDetail.MailSubject, _locMessageBody);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FreightForwardingOrigin " + ex.ToString());
            }
        }

        #endregion Email

        #region Global

        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0} !", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }


        #endregion Global

        
    }
}
