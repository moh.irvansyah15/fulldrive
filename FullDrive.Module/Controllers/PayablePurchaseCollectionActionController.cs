﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using System.Collections;
using DevExpress.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class PayablePurchaseCollectionActionController : ViewController
    {
        public PayablePurchaseCollectionActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }

        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }

        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }

        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void PayablePurchaseCollectionShowPIAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
            var ListView = View as ListView;
            IObjectSpace os = Application.CreateObjectSpace();
            string listViewId = Application.FindListViewId(typeof(PurchaseInvoiceMonitoring));
            CollectionSourceBase cs = Application.CreateCollectionSource(os, typeof(PurchaseInvoiceMonitoring), listViewId);
            //Tinggal buat 
            ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
            string _stringPIM = null;
            if (_objectsToProcess != null)
            {
                foreach (Object obj in _objectsToProcess)
                {
                    PayablePurchaseCollection _locPayablePurchaseCollection = (PayablePurchaseCollection)_objectSpace.GetObject(obj);
                    if (_locPayablePurchaseCollection != null)
                    {
                        if (_locPayablePurchaseCollection.PurchaseInvoice != null)
                        {
                            if (_stringPIM == null)
                            {
                                if (_locPayablePurchaseCollection.PurchaseInvoice.Code != null)
                                {
                                    _stringPIM = "( [PurchaseInvoice.Code]=='" + _locPayablePurchaseCollection.PurchaseInvoice.Code + "' AND [Status] != 4 )";
                                }
                            }
                            else
                            {
                                if (_locPayablePurchaseCollection.PurchaseInvoice.Code != null)
                                {
                                    _stringPIM = _stringPIM + " OR ( [PurchaseInvoice.Code]=='" + _locPayablePurchaseCollection.PurchaseInvoice.Code + "' AND [Status] != 4 )";
                                }
                            }
                        }
                    }
                }
            }
            if (_stringPIM != null)
            {
                cs.Criteria["PayablePurchaseCollectionFilterPIM"] = CriteriaOperator.Parse(_stringPIM);
            }
            e.ShowViewParameters.CreatedView = Application.CreateListView(
                listViewId,
                cs,
                true
            );
            e.ShowViewParameters.Context = TemplateContext.View;
            e.ShowViewParameters.TargetWindow = TargetWindow.NewModalWindow;
            DialogController dc = Application.CreateController<DialogController>();
            dc.Accepting += new EventHandler<DialogControllerAcceptingEventArgs>(dc_Accepting);
            e.ShowViewParameters.Controllers.Add(dc);
        }

        void dc_Accepting(object sender, DialogControllerAcceptingEventArgs e)
        {
            try
            {
                PayableTransaction _locPayableTransaction = null;
                View popupView = ((Controller)sender).Frame.View;
                ArrayList _objectsToProcess = new ArrayList(View.SelectedObjects);
                if (_objectsToProcess != null)
                {
                    foreach (Object _obj in _objectsToProcess)
                    {
                        PayablePurchaseCollection _locPayablePurchaseCollection = (PayablePurchaseCollection)popupView.ObjectSpace.GetObject(_obj);
                        if (_locPayablePurchaseCollection != null)
                        {
                            if (_locPayablePurchaseCollection.PayableTransaction != null)
                            {
                                _locPayableTransaction = _locPayablePurchaseCollection.PayableTransaction;
                            }
                        }
                    }
                }
                if (_locPayableTransaction != null)
                {
                    ArrayList _objectsToProcess2 = new ArrayList(popupView.SelectedObjects);
                    if (_objectsToProcess2 != null)
                    {
                        foreach (Object _obj2 in _objectsToProcess2)
                        {
                            PurchaseInvoiceMonitoring _locPurchaseInvMonitoring = (PurchaseInvoiceMonitoring)popupView.ObjectSpace.GetObject(_obj2);
                            if (_locPurchaseInvMonitoring != null && _locPurchaseInvMonitoring.Select == true && _locPurchaseInvMonitoring.Status != Status.Close)
                            {
                                _locPurchaseInvMonitoring.PayableTransaction = _locPayableTransaction;
                            }
                        }
                    }
                }
                popupView.ObjectSpace.CommitChanges();
            }
            catch (ValidationException)
            {
            }
        }

        private void PayablePurchaseCollectionShowPPIAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
            var ListView = View as ListView;
            IObjectSpace os = Application.CreateObjectSpace();
            string listViewId = Application.FindListViewId(typeof(PurchasePrePaymentInvoice));
            CollectionSourceBase cs = Application.CreateCollectionSource(os, typeof(PurchasePrePaymentInvoice), listViewId);
            //Tinggal buat 
            ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
            string _stringPPI = null;
            if (_objectsToProcess != null)
            {
                foreach (Object obj in _objectsToProcess)
                {
                    PayablePurchaseCollection _locPayablePurchaseCollection = (PayablePurchaseCollection)_objectSpace.GetObject(obj);
                    if (_locPayablePurchaseCollection != null)
                    {
                        if (_locPayablePurchaseCollection.PurchasePrePaymentInvoice != null)
                        {
                            if (_stringPPI == null)
                            {
                                if (_locPayablePurchaseCollection.PurchasePrePaymentInvoice.Code != null)
                                {
                                    _stringPPI = "( [Code]=='" + _locPayablePurchaseCollection.PurchasePrePaymentInvoice.Code + "' AND [Status] != 4 )";
                                }
                            }
                            else
                            {
                                if (_locPayablePurchaseCollection.PurchasePrePaymentInvoice.Code != null)
                                {
                                    _stringPPI = _stringPPI + " OR ( [Code]=='" + _locPayablePurchaseCollection.PurchasePrePaymentInvoice.Code + "' AND [Status] != 4 )";
                                }
                            }
                        }
                    }
                }
            }
            if (_stringPPI != null)
            {
                cs.Criteria["PayablePurchaseCollectionFilterPPI"] = CriteriaOperator.Parse(_stringPPI);
            }
            e.ShowViewParameters.CreatedView = Application.CreateListView(
                listViewId,
                cs,
                true
            );
            e.ShowViewParameters.Context = TemplateContext.View;
            e.ShowViewParameters.TargetWindow = TargetWindow.NewModalWindow;
            DialogController dc = Application.CreateController<DialogController>();
            dc.Accepting += new EventHandler<DialogControllerAcceptingEventArgs>(dc_Accepting2);
            e.ShowViewParameters.Controllers.Add(dc);
        }

        void dc_Accepting2(object sender, DialogControllerAcceptingEventArgs e)
        {
            try
            {
                PayableTransaction _locPayableTransaction = null;
                View popupView = ((Controller)sender).Frame.View;
                ArrayList _objectsToProcess = new ArrayList(View.SelectedObjects);
                if (_objectsToProcess != null)
                {
                    foreach (Object _obj in _objectsToProcess)
                    {
                        PayablePurchaseCollection _locPayablePurchaseCollection = (PayablePurchaseCollection)popupView.ObjectSpace.GetObject(_obj);
                        if (_locPayablePurchaseCollection != null)
                        {
                            if (_locPayablePurchaseCollection.PayableTransaction != null)
                            {
                                _locPayableTransaction = _locPayablePurchaseCollection.PayableTransaction;
                            }
                        }
                    }
                }
                if (_locPayableTransaction != null)
                {
                    ArrayList _objectsToProcess2 = new ArrayList(popupView.SelectedObjects);
                    if (_objectsToProcess2 != null)
                    {
                        foreach (Object _obj2 in _objectsToProcess2)
                        {
                            PurchasePrePaymentInvoice _locPurchasePrePayment = (PurchasePrePaymentInvoice)popupView.ObjectSpace.GetObject(_obj2);
                            if (_locPurchasePrePayment != null && _locPurchasePrePayment.Select == true && _locPurchasePrePayment.Status != Status.Close)
                            {
                                _locPurchasePrePayment.PayableTransaction = _locPayableTransaction;
                            }
                        }
                    }
                }
                popupView.ObjectSpace.CommitChanges();
            }
            catch (ValidationException)
            {
            }
        }

    }
}
