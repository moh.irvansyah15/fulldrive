﻿namespace FullDrive.Module.Controllers
{
    partial class ShipmentBookingDestinationCollectionActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ShipmentBookingDestinationCollectionShowSBMAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // ShipmentBookingDestinationCollectionShowSBMAction
            // 
            this.ShipmentBookingDestinationCollectionShowSBMAction.Caption = "Show SBM";
            this.ShipmentBookingDestinationCollectionShowSBMAction.ConfirmationMessage = null;
            this.ShipmentBookingDestinationCollectionShowSBMAction.Id = "ShipmentBookingDestinationCollectionShowSBMActionId";
            this.ShipmentBookingDestinationCollectionShowSBMAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.ShipmentBookingDestinationCollection);
            this.ShipmentBookingDestinationCollectionShowSBMAction.ToolTip = null;
            this.ShipmentBookingDestinationCollectionShowSBMAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.ShipmentBookingDestinationCollectionShowSBMAction_Execute);
            // 
            // ShipmentBookingDestinationCollectionActionController
            // 
            this.Actions.Add(this.ShipmentBookingDestinationCollectionShowSBMAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction ShipmentBookingDestinationCollectionShowSBMAction;
    }
}
