﻿namespace FullDrive.Module.Controllers
{
    partial class ReturnSalesCollectionActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ReturnSalesCollectionShowITOMAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // ReturnSalesCollectionShowITOMAction
            // 
            this.ReturnSalesCollectionShowITOMAction.Caption = "Show ITOM";
            this.ReturnSalesCollectionShowITOMAction.ConfirmationMessage = null;
            this.ReturnSalesCollectionShowITOMAction.Id = "ReturnSalesCollectionShowITOMActionId";
            this.ReturnSalesCollectionShowITOMAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.ReturnSalesCollection);
            this.ReturnSalesCollectionShowITOMAction.ToolTip = null;
            this.ReturnSalesCollectionShowITOMAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.ReturnSalesCollectionShowITOMAction_Execute);
            // 
            // ReturnSalesCollectionActionController
            // 
            this.Actions.Add(this.ReturnSalesCollectionShowITOMAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction ReturnSalesCollectionShowITOMAction;
    }
}
