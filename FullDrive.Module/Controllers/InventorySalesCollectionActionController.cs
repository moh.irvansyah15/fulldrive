﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using System.Collections;
using DevExpress.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class InventorySalesCollectionActionController : ViewController
    {
        public InventorySalesCollectionActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }

        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void InventorySalesCollectionShowSOMAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
            var ListView = View as ListView;
            IObjectSpace os = Application.CreateObjectSpace();
            string listViewId = Application.FindListViewId(typeof(SalesOrderMonitoring));
            CollectionSourceBase cs = Application.CreateCollectionSource(os, typeof(SalesOrderMonitoring), listViewId);
            //Tinggal buat 
            ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
            string _stringSO = null;
            if (_objectsToProcess != null)
            {
                foreach (Object obj in _objectsToProcess)
                {
                    InventorySalesCollection _locInventorySalesCollection = (InventorySalesCollection)_objectSpace.GetObject(obj);
                    if (_locInventorySalesCollection != null)
                    {
                        if (_locInventorySalesCollection.SalesOrder != null)
                        {
                            if (_stringSO == null)
                            {
                                if (_locInventorySalesCollection.SalesOrder.Code != null)
                                {
                                    _stringSO = "( [SalesOrder.Code]=='" + _locInventorySalesCollection.SalesOrder.Code + "' AND [PostedCount] == 0 )";
                                }

                            }
                            else
                            {
                                if (_locInventorySalesCollection.SalesOrder.Code != null)
                                {
                                    _stringSO = _stringSO + " OR ( [SalesOrder.Code]=='" + _locInventorySalesCollection.SalesOrder.Code + "' AND [PostedCount] == 0 )";
                                }
                            }
                        }
                    }
                }
            }
            if (_stringSO != null)
            {
                cs.Criteria["InventorySalesCollectionFilter"] = CriteriaOperator.Parse(_stringSO);
            }
            e.ShowViewParameters.CreatedView = Application.CreateListView(
                listViewId,
                cs,
                true
            );
            e.ShowViewParameters.Context = TemplateContext.View;
            e.ShowViewParameters.TargetWindow = TargetWindow.NewModalWindow;
            DialogController dc = Application.CreateController<DialogController>();
            dc.Accepting += new EventHandler<DialogControllerAcceptingEventArgs>(dc_Accepting);
            e.ShowViewParameters.Controllers.Add(dc);
        }

        void dc_Accepting(object sender, DialogControllerAcceptingEventArgs e)
        {
            try
            {
                InventoryTransferOut _locInventoryTransferOut = null;
                View popupView = ((Controller)sender).Frame.View;
                ArrayList _objectsToProcess = new ArrayList(View.SelectedObjects);
                if (_objectsToProcess != null)
                {
                    foreach (Object _obj in _objectsToProcess)
                    {
                        InventorySalesCollection _locInvSalesCollection = (InventorySalesCollection)popupView.ObjectSpace.GetObject(_obj);
                        if (_locInvSalesCollection != null)
                        {
                            if (_locInvSalesCollection.InventoryTransferOut != null)
                            {
                                _locInventoryTransferOut = _locInvSalesCollection.InventoryTransferOut;
                            }
                        }
                    }
                }
                if (_locInventoryTransferOut != null)
                {
                    ArrayList _objectsToProcess2 = new ArrayList(popupView.SelectedObjects);
                    if (_objectsToProcess2 != null)
                    {
                        foreach (Object _obj2 in _objectsToProcess2)
                        {
                            SalesOrderMonitoring _locSalOrderMonitoring = (SalesOrderMonitoring)popupView.ObjectSpace.GetObject(_obj2);
                            if (_locSalOrderMonitoring != null && _locSalOrderMonitoring.Select == true && _locSalOrderMonitoring.PostedCount == 0)
                            {
                                _locSalOrderMonitoring.InventoryTransferOut = _locInventoryTransferOut;
                            }
                        }
                    }
                }
                popupView.ObjectSpace.CommitChanges();
            }
            catch (ValidationException)
            {
            }
        }

        private void InventorySalesCollectionShowSRMAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
            var ListView = View as ListView;
            IObjectSpace os = Application.CreateObjectSpace();
            string listViewId = Application.FindListViewId(typeof(SalesReturnMonitoring));
            CollectionSourceBase cs = Application.CreateCollectionSource(os, typeof(SalesReturnMonitoring), listViewId);
            //Tinggal buat 
            ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
            string _stringSR = null;
            if (_objectsToProcess != null)
            {
                foreach (Object obj in _objectsToProcess)
                {
                    InventorySalesCollection _locInventorySalesCollection = (InventorySalesCollection)_objectSpace.GetObject(obj);
                    if (_locInventorySalesCollection != null)
                    {
                        if (_locInventorySalesCollection.SalesReturn != null)
                        {
                            if (_stringSR == null)
                            {
                                if (_locInventorySalesCollection.SalesReturn.Code != null)
                                {
                                    _stringSR = "( [SalesReturn.Code]=='" + _locInventorySalesCollection.SalesReturn.Code + "' )";
                                }

                            }
                            else
                            {
                                if (_locInventorySalesCollection.SalesReturn.Code != null)
                                {
                                    _stringSR = _stringSR + " OR ( [SalesReturn.Code]=='" + _locInventorySalesCollection.SalesReturn.Code + "' )";
                                }
                            }
                        }
                    }
                }
            }
            if (_stringSR != null)
            {
                cs.Criteria["InventorySalesCollectionFilter"] = CriteriaOperator.Parse(_stringSR);
            }
            e.ShowViewParameters.CreatedView = Application.CreateListView(
                listViewId,
                cs,
                true
            );
            e.ShowViewParameters.Context = TemplateContext.View;
            e.ShowViewParameters.TargetWindow = TargetWindow.NewModalWindow;
            DialogController dc = Application.CreateController<DialogController>();
            dc.Accepting += new EventHandler<DialogControllerAcceptingEventArgs>(dc_Accepting2);
            e.ShowViewParameters.Controllers.Add(dc);
        }

        void dc_Accepting2(object sender, DialogControllerAcceptingEventArgs e)
        {
            try
            {
                InventoryTransferIn _locInventoryTransferIn = null;
                View popupView = ((Controller)sender).Frame.View;
                ArrayList _objectsToProcess = new ArrayList(View.SelectedObjects);
                if (_objectsToProcess != null)
                {
                    foreach (Object _obj in _objectsToProcess)
                    {
                        InventorySalesCollection _locInvSalesCollection = (InventorySalesCollection)popupView.ObjectSpace.GetObject(_obj);
                        if (_locInvSalesCollection != null)
                        {
                            if (_locInvSalesCollection.InventoryTransferIn != null)
                            {
                                _locInventoryTransferIn = _locInvSalesCollection.InventoryTransferIn;
                            }
                        }
                    }
                }
                if (_locInventoryTransferIn != null)
                {
                    ArrayList _objectsToProcess2 = new ArrayList(popupView.SelectedObjects);
                    if (_objectsToProcess2 != null)
                    {
                        foreach (Object _obj2 in _objectsToProcess2)
                        {
                            SalesReturnMonitoring _locSalReturnMonitoring = (SalesReturnMonitoring)popupView.ObjectSpace.GetObject(_obj2);
                            if (_locSalReturnMonitoring != null && _locSalReturnMonitoring.Select == true && _locSalReturnMonitoring.InventoryStatus != Status.Close)
                            {
                                _locSalReturnMonitoring.InventoryTransferIn = _locInventoryTransferIn;
                            }
                        }
                    }
                }
                popupView.ObjectSpace.CommitChanges();
            }
            catch (ValidationException)
            {
            }
        }
    }
}
