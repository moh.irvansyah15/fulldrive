﻿namespace FullDrive.Module.Controllers
{
    partial class InventoryTransferOutActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.InventoryTransferOutProgressAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.InventoryTransferOutPostingAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.InventoryTransferOutListviewFilterSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.InventoryTransferOutGenerateLotAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.InventoryTransferOutGetSOAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.InventoryTransferOutGetITOAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.InventoryTransferOutGetPRAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // InventoryTransferOutProgressAction
            // 
            this.InventoryTransferOutProgressAction.Caption = "Progress";
            this.InventoryTransferOutProgressAction.ConfirmationMessage = null;
            this.InventoryTransferOutProgressAction.Id = "InventoryTransferOutProgressActionId";
            this.InventoryTransferOutProgressAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.InventoryTransferOut);
            this.InventoryTransferOutProgressAction.ToolTip = null;
            this.InventoryTransferOutProgressAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.InventoryTransferOutProgressAction_Execute);
            // 
            // InventoryTransferOutPostingAction
            // 
            this.InventoryTransferOutPostingAction.Caption = "Posting";
            this.InventoryTransferOutPostingAction.ConfirmationMessage = null;
            this.InventoryTransferOutPostingAction.Id = "InventoryTransferOutPostingActionId";
            this.InventoryTransferOutPostingAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.InventoryTransferOut);
            this.InventoryTransferOutPostingAction.ToolTip = null;
            this.InventoryTransferOutPostingAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.InventoryTransferOutPostingAction_Execute);
            // 
            // InventoryTransferOutListviewFilterSelectionAction
            // 
            this.InventoryTransferOutListviewFilterSelectionAction.Caption = "Filter";
            this.InventoryTransferOutListviewFilterSelectionAction.ConfirmationMessage = null;
            this.InventoryTransferOutListviewFilterSelectionAction.Id = "InventoryTransferOutListviewFilterSelectionActionId";
            this.InventoryTransferOutListviewFilterSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.InventoryTransferOutListviewFilterSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.InventoryTransferOut);
            this.InventoryTransferOutListviewFilterSelectionAction.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.InventoryTransferOutListviewFilterSelectionAction.ToolTip = null;
            this.InventoryTransferOutListviewFilterSelectionAction.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.InventoryTransferOutListviewFilterSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.InventoryTransferOutListviewFilterSelectionAction_Execute);
            // 
            // InventoryTransferOutGenerateLotAction
            // 
            this.InventoryTransferOutGenerateLotAction.Caption = "Generate Lot";
            this.InventoryTransferOutGenerateLotAction.ConfirmationMessage = null;
            this.InventoryTransferOutGenerateLotAction.Id = "InventoryTransferOutGenerateLotActionId";
            this.InventoryTransferOutGenerateLotAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.InventoryTransferOut);
            this.InventoryTransferOutGenerateLotAction.ToolTip = null;
            this.InventoryTransferOutGenerateLotAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.InventoryTransferOutGenerateLotAction_Execute);
            // 
            // InventoryTransferOutGetSOAction
            // 
            this.InventoryTransferOutGetSOAction.Caption = "Get SO";
            this.InventoryTransferOutGetSOAction.ConfirmationMessage = null;
            this.InventoryTransferOutGetSOAction.Id = "InventoryTransferOutGetSOActionId";
            this.InventoryTransferOutGetSOAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.InventoryTransferOut);
            this.InventoryTransferOutGetSOAction.ToolTip = null;
            this.InventoryTransferOutGetSOAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.InventoryTransferOutGetSOAction_Execute);
            // 
            // InventoryTransferOutGetITOAction
            // 
            this.InventoryTransferOutGetITOAction.Caption = "Get ITO";
            this.InventoryTransferOutGetITOAction.ConfirmationMessage = null;
            this.InventoryTransferOutGetITOAction.Id = "InventoryTransferOutGetITOActionId";
            this.InventoryTransferOutGetITOAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.InventoryTransferOut);
            this.InventoryTransferOutGetITOAction.ToolTip = null;
            this.InventoryTransferOutGetITOAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.InventoryTransferOutGetITOAction_Execute);
            // 
            // InventoryTransferOutGetPRAction
            // 
            this.InventoryTransferOutGetPRAction.Caption = "Get PR";
            this.InventoryTransferOutGetPRAction.ConfirmationMessage = null;
            this.InventoryTransferOutGetPRAction.Id = "InventoryTransferOutGetPRActionId";
            this.InventoryTransferOutGetPRAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.InventoryTransferOut);
            this.InventoryTransferOutGetPRAction.ToolTip = null;
            this.InventoryTransferOutGetPRAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.InventoryTransferOutGetPRAction_Execute);
            // 
            // InventoryTransferOutActionController
            // 
            this.Actions.Add(this.InventoryTransferOutProgressAction);
            this.Actions.Add(this.InventoryTransferOutPostingAction);
            this.Actions.Add(this.InventoryTransferOutListviewFilterSelectionAction);
            this.Actions.Add(this.InventoryTransferOutGenerateLotAction);
            this.Actions.Add(this.InventoryTransferOutGetSOAction);
            this.Actions.Add(this.InventoryTransferOutGetITOAction);
            this.Actions.Add(this.InventoryTransferOutGetPRAction);
            this.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.InventoryTransferOut);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction InventoryTransferOutProgressAction;
        private DevExpress.ExpressApp.Actions.SimpleAction InventoryTransferOutPostingAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction InventoryTransferOutListviewFilterSelectionAction;
        private DevExpress.ExpressApp.Actions.SimpleAction InventoryTransferOutGenerateLotAction;
        private DevExpress.ExpressApp.Actions.SimpleAction InventoryTransferOutGetSOAction;
        private DevExpress.ExpressApp.Actions.SimpleAction InventoryTransferOutGetITOAction;
        private DevExpress.ExpressApp.Actions.SimpleAction InventoryTransferOutGetPRAction;
    }
}
