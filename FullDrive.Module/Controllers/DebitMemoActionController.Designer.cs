﻿namespace FullDrive.Module.Controllers
{
    partial class DebitMemoActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.DebitMemoProgressAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.DebitMemoPostingAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.DebitMemoFilterAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.DebitMemoApprovalAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.DebitMemoGetSRMAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // DebitMemoProgressAction
            // 
            this.DebitMemoProgressAction.Caption = "Progress";
            this.DebitMemoProgressAction.ConfirmationMessage = null;
            this.DebitMemoProgressAction.Id = "DebitMemoProgressActionId";
            this.DebitMemoProgressAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.DebitMemo);
            this.DebitMemoProgressAction.ToolTip = null;
            this.DebitMemoProgressAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.DebitMemoProgressAction_Execute);
            // 
            // DebitMemoPostingAction
            // 
            this.DebitMemoPostingAction.Caption = "Posting";
            this.DebitMemoPostingAction.ConfirmationMessage = null;
            this.DebitMemoPostingAction.Id = "DebitMemoPostingActionId";
            this.DebitMemoPostingAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.DebitMemo);
            this.DebitMemoPostingAction.ToolTip = null;
            this.DebitMemoPostingAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.DebitMemoPostingAction_Execute);
            // 
            // DebitMemoFilterAction
            // 
            this.DebitMemoFilterAction.Caption = "Filter";
            this.DebitMemoFilterAction.ConfirmationMessage = null;
            this.DebitMemoFilterAction.Id = "DebitMemoFilterActionId";
            this.DebitMemoFilterAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.DebitMemoFilterAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.DebitMemo);
            this.DebitMemoFilterAction.ToolTip = null;
            this.DebitMemoFilterAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.DebitMemoFilterAction_Execute);
            // 
            // DebitMemoApprovalAction
            // 
            this.DebitMemoApprovalAction.Caption = "Approval";
            this.DebitMemoApprovalAction.ConfirmationMessage = null;
            this.DebitMemoApprovalAction.Id = "DebitMemoApprovalActionId";
            this.DebitMemoApprovalAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.DebitMemoApprovalAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.DebitMemo);
            this.DebitMemoApprovalAction.ToolTip = null;
            this.DebitMemoApprovalAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.DebitMemoApprovalAction_Execute);
            // 
            // DebitMemoGetSRMAction
            // 
            this.DebitMemoGetSRMAction.Caption = "Get SRM";
            this.DebitMemoGetSRMAction.ConfirmationMessage = null;
            this.DebitMemoGetSRMAction.Id = "DebitMemoGetSRMActionId";
            this.DebitMemoGetSRMAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.DebitMemo);
            this.DebitMemoGetSRMAction.ToolTip = null;
            this.DebitMemoGetSRMAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.DebitMemoGetSRMAction_Execute);
            // 
            // DebitMemoActionController
            // 
            this.Actions.Add(this.DebitMemoProgressAction);
            this.Actions.Add(this.DebitMemoPostingAction);
            this.Actions.Add(this.DebitMemoFilterAction);
            this.Actions.Add(this.DebitMemoApprovalAction);
            this.Actions.Add(this.DebitMemoGetSRMAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction DebitMemoProgressAction;
        private DevExpress.ExpressApp.Actions.SimpleAction DebitMemoPostingAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction DebitMemoFilterAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction DebitMemoApprovalAction;
        private DevExpress.ExpressApp.Actions.SimpleAction DebitMemoGetSRMAction;
    }
}
