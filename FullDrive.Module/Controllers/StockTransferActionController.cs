﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using System.Collections;
using DevExpress.Xpo;
using DevExpress.ExpressApp.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class StockTransferActionController : ViewController
    {
        private ChoiceActionItem _selectionListviewFilter;

        public StockTransferActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            StockTransferListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {

                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                StockTransferListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
        }

        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }

        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }

        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void StockTransferProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;


                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        StockTransfer _locPurchaseOrderOS = (StockTransfer)_objectSpace.GetObject(obj);

                        if (_locPurchaseOrderOS != null)
                        {
                            if (_locPurchaseOrderOS.Code != null)
                            {
                                _currObjectId = _locPurchaseOrderOS.Code;

                                StockTransfer _locStockTransferXPO = _currSession.FindObject<StockTransfer>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locStockTransferXPO != null)
                                {
                                    if (_locStockTransferXPO.Status == Status.Open || _locStockTransferXPO.Status == Status.Progress)
                                    {
                                        if (_locStockTransferXPO.Status == Status.Open)
                                        {
                                            _locStockTransferXPO.Status = Status.Progress;
                                            _locStockTransferXPO.StatusDate = now;
                                            _locStockTransferXPO.Save();
                                            _locStockTransferXPO.Session.CommitTransaction();
                                        }

                                        XPCollection<StockTransferLine> _locStockTransferLines = new XPCollection<StockTransferLine>
                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("StockTransfer", _locStockTransferXPO),
                                                                new BinaryOperator("Status", Status.Open)));

                                        if (_locStockTransferLines != null && _locStockTransferLines.Count > 0)
                                        {
                                            foreach (StockTransferLine _locStockTransferLine in _locStockTransferLines)
                                            {
                                                _locStockTransferLine.StatusFrom = Status.Progress;
                                                _locStockTransferLine.StatusDateFrom = now;
                                                _locStockTransferLine.Save();
                                                _locStockTransferLine.Session.CommitTransaction();
                                            }
                                        }
                                    }
                                    SuccessMessageShow("Stock Transfer has been successfully updated to progress");
                                }
                                else
                                {
                                    ErrorMessageShow("Data Stock Transfer Not Available");
                                }

                            }
                            else
                            {
                                ErrorMessageShow("Data Stock Transfer Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = StockTransfer " + ex.ToString());
            }
        }

        private void StockTransferConfirmAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        StockTransfer _locPurchaseOrderOS = (StockTransfer)_objectSpace.GetObject(obj);

                        if (_locPurchaseOrderOS != null)
                        {
                            if (_locPurchaseOrderOS.Code != null)
                            {
                                _currObjectId = _locPurchaseOrderOS.Code;

                                StockTransfer _locStockTransferXPO = _currSession.FindObject<StockTransfer>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locStockTransferXPO != null)
                                {
                                    if (_locStockTransferXPO.Status == Status.Progress || _locStockTransferXPO.Status == Status.Posted)
                                    {
                                        if (CheckAvailableStock(_currSession, _locStockTransferXPO) == true)
                                        {
                                            SetTransferConfirmStock(_currSession, _locStockTransferXPO);
                                            SetFinalStatus(_currSession, _locStockTransferXPO);
                                        }
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data Stock Transfer Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Stock Transfer Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = StockTransfer " + ex.ToString());
            }
        }

        private void StockTransferPostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        StockTransfer _locPurchaseOrderOS = (StockTransfer)_objectSpace.GetObject(obj);

                        if (_locPurchaseOrderOS != null)
                        {
                            if (_locPurchaseOrderOS.Code != null)
                            {
                                _currObjectId = _locPurchaseOrderOS.Code;

                                StockTransfer _locStockTransferXPO = _currSession.FindObject<StockTransfer>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locStockTransferXPO != null)
                                {
                                    if (_locStockTransferXPO.Status == Status.Progress || _locStockTransferXPO.Status == Status.Posted)
                                    {
                                        if (CheckAvailableStock(_currSession, _locStockTransferXPO) == true)
                                        {
                                            SetTransferPostingStock(_currSession, _locStockTransferXPO);
                                        }
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data Stock Transfer Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Stock Transfer Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = StockTransfer " + ex.ToString());
            }
        }

        private void StockTransferListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(StockTransfer)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = StockTransfer " + ex.ToString());
            }
        }

        #region Stock Transfer

        private void SetFinalStatus(Session _currSession, StockTransfer _locStockTransferXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locCountStatus = 0;

                if (_locStockTransferXPO != null)
                {
                    XPCollection<StockTransferLine> _locStockTransferLines = new XPCollection<StockTransferLine>
                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("StockTransfer", _locStockTransferXPO)));

                    if (_locStockTransferLines != null && _locStockTransferLines.Count() > 0)
                    {
                        foreach (StockTransferLine _locStockTransferLine in _locStockTransferLines)
                        {
                            if (_locStockTransferLine.StatusTo != Status.Close)
                            {
                                _locCountStatus = _locCountStatus + 1;
                            }
                        }
                    }

                    if (_locCountStatus == 0)
                    {
                        _locStockTransferXPO.Status = Status.Close;
                        _locStockTransferXPO.ActivationPosting = true;
                        _locStockTransferXPO.Save();
                        _locStockTransferXPO.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PrePurchaseOrder " + ex.ToString());
            }
        }

        private bool CheckAvailableStock(Session _currSession, StockTransfer _locStockTransferXPO)
        {
            bool _result = true;
            try
            {
                XPCollection<StockTransferLine> _locStoTransLines = new XPCollection<StockTransferLine>(_currSession,
                                                                        new BinaryOperator("StockTransfer", _locStockTransferXPO));

                if (_locStoTransLines != null && _locStoTransLines.Count > 0)
                {
                    double _locStoLineTotal = 0;
                    BeginingInventory _locBegInventory = null;
                    foreach (StockTransferLine _locStoTransLine in _locStoTransLines)
                    {
                        if (_locStoTransLine.LocationFrom != null && _locStoTransLine.BinLocationFrom != null)
                        {
                            _locBegInventory = _currSession.FindObject<BeginingInventory>
                                                (new GroupOperator(GroupOperatorType.And,
                                                new BinaryOperator("Item", _locStoTransLine.Item)));

                            if (_locBegInventory != null)
                            {
                                XPCollection<BeginingInventoryLine> _locBegInvLines = new XPCollection<BeginingInventoryLine>(_currSession,
                                                                                    new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("BeginingInventory", _locBegInventory),
                                                                                    new BinaryOperator("Item", _locStoTransLine.Item),
                                                                                    new BinaryOperator("Location", _locStoTransLine.LocationFrom),
                                                                                    new BinaryOperator("BinLocation", _locStoTransLine.BinLocationFrom),
                                                                                    new BinaryOperator("StockType", _locStoTransLine.StockTypeFrom)));

                                if (_locBegInvLines != null && _locBegInvLines.Count() > 0)
                                {
                                    foreach (BeginingInventoryLine _locBegInvLine in _locBegInvLines)
                                    {
                                        ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", _locStoTransLine.Item),
                                                         new BinaryOperator("UOM", _locStoTransLine.UOM_From),
                                                         new BinaryOperator("DefaultUOM", _locStoTransLine.DUOM_From),
                                                         new BinaryOperator("Active", true)));

                                        if (_locItemUOM != null)
                                        {
                                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                            {
                                                _locStoLineTotal = _locStoTransLine.QtyFrom * _locItemUOM.DefaultConversion + _locStoTransLine.DQtyFrom;
                                            }
                                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                            {
                                                _locStoLineTotal = _locStoTransLine.QtyFrom / _locItemUOM.Conversion + _locStoTransLine.DQtyFrom;
                                            }
                                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                            {
                                                _locStoLineTotal = _locStoTransLine.QtyFrom + _locStoTransLine.DQtyFrom;
                                            }

                                            if (_locStoLineTotal > _locBegInventory.QtyAvailable)
                                            {
                                                _result = false;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = StockTransfer ", ex.ToString());
            }
            return _result;
        }

        private void SetTransferPostingStock(Session _currSession, StockTransfer _locStockTransferXPO)
        {
            try
            {
                XPCollection<StockTransferLine> _locStoTransLines = new XPCollection<StockTransferLine>(_currSession,
                                                                        new BinaryOperator("StockTransfer", _locStockTransferXPO));

                if (_locStoTransLines != null && _locStoTransLines.Count > 0)
                {
                    double _locStoLineTotal = 0;
                    BeginingInventory _locBegInventory = null;
                    DateTime now = DateTime.Now;

                    foreach (StockTransferLine _locStoTransLine in _locStoTransLines)
                    {
                        if (_locStoTransLine.LocationFrom != null && _locStoTransLine.BinLocationFrom != null
                            && _locStoTransLine.LocationTo != null && _locStoTransLine.BinLocationTo != null)
                        {
                            _locBegInventory = _currSession.FindObject<BeginingInventory>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", _locStoTransLine.Item)));

                            if (_locBegInventory != null)
                            {
                                #region From
                                XPCollection<BeginingInventoryLine> _locBegInvLineFroms = new XPCollection<BeginingInventoryLine>(_currSession,
                                                                                    new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("BeginingInventory", _locBegInventory),
                                                                                    new BinaryOperator("Item", _locStoTransLine.Item),
                                                                                    new BinaryOperator("Location", _locStoTransLine.LocationFrom),
                                                                                    new BinaryOperator("BinLocation", _locStoTransLine.BinLocationFrom),
                                                                                    new BinaryOperator("StockType", _locStoTransLine.StockTypeFrom)));

                                if (_locBegInvLineFroms != null && _locBegInvLineFroms.Count() > 0)
                                {
                                    foreach (BeginingInventoryLine _locBegInvLineFrom in _locBegInvLineFroms)
                                    {
                                        ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", _locStoTransLine.Item),
                                                         new BinaryOperator("UOM", _locStoTransLine.UOM_From),
                                                         new BinaryOperator("DefaultUOM", _locStoTransLine.DUOM_From),
                                                         new BinaryOperator("Active", true)));

                                        if (_locItemUOM != null)
                                        {
                                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                            {
                                                _locStoLineTotal = _locStoTransLine.QtyFrom * _locItemUOM.DefaultConversion + _locStoTransLine.DQtyFrom;
                                            }
                                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                            {
                                                _locStoLineTotal = _locStoTransLine.QtyFrom / _locItemUOM.Conversion + _locStoTransLine.DQtyFrom;
                                            }
                                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                            {
                                                _locStoLineTotal = _locStoTransLine.QtyFrom + _locStoTransLine.DQtyFrom;
                                            }
                                        }
                                        else
                                        {
                                            _locStoLineTotal = _locStoTransLine.QtyFrom + _locStoTransLine.DQtyFrom;
                                        }

                                        //Mengurangkan Stock 
                                        _locBegInvLineFrom.QtyAvailable = _locBegInvLineFrom.QtyAvailable - _locStoLineTotal;
                                        _locBegInvLineFrom.Save();
                                        _locBegInvLineFrom.Session.CommitTransaction();

                                        //Membuat Journal Negatif di Inventory Journal
                                        InventoryJournal _saveNegatifInvJournal = new InventoryJournal(_currSession)
                                        {

                                            DocumentType = _locStockTransferXPO.DocumentType,
                                            DocNo = _locStockTransferXPO.DocNo,
                                            Location = _locStoTransLine.LocationFrom,
                                            BinLocation = _locStoTransLine.BinLocationFrom,
                                            Item = _locStoTransLine.Item,
                                            QtyNeg = _locStoLineTotal,
                                            QtyPos = 0,
                                            JournalDate = now
                                        };
                                        _saveNegatifInvJournal.Save();
                                        _saveNegatifInvJournal.Session.CommitTransaction();

                                    }
                                    _locStoTransLine.DQtyTo = _locStoTransLine.DQtyFrom;
                                    _locStoTransLine.DUOM_To = _locStoTransLine.DUOM_From;
                                    _locStoTransLine.QtyTo = _locStoTransLine.QtyFrom;
                                    _locStoTransLine.UOM_To = _locStoTransLine.UOM_From;
                                    _locStoTransLine.ActivationPostingFrom = true;
                                    _locStoTransLine.StatusFrom = Status.Close;
                                    _locStoTransLine.StatusDateFrom = now;
                                    _locStoTransLine.Save();
                                    _locStoTransLine.Session.CommitTransaction();
                                }
                                #endregion From
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = StockTransfer ", ex.ToString());
            }
        }

        private void SetTransferConfirmStock(Session _currSession, StockTransfer _locStockTransferXPO)
        {
            try
            {
                XPCollection<StockTransferLine> _locStoTransLines = new XPCollection<StockTransferLine>(_currSession,
                                                                        new BinaryOperator("StockTransfer", _locStockTransferXPO));

                if (_locStoTransLines != null && _locStoTransLines.Count > 0)
                {
                    double _locStoLineTotal = 0;
                    BeginingInventory _locBegInventory = null;
                    DateTime now = DateTime.Now;

                    foreach (StockTransferLine _locStoTransLine in _locStoTransLines)
                    {
                        if (_locStoTransLine.LocationFrom != null && _locStoTransLine.BinLocationFrom != null
                            && _locStoTransLine.LocationTo != null && _locStoTransLine.BinLocationTo != null)
                        {
                            _locBegInventory = _currSession.FindObject<BeginingInventory>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", _locStoTransLine.Item)));

                            if (_locBegInventory != null)
                            {
                                #region To
                                XPCollection<BeginingInventoryLine> _locBegInvLineTos = new XPCollection<BeginingInventoryLine>(_currSession,
                                                                                    new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("BeginingInventory", _locBegInventory),
                                                                                    new BinaryOperator("Item", _locStoTransLine.Item),
                                                                                    new BinaryOperator("Location", _locStoTransLine.LocationTo),
                                                                                    new BinaryOperator("BinLocation", _locStoTransLine.BinLocationTo),
                                                                                    new BinaryOperator("StockType", _locStoTransLine.StockTypeTo)));

                                if (_locBegInvLineTos != null && _locBegInvLineTos.Count() > 0)
                                {
                                    foreach (BeginingInventoryLine _locBegInvLineTo in _locBegInvLineTos)
                                    {
                                        ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", _locStoTransLine.Item),
                                                         new BinaryOperator("UOM", _locStoTransLine.UOM_From),
                                                         new BinaryOperator("DefaultUOM", _locStoTransLine.DUOM_From),
                                                         new BinaryOperator("Active", true)));

                                        if (_locItemUOM != null)
                                        {
                                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                            {
                                                _locStoLineTotal = _locStoTransLine.QtyTo * _locItemUOM.DefaultConversion + _locStoTransLine.DQtyTo;
                                            }
                                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                            {
                                                _locStoLineTotal = _locStoTransLine.QtyTo / _locItemUOM.Conversion + _locStoTransLine.DQtyTo;
                                            }
                                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                            {
                                                _locStoLineTotal = _locStoTransLine.QtyTo + _locStoTransLine.DQtyTo;
                                            }
                                        }
                                        else
                                        {
                                            _locStoLineTotal = _locStoTransLine.QtyTo + _locStoTransLine.DQtyTo;
                                        }

                                        //Menambah Stock
                                        _locBegInvLineTo.QtyAvailable = _locBegInvLineTo.QtyAvailable + _locStoLineTotal;
                                        _locBegInvLineTo.Save();
                                        _locBegInvLineTo.Session.CommitTransaction();

                                        //Membuat Journal Positid di Inventory Journal
                                        InventoryJournal _savePositifInvJournal = new InventoryJournal(_currSession)
                                        {
                                            DocumentType = _locStockTransferXPO.DocumentType,
                                            DocNo = _locStockTransferXPO.DocNo,
                                            Location = _locStoTransLine.LocationTo,
                                            BinLocation = _locStoTransLine.BinLocationTo,
                                            Item = _locStoTransLine.Item,
                                            QtyNeg = 0,
                                            QtyPos = _locStoLineTotal,
                                            JournalDate = now
                                        };
                                        _savePositifInvJournal.Save();
                                        _savePositifInvJournal.Session.CommitTransaction();
                                    }
                                    _locStoTransLine.StatusTo = Status.Close;
                                    _locStoTransLine.StatusDateTo = now;
                                    _locStoTransLine.ActivationPostingTo = true;
                                    _locStoTransLine.Save();
                                    _locStoTransLine.Session.CommitTransaction();
                                }
                                else
                                {
                                    ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", _locStoTransLine.Item),
                                                         new BinaryOperator("UOM", _locStoTransLine.UOM_From),
                                                         new BinaryOperator("DefaultUOM", _locStoTransLine.DUOM_From),
                                                         new BinaryOperator("Active", true)));

                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locStoLineTotal = _locStoTransLine.QtyTo * _locItemUOM.DefaultConversion + _locStoTransLine.DQtyTo;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locStoLineTotal = _locStoTransLine.QtyTo / _locItemUOM.Conversion + _locStoTransLine.DQtyTo;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locStoLineTotal = _locStoTransLine.QtyTo + _locStoTransLine.DQtyTo;
                                        }
                                    }
                                    else
                                    {
                                        _locStoLineTotal = _locStoTransLine.QtyTo + _locStoTransLine.DQtyTo;
                                    }
                                    //Membuat Stock Begining Inventory Line
                                    BeginingInventoryLine _saveBegInvLine = new BeginingInventoryLine(_currSession)
                                    {
                                        Item = _locStoTransLine.Item,
                                        Location = _locStoTransLine.LocationTo,
                                        BinLocation = _locStoTransLine.BinLocationTo,
                                        QtyAvailable = _locStoLineTotal,
                                        DefaultUOM = _locStoTransLine.DUOM_From,
                                        StockType = _locStoTransLine.StockTypeTo,
                                        Active = true,
                                        BeginingInventory = _locBegInventory,
                                        ProjectHeader = _locStockTransferXPO.ProjectHeader,
                                    };
                                    _saveBegInvLine.Save();
                                    _saveBegInvLine.Session.CommitTransaction();

                                    InventoryJournal _savePositifInvJournal = new InventoryJournal(_currSession)
                                    {
                                        DocumentType = _locStockTransferXPO.DocumentType,
                                        DocNo = _locStockTransferXPO.DocNo,
                                        Location = _locStoTransLine.LocationTo,
                                        BinLocation = _locStoTransLine.BinLocationTo,
                                        Item = _locStoTransLine.Item,
                                        QtyNeg = 0,
                                        QtyPos = _locStoLineTotal,
                                        JournalDate = now
                                    };
                                    _savePositifInvJournal.Save();
                                    _savePositifInvJournal.Session.CommitTransaction();

                                    _locStoTransLine.StatusTo = Status.Close;
                                    _locStoTransLine.StatusDateTo = now;
                                    _locStoTransLine.ActivationPostingTo = true;
                                    _locStoTransLine.Save();
                                    _locStoTransLine.Session.CommitTransaction();
                                }
                                #endregion To  
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = StockTransfer ", ex.ToString());
            }
        }

        #endregion Stock Transfer

        #region Global Method

        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0} !", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method

    }
}
