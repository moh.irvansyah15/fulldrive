﻿namespace FullDrive.Module.Controllers
{
    partial class TransferOrderLineActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.TransferOrderLineSelectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.TransferOrderLineUnselectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.TransferOrderLineListviewFilterSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // TransferOrderLineSelectAction
            // 
            this.TransferOrderLineSelectAction.Caption = "Select";
            this.TransferOrderLineSelectAction.ConfirmationMessage = null;
            this.TransferOrderLineSelectAction.Id = "TransferOrderLineSelectActionId";
            this.TransferOrderLineSelectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.TransferOrderLine);
            this.TransferOrderLineSelectAction.ToolTip = null;
            this.TransferOrderLineSelectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.TransferOrderLineSelectAction_Execute);
            // 
            // TransferOrderLineUnselectAction
            // 
            this.TransferOrderLineUnselectAction.Caption = "Unselect";
            this.TransferOrderLineUnselectAction.ConfirmationMessage = null;
            this.TransferOrderLineUnselectAction.Id = "TransferOrderLineUnselectActionId";
            this.TransferOrderLineUnselectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.TransferOrderLine);
            this.TransferOrderLineUnselectAction.ToolTip = null;
            this.TransferOrderLineUnselectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.TransferOrderLineUnselectAction_Execute);
            // 
            // TransferOrderLineListviewFilterSelectionAction
            // 
            this.TransferOrderLineListviewFilterSelectionAction.Caption = "Filter";
            this.TransferOrderLineListviewFilterSelectionAction.ConfirmationMessage = null;
            this.TransferOrderLineListviewFilterSelectionAction.Id = "TransferOrderLineListviewFilterSelectionActionId";
            this.TransferOrderLineListviewFilterSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.TransferOrderLineListviewFilterSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.TransferOrderLine);
            this.TransferOrderLineListviewFilterSelectionAction.ToolTip = null;
            this.TransferOrderLineListviewFilterSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.TransferOrderLineListviewFilterSelectionAction_Execute);
            // 
            // TransferOrderLineActionController
            // 
            this.Actions.Add(this.TransferOrderLineSelectAction);
            this.Actions.Add(this.TransferOrderLineUnselectAction);
            this.Actions.Add(this.TransferOrderLineListviewFilterSelectionAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction TransferOrderLineSelectAction;
        private DevExpress.ExpressApp.Actions.SimpleAction TransferOrderLineUnselectAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction TransferOrderLineListviewFilterSelectionAction;
    }
}
