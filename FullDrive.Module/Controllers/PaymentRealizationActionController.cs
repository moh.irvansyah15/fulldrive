﻿#region Default

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

#endregion Default

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;
using System.Web;
using System.IO;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class PaymentRealizationActionController : ViewController
    {
        #region Default

        private ChoiceActionItem _selectionListviewFilter;
        private ChoiceActionItem _setApprovalLevel;

        public PaymentRealizationActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.

            #region FilterApproval
            PaymentRealizationListviewFilterApprovalSelectionAction.Items.Clear();
            foreach (object _currApprovalFilter in Enum.GetValues(typeof(CustomProcess.ApprovalFilter)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.ApprovalFilter));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApprovalFilter), _currApprovalFilter);
                PaymentRealizationListviewFilterApprovalSelectionAction.Items.Add(_selectionListviewFilter);
            }
            #endregion FilterApproval
        }
        protected override void OnActivated()
        {
            TypeOfView = typeof(ObjectView);
            base.OnActivated();
            // Perform various tasks depending on the target View.
            #region Approval
            var _locUser = SecuritySystem.CurrentUserName;
            GlobalFunction _globalFunction = new GlobalFunction();
            Session _currentSession = null;
            if (this.ObjectSpace != null)
            {
                _currentSession = ((XPObjectSpace)this.ObjectSpace).Session;
            }

            if (_currentSession != null)
            {
                UserAccess _locUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", _locUser));

                if (_locUserAccess != null)
                {
                    PaymentRealizationApprovalAction.Items.Clear();

                    XPCollection<ApplicationSetupDetail> _locAppSetupDetails = new XPCollection<ApplicationSetupDetail>
                                                                                 (_currentSession, new GroupOperator(GroupOperatorType.And,
                                                                                  new BinaryOperator("UserAccess", _locUserAccess),
                                                                                  new BinaryOperator("ObjectList", CustomProcess.ObjectList.PaymentRealization),
                                                                                  new BinaryOperator("FunctionList", CustomProcess.FunctionList.Approval),
                                                                                  new BinaryOperator("Active", true)));

                    if (_locAppSetupDetails != null && _locAppSetupDetails.Count > 0)
                    {

                        foreach (ApplicationSetupDetail _locAppSetupDetail in _locAppSetupDetails)
                        {
                            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.ApprovalLevel)))
                            {
                                if (_locAppSetupDetail.ApprovalLevel != CustomProcess.ApprovalLevel.None)
                                {
                                    EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.ApprovalLevel));
                                    _setApprovalLevel = new ChoiceActionItem(_ed.GetCaption(_locAppSetupDetail.ApprovalLevel), _locAppSetupDetail.ApprovalLevel);
                                }
                            }
                            PaymentRealizationApprovalAction.Items.Add(_setApprovalLevel);
                        }
                    }
                }
            }
            #endregion Approval
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        #endregion Default

        private void PaymentRealizationProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        PaymentRealization _locPaymentRealizationOS = (PaymentRealization)_objectSpace.GetObject(obj);

                        if (_locPaymentRealizationOS != null)
                        {
                            if (_locPaymentRealizationOS.Code != null)
                            {
                                _currObjectId = _locPaymentRealizationOS.Code;

                                PaymentRealization _locPaymentRealizationXPO = _currSession.FindObject<PaymentRealization>
                                                                 (new GroupOperator(GroupOperatorType.And,
                                                                  new BinaryOperator("Code", _currObjectId)));

                                if (_locPaymentRealizationXPO != null)
                                {
                                    if (_locPaymentRealizationXPO.Status == Status.Open || _locPaymentRealizationXPO.Status == Status.Progress)
                                    {
                                        if (_locPaymentRealizationXPO.Status == Status.Open)
                                        {
                                            _locPaymentRealizationXPO.Status = Status.Progress;
                                            _locPaymentRealizationXPO.StatusDate = now;
                                            _locPaymentRealizationXPO.Save();
                                            _locPaymentRealizationXPO.Session.CommitTransaction();
                                        }

                                        XPCollection<PaymentRealizationLine> _locPaymentRealizationLines = new XPCollection<PaymentRealizationLine>
                                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                            new BinaryOperator("PaymentRealization", _locPaymentRealizationXPO),
                                                                                                            new BinaryOperator("Status", Status.Open)));

                                        if (_locPaymentRealizationLines != null && _locPaymentRealizationLines.Count > 0)
                                        {
                                            foreach (PaymentRealizationLine _locPaymentRealizationLine in _locPaymentRealizationLines)
                                            {
                                                _locPaymentRealizationLine.Status = Status.Progress;
                                                _locPaymentRealizationLine.StatusDate = now;
                                                _locPaymentRealizationLine.Save();
                                                _locPaymentRealizationLine.Session.CommitTransaction();
                                            }
                                        }

                                        XPCollection<CashAdvanceCollection> _locCashAdvanceCollections = new XPCollection<CashAdvanceCollection>
                                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                        new BinaryOperator("PaymentRealization", _locPaymentRealizationXPO),
                                                                                                        new BinaryOperator("Status", Status.Open)));
                                        if(_locCashAdvanceCollections != null && _locCashAdvanceCollections.Count() > 0)
                                        {
                                            foreach(CashAdvanceCollection _locCashAdvanceCollection in _locCashAdvanceCollections)
                                            {
                                                _locCashAdvanceCollection.Status = Status.Progress;
                                                _locCashAdvanceCollection.StatusDate = now;
                                                _locCashAdvanceCollection.Save();
                                                _locCashAdvanceCollection.Session.CommitTransaction();
                                            }
                                        }
                                    }

                                    SuccessMessageShow("PaymentRealization has successfully updated to progress");
                                }
                                else
                                {
                                    ErrorMessageShow("PaymentRealization Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("PaymentRealization Not Available");
                            }
                        }
                    }
                }
                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PaymentRealization" + ex.ToString());
            }
        }

        private void PaymentRealizationGetCAAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;


                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        PaymentRealization _locPaymentRealizationOS = (PaymentRealization)_objectSpace.GetObject(obj);

                        if (_locPaymentRealizationOS != null)
                        {
                            if (_locPaymentRealizationOS.Code != null)
                            {
                                _currObjectId = _locPaymentRealizationOS.Code;

                                PaymentRealization _locPaymentRealizationXPO = _currSession.FindObject<PaymentRealization>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locPaymentRealizationXPO != null)
                                {
                                    if (_locPaymentRealizationXPO.Status == Status.Open || _locPaymentRealizationXPO.Status == Status.Progress)
                                    {
                                        XPCollection<CashAdvanceCollection> _locCashAdvanceCollections = new XPCollection<CashAdvanceCollection>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("PaymentRealization", _locPaymentRealizationXPO),
                                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("Status", Status.Open),
                                                                                        new BinaryOperator("Status", Status.Progress))));

                                        if (_locCashAdvanceCollections != null && _locCashAdvanceCollections.Count() > 0)
                                        {
                                            foreach (CashAdvanceCollection _locCashAdvanceCollection in _locCashAdvanceCollections)
                                            {
                                                if (_locCashAdvanceCollection.CashAdvance != null && _locCashAdvanceCollection.PaymentRealization != null)
                                                {
                                                    GetCashAdvanceMonitoring(_currSession, _locCashAdvanceCollection.CashAdvance, _locPaymentRealizationXPO);
                                                }
                                            }
                                            SuccessMessageShow("CashAdvance Has Been Successfully Getting into SalesInvoice");
                                        }
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("CashAdvance Order Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("CashAdvance Order Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject =  PaymentRealization " + ex.ToString());
            }
        }

        private void PaymentRealizationPostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        PaymentRealization _locPaymentRealizationOS = (PaymentRealization)_objectSpace.GetObject(obj);

                        if (_locPaymentRealizationOS != null)
                        {
                            if (_locPaymentRealizationOS.Code != null)
                            {
                                _currObjectId = _locPaymentRealizationOS.Code;

                                PaymentRealization _locPaymentRealizationXPO = _currSession.FindObject<PaymentRealization>
                                                                               (new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("Code", _currObjectId)));

                                if (_locPaymentRealizationXPO != null)
                                {
                                    if (_locPaymentRealizationXPO.Status == Status.Progress || _locPaymentRealizationXPO.Status == Status.Posted)
                                    {
                                        ApprovalLine _locApprovalLineXPO = _currSession.FindObject<ApprovalLine>
                                                                           (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("EndApproval", true),
                                                                            new BinaryOperator("PaymentRealization", _locPaymentRealizationXPO)));

                                        if (_locApprovalLineXPO != null)
                                        {
                                            SetPaymentRealizationMonitoring(_currSession, _locPaymentRealizationXPO);
                                            SetJournalRealization(_currSession, _locPaymentRealizationXPO);
                                            SetProcessCount(_currSession, _locPaymentRealizationXPO);
                                            SetStatusPaymentRealizationLine(_currSession, _locPaymentRealizationXPO);
                                            SetStatusPaymentRealization(_currSession, _locPaymentRealizationXPO);
                                            SetCloseAllCAProcess(_currSession, _locPaymentRealizationXPO);
                                        }
                                        else
                                        {
                                            ErrorMessageShow("Data PaymentRealization Not Available");
                                        }
                                    }
                                    else
                                    {
                                        ErrorMessageShow("Data PaymentRealization Not Available");
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data PaymentRealization Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data PaymentRealization Not Available");
                            }
                        }
                        else
                        {
                            ErrorMessageShow("Data PaymentRealization Not Available");
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PaymentRealization" + ex.ToString());
            }
        }

        private void PaymentRealizationListviewFilterApprovalSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(PaymentRealization)))
                {
                    if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.Approval)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("ActiveApproved3", false, BinaryOperatorType.Equal),
                                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("ActiveApproved1", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", true, BinaryOperatorType.Equal)));
                    }
                    else if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.EndApproval)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new BinaryOperator("ActiveApproved3", true, BinaryOperatorType.Equal);
                    }
                    else if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.AllData)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("ActiveApproved1", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved3", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved1", false, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", false, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved3", false, BinaryOperatorType.Equal));
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PaymentRealization " + ex.ToString());
            }
        }

        private void PaymentRealizationApprovalAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                ApplicationSetupDetail _locAppSetDetail = null;
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                var User = SecuritySystem.CurrentUserName;
                Session _currentSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currentSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                UserAccess _locUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", User));

                foreach (Object obj in _objectsToProcess)
                {
                    PaymentRealization _objInNewObjectSpace = (PaymentRealization)_objectSpace.GetObject(obj);

                    if (_objInNewObjectSpace != null)
                    {
                        if (_objInNewObjectSpace.Code != null)
                        {
                            _currObjectId = _objInNewObjectSpace.Code;
                        }
                    }

                    if (_currObjectId != null)
                    {
                        PaymentRealization _locPaymentRealizationXPO = _currentSession.FindObject<PaymentRealization>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("Code", _currObjectId)));

                        if (_locPaymentRealizationXPO != null)
                        {
                            if (_locPaymentRealizationXPO.Status == Status.Progress)
                            {
                                ApprovalLine _locApprovalLine = _currentSession.FindObject<ApprovalLine>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("PaymentRealization", _locPaymentRealizationXPO),
                                                                             new BinaryOperator("EndApproval", true)));
                                if (_locApprovalLine == null)
                                {
                                    #region Approval Level 1
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level1)
                                    {
                                        _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level1, ObjectList.PaymentRealization);
                                        if (_locAppSetDetail != null)
                                        {
                                            //Buat bs input langsung ke approvalline
                                            ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("PaymentRealization", _locPaymentRealizationXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level1)));
                                            if (_locApprovalLineXPO == null)
                                            {

                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level1,
                                                        EndApproval = true,
                                                        PaymentRealization = _locPaymentRealizationXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locPaymentRealizationXPO.ActivationPosting = true;
                                                    _locPaymentRealizationXPO.ActiveApproved1 = false;
                                                    _locPaymentRealizationXPO.ActiveApproved2 = false;
                                                    _locPaymentRealizationXPO.ActiveApproved3 = true;
                                                    _locPaymentRealizationXPO.Save();
                                                    _locPaymentRealizationXPO.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level1,
                                                        PaymentRealization = _locPaymentRealizationXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locPaymentRealizationXPO.ActiveApproved1 = true;
                                                    _locPaymentRealizationXPO.ActiveApproved2 = false;
                                                    _locPaymentRealizationXPO.ActiveApproved3 = false;
                                                    _locPaymentRealizationXPO.Save();
                                                    _locPaymentRealizationXPO.Session.CommitTransaction();
                                                }

                                                //Send Email
                                                ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("PaymentRealization", _locPaymentRealizationXPO)));

                                                if (_locApprovalLineXPO2 != null)
                                                {
                                                    SendEmail(_currentSession, _locPaymentRealizationXPO, Status.Progress, _locApprovalLineXPO2);
                                                }

                                                SuccessMessageShow("PaymentRealization has successfully Approve");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 1

                                    #region Approval Level 2
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level2)
                                    {
                                        _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level2, ObjectList.PaymentRealization);

                                        if (_locAppSetDetail != null)
                                        {
                                            ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("PaymentRealization", _locPaymentRealizationXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level2)));

                                            if (_locApprovalLineXPO == null)
                                            {
                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level2,
                                                        EndApproval = true,
                                                        PaymentRealization = _locPaymentRealizationXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locPaymentRealizationXPO.ActivationPosting = true;
                                                    _locPaymentRealizationXPO.ActiveApproved1 = false;
                                                    _locPaymentRealizationXPO.ActiveApproved2 = false;
                                                    _locPaymentRealizationXPO.ActiveApproved3 = true;
                                                    _locPaymentRealizationXPO.Save();
                                                    _locPaymentRealizationXPO.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level2,
                                                        PaymentRealization = _locPaymentRealizationXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    SetApprovalLine(_currentSession, _locPaymentRealizationXPO, ApprovalLevel.Level1);

                                                    _locPaymentRealizationXPO.ActiveApproved1 = false;
                                                    _locPaymentRealizationXPO.ActiveApproved2 = true;
                                                    _locPaymentRealizationXPO.ActiveApproved3 = false;
                                                    _locPaymentRealizationXPO.Save();
                                                    _locPaymentRealizationXPO.Session.CommitTransaction();
                                                }

                                                //Send Email
                                                ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("PaymentRealization", _locPaymentRealizationXPO)));

                                                if (_locApprovalLineXPO2 != null)
                                                {
                                                    SendEmail(_currentSession, _locPaymentRealizationXPO, Status.Progress, _locApprovalLineXPO2);
                                                }

                                                SuccessMessageShow("PaymentRealization has successfully Approve");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 2

                                    #region Approval Level 3
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level3)
                                    {
                                        _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level3, ObjectList.PaymentRealization);

                                        if (_locAppSetDetail != null)
                                        {
                                            ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("PaymentRealization", _locPaymentRealizationXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level3)));

                                            if (_locApprovalLineXPO == null)
                                            {
                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level3,
                                                        EndApproval = true,
                                                        PaymentRealization = _locPaymentRealizationXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locPaymentRealizationXPO.ActivationPosting = true;
                                                    _locPaymentRealizationXPO.ActiveApproved1 = false;
                                                    _locPaymentRealizationXPO.ActiveApproved2 = false;
                                                    _locPaymentRealizationXPO.ActiveApproved3 = true;
                                                    _locPaymentRealizationXPO.Save();
                                                    _locPaymentRealizationXPO.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level3,
                                                        PaymentRealization = _locPaymentRealizationXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    SetApprovalLine(_currentSession, _locPaymentRealizationXPO, ApprovalLevel.Level2);
                                                    SetApprovalLine(_currentSession, _locPaymentRealizationXPO, ApprovalLevel.Level1);

                                                    _locPaymentRealizationXPO.ActiveApproved1 = false;
                                                    _locPaymentRealizationXPO.ActiveApproved2 = false;
                                                    _locPaymentRealizationXPO.ActiveApproved3 = true;
                                                    _locPaymentRealizationXPO.Save();
                                                    _locPaymentRealizationXPO.Session.CommitTransaction();
                                                }

                                                //Send Email
                                                ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("PaymentRealization", _locPaymentRealizationXPO)));

                                                if (_locApprovalLineXPO2 != null)
                                                {
                                                    SendEmail(_currentSession, _locPaymentRealizationXPO, Status.Progress, _locApprovalLineXPO2);
                                                }

                                                SuccessMessageShow("PaymentRealization has successfully Approve");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 3
                                }

                            }
                            else
                            {
                                ErrorMessageShow("PaymentRealization Status Not Available");
                            }
                        }
                        else
                        {
                            ErrorMessageShow("PaymentRealization Not Available");
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PaymentRealization " + ex.ToString());
            }
        }

        

        //============================================= Code Only =================================================


        #region GetCA

        private void GetCashAdvanceMonitoring(Session _currSession, CashAdvance _locCashAdvanceXPO, PaymentRealization _locPaymentRealizationXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;

                if (_locCashAdvanceXPO != null && _locPaymentRealizationXPO != null)
                {
                    XPCollection<CashAdvanceMonitoring> _locCashAdvanceMonitorings = new XPCollection<CashAdvanceMonitoring>
                                                                                     (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("CashAdvance", _locCashAdvanceXPO),
                                                                                        new BinaryOperator("PaymentRealization", _locPaymentRealizationXPO),
                                                                                        new BinaryOperator("Select", true),
                                                                                        new BinaryOperator("Status", Status.Close, BinaryOperatorType.NotEqual)));
                    if (_locCashAdvanceMonitorings != null && _locCashAdvanceMonitorings.Count() > 0)
                    {
                        foreach (CashAdvanceMonitoring _locCashAdvanceMonitoring in _locCashAdvanceMonitorings)
                        {
                            if (_locCashAdvanceMonitoring.CashAdvanceLine != null)
                            {
                                PaymentRealizationLine _saveDataPaymentRealizationLine = new PaymentRealizationLine(_currSession)
                                {
                                    CashAdvanceType = _locCashAdvanceMonitoring.CashAdvanceType,
                                    Employee = _locCashAdvanceMonitoring.Employee,
                                    Amount = _locCashAdvanceMonitoring.AmountRealized,
                                    AmountRealized = _locCashAdvanceMonitoring.AmountRealized,
                                    Description = _locCashAdvanceMonitoring.Description,
                                    Company = _locCashAdvanceMonitoring.Company,
                                    Division = _locCashAdvanceMonitoring.Division,
                                    Department = _locCashAdvanceMonitoring.Department,
                                    Section = _locCashAdvanceMonitoring.Section,
                                    CashAdvanceMonitoring = _locCashAdvanceMonitoring,
                                    CashAdvance = _locCashAdvanceMonitoring.CashAdvance,
                                    PaymentRealization = _locPaymentRealizationXPO,
                                };
                                _saveDataPaymentRealizationLine.Save();
                                _saveDataPaymentRealizationLine.Session.CommitTransaction();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PaymentRealization ", ex.ToString());
            }
        }

        #endregion GetCA

        #region PostingForPaymentRealization

        private void SetPaymentRealizationMonitoring(Session _currSession, PaymentRealization _locPaymentRealizationXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _currSignCode = null;

                if (_locPaymentRealizationXPO != null)
                {
                    XPCollection<PaymentRealizationLine> _locPaymentRealizationLines = new XPCollection<PaymentRealizationLine>
                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("PaymentRealization", _locPaymentRealizationXPO),
                                                                                new BinaryOperator("Select", true),
                                                                                new GroupOperator(GroupOperatorType.Or, 
                                                                                new BinaryOperator("Status", Status.Progress),
                                                                                new BinaryOperator("Status", Status.Posted))));

                    if(_locPaymentRealizationLines != null && _locPaymentRealizationLines.Count() > 0)
                    {
                        foreach(PaymentRealizationLine _locPaymentRealizationLine in _locPaymentRealizationLines)
                        {
                            if(_locPaymentRealizationLine.AmountRealized > 0)
                            {
                                _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.PaymentRealizationMonitoring);
                                if(_currSignCode != null)
                                {
                                    PaymentRealizationMonitoring _saveDataPaymentRealizationMonitoring = new PaymentRealizationMonitoring(_currSession)
                                    {
                                        SignCode = _currSignCode,
                                        PaymentRealization = _locPaymentRealizationXPO,
                                        PaymentRealizationLine = _locPaymentRealizationLine,
                                        CashAdvanceType = _locPaymentRealizationLine.CashAdvanceType,
                                        Employee = _locPaymentRealizationLine.Employee,
                                        Amount = _locPaymentRealizationLine.AmountRealized,
                                        AmountRealized = _locPaymentRealizationLine.AmountRealized,
                                        Description = _locPaymentRealizationLine.Description,
                                        Company = _locPaymentRealizationLine.Company,
                                        Division = _locPaymentRealizationLine.Division,
                                        Department = _locPaymentRealizationLine.Department,
                                        Section = _locPaymentRealizationLine.Section,
                                        CashAdvance = _locPaymentRealizationLine.CashAdvance,
                                        CashAdvanceMonitoring = _locPaymentRealizationLine.CashAdvanceMonitoring,
                                    };
                                    _saveDataPaymentRealizationMonitoring.Save();
                                    _saveDataPaymentRealizationMonitoring.Session.CommitTransaction();

                                    PaymentRealizationMonitoring _locPaymentRealizationMonitoring = _currSession.FindObject<PaymentRealizationMonitoring>
                                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                                    new BinaryOperator("SignCode", _currSignCode)));
                                    if(_locPaymentRealizationMonitoring != null)
                                    {
                                        #region CashAdvancePaymentPlan

                                        if(_locPaymentRealizationMonitoring.CashAdvanceMonitoring != null)
                                        {
                                            double _locAmountRealized = 0;
                                            double _locOutstanding = 0;
                                            Status _locStatus = Status.Open;

                                            CashAdvancePaymentPlan _cashAdvancePaymentPlan = _currSession.FindObject<CashAdvancePaymentPlan>
                                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("CashAdvanceMonitoring", _locPaymentRealizationMonitoring.CashAdvanceMonitoring)));
                                            if (_cashAdvancePaymentPlan != null)
                                            {
                                                _locAmountRealized = _locPaymentRealizationMonitoring.AmountRealized;
                                                _locOutstanding = _cashAdvancePaymentPlan.Amount - _locAmountRealized;
                                                if (_locOutstanding == 0)
                                                {
                                                    _locStatus = Status.Close;
                                                }
                                                else
                                                {
                                                    _locStatus = Status.Posted;
                                                }

                                                _cashAdvancePaymentPlan.AmountRealized = _cashAdvancePaymentPlan.AmountRealized + _locAmountRealized;
                                                _cashAdvancePaymentPlan.Outstanding = _locOutstanding;
                                                _cashAdvancePaymentPlan.Status = _locStatus;
                                                _cashAdvancePaymentPlan.StatusDate = now;
                                                _cashAdvancePaymentPlan.Save();
                                                _cashAdvancePaymentPlan.Session.CommitTransaction();
                                            }
                                        }
                                        
                                        #endregion CashAdvancePaymentPlan

                                        #region CashAdvanceMonitoring

                                        if(_locPaymentRealizationMonitoring.CashAdvanceMonitoring != null)
                                        {
                                            if(_locPaymentRealizationMonitoring.CashAdvanceMonitoring.Code != null)
                                            {
                                                CashAdvanceMonitoring _locCashAdvanceMonitoring = _currSession.FindObject<CashAdvanceMonitoring>
                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("Code", _locPaymentRealizationMonitoring.CashAdvanceMonitoring.Code)));
                                                if(_locCashAdvanceMonitoring != null)
                                                {
                                                    if(_locCashAdvanceMonitoring.AmountRealized == _locPaymentRealizationMonitoring.AmountRealized)
                                                    {
                                                        _locCashAdvanceMonitoring.Status = Status.Close;
                                                        _locCashAdvanceMonitoring.StatusDate = now;
                                                        _locCashAdvanceMonitoring.Save();
                                                        _locCashAdvanceMonitoring.Session.CommitTransaction();
                                                    }
                                                    else
                                                    {
                                                        _locCashAdvanceMonitoring.Status = Status.Posted;
                                                        _locCashAdvanceMonitoring.StatusDate = now;
                                                        _locCashAdvanceMonitoring.Save();
                                                        _locCashAdvanceMonitoring.Session.CommitTransaction();
                                                    }
                                                }
                                            }
                                        }

                                        #endregion CashAdvanceMonitoring
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PaymentRealization ", ex.ToString());
            }
        }

        private void SetJournalRealization(Session _currSession, PaymentRealization _locPaymentRealizationXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                double _locAmountRealized = 0;
                
                if (_locPaymentRealizationXPO != null)
                {
                    
                    XPCollection<PaymentRealizationLine> _locPaymentRealizationLines = new XPCollection<PaymentRealizationLine>
                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("PaymentRealization", _locPaymentRealizationXPO),
                                                                                    new BinaryOperator("Select", true),
                                                                                    new GroupOperator(GroupOperatorType.Or,
                                                                                    new BinaryOperator("Status", Status.Progress),
                                                                                    new BinaryOperator("Status", Status.Posted)
                                                                                    )));
                    if(_locPaymentRealizationLines != null && _locPaymentRealizationLines.Count() > 0)
                    {
                        foreach(PaymentRealizationLine _locPaymentRealizationLine in _locPaymentRealizationLines)
                        {
                            if(_locPaymentRealizationLine.AmountRealized > 0)
                            {
                                _locAmountRealized = _locPaymentRealizationLine.AmountRealized;

                                #region JournalMapByEmployee
                                if(_locPaymentRealizationXPO.Employee != null)
                                {
                                    OrganizationAccountGroup _locOrganizationAccountGroup = _currSession.FindObject<OrganizationAccountGroup>
                                                                                           (new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("Employee", _locPaymentRealizationXPO.Employee),
                                                                                            new BinaryOperator("Active", true)));
                                    if(_locOrganizationAccountGroup != null)
                                    {
                                        XPCollection<JournalMap> _locJournalMaps = new XPCollection<JournalMap>
                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("OrganizationAccountGroup", _locOrganizationAccountGroup)));

                                        if (_locJournalMaps != null && _locJournalMaps.Count() > 0)
                                        {
                                            foreach (JournalMap _locJournalMap in _locJournalMaps)
                                            {
                                                XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                    new BinaryOperator("JournalMap", _locJournalMap)));

                                                if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                {
                                                    foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                    {
                                                        AccountMap _locAccountMap = _currSession.FindObject<AccountMap>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("Code", _locJournalMapLine.AccountMap.Code),
                                                                                     new BinaryOperator("PostingType", PostingType.Advances),
                                                                                     new BinaryOperator("OrderType", OrderType.Account),
                                                                                     new BinaryOperator("PostingMethod", PostingMethod.CashRealization),
                                                                                     new BinaryOperator("PostingMethodType", PostingMethodType.Advances)));

                                                        if (_locAccountMap != null)
                                                        {
                                                            XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                new BinaryOperator("AccountMap", _locAccountMap),
                                                                                                                new BinaryOperator("Active", true)));

                                                            if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                            {
                                                                double _locTotalAmountDebit = 0;
                                                                double _locTotalAmountCredit = 0;
                                                                double _locTotalBalance = 0;

                                                                foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                {
                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                    {
                                                                        _locTotalAmountDebit = _locAmountRealized;
                                                                    }
                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                    {
                                                                        _locTotalAmountCredit = _locAmountRealized;
                                                                    }

                                                                    GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                    {
                                                                        PostingDate = now,
                                                                        PostingType = PostingType.Advances,
                                                                        PostingMethod = PostingMethod.CashRealization,
                                                                        Account = _locAccountMapLine.Account,
                                                                        Debit = _locTotalAmountDebit,
                                                                        Credit = _locTotalAmountCredit,
                                                                        Company = _locPaymentRealizationXPO.Company,
                                                                        PaymentRealization = _locPaymentRealizationXPO
                                                                    };
                                                                    _saveGeneralJournal.Save();
                                                                    _saveGeneralJournal.Session.CommitTransaction();

                                                                    if (_locAccountMapLine.Account.Code != null)
                                                                    {
                                                                        #region COA
                                                                        ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("Code", _locAccountMapLine.Account.Code)));

                                                                        if (_locCOA != null)
                                                                        {
                                                                            if (_locCOA.BalanceType == BalanceType.Change)
                                                                            {
                                                                                if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                                {
                                                                                    if (_locTotalAmountDebit > 0)
                                                                                    {
                                                                                        _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                                    }
                                                                                    if (_locTotalAmountCredit > 0)
                                                                                    {
                                                                                        _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                                    }
                                                                                }
                                                                                if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                                {
                                                                                    if (_locTotalAmountDebit > 0)
                                                                                    {
                                                                                        _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                                    }
                                                                                    if (_locTotalAmountCredit > 0)
                                                                                    {
                                                                                        _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                                    }
                                                                                }
                                                                            }

                                                                            _locCOA.Balance = _locTotalBalance;
                                                                            _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                            _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                            _locCOA.Save();
                                                                            _locCOA.Session.CommitTransaction();
                                                                        }
                                                                        #endregion COA
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                #endregion JournalMapByEmployee

                                #region JournalMapByCompany

                                if (_locPaymentRealizationLine.CashAdvanceType != null)
                                {
                                    if (_locPaymentRealizationLine.CashAdvanceType.AdvanceAccountGroup != null)
                                    {

                                        XPCollection<JournalMap> _locJournalMaps = new XPCollection<JournalMap>
                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("AdvanceAccountGroup", _locPaymentRealizationLine.CashAdvanceType.AdvanceAccountGroup)));

                                        if (_locJournalMaps != null && _locJournalMaps.Count() > 0)
                                        {
                                            foreach (JournalMap _locJournalMap in _locJournalMaps)
                                            {
                                                XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                    new BinaryOperator("JournalMap", _locJournalMap)));

                                                if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                {
                                                    foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                    {
                                                        AccountMap _locAccountMap = _currSession.FindObject<AccountMap>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("Code", _locJournalMapLine.AccountMap.Code),
                                                                                     new BinaryOperator("PostingType", PostingType.Advances),
                                                                                     new BinaryOperator("OrderType", OrderType.Account),
                                                                                     new BinaryOperator("PostingMethod", PostingMethod.CashRealization),
                                                                                     new BinaryOperator("PostingMethodType", PostingMethodType.Advances)));

                                                        if (_locAccountMap != null)
                                                        {
                                                            XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                new BinaryOperator("AccountMap", _locAccountMap),
                                                                                                                new BinaryOperator("Active", true)));

                                                            if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                            {
                                                                double _locTotalAmountDebit = 0;
                                                                double _locTotalAmountCredit = 0;
                                                                double _locTotalBalance = 0;

                                                                foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                {
                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                    {
                                                                        _locTotalAmountDebit = _locAmountRealized;
                                                                    }
                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                    {
                                                                        _locTotalAmountCredit = _locAmountRealized;
                                                                    }

                                                                    GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                    {
                                                                        PostingDate = now,
                                                                        PostingType = PostingType.Advances,
                                                                        PostingMethod = PostingMethod.CashRealization,
                                                                        Account = _locAccountMapLine.Account,
                                                                        Debit = _locTotalAmountDebit,
                                                                        Credit = _locTotalAmountCredit,
                                                                        Company = _locPaymentRealizationXPO.Company,
                                                                        PaymentRealization = _locPaymentRealizationXPO,
                                                                    };
                                                                    _saveGeneralJournal.Save();
                                                                    _saveGeneralJournal.Session.CommitTransaction();

                                                                    if (_locAccountMapLine.Account.Code != null)
                                                                    {
                                                                        #region COA
                                                                        ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("Code", _locAccountMapLine.Account.Code)));

                                                                        if (_locCOA != null)
                                                                        {
                                                                            if (_locCOA.BalanceType == BalanceType.Change)
                                                                            {
                                                                                if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                                {
                                                                                    if (_locTotalAmountDebit > 0)
                                                                                    {
                                                                                        _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                                    }
                                                                                    if (_locTotalAmountCredit > 0)
                                                                                    {
                                                                                        _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                                    }
                                                                                }
                                                                                if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                                {
                                                                                    if (_locTotalAmountDebit > 0)
                                                                                    {
                                                                                        _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                                    }
                                                                                    if (_locTotalAmountCredit > 0)
                                                                                    {
                                                                                        _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                                    }
                                                                                }
                                                                            }

                                                                            _locCOA.Balance = _locTotalBalance;
                                                                            _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                            _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                            _locCOA.Save();
                                                                            _locCOA.Session.CommitTransaction();
                                                                        }
                                                                        #endregion COA
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                #endregion JournalMapByCompany
                            }
                        }
                    } 
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PaymentRealization ", ex.ToString());
            }
        }

        private void SetProcessCount(Session _currSession, PaymentRealization _locPaymentRealizationXPO)
        {
            try
            {
                if (_locPaymentRealizationXPO != null)
                {
                    XPCollection<PaymentRealizationLine> _locPaymentRealizationLines = new XPCollection<PaymentRealizationLine>(_currSession,
                                                                         new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("PaymentRealization", _locPaymentRealizationXPO),
                                                                         new BinaryOperator("Select", true),
                                                                         new GroupOperator(GroupOperatorType.Or, 
                                                                         new BinaryOperator("Status", Status.Progress),
                                                                         new BinaryOperator("Status", Status.Posted))));

                    if (_locPaymentRealizationLines != null && _locPaymentRealizationLines.Count > 0)
                    {
                        foreach (PaymentRealizationLine _locPaymentRealizationLine in _locPaymentRealizationLines)
                        {
                            if (_locPaymentRealizationLine.AmountRealized > 0)
                            {
                                _locPaymentRealizationLine.ProcessCount = _locPaymentRealizationLine.ProcessCount + 1;
                                _locPaymentRealizationLine.Save();
                                _locPaymentRealizationLine.Session.CommitTransaction();
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PaymentRealization " + ex.ToString());
            }
        }

        private void SetStatusPaymentRealizationLine(Session _currSession, PaymentRealization _locPaymentRealizationXPO)
        {
            try
            {
                DateTime now = DateTime.Now;

                if (_locPaymentRealizationXPO != null)
                {
                    XPCollection<PaymentRealizationLine> _locPaymentRealizationLines = new XPCollection<PaymentRealizationLine>(_currSession,
                                                                         new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("PaymentRealization", _locPaymentRealizationXPO),
                                                                         new BinaryOperator("Select", true),
                                                                         new GroupOperator(GroupOperatorType.Or,
                                                                         new BinaryOperator("Status", Status.Progress),
                                                                         new BinaryOperator("Status", Status.Posted))));

                    if (_locPaymentRealizationLines != null && _locPaymentRealizationLines.Count > 0)
                    {
                        foreach (PaymentRealizationLine _locPaymentRealizationLine in _locPaymentRealizationLines)
                        {
                            _locPaymentRealizationLine.Status = Status.Close;
                            _locPaymentRealizationLine.ActivationPosting = true;
                            _locPaymentRealizationLine.Select = false;
                            _locPaymentRealizationLine.StatusDate = now;
                            _locPaymentRealizationLine.Save();
                            _locPaymentRealizationLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PaymentRealization " + ex.ToString());
            }
        }

        private void SetStatusPaymentRealization(Session _currSession, PaymentRealization _locPaymentRealizationXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locCount = 0;

                if (_locPaymentRealizationXPO != null)
                {
                    XPCollection<PaymentRealizationLine> _locPaymentRealizationLines = new XPCollection<PaymentRealizationLine>(_currSession,
                                                                         new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("PaymentRealization", _locPaymentRealizationXPO)));

                    if (_locPaymentRealizationLines != null && _locPaymentRealizationLines.Count() > 0)
                    {
                        foreach (PaymentRealizationLine _locPaymentRealizationLine in _locPaymentRealizationLines)
                        {
                            if (_locPaymentRealizationLine.Status == Status.Close)
                            {
                                _locCount = _locCount + 1;
                            }
                        }

                        if (_locCount == _locPaymentRealizationLines.Count())
                        {
                            _locPaymentRealizationXPO.ActivationPosting = true;
                            _locPaymentRealizationXPO.Status = Status.Close;
                            _locPaymentRealizationXPO.StatusDate = now;
                            _locPaymentRealizationXPO.Save();
                            _locPaymentRealizationXPO.Session.CommitTransaction();
                        }
                        else
                        {
                            _locPaymentRealizationXPO.ActivationPosting = true;
                            _locPaymentRealizationXPO.Status = Status.Close;
                            _locPaymentRealizationXPO.StatusDate = now;
                            _locPaymentRealizationXPO.Save();
                            _locPaymentRealizationXPO.Session.CommitTransaction();
                        }

                        //SetCloseAllCAProcess(_currSession, _locPaymentRealizationXPO);
                        SuccessMessageShow(_locPaymentRealizationXPO.Code + " has been change successfully to Close");
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PaymentRealization " + ex.ToString());
            }
        }

        private void SetCloseAllCAProcess(Session _currSession, PaymentRealization _locPaymentRealizationXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                double _locMaxAmountRealized = 0;
                
                if(_locPaymentRealizationXPO != null)
                {
                    #region ClosePaymentRealizationMonitoringAndCashAdvancePaymentPlan

                    XPCollection<PaymentRealizationLine> _locPaymentRealizationLines = new XPCollection<PaymentRealizationLine>
                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("PaymentRealization", _locPaymentRealizationXPO),
                                                                                            new BinaryOperator("Status", Status.Close)));

                    if(_locPaymentRealizationLines != null && _locPaymentRealizationLines.Count() > 0)
                    {
                        foreach(PaymentRealizationLine _locPaymentRealizationLine in _locPaymentRealizationLines)
                        {
                            if(_locPaymentRealizationLine.CashAdvanceMonitoring != null)
                            {
                                double _locAmountRealizedPRM = 0;
                                double _locAmountRealizedCAPP = 0;
                                XPCollection<PaymentRealizationMonitoring> _locPaymentRealizationMonitorings = new XPCollection<PaymentRealizationMonitoring>
                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("CashAdvanceMonitoring", _locPaymentRealizationLine.CashAdvanceMonitoring)));

                                if (_locPaymentRealizationMonitorings != null && _locPaymentRealizationMonitorings.Count() > 0)
                                {
                                    foreach (PaymentRealizationMonitoring _locPaymentRealizationMonitoring in _locPaymentRealizationMonitorings)
                                    {

                                        _locAmountRealizedPRM = _locAmountRealizedPRM + _locPaymentRealizationMonitoring.AmountRealized; 
                                    }
                                }

                                XPCollection<CashAdvancePaymentPlan> _locCashAdvancePayments = new XPCollection<CashAdvancePaymentPlan>
                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("CashAdvanceMonitoring", _locPaymentRealizationLine.CashAdvanceMonitoring)));
                                if(_locCashAdvancePayments != null && _locCashAdvancePayments.Count() > 0)
                                {
                                    foreach(CashAdvancePaymentPlan _locCashAdvancePayment in _locCashAdvancePayments)
                                    {
                                        _locAmountRealizedCAPP = _locAmountRealizedCAPP + _locCashAdvancePayment.AmountRealized;
                                    }
                                }

                                if(_locAmountRealizedPRM == _locAmountRealizedCAPP)
                                {
                                    XPCollection<PaymentRealizationMonitoring> _locPaymentRealizationMonitoring2s = new XPCollection<PaymentRealizationMonitoring>
                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("CashAdvanceMonitoring", _locPaymentRealizationLine.CashAdvanceMonitoring)));

                                    if (_locPaymentRealizationMonitoring2s != null && _locPaymentRealizationMonitoring2s.Count() > 0)
                                    {
                                        foreach (PaymentRealizationMonitoring _locPaymentRealizationMonitoring2 in _locPaymentRealizationMonitoring2s)
                                        {

                                            _locPaymentRealizationMonitoring2.Status = Status.Close;
                                            _locPaymentRealizationMonitoring2.StatusDate = now;
                                            _locPaymentRealizationMonitoring2.Save();
                                            _locPaymentRealizationMonitoring2.Session.CommitTransaction();
                                        }
                                    }

                                    XPCollection<CashAdvancePaymentPlan> _locCashAdvancePayment2s = new XPCollection<CashAdvancePaymentPlan>
                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("CashAdvanceMonitoring", _locPaymentRealizationLine.CashAdvanceMonitoring)));
                                    if (_locCashAdvancePayment2s != null && _locCashAdvancePayment2s.Count() > 0)
                                    {
                                        foreach (CashAdvancePaymentPlan _locCashAdvancePayment2 in _locCashAdvancePayment2s)
                                        {
                                            _locCashAdvancePayment2.Status = Status.Close;
                                            _locCashAdvancePayment2.StatusDate = now;
                                            _locCashAdvancePayment2.Save();
                                            _locCashAdvancePayment2.Session.CommitTransaction();
                                        }
                                    }
                                }
                            }
                        }
                    }


                    #endregion ClosePaymentRealizationMonitoringAndCashAdvancePaymentPlan

                    #region CloseCashAdvance
                    XPQuery<PaymentRealizationMonitoring> _paymentRealizationMonitoringQuerys = new XPQuery<PaymentRealizationMonitoring>(_currSession);

                    var _paymentRealizationMonitorings = from rtl in _paymentRealizationMonitoringQuerys
                                                      where (rtl.PaymentRealization == _locPaymentRealizationXPO )
                                                      group rtl by rtl.CashAdvance into g
                                                      select new { CashAdvance = g.Key };

                    if(_paymentRealizationMonitorings != null && _paymentRealizationMonitorings.Count() > 0)
                    {
                        foreach(var _paymentRealizationMonitoring in _paymentRealizationMonitorings)
                        {
                            
                            
                            if (_paymentRealizationMonitoring.CashAdvance.Code != null)
                            {
                                CashAdvance _locCashAdvance = _currSession.FindObject<CashAdvance>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Code", _paymentRealizationMonitoring.CashAdvance.Code)));
                                if(_locCashAdvance != null)
                                {
                                    XPCollection<CashAdvancePaymentPlan> _locCashAdvancePaymentPlans = new XPCollection<CashAdvancePaymentPlan>
                                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("CashAdvance", _locCashAdvance)));
                                    if (_locCashAdvancePaymentPlans != null && _locCashAdvancePaymentPlans.Count() > 0)
                                    {
                                        foreach (CashAdvancePaymentPlan _locCashAdvancePaymentPlan in _locCashAdvancePaymentPlans)
                                        {
                                            _locMaxAmountRealized = _locMaxAmountRealized + _locCashAdvancePaymentPlan.AmountRealized;
                                        }
                                    }

                                    if(_locMaxAmountRealized == _locCashAdvance.AmountRealized)
                                    {
                                        _locCashAdvance.Status = Status.Close;
                                        _locCashAdvance.StatusDate = now;
                                        _locCashAdvance.Save();
                                        _locCashAdvance.Session.CommitTransaction();
                                    }
                                }
                            }
                            _locMaxAmountRealized = 0; 
                        }
                    }
                    #endregion CloseCashAdvance   
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PaymentRealization " + ex.ToString());
            }
        }

        #endregion PostingForPaymentRealization

        #region Approval Process

        private void SetApprovalLine(Session _currentSession, PaymentRealization _locPaymentRealizationXPO, ApprovalLevel _locApprovalLevel)
        {
            try
            {
                DateTime now = DateTime.Now;

                ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("PaymentRealization", _locPaymentRealizationXPO),
                                                                         new BinaryOperator("ApprovalLevel", _locApprovalLevel)));

                if (_locApprovalLineXPO == null)
                {
                    ApprovalLine _saveDataAL2 = new ApprovalLine(_currentSession)
                    {
                        ApprovalDate = now,
                        ApprovalStatus = Status.Approved,
                        ApprovalLevel = _locApprovalLevel,
                        PaymentRealization = _locPaymentRealizationXPO,
                    };
                    _saveDataAL2.Save();
                    _saveDataAL2.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PaymentRealization " + ex.ToString());
            }
        }

        #endregion Approval Process

        #region Email

        private string BackgroundBody(Session _currentSession, PaymentRealization _locPaymentRealizationXPO, MailSetupDetail _locMailSetDetail)
        {
            string body = string.Empty;

            if (_locPaymentRealizationXPO != null)
            {
                ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                               (new GroupOperator(GroupOperatorType.And,
                                                new BinaryOperator("PaymentRealization", _locPaymentRealizationXPO)));

                string Status = _locApprovalLineXPO.ApprovalStatus.ToString();

                using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Email/EmailTemplate.html")))
                {
                    body = reader.ReadToEnd();
                }
                body = body.Replace("{Code}", _locPaymentRealizationXPO.Code);

                #region Level
                if (_locPaymentRealizationXPO.ActiveApproved1 == true)
                {
                    string Active = "Level 1";
                    body = body.Replace("{Level}", Active);
                }
                else if (_locPaymentRealizationXPO.ActiveApproved2 == true)
                {
                    string Active = "Level 2";
                    body = body.Replace("{Level}", Active);
                }
                else if (_locPaymentRealizationXPO.ActiveApproved3 == true)
                {
                    string Active = "Level 3";
                    body = body.Replace("{Level}", Active);
                }
                else
                {
                    string Active = "Not Available";
                    body = body.Replace("{Level}", Active);
                }
                #endregion Level

                body = body.Replace("{Status}", Status);
                body = body.Replace("{Message}", _locMailSetDetail.MessageBody);
            }
            return body;
        }

        protected void SendEmail(Session _currentSession, PaymentRealization _locPaymentRealizationXPO, Status _locStatus, ApprovalLine _locApprovalLineXPO2)
        {
            DateTime now = DateTime.Now;
            var User = SecuritySystem.CurrentUserName;
            GlobalFunction _globFunc = new GlobalFunction();
            UserAccess _numLineUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", User));
            string _locMessageBody = null;

            try
            {
                if (_locPaymentRealizationXPO != null)
                {
                    XPCollection<MailSetupDetail> _locMailSetDetails = new XPCollection<MailSetupDetail>
                                                                       (_currentSession, new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("ObjectList", ObjectList.PaymentRealization),
                                                                        new BinaryOperator("UserAccess", _numLineUserAccess),
                                                                        new BinaryOperator("Active", true)));

                    if (_locMailSetDetails != null && _locMailSetDetails.Count > 0)
                    {
                        foreach (MailSetupDetail _locMailSetDetail in _locMailSetDetails)
                        {
                            if (_locMailSetDetail.MessageBody != null)
                            {
                                if (_locApprovalLineXPO2.EndApproval == true)
                                {
                                    _locMessageBody = this.BackgroundBody(_currentSession, _locPaymentRealizationXPO, _locMailSetDetail);
                                }
                                else
                                {
                                    _locMessageBody = this.BackgroundBody(_currentSession, _locPaymentRealizationXPO, _locMailSetDetail);
                                }
                            }

                            _globFunc.SetAndSendMail(_locMailSetDetail.SmtpHost, _locMailSetDetail.SmtpPort,
                            _locMailSetDetail.MailFrom, _locMailSetDetail.MailFromPassword, _locMailSetDetail.MailTo, _locMailSetDetail.MailSubject, _locMessageBody);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PaymentRealization " + ex.ToString());
            }
        }

        #endregion Email

        #region GlobalMethod

        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0} !", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }


        #endregion GlobalMethod

        
    }
}
