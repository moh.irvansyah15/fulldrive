﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using FullDrive.Module.BusinessObjects;
using DevExpress.ExpressApp.Xpo;
using FullDrive.Module.CustomProcess;
using System.Collections;
using DevExpress.Xpo;

using System.Web;
using System.IO;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class RoutingLineActionController : ViewController
    {
        public RoutingLineActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void RoutingLineSelectAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        RoutingLine _locRoutingLineOS = (RoutingLine)_objectSpace.GetObject(obj);

                        if (_locRoutingLineOS != null)
                        {
                            if (_locRoutingLineOS.Code != null)
                            {
                                _currObjectId = _locRoutingLineOS.Code;

                                XPCollection<RoutingLine> _locRoutingLines = new XPCollection<RoutingLine>
                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Code", _currObjectId),
                                                                            new GroupOperator(GroupOperatorType.Or,
                                                                            new BinaryOperator("Status", Status.Delivered),
                                                                            new BinaryOperator("Status", Status.Progress),
                                                                            new BinaryOperator("Status", Status.Open)
                                                                            )));

                                if (_locRoutingLines != null && _locRoutingLines.Count > 0)
                                {
                                    foreach (RoutingLine _locRoutingLine in _locRoutingLines)
                                    {
                                        _locRoutingLine.Select = true;
                                        _locRoutingLine.Save();
                                        _locRoutingLine.Session.CommitTransaction();
                                    }

                                    SuccessMessageShow("RoutingLine has been Select");
                                }
                                else
                                {
                                    ErrorMessageShow("Data RoutingLine Not Available");
                                }
                            }
                        }
                    }
                    if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                    {
                        _objectSpace.CommitChanges();
                        _objectSpace.Refresh();
                    }
                    if (View is ListView)
                    {
                        _objectSpace.CommitChanges();
                        View.ObjectSpace.Refresh();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = RoutingLine " + ex.ToString());
            }
        }

        private void RoutingLineUnselectAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        RoutingLine _locRoutingLineOS = (RoutingLine)_objectSpace.GetObject(obj);

                        if (_locRoutingLineOS != null)
                        {
                            if (_locRoutingLineOS.Code != null)
                            {
                                _currObjectId = _locRoutingLineOS.Code;

                                XPCollection<RoutingLine> _locRoutingLines = new XPCollection<RoutingLine>
                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Code", _currObjectId),
                                                                            new GroupOperator(GroupOperatorType.Or,
                                                                            new BinaryOperator("Status", Status.Delivered),
                                                                            new BinaryOperator("Status", Status.Progress),
                                                                            new BinaryOperator("Status", Status.Open)
                                                                            )));

                                if (_locRoutingLines != null && _locRoutingLines.Count > 0)
                                {
                                    foreach (RoutingLine _locRoutingLine in _locRoutingLines)
                                    {
                                        _locRoutingLine.Select = false;
                                        _locRoutingLine.Save();
                                        _locRoutingLine.Session.CommitTransaction();
                                    }

                                    SuccessMessageShow("RoutingLine has been Unselect");
                                }
                                else
                                {
                                    ErrorMessageShow("Data RoutingLine Not Available");
                                }
                            }
                        }
                    }
                    if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                    {
                        _objectSpace.CommitChanges();
                        _objectSpace.Refresh();
                    }
                    if (View is ListView)
                    {
                        _objectSpace.CommitChanges();
                        View.ObjectSpace.Refresh();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = RoutingLine " + ex.ToString());
            }
        }

        private void RoutingLineDeliveryAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                
                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        RoutingLine _locRoutingLineOS = (RoutingLine)_objectSpace.GetObject(obj);

                        if (_locRoutingLineOS != null)
                        {
                            if (_locRoutingLineOS.Code != null)
                            {
                                _currObjectId = _locRoutingLineOS.Code;

                                RoutingLine _locRoutingLineXPO = _currSession.FindObject<RoutingLine>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId),
                                                                     new BinaryOperator("Select", true)));

                                if (_locRoutingLineXPO != null)
                                {
                                    if ( _locRoutingLineXPO.Status == Status.Progress )
                                    {
                                        _locRoutingLineXPO.Status = Status.Delivered;
                                        _locRoutingLineXPO.StatusDate = now;
                                        _locRoutingLineXPO.Save();
                                        _locRoutingLineXPO.Session.CommitTransaction();
                                        
                                        SuccessMessageShow(_locRoutingLineXPO.Code + " has been change successfully to Delivered");
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data RoutingLine Not Available");
                                }

                            }
                            else
                            {
                                ErrorMessageShow("Data RoutingLine Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = RoutingLine " + ex.ToString());
            }
        }

        //================================================ Code Only =========================================

        #region Global Method

        private void SuccessMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 4000;
            options.Message = string.Format("{0}", _locMessage);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }


        #endregion Global Method

        
    }
}
