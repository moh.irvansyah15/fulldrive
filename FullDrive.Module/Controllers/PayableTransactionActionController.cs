﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using System.Collections;
using DevExpress.Xpo;
using DevExpress.ExpressApp.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class PayableTransactionActionController : ViewController
    {
        private ChoiceActionItem _selectionListviewFilter;

        public PayableTransactionActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            PayableTransactionListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                PayableTransactionListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
        }

        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }

        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }

        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void PayableTransactionProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        PayableTransaction _locPayableTransactionOS = (PayableTransaction)_objectSpace.GetObject(obj);

                        if (_locPayableTransactionOS != null)
                        {
                            if (_locPayableTransactionOS.Code != null)
                            {
                                _currObjectId = _locPayableTransactionOS.Code;

                                PayableTransaction _locPayableTransactionXPO = _currSession.FindObject<PayableTransaction>
                                                                                 (new GroupOperator(GroupOperatorType.And,
                                                                                  new BinaryOperator("Code", _currObjectId)));

                                if (_locPayableTransactionXPO != null)
                                {
                                    if (_locPayableTransactionXPO.Status == Status.Open)
                                    {
                                        _locPayableTransactionXPO.Status = Status.Progress;
                                        _locPayableTransactionXPO.StatusDate = now;
                                        _locPayableTransactionXPO.Save();
                                        _locPayableTransactionXPO.Session.CommitTransaction();

                                        XPCollection<PayableTransactionLine> _locPayableTransactionLines = new XPCollection<PayableTransactionLine>
                                                                                                                 (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                  new BinaryOperator("PayableTransaction", _locPayableTransactionXPO)));

                                        if (_locPayableTransactionLines != null && _locPayableTransactionLines.Count > 0)
                                        {
                                            foreach (PayableTransactionLine _locPayableTransactionLine in _locPayableTransactionLines)
                                            {
                                                if (_locPayableTransactionLine.Status == Status.Open)
                                                {
                                                    _locPayableTransactionLine.Status = Status.Progress;
                                                    _locPayableTransactionLine.StatusDate = now;
                                                    _locPayableTransactionLine.Save();
                                                    _locPayableTransactionLine.Session.CommitTransaction();
                                                }
                                            }
                                        }

                                        XPCollection<PayablePurchaseCollection> _locPayableTransactionCollections = new XPCollection<PayablePurchaseCollection>
                                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                            new BinaryOperator("PayableTransaction", _locPayableTransactionXPO)));
                                        if (_locPayableTransactionCollections != null && _locPayableTransactionCollections.Count() > 0)
                                        {
                                            foreach (PayablePurchaseCollection _locPayableTransactionCollection in _locPayableTransactionCollections)
                                            {
                                                if (_locPayableTransactionCollection.Status == Status.Open)
                                                {
                                                    _locPayableTransactionCollection.Status = Status.Progress;
                                                    _locPayableTransactionCollection.StatusDate = now;
                                                    _locPayableTransactionCollection.Save();
                                                    _locPayableTransactionCollection.Session.CommitTransaction();
                                                }
                                            }
                                        }
                                        SuccessMessageShow(_locPayableTransactionXPO.Code + "Has been change successfully to Progress");
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data Payable Transaction not available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Payable Transaction not available");
                            }
                        }
                    }
                }
                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business = PayableTransaction " + ex.ToString());
            }
        }

        private void PayableTransactionPostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        PayableTransaction _locPayableTransactionOS = (PayableTransaction)_objectSpace.GetObject(obj);

                        if (_locPayableTransactionOS != null)
                        {
                            if (_locPayableTransactionOS.Code != null)
                            {
                                _currObjectId = _locPayableTransactionOS.Code;

                                PayableTransaction _locPayableTransactionXPO = _currSession.FindObject<PayableTransaction>
                                                                                     (new GroupOperator(GroupOperatorType.And,
                                                                                      new BinaryOperator("Code", _currObjectId),
                                                                                      new GroupOperator(GroupOperatorType.Or,
                                                                                      new BinaryOperator("Status", Status.Progress),
                                                                                      new BinaryOperator("Status", Status.Posted))));
                                if (_locPayableTransactionXPO != null)
                                {
                                    if (_locPayableTransactionXPO.PaymentMethodType == PaymentMethodType.Normal)
                                    {
                                        if (CheckMaksAmountDebitBasedPurchaseInvoiceMonitoring(_currSession, _locPayableTransactionXPO) == true)
                                        {
                                            if (CheckAmountDebitCreditBasedPurchaseInvoiceMonitoring(_currSession, _locPayableTransactionXPO) == true)
                                            {
                                                SetPayableTransactionMonitoringBasedPurchaseInvoiceMonitoring(_currSession, _locPayableTransactionXPO);
                                                SetPayableJournalBasedPurchaseInvoiceMonitoring(_currSession, _locPayableTransactionXPO);
                                                SetNormalPurchaseInvoiceMonitoring(_currSession, _locPayableTransactionXPO);
                                                SetStatusPayableTransactionLine(_currSession, _locPayableTransactionXPO);
                                                SetFinalStatusPayableTransaction(_currSession, _locPayableTransactionXPO);
                                                SetCloseAllPurchaseProcess(_currSession, _locPayableTransactionXPO);
                                                SuccessMessageShow(_locPayableTransactionXPO.Code + " has been change successfully to Progress");
                                            }
                                            else
                                            {
                                                ErrorMessageShow("Please Check Amount Transaction");
                                            }
                                        }
                                        else
                                        {
                                            ErrorMessageShow("Please Check Amount Transaction");
                                        }
                                    }
                                    else if (_locPayableTransactionXPO.PaymentMethodType == PaymentMethodType.DownPayment || _locPayableTransactionXPO.PaymentMethodType == PaymentMethodType.CashBeforeDelivery)
                                    {
                                        if (CheckMaksAmountDebitBasedPurchasePrePaymentInvoice(_currSession, _locPayableTransactionXPO) == true)
                                        {
                                            if (CheckAmountDebitCreditBasedPurchasePrePaymentInvoice(_currSession, _locPayableTransactionXPO) == true)
                                            {
                                                SetPayableTransactionMonitoringBasedPurchasePrePaymentInvoice(_currSession, _locPayableTransactionXPO);
                                                SetPayableJournalBasedPurchasePrePaymentInvoice(_currSession, _locPayableTransactionXPO);
                                                SetNormalPurchaseInvoiceMonitoring(_currSession, _locPayableTransactionXPO);
                                                SetStatusPayableTransactionLine(_currSession, _locPayableTransactionXPO);
                                                SetFinalStatusPayableTransaction(_currSession, _locPayableTransactionXPO);
                                                SetCloseAllPurchaseProcess(_currSession, _locPayableTransactionXPO);
                                                SuccessMessageShow(_locPayableTransactionXPO.Code + " has been change successfully to Progress");
                                            }
                                            else
                                            {
                                                ErrorMessageShow("Please Check Amount Transaction");
                                            }
                                        }
                                        else
                                        {
                                            ErrorMessageShow("Please Check Amount Transaction");
                                        }
                                    }

                                }
                                else
                                {
                                    ErrorMessageShow("Data Payable Transaction Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Payable Transaction Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PayableTransaction " + ex.ToString());
            }
        }

        private void PayableTransactionGetPIAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;


                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        PayableTransaction _locPayableTransactionOS = (PayableTransaction)_objectSpace.GetObject(obj);

                        if (_locPayableTransactionOS != null)
                        {
                            if (_locPayableTransactionOS.Code != null)
                            {
                                _currObjectId = _locPayableTransactionOS.Code;

                                PayableTransaction _locPayableTransactionXPO = _currSession.FindObject<PayableTransaction>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locPayableTransactionXPO != null)
                                {
                                    if (_locPayableTransactionXPO.Status == Status.Open || _locPayableTransactionXPO.Status == Status.Progress)
                                    {
                                        XPCollection<PayablePurchaseCollection> _locPayablePurchaseCollections = new XPCollection<PayablePurchaseCollection>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("PayableTransaction", _locPayableTransactionXPO),
                                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("Status", Status.Open),
                                                                                        new BinaryOperator("Status", Status.Progress))));

                                        if (_locPayablePurchaseCollections != null && _locPayablePurchaseCollections.Count() > 0)
                                        {
                                            foreach (PayablePurchaseCollection _locPayablePurchaseCollection in _locPayablePurchaseCollections)
                                            {
                                                if (_locPayablePurchaseCollection.PayableTransaction != null && _locPayablePurchaseCollection.PurchaseInvoice != null)
                                                {
                                                    GetPurchaseInvoiceMonitoring(_currSession, _locPayablePurchaseCollection.PurchaseInvoice, _locPayableTransactionXPO);
                                                }
                                            }
                                            SuccessMessageShow("Purchase Invoice Has Been Successfully Getting into Payable Transaction");
                                        }
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Purchase Invoice Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Purchase Invoice Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PayableTransaction " + ex.ToString());
            }
        }

        private void PayableTransactionGetPPIAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;


                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        PayableTransaction _locPayableTransactionOS = (PayableTransaction)_objectSpace.GetObject(obj);

                        if (_locPayableTransactionOS != null)
                        {
                            if (_locPayableTransactionOS.Code != null)
                            {
                                _currObjectId = _locPayableTransactionOS.Code;

                                PayableTransaction _locPayableTransactionXPO = _currSession.FindObject<PayableTransaction>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locPayableTransactionXPO != null)
                                {
                                    if (_locPayableTransactionXPO.Status == Status.Open || _locPayableTransactionXPO.Status == Status.Progress)
                                    {
                                        XPCollection<PayablePurchaseCollection> _locPayablePurchaseCollections = new XPCollection<PayablePurchaseCollection>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("PayableTransaction", _locPayableTransactionXPO),
                                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("Status", Status.Open),
                                                                                        new BinaryOperator("Status", Status.Progress))));

                                        if (_locPayablePurchaseCollections != null && _locPayablePurchaseCollections.Count() > 0)
                                        {
                                            foreach (PayablePurchaseCollection _locPayablePurchaseCollection in _locPayablePurchaseCollections)
                                            {
                                                if (_locPayablePurchaseCollection.PayableTransaction != null && _locPayablePurchaseCollection.PurchasePrePaymentInvoice != null)
                                                {
                                                    GetPurchasePrePaymentInvoice(_currSession, _locPayablePurchaseCollection.PurchasePrePaymentInvoice, _locPayableTransactionXPO);
                                                }
                                            }
                                            SuccessMessageShow("Purchase Pre Payment Invoice Has Been Successfully Getting into Payable Transaction");
                                        }
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Purchase Pre Payment Invoice Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Purchase Pre Payment Invoice Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PayableTransaction " + ex.ToString());
            }
        }

        private void PayableTransactionListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(PayableTransaction)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseInvoice " + ex.ToString());
            }
        }

        //============================================ Code Only ==============================================

        #region GetPI

        private void GetPurchaseInvoiceMonitoring(Session _currSession, PurchaseInvoice _locPurchaseInvoiceXPO, PayableTransaction _locPayableTransactionXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                ChartOfAccount _locAccountForCompany = null;
                ChartOfAccount _locAccountForVendor = null;
                if (_locPurchaseInvoiceXPO != null && _locPayableTransactionXPO != null)
                {
                    //Cek 
                    //Harus mampu menghitung otomatis buat MultiTax dan MultiDiscount

                    XPCollection<PurchaseInvoiceMonitoring> _locPurchaseInvoiceMonitorings = new XPCollection<PurchaseInvoiceMonitoring>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO),
                                                                                        new BinaryOperator("PayableTransaction", _locPayableTransactionXPO),
                                                                                        new BinaryOperator("Select", true),
                                                                                        new BinaryOperator("PostedCount", 0)));

                    if (_locPurchaseInvoiceMonitorings != null && _locPurchaseInvoiceMonitorings.Count() > 0)
                    {
                        //Cek Total PIP dan Purchase Order Maks Pay
                        foreach (PurchaseInvoiceMonitoring _locPurchaseInvoiceMonitoring in _locPurchaseInvoiceMonitorings)
                        {
                            if (_locPurchaseInvoiceMonitoring.PurchaseOrderMonitoring != null
                                && _locPurchaseInvoiceMonitoring.PurchaseOrderMonitoring.PurchaseOrder != null)
                            {
                                #region Company
                                if (_locPurchaseInvoiceMonitoring.PurchaseInvoice.BankAccountCompany != null && _locPurchaseInvoiceMonitoring.PurchaseInvoice.Company != null)
                                {
                                    _locAccountForCompany = GetAccountFromBankAccountForCompany(_currSession, _locPurchaseInvoiceMonitoring.PurchaseInvoice.BankAccountCompany, _locPurchaseInvoiceMonitoring.PurchaseInvoice.Company);
                                }

                                PayableTransactionLine _saveDataPayableTransactionLine1 = new PayableTransactionLine(_currSession)
                                {
                                    PostingDate = now,
                                    PurchaseType = PostingType.Purchase,
                                    PostingMethod = PostingMethod.Payment,
                                    OpenCompany = true,
                                    Company = _locPurchaseInvoiceMonitoring.PurchaseInvoice.Company,
                                    BankAccount = _locPurchaseInvoiceMonitoring.PurchaseInvoice.BankAccountCompany,
                                    AccountNo = _locPurchaseInvoiceMonitoring.PurchaseInvoice.CompanyAccountNo,
                                    AccountName = _locPurchaseInvoiceMonitoring.PurchaseInvoice.CompanyAccountName,
                                    Account = _locAccountForCompany,
                                    Debit = _locPurchaseInvoiceMonitoring.Pay,
                                    CloseCredit = true,
                                    PurchaseInvoice = _locPurchaseInvoiceMonitoring.PurchaseInvoice,
                                    PurchaseOrder = _locPurchaseInvoiceMonitoring.PurchaseOrderMonitoring.PurchaseOrder,
                                    PurchaseInvoiceMonitoring = _locPurchaseInvoiceMonitoring,
                                    PayableTransaction = _locPayableTransactionXPO,
                                };
                                _saveDataPayableTransactionLine1.Save();
                                _saveDataPayableTransactionLine1.Session.CommitTransaction();

                                #endregion Company

                                #region Vendor 
                                if (_locPurchaseInvoiceMonitoring.PurchaseInvoice.BankAccount != null && _locPurchaseInvoiceMonitoring.PurchaseInvoice.BuyFromVendor != null)
                                {
                                    _locAccountForVendor = GetAccountFromBankAccountForVendor(_currSession, _locPurchaseInvoiceMonitoring.PurchaseInvoice.BankAccount, _locPurchaseInvoiceMonitoring.PurchaseInvoice.BuyFromVendor);
                                }
                                PayableTransactionLine _saveDataPayableTransactionLine2 = new PayableTransactionLine(_currSession)
                                {
                                    PostingDate = now,
                                    PurchaseType = PostingType.Purchase,
                                    PostingMethod = PostingMethod.Payment,
                                    OpenVendor = true,
                                    Vendor = _locPurchaseInvoiceMonitoring.PurchaseInvoice.PayToVendor,
                                    BankAccount = _locPurchaseInvoiceMonitoring.PurchaseInvoice.BankAccount,
                                    AccountNo = _locPurchaseInvoiceMonitoring.PurchaseInvoice.AccountNo,
                                    AccountName = _locPurchaseInvoiceMonitoring.PurchaseInvoice.AccountName,
                                    Account = _locAccountForVendor,
                                    Credit = _locPurchaseInvoiceMonitoring.Pay,
                                    CloseDebit = true,
                                    PurchaseInvoice = _locPurchaseInvoiceMonitoring.PurchaseInvoice,
                                    PurchaseOrder = _locPurchaseInvoiceMonitoring.PurchaseOrderMonitoring.PurchaseOrder,
                                    PurchaseInvoiceMonitoring = _locPurchaseInvoiceMonitoring,
                                    PayableTransaction = _locPayableTransactionXPO,
                                };
                                _saveDataPayableTransactionLine2.Save();
                                _saveDataPayableTransactionLine2.Session.CommitTransaction();
                                #endregion Vendor

                                _locPurchaseInvoiceMonitoring.Status = Status.Posted;
                                _locPurchaseInvoiceMonitoring.StatusDate = now;
                                _locPurchaseInvoiceMonitoring.PostedCount = _locPurchaseInvoiceMonitoring.PostedCount + 1;
                                _locPurchaseInvoiceMonitoring.Save();
                                _locPurchaseInvoiceMonitoring.Session.CommitTransaction();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PayableTransaction ", ex.ToString());
            }
        }

        private ChartOfAccount GetAccountFromBankAccountForCompany(Session _currSession, BankAccount _locBankAccountXPO, Company _locCompanyXPO)
        {
            ChartOfAccount _result = null;

            try
            {
                if (_locBankAccountXPO != null && _locCompanyXPO != null)
                {
                    #region JournalMapBankAccountGroupWithCompany
                    if (_locBankAccountXPO.BankAccountGroup != null)
                    {
                        XPCollection<JournalMap> _locJournalMaps = new XPCollection<JournalMap>
                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                               new BinaryOperator("BankAccountGroup", _locBankAccountXPO.BankAccountGroup)));

                        if (_locJournalMaps != null && _locJournalMaps.Count() > 0)
                        {
                            foreach (JournalMap _locJournalMap in _locJournalMaps)
                            {
                                XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("JournalMap", _locJournalMap)));

                                if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                {
                                    foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                    {
                                        AccountMap _locAccountMap = _currSession.FindObject<AccountMap>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _locJournalMapLine.AccountMap.Code),
                                                                     new BinaryOperator("PostingType", PostingType.Purchase),
                                                                     new BinaryOperator("PostingMethod", PostingMethod.Payment),
                                                                     new BinaryOperator("PostingMethodType", PostingMethodType.Normal)));

                                        if (_locAccountMap != null)
                                        {
                                            XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("AccountMap", _locAccountMap),
                                                                                                new BinaryOperator("Active", true)));

                                            if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                            {
                                                foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                {
                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                    {
                                                        _result = _locAccountMapLine.Account;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    #endregion JournalMapBankAccountGroupWithCompany
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PayableTransaction ", ex.ToString());
            }

            return _result;
        }

        private ChartOfAccount GetAccountFromBankAccountForVendor(Session _currSession, BankAccount _locBankAccountXPO, BusinessPartner _locVendorXPO)
        {
            ChartOfAccount _result = null;

            try
            {
                if (_locBankAccountXPO != null && _locVendorXPO != null)
                {
                    #region JournalMapBankAccountGroupWithVendor
                    if (_locBankAccountXPO.BankAccountGroup != null)
                    {
                        XPCollection<JournalMap> _locJournalMaps = new XPCollection<JournalMap>
                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                               new BinaryOperator("BankAccountGroup", _locBankAccountXPO.BankAccountGroup)));

                        if (_locJournalMaps != null && _locJournalMaps.Count() > 0)
                        {
                            foreach (JournalMap _locJournalMap in _locJournalMaps)
                            {
                                XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("JournalMap", _locJournalMap)));

                                if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                {
                                    foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                    {
                                        AccountMap _locAccountMap = _currSession.FindObject<AccountMap>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _locJournalMapLine.AccountMap.Code),
                                                                     new BinaryOperator("PostingType", PostingType.Purchase),
                                                                     new BinaryOperator("PostingMethod", PostingMethod.Payment),
                                                                     new BinaryOperator("PostingMethodType", PostingMethodType.Normal)));

                                        if (_locAccountMap != null)
                                        {
                                            XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("AccountMap", _locAccountMap),
                                                                                                new BinaryOperator("Active", true)));

                                            if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                            {
                                                foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                {
                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                    {
                                                        _result = _locAccountMapLine.Account;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    #endregion JournalMapBankAccountGroupWithVendor
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PayableTransaction ", ex.ToString());
            }

            return _result;
        }

        #endregion GetPI

        #region GetPPI

        private void GetPurchasePrePaymentInvoice(Session _currSession, PurchasePrePaymentInvoice _locPurchasePrePaymentInvoiceXPO, PayableTransaction _locPayableTransactionXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                ChartOfAccount _locAccountForCompany = null;
                ChartOfAccount _locAccountForVendor = null;
                if (_locPurchasePrePaymentInvoiceXPO != null && _locPayableTransactionXPO != null)
                {
                    if (_locPurchasePrePaymentInvoiceXPO.Code != null)
                    {
                        XPCollection<PurchasePrePaymentInvoice> _locPurchasePrePaymentInvoices = new XPCollection<PurchasePrePaymentInvoice>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("Code", _locPurchasePrePaymentInvoiceXPO.Code),
                                                                                        new BinaryOperator("PayableTransaction", _locPayableTransactionXPO),
                                                                                        new BinaryOperator("Select", true),
                                                                                        new BinaryOperator("Status", Status.Close, BinaryOperatorType.NotEqual)));

                        if (_locPurchasePrePaymentInvoices != null && _locPurchasePrePaymentInvoices.Count() > 0)
                        {
                            //Cek Total PIP dan Purchase Order Maks Pay
                            foreach (PurchasePrePaymentInvoice _locPurchasePrePaymentInvoice in _locPurchasePrePaymentInvoices)
                            {
                                if (_locPurchasePrePaymentInvoice.PurchaseOrder != null)
                                {
                                    #region Company

                                    if (_locPurchasePrePaymentInvoice.BankAccountCompany != null && _locPurchasePrePaymentInvoice.Company != null)
                                    {
                                        _locAccountForCompany = GetAccountFromBankAccountForCompany(_currSession, _locPurchasePrePaymentInvoice.BankAccountCompany, _locPurchasePrePaymentInvoice.Company);
                                    }

                                    PayableTransactionLine _saveDataPayableTransactionLine1 = new PayableTransactionLine(_currSession)
                                    {
                                        PostingDate = now,
                                        PurchaseType = PostingType.Purchase,
                                        PostingMethod = PostingMethod.Payment,
                                        OpenCompany = true,
                                        Company = _locPurchasePrePaymentInvoice.Company,
                                        BankAccount = _locPurchasePrePaymentInvoice.BankAccountCompany,
                                        AccountNo = _locPurchasePrePaymentInvoice.CompanyAccountNo,
                                        AccountName = _locPurchasePrePaymentInvoice.CompanyAccountName,
                                        Account = _locAccountForCompany,
                                        Debit = _locPurchasePrePaymentInvoice.DP_Amount,
                                        CloseCredit = true,
                                        PurchasePrePaymentInvoice = _locPurchasePrePaymentInvoice,
                                        PurchaseOrder = _locPurchasePrePaymentInvoice.PurchaseOrder,
                                        PayableTransaction = _locPayableTransactionXPO,
                                    };
                                    _saveDataPayableTransactionLine1.Save();
                                    _saveDataPayableTransactionLine1.Session.CommitTransaction();

                                    #endregion Company

                                    #region Vendor

                                    if (_locPurchasePrePaymentInvoice.BankAccount != null && _locPurchasePrePaymentInvoice.BuyFromVendor != null)
                                    {
                                        _locAccountForVendor = GetAccountFromBankAccountForVendor(_currSession, _locPurchasePrePaymentInvoice.BankAccount, _locPurchasePrePaymentInvoice.BuyFromVendor);
                                    }
                                    PayableTransactionLine _saveDataPayableTransactionLine2 = new PayableTransactionLine(_currSession)
                                    {
                                        PostingDate = now,
                                        PurchaseType = PostingType.Purchase,
                                        PostingMethod = PostingMethod.Payment,
                                        OpenVendor = true,
                                        Vendor = _locPurchasePrePaymentInvoice.PayToVendor,
                                        BankAccount = _locPurchasePrePaymentInvoice.BankAccount,
                                        AccountNo = _locPurchasePrePaymentInvoice.AccountNo,
                                        AccountName = _locPurchasePrePaymentInvoice.AccountName,
                                        Account = _locAccountForVendor,
                                        Credit = _locPurchasePrePaymentInvoice.DP_Amount,
                                        CloseDebit = true,
                                        PurchasePrePaymentInvoice = _locPurchasePrePaymentInvoice,
                                        PurchaseOrder = _locPurchasePrePaymentInvoice.PurchaseOrder,
                                        PayableTransaction = _locPayableTransactionXPO,
                                    };
                                    _saveDataPayableTransactionLine2.Save();
                                    _saveDataPayableTransactionLine2.Session.CommitTransaction();

                                    #endregion Vendor

                                    _locPurchasePrePaymentInvoice.Status = Status.Posted;
                                    _locPurchasePrePaymentInvoice.StatusDate = now;
                                    _locPurchasePrePaymentInvoice.PostedCount = _locPurchasePrePaymentInvoice.PostedCount + 1;
                                    _locPurchasePrePaymentInvoice.Save();
                                    _locPurchasePrePaymentInvoice.Session.CommitTransaction();
                                }
                            }
                        }
                    }
                    //Cek 
                    //Harus mampu menghitung otomatis buat MultiTax dan MultiDiscount


                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PayableTransaction ", ex.ToString());
            }
        }

        #endregion GetPPI

        #region Posting

        #region PostingPurchaseInvoice

        private bool CheckMaksAmountDebitBasedPurchaseInvoiceMonitoring(Session _currSession, PayableTransaction _locPayableTransactionXPO)
        {
            bool _result = true;

            try
            {
                double _totDebit = 0;

                XPQuery<PayableTransactionLine> _payableTransactionLineQuerys = new XPQuery<PayableTransactionLine>(_currSession);

                var _payableTransactionLines = from ptl in _payableTransactionLineQuerys
                                                  where (ptl.PayableTransaction == _locPayableTransactionXPO
                                                  && ptl.Select == true
                                                  && (ptl.Status == Status.Progress || ptl.Status == Status.Posted))
                                                  group ptl by ptl.PurchaseInvoiceMonitoring into g
                                                  select new { PurchaseInvoiceMonitoring = g.Key };

                if (_payableTransactionLines != null && _payableTransactionLines.Count() > 0)
                {
                    foreach (var _payableTransactionLine in _payableTransactionLines)
                    {
                        if (_payableTransactionLine.PurchaseInvoiceMonitoring != null)
                        {
                            XPCollection<PayableTransactionLine> _locPayableTransactionLines = new XPCollection<PayableTransactionLine>
                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("PayableTransaction", _locPayableTransactionXPO),
                                                                                            new BinaryOperator("PurchaseInvoiceMonitoring", _payableTransactionLine.PurchaseInvoiceMonitoring),
                                                                                            new GroupOperator(GroupOperatorType.Or,
                                                                                            new BinaryOperator("Status", Status.Progress),
                                                                                            new BinaryOperator("Status", Status.Posted))));
                            if (_locPayableTransactionLines != null && _locPayableTransactionLines.Count() > 0)
                            {
                                foreach (PayableTransactionLine _locPayableTransactionLine in _locPayableTransactionLines)
                                {
                                    if (_locPayableTransactionLine.CloseCredit == true && _locPayableTransactionLine.Debit > 0)
                                    {
                                        _totDebit = _totDebit + _locPayableTransactionLine.Debit;
                                    }
                                }

                                if (_totDebit != _payableTransactionLine.PurchaseInvoiceMonitoring.Pay)
                                {
                                    _result = false;
                                }

                                _totDebit = 0;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PayableTransaction ", ex.ToString());
            }

            return _result;
        }

        private bool CheckAmountDebitCreditBasedPurchaseInvoiceMonitoring(Session _currSession, PayableTransaction _locPayableTransactionXPO)
        {
            bool _result = true;

            try
            {
                double _totDebit = 0;
                double _totCredit = 0;
                XPQuery<PayableTransactionLine> _payableTransactionLineQuerys = new XPQuery<PayableTransactionLine>(_currSession);

                var _payableTransactionLines = from ptl in _payableTransactionLineQuerys
                                                  where (ptl.PayableTransaction == _locPayableTransactionXPO
                                                  && ptl.Select == true
                                                  && (ptl.Status == Status.Progress || ptl.Status == Status.Posted))
                                                  group ptl by ptl.PurchaseInvoiceMonitoring into g
                                                  select new { PurchaseInvoiceMonitoring = g.Key };

                if (_payableTransactionLines != null && _payableTransactionLines.Count() > 0)
                {
                    foreach (var _payableTransactionLine in _payableTransactionLines)
                    {
                        if (_payableTransactionLine.PurchaseInvoiceMonitoring != null)
                        {
                            XPCollection<PayableTransactionLine> _locPayableTransactionLines = new XPCollection<PayableTransactionLine>
                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("PayableTransaction", _locPayableTransactionXPO),
                                                                                            new BinaryOperator("PurchaseInvoiceMonitoring", _payableTransactionLine.PurchaseInvoiceMonitoring),
                                                                                            new GroupOperator(GroupOperatorType.Or,
                                                                                            new BinaryOperator("Status", Status.Progress),
                                                                                            new BinaryOperator("Status", Status.Posted))));
                            if (_locPayableTransactionLines != null && _locPayableTransactionLines.Count() > 0)
                            {
                                foreach (PayableTransactionLine _locPayableTransactionLine in _locPayableTransactionLines)
                                {
                                    if (_locPayableTransactionLine.CloseCredit == true && _locPayableTransactionLine.Debit > 0)
                                    {
                                        _totDebit = _totDebit + _locPayableTransactionLine.Debit;
                                    }
                                    else if (_locPayableTransactionLine.CloseDebit == true && _locPayableTransactionLine.Credit > 0)
                                    {
                                        _totCredit = _totCredit + _locPayableTransactionLine.Credit;
                                    }
                                }

                                if (_totDebit != _totCredit)
                                {
                                    _result = false;
                                }

                                _totDebit = 0;
                                _totCredit = 0;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PayableTransaction ", ex.ToString());
            }

            return _result;
        }

        private void SetPayableTransactionMonitoringBasedPurchaseInvoiceMonitoring(Session _currSession, PayableTransaction _locPayableTransactionXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                double _totDebit = 0;
                double _totCredit = 0;
                string _currSignCode = null;
                DateTime now = DateTime.Now;

                XPQuery<PayableTransactionLine> _payableTransactionLineQuerys = new XPQuery<PayableTransactionLine>(_currSession);

                var _payableTransactionLines = from ptl in _payableTransactionLineQuerys
                                                  where (ptl.PayableTransaction == _locPayableTransactionXPO
                                                  && ptl.Select == true
                                                  && (ptl.Status == Status.Progress || ptl.Status == Status.Posted))
                                                  group ptl by ptl.PurchaseInvoiceMonitoring into g
                                                  select new { PurchaseInvoiceMonitoring = g.Key };

                if (_payableTransactionLines != null && _payableTransactionLines.Count() > 0)
                {
                    foreach (var _payableTransactionLine in _payableTransactionLines)
                    {
                        if (_payableTransactionLine.PurchaseInvoiceMonitoring != null)
                        {
                            XPCollection<PayableTransactionLine> _locPayableTransactionLines = new XPCollection<PayableTransactionLine>
                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("PayableTransaction", _locPayableTransactionXPO),
                                                                                            new BinaryOperator("PurchaseInvoiceMonitoring", _payableTransactionLine.PurchaseInvoiceMonitoring),
                                                                                            new GroupOperator(GroupOperatorType.Or,
                                                                                            new BinaryOperator("Status", Status.Progress),
                                                                                            new BinaryOperator("Status", Status.Posted))));

                            if (_locPayableTransactionLines != null && _locPayableTransactionLines.Count() > 0)
                            {
                                foreach (PayableTransactionLine _locPayableTransactionLine in _locPayableTransactionLines)
                                {
                                    if (_locPayableTransactionLine.CloseCredit == true && _locPayableTransactionLine.Debit > 0)
                                    {
                                        _totDebit = _totDebit + _locPayableTransactionLine.Debit;
                                    }
                                    else if (_locPayableTransactionLine.CloseDebit == true && _locPayableTransactionLine.Credit > 0)
                                    {
                                        _totCredit = _totCredit + _locPayableTransactionLine.Credit;
                                    }
                                }

                                _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.PayableTransactionMonitoring);

                                if (_currSignCode != null)
                                {
                                    PayableTransactionMonitoring _saveDataPayableTransactionMonitoring = new PayableTransactionMonitoring(_currSession)
                                    {
                                        SignCode = _currSignCode,
                                        PayableTransaction = _locPayableTransactionXPO,
                                        PurchaseInvoiceMonitoring = _payableTransactionLine.PurchaseInvoiceMonitoring,
                                        PurchaseInvoice = _payableTransactionLine.PurchaseInvoiceMonitoring.PurchaseInvoice,
                                        PurchaseOrder = _payableTransactionLine.PurchaseInvoiceMonitoring.PurchaseOrderMonitoring.PurchaseOrder,
                                        Currency = _locPayableTransactionXPO.Currency,
                                        TotAmountDebit = _totDebit,
                                        TotAmountCredit = _totCredit,
                                    };
                                    _saveDataPayableTransactionMonitoring.Save();
                                    _saveDataPayableTransactionMonitoring.Session.CommitTransaction();

                                    PayableTransactionMonitoring _locPayableTransactionMonitoring = _currSession.FindObject<PayableTransactionMonitoring>
                                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                                    new BinaryOperator("SignCode", _currSignCode)));
                                    if (_locPayableTransactionMonitoring != null)
                                    {
                                        #region PaymentOutPlan

                                        double _locPaid = 0;
                                        double _locOutstanding = 0;
                                        Status _locStatus = Status.Open;

                                        PaymentOutPlan _locPaymentOutPlan = _currSession.FindObject<PaymentOutPlan>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("PurchaseInvoiceMonitoring", _locPayableTransactionMonitoring.PurchaseInvoiceMonitoring)));
                                        if (_locPaymentOutPlan != null)
                                        {
                                            _locPaid = _locPayableTransactionMonitoring.TotAmountCredit;
                                            _locOutstanding = _locPaymentOutPlan.Plan - _locPayableTransactionMonitoring.TotAmountCredit;
                                            if (_locOutstanding == 0)
                                            {
                                                _locStatus = Status.Close;
                                            }
                                            else
                                            {
                                                _locStatus = Status.Posted;
                                            }

                                            _locPaymentOutPlan.Paid = _locPaymentOutPlan.Paid + _locPaid;
                                            _locPaymentOutPlan.Outstanding = _locOutstanding;
                                            _locPaymentOutPlan.Status = _locStatus;
                                            _locPaymentOutPlan.StatusDate = now;
                                            _locPaymentOutPlan.Save();
                                            _locPaymentOutPlan.Session.CommitTransaction();
                                        }

                                        #endregion PaymentOutPlan

                                        #region PurchaseInvoiceMonitoring

                                        if (_locPayableTransactionMonitoring.PurchaseInvoiceMonitoring != null)
                                        {
                                            if (_locPayableTransactionMonitoring.PurchaseInvoiceMonitoring.Code != null)
                                            {
                                                PurchaseInvoiceMonitoring _locPurchaseInvoiceMonitoring = _currSession.FindObject<PurchaseInvoiceMonitoring>
                                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("Code", _locPayableTransactionMonitoring.PurchaseInvoiceMonitoring.Code)));
                                                if (_locPurchaseInvoiceMonitoring != null)
                                                {
                                                    if (_locPurchaseInvoiceMonitoring.TAmount == _locPayableTransactionMonitoring.TotAmountCredit)
                                                    {
                                                        _locPurchaseInvoiceMonitoring.Status = Status.Close;
                                                        _locPurchaseInvoiceMonitoring.StatusDate = now;
                                                        _locPurchaseInvoiceMonitoring.Save();
                                                        _locPurchaseInvoiceMonitoring.Session.CommitTransaction();
                                                    }
                                                    else
                                                    {
                                                        _locPurchaseInvoiceMonitoring.Status = Status.Posted;
                                                        _locPurchaseInvoiceMonitoring.StatusDate = now;
                                                        _locPurchaseInvoiceMonitoring.Save();
                                                        _locPurchaseInvoiceMonitoring.Session.CommitTransaction();
                                                    }
                                                }
                                            }
                                        }

                                        #endregion PurchaseInvoiceMonitoring
                                    }
                                }
                                _totDebit = 0;
                                _totCredit = 0;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PayableTransaction ", ex.ToString());
            }

        }

        private void SetPayableJournalBasedPurchaseInvoiceMonitoring(Session _currSession, PayableTransaction _locPayableTransactionXPO)
        {
            try
            {
                DateTime now = DateTime.Now;

                PurchaseOrder _locPurchaseOrder = null;
                PurchaseInvoice _locPurchaseInvoice = null;

                if (_locPayableTransactionXPO != null)
                {
                    #region JournalPayableBankAccountByCompany

                    if (_locPayableTransactionXPO.CompanyDefault != null)
                    {
                        double _locTotDebit = 0;
                        double _locTotCredit = 0;
                        XPQuery<PayableTransactionLine> _payableTransactionLineQuery1 = new XPQuery<PayableTransactionLine>(_currSession);

                        var _payableTransactionLine1s = from ptl in _payableTransactionLineQuery1
                                                           where (ptl.PayableTransaction == _locPayableTransactionXPO
                                                           && ptl.Company == _locPayableTransactionXPO.CompanyDefault
                                                           && ptl.Select == true
                                                           && (ptl.Status == Status.Progress || ptl.Status == Status.Posted))
                                                           group ptl by ptl.PurchaseInvoiceMonitoring into g
                                                           select new { PurchaseInvoiceMonitoring = g.Key };

                        if (_payableTransactionLine1s != null && _payableTransactionLine1s.Count() > 0)
                        {
                            foreach (var _payableTransactionLine1 in _payableTransactionLine1s)
                            {
                                if (_payableTransactionLine1.PurchaseInvoiceMonitoring != null)
                                {
                                    XPQuery<PayableTransactionLine> _payableTransactionLineQuery1a = new XPQuery<PayableTransactionLine>(_currSession);

                                    var _payableTransactionLine1as = from ptl in _payableTransactionLineQuery1a
                                                                        where (ptl.PayableTransaction == _locPayableTransactionXPO
                                                                        && ptl.Company == _locPayableTransactionXPO.CompanyDefault
                                                                        && ptl.PurchaseInvoiceMonitoring == _payableTransactionLine1.PurchaseInvoiceMonitoring
                                                                        && ptl.Select == true
                                                                        && (ptl.Status == Status.Progress || ptl.Status == Status.Posted))
                                                                        group ptl by ptl.BankAccount into g
                                                                        select new { BankAccount = g.Key };

                                    if (_payableTransactionLine1as != null && _payableTransactionLine1as.Count() > 0)
                                    {
                                        foreach (var _payableTransactionLine1a in _payableTransactionLine1as)
                                        {
                                            if (_payableTransactionLine1a.BankAccount != null)
                                            {
                                                XPCollection<PayableTransactionLine> _locPayableTransactionLines = new XPCollection<PayableTransactionLine>
                                                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                new BinaryOperator("PayableTransaction", _locPayableTransactionXPO),
                                                                                                                new BinaryOperator("PurchaseInvoiceMonitoring", _payableTransactionLine1.PurchaseInvoiceMonitoring),
                                                                                                                new BinaryOperator("Company", _locPayableTransactionXPO.CompanyDefault),
                                                                                                                new BinaryOperator("Select", true),
                                                                                                                new BinaryOperator("BankAccount", _payableTransactionLine1a.BankAccount),
                                                                                                                new GroupOperator(GroupOperatorType.Or,
                                                                                                                new BinaryOperator("Status", Status.Progress),
                                                                                                                new BinaryOperator("Status", Status.Posted))));

                                                if (_locPayableTransactionLines != null && _locPayableTransactionLines.Count() > 0)
                                                {
                                                    foreach (PayableTransactionLine _locPayableTransactionLine in _locPayableTransactionLines)
                                                    {
                                                        if (_locPayableTransactionLine.OpenCompany == true && _locPayableTransactionLine.CloseCredit == true && _locPayableTransactionLine.Debit > 0)
                                                        {
                                                            _locTotDebit = _locTotDebit + _locPayableTransactionLine.Debit;
                                                        }
                                                        if (_locPayableTransactionLine.PurchaseInvoiceMonitoring != null)
                                                        {
                                                            if (_locPayableTransactionLine.PurchaseInvoiceMonitoring.PurchaseOrderMonitoring != null)
                                                            {
                                                                if (_locPayableTransactionLine.PurchaseInvoiceMonitoring.PurchaseOrderMonitoring.PurchaseOrder != null)
                                                                {
                                                                    _locPurchaseOrder = _locPayableTransactionLine.PurchaseInvoiceMonitoring.PurchaseOrderMonitoring.PurchaseOrder;
                                                                }

                                                            }
                                                            if (_locPayableTransactionLine.PurchaseInvoiceMonitoring.PurchaseInvoice != null)
                                                            {
                                                                _locPurchaseInvoice = _locPayableTransactionLine.PurchaseInvoiceMonitoring.PurchaseInvoice;
                                                            }
                                                        }
                                                    }

                                                    #region JournalMapBankAccountGroup

                                                    if (_payableTransactionLine1a.BankAccount.BankAccountGroup != null)
                                                    {
                                                        XPCollection<JournalMap> _locJournalMaps = new XPCollection<JournalMap>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("BankAccountGroup", _payableTransactionLine1a.BankAccount.BankAccountGroup)));

                                                        if (_locJournalMaps != null && _locJournalMaps.Count() > 0)
                                                        {
                                                            foreach (JournalMap _locJournalMap in _locJournalMaps)
                                                            {
                                                                XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                    new BinaryOperator("JournalMap", _locJournalMap)));

                                                                if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                                {
                                                                    foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                                    {
                                                                        AccountMap _locAccountMap = _currSession.FindObject<AccountMap>
                                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                                     new BinaryOperator("Code", _locJournalMapLine.AccountMap.Code),
                                                                                                     new BinaryOperator("PostingType", PostingType.Purchase),
                                                                                                     new BinaryOperator("PostingMethod", PostingMethod.Payment),
                                                                                                     new BinaryOperator("PostingMethodType", PostingMethodType.Normal)));

                                                                        if (_locAccountMap != null)
                                                                        {
                                                                            XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                                new BinaryOperator("AccountMap", _locAccountMap),
                                                                                                                                new BinaryOperator("Active", true)));

                                                                            if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                            {
                                                                                double _locTotalAmountDebit = 0;
                                                                                double _locTotalAmountCredit = 0;
                                                                                double _locTotalBalance = 0;

                                                                                foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                                {
                                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                                    {
                                                                                        _locTotalAmountDebit = _locTotDebit;
                                                                                    }
                                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                                    {
                                                                                        _locTotalAmountCredit = _locTotCredit;
                                                                                    }

                                                                                    GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                                    {
                                                                                        PostingDate = now,
                                                                                        PostingType = PostingType.Purchase,
                                                                                        PostingMethod = PostingMethod.Payment,
                                                                                        PostingMethodType = PostingMethodType.Normal,
                                                                                        Account = _locAccountMapLine.Account,
                                                                                        Debit = _locTotalAmountDebit,
                                                                                        Credit = _locTotalAmountCredit,
                                                                                        PurchaseOrder = _locPurchaseOrder,
                                                                                        PurchaseInvoice = _locPurchaseInvoice,
                                                                                        PayableTransaction = _locPayableTransactionXPO,
                                                                                        Company = _locPayableTransactionXPO.CompanyDefault,
                                                                                    };
                                                                                    _saveGeneralJournal.Save();
                                                                                    _saveGeneralJournal.Session.CommitTransaction();

                                                                                    if (_locAccountMapLine.Account.Code != null)
                                                                                    {
                                                                                        ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                                                 (new GroupOperator(GroupOperatorType.And,
                                                                                                                  new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                                                                                        if (_locCOA != null)
                                                                                        {
                                                                                            if (_locCOA.BalanceType == BalanceType.Change)
                                                                                            {
                                                                                                if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                                                {
                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                                                    }
                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                                                    }

                                                                                                }
                                                                                                if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                                                {
                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                                                    }
                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                                                    }
                                                                                                }
                                                                                            }

                                                                                            _locCOA.Balance = _locTotalBalance;
                                                                                            _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                                            _locCOA.Save();
                                                                                            _locCOA.Session.CommitTransaction();
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            ErrorMessageShow("Data Journal Map Not Available");
                                                        }
                                                    }

                                                    #endregion JournalMapBankAccountGroup

                                                    _locTotDebit = 0;

                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //Buat GroupBy SalesInvoiceMonitoring


                    #endregion JournalReceivableBankAccountByCompany

                    #region JournalPayableByBusinessPartner

                    if (_locPayableTransactionXPO.PayToVendor != null && _locPayableTransactionXPO.BuyFromVendor != null)
                    {
                        double _locTotDebit = 0;
                        double _locTotCredit = 0;
                        //Total Credit dari BillToCustomer
                        XPQuery<PayableTransactionLine> _payableTransactionLineQuery2 = new XPQuery<PayableTransactionLine>(_currSession);

                        var _payableTransactionLine2s = from ptl in _payableTransactionLineQuery2
                                                           where (ptl.PayableTransaction == _locPayableTransactionXPO
                                                           && ptl.Select == true
                                                           && ptl.Vendor == _locPayableTransactionXPO.PayToVendor
                                                           && (ptl.Status == Status.Progress || ptl.Status == Status.Posted))
                                                           group ptl by ptl.PurchaseInvoiceMonitoring into g
                                                           select new { PurchaseInvoiceMonitoring = g.Key };

                        if (_payableTransactionLine2s != null && _payableTransactionLine2s.Count() > 0)
                        {
                            foreach (var _payableTransactionLine2 in _payableTransactionLine2s)
                            {
                                if (_payableTransactionLine2.PurchaseInvoiceMonitoring != null)
                                {
                                    XPCollection<PayableTransactionLine> _locPayableTransactionLines = new XPCollection<PayableTransactionLine>
                                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                    new BinaryOperator("PayableTransaction", _locPayableTransactionXPO),
                                                                                                    new BinaryOperator("Select", true),
                                                                                                    new BinaryOperator("Vendor", _locPayableTransactionXPO.PayToVendor),
                                                                                                    new BinaryOperator("PurchaseInvoiceMonitoring", _payableTransactionLine2.PurchaseInvoiceMonitoring),
                                                                                                    new GroupOperator(GroupOperatorType.Or,
                                                                                                    new BinaryOperator("Status", Status.Progress),
                                                                                                    new BinaryOperator("Status", Status.Posted))));
                                    if (_locPayableTransactionLines != null && _locPayableTransactionLines.Count() > 0)
                                    {
                                        foreach (PayableTransactionLine _locPayableTransactionLine in _locPayableTransactionLines)
                                        {
                                            if (_locPayableTransactionLine.OpenVendor == true && _locPayableTransactionLine.CloseDebit == true && _locPayableTransactionLine.Credit > 0)
                                            {
                                                _locTotCredit = _locTotCredit + _locPayableTransactionLine.Credit;
                                            }

                                            if (_locPayableTransactionLine.PurchaseInvoiceMonitoring != null)
                                            {
                                                if (_locPayableTransactionLine.PurchaseInvoiceMonitoring.PurchaseOrderMonitoring != null)
                                                {
                                                    if (_locPayableTransactionLine.PurchaseInvoiceMonitoring.PurchaseOrderMonitoring.PurchaseOrder != null)
                                                    {
                                                        _locPurchaseOrder = _locPayableTransactionLine.PurchaseInvoiceMonitoring.PurchaseOrderMonitoring.PurchaseOrder;
                                                    }
                                                }
                                                if (_locPayableTransactionLine.PurchaseInvoiceMonitoring.PurchaseInvoice != null)
                                                {
                                                    _locPurchaseInvoice = _locPayableTransactionLine.PurchaseInvoiceMonitoring.PurchaseInvoice;
                                                }
                                            }
                                        }

                                        #region JournalMapBusinessPartnerAcccountGroup

                                        if (_locPayableTransactionXPO.BuyFromVendor.BusinessPartnerAccountGroup != null)
                                        {
                                            XPCollection<JournalMap> _locJournalMaps = new XPCollection<JournalMap>
                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("BusinessPartnerAccountGroup", _locPayableTransactionXPO.BuyFromVendor.BusinessPartnerAccountGroup)));

                                            if (_locJournalMaps != null && _locJournalMaps.Count() > 0)
                                            {
                                                foreach (JournalMap _locJournalMap in _locJournalMaps)
                                                {
                                                    XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                        new BinaryOperator("JournalMap", _locJournalMap)));

                                                    if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                    {
                                                        foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                        {
                                                            AccountMap _locAccountMap = _currSession.FindObject<AccountMap>
                                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                                         new BinaryOperator("Code", _locJournalMapLine.AccountMap.Code),
                                                                                         new BinaryOperator("PostingType", PostingType.Purchase),
                                                                                         new BinaryOperator("PostingMethod", PostingMethod.Payment),
                                                                                         new BinaryOperator("PostingMethodType", PostingMethodType.Normal)));

                                                            if (_locAccountMap != null)
                                                            {
                                                                XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                    new BinaryOperator("AccountMap", _locAccountMap),
                                                                                                                    new BinaryOperator("Active", true)));

                                                                if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                {
                                                                    double _locTotalAmountDebit = 0;
                                                                    double _locTotalAmountCredit = 0;
                                                                    double _locTotalBalance = 0;

                                                                    foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                    {
                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                        {
                                                                            _locTotalAmountDebit = _locTotDebit;
                                                                        }
                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                        {
                                                                            _locTotalAmountCredit = _locTotCredit;
                                                                        }

                                                                        GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                        {
                                                                            PostingDate = now,
                                                                            PostingType = PostingType.Purchase,
                                                                            PostingMethod = PostingMethod.Payment,
                                                                            PostingMethodType = PostingMethodType.Normal,
                                                                            Account = _locAccountMapLine.Account,
                                                                            Debit = _locTotalAmountDebit,
                                                                            Credit = _locTotalAmountCredit,
                                                                            PurchaseOrder = _locPurchaseOrder,
                                                                            PurchaseInvoice = _locPurchaseInvoice,
                                                                            PayableTransaction = _locPayableTransactionXPO,
                                                                            Company = _locPayableTransactionXPO.CompanyDefault,
                                                                        };
                                                                        _saveGeneralJournal.Save();
                                                                        _saveGeneralJournal.Session.CommitTransaction();

                                                                        if (_locAccountMapLine.Account.Code != null)
                                                                        {
                                                                            ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                                     (new GroupOperator(GroupOperatorType.And,
                                                                                                      new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                                                                            if (_locCOA != null)
                                                                            {
                                                                                if (_locCOA.BalanceType == BalanceType.Change)
                                                                                {
                                                                                    if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                                    {
                                                                                        if (_locTotalAmountDebit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                                        }
                                                                                        if (_locTotalAmountCredit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                                        }
                                                                                    }
                                                                                    if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                                    {
                                                                                        if (_locTotalAmountDebit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                                        }
                                                                                        if (_locTotalAmountCredit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                                        }
                                                                                    }
                                                                                }

                                                                                _locCOA.Balance = _locTotalBalance;
                                                                                _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                                _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                                _locCOA.Save();
                                                                                _locCOA.Session.CommitTransaction();
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        #endregion JournalMapBankAccountGroup

                                        _locTotCredit = 0;
                                    }
                                }
                            }
                        }
                    }

                    #endregion JournalPayableByBusinessPartner
                }
                else
                {
                    ErrorMessageShow("Data Payable Transaction Not Available");
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PayableTransaction ", ex.ToString());
            }
        }

        private void SetNormalPurchaseInvoiceMonitoring(Session _currSession, PayableTransaction _locPayableTransactionXPO)
        {
            try
            {
                XPQuery<PayableTransactionLine> _payableTransactionLineQuery1 = new XPQuery<PayableTransactionLine>(_currSession);

                var _payableTransactionLine1s = from ptl in _payableTransactionLineQuery1
                                                   where (ptl.PayableTransaction == _locPayableTransactionXPO
                                                   && ptl.Select == true
                                                   && (ptl.Status == Status.Progress || ptl.Status == Status.Posted))
                                                   group ptl by ptl.PurchaseInvoiceMonitoring into g
                                                   select new { PurchaseInvoiceMonitoring = g.Key };
                if (_payableTransactionLine1s != null && _payableTransactionLine1s.Count() > 0)
                {
                    foreach (var _payableTransactionLine1 in _payableTransactionLine1s)
                    {
                        if (_payableTransactionLine1.PurchaseInvoiceMonitoring != null)
                        {
                            if (_payableTransactionLine1.PurchaseInvoiceMonitoring.Code != null)
                            {
                                PurchaseInvoiceMonitoring _locPurchaseInvoiceMonitoring = _currSession.FindObject<PurchaseInvoiceMonitoring>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Code", _payableTransactionLine1.PurchaseInvoiceMonitoring.Code)));
                                if (_locPurchaseInvoiceMonitoring != null)
                                {
                                    _locPurchaseInvoiceMonitoring.PayableTransaction = null;
                                    _locPurchaseInvoiceMonitoring.Save();
                                    _locPurchaseInvoiceMonitoring.Session.CommitTransaction();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PayableTransaction ", ex.ToString());
            }
        }

        #endregion PostingPurchaseInvoice

        #region PostingPurchasePrePaymentInvoice

        private bool CheckMaksAmountDebitBasedPurchasePrePaymentInvoice(Session _currSession, PayableTransaction _locPayableTransactionXPO)
        {
            bool _result = true;

            try
            {
                double _totDebit = 0;

                XPQuery<PayableTransactionLine> _payableTransactionLineQuerys = new XPQuery<PayableTransactionLine>(_currSession);

                var _payableTransactionLines = from ptl in _payableTransactionLineQuerys
                                                  where (ptl.PayableTransaction == _locPayableTransactionXPO
                                                  && ptl.Select == true
                                                  && (ptl.Status == Status.Progress || ptl.Status == Status.Posted))
                                                  group ptl by ptl.PurchasePrePaymentInvoice into g
                                                  select new { PurchasePrePaymentInvoice = g.Key };

                if (_payableTransactionLines != null && _payableTransactionLines.Count() > 0)
                {
                    foreach (var _payableTransactionLine in _payableTransactionLines)
                    {
                        if (_payableTransactionLine.PurchasePrePaymentInvoice != null)
                        {
                            XPCollection<PayableTransactionLine> _locPayableTransactionLines = new XPCollection<PayableTransactionLine>
                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("PayableTransaction", _locPayableTransactionXPO),
                                                                                            new BinaryOperator("PurchasePrePaymentInvoice", _payableTransactionLine.PurchasePrePaymentInvoice),
                                                                                            new GroupOperator(GroupOperatorType.Or,
                                                                                            new BinaryOperator("Status", Status.Progress),
                                                                                            new BinaryOperator("Status", Status.Posted))));
                            if (_locPayableTransactionLines != null && _locPayableTransactionLines.Count() > 0)
                            {
                                foreach (PayableTransactionLine _locPayableTransactionLine in _locPayableTransactionLines)
                                {
                                    if (_locPayableTransactionLine.CloseCredit == true && _locPayableTransactionLine.Debit > 0)
                                    {
                                        _totDebit = _totDebit + _locPayableTransactionLine.Debit;
                                    }
                                }

                                if (_totDebit != _payableTransactionLine.PurchasePrePaymentInvoice.DP_Amount)
                                {
                                    _result = false;
                                }

                                _totDebit = 0;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PayableTransaction ", ex.ToString());
            }

            return _result;
        }

        private bool CheckAmountDebitCreditBasedPurchasePrePaymentInvoice(Session _currSession, PayableTransaction _locPayableTransactionXPO)
        {
            bool _result = true;

            try
            {
                double _totDebit = 0;
                double _totCredit = 0;
                XPQuery<PayableTransactionLine> _payableTransactionLineQuerys = new XPQuery<PayableTransactionLine>(_currSession);

                var _payableTransactionLines = from ptl in _payableTransactionLineQuerys
                                                  where (ptl.PayableTransaction == _locPayableTransactionXPO
                                                  && ptl.Select == true
                                                  && (ptl.Status == Status.Progress || ptl.Status == Status.Posted))
                                                  group ptl by ptl.PurchasePrePaymentInvoice into g
                                                  select new { PurchasePrePaymentInvoice = g.Key };

                if (_payableTransactionLines != null && _payableTransactionLines.Count() > 0)
                {
                    foreach (var _payableTransactionLine in _payableTransactionLines)
                    {
                        if (_payableTransactionLine.PurchasePrePaymentInvoice != null)
                        {
                            XPCollection<PayableTransactionLine> _locPayableTransactionLines = new XPCollection<PayableTransactionLine>
                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("PayableTransaction", _locPayableTransactionXPO),
                                                                                            new BinaryOperator("PurchasePrePaymentInvoice", _payableTransactionLine.PurchasePrePaymentInvoice),
                                                                                            new GroupOperator(GroupOperatorType.Or,
                                                                                            new BinaryOperator("Status", Status.Progress),
                                                                                            new BinaryOperator("Status", Status.Posted))));
                            if (_locPayableTransactionLines != null && _locPayableTransactionLines.Count() > 0)
                            {
                                foreach (PayableTransactionLine _locPayableTransactionLine in _locPayableTransactionLines)
                                {
                                    if (_locPayableTransactionLine.CloseCredit == true && _locPayableTransactionLine.Debit > 0)
                                    {
                                        _totDebit = _totDebit + _locPayableTransactionLine.Debit;
                                    }
                                    else if (_locPayableTransactionLine.CloseDebit == true && _locPayableTransactionLine.Credit > 0)
                                    {
                                        _totCredit = _totCredit + _locPayableTransactionLine.Credit;
                                    }
                                }

                                if (_totDebit != _totCredit)
                                {
                                    _result = false;
                                }
                                _totDebit = 0;
                                _totCredit = 0;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PayableTransaction ", ex.ToString());
            }

            return _result;
        }

        private void SetPayableTransactionMonitoringBasedPurchasePrePaymentInvoice(Session _currSession, PayableTransaction _locPayableTransactionXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                double _totDebit = 0;
                double _totCredit = 0;
                string _currSignCode = null;
                DateTime now = DateTime.Now;

                XPQuery<PayableTransactionLine> _payableTransactionLineQuerys = new XPQuery<PayableTransactionLine>(_currSession);

                var _payableTransactionLines = from ptl in _payableTransactionLineQuerys
                                                  where (ptl.PayableTransaction == _locPayableTransactionXPO
                                                  && ptl.Select == true
                                                  && (ptl.Status == Status.Progress || ptl.Status == Status.Posted))
                                                  group ptl by ptl.PurchasePrePaymentInvoice into g
                                                  select new { PurchasePrePaymentInvoice = g.Key };

                if (_payableTransactionLines != null && _payableTransactionLines.Count() > 0)
                {
                    foreach (var _payableTransactionLine in _payableTransactionLines)
                    {
                        if (_payableTransactionLine.PurchasePrePaymentInvoice != null)
                        {
                            XPCollection<PayableTransactionLine> _locPayableTransactionLines = new XPCollection<PayableTransactionLine>
                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("PayableTransaction", _locPayableTransactionXPO),
                                                                                            new BinaryOperator("PurchasePrePaymentInvoice", _payableTransactionLine.PurchasePrePaymentInvoice),
                                                                                            new GroupOperator(GroupOperatorType.Or,
                                                                                            new BinaryOperator("Status", Status.Progress),
                                                                                            new BinaryOperator("Status", Status.Posted))));

                            if (_locPayableTransactionLines != null && _locPayableTransactionLines.Count() > 0)
                            {
                                foreach (PayableTransactionLine _locPayableTransactionLine in _locPayableTransactionLines)
                                {
                                    if (_locPayableTransactionLine.CloseCredit == true && _locPayableTransactionLine.Debit > 0)
                                    {
                                        _totDebit = _totDebit + _locPayableTransactionLine.Debit;
                                    }
                                    else if (_locPayableTransactionLine.CloseDebit == true && _locPayableTransactionLine.Credit > 0)
                                    {
                                        _totCredit = _totCredit + _locPayableTransactionLine.Credit;
                                    }
                                }

                                _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.PayableTransactionMonitoring);

                                if (_currSignCode != null)
                                {
                                    PayableTransactionMonitoring _saveDataPayableTransactionMonitoring = new PayableTransactionMonitoring(_currSession)
                                    {
                                        SignCode = _currSignCode,
                                        PayableTransaction = _locPayableTransactionXPO,
                                        PurchasePrePaymentInvoice = _payableTransactionLine.PurchasePrePaymentInvoice,
                                        PurchaseOrder = _payableTransactionLine.PurchasePrePaymentInvoice.PurchaseOrder,
                                        Currency = _locPayableTransactionXPO.Currency,
                                        TotAmountDebit = _totDebit,
                                        TotAmountCredit = _totCredit,
                                    };
                                    _saveDataPayableTransactionMonitoring.Save();
                                    _saveDataPayableTransactionMonitoring.Session.CommitTransaction();

                                    PayableTransactionMonitoring _locPayableTransactionMonitoring = _currSession.FindObject<PayableTransactionMonitoring>
                                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                                    new BinaryOperator("SignCode", _currSignCode)));
                                    if (_locPayableTransactionMonitoring != null)
                                    {
                                        double _locPaid = 0;
                                        double _locOutstanding = 0;
                                        Status _locStatus = Status.Open;

                                        PaymentOutPlan _locPaymentOutPlan = _currSession.FindObject<PaymentOutPlan>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("PurchasePrePaymentInvoice", _locPayableTransactionMonitoring.PurchasePrePaymentInvoice)));
                                        if (_locPaymentOutPlan != null)
                                        {
                                            _locPaid = _locPayableTransactionMonitoring.TotAmountCredit;
                                            _locOutstanding = _locPaymentOutPlan.Plan - _locPayableTransactionMonitoring.TotAmountCredit;
                                            if (_locOutstanding == 0)
                                            {
                                                _locStatus = Status.Close;
                                            }
                                            else
                                            {
                                                _locStatus = Status.Posted;
                                            }

                                            _locPaymentOutPlan.Paid = _locPaid;
                                            _locPaymentOutPlan.Outstanding = _locOutstanding;
                                            _locPaymentOutPlan.Status = _locStatus;
                                            _locPaymentOutPlan.StatusDate = now;
                                        }

                                        if (_locPayableTransactionMonitoring.PurchasePrePaymentInvoice != null)
                                        {
                                            if (_locPayableTransactionMonitoring.PurchasePrePaymentInvoice.Code != null)
                                            {
                                                PurchasePrePaymentInvoice _locPurchasePrePaymentInvoice = _currSession.FindObject<PurchasePrePaymentInvoice>
                                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("Code", _locPayableTransactionMonitoring.PurchasePrePaymentInvoice.Code)));
                                                if (_locPurchasePrePaymentInvoice != null)
                                                {
                                                    if (_locPurchasePrePaymentInvoice.DP_Amount == _locPayableTransactionMonitoring.TotAmountCredit)
                                                    {
                                                        _locPurchasePrePaymentInvoice.Status = Status.Close;
                                                        _locPurchasePrePaymentInvoice.StatusDate = now;
                                                        _locPurchasePrePaymentInvoice.Save();
                                                        _locPurchasePrePaymentInvoice.Session.CommitTransaction();
                                                    }
                                                    else
                                                    {
                                                        _locPurchasePrePaymentInvoice.Status = Status.Posted;
                                                        _locPurchasePrePaymentInvoice.StatusDate = now;
                                                        _locPurchasePrePaymentInvoice.Save();
                                                        _locPurchasePrePaymentInvoice.Session.CommitTransaction();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                _totDebit = 0;
                                _totCredit = 0;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PayableTransaction ", ex.ToString());
            }
        }

        private void SetPayableJournalBasedPurchasePrePaymentInvoice(Session _currSession, PayableTransaction _locPayableTransactionXPO)
        {
            try
            {
                DateTime now = DateTime.Now;

                PurchaseOrder _locPurchaseOrder = null;
                PurchasePrePaymentInvoice _locPurchasePrePaymentInvoice = null;

                if (_locPayableTransactionXPO != null)
                {
                    #region JournalPayableBankAccountByCompany

                    if (_locPayableTransactionXPO.CompanyDefault != null)
                    {
                        double _locTotDebit = 0;
                        double _locTotCredit = 0;
                        XPQuery<PayableTransactionLine> _payableTransactionLineQuery1 = new XPQuery<PayableTransactionLine>(_currSession);

                        var _payableTransactionLine1s = from ptl in _payableTransactionLineQuery1
                                                           where (ptl.PayableTransaction == _locPayableTransactionXPO
                                                           && ptl.Company == _locPayableTransactionXPO.CompanyDefault
                                                           && ptl.Select == true
                                                           && (ptl.Status == Status.Progress || ptl.Status == Status.Posted))
                                                           group ptl by ptl.PurchasePrePaymentInvoice into g
                                                           select new { PurchasePrePaymentInvoice = g.Key };

                        if (_payableTransactionLine1s != null && _payableTransactionLine1s.Count() > 0)
                        {
                            foreach (var _payableTransactionLine1 in _payableTransactionLine1s)
                            {
                                if (_payableTransactionLine1.PurchasePrePaymentInvoice != null)
                                {
                                    XPQuery<PayableTransactionLine> _payableTransactionLineQuery1a = new XPQuery<PayableTransactionLine>(_currSession);

                                    var _payableTransactionLine1as = from ptl in _payableTransactionLineQuery1a
                                                                        where (ptl.PayableTransaction == _locPayableTransactionXPO
                                                                        && ptl.Company == _locPayableTransactionXPO.CompanyDefault
                                                                        && ptl.PurchasePrePaymentInvoice == _payableTransactionLine1.PurchasePrePaymentInvoice
                                                                        && ptl.Select == true
                                                                        && (ptl.Status == Status.Progress || ptl.Status == Status.Posted))
                                                                        group ptl by ptl.BankAccount into g
                                                                        select new { BankAccount = g.Key };

                                    if (_payableTransactionLine1as != null && _payableTransactionLine1as.Count() > 0)
                                    {
                                        foreach (var _payableTransactionLine1a in _payableTransactionLine1as)
                                        {
                                            if (_payableTransactionLine1a.BankAccount != null)
                                            {
                                                XPCollection<PayableTransactionLine> _locPayableTransactionLines = new XPCollection<PayableTransactionLine>
                                                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                new BinaryOperator("PayableTransaction", _locPayableTransactionXPO),
                                                                                                                new BinaryOperator("PurchasePrePaymentInvoice", _payableTransactionLine1.PurchasePrePaymentInvoice),
                                                                                                                new BinaryOperator("Company", _locPayableTransactionXPO.CompanyDefault),
                                                                                                                new BinaryOperator("Select", true),
                                                                                                                new BinaryOperator("BankAccount", _payableTransactionLine1a.BankAccount),
                                                                                                                new GroupOperator(GroupOperatorType.Or,
                                                                                                                new BinaryOperator("Status", Status.Progress),
                                                                                                                new BinaryOperator("Status", Status.Posted))));

                                                if (_locPayableTransactionLines != null && _locPayableTransactionLines.Count() > 0)
                                                {
                                                    foreach (PayableTransactionLine _locPayableTransactionLine in _locPayableTransactionLines)
                                                    {
                                                        if (_locPayableTransactionLine.OpenCompany == true && _locPayableTransactionLine.CloseCredit == true && _locPayableTransactionLine.Debit > 0)
                                                        {
                                                            _locTotDebit = _locTotDebit + _locPayableTransactionLine.Debit;
                                                        }
                                                        if (_locPayableTransactionLine.PurchasePrePaymentInvoice.PurchaseOrder != null)
                                                        {
                                                            _locPurchaseOrder = _locPayableTransactionLine.PurchasePrePaymentInvoice.PurchaseOrder;
                                                        }

                                                        _locPurchasePrePaymentInvoice = _locPayableTransactionLine.PurchasePrePaymentInvoice;
                                                    }

                                                    #region JournalMapBankAccountGroup

                                                    if (_payableTransactionLine1a.BankAccount.BankAccountGroup != null)
                                                    {
                                                        XPCollection<JournalMap> _locJournalMaps = new XPCollection<JournalMap>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("BankAccountGroup", _payableTransactionLine1a.BankAccount.BankAccountGroup)));

                                                        if (_locJournalMaps != null && _locJournalMaps.Count() > 0)
                                                        {
                                                            foreach (JournalMap _locJournalMap in _locJournalMaps)
                                                            {
                                                                XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                    new BinaryOperator("JournalMap", _locJournalMap)));

                                                                if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                                {
                                                                    foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                                    {
                                                                        AccountMap _locAccountMap = _currSession.FindObject<AccountMap>
                                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                                     new BinaryOperator("Code", _locJournalMapLine.AccountMap.Code),
                                                                                                     new BinaryOperator("PostingType", PostingType.Purchase),
                                                                                                     new BinaryOperator("PostingMethod", PostingMethod.Payment),
                                                                                                     new BinaryOperator("PostingMethodType", PostingMethodType.Normal)));

                                                                        if (_locAccountMap != null)
                                                                        {
                                                                            XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                                new BinaryOperator("AccountMap", _locAccountMap),
                                                                                                                                new BinaryOperator("Active", true)));

                                                                            if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                            {
                                                                                double _locTotalAmountDebit = 0;
                                                                                double _locTotalAmountCredit = 0;
                                                                                double _locTotalBalance = 0;

                                                                                foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                                {
                                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                                    {
                                                                                        _locTotalAmountDebit = _locTotDebit;
                                                                                    }
                                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                                    {
                                                                                        _locTotalAmountCredit = _locTotCredit;
                                                                                    }

                                                                                    GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                                    {
                                                                                        PostingDate = now,
                                                                                        PostingType = PostingType.Purchase,
                                                                                        PostingMethod = PostingMethod.Payment,
                                                                                        PostingMethodType = PostingMethodType.Normal,
                                                                                        Account = _locAccountMapLine.Account,
                                                                                        Debit = _locTotalAmountDebit,
                                                                                        Credit = _locTotalAmountCredit,
                                                                                        PurchaseOrder = _locPurchaseOrder,
                                                                                        PurchasePrePaymentInvoice = _locPurchasePrePaymentInvoice,
                                                                                        PayableTransaction = _locPayableTransactionXPO,
                                                                                        Company = _locPayableTransactionXPO.CompanyDefault,
                                                                                    };
                                                                                    _saveGeneralJournal.Save();
                                                                                    _saveGeneralJournal.Session.CommitTransaction();

                                                                                    if (_locAccountMapLine.Account.Code != null)
                                                                                    {
                                                                                        ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                                                 (new GroupOperator(GroupOperatorType.And,
                                                                                                                  new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                                                                                        if (_locCOA != null)
                                                                                        {
                                                                                            if (_locCOA.BalanceType == BalanceType.Change)
                                                                                            {
                                                                                                if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                                                {
                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                                                    }
                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                                                    }

                                                                                                }
                                                                                                if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                                                {
                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                                                    }
                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                                                    }
                                                                                                }
                                                                                            }

                                                                                            _locCOA.Balance = _locTotalBalance;
                                                                                            _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                                            _locCOA.Save();
                                                                                            _locCOA.Session.CommitTransaction();
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            ErrorMessageShow("Data Journal Map Not Available");
                                                        }
                                                    }

                                                    #endregion JournalMapBankAccountGroup

                                                    _locTotDebit = 0;

                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    #endregion JournalPayableBankAccountByCompany

                    #region JournalPayableByBusinessPartner

                    if (_locPayableTransactionXPO.PayToVendor != null && _locPayableTransactionXPO.BuyFromVendor != null)
                    {
                        double _locTotDebit = 0;
                        double _locTotCredit = 0;
                        //Total Credit dari PayToVendor
                        XPQuery<PayableTransactionLine> _payableTransactionLineQuery2 = new XPQuery<PayableTransactionLine>(_currSession);

                        var _payableTransactionLine2s = from ptl in _payableTransactionLineQuery2
                                                           where (ptl.PayableTransaction == _locPayableTransactionXPO
                                                           && ptl.Select == true
                                                           && ptl.Vendor == _locPayableTransactionXPO.PayToVendor
                                                           && (ptl.Status == Status.Progress || ptl.Status == Status.Posted))
                                                           group ptl by ptl.PurchasePrePaymentInvoice into g
                                                           select new { PurchasePrePaymentInvoice = g.Key };

                        if (_payableTransactionLine2s != null && _payableTransactionLine2s.Count() > 0)
                        {
                            foreach (var _payableTransactionLine2 in _payableTransactionLine2s)
                            {
                                if (_payableTransactionLine2.PurchasePrePaymentInvoice != null)
                                {
                                    XPCollection<PayableTransactionLine> _locPayableTransactionLines = new XPCollection<PayableTransactionLine>
                                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                    new BinaryOperator("PayableTransaction", _locPayableTransactionXPO),
                                                                                                    new BinaryOperator("Select", true),
                                                                                                    new BinaryOperator("Vendor", _locPayableTransactionXPO.PayToVendor),
                                                                                                    new BinaryOperator("PurchasePrePaymentInvoice", _payableTransactionLine2.PurchasePrePaymentInvoice),
                                                                                                    new GroupOperator(GroupOperatorType.Or,
                                                                                                    new BinaryOperator("Status", Status.Progress),
                                                                                                    new BinaryOperator("Status", Status.Posted))));

                                    if (_locPayableTransactionLines != null && _locPayableTransactionLines.Count() > 0)
                                    {
                                        foreach (PayableTransactionLine _locPayableTransactionLine in _locPayableTransactionLines)
                                        {
                                            if (_locPayableTransactionLine.OpenVendor == true && _locPayableTransactionLine.CloseDebit == true && _locPayableTransactionLine.Credit > 0)
                                            {
                                                _locTotCredit = _locTotCredit + _locPayableTransactionLine.Credit;
                                            }

                                            if (_locPayableTransactionLine.PurchasePrePaymentInvoice.PurchaseOrder != null)
                                            {
                                                _locPurchaseOrder = _locPayableTransactionLine.PurchasePrePaymentInvoice.PurchaseOrder;
                                            }
                                            else
                                            {
                                                _locPurchasePrePaymentInvoice = _locPayableTransactionLine.PurchasePrePaymentInvoice;
                                            }
                                        }

                                        #region JournalMapBusinessPartnerAcccountGroup

                                        if (_locPayableTransactionXPO.BuyFromVendor.BusinessPartnerAccountGroup != null)
                                        {
                                            XPCollection<JournalMap> _locJournalMaps = new XPCollection<JournalMap>
                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("BusinessPartnerAccountGroup", _locPayableTransactionXPO.BuyFromVendor.BusinessPartnerAccountGroup)));

                                            if (_locJournalMaps != null && _locJournalMaps.Count() > 0)
                                            {
                                                foreach (JournalMap _locJournalMap in _locJournalMaps)
                                                {
                                                    XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                        new BinaryOperator("JournalMap", _locJournalMap)));

                                                    if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                    {
                                                        foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                        {
                                                            AccountMap _locAccountMap = _currSession.FindObject<AccountMap>
                                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                                         new BinaryOperator("Code", _locJournalMapLine.AccountMap.Code),
                                                                                         new BinaryOperator("PostingType", PostingType.Purchase),
                                                                                         new BinaryOperator("PostingMethod", PostingMethod.Payment),
                                                                                         new BinaryOperator("PostingMethodType", PostingMethodType.Normal)));

                                                            if (_locAccountMap != null)
                                                            {
                                                                XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                    new BinaryOperator("AccountMap", _locAccountMap),
                                                                                                                    new BinaryOperator("Active", true)));

                                                                if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                {
                                                                    double _locTotalAmountDebit = 0;
                                                                    double _locTotalAmountCredit = 0;
                                                                    double _locTotalBalance = 0;

                                                                    foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                    {
                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                        {
                                                                            _locTotalAmountDebit = _locTotDebit;
                                                                        }
                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                        {
                                                                            _locTotalAmountCredit = _locTotCredit;
                                                                        }

                                                                        GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                        {
                                                                            PostingDate = now,
                                                                            PostingType = PostingType.Purchase,
                                                                            PostingMethod = PostingMethod.Payment,
                                                                            PostingMethodType = PostingMethodType.Normal,
                                                                            Account = _locAccountMapLine.Account,
                                                                            Debit = _locTotalAmountDebit,
                                                                            Credit = _locTotalAmountCredit,
                                                                            PurchaseOrder = _locPurchaseOrder,
                                                                            PurchasePrePaymentInvoice = _locPurchasePrePaymentInvoice,
                                                                            PayableTransaction = _locPayableTransactionXPO,
                                                                            Company = _locPayableTransactionXPO.CompanyDefault,
                                                                        };
                                                                        _saveGeneralJournal.Save();
                                                                        _saveGeneralJournal.Session.CommitTransaction();

                                                                        if (_locAccountMapLine.Account.Code != null)
                                                                        {
                                                                            ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                                     (new GroupOperator(GroupOperatorType.And,
                                                                                                      new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                                                                            if (_locCOA != null)
                                                                            {
                                                                                if (_locCOA.BalanceType == BalanceType.Change)
                                                                                {
                                                                                    if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                                    {
                                                                                        if (_locTotalAmountDebit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                                        }
                                                                                        if (_locTotalAmountCredit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                                        }
                                                                                    }
                                                                                    if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                                    {
                                                                                        if (_locTotalAmountDebit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                                        }
                                                                                        if (_locTotalAmountCredit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                                        }
                                                                                    }
                                                                                }

                                                                                _locCOA.Balance = _locTotalBalance;
                                                                                _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                                _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                                _locCOA.Save();
                                                                                _locCOA.Session.CommitTransaction();
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        #endregion JournalMapBankAccountGroup

                                        _locTotCredit = 0;
                                    }
                                }
                            }
                        }
                    }

                    #endregion JournalReceivableByBusinessPartner
                }
                else
                {
                    ErrorMessageShow("Data Payable Transaction Not Available");
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PayableTransaction ", ex.ToString());
            }
        }

        private void SetNormalPurchasePrePaymentInvoice(Session _currSession, PayableTransaction _locPayableTransactionXPO)
        {
            try
            {
                XPQuery<PayableTransactionLine> _payableTransactionLineQuery1 = new XPQuery<PayableTransactionLine>(_currSession);

                var _payableTransactionLine1s = from ptl in _payableTransactionLineQuery1
                                                   where (ptl.PayableTransaction == _locPayableTransactionXPO
                                                   && ptl.Select == true
                                                   && (ptl.Status == Status.Progress || ptl.Status == Status.Posted))
                                                   group ptl by ptl.PurchasePrePaymentInvoice into g
                                                   select new { PurchasePrePaymentInvoice = g.Key };
                if (_payableTransactionLine1s != null && _payableTransactionLine1s.Count() > 0)
                {
                    foreach (var _payableTransactionLine1 in _payableTransactionLine1s)
                    {
                        if (_payableTransactionLine1.PurchasePrePaymentInvoice != null)
                        {
                            if (_payableTransactionLine1.PurchasePrePaymentInvoice.Code != null)
                            {
                                PurchasePrePaymentInvoice _locPurchasePrePaymentInvoice = _currSession.FindObject<PurchasePrePaymentInvoice>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Code", _payableTransactionLine1.PurchasePrePaymentInvoice.Code)));
                                if (_locPurchasePrePaymentInvoice != null)
                                {
                                    _locPurchasePrePaymentInvoice.PayableTransaction = null;
                                    _locPurchasePrePaymentInvoice.Save();
                                    _locPurchasePrePaymentInvoice.Session.CommitTransaction();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PayableTransaction ", ex.ToString());
            }
        }

        #endregion PostingPurchasePrePaymentInvoice

        private void SetStatusPayableTransactionLine(Session _currSession, PayableTransaction _locPayableTransactionXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locPayableTransactionXPO != null)
                {
                    XPCollection<PayableTransactionLine> _locPayableTransactionLineLines = new XPCollection<PayableTransactionLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PayableTransaction", _locPayableTransactionXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locPayableTransactionLineLines != null && _locPayableTransactionLineLines.Count > 0)
                    {
                        foreach (PayableTransactionLine _locPayableTransactionLine in _locPayableTransactionLineLines)
                        {
                            if (_locPayableTransactionLine.Status == Status.Progress || _locPayableTransactionLine.Status == Status.Posted)
                            {
                                _locPayableTransactionLine.Status = Status.Close;
                                _locPayableTransactionLine.ActivationPosting = true;
                                _locPayableTransactionLine.StatusDate = now;
                                _locPayableTransactionLine.Save();
                                _locPayableTransactionLine.Session.CommitTransaction();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PayableTransaction " + ex.ToString());
            }
        }

        private void SetFinalStatusPayableTransaction(Session _currSession, PayableTransaction _locPayableTransactionXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locCount = 0;

                if (_locPayableTransactionXPO != null)
                {
                    XPCollection<PayableTransactionLine> _locPayableTransactionLines = new XPCollection<PayableTransactionLine>(_currSession,
                                                                                             new GroupOperator(GroupOperatorType.And,
                                                                                             new BinaryOperator("PayableTransaction", _locPayableTransactionXPO),
                                                                                             new BinaryOperator("Select", true)));

                    if (_locPayableTransactionLines != null && _locPayableTransactionLines.Count() > 0)
                    {
                        foreach (PayableTransactionLine _locPayableTransactionLine in _locPayableTransactionLines)
                        {
                            if (_locPayableTransactionLine.Status == Status.Close)
                            {
                                _locCount = _locCount + 1;
                            }
                        }

                        if (_locCount == _locPayableTransactionLines.Count())
                        {
                            _locPayableTransactionXPO.ActivationPosting = true;
                            _locPayableTransactionXPO.Status = Status.Close;
                            _locPayableTransactionXPO.StatusDate = now;
                            _locPayableTransactionXPO.Save();
                            _locPayableTransactionXPO.Session.CommitTransaction();
                        }
                        else
                        {
                            _locPayableTransactionXPO.Status = Status.Posted;
                            _locPayableTransactionXPO.StatusDate = now;
                            _locPayableTransactionXPO.Save();
                            _locPayableTransactionXPO.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PayableTransaction " + ex.ToString());
            }
        }

        private void SetCloseAllPurchaseProcess(Session _currSession, PayableTransaction _locPayableTransactionXPO)
        {
            //GroupByPaymentOutPlanBasedOnPurchaseInvoice
            //Cari Payable Transaction Monitoring 
            DateTime now = DateTime.Now;
            double _locTotPaySO = 0;
            double _locTotPaySI = 0;

            if (_locPayableTransactionXPO != null)
            {
                XPCollection<PayableTransactionMonitoring> _locPayableTransactionMonitorings = new XPCollection<PayableTransactionMonitoring>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                               new BinaryOperator("PayableTransaction", _locPayableTransactionXPO)));

                if (_locPayableTransactionMonitorings != null && _locPayableTransactionMonitorings.Count() > 0)
                {
                    foreach (PayableTransactionMonitoring _locPayableTransactionMonitoring in _locPayableTransactionMonitorings)
                    {
                        #region PurchaseOrderClose
                        if (_locPayableTransactionMonitoring.PurchaseOrder != null)
                        {
                            XPCollection<PaymentOutPlan> _locPaymentOutPlans = new XPCollection<PaymentOutPlan>
                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("PurchaseOrder", _locPayableTransactionMonitoring.PurchaseOrder)));

                            if (_locPaymentOutPlans != null && _locPaymentOutPlans.Count() > 0)
                            {
                                foreach (PaymentOutPlan _locPaymentOutPlan in _locPaymentOutPlans)
                                {
                                    _locTotPaySO = _locTotPaySO + _locPaymentOutPlan.Paid;
                                }

                                if (_locTotPaySO > 0)
                                {
                                    if (_locTotPaySO == _locPayableTransactionMonitoring.PurchaseOrder.MaxPay)
                                    {
                                        _locPayableTransactionMonitoring.PurchaseOrder.Status = Status.Close;
                                        _locPayableTransactionMonitoring.PurchaseOrder.StatusDate = now;
                                        _locPayableTransactionMonitoring.PurchaseOrder.Save();
                                        _locPayableTransactionMonitoring.PurchaseOrder.Session.CommitTransaction();
                                    }
                                }
                            }
                        }
                        #endregion PurchaseOrderClose

                        #region PurchaseInvoiceClose
                        if (_locPayableTransactionMonitoring.PurchaseInvoice != null)
                        {
                            XPCollection<PaymentOutPlan> _locPaymentOutPlans = new XPCollection<PaymentOutPlan>
                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("PurchaseInvoice", _locPayableTransactionMonitoring.PurchaseInvoice)));

                            if (_locPaymentOutPlans != null && _locPaymentOutPlans.Count() > 0)
                            {
                                foreach (PaymentOutPlan _locPaymentOutPlan in _locPaymentOutPlans)
                                {
                                    _locTotPaySI = _locTotPaySI + _locPaymentOutPlan.Paid;
                                }

                                if (_locTotPaySI > 0)
                                {
                                    if (_locTotPaySI == _locPayableTransactionMonitoring.PurchaseInvoice.MaxPay)
                                    {
                                        _locPayableTransactionMonitoring.PurchaseOrder.Status = Status.Close;
                                        _locPayableTransactionMonitoring.PurchaseOrder.StatusDate = now;
                                        _locPayableTransactionMonitoring.PurchaseOrder.Save();
                                        _locPayableTransactionMonitoring.PurchaseOrder.Session.CommitTransaction();
                                    }
                                }
                            }
                        }
                        #endregion PurchaseInvoiceClose
                    }
                }
            }
        }

        #endregion Posting

        #region PostingMethodOld

        private void SetCloseAllPurchaseProcessOld(Session _currSession, PayableTransaction _locPayableTransactionXPO)
        {
            ////GroupByPaymentOutPlanBasedOnPurchaseInvoice
            //double _locPaid = 0;
            //DateTime now = DateTime.Now;
            //if (_locPayableTransactionXPO.PurchaseInvoice != null)
            //{
            //    XPCollection<PaymentOutPlan> _locPaymentOutPlans = new XPCollection<PaymentOutPlan>(_currSession,
            //                                                new GroupOperator(GroupOperatorType.And,
            //                                                new BinaryOperator("PurchaseInvoice", _locPayableTransactionXPO.PurchaseInvoice)));

            //    if (_locPaymentOutPlans != null && _locPaymentOutPlans.Count > 0)
            //    {
            //        foreach (PaymentOutPlan _locPaymentOutPlan in _locPaymentOutPlans)
            //        {
            //            _locPaid = _locPaid + _locPaymentOutPlan.Paid;
            //        }

            //        if (_locPayableTransactionXPO.PurchaseInvoice.MaxPay > 0)
            //        {
            //            if (_locPayableTransactionXPO.PurchaseInvoice.MaxPay == _locPaid)
            //            {
            //                if (_locPayableTransactionXPO.PurchaseOrder != null)
            //                {
            //                    if (_locPayableTransactionXPO.PurchaseOrder.Code != null)
            //                    {
            //                        PurchaseOrder _locPurchaseOrder = _currSession.FindObject<PurchaseOrder>(new GroupOperator(GroupOperatorType.And,
            //                                                        new BinaryOperator("Code", _locPayableTransactionXPO.PurchaseOrder.Code)));
            //                        if (_locPurchaseOrder != null)
            //                        {
            //                            _locPurchaseOrder.Status = Status.Close;
            //                            _locPurchaseOrder.StatusDate = now;
            //                            _locPurchaseOrder.Save();
            //                            _locPurchaseOrder.Session.CommitTransaction();
            //                        }
            //                    }

            //                }

            //                if (_locPayableTransactionXPO.PurchaseInvoice.Code != null)
            //                {
            //                    PurchaseInvoice _locPurchaseInvoice = _currSession.FindObject<PurchaseInvoice>(new GroupOperator(GroupOperatorType.And,
            //                                                        new BinaryOperator("Code", _locPayableTransactionXPO.PurchaseInvoice.Code)));
            //                    if (_locPurchaseInvoice != null)
            //                    {
            //                        _locPurchaseInvoice.Status = Status.Close;
            //                        _locPurchaseInvoice.StatusDate = now;
            //                        _locPurchaseInvoice.Save();
            //                        _locPurchaseInvoice.Session.CommitTransaction();
            //                    }
            //                }

            //                foreach (PaymentOutPlan _locPaymentOutPlan in _locPaymentOutPlans)
            //                {
            //                    _locPaymentOutPlan.Status = Status.Close;
            //                    _locPaymentOutPlan.StatusDate = now;
            //                    _locPaymentOutPlan.Save();
            //                    _locPaymentOutPlan.Session.CommitTransaction();
            //                }
            //            }
            //        }
            //    }
            //}
        }

        #endregion PostingMethodOld

        #region Global Method

        private void SuccessMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 4000;
            options.Message = string.Format("{0}", _locMessage);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method

        
    }
}
