﻿namespace FullDrive.Module.Controllers
{
    partial class PurchaseReturnLineActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.PurchaseReturnLineTaxCalculationAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PurchaseReturnLineDiscCalculationAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // PurchaseReturnLineTaxCalculationAction
            // 
            this.PurchaseReturnLineTaxCalculationAction.Caption = "Tax Calculation";
            this.PurchaseReturnLineTaxCalculationAction.ConfirmationMessage = null;
            this.PurchaseReturnLineTaxCalculationAction.Id = "PurchaseReturnLineTaxCalculationActionId";
            this.PurchaseReturnLineTaxCalculationAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchaseReturnLine);
            this.PurchaseReturnLineTaxCalculationAction.ToolTip = null;
            this.PurchaseReturnLineTaxCalculationAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PurchaseReturnLineTaxCalculationAction_Execute);
            // 
            // PurchaseReturnLineDiscCalculationAction
            // 
            this.PurchaseReturnLineDiscCalculationAction.Caption = "Disc Calculation";
            this.PurchaseReturnLineDiscCalculationAction.ConfirmationMessage = null;
            this.PurchaseReturnLineDiscCalculationAction.Id = "PurchaseReturnLineDiscCalculationActionId";
            this.PurchaseReturnLineDiscCalculationAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchaseReturnLine);
            this.PurchaseReturnLineDiscCalculationAction.ToolTip = null;
            this.PurchaseReturnLineDiscCalculationAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PurchaseReturnLineDiscCalculationAction_Execute);
            // 
            // PurchaseReturnLineActionController
            // 
            this.Actions.Add(this.PurchaseReturnLineTaxCalculationAction);
            this.Actions.Add(this.PurchaseReturnLineDiscCalculationAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction PurchaseReturnLineTaxCalculationAction;
        private DevExpress.ExpressApp.Actions.SimpleAction PurchaseReturnLineDiscCalculationAction;
    }
}
