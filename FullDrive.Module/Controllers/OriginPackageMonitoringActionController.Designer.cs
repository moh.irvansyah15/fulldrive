﻿namespace FullDrive.Module.Controllers
{
    partial class OriginPackageMonitoringActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.OriginPackageMonitoringUnselectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.OriginPackageMonitoringSelectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // OriginPackageMonitoringUnselectAction
            // 
            this.OriginPackageMonitoringUnselectAction.Caption = "Unselect";
            this.OriginPackageMonitoringUnselectAction.ConfirmationMessage = null;
            this.OriginPackageMonitoringUnselectAction.Id = "OriginPackageMonitoringUnselectActionId";
            this.OriginPackageMonitoringUnselectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.OriginPackageMonitoring);
            this.OriginPackageMonitoringUnselectAction.ToolTip = null;
            this.OriginPackageMonitoringUnselectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.OriginPackageMonitoringUnselectAction_Execute);
            // 
            // OriginPackageMonitoringSelectAction
            // 
            this.OriginPackageMonitoringSelectAction.Caption = "Select";
            this.OriginPackageMonitoringSelectAction.ConfirmationMessage = null;
            this.OriginPackageMonitoringSelectAction.Id = "OriginPackageMonitoringSelectActionId";
            this.OriginPackageMonitoringSelectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.OriginPackageMonitoring);
            this.OriginPackageMonitoringSelectAction.ToolTip = null;
            this.OriginPackageMonitoringSelectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.OriginPackageMonitoringSelectAction_Execute);
            // 
            // OriginPackageMonitoringActionController
            // 
            this.Actions.Add(this.OriginPackageMonitoringUnselectAction);
            this.Actions.Add(this.OriginPackageMonitoringSelectAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction OriginPackageMonitoringUnselectAction;
        private DevExpress.ExpressApp.Actions.SimpleAction OriginPackageMonitoringSelectAction;
    }
}
