﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;
using System.Web;
using System.IO;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class SalesPrePaymentInvoiceActionController : ViewController
    {
        private ChoiceActionItem _selectionListviewFilter;
        private ChoiceActionItem _setApprovalLevel;

        public SalesPrePaymentInvoiceActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            SalesPrePaymentInvoiceListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                SalesPrePaymentInvoiceListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
        }
        protected override void OnActivated()
        {
            TypeOfView = typeof(ObjectView);
            base.OnActivated();
            // Perform various tasks depending on the target View.
            #region Approval
            var _locUser = SecuritySystem.CurrentUserName;
            GlobalFunction _globalFunction = new GlobalFunction();
            Session _currentSession = null;
            if (this.ObjectSpace != null)
            {
                _currentSession = ((XPObjectSpace)this.ObjectSpace).Session;
            }

            if (_currentSession != null)
            {
                UserAccess _locUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", _locUser));

                if (_locUserAccess != null)
                {
                    SalesPrePaymentInvoiceApprovalAction.Items.Clear();

                    XPCollection<ApplicationSetupDetail> _locAppSetupDetails = new XPCollection<ApplicationSetupDetail>
                                                                                 (_currentSession, new GroupOperator(GroupOperatorType.And,
                                                                                  new BinaryOperator("UserAccess", _locUserAccess),
                                                                                  new BinaryOperator("ObjectList", CustomProcess.ObjectList.SalesPrePaymentInvoice),
                                                                                  new BinaryOperator("FunctionList", CustomProcess.FunctionList.Approval),
                                                                                  new BinaryOperator("Active", true)));

                    if (_locAppSetupDetails != null && _locAppSetupDetails.Count > 0)
                    {

                        foreach (ApplicationSetupDetail _locAppSetupDetail in _locAppSetupDetails)
                        {
                            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.ApprovalLevel)))
                            {
                                if (_locAppSetupDetail.ApprovalLevel != CustomProcess.ApprovalLevel.None)
                                {
                                    EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.ApprovalLevel));
                                    _setApprovalLevel = new ChoiceActionItem(_ed.GetCaption(_locAppSetupDetail.ApprovalLevel), _locAppSetupDetail.ApprovalLevel);
                                }
                            }
                            SalesPrePaymentInvoiceApprovalAction.Items.Add(_setApprovalLevel);
                        }
                    }
                }
            }
            #endregion Approval
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void SalesPrePaymentInvoiceProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        SalesPrePaymentInvoice _locSalesPrePaymentInvoiceOS = (SalesPrePaymentInvoice)_objectSpace.GetObject(obj);

                        if (_locSalesPrePaymentInvoiceOS != null)
                        {
                            if (_locSalesPrePaymentInvoiceOS.Code != null)
                            {
                                _currObjectId = _locSalesPrePaymentInvoiceOS.Code;

                                SalesPrePaymentInvoice _locSalesPrePaymentInvoiceXPO = _currSession.FindObject<SalesPrePaymentInvoice>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId),
                                                                     new BinaryOperator("Status", Status.Open)));

                                if (_locSalesPrePaymentInvoiceXPO != null)
                                {
                                    _locSalesPrePaymentInvoiceXPO.Status = Status.Progress;
                                    _locSalesPrePaymentInvoiceXPO.StatusDate = now;
                                    _locSalesPrePaymentInvoiceXPO.Save();
                                    _locSalesPrePaymentInvoiceXPO.Session.CommitTransaction();
                                    SuccessMessageShow(_locSalesPrePaymentInvoiceXPO.Code + " has been change successfully to Progress");                                   
                                }
                                else
                                {
                                    ErrorMessageShow("Data SalesPrePaymentInvoice Not Available");
                                }

                            }
                            else
                            {
                                ErrorMessageShow("Data SalesPrePaymentInvoice Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesPrePaymentInvoice " + ex.ToString());
            }
        }

        private void SalesPrePaymentInvoiceGetBillAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        SalesPrePaymentInvoice _locSalesPrePaymentInvoiceOS = (SalesPrePaymentInvoice)_objectSpace.GetObject(obj);

                        if (_locSalesPrePaymentInvoiceOS != null)
                        {
                            if (_locSalesPrePaymentInvoiceOS.Code != null)
                            {
                                _currObjectId = _locSalesPrePaymentInvoiceOS.Code;

                                SalesPrePaymentInvoice _locSalesPrePaymentInvoiceXPO = _currSession.FindObject<SalesPrePaymentInvoice>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId),
                                                                     new BinaryOperator("Status", Status.Progress)));

                                if (_locSalesPrePaymentInvoiceXPO != null)
                                {
                                    GetBillOfSO(_currSession, _locSalesPrePaymentInvoiceXPO);
                                }
                                else
                                {
                                    ErrorMessageShow("Data Sales Pre Payment Invoice Not Available");
                                }

                            }
                            else
                            {
                                ErrorMessageShow("Data Sales Pre Payment Invoice Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesPrePaymentInvoice " + ex.ToString());
            }
        }

        private void SalesPrePaymentInvoicePostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;


                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        SalesPrePaymentInvoice _locSalesPrePaymentInvoiceOS = (SalesPrePaymentInvoice)_objectSpace.GetObject(obj);

                        if (_locSalesPrePaymentInvoiceOS != null)
                        {
                            if (_locSalesPrePaymentInvoiceOS.Code != null)
                            {
                                _currObjectId = _locSalesPrePaymentInvoiceOS.Code;

                                SalesPrePaymentInvoice _locSalesPrePaymentInvoiceXPO = _currSession.FindObject<SalesPrePaymentInvoice>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId),
                                                                     new GroupOperator(GroupOperatorType.Or,
                                                                     new BinaryOperator("Status", Status.Lock),
                                                                     new BinaryOperator("Status", Status.Posted))));

                                if (_locSalesPrePaymentInvoiceXPO != null)
                                {
                                    ApprovalLine _locApprovalLineXPO = _currSession.FindObject<ApprovalLine>
                                                                           (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("EndApproval", true),
                                                                            new BinaryOperator("SalesPrePaymentInvoice", _locSalesPrePaymentInvoiceXPO)));

                                    if (_locApprovalLineXPO != null)
                                    {
                                        if (_locSalesPrePaymentInvoiceXPO.DP_Amount > 0)
                                        {
                                            SetPrePaymentJournal(_currSession, _locSalesPrePaymentInvoiceXPO);
                                            SetPaymentInPlan(_currSession, _locSalesPrePaymentInvoiceXPO);
                                            SetFinalSalesPrePaymentInvoice(_currSession, _locSalesPrePaymentInvoiceXPO);
                                            SuccessMessageShow(_locSalesPrePaymentInvoiceXPO.Code + " has been change successfully to Posted");
                                        }
                                    }
                                        
                                }
                                else
                                {
                                    ErrorMessageShow("Data SalesPrePaymentInvoice Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data SalesPrePaymentInvoice Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchasePrePaymentInvoice " + ex.ToString());
            }
        }

        private void SalesPrePaymentInvoiceListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(SalesPrePaymentInvoice)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesPrePaymentInvoice " + ex.ToString());
            }
        }

        private void SalesPrePaymentInvoiceApprovalAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                ApplicationSetupDetail _locAppSetDetail = null;
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                var User = SecuritySystem.CurrentUserName;
                Session _currentSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currentSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                UserAccess _locUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", User));

                foreach (Object obj in _objectsToProcess)
                {
                    SalesPrePaymentInvoice _objInNewObjectSpace = (SalesPrePaymentInvoice)_objectSpace.GetObject(obj);

                    if (_objInNewObjectSpace != null)
                    {
                        if (_objInNewObjectSpace.Code != null)
                        {
                            _currObjectId = _objInNewObjectSpace.Code;
                        }
                    }

                    if (_currObjectId != null)
                    {
                        SalesPrePaymentInvoice _locSalesPrePaymentInvoiceXPO = _currentSession.FindObject<SalesPrePaymentInvoice>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("Code", _currObjectId)));

                        if (_locSalesPrePaymentInvoiceXPO != null)
                        {
                            if (_locSalesPrePaymentInvoiceXPO.Status == Status.Progress || _locSalesPrePaymentInvoiceXPO.Status == Status.Lock)
                            {
                                ApprovalLine _locApprovalLine = _currentSession.FindObject<ApprovalLine>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("SalesPrePaymentInvoice", _locSalesPrePaymentInvoiceXPO),
                                                                             new BinaryOperator("EndApproval", true)));
                                if (_locApprovalLine == null)
                                {
                                    #region Approval Level 1
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level1)
                                    {
                                        _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level1, ObjectList.SalesPrePaymentInvoice);
                                        if (_locAppSetDetail != null)
                                        {
                                            //Buat bs input langsung ke approvalline
                                            ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("SalesPrePaymentInvoice", _locSalesPrePaymentInvoiceXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level1)));
                                            if (_locApprovalLineXPO == null)
                                            {

                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level1,
                                                        EndApproval = true,
                                                        SalesPrePaymentInvoice = _locSalesPrePaymentInvoiceXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locSalesPrePaymentInvoiceXPO.ActivationPosting = true;
                                                    _locSalesPrePaymentInvoiceXPO.ActiveApproved1 = false;
                                                    _locSalesPrePaymentInvoiceXPO.ActiveApproved2 = false;
                                                    _locSalesPrePaymentInvoiceXPO.ActiveApproved3 = true;
                                                    _locSalesPrePaymentInvoiceXPO.Save();
                                                    _locSalesPrePaymentInvoiceXPO.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level1,
                                                        SalesPrePaymentInvoice = _locSalesPrePaymentInvoiceXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locSalesPrePaymentInvoiceXPO.ActiveApproved1 = true;
                                                    _locSalesPrePaymentInvoiceXPO.ActiveApproved2 = false;
                                                    _locSalesPrePaymentInvoiceXPO.ActiveApproved3 = false;
                                                    _locSalesPrePaymentInvoiceXPO.Save();
                                                    _locSalesPrePaymentInvoiceXPO.Session.CommitTransaction();
                                                }

                                                //Send Email
                                                ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("SalesPrePaymentInvoice", _locSalesPrePaymentInvoiceXPO)));

                                                if (_locApprovalLineXPO2 != null)
                                                {
                                                    SendEmail(_currentSession, _locSalesPrePaymentInvoiceXPO, Status.Progress, _locApprovalLineXPO2);
                                                }

                                                SuccessMessageShow("SalesPrePaymentInvoice has successfully Approve");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 1

                                    #region Approval Level 2
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level2)
                                    {
                                        _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level2, ObjectList.SalesPrePaymentInvoice);

                                        if (_locAppSetDetail != null)
                                        {
                                            ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("SalesPrePaymentInvoice", _locSalesPrePaymentInvoiceXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level2)));

                                            if (_locApprovalLineXPO == null)
                                            {
                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level2,
                                                        EndApproval = true,
                                                        SalesPrePaymentInvoice = _locSalesPrePaymentInvoiceXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locSalesPrePaymentInvoiceXPO.ActivationPosting = true;
                                                    _locSalesPrePaymentInvoiceXPO.ActiveApproved1 = false;
                                                    _locSalesPrePaymentInvoiceXPO.ActiveApproved2 = false;
                                                    _locSalesPrePaymentInvoiceXPO.ActiveApproved3 = true;
                                                    _locSalesPrePaymentInvoiceXPO.Save();
                                                    _locSalesPrePaymentInvoiceXPO.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level2,
                                                        SalesPrePaymentInvoice = _locSalesPrePaymentInvoiceXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    SetApprovalLine(_currentSession, _locSalesPrePaymentInvoiceXPO, ApprovalLevel.Level1);

                                                    _locSalesPrePaymentInvoiceXPO.ActiveApproved1 = false;
                                                    _locSalesPrePaymentInvoiceXPO.ActiveApproved2 = true;
                                                    _locSalesPrePaymentInvoiceXPO.ActiveApproved3 = false;
                                                    _locSalesPrePaymentInvoiceXPO.Save();
                                                    _locSalesPrePaymentInvoiceXPO.Session.CommitTransaction();
                                                }

                                                //Send Email
                                                ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("SalesPrePaymentInvoice", _locSalesPrePaymentInvoiceXPO)));

                                                if (_locApprovalLineXPO2 != null)
                                                {
                                                    SendEmail(_currentSession, _locSalesPrePaymentInvoiceXPO, Status.Progress, _locApprovalLineXPO2);
                                                }

                                                SuccessMessageShow("SalesPrePaymentInvoice has successfully Approve");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 2

                                    #region Approval Level 3
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level3)
                                    {
                                        _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level3, ObjectList.SalesPrePaymentInvoice);

                                        if (_locAppSetDetail != null)
                                        {
                                            ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("SalesPrePaymentInvoice", _locSalesPrePaymentInvoiceXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level3)));

                                            if (_locApprovalLineXPO == null)
                                            {
                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level3,
                                                        EndApproval = true,
                                                        SalesPrePaymentInvoice = _locSalesPrePaymentInvoiceXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locSalesPrePaymentInvoiceXPO.ActivationPosting = true;
                                                    _locSalesPrePaymentInvoiceXPO.ActiveApproved1 = false;
                                                    _locSalesPrePaymentInvoiceXPO.ActiveApproved2 = false;
                                                    _locSalesPrePaymentInvoiceXPO.ActiveApproved3 = true;
                                                    _locSalesPrePaymentInvoiceXPO.Save();
                                                    _locSalesPrePaymentInvoiceXPO.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level3,
                                                        SalesPrePaymentInvoice = _locSalesPrePaymentInvoiceXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    SetApprovalLine(_currentSession, _locSalesPrePaymentInvoiceXPO, ApprovalLevel.Level2);
                                                    SetApprovalLine(_currentSession, _locSalesPrePaymentInvoiceXPO, ApprovalLevel.Level1);

                                                    _locSalesPrePaymentInvoiceXPO.ActiveApproved1 = false;
                                                    _locSalesPrePaymentInvoiceXPO.ActiveApproved2 = false;
                                                    _locSalesPrePaymentInvoiceXPO.ActiveApproved3 = true;
                                                    _locSalesPrePaymentInvoiceXPO.Save();
                                                    _locSalesPrePaymentInvoiceXPO.Session.CommitTransaction();
                                                }

                                                //Send Email
                                                ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("SalesPrePaymentInvoice", _locSalesPrePaymentInvoiceXPO)));

                                                if (_locApprovalLineXPO2 != null)
                                                {
                                                    SendEmail(_currentSession, _locSalesPrePaymentInvoiceXPO, Status.Progress, _locApprovalLineXPO2);
                                                }

                                                SuccessMessageShow("SalesPrePaymentInvoice has successfully Approve");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 3
                                }

                            }
                            else
                            {
                                ErrorMessageShow("SalesInvoice Status Not Available");
                            }
                        }
                        else
                        {
                            ErrorMessageShow("SalesInvoice Not Available");
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrder " + ex.ToString());
            }
        }

        private void SalesPrePaymentInvoiceSelectAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        SalesPrePaymentInvoice _locSalesPrePaymentInvoiceOS = (SalesPrePaymentInvoice)_objectSpace.GetObject(obj);

                        if (_locSalesPrePaymentInvoiceOS != null)
                        {
                            if (_locSalesPrePaymentInvoiceOS.Code != null)
                            {
                                _currObjectId = _locSalesPrePaymentInvoiceOS.Code;

                                XPCollection<SalesPrePaymentInvoice> _locSalesPrePaymentInvoices = new XPCollection<SalesPrePaymentInvoice>
                                                                                                     (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                      new BinaryOperator("Code", _currObjectId),
                                                                                                      new GroupOperator(GroupOperatorType.Or,
                                                                                                      new BinaryOperator("Status", Status.Progress),
                                                                                                      new BinaryOperator("Status", Status.Open)
                                                                                                      )));

                                if (_locSalesPrePaymentInvoices != null && _locSalesPrePaymentInvoices.Count > 0)
                                {
                                    foreach (SalesPrePaymentInvoice _locSalesPrePaymentInvoice in _locSalesPrePaymentInvoices)
                                    {
                                        _locSalesPrePaymentInvoice.Select = true;
                                        _locSalesPrePaymentInvoice.Save();
                                        _locSalesPrePaymentInvoice.Session.CommitTransaction();
                                    }

                                    SuccessMessageShow("SalesPrePaymentInvoice has been Select");
                                }
                                else
                                {
                                    ErrorMessageShow("Data SalesPrePaymentInvoice Not Available");
                                }
                            }
                        }
                    }
                    if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                    {
                        _objectSpace.CommitChanges();
                        _objectSpace.Refresh();
                    }
                    if (View is ListView)
                    {
                        _objectSpace.CommitChanges();
                        View.ObjectSpace.Refresh();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = SalesPrePaymentInvoice" + ex.ToString());
            }
        }

        private void SalesPrePaymentInvoiceUnselectAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        SalesPrePaymentInvoice _locSalesPrePaymentInvoiceOS = (SalesPrePaymentInvoice)_objectSpace.GetObject(obj);

                        if (_locSalesPrePaymentInvoiceOS != null)
                        {
                            if (_locSalesPrePaymentInvoiceOS.Code != null)
                            {
                                _currObjectId = _locSalesPrePaymentInvoiceOS.Code;

                                XPCollection<SalesPrePaymentInvoice> _locSalesPrePaymentInvoices = new XPCollection<SalesPrePaymentInvoice>
                                                                                                     (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                      new BinaryOperator("Code", _currObjectId),
                                                                                                      new GroupOperator(GroupOperatorType.Or,
                                                                                                      new BinaryOperator("Status", Status.Progress),
                                                                                                      new BinaryOperator("Status", Status.Open)
                                                                                                      )));

                                if (_locSalesPrePaymentInvoices != null && _locSalesPrePaymentInvoices.Count > 0)
                                {
                                    foreach (SalesPrePaymentInvoice _locSalesPrePaymentInvoice in _locSalesPrePaymentInvoices)
                                    {
                                        _locSalesPrePaymentInvoice.Select = false;
                                        _locSalesPrePaymentInvoice.Save();
                                        _locSalesPrePaymentInvoice.Session.CommitTransaction();
                                    }

                                    SuccessMessageShow("SalesPrePaymentInvoice has been Unselect");
                                }
                                else
                                {
                                    ErrorMessageShow("Data SalesPrePaymentInvoice Not Available");
                                }
                            }
                        }
                    }
                    if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                    {
                        _objectSpace.CommitChanges();
                        _objectSpace.Refresh();
                    }
                    if (View is ListView)
                    {
                        _objectSpace.CommitChanges();
                        View.ObjectSpace.Refresh();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = SalesPrePaymentInvoice" + ex.ToString());
            }
        }

        //======================================= Code Only =======================================================

        #region GetSOBill

        private void GetBillOfSO(Session _currSession, SalesPrePaymentInvoice _locSalesPrePaymentInvoiceXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                double _locMaksBill = 0;
                double _locTotMaksBill = 0;
                if(_locSalesPrePaymentInvoiceXPO != null && _locSalesPrePaymentInvoiceXPO.SalesOrder != null)
                {
                    //Cek Payment In Plan
                    XPCollection<PaymentInPlan> _locPaymentInPlans = new XPCollection<PaymentInPlan>
                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("SalesOrder", _locSalesPrePaymentInvoiceXPO.SalesOrder)));
                    if(_locPaymentInPlans != null && _locPaymentInPlans.Count() > 0)
                    {
                        foreach(PaymentInPlan _locPaymentInPlan in _locPaymentInPlans)
                        {
                            if(_locPaymentInPlan.Plan > 0)
                            {
                                _locMaksBill = _locMaksBill + _locPaymentInPlan.Plan;
                            }
                        }

                        if(_locMaksBill <= _locSalesPrePaymentInvoiceXPO.SalesOrder.MaxBill)
                        {
                            _locTotMaksBill = _locSalesPrePaymentInvoiceXPO.SalesOrder.MaxBill - _locMaksBill;
                        }
                    }
                    else
                    {
                        _locTotMaksBill = _locSalesPrePaymentInvoiceXPO.SalesOrder.MaxBill;
                    }

                    _locSalesPrePaymentInvoiceXPO.MaxBill = _locTotMaksBill;
                    _locSalesPrePaymentInvoiceXPO.Bill = _locTotMaksBill;
                    _locSalesPrePaymentInvoiceXPO.DP_Percentage = _locSalesPrePaymentInvoiceXPO.SalesOrder.DP_Percentage;
                    _locSalesPrePaymentInvoiceXPO.Status = Status.Lock;
                    _locSalesPrePaymentInvoiceXPO.StatusDate = now;
                    _locSalesPrePaymentInvoiceXPO.Save();
                    _locSalesPrePaymentInvoiceXPO.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = SalesPrePaymentInvoice ", ex.ToString());
            }
        }

        #endregion GetSOBill

        #region PostingMethod

        private void SetPrePaymentJournal(Session _currSession, SalesPrePaymentInvoice _locSalesPrePaymentInvoiceXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                double _locDP = 0;

                #region CreateInvoiceAPJournalDownPayment

                if (_locSalesPrePaymentInvoiceXPO.DP_Amount > 0)
                {
                    _locDP = _locSalesPrePaymentInvoiceXPO.DP_Amount;

                    #region JournalMapCompanyAccountGroup
                    if (_locSalesPrePaymentInvoiceXPO.Company != null)
                    {
                        if (_locSalesPrePaymentInvoiceXPO.Company.CompanyAccountGroup != null)
                        {
                            XPCollection<JournalMap> _locJournalMapByCompanys = new XPCollection<JournalMap>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                         new BinaryOperator("CompanyAccountGroup", _locSalesPrePaymentInvoiceXPO.Company.CompanyAccountGroup)));

                            if (_locJournalMapByCompanys != null && _locJournalMapByCompanys.Count() > 0)
                            {
                                foreach (JournalMap _locJournalMapByCompany in _locJournalMapByCompanys)
                                {
                                    XPCollection<JournalMapLine> _locJournalMapLineByCompanys = new XPCollection<JournalMapLine>
                                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                         new BinaryOperator("JournalMap", _locJournalMapByCompany)));

                                    if (_locJournalMapLineByCompanys != null && _locJournalMapLineByCompanys.Count() > 0)
                                    {
                                        foreach (JournalMapLine _locJournalMapLineByCompany in _locJournalMapLineByCompanys)
                                        {
                                            AccountMap _locAccountMapByBusinessPartner = _currSession.FindObject<AccountMap>
                                                                                         (new GroupOperator(GroupOperatorType.And,
                                                                                          new BinaryOperator("Code", _locJournalMapLineByCompany.AccountMap.Code),
                                                                                          new BinaryOperator("PostingType", PostingType.Sales),
                                                                                          new BinaryOperator("PostingMethod", PostingMethod.PrePayment),
                                                                                          new BinaryOperator("PostingMethodType", PostingMethodType.DownPayment)));

                                            if (_locAccountMapByBusinessPartner != null)
                                            {
                                                XPCollection<AccountMapLine> _locAccountMapLineByBusinessPartners = new XPCollection<AccountMapLine>
                                                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                     new BinaryOperator("AccountMap", _locAccountMapByBusinessPartner),
                                                                                                                     new BinaryOperator("Active", true)));

                                                if (_locAccountMapLineByBusinessPartners != null && _locAccountMapLineByBusinessPartners.Count() > 0)
                                                {
                                                    double _locTotalAmountDebit = 0;
                                                    double _locTotalAmountCredit = 0;
                                                    double _locTotalBalance = 0;

                                                    foreach (AccountMapLine _locAccountMapLine in _locAccountMapLineByBusinessPartners)
                                                    {
                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                        {
                                                            _locTotalAmountDebit = _locDP;
                                                        }
                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                        {
                                                            _locTotalAmountCredit = _locDP;
                                                        }

                                                        GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                        {
                                                            PostingDate = now,
                                                            PostingType = PostingType.Sales,
                                                            PostingMethod = PostingMethod.PrePayment,
                                                            Account = _locAccountMapLine.Account,
                                                            Debit = _locTotalAmountDebit,
                                                            Credit = _locTotalAmountCredit,
                                                            SalesOrder = _locSalesPrePaymentInvoiceXPO.SalesOrder,
                                                            SalesPrePaymentInvoice = _locSalesPrePaymentInvoiceXPO,
                                                            Company = _locSalesPrePaymentInvoiceXPO.Company,
                                                        };
                                                        _saveGeneralJournal.Save();
                                                        _saveGeneralJournal.Session.CommitTransaction();

                                                        if (_locAccountMapLine.Account.Code != null)
                                                        {
                                                            ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                     (new GroupOperator(GroupOperatorType.And,
                                                                                      new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                                                            if (_locCOA != null)
                                                            {
                                                                if (_locCOA.BalanceType == BalanceType.Change)
                                                                {
                                                                    if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                    {
                                                                        if (_locTotalAmountDebit > 0)
                                                                        {
                                                                            _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                        }
                                                                        if (_locTotalAmountCredit > 0)
                                                                        {
                                                                            _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                        }
                                                                    }
                                                                    if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                    {
                                                                        if (_locTotalAmountDebit > 0)
                                                                        {
                                                                            _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                        }
                                                                        if (_locTotalAmountCredit > 0)
                                                                        {
                                                                            _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                        }
                                                                    }
                                                                }

                                                                _locCOA.Balance = _locTotalBalance;
                                                                _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                _locCOA.Save();
                                                                _locCOA.Session.CommitTransaction();
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    #endregion JournalMapCompanyAccountGroup
                    #region JournalMapBusinessPartnerAccountGroup
                    if (_locSalesPrePaymentInvoiceXPO.SalesToCustomer != null)
                    {
                        if (_locSalesPrePaymentInvoiceXPO.SalesToCustomer.BusinessPartnerAccountGroup != null)
                        {
                            XPCollection<JournalMap> _locJournalMapByBusinessPartners = new XPCollection<JournalMap>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                         new BinaryOperator("BusinessPartnerAccountGroup", _locSalesPrePaymentInvoiceXPO.SalesToCustomer.BusinessPartnerAccountGroup)));

                            if (_locJournalMapByBusinessPartners != null && _locJournalMapByBusinessPartners.Count() > 0)
                            {
                                foreach (JournalMap _locJournalMapByBusinessPartner in _locJournalMapByBusinessPartners)
                                {
                                    XPCollection<JournalMapLine> _locJournalMapLineByBusinessPartners = new XPCollection<JournalMapLine>
                                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                         new BinaryOperator("JournalMap", _locJournalMapByBusinessPartner)));

                                    if (_locJournalMapLineByBusinessPartners != null && _locJournalMapLineByBusinessPartners.Count() > 0)
                                    {
                                        foreach (JournalMapLine _locJournalMapLineByBusinessPartner in _locJournalMapLineByBusinessPartners)
                                        {
                                            AccountMap _locAccountMapByBusinessPartner = _currSession.FindObject<AccountMap>
                                                                                         (new GroupOperator(GroupOperatorType.And,
                                                                                          new BinaryOperator("Code", _locJournalMapLineByBusinessPartner.AccountMap.Code),
                                                                                          new BinaryOperator("PostingType", PostingType.Sales),
                                                                                          new BinaryOperator("PostingMethod", PostingMethod.PrePayment),
                                                                                          new BinaryOperator("PostingMethodType", PostingMethodType.DownPayment)));

                                            if (_locAccountMapByBusinessPartner != null)
                                            {
                                                XPCollection<AccountMapLine> _locAccountMapLineByBusinessPartners = new XPCollection<AccountMapLine>
                                                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                     new BinaryOperator("AccountMap", _locAccountMapByBusinessPartner),
                                                                                                                     new BinaryOperator("Active", true)));

                                                if (_locAccountMapLineByBusinessPartners != null && _locAccountMapLineByBusinessPartners.Count() > 0)
                                                {
                                                    double _locTotalAmountDebit = 0;
                                                    double _locTotalAmountCredit = 0;
                                                    double _locTotalBalance = 0;

                                                    foreach (AccountMapLine _locAccountMapLine in _locAccountMapLineByBusinessPartners)
                                                    {
                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                        {
                                                            _locTotalAmountDebit = _locDP;
                                                        }
                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                        {
                                                            _locTotalAmountCredit = _locDP;
                                                        }

                                                        GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                        {
                                                            PostingDate = now,
                                                            PostingType = PostingType.Sales,
                                                            PostingMethod = PostingMethod.PrePayment,
                                                            Account = _locAccountMapLine.Account,
                                                            Debit = _locTotalAmountDebit,
                                                            Credit = _locTotalAmountCredit,
                                                            SalesOrder = _locSalesPrePaymentInvoiceXPO.SalesOrder,
                                                            SalesPrePaymentInvoice = _locSalesPrePaymentInvoiceXPO,
                                                            Company = _locSalesPrePaymentInvoiceXPO.Company,
                                                        };
                                                        _saveGeneralJournal.Save();
                                                        _saveGeneralJournal.Session.CommitTransaction();

                                                        if (_locAccountMapLine.Account.Code != null)
                                                        {
                                                            ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                     (new GroupOperator(GroupOperatorType.And,
                                                                                      new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                                                            if (_locCOA != null)
                                                            {
                                                                if (_locCOA.BalanceType == BalanceType.Change)
                                                                {
                                                                    if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                    {
                                                                        if (_locTotalAmountDebit > 0)
                                                                        {
                                                                            _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                        }
                                                                        if (_locTotalAmountCredit > 0)
                                                                        {
                                                                            _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                        }
                                                                    }
                                                                    if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                    {
                                                                        if (_locTotalAmountDebit > 0)
                                                                        {
                                                                            _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                        }
                                                                        if (_locTotalAmountCredit > 0)
                                                                        {
                                                                            _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                        }
                                                                    }
                                                                }

                                                                _locCOA.Balance = _locTotalBalance;
                                                                _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                _locCOA.Save();
                                                                _locCOA.Session.CommitTransaction();
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    #endregion JournalMapBusinessPartnerAccountGroup
                }

                #endregion CreateInvoiceAPJournalDownPayment

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = SalesPrePaymentInvoice ", ex.ToString());
            }
        }

        private void SetPaymentInPlan(Session _currSession, SalesPrePaymentInvoice _locSalesPrePaymentInvoiceXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                double _locTotalUnitPrice = _locSalesPrePaymentInvoiceXPO.Bill;
                

                if (_locSalesPrePaymentInvoiceXPO != null )
                {
                    if (_locSalesPrePaymentInvoiceXPO.SalesOrder != null && _locSalesPrePaymentInvoiceXPO.DP_Amount > 0)
                    {
                        PaymentInPlan _savePaymentInPlan = new PaymentInPlan(_currSession)
                        {
                            PaymentMethod = _locSalesPrePaymentInvoiceXPO.PaymentMethod,
                            PaymentMethodType = _locSalesPrePaymentInvoiceXPO.PaymentMethodType,
                            PaymentType = _locSalesPrePaymentInvoiceXPO.PaymentType,
                            EstimatedDate = _locSalesPrePaymentInvoiceXPO.EstimatedDate,
                            Plan = _locSalesPrePaymentInvoiceXPO.DP_Amount,
                            Outstanding = _locSalesPrePaymentInvoiceXPO.DP_Amount,
                            SalesOrder = _locSalesPrePaymentInvoiceXPO.SalesOrder,
                            SalesPrePaymentInvoice = _locSalesPrePaymentInvoiceXPO,
                            Company = _locSalesPrePaymentInvoiceXPO.Company,
                        };
                        _savePaymentInPlan.Save();
                        _savePaymentInPlan.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = SalesPrePaymentInvoice ", ex.ToString());
            }
        }

        private void SetFinalSalesPrePaymentInvoice(Session _currSession, SalesPrePaymentInvoice _locSalesPrePaymentInvoiceXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                
                if (_locSalesPrePaymentInvoiceXPO != null)
                {
                    _locSalesPrePaymentInvoiceXPO.ActivationPosting = true;
                    _locSalesPrePaymentInvoiceXPO.Status = Status.Posted;
                    _locSalesPrePaymentInvoiceXPO.StatusDate = now;
                    _locSalesPrePaymentInvoiceXPO.Save();
                    _locSalesPrePaymentInvoiceXPO.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesPrePaymentInvoice " + ex.ToString());
            }
        }

        #endregion PostingMethod

        #region Approval Process

        private void SetApprovalLine(Session _currentSession, SalesPrePaymentInvoice _locSalesPrePaymentInvoiceXPO, ApprovalLevel _locApprovalLevel)
        {
            try
            {
                DateTime now = DateTime.Now;

                ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("SalesPrePaymentInvoice", _locSalesPrePaymentInvoiceXPO),
                                                                         new BinaryOperator("ApprovalLevel", _locApprovalLevel)));

                if (_locApprovalLineXPO == null)
                {
                    ApprovalLine _saveDataAL2 = new ApprovalLine(_currentSession)
                    {
                        ApprovalDate = now,
                        ApprovalStatus = Status.Approved,
                        ApprovalLevel = _locApprovalLevel,
                        SalesPrePaymentInvoice = _locSalesPrePaymentInvoiceXPO,
                    };
                    _saveDataAL2.Save();
                    _saveDataAL2.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesPrePaymentInvoice " + ex.ToString());
            }
        }

        #endregion Approval Process

        #region Email

        private string BackgroundBody(Session _currentSession, SalesPrePaymentInvoice _locSalesPrePaymentInvoiceXPO, MailSetupDetail _locMailSetDetail)
        {
            string body = string.Empty;

            SalesPrePaymentInvoice _locSalesPrePaymentInvoice = _currentSession.FindObject<SalesPrePaymentInvoice>
                                           (new GroupOperator(GroupOperatorType.And,
                                            new BinaryOperator("Code", _locSalesPrePaymentInvoiceXPO.Code)));

            ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                               (new GroupOperator(GroupOperatorType.And,
                                                new BinaryOperator("SalesInvoice", _locSalesPrePaymentInvoice)));

            string Status = _locApprovalLineXPO.ApprovalStatus.ToString();

            using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Email/EmailTemplate.html")))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{Code}", _locSalesPrePaymentInvoiceXPO.Code);

            #region Level
            if (_locSalesPrePaymentInvoice.ActiveApproved1 == true)
            {
                string Active = "Level 1";
                body = body.Replace("{Level}", Active);
            }
            else if (_locSalesPrePaymentInvoice.ActiveApproved2 == true)
            {
                string Active = "Level 2";
                body = body.Replace("{Level}", Active);
            }
            else if (_locSalesPrePaymentInvoice.ActiveApproved3 == true)
            {
                string Active = "Level 3";
                body = body.Replace("{Level}", Active);
            }
            else
            {
                string Active = "Not Available";
                body = body.Replace("{Level}", Active);
            }
            #endregion Level

            body = body.Replace("{Status}", Status);
            body = body.Replace("{Message}", _locMailSetDetail.MessageBody);
            return body;
        }

        protected void SendEmail(Session _currentSession, SalesPrePaymentInvoice _locSalesPrePaymentInvoiceXPO, Status _locStatus, ApprovalLine _locApprovalLineXPO2)
        {
            DateTime now = DateTime.Now;
            var User = SecuritySystem.CurrentUserName;
            GlobalFunction _globFunc = new GlobalFunction();
            UserAccess _numLineUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", User));
            string _locMessageBody = null;

            try
            {
                if (_locSalesPrePaymentInvoiceXPO != null)
                {
                    XPCollection<MailSetupDetail> _locMailSetDetails = new XPCollection<MailSetupDetail>
                                                                       (_currentSession, new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("ObjectList", ObjectList.SalesOrder),
                                                                        new BinaryOperator("UserAccess", _numLineUserAccess),
                                                                        new BinaryOperator("Active", true)));

                    if (_locMailSetDetails != null && _locMailSetDetails.Count > 0)
                    {
                        foreach (MailSetupDetail _locMailSetDetail in _locMailSetDetails)
                        {
                            if (_locMailSetDetail.MessageBody != null)
                            {
                                if (_locApprovalLineXPO2.EndApproval == true)
                                {
                                    _locMessageBody = this.BackgroundBody(_currentSession, _locSalesPrePaymentInvoiceXPO, _locMailSetDetail);
                                }
                                else
                                {
                                    _locMessageBody = this.BackgroundBody(_currentSession, _locSalesPrePaymentInvoiceXPO, _locMailSetDetail);
                                }
                            }

                            _globFunc.SetAndSendMail(_locMailSetDetail.SmtpHost, _locMailSetDetail.SmtpPort,
                            _locMailSetDetail.MailFrom, _locMailSetDetail.MailFromPassword, _locMailSetDetail.MailTo, _locMailSetDetail.MailSubject, _locMessageBody);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesPrePaymentInvoice " + ex.ToString());
            }
        }

        #endregion Email

        #region Global Method

        private void SuccessMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 4000;
            options.Message = string.Format("{0}", _locMessage);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }





        #endregion Global Method

        
    }
}
