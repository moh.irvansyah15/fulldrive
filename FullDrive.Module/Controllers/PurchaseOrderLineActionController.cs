﻿#region Default

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

#endregion Default

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class PurchaseOrderLineActionController : ViewController
    {
        #region Default

        private ChoiceActionItem _selectionListviewFilter;
        public PurchaseOrderLineActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.

            #region FilterStatus
            PurchaseOrderLineListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                if (_ed.GetCaption(_currApproval) != "Approved" && _ed.GetCaption(_currApproval) != "Lock")
                {
                    _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                }
                PurchaseOrderLineListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
            #endregion FilterStatus
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        #endregion Default

        private void PurchaseOrderLineTaxCalculationAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        PurchaseOrderLine _locPurchaseOrderLineOS = (PurchaseOrderLine)_objectSpace.GetObject(obj);

                        if (_locPurchaseOrderLineOS != null)
                        {
                            if (_locPurchaseOrderLineOS.TQty != 0 && _locPurchaseOrderLineOS.UAmount != 0)
                            {
                                if (_locPurchaseOrderLineOS.Code != null)
                                {
                                    _currObjectId = _locPurchaseOrderLineOS.Code;

                                    PurchaseOrderLine _locPurchaseOrderLineXPO = _currSession.FindObject<PurchaseOrderLine>
                                                                           (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Code", _currObjectId)));

                                    if (_locPurchaseOrderLineXPO != null)
                                    {
                                        GetSumTotalTax(_currSession, _locPurchaseOrderLineXPO);
                                        GetTotalPPNPPH(_currSession, _locPurchaseOrderLineXPO);
                                    }
                                }
                            }
                        }
                    }
                }
                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PurchaseOrderLine" + ex.ToString());
            }
        }

        private void PurchaseOrderLineDiscountCalculationAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        PurchaseOrderLine _locPurchaseOrderLineOS = (PurchaseOrderLine)_objectSpace.GetObject(obj);

                        if (_locPurchaseOrderLineOS != null)
                        {
                            if (_locPurchaseOrderLineOS.TQty != 0 && _locPurchaseOrderLineOS.UAmount != 0)
                            {
                                if (_locPurchaseOrderLineOS.Code != null)
                                {
                                    _currObjectId = _locPurchaseOrderLineOS.Code;

                                    PurchaseOrderLine _locPurchaseOrderLineXPO = _currSession.FindObject<PurchaseOrderLine>
                                                                           (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Code", _currObjectId)));

                                    if (_locPurchaseOrderLineXPO != null)
                                    {
                                        GetSumTotalDisc(_currSession, _locPurchaseOrderLineXPO);
                                    }
                                }
                            }
                        }
                    }
                }
                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PurchaseOrderLine" + ex.ToString());
            }
        }

        private void PurchaseOrderLineListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(PurchaseOrderLine)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseOrderLine " + ex.ToString());
            }
        }

        private void PurchaseOrderLineSelectAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        PurchaseOrderLine _locPurchaseOrderLineOS = (PurchaseOrderLine)_objectSpace.GetObject(obj);

                        if (_locPurchaseOrderLineOS != null)
                        {
                            if (_locPurchaseOrderLineOS.Code != null)
                            {
                                _currObjectId = _locPurchaseOrderLineOS.Code;

                                XPCollection<PurchaseOrderLine> _locPurchaseOrderLines = new XPCollection<PurchaseOrderLine>
                                                                                                     (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                      new BinaryOperator("Code", _currObjectId),
                                                                                                      new GroupOperator(GroupOperatorType.Or,
                                                                                                      new BinaryOperator("Status", Status.Posted),
                                                                                                      new BinaryOperator("Status", Status.Progress),
                                                                                                      new BinaryOperator("Status", Status.Open)
                                                                                                      )));

                                if (_locPurchaseOrderLines != null && _locPurchaseOrderLines.Count > 0)
                                {
                                    foreach (PurchaseOrderLine _locPurchaseOrderLine in _locPurchaseOrderLines)
                                    {
                                        _locPurchaseOrderLine.Select = true;
                                        _locPurchaseOrderLine.Save();
                                        _locPurchaseOrderLine.Session.CommitTransaction();
                                    }

                                    SuccessMessageShow("Purchase Order Line has been Select");
                                }
                                else
                                {
                                    ErrorMessageShow("Data Purchase Order Line Not Available");
                                }
                            }
                        }
                    }
                    if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                    {
                        _objectSpace.CommitChanges();
                        _objectSpace.Refresh();
                    }
                    if (View is ListView)
                    {
                        _objectSpace.CommitChanges();
                        View.ObjectSpace.Refresh();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PurchaseOrderLine" + ex.ToString());
            }
        }

        private void PurchaseOrderLineUnselectAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        PurchaseOrderLine _locPurchaseOrderLineOS = (PurchaseOrderLine)_objectSpace.GetObject(obj);

                        if (_locPurchaseOrderLineOS != null)
                        {
                            if (_locPurchaseOrderLineOS.Code != null)
                            {
                                _currObjectId = _locPurchaseOrderLineOS.Code;

                                XPCollection<PurchaseOrderLine> _locPurchaseOrderLines = new XPCollection<PurchaseOrderLine>
                                                                                                     (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                      new BinaryOperator("Code", _currObjectId),
                                                                                                      new GroupOperator(GroupOperatorType.Or,
                                                                                                      new BinaryOperator("Status", Status.Posted),
                                                                                                      new BinaryOperator("Status", Status.Progress),
                                                                                                      new BinaryOperator("Status", Status.Open)
                                                                                                      )));

                                if (_locPurchaseOrderLines != null && _locPurchaseOrderLines.Count > 0)
                                {
                                    foreach (PurchaseOrderLine _locPurchaseOrderLine in _locPurchaseOrderLines)
                                    {
                                        _locPurchaseOrderLine.Select = false;
                                        _locPurchaseOrderLine.Save();
                                        _locPurchaseOrderLine.Session.CommitTransaction();
                                    }

                                    SuccessMessageShow("Purchase Order Line has been Unselect");
                                }
                                else
                                {
                                    ErrorMessageShow("Data Purchase Order Line Not Available");
                                }
                            }
                        }
                    }
                    if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                    {
                        _objectSpace.CommitChanges();
                        _objectSpace.Refresh();
                    }
                    if (View is ListView)
                    {
                        _objectSpace.CommitChanges();
                        View.ObjectSpace.Refresh();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PurchaseOrderLine" + ex.ToString());
            }
        }

        //==== Code Only ====

        #region Get

        private void GetSumTotalTax(Session _currSession, PurchaseOrderLine _locPurchaseOrderLineXPO)
        {
            try
            {
                double _locTxAmount = 0;
                double _locTxValue = 0;

                if (_locPurchaseOrderLineXPO != null)
                {
                    XPCollection<TaxLine> _locTaxLines = new XPCollection<TaxLine>(_currSession,
                                                     new BinaryOperator("PurchaseOrderLine", _locPurchaseOrderLineXPO));

                    if (_locTaxLines != null && _locTaxLines.Count() > 0)
                    {
                        foreach (TaxLine _locTaxLine in _locTaxLines)
                        {
                            if (_locTaxLine.TxValue > 0 && _locTaxLine.TxAmount > 0)
                            {
                                if (_locTaxLine.TaxNature == TaxNature.Increase)
                                {
                                    _locTxAmount = _locTxAmount + _locTaxLine.TxAmount;
                                    _locTxValue = _locTxValue + _locTaxLine.TxValue;

                                }
                                else if (_locTaxLine.TaxNature == TaxNature.Decrease)
                                {
                                    _locTxAmount = _locTxAmount - _locTaxLine.TxAmount;
                                    _locTxValue = _locTxValue - _locTaxLine.TxValue;
                                }
                            }
                        }
                        _locPurchaseOrderLineXPO.TxValue = _locTxValue;
                        _locPurchaseOrderLineXPO.TxAmount = _locTxAmount;
                        _locPurchaseOrderLineXPO.Save();
                        _locPurchaseOrderLineXPO.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PurchaseOrderLine" + ex.ToString());
            }
        }

        private void GetSumTotalDisc(Session _currSession, PurchaseOrderLine _locPurchaseOrderLineXPO)
        {
            try
            {
                double _locDisc = 0;
                double _locDiscAmount = 0;

                XPCollection<DiscountLine> _locDiscountLines = new XPCollection<DiscountLine>(_currSession,
                                                               new BinaryOperator("PurchaseOrderLine", _locPurchaseOrderLineXPO));

                if (_locDiscountLines != null && _locDiscountLines.Count() > 0)
                {
                    foreach (DiscountLine _locDiscountLine in _locDiscountLines)
                    {
                        if (_locDiscountLine.Disc > 0 && _locDiscountLine.DiscAmount > 0)
                        {
                            _locDisc = _locDisc + _locDiscountLine.Disc;
                            _locDiscAmount = _locDiscAmount + _locDiscountLine.DiscAmount;
                        }
                    }

                    _locPurchaseOrderLineXPO.Disc = _locDisc;
                    _locPurchaseOrderLineXPO.DiscAmount = _locDiscAmount;
                    _locPurchaseOrderLineXPO.Save();
                    _locPurchaseOrderLineXPO.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PurchaseOrderLine" + ex.ToString());
            }
        }

        private void GetTotalPPNPPH(Session _currSession, PurchaseOrderLine _locPurchaseOrderLineXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                double _locTotalPPN = 0;
                double _locTotalPPH = 0;
                double _locTotalNPWP = 0;
                double _locTotalNonNPWP = 0;
                double _locTotalMaterai = 0;

                if (_locPurchaseOrderLineXPO != null)
                {
                    if (_locPurchaseOrderLineXPO.TaxLines != null)
                    {
                        XPQuery<TaxLine> _taxLinesQuery = new XPQuery<TaxLine>(_currSession);

                        var _taxLines = from txl in _taxLinesQuery
                                        where (txl.PurchaseOrderLine == _locPurchaseOrderLineXPO)
                                        group txl by txl.TaxGroup.TaxGroupType into g
                                        select new { TaxGroupType = g.Key };

                        if (_taxLines != null && _taxLines.Count() > 0)
                        {
                            foreach (var _taxLine in _taxLines)
                            {
                                XPCollection<TaxLine> _locTaxLines = new XPCollection<TaxLine>(_currSession,
                                                                     new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("PurchaseOrderLine", _locPurchaseOrderLineXPO),
                                                                     new BinaryOperator("TaxGroupType", _taxLine.TaxGroupType)));

                                if (_locTaxLines != null && _locTaxLines.Count > 0)
                                {
                                    foreach (TaxLine _locTaxLine in _locTaxLines)
                                    {
                                        if (_locTaxLine.TaxGroupType == TaxGroupType.PPN)
                                        {
                                            _locTotalPPN = _locTotalPPN + _locTaxLine.TxAmount;
                                        }
                                        if (_locTaxLine.TaxGroupType == TaxGroupType.PPH)
                                        {
                                            _locTotalPPH = _locTotalPPH + _locTaxLine.TxAmount;
                                        }
                                        if (_locTaxLine.TaxGroupType == TaxGroupType.NPWP)
                                        {
                                            _locTotalNPWP = _locTotalNPWP + _locTaxLine.TxAmount;
                                        }
                                        if (_locTaxLine.TaxGroupType == TaxGroupType.NonNPWP)
                                        {
                                            _locTotalNonNPWP = _locTotalNonNPWP + _locTaxLine.TxAmount;
                                        }
                                        if (_locTaxLine.TaxGroupType == TaxGroupType.Materai)
                                        {
                                            _locTotalMaterai = _locTotalMaterai + _locTaxLine.TxAmount;
                                        }
                                    }

                                    _locPurchaseOrderLineXPO.TotalPPN = _locTotalPPN;
                                    _locPurchaseOrderLineXPO.TotalPPH = _locTotalPPH;
                                    _locPurchaseOrderLineXPO.TotalNPWP = _locTotalNPWP;
                                    _locPurchaseOrderLineXPO.TotalNonNPWP = _locTotalNonNPWP;
                                    _locPurchaseOrderLineXPO.TotalMaterai = _locTotalMaterai;
                                    _locPurchaseOrderLineXPO.Save();
                                    _locPurchaseOrderLineXPO.Session.CommitTransaction();
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PurchaseOrderLine" + ex.ToString());
            }
        }

        #endregion Get

        #region Global Method

        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0}", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method


    }
}
