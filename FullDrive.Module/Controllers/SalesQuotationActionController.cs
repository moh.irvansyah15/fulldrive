﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using FullDrive.Module.BusinessObjects;
using DevExpress.ExpressApp.Xpo;
using FullDrive.Module.CustomProcess;
using System.Collections;
using DevExpress.Xpo;

using System.Web;
using System.IO;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class SalesQuotationActionController : ViewController
    {
        private ChoiceActionItem _selectionListviewFilter;
        private ChoiceActionItem _selectionApprovalListviewFilter;
        private ChoiceActionItem _setApprovalLevel;
        public SalesQuotationActionController()
        {
            InitializeComponent();
            #region FilterStatus
            SalesQuotationListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                SalesQuotationListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
            #endregion FilterStatus

            #region FilterApproval
            SalesQuotationListviewFilterApprovalSelectionAction.Items.Clear();
            foreach (object _currApprovalFilter in Enum.GetValues(typeof(CustomProcess.ApprovalFilter)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.ApprovalFilter));
                _selectionApprovalListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApprovalFilter), _currApprovalFilter);
                SalesQuotationListviewFilterApprovalSelectionAction.Items.Add(_selectionApprovalListviewFilter);
            }
            #endregion FilterApproval
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            TypeOfView = typeof(ObjectView);
            base.OnActivated();
            // Perform various tasks depending on the target View.
            #region Approval
            var _locUser = SecuritySystem.CurrentUserName;
            GlobalFunction _globalFunction = new GlobalFunction();
            Session _currentSession = null;
            if (this.ObjectSpace != null)
            {
                _currentSession = ((XPObjectSpace)this.ObjectSpace).Session;
            }

            if (_currentSession != null)
            {
                UserAccess _locUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", _locUser));

                if (_locUserAccess != null)
                {
                    SalesQuotationApprovalAction.Items.Clear();

                    XPCollection<ApplicationSetupDetail> _locAppSetupDetails = new XPCollection<ApplicationSetupDetail>
                                                                                 (_currentSession, new GroupOperator(GroupOperatorType.And,
                                                                                  new BinaryOperator("UserAccess", _locUserAccess),
                                                                                  new BinaryOperator("ObjectList", CustomProcess.ObjectList.SalesQuotation),
                                                                                  new BinaryOperator("FunctionList", CustomProcess.FunctionList.Approval),
                                                                                  new BinaryOperator("Active", true)));

                    if (_locAppSetupDetails != null && _locAppSetupDetails.Count > 0)
                    {

                        foreach (ApplicationSetupDetail _locAppSetupDetail in _locAppSetupDetails)
                        {
                            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.ApprovalLevel)))
                            {
                                if (_locAppSetupDetail.ApprovalLevel != CustomProcess.ApprovalLevel.None)
                                {
                                    EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.ApprovalLevel));
                                    _setApprovalLevel = new ChoiceActionItem(_ed.GetCaption(_locAppSetupDetail.ApprovalLevel), _locAppSetupDetail.ApprovalLevel);
                                }
                            }
                            SalesQuotationApprovalAction.Items.Add(_setApprovalLevel);
                        }
                    }
                }
            }
            #endregion Approval
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void SalesQuotationProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        SalesQuotation _locSalesQuotationOS = (SalesQuotation)_objectSpace.GetObject(obj);

                        if (_locSalesQuotationOS != null)
                        {
                            if (_locSalesQuotationOS.Code != null)
                            {
                                _currObjectId = _locSalesQuotationOS.Code;

                                SalesQuotation _locSalesQuotationXPO = _currSession.FindObject<SalesQuotation>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locSalesQuotationXPO != null)
                                {
                                    #region Status
                                    if (_locSalesQuotationXPO.Status == Status.Open || _locSalesQuotationXPO.Status == Status.Progress)
                                    {
                                        if (_locSalesQuotationXPO.Status == Status.Open)
                                        {
                                            _locSalesQuotationXPO.Status = Status.Progress;
                                            _locSalesQuotationXPO.StatusDate = now;
                                            _locSalesQuotationXPO.Save();
                                            _locSalesQuotationXPO.Session.CommitTransaction();
                                        }

                                        XPCollection<SalesQuotationLine> _locSalesQuotationLines = new XPCollection<SalesQuotationLine>
                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("SalesQuotation", _locSalesQuotationXPO),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Open),
                                                            new BinaryOperator("Status", Status.Progress))
                                                            ));

                                        if (_locSalesQuotationLines != null && _locSalesQuotationLines.Count > 0)
                                        {
                                            foreach (SalesQuotationLine _locSalesQuotationLine in _locSalesQuotationLines)
                                            {
                                                _locSalesQuotationLine.Status = Status.Progress;
                                                _locSalesQuotationLine.StatusDate = now;
                                                _locSalesQuotationLine.Save();
                                                _locSalesQuotationLine.Session.CommitTransaction();
                                            }
                                        }

                                    }
                                    #endregion Status

                                    SuccessMessageShow("SalesQuotation has successfully updated to progress");
                                }
                                else
                                {
                                    ErrorMessageShow("Data SalesQuotation Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data SalesQuotation Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesQuotation " + ex.ToString());
            }
        }

        private void SalesQuotationPostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        SalesQuotation _locSalesQuotations = (SalesQuotation)_objectSpace.GetObject(obj);

                        if (_locSalesQuotations != null)
                        {
                            if (_locSalesQuotations.Code != null)
                            {
                                _currObjectId = _locSalesQuotations.Code;

                                SalesQuotation _locSalesQuotationXPO = _currSession.FindObject<SalesQuotation>
                                                                       (new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Code", _currObjectId)));

                                if (_locSalesQuotationXPO != null)
                                {
                                    if (_locSalesQuotationXPO.Status == Status.Progress || _locSalesQuotationXPO.Status == Status.Posted)
                                    {
                                        ApprovalLine _locApprovalLineXPO = _currSession.FindObject<ApprovalLine>
                                                                           (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("EndApproval", true),
                                                                            new BinaryOperator("SalesQuotation", _locSalesQuotationXPO)));
                                        if (_locApprovalLineXPO != null)
                                        {
                                            SetSalesQuotationMonitoring(_currSession, _locSalesQuotationXPO);
                                            SetRemainQty(_currSession, _locSalesQuotationXPO);
                                            SetPostingQty(_currSession, _locSalesQuotationXPO);
                                            SetProcessCount(_currSession, _locSalesQuotationXPO);
                                            SetStatusSalesQuotationLine(_currSession, _locSalesQuotationXPO);
                                            SetNormalQuantity(_currSession, _locSalesQuotationXPO);
                                            SetFinalStatusSalesQuotation(_currSession, _locSalesQuotationXPO);
                                            SuccessMessageShow("Sales Quotation has successfully posted into Sales Quotation Monitoring");
                                        }
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data SalesQuotation Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data SalesQuotation Not Available");
                            }
                        }
                    }
                }
                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesQuotation " + ex.ToString());
            }
        }

        private void SalesQuotationListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(SalesQuotation)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesQuotation " + ex.ToString());
            }
        }

        private void SalesQuotationListviewFilterApprovalSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(SalesQuotation)))
                {
                    if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.Approval)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("ActiveApproved3", false, BinaryOperatorType.Equal),
                                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("ActiveApproved1", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", true, BinaryOperatorType.Equal)));
                    }
                    else if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.EndApproval)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new BinaryOperator("ActiveApproved3", true, BinaryOperatorType.Equal);
                    }
                    else if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.AllData)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("ActiveApproved1", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved3", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved1", false, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", false, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved3", false, BinaryOperatorType.Equal));
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesQuotation " + ex.ToString());
            }
        }

        private void SalesQuotationApprovalAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                ApplicationSetupDetail _locAppSetDetail = null;
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                var User = SecuritySystem.CurrentUserName;
                Session _currentSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currentSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                UserAccess _locUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", User));

                foreach (Object obj in _objectsToProcess)
                {
                    SalesQuotation _objInNewObjectSpace = (SalesQuotation)_objectSpace.GetObject(obj);

                    if (_objInNewObjectSpace != null)
                    {
                        if (_objInNewObjectSpace.Code != null)
                        {
                            _currObjectId = _objInNewObjectSpace.Code;
                        }
                    }

                    if (_currObjectId != null)
                    {
                        SalesQuotation _locSalesQuotationXPO = _currentSession.FindObject<SalesQuotation>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("Code", _currObjectId)));

                        if (_locSalesQuotationXPO != null)
                        {
                            if (_locSalesQuotationXPO.Status == Status.Progress)
                            {
                                ApprovalLine _locApprovalLine = _currentSession.FindObject<ApprovalLine>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("SalesQuotation", _locSalesQuotationXPO),
                                                                             new BinaryOperator("EndApproval", true)));
                                if (_locApprovalLine == null)
                                {
                                    #region Approval Level 1
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level1)
                                    {
                                        _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level1, ObjectList.SalesQuotation);
                                        if (_locAppSetDetail != null)
                                        {
                                            //Buat bs input langsung ke approvalline
                                            ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("SalesQuotation", _locSalesQuotationXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level1)));
                                            if (_locApprovalLineXPO == null)
                                            {

                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level1,
                                                        EndApproval = true,
                                                        SalesQuotation = _locSalesQuotationXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locSalesQuotationXPO.ActivationPosting = true;
                                                    _locSalesQuotationXPO.ActiveApproved1 = false;
                                                    _locSalesQuotationXPO.ActiveApproved2 = false;
                                                    _locSalesQuotationXPO.ActiveApproved3 = true;
                                                    _locSalesQuotationXPO.Save();
                                                    _locSalesQuotationXPO.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level1,
                                                        SalesQuotation = _locSalesQuotationXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locSalesQuotationXPO.ActiveApproved1 = true;
                                                    _locSalesQuotationXPO.ActiveApproved2 = false;
                                                    _locSalesQuotationXPO.ActiveApproved3 = false;
                                                    _locSalesQuotationXPO.Save();
                                                    _locSalesQuotationXPO.Session.CommitTransaction();
                                                }

                                                //Send Email
                                                ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("SalesQuotation", _locSalesQuotationXPO)));
                                                if (_locApprovalLineXPO2 != null)
                                                {
                                                    SendEmail(_currentSession, _locSalesQuotationXPO, Status.Progress, _locApprovalLineXPO2);
                                                }

                                                SuccessMessageShow("SalesQuotation has successfully Approve");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 1

                                    #region Approval Level 2
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level2)
                                    {
                                        _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level2, ObjectList.SalesQuotation);

                                        if (_locAppSetDetail != null)
                                        {
                                            ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("SalesQuotation", _locSalesQuotationXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level2)));

                                            if (_locApprovalLineXPO == null)
                                            {
                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level2,
                                                        EndApproval = true,
                                                        SalesQuotation = _locSalesQuotationXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locSalesQuotationXPO.ActivationPosting = true;
                                                    _locSalesQuotationXPO.ActiveApproved1 = false;
                                                    _locSalesQuotationXPO.ActiveApproved2 = false;
                                                    _locSalesQuotationXPO.ActiveApproved3 = true;
                                                    _locSalesQuotationXPO.Save();
                                                    _locSalesQuotationXPO.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level2,
                                                        SalesQuotation = _locSalesQuotationXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    SetApprovalLine(_currentSession, _locSalesQuotationXPO, ApprovalLevel.Level1);

                                                    _locSalesQuotationXPO.ActiveApproved1 = false;
                                                    _locSalesQuotationXPO.ActiveApproved2 = true;
                                                    _locSalesQuotationXPO.ActiveApproved3 = false;
                                                    _locSalesQuotationXPO.Save();
                                                    _locSalesQuotationXPO.Session.CommitTransaction();
                                                }

                                                //Send Email
                                                ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("SalesQuotation", _locSalesQuotationXPO)));
                                                if (_locApprovalLineXPO2 != null)
                                                {
                                                    SendEmail(_currentSession, _locSalesQuotationXPO, Status.Progress, _locApprovalLineXPO2);
                                                }

                                                SuccessMessageShow("SalesQuotation has successfully Approve");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 2

                                    #region Approval Level 3
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level3)
                                    {
                                        _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level3, ObjectList.SalesQuotation);

                                        if (_locAppSetDetail != null)
                                        {
                                            ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("SalesQuotation", _locSalesQuotationXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level3)));

                                            if (_locApprovalLineXPO == null)
                                            {
                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level3,
                                                        EndApproval = true,
                                                        SalesQuotation = _locSalesQuotationXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locSalesQuotationXPO.ActivationPosting = true;
                                                    _locSalesQuotationXPO.ActiveApproved1 = false;
                                                    _locSalesQuotationXPO.ActiveApproved2 = false;
                                                    _locSalesQuotationXPO.ActiveApproved3 = true;
                                                    _locSalesQuotationXPO.Save();
                                                    _locSalesQuotationXPO.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level3,
                                                        SalesQuotation = _locSalesQuotationXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    SetApprovalLine(_currentSession, _locSalesQuotationXPO, ApprovalLevel.Level2);
                                                    SetApprovalLine(_currentSession, _locSalesQuotationXPO, ApprovalLevel.Level1);

                                                    _locSalesQuotationXPO.ActiveApproved1 = false;
                                                    _locSalesQuotationXPO.ActiveApproved2 = false;
                                                    _locSalesQuotationXPO.ActiveApproved3 = true;
                                                    _locSalesQuotationXPO.Save();
                                                    _locSalesQuotationXPO.Session.CommitTransaction();
                                                }

                                                //Send Email
                                                ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("SalesQuotation", _locSalesQuotationXPO)));
                                                if (_locApprovalLineXPO2 != null)
                                                {
                                                    SendEmail(_currentSession, _locSalesQuotationXPO, Status.Progress, _locApprovalLineXPO2);
                                                }

                                                SuccessMessageShow("SalesQuotation has successfully Approve");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 3
                                }

                            }
                            else
                            {
                                ErrorMessageShow("SalesQuotation Status Not Available");
                            }
                        }
                        else
                        {
                            ErrorMessageShow("SalesQuotation Not Available");
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesQuotation " + ex.ToString());
            }
        }

        //===================================== Code Only ====================================

        #region Posting

        private void SetSalesQuotationMonitoring(Session _currSession, SalesQuotation _locSalesQuotationXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locSalesQuotationXPO != null)
                {
                    XPCollection<SalesQuotationLine> _locSalesQuotationLines = new XPCollection<SalesQuotationLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("SalesQuotation", _locSalesQuotationXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locSalesQuotationLines != null && _locSalesQuotationLines.Count() > 0)
                    {

                        foreach (SalesQuotationLine _locSalesQuotationLine in _locSalesQuotationLines)
                        {
                            if (_locSalesQuotationLine.Status == Status.Progress || _locSalesQuotationLine.Status == Status.Posted)
                            {
                                SalesQuotationMonitoring _saveDataSalesQuotationMonitoring = new SalesQuotationMonitoring(_currSession)
                                {
                                    SalesQuotation = _locSalesQuotationXPO,
                                    SalesQuotationLine = _locSalesQuotationLine,
                                    SalesType = _locSalesQuotationLine.SalesType,
                                    Item = _locSalesQuotationLine.Item,
                                    DQty = _locSalesQuotationLine.DQty,
                                    DUOM = _locSalesQuotationLine.DUOM,
                                    Qty = _locSalesQuotationLine.Qty,
                                    TQty = _locSalesQuotationLine.TQty,
                                    UOM = _locSalesQuotationLine.UOM,
                                    Currency = _locSalesQuotationLine.Currency,
                                    PriceGroup = _locSalesQuotationLine.PriceGroup,
                                    Price = _locSalesQuotationLine.Price,
                                    PriceLine = _locSalesQuotationLine.PriceLine,
                                    UAmount = _locSalesQuotationLine.UAmount,
                                    TUAmount = _locSalesQuotationLine.TUAmount,
                                    MultiTax = _locSalesQuotationLine.MultiTax,
                                    Tax = _locSalesQuotationLine.Tax,
                                    TxValue = _locSalesQuotationLine.TxValue,
                                    TxAmount = _locSalesQuotationLine.TxAmount,
                                    MultiDiscount = _locSalesQuotationLine.MultiDiscount,
                                    Discount = _locSalesQuotationLine.Discount,
                                    Disc = _locSalesQuotationLine.Disc,
                                    DiscAmount = _locSalesQuotationLine.DiscAmount,
                                    TAmount = _locSalesQuotationLine.TAmount,
                                };
                                _saveDataSalesQuotationMonitoring.Save();
                                _saveDataSalesQuotationMonitoring.Session.CommitTransaction();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesQuotation " + ex.ToString());
            }
        }

        private void SetRemainQty(Session _currSession, SalesQuotation _locSalesQuotationXPO)
        {
            try
            {
                if (_locSalesQuotationXPO != null)
                {
                    XPCollection<SalesQuotationLine> _locSalesQuotationLines = new XPCollection<SalesQuotationLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("SalesQuotation", _locSalesQuotationXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locSalesQuotationLines != null && _locSalesQuotationLines.Count > 0)
                    {
                        double _locRmDQty = 0;
                        double _locRmQty = 0;
                        double _locInvLineTotal = 0;
                        ItemUnitOfMeasure _locItemUOM = null;

                        foreach (SalesQuotationLine _locSalesQuotationLine in _locSalesQuotationLines)
                        {
                            #region ProcessCount=0
                            if (_locSalesQuotationLine.PostedCount == 0)
                            {
                                #region MaxQuantity
                                if (_locSalesQuotationLine.MxDQty > 0)
                                {
                                    if (_locSalesQuotationLine.DQty > 0 && _locSalesQuotationLine.DQty <= _locSalesQuotationLine.MxDQty)
                                    {
                                        _locRmDQty = _locSalesQuotationLine.MxDQty - _locSalesQuotationLine.DQty;
                                    }

                                    if (_locSalesQuotationLine.Qty > 0 && _locSalesQuotationLine.Qty <= _locSalesQuotationLine.MxQty)
                                    {
                                        _locRmQty = _locSalesQuotationLine.MxQty - _locSalesQuotationLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locSalesQuotationLine.Item),
                                                                new BinaryOperator("UOM", _locSalesQuotationLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locSalesQuotationLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty + _locRmDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }

                                }
                                #endregion MaxQuantity
                                #region NonMaxQuantity
                                else
                                {
                                    if (_locSalesQuotationLine.DQty > 0)
                                    {
                                        _locRmDQty = 0;
                                    }

                                    if (_locSalesQuotationLine.Qty > 0)
                                    {
                                        _locRmQty = 0;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locSalesQuotationLine.Item),
                                                                new BinaryOperator("UOM", _locSalesQuotationLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locSalesQuotationLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty + _locRmDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion ProcessCount=0

                            #region ProcessCount>0
                            if (_locSalesQuotationLine.PostedCount > 0)
                            {
                                if (_locSalesQuotationLine.RmDQty > 0)
                                {
                                    _locRmDQty = _locSalesQuotationLine.RmDQty - _locSalesQuotationLine.DQty;
                                }

                                if (_locSalesQuotationLine.RmQty > 0)
                                {
                                    _locRmQty = _locSalesQuotationLine.RmQty - _locSalesQuotationLine.Qty;
                                }

                                if (_locSalesQuotationLine.MxDQty > 0 || _locSalesQuotationLine.MxQty > 0)
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locSalesQuotationLine.Item),
                                                                new BinaryOperator("UOM", _locSalesQuotationLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locSalesQuotationLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                }
                                else
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locSalesQuotationLine.Item),
                                                                new BinaryOperator("UOM", _locSalesQuotationLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locSalesQuotationLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                }


                                if (_locItemUOM != null)
                                {
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locRmQty + _locRmDQty;
                                }

                            }
                            #endregion ProcessCount>0

                            _locSalesQuotationLine.RmDQty = _locRmDQty;
                            _locSalesQuotationLine.RmQty = _locRmQty;
                            _locSalesQuotationLine.RmTQty = _locInvLineTotal;
                            _locSalesQuotationLine.Save();
                            _locSalesQuotationLine.Session.CommitTransaction();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrder " + ex.ToString());
            }
        }

        private void SetPostingQty(Session _currSession, SalesQuotation _locSalesQuotationXPO)
        {
            try
            {
                if (_locSalesQuotationXPO != null)
                {
                    XPCollection<SalesQuotationLine> _locSalesQuotationLines = new XPCollection<SalesQuotationLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("SalesQuotation", _locSalesQuotationXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locSalesQuotationLines != null && _locSalesQuotationLines.Count > 0)
                    {
                        double _locPDQty = 0;
                        double _locPQty = 0;
                        double _locInvLineTotal = 0;
                        ItemUnitOfMeasure _locItemUOM = null;

                        foreach (SalesQuotationLine _locSalesQuotationLine in _locSalesQuotationLines)
                        {
                            #region ProcessCount=0
                            if (_locSalesQuotationLine.PostedCount == 0)
                            {
                                #region MaxQuantity
                                if (_locSalesQuotationLine.MxDQty > 0)
                                {
                                    if (_locSalesQuotationLine.DQty > 0 && _locSalesQuotationLine.DQty <= _locSalesQuotationLine.MxDQty)
                                    {
                                        _locPDQty = _locSalesQuotationLine.DQty;
                                    }

                                    if (_locSalesQuotationLine.Qty > 0 && _locSalesQuotationLine.Qty <= _locSalesQuotationLine.MxQty)
                                    {
                                        _locPQty = _locSalesQuotationLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locSalesQuotationLine.Item),
                                                                new BinaryOperator("UOM", _locSalesQuotationLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locSalesQuotationLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty + _locPDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }

                                }
                                #endregion MaxQuantity
                                #region NonMaxQuantity
                                else
                                {
                                    if (_locSalesQuotationLine.DQty > 0)
                                    {
                                        _locPDQty = _locSalesQuotationLine.DQty;
                                    }

                                    if (_locSalesQuotationLine.Qty > 0)
                                    {
                                        _locPQty = _locSalesQuotationLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locSalesQuotationLine.Item),
                                                                new BinaryOperator("UOM", _locSalesQuotationLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locSalesQuotationLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty + _locPDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion ProcessCount=0

                            #region ProcessCount>0
                            if (_locSalesQuotationLine.PostedCount > 0)
                            {
                                if (_locSalesQuotationLine.PDQty > 0)
                                {
                                    _locPDQty = _locSalesQuotationLine.PDQty + _locSalesQuotationLine.DQty;
                                }

                                if (_locSalesQuotationLine.PQty > 0)
                                {
                                    _locPQty = _locSalesQuotationLine.PQty + _locSalesQuotationLine.Qty;
                                }

                                if (_locSalesQuotationLine.MxDQty > 0 || _locSalesQuotationLine.MxQty > 0)
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                           (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Item", _locSalesQuotationLine.Item),
                                                            new BinaryOperator("UOM", _locSalesQuotationLine.MxUOM),
                                                            new BinaryOperator("DefaultUOM", _locSalesQuotationLine.MxDUOM),
                                                            new BinaryOperator("Active", true)));
                                }
                                else
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                           (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Item", _locSalesQuotationLine.Item),
                                                            new BinaryOperator("UOM", _locSalesQuotationLine.UOM),
                                                            new BinaryOperator("DefaultUOM", _locSalesQuotationLine.DUOM),
                                                            new BinaryOperator("Active", true)));
                                }

                                if (_locItemUOM != null)
                                {
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locPQty + _locPDQty;
                                }

                            }
                            #endregion ProcessCount>0

                            _locSalesQuotationLine.PDQty = _locPDQty;
                            _locSalesQuotationLine.PDUOM = _locSalesQuotationLine.DUOM;
                            _locSalesQuotationLine.PQty = _locPQty;
                            _locSalesQuotationLine.PUOM = _locSalesQuotationLine.UOM;
                            _locSalesQuotationLine.PTQty = _locInvLineTotal;
                            _locSalesQuotationLine.Save();
                            _locSalesQuotationLine.Session.CommitTransaction();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesQuotation " + ex.ToString());
            }

        }

        private void SetProcessCount(Session _currSession, SalesQuotation _locSalesQuotationXPO)
        {
            try
            {
                if (_locSalesQuotationXPO != null)
                {
                    XPCollection<SalesQuotationLine> _locSalesQuotationLines = new XPCollection<SalesQuotationLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("SalesQuotation", _locSalesQuotationXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locSalesQuotationLines != null && _locSalesQuotationLines.Count > 0)
                    {

                        foreach (SalesQuotationLine _locSalesQuotationLine in _locSalesQuotationLines)
                        {
                            if (_locSalesQuotationLine.Status == Status.Progress || _locSalesQuotationLine.Status == Status.Posted)
                            {
                                _locSalesQuotationLine.PostedCount = _locSalesQuotationLine.PostedCount + 1;
                                _locSalesQuotationLine.Save();
                                _locSalesQuotationLine.Session.CommitTransaction();
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesQuotation " + ex.ToString());
            }
        }

        private void SetStatusSalesQuotationLine(Session _currSession, SalesQuotation _locSalesQuotationXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locSalesQuotationXPO != null)
                {
                    XPCollection<SalesQuotationLine> _locSalesQuotationLines = new XPCollection<SalesQuotationLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("SalesQuotation", _locSalesQuotationXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locSalesQuotationLines != null && _locSalesQuotationLines.Count > 0)
                    {

                        foreach (SalesQuotationLine _locSalesQuotationLine in _locSalesQuotationLines)
                        {
                            if (_locSalesQuotationLine.Status == Status.Progress || _locSalesQuotationLine.Status == Status.Posted)
                            {
                                if (_locSalesQuotationLine.RmDQty == 0 && _locSalesQuotationLine.RmQty == 0 && _locSalesQuotationLine.RmTQty == 0)
                                {
                                    _locSalesQuotationLine.Status = Status.Close;
                                    _locSalesQuotationLine.ActivationPosting = true;
                                    _locSalesQuotationLine.ActivationQuantity = true;
                                    _locSalesQuotationLine.StatusDate = now;
                                }
                                else
                                {
                                    _locSalesQuotationLine.Status = Status.Posted;
                                    _locSalesQuotationLine.StatusDate = now;
                                }
                                _locSalesQuotationLine.Save();
                                _locSalesQuotationLine.Session.CommitTransaction();
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesQuotation " + ex.ToString());
            }
        }

        private void SetNormalQuantity(Session _currSession, SalesQuotation _locSalesQuotationXPO)
        {
            try
            {
                if (_locSalesQuotationXPO != null)
                {
                    XPCollection<SalesQuotationLine> _locSalesQuotationLines = new XPCollection<SalesQuotationLine>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("SalesQuotation", _locSalesQuotationXPO),
                                                                                     new BinaryOperator("Select", true)));

                    if (_locSalesQuotationLines != null && _locSalesQuotationLines.Count > 0)
                    {
                        foreach (SalesQuotationLine _locSalesQuotationLine in _locSalesQuotationLines)
                        {
                            if (_locSalesQuotationLine.Status == Status.Progress || _locSalesQuotationLine.Status == Status.Posted || _locSalesQuotationLine.Status == Status.Close)
                            {
                                if (_locSalesQuotationLine.DQty > 0 || _locSalesQuotationLine.Qty > 0)
                                {
                                    _locSalesQuotationLine.Select = false;
                                    _locSalesQuotationLine.DQty = 0;
                                    _locSalesQuotationLine.Qty = 0;
                                    _locSalesQuotationLine.Save();
                                    _locSalesQuotationLine.Session.CommitTransaction();
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesQuotation " + ex.ToString());
            }
        }

        private void SetFinalStatusSalesQuotation(Session _currSession, SalesQuotation _locSalesQuotationXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locStatusCount = 0;
                int _locSalesQuotationLineCount = 0;

                if (_locSalesQuotationXPO != null)
                {

                    XPCollection<SalesQuotationLine> _locSalesQuotationLines = new XPCollection<SalesQuotationLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("SalesQuotation", _locSalesQuotationXPO)));

                    if (_locSalesQuotationLines != null && _locSalesQuotationLines.Count() > 0)
                    {
                        _locSalesQuotationLineCount = _locSalesQuotationLines.Count();

                        foreach (SalesQuotationLine _locSalesQuotationLine in _locSalesQuotationLines)
                        {
                            if (_locSalesQuotationLine.Status == Status.Close)
                            {
                                _locStatusCount = _locStatusCount + 1;
                            }
                        }
                    }

                    if (_locStatusCount == _locSalesQuotationLineCount)
                    {
                        _locSalesQuotationXPO.ActivationPosting = true;
                        _locSalesQuotationXPO.Status = Status.Close;
                        _locSalesQuotationXPO.StatusDate = now;
                        _locSalesQuotationXPO.Save();
                        _locSalesQuotationXPO.Session.CommitTransaction();
                    }
                    else
                    {
                        _locSalesQuotationXPO.Status = Status.Posted;
                        _locSalesQuotationXPO.StatusDate = now;
                        _locSalesQuotationXPO.Save();
                        _locSalesQuotationXPO.Session.CommitTransaction();
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesQuotation " + ex.ToString());
            }
        }

        #endregion Posting

        #region Approval Process

        private void SetApprovalLine(Session _currentSession, SalesQuotation _locSalesQuotationXPO, ApprovalLevel _locApprovalLevel)
        {
            try
            {
                DateTime now = DateTime.Now;

                ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("SalesQuotation", _locSalesQuotationXPO),
                                                                         new BinaryOperator("ApprovalLevel", _locApprovalLevel)));

                if (_locApprovalLineXPO == null)
                {
                    ApprovalLine _saveDataAL2 = new ApprovalLine(_currentSession)
                    {
                        ApprovalDate = now,
                        ApprovalStatus = Status.Approved,
                        ApprovalLevel = _locApprovalLevel,
                        SalesQuotation = _locSalesQuotationXPO,
                    };
                    _saveDataAL2.Save();
                    _saveDataAL2.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesQuotation " + ex.ToString());
            }
        }

        #endregion Approval Process

        #region Email

        private string BackgroundBody(Session _currentSession, SalesQuotation _locSalesQuotationXPO, MailSetupDetail _locMailSetDetail)
        {
            string body = string.Empty;

            SalesQuotation _locSalesQuotations = _currentSession.FindObject<SalesQuotation>
                                           (new GroupOperator(GroupOperatorType.And,
                                            new BinaryOperator("Code", _locSalesQuotationXPO.Code)));

            ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                               (new GroupOperator(GroupOperatorType.And,
                                                new BinaryOperator("SalesQuotation", _locSalesQuotations)));

            string Status = _locApprovalLineXPO.ApprovalStatus.ToString();

            using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Email/EmailTemplate.html")))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{Code}", _locSalesQuotationXPO.Code);

            #region Level
            if (_locSalesQuotations.ActiveApproved1 == true)
            {
                string Active = "Level 1";
                body = body.Replace("{Level}", Active);
            }
            else if (_locSalesQuotations.ActiveApproved2 == true)
            {
                string Active = "Level 2";
                body = body.Replace("{Level}", Active);
            }
            else if (_locSalesQuotations.ActiveApproved3 == true)
            {
                string Active = "Level 3";
                body = body.Replace("{Level}", Active);
            }
            else
            {
                string Active = "Not Available";
                body = body.Replace("{Level}", Active);
            }
            #endregion Level

            body = body.Replace("{Status}", Status);
            body = body.Replace("{Message}", _locMailSetDetail.MessageBody);
            return body;
        }

        protected void SendEmail(Session _currentSession, SalesQuotation _locSalesQuotationXPO, Status _locStatus, ApprovalLine _locApprovalLineXPO2)
        {
            DateTime now = DateTime.Now;
            var User = SecuritySystem.CurrentUserName;
            GlobalFunction _globFunc = new GlobalFunction();
            UserAccess _numLineUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", User));
            string _locMessageBody = null;

            try
            {
                if (_locSalesQuotationXPO != null)
                {
                    XPCollection<MailSetupDetail> _locMailSetDetails = new XPCollection<MailSetupDetail>
                                                                       (_currentSession, new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("ObjectList", ObjectList.SalesQuotation),
                                                                        new BinaryOperator("UserAccess", _numLineUserAccess),
                                                                        new BinaryOperator("Active", true)));

                    if (_locMailSetDetails != null && _locMailSetDetails.Count > 0)
                    {
                        foreach (MailSetupDetail _locMailSetDetail in _locMailSetDetails)
                        {
                            if (_locMailSetDetail.MessageBody != null)
                            {
                                if (_locApprovalLineXPO2.EndApproval == true)
                                {
                                    _locMessageBody = this.BackgroundBody(_currentSession, _locSalesQuotationXPO, _locMailSetDetail);
                                }
                                else
                                {
                                    _locMessageBody = this.BackgroundBody(_currentSession, _locSalesQuotationXPO, _locMailSetDetail);
                                }
                            }

                            _globFunc.SetAndSendMail(_locMailSetDetail.SmtpHost, _locMailSetDetail.SmtpPort,
                            _locMailSetDetail.MailFrom, _locMailSetDetail.MailFromPassword, _locMailSetDetail.MailTo, _locMailSetDetail.MailSubject, _locMessageBody);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesQuotation " + ex.ToString());
            }
        }

        #endregion Email

        #region Global

        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0} !", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }





        #endregion Global

        
    }
}
