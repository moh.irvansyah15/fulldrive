﻿namespace FullDrive.Module.Controllers
{
    partial class SalesReturnLineActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.SalesReturnLineTaxCalculationAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.SalesReturnLineDiscCalculationAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // SalesReturnLineTaxCalculationAction
            // 
            this.SalesReturnLineTaxCalculationAction.Caption = "Tax Calculation";
            this.SalesReturnLineTaxCalculationAction.ConfirmationMessage = null;
            this.SalesReturnLineTaxCalculationAction.Id = "SalesReturnLineTaxCalculationActionId";
            this.SalesReturnLineTaxCalculationAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesReturnLine);
            this.SalesReturnLineTaxCalculationAction.ToolTip = null;
            this.SalesReturnLineTaxCalculationAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SalesReturnLineTaxCalculationAction_Execute);
            // 
            // SalesReturnLineDiscCalculationAction
            // 
            this.SalesReturnLineDiscCalculationAction.Caption = "Disc Calculation";
            this.SalesReturnLineDiscCalculationAction.ConfirmationMessage = null;
            this.SalesReturnLineDiscCalculationAction.Id = "SalesReturnLineDiscCalculationActionId";
            this.SalesReturnLineDiscCalculationAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesReturnLine);
            this.SalesReturnLineDiscCalculationAction.ToolTip = null;
            this.SalesReturnLineDiscCalculationAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SalesReturnLineDiscCalculationAction_Execute);
            // 
            // SalesReturnLineActionController
            // 
            this.Actions.Add(this.SalesReturnLineTaxCalculationAction);
            this.Actions.Add(this.SalesReturnLineDiscCalculationAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction SalesReturnLineTaxCalculationAction;
        private DevExpress.ExpressApp.Actions.SimpleAction SalesReturnLineDiscCalculationAction;
    }
}
