﻿namespace FullDrive.Module.Controllers
{
    partial class ItemConsumptionApprovalActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ItemConsumptionApprovalAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // ItemConsumptionApprovalAction
            // 
            this.ItemConsumptionApprovalAction.Caption = "Approval";
            this.ItemConsumptionApprovalAction.ConfirmationMessage = null;
            this.ItemConsumptionApprovalAction.Id = "ItemConsumptionApprovalActionId";
            this.ItemConsumptionApprovalAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.ItemConsumptionApprovalAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.ItemConsumption);
            this.ItemConsumptionApprovalAction.ToolTip = null;
            this.ItemConsumptionApprovalAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.ItemConsumptionApprovalAction_Execute);
            // 
            // ItemConsumptionApprovalActionController
            // 
            this.Actions.Add(this.ItemConsumptionApprovalAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SingleChoiceAction ItemConsumptionApprovalAction;
    }
}
