﻿namespace FullDrive.Module.Controllers
{
    partial class TransferOrderMonitoringActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.TransferOrderMonitoringSelectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.TransferOrderMonitoringUnselectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // TransferOrderMonitoringSelectAction
            // 
            this.TransferOrderMonitoringSelectAction.Caption = "Select";
            this.TransferOrderMonitoringSelectAction.ConfirmationMessage = null;
            this.TransferOrderMonitoringSelectAction.Id = "TransferOrderMonitoringSelectActionId";
            this.TransferOrderMonitoringSelectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.TransferOrderMonitoring);
            this.TransferOrderMonitoringSelectAction.ToolTip = null;
            this.TransferOrderMonitoringSelectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.TransferOrderMonitoringSelectAction_Execute);
            // 
            // TransferOrderMonitoringUnselectAction
            // 
            this.TransferOrderMonitoringUnselectAction.Caption = "Unselect";
            this.TransferOrderMonitoringUnselectAction.ConfirmationMessage = null;
            this.TransferOrderMonitoringUnselectAction.Id = "TransferOrderMonitoringUnselectActionId";
            this.TransferOrderMonitoringUnselectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.TransferOrderMonitoring);
            this.TransferOrderMonitoringUnselectAction.ToolTip = null;
            this.TransferOrderMonitoringUnselectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.TransferOrderMonitoringUnselectAction_Execute);
            // 
            // TransferOrderMonitoringActionController
            // 
            this.Actions.Add(this.TransferOrderMonitoringSelectAction);
            this.Actions.Add(this.TransferOrderMonitoringUnselectAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction TransferOrderMonitoringSelectAction;
        private DevExpress.ExpressApp.Actions.SimpleAction TransferOrderMonitoringUnselectAction;
    }
}
