﻿#region Default

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

#endregion Default

using FullDrive.Module.BusinessObjects;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Xpo;
using System.Web.UI.WebControls;
using DevExpress.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class TransferOutListViewController : ViewController
    {
        #region Default

        public TransferOutListViewController()
        {
            InitializeComponent();
            RegisterActions(components);
            TargetViewId = "TransferOut_ListView";
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
            var User = SecuritySystem.CurrentUserName;
            string _beginString1 = null;
            string _endString1 = null;
            string _fullString1 = null;
            string _beginString2 = null;
            string _endString2 = null;
            string _fullString2 = null;
            string _fullString = null;
            string _locLocationCode = null;
            var ListView = View as ListView;
            Session currentSession = ((XPObjectSpace)this.ObjectSpace).Session;
            UserAccess _locUserAccess = currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", User));
            if (_locUserAccess != null)
            {
                List<string> _stringLocation = new List<string>();

                XPCollection<WarehouseSetupDetail> _locWhsSetupDetails = new XPCollection<WarehouseSetupDetail>
                                                                         (currentSession, new GroupOperator(GroupOperatorType.And,
                                                                          new BinaryOperator("UserAccess", _locUserAccess),
                                                                          new BinaryOperator("Owner", true),
                                                                          new BinaryOperator("Active", true)));

                if (_locWhsSetupDetails != null && _locWhsSetupDetails.Count() > 0)
                {
                    foreach (WarehouseSetupDetail _locWhsSetupDetail in _locWhsSetupDetails)
                    {
                        if(_locWhsSetupDetail.Location != null)
                        {
                            if(_locWhsSetupDetail.Location.Code != null)
                            {
                                _locLocationCode = _locWhsSetupDetail.Location.Code;
                                _stringLocation.Add(_locLocationCode);
                            }
                        }
                        
                    }

                }

                #region ForLocationFrom
                IEnumerable<string> _stringArrayLocationFromDistinct = _stringLocation.Distinct();
                string[] _stringArrayLocationFromList = _stringArrayLocationFromDistinct.ToArray();
                if (_stringArrayLocationFromList.Length == 1)
                {
                    for (int i = 0; i < _stringArrayLocationFromList.Length; i++)
                    {
                        Location _locLocationFrom = currentSession.FindObject<Location>(new BinaryOperator("Code", _stringArrayLocationFromList[i]));
                        if (_locLocationFrom != null)
                        {
                            if (i == 0)
                            {
                                _beginString1 = "[LocationFrom.Code]=='" + _locLocationFrom.Code + "'";
                            }
                        }
                    }
                }
                else if (_stringArrayLocationFromList.Length > 1)
                {
                    for (int i = 0; i < _stringArrayLocationFromList.Length; i++)
                    {
                        Location _locLocationFrom = currentSession.FindObject<Location>(new BinaryOperator("Code", _stringArrayLocationFromList[i]));
                        if (_locLocationFrom != null)
                        {
                            if (i == 0)
                            {
                                _beginString1 = "[LocationFrom.Code]=='" + _locLocationFrom.Code + "'";
                            }
                            else
                            {
                                _endString1 = " OR [LocationFrom.Code]=='" + _locLocationFrom.Code + "'";
                            }
                        }
                    }
                }
                _fullString1 = _beginString1 + _endString1;
                #endregion ForLocationFrom

                #region ForLocationTo
                IEnumerable<string> _stringArrayLocationToDistinct = _stringLocation.Distinct();
                string[] _stringArrayLocationToList = _stringArrayLocationToDistinct.ToArray();
                if (_stringArrayLocationToList.Length == 1)
                {
                    for (int i = 0; i < _stringArrayLocationToList.Length; i++)
                    {
                        Location _locLocationTo = currentSession.FindObject<Location>(new BinaryOperator("Code", _stringArrayLocationToList[i]));
                        if (_locLocationTo != null)
                        {
                            if (i == 0)
                            {
                                _beginString2 = "[LocationTo.Code]=='" + _locLocationTo.Code + "'";
                            }
                        }
                    }
                }
                else if (_stringArrayLocationToList.Length > 1)
                {
                    for (int i = 0; i < _stringArrayLocationToList.Length; i++)
                    {
                        Location _locLocationTo = currentSession.FindObject<Location>(new BinaryOperator("Code", _stringArrayLocationToList[i]));
                        if (_locLocationTo != null)
                        {
                            if (i == 0)
                            {
                                _beginString2 = "[LocationTo.Code]=='" + _locLocationTo.Code + "'";
                            }
                            else
                            {
                                _endString2 = " OR [LocationTo.Code]=='" + _locLocationTo.Code + "'";
                            }
                        }
                    }
                }
                _fullString2 = _beginString2 + _endString2;
                #endregion ForLocationTo

                if (_stringArrayLocationFromList.Length > 0 && _stringArrayLocationToList.Length > 0)
                {
                    _fullString = _fullString1 + " OR " + _fullString2;
                }
                else if (_stringArrayLocationFromList.Length > 0 && _stringArrayLocationToList.Length == 0)
                {
                    _fullString = _fullString1;
                }
                else if (_stringArrayLocationFromList.Length == 0 && _stringArrayLocationToList.Length > 0)
                {
                    _fullString = _fullString2;
                }

                ListView.CollectionSource.Criteria["TransferOutListViewFilter"] = CriteriaOperator.Parse(_fullString);

            }
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        #endregion Default

    }
}
