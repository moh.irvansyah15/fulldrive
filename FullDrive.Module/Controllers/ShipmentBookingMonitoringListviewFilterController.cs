﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using FullDrive.Module.BusinessObjects;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class ShipmentBookingMonitoringListviewFilterController : ViewController
    {
        public ShipmentBookingMonitoringListviewFilterController()
        {
            InitializeComponent();
            RegisterActions(components);
            TargetViewId = "ShipmentBookingMonitoring_ListView";
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
            var User = SecuritySystem.CurrentUserName;
            string _beginString1 = null;
            string _endString1 = null;
            string _fullString1 = null;
            string _beginString2 = null;
            string _endString2 = null;
            string _fullString2 = null;
            string _fullString = null;
            var ListView = View as ListView;
            Session currentSession = ((XPObjectSpace)this.ObjectSpace).Session;
            UserAccess _locUserAccess = currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", User));
            if (_locUserAccess != null)
            {
                if (_locUserAccess.Employee != null)
                {
                    List<string> _stringCountry = new List<string>();

                    XPCollection<OrganizationSetupDetail> _locOrganizationSetupDetails = new XPCollection<OrganizationSetupDetail>
                                                                              (currentSession, new GroupOperator(GroupOperatorType.And,
                                                                               new BinaryOperator("UserAccess", _locUserAccess),
                                                                               new BinaryOperator("ObjectList", CustomProcess.ObjectList.ShipmentBookingMonitoring),
                                                                               new BinaryOperator("PageType", CustomProcess.PageType.ListView),
                                                                               new BinaryOperator("Active", true)));

                    if (_locOrganizationSetupDetails != null && _locOrganizationSetupDetails.Count > 0)
                    {
                        foreach (OrganizationSetupDetail _locOrganizationSetupDetail in _locOrganizationSetupDetails)
                        {
                            if (_locOrganizationSetupDetail.Country != null)
                            {
                                _stringCountry.Add(_locOrganizationSetupDetail.Country.Code);
                            }
                        }

                        foreach (OrganizationSetupDetail _locOrganizationSetupDetail2 in _locOrganizationSetupDetails)
                        {
                            if (_locOrganizationSetupDetail2.ShipmentType == CustomProcess.ShipmentType.Export || _locOrganizationSetupDetail2.ShipmentType == CustomProcess.ShipmentType.ExportAndImport)
                            {
                                #region CountryFrom
                                IEnumerable<string> _stringArrayCountryFromDistinct = _stringCountry.Distinct();
                                string[] _stringArrayCountryFromList = _stringArrayCountryFromDistinct.ToArray();
                                if (_stringArrayCountryFromList.Length == 1)
                                {
                                    for (int i = 0; i < _stringArrayCountryFromList.Length; i++)
                                    {
                                        Country _locCountryFrom = currentSession.FindObject<Country>(new BinaryOperator("Code", _stringArrayCountryFromList[i]));
                                        if (_locCountryFrom != null)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString1 = "[CountryFrom.Code]=='" + _locCountryFrom.Code + "'";
                                            }
                                        }
                                    }
                                }
                                else if (_stringArrayCountryFromList.Length > 1)
                                {
                                    for (int i = 0; i < _stringArrayCountryFromList.Length; i++)
                                    {
                                        Country _locCountryFrom = currentSession.FindObject<Country>(new BinaryOperator("Code", _stringArrayCountryFromList[i]));
                                        if (_locCountryFrom != null)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString1 = "[CountryFrom.Code]=='" + _locCountryFrom.Code + "'";
                                            }
                                            else
                                            {
                                                _endString1 = _endString1 + " OR [CountryFrom.Code]=='" + _locCountryFrom.Code + "'";
                                            }
                                        }
                                    }
                                }
                                _fullString1 = _beginString1 + _endString1;

                                #endregion CountryFrom
                            }

                            if (_locOrganizationSetupDetail2.ShipmentType == CustomProcess.ShipmentType.Import || _locOrganizationSetupDetail2.ShipmentType == CustomProcess.ShipmentType.ExportAndImport)
                            {
                                #region CountryTo
                                IEnumerable<string> _stringArrayCountryToDistinct = _stringCountry.Distinct();
                                string[] _stringArrayCountryToList = _stringArrayCountryToDistinct.ToArray();
                                if (_stringArrayCountryToList.Length == 1)
                                {
                                    for (int i = 0; i < _stringArrayCountryToList.Length; i++)
                                    {
                                        Country _locCountryTo = currentSession.FindObject<Country>(new BinaryOperator("Code", _stringArrayCountryToList[i]));
                                        if (_locCountryTo != null)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString2 = "[CountryTo.Code]=='" + _locCountryTo.Code + "'";
                                            }
                                        }
                                    }
                                }
                                else if (_stringArrayCountryToList.Length > 1)
                                {
                                    for (int i = 0; i < _stringArrayCountryToList.Length; i++)
                                    {
                                        Country _locCountryTo = currentSession.FindObject<Country>(new BinaryOperator("Code", _stringArrayCountryToList[i]));
                                        if (_locCountryTo != null)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString2 = "[CountryTo.Code]=='" + _locCountryTo.Code + "'";
                                            }
                                            else
                                            {
                                                _endString2 = _endString2 + " OR [CountryTo.Code]=='" + _locCountryTo.Code + "'";
                                            }
                                        }
                                    }
                                }
                                _fullString2 = _beginString2 + _endString2;

                                #endregion CountryTo
                            }
                        }
                    }

                    if (_fullString1 != null && _fullString2 != null)
                    {
                        _fullString = _fullString1 + " OR " + _fullString2;
                    }
                    else if (_fullString1 != null && _fullString2 == null)
                    {
                        _fullString = _fullString1;
                    }
                    else if (_fullString1 == null && _fullString2 != null)
                    {
                        _fullString = _fullString2;
                    }


                    ListView.CollectionSource.Criteria["ShipmentBookingMonitoringFilter"] = CriteriaOperator.Parse(_fullString);
                }
            }
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }
    }
}
