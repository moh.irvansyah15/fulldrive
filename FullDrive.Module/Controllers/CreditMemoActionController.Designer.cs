﻿namespace FullDrive.Module.Controllers
{
    partial class CreditMemoActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.CreditMemoProgressAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.CreditMemoPostingAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.CreditMemoListviewFilterSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.CreditMemoApprovalAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.CreditMemoGetPRMAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // CreditMemoProgressAction
            // 
            this.CreditMemoProgressAction.Caption = "Progress";
            this.CreditMemoProgressAction.ConfirmationMessage = null;
            this.CreditMemoProgressAction.Id = "CreditMemoProgressActionId";
            this.CreditMemoProgressAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.CreditMemo);
            this.CreditMemoProgressAction.ToolTip = null;
            this.CreditMemoProgressAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.CreditMemoProgressAction_Execute);
            // 
            // CreditMemoPostingAction
            // 
            this.CreditMemoPostingAction.Caption = "Posting";
            this.CreditMemoPostingAction.ConfirmationMessage = null;
            this.CreditMemoPostingAction.Id = "CreditMemoPostingActionId";
            this.CreditMemoPostingAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.CreditMemo);
            this.CreditMemoPostingAction.ToolTip = null;
            this.CreditMemoPostingAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.CreditMemoPostingAction_Execute);
            // 
            // CreditMemoListviewFilterSelectionAction
            // 
            this.CreditMemoListviewFilterSelectionAction.Caption = "Filter";
            this.CreditMemoListviewFilterSelectionAction.ConfirmationMessage = null;
            this.CreditMemoListviewFilterSelectionAction.Id = "CreditMemoListviewFilterSelectionActionId";
            this.CreditMemoListviewFilterSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.CreditMemoListviewFilterSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.CreditMemo);
            this.CreditMemoListviewFilterSelectionAction.ToolTip = null;
            this.CreditMemoListviewFilterSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.CreditMemoListviewFilterSelectionAction_Execute);
            // 
            // CreditMemoApprovalAction
            // 
            this.CreditMemoApprovalAction.Caption = "Approval";
            this.CreditMemoApprovalAction.ConfirmationMessage = null;
            this.CreditMemoApprovalAction.Id = "CreditMemoApprovalActionId";
            this.CreditMemoApprovalAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.CreditMemoApprovalAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.CreditMemo);
            this.CreditMemoApprovalAction.ToolTip = null;
            this.CreditMemoApprovalAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.CreditMemoApprovalAction_Execute);
            // 
            // CreditMemoGetPRMAction
            // 
            this.CreditMemoGetPRMAction.Caption = "Get PRM";
            this.CreditMemoGetPRMAction.ConfirmationMessage = null;
            this.CreditMemoGetPRMAction.Id = "CreditMemoGetPRMActionId";
            this.CreditMemoGetPRMAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.CreditMemo);
            this.CreditMemoGetPRMAction.ToolTip = null;
            this.CreditMemoGetPRMAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.CreditMemoGetPRMAction_Execute);
            // 
            // CreditMemoActionController
            // 
            this.Actions.Add(this.CreditMemoProgressAction);
            this.Actions.Add(this.CreditMemoPostingAction);
            this.Actions.Add(this.CreditMemoListviewFilterSelectionAction);
            this.Actions.Add(this.CreditMemoApprovalAction);
            this.Actions.Add(this.CreditMemoGetPRMAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction CreditMemoProgressAction;
        private DevExpress.ExpressApp.Actions.SimpleAction CreditMemoPostingAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction CreditMemoListviewFilterSelectionAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction CreditMemoApprovalAction;
        private DevExpress.ExpressApp.Actions.SimpleAction CreditMemoGetPRMAction;
    }
}
