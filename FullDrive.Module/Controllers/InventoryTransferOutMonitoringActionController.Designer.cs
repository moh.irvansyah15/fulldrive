﻿namespace FullDrive.Module.Controllers
{
    partial class InventoryTransferOutMonitoringActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.InventoryTransferOutMonitoringSelectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.InventoryTransferOutMonitoringUnselectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.InventoryTransferOutMonitoringReturnTypeAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // InventoryTransferOutMonitoringSelectAction
            // 
            this.InventoryTransferOutMonitoringSelectAction.Caption = "Select";
            this.InventoryTransferOutMonitoringSelectAction.ConfirmationMessage = null;
            this.InventoryTransferOutMonitoringSelectAction.Id = "InventoryTransferOutMonitoringSelectActionId";
            this.InventoryTransferOutMonitoringSelectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.InventoryTransferOutMonitoring);
            this.InventoryTransferOutMonitoringSelectAction.ToolTip = null;
            this.InventoryTransferOutMonitoringSelectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.InventoryTransferOutMonitoringSelectAction_Execute);
            // 
            // InventoryTransferOutMonitoringUnselectAction
            // 
            this.InventoryTransferOutMonitoringUnselectAction.Caption = "Unselect";
            this.InventoryTransferOutMonitoringUnselectAction.ConfirmationMessage = null;
            this.InventoryTransferOutMonitoringUnselectAction.Id = "InventoryTransferOutMonitoringUnselectActionId";
            this.InventoryTransferOutMonitoringUnselectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.InventoryTransferOutMonitoring);
            this.InventoryTransferOutMonitoringUnselectAction.ToolTip = null;
            this.InventoryTransferOutMonitoringUnselectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.InventoryTransferOutMonitoringUnselectAction_Execute);
            // 
            // InventoryTransferOutMonitoringReturnTypeAction
            // 
            this.InventoryTransferOutMonitoringReturnTypeAction.Caption = "Return Type";
            this.InventoryTransferOutMonitoringReturnTypeAction.ConfirmationMessage = null;
            this.InventoryTransferOutMonitoringReturnTypeAction.Id = "InventoryTransferOutMonitoringReturnTypeActionId";
            this.InventoryTransferOutMonitoringReturnTypeAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.InventoryTransferOutMonitoring);
            this.InventoryTransferOutMonitoringReturnTypeAction.ToolTip = null;
            this.InventoryTransferOutMonitoringReturnTypeAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.InventoryTransferOutMonitoringReturnTypeAction_Execute);
            // 
            // InventoryTransferOutMonitoringActionController
            // 
            this.Actions.Add(this.InventoryTransferOutMonitoringSelectAction);
            this.Actions.Add(this.InventoryTransferOutMonitoringUnselectAction);
            this.Actions.Add(this.InventoryTransferOutMonitoringReturnTypeAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction InventoryTransferOutMonitoringSelectAction;
        private DevExpress.ExpressApp.Actions.SimpleAction InventoryTransferOutMonitoringUnselectAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction InventoryTransferOutMonitoringReturnTypeAction;
    }
}
