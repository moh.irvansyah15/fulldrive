﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using System.Collections;
using DevExpress.Xpo;


namespace FullDrive.Module.BusinessObjects
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class ShipmentPurchaseCollectionActionController : ViewController
    {
        public ShipmentPurchaseCollectionActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void ShipmentPurchaseCollectionShowSPMAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
            var ListView = View as ListView;
            IObjectSpace os = Application.CreateObjectSpace();
            string listViewId = Application.FindListViewId(typeof(ShipmentPurchaseMonitoring));
            CollectionSourceBase cs = Application.CreateCollectionSource(os, typeof(ShipmentPurchaseMonitoring), listViewId);
            //Tinggal buat 
            ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
            string _stringSPM = null;
            if (_objectsToProcess != null)
            {
                foreach (Object obj in _objectsToProcess)
                {
                    ShipmentPurchaseCollection _locShipmentPurchaseCollection = (ShipmentPurchaseCollection)_objectSpace.GetObject(obj);
                    if (_locShipmentPurchaseCollection != null)
                    {
                        if (_locShipmentPurchaseCollection.ShipmentPurchaseMonitoring != null && _locShipmentPurchaseCollection.Select == true)
                        {
                            if (_stringSPM == null)
                            {
                                if (_locShipmentPurchaseCollection.ShipmentPurchaseMonitoring.Code != null)
                                {
                                    _stringSPM = "( [Code]=='" + _locShipmentPurchaseCollection.ShipmentPurchaseMonitoring.Code + "' AND [Status] != 4 )";
                                }

                            }
                            else
                            {
                                if (_locShipmentPurchaseCollection.ShipmentPurchaseMonitoring.Code != null)
                                {
                                    _stringSPM = _stringSPM + " OR ( [Code]=='" + _locShipmentPurchaseCollection.ShipmentPurchaseMonitoring.Code + "' AND [Status] != 4 )";
                                }
                            }
                        }
                    }
                }
            }
            if (_stringSPM != null)
            {
                cs.Criteria["ShipmentPurchaseMonitoringCollectionFilterITO"] = CriteriaOperator.Parse(_stringSPM);
            }
            e.ShowViewParameters.CreatedView = Application.CreateListView(
                listViewId,
                cs,
                true
            );
            e.ShowViewParameters.Context = TemplateContext.View;
            e.ShowViewParameters.TargetWindow = TargetWindow.NewModalWindow;
            DialogController dc = Application.CreateController<DialogController>();
            dc.Accepting += new EventHandler<DialogControllerAcceptingEventArgs>(dc_Accepting);
            e.ShowViewParameters.Controllers.Add(dc);
        }

        void dc_Accepting(object sender, DialogControllerAcceptingEventArgs e)
        {
            try
            {
                ShipmentPurchaseInvoice _locShipmentPurchaseInvoice = null;
                View popupView = ((Controller)sender).Frame.View;
                ArrayList _objectsToProcess = new ArrayList(View.SelectedObjects);
                if (_objectsToProcess != null)
                {
                    foreach (Object _obj in _objectsToProcess)
                    {
                        ShipmentPurchaseCollection _locShipmentPurchaseCollection = (ShipmentPurchaseCollection)popupView.ObjectSpace.GetObject(_obj);
                        if (_locShipmentPurchaseCollection != null)
                        {
                            if (_locShipmentPurchaseCollection.ShipmentPurchaseInvoice != null)
                            {
                                _locShipmentPurchaseInvoice = _locShipmentPurchaseCollection.ShipmentPurchaseInvoice;
                            }
                        }
                    }
                }
                if (_locShipmentPurchaseInvoice != null)
                {
                    ArrayList _objectsToProcess2 = new ArrayList(popupView.SelectedObjects);
                    if (_objectsToProcess2 != null)
                    {
                        foreach (Object _obj2 in _objectsToProcess2)
                        {
                            ShipmentPurchaseMonitoring _locShipPurMonitoring = (ShipmentPurchaseMonitoring)popupView.ObjectSpace.GetObject(_obj2);
                            if (_locShipPurMonitoring != null && _locShipPurMonitoring.Select == true && _locShipPurMonitoring.Status != Status.Close)
                            {
                                _locShipPurMonitoring.ShipmentPurchaseInvoice = _locShipmentPurchaseInvoice;
                            }
                        }
                    }
                }
                popupView.ObjectSpace.CommitChanges();
            }
            catch (ValidationException)
            {
            }
        }

        
    }
}
