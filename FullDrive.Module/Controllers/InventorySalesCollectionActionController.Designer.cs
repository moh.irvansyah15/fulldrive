﻿namespace FullDrive.Module.Controllers
{
    partial class InventorySalesCollectionActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.InventorySalesCollectionShowSOMAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.InventorySalesCollectionShowSRMAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // InventorySalesCollectionShowSOMAction
            // 
            this.InventorySalesCollectionShowSOMAction.Caption = "Show SOM";
            this.InventorySalesCollectionShowSOMAction.ConfirmationMessage = null;
            this.InventorySalesCollectionShowSOMAction.Id = "InventorySalesCollectionShowSOMActionId";
            this.InventorySalesCollectionShowSOMAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.InventorySalesCollection);
            this.InventorySalesCollectionShowSOMAction.ToolTip = null;
            this.InventorySalesCollectionShowSOMAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.InventorySalesCollectionShowSOMAction_Execute);
            // 
            // InventorySalesCollectionShowSRMAction
            // 
            this.InventorySalesCollectionShowSRMAction.Caption = "Show SRM";
            this.InventorySalesCollectionShowSRMAction.ConfirmationMessage = null;
            this.InventorySalesCollectionShowSRMAction.Id = "InventorySalesCollectionShowSRMActionId";
            this.InventorySalesCollectionShowSRMAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.InventorySalesCollection);
            this.InventorySalesCollectionShowSRMAction.ToolTip = null;
            this.InventorySalesCollectionShowSRMAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.InventorySalesCollectionShowSRMAction_Execute);
            // 
            // InventorySalesCollectionActionController
            // 
            this.Actions.Add(this.InventorySalesCollectionShowSOMAction);
            this.Actions.Add(this.InventorySalesCollectionShowSRMAction);

        }

        #endregion
        private DevExpress.ExpressApp.Actions.SimpleAction InventorySalesCollectionShowSOMAction;
        private DevExpress.ExpressApp.Actions.SimpleAction InventorySalesCollectionShowSRMAction;
    }
}
