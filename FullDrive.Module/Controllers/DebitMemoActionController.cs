﻿#region Default

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

#endregion Default

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;
using System.Web;
using System.IO;


namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class DebitMemoActionController : ViewController
    {
        #region Default

        private ChoiceActionItem _selectionListviewFilter;
        private ChoiceActionItem _setApprovalLevel;

        public DebitMemoActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            DebitMemoFilterAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                DebitMemoFilterAction.Items.Add(_selectionListviewFilter);
            }
        }
        protected override void OnActivated()
        {
            TypeOfView = typeof(ObjectView);
            base.OnActivated();
            // Perform various tasks depending on the target View.
            #region Approval
            var _locUser = SecuritySystem.CurrentUserName;
            GlobalFunction _globalFunction = new GlobalFunction();
            Session _currentSession = null;
            if (this.ObjectSpace != null)
            {
                _currentSession = ((XPObjectSpace)this.ObjectSpace).Session;
            }

            if (_currentSession != null)
            {
                UserAccess _locUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", _locUser));

                if (_locUserAccess != null)
                {
                    DebitMemoApprovalAction.Items.Clear();

                    XPCollection<ApplicationSetupDetail> _locAppSetupDetails = new XPCollection<ApplicationSetupDetail>
                                                                                 (_currentSession, new GroupOperator(GroupOperatorType.And,
                                                                                  new BinaryOperator("UserAccess", _locUserAccess),
                                                                                  new BinaryOperator("ObjectList", CustomProcess.ObjectList.DebitMemo),
                                                                                  new BinaryOperator("FunctionList", CustomProcess.FunctionList.Approval),
                                                                                  new BinaryOperator("Active", true)));

                    if (_locAppSetupDetails != null && _locAppSetupDetails.Count > 0)
                    {

                        foreach (ApplicationSetupDetail _locAppSetupDetail in _locAppSetupDetails)
                        {
                            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.ApprovalLevel)))
                            {
                                if (_locAppSetupDetail.ApprovalLevel != CustomProcess.ApprovalLevel.None)
                                {
                                    EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.ApprovalLevel));
                                    _setApprovalLevel = new ChoiceActionItem(_ed.GetCaption(_locAppSetupDetail.ApprovalLevel), _locAppSetupDetail.ApprovalLevel);
                                }
                            }
                            DebitMemoApprovalAction.Items.Add(_setApprovalLevel);
                        }
                    }
                }
            }
            #endregion Approval
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        #endregion Default

        private void DebitMemoGetSRMAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        DebitMemo _locDebitMemoOS = (DebitMemo)_objectSpace.GetObject(obj);

                        if (_locDebitMemoOS != null)
                        {
                            if (_locDebitMemoOS.Code != null)
                            {
                                _currObjectId = _locDebitMemoOS.Code;

                                DebitMemo _locDebitMemoXPO = _currSession.FindObject<DebitMemo>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locDebitMemoXPO != null)
                                {
                                    if (_locDebitMemoXPO.Status == Status.Open || _locDebitMemoXPO.Status == Status.Progress)
                                    {
                                        XPCollection<DebitSalesCollection> _locDebitSalesCollections = new XPCollection<DebitSalesCollection>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("DebitMemo", _locDebitMemoXPO),
                                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("Status", Status.Open),
                                                                                        new BinaryOperator("Status", Status.Progress))));

                                        if (_locDebitSalesCollections != null && _locDebitSalesCollections.Count() > 0)
                                        {
                                            foreach (DebitSalesCollection _locDebitSalesCollection in _locDebitSalesCollections)
                                            {
                                                if (_locDebitSalesCollection.DebitMemo != null)
                                                {
                                                    ApprovalLine _locApprovalLineXPO = _currSession.FindObject<ApprovalLine>
                                                                       (new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("EndApproval", true),
                                                                        new BinaryOperator("DebitMemo", _locDebitSalesCollection.DebitMemo)));
                                                    if (_locApprovalLineXPO != null)
                                                    {
                                                        GetSalesReturnMonitoring(_currSession, _locDebitSalesCollection.SalesReturn, _locDebitMemoXPO);
                                                    }
                                                }
                                            }
                                            SuccessMessageShow("Debit Memo Has Been Successfully Getting into Sales Order");
                                        }
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data Debit Memo Not Available");
                                }

                            }
                            else
                            {
                                ErrorMessageShow("Data Debit Memo Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrder " + ex.ToString());
            }
        }

        private void DebitMemoProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        DebitMemo _locDebitMemoOS = (DebitMemo)_objectSpace.GetObject(obj);

                        if (_locDebitMemoOS != null)
                        {
                            if (_locDebitMemoOS.Code != null)
                            {
                                _currObjectId = _locDebitMemoOS.Code;

                                DebitMemo _locDebitMemoXPO = _currSession.FindObject<DebitMemo>
                                                             (new GroupOperator(GroupOperatorType.And,
                                                              new BinaryOperator("Code", _currObjectId)));

                                if (_locDebitMemoXPO != null)
                                {
                                    if (_locDebitMemoXPO.Status == Status.Open || _locDebitMemoXPO.Status == Status.Progress)
                                    {
                                        _locDebitMemoXPO.Status = Status.Progress;
                                        _locDebitMemoXPO.StatusDate = now;
                                        _locDebitMemoXPO.Save();
                                        _locDebitMemoXPO.Session.CommitTransaction();

                                        XPCollection<DebitMemoLine> _locDebitMemoLines = new XPCollection<DebitMemoLine>
                                                                                         (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                          new BinaryOperator("DebitMemo", _locDebitMemoXPO),
                                                                                          new BinaryOperator("Status", Status.Open)));

                                        if (_locDebitMemoLines != null && _locDebitMemoLines.Count > 0)
                                        {
                                            foreach (DebitMemoLine _locDebitMemoLine in _locDebitMemoLines)
                                            {
                                                _locDebitMemoLine.Status = Status.Progress;
                                                _locDebitMemoLine.StatusDate = now;
                                                _locDebitMemoLine.Save();
                                                _locDebitMemoLine.Session.CommitTransaction();
                                                
                                            }
                                        }

                                        XPCollection<DebitSalesCollection> _locDebitSalesCollections = new XPCollection<DebitSalesCollection>
                                                                                         (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                          new BinaryOperator("DebitMemo", _locDebitMemoXPO),
                                                                                          new BinaryOperator("Status", Status.Open)));
                                        if(_locDebitSalesCollections != null && _locDebitSalesCollections.Count() > 0)
                                        {
                                            foreach(DebitSalesCollection _locDebitSalesCollection in _locDebitSalesCollections)
                                            {
                                                _locDebitSalesCollection.Status = Status.Progress;
                                                _locDebitSalesCollection.StatusDate = now;
                                                _locDebitSalesCollection.Save();
                                                _locDebitSalesCollection.Session.CommitTransaction();
                                            }
                                        }
                                        SuccessMessageShow(_locDebitMemoXPO.Code + " has been change successfully to Progress");
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data DebitMemo Not Available");
                                }

                            }
                            else
                            {
                                ErrorMessageShow("Data DebitMemo Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = DebitMemo " + ex.ToString());
            }
        }

        private void DebitMemoApprovalAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                ApplicationSetupDetail _locAppSetDetail = null;
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                var User = SecuritySystem.CurrentUserName;
                Session _currentSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currentSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                UserAccess _locUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", User));

                foreach (Object obj in _objectsToProcess)
                {
                    DebitMemo _objInNewObjectSpace = (DebitMemo)_objectSpace.GetObject(obj);

                    if (_objInNewObjectSpace != null)
                    {
                        if (_objInNewObjectSpace.Code != null)
                        {
                            _currObjectId = _objInNewObjectSpace.Code;
                        }
                    }

                    if (_currObjectId != null)
                    {
                        DebitMemo _locDebitMemoXPO = _currentSession.FindObject<DebitMemo>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("Code", _currObjectId)));

                        if (_locDebitMemoXPO != null)
                        {
                            if (_locDebitMemoXPO.Status == Status.Progress)
                            {
                                ApprovalLine _locApprovalLine = _currentSession.FindObject<ApprovalLine>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("DebitMemo", _locDebitMemoXPO),
                                                                             new BinaryOperator("EndApproval", true)));
                                if (_locApprovalLine == null)
                                {
                                    #region Approval Level 1
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level1)
                                    {
                                        _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level1, ObjectList.DebitMemo);
                                        if (_locAppSetDetail != null)
                                        {
                                            //Buat bs input langsung ke approvalline
                                            ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("DebitMemo", _locDebitMemoXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level1)));
                                            if (_locApprovalLineXPO == null)
                                            {

                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level1,
                                                        EndApproval = true,
                                                        DebitMemo = _locDebitMemoXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locDebitMemoXPO.ActivationPosting = true;
                                                    _locDebitMemoXPO.ActiveApproved1 = false;
                                                    _locDebitMemoXPO.ActiveApproved2 = false;
                                                    _locDebitMemoXPO.ActiveApproved3 = true;
                                                    _locDebitMemoXPO.Save();
                                                    _locDebitMemoXPO.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level1,
                                                        DebitMemo = _locDebitMemoXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locDebitMemoXPO.ActiveApproved1 = true;
                                                    _locDebitMemoXPO.ActiveApproved2 = false;
                                                    _locDebitMemoXPO.ActiveApproved3 = false;
                                                    _locDebitMemoXPO.Save();
                                                    _locDebitMemoXPO.Session.CommitTransaction();
                                                }

                                                //Send Email
                                                ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("DebitMemo", _locDebitMemoXPO)));

                                                if (_locApprovalLineXPO2 != null)
                                                {
                                                    SendEmail(_currentSession, _locDebitMemoXPO, Status.Progress, _locApprovalLineXPO2);
                                                }

                                                SuccessMessageShow("DebitMemo has successfully Approve");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 1

                                    #region Approval Level 2
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level2)
                                    {
                                        _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level2, ObjectList.DebitMemo);

                                        if (_locAppSetDetail != null)
                                        {
                                            ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("DebitMemo", _locDebitMemoXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level2)));

                                            if (_locApprovalLineXPO == null)
                                            {
                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level2,
                                                        EndApproval = true,
                                                        DebitMemo = _locDebitMemoXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locDebitMemoXPO.ActivationPosting = true;
                                                    _locDebitMemoXPO.ActiveApproved1 = false;
                                                    _locDebitMemoXPO.ActiveApproved2 = false;
                                                    _locDebitMemoXPO.ActiveApproved3 = true;
                                                    _locDebitMemoXPO.Save();
                                                    _locDebitMemoXPO.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level2,
                                                        DebitMemo = _locDebitMemoXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    SetApprovalLine(_currentSession, _locDebitMemoXPO, ApprovalLevel.Level1);

                                                    _locDebitMemoXPO.ActiveApproved1 = false;
                                                    _locDebitMemoXPO.ActiveApproved2 = true;
                                                    _locDebitMemoXPO.ActiveApproved3 = false;
                                                    _locDebitMemoXPO.Save();
                                                    _locDebitMemoXPO.Session.CommitTransaction();
                                                }

                                                //Send Email
                                                ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("DebitMemo", _locDebitMemoXPO)));

                                                if (_locApprovalLineXPO2 != null)
                                                {
                                                    SendEmail(_currentSession, _locDebitMemoXPO, Status.Progress, _locApprovalLineXPO2);
                                                }

                                                SuccessMessageShow("DebitMemo has successfully Approve");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 2

                                    #region Approval Level 3
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level3)
                                    {
                                        _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level3, ObjectList.DebitMemo);

                                        if (_locAppSetDetail != null)
                                        {
                                            ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("DebitMemo", _locDebitMemoXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level3)));

                                            if (_locApprovalLineXPO == null)
                                            {
                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level3,
                                                        EndApproval = true,
                                                        DebitMemo = _locDebitMemoXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locDebitMemoXPO.ActivationPosting = true;
                                                    _locDebitMemoXPO.ActiveApproved1 = false;
                                                    _locDebitMemoXPO.ActiveApproved2 = false;
                                                    _locDebitMemoXPO.ActiveApproved3 = true;
                                                    _locDebitMemoXPO.Save();
                                                    _locDebitMemoXPO.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level3,
                                                        DebitMemo = _locDebitMemoXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    SetApprovalLine(_currentSession, _locDebitMemoXPO, ApprovalLevel.Level2);
                                                    SetApprovalLine(_currentSession, _locDebitMemoXPO, ApprovalLevel.Level1);

                                                    _locDebitMemoXPO.ActiveApproved1 = false;
                                                    _locDebitMemoXPO.ActiveApproved2 = false;
                                                    _locDebitMemoXPO.ActiveApproved3 = true;
                                                    _locDebitMemoXPO.Save();
                                                    _locDebitMemoXPO.Session.CommitTransaction();
                                                }

                                                //Send Email
                                                ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("DebitMemo", _locDebitMemoXPO)));

                                                if (_locApprovalLineXPO2 != null)
                                                {
                                                    SendEmail(_currentSession, _locDebitMemoXPO, Status.Progress, _locApprovalLineXPO2);
                                                }

                                                SuccessMessageShow("DebitMemo has successfully Approve");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 3
                                }

                            }
                            else
                            {
                                ErrorMessageShow("SalesReturn Status Not Available");
                            }
                        }
                        else
                        {
                            ErrorMessageShow("SalesReturn Not Available");
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesReturn " + ex.ToString());
            }
        }

        private void DebitMemoPostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        DebitMemo _locDebitMemoOS = (DebitMemo)_objectSpace.GetObject(obj);

                        if (_locDebitMemoOS != null)
                        {
                            if (_locDebitMemoOS.Code != null)
                            {
                                _currObjectId = _locDebitMemoOS.Code;

                                DebitMemo _locDebitMemoXPO = _currSession.FindObject<DebitMemo>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locDebitMemoXPO != null)
                                {
                                    if (_locDebitMemoXPO.Status == Status.Progress || _locDebitMemoXPO.Status == Status.Posted)
                                    {
                                        SetReverseGoodsReceiveReturnJournal(_currSession, _locDebitMemoXPO);
                                        SetReturnJournal(_currSession, _locDebitMemoXPO);
                                        SetSalesReturnMonitoring(_currSession, _locDebitMemoXPO);
                                        SetRemainQty(_currSession, _locDebitMemoXPO);
                                        SetPostingQty(_currSession, _locDebitMemoXPO);
                                        SetProcessCount(_currSession, _locDebitMemoXPO);
                                        SetStatusSalesReturnLine(_currSession, _locDebitMemoXPO);
                                        SetNormalQuantity(_currSession, _locDebitMemoXPO);
                                        SetFinalStatusSalesReturn(_currSession, _locDebitMemoXPO);
                                    }

                                    SuccessMessageShow("Sales Return has been successfully Posted");
                                }
                                else
                                {
                                    ErrorMessageShow("Data Sales Return Not Available");
                                }

                            }
                            else
                            {
                                ErrorMessageShow("Data Sales Return Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesReturn " + ex.ToString());
            }
        }

        private void DebitMemoFilterAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(DebitMemo)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = DebitMemo " + ex.ToString());
            }
        }

        //=========================================== Code Only ===========================================

        #region GetITO

        private void GetSalesReturnMonitoring(Session _currSession, SalesReturn _locSalesReturnXPO, DebitMemo _locDebitMemoXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _currSignCode = null;
                double _locTxValue = 0;
                double _locTxAmount = 0;
                double _locDisc = 0;
                double _locDiscAmount = 0;
                double _locTAmount = 0;
                

                if (_locSalesReturnXPO != null)
                {
                    XPCollection<SalesReturnMonitoring> _locSalesReturnMonitorings = new XPCollection<SalesReturnMonitoring>
                                                                                 (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                  new BinaryOperator("SalesReturn", _locSalesReturnXPO),
                                                                                  new BinaryOperator("DebitMemo", _locDebitMemoXPO),
                                                                                  new BinaryOperator("Select", true),
                                                                                  new BinaryOperator("ReturnType", ReturnType.DebitMemo)
                                                                                  ));
                    if(_locSalesReturnMonitorings != null && _locSalesReturnMonitorings.Count() > 0)
                    {
                        foreach(SalesReturnMonitoring _locSalesReturnMonitoring in _locSalesReturnMonitorings)
                        {
                            if (_locSalesReturnMonitoring.SalesReturnLine != null && _locSalesReturnMonitoring.SalesOrderMonitoring != null )
                            {
                                _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.DebitMemoLine);

                                if (_currSignCode != null)
                                {
                                    #region CountTax
                                    if (_locSalesReturnMonitoring.SalesOrderMonitoring.MultiTax == true)
                                    {
                                        _locTxValue = GetSumTotalTaxValue(_currSession, _locSalesReturnMonitoring.SalesReturnLine);
                                        _locTxAmount = GetSumTotalTaxAmount(_currSession, _locSalesReturnMonitoring.SalesReturnLine, _locSalesReturnMonitoring);
                                    }
                                    else
                                    {
                                        if (_locSalesReturnMonitoring.TxValue > 0)
                                        {
                                            _locTxValue = _locSalesReturnMonitoring.TxValue;
                                            _locTxAmount = (_locTxValue / 100) * (_locSalesReturnMonitoring.TQty * _locSalesReturnMonitoring.UAmount);
                                        }
                                    }
                                    #endregion CountTax

                                    #region CountDiscount
                                    if (_locSalesReturnMonitoring.MultiDiscount == true)
                                    {
                                        _locDisc = GetSumTotalDisc(_currSession, _locSalesReturnMonitoring.SalesReturnLine);
                                        _locDiscAmount = GetSumTotalDiscAmount(_currSession, _locSalesReturnMonitoring.SalesReturnLine, _locSalesReturnMonitoring);
                                    }
                                    else
                                    {
                                        if (_locSalesReturnMonitoring.SalesOrderMonitoring.Disc > 0)
                                        {
                                            _locDisc = _locSalesReturnMonitoring.Disc;
                                            _locDiscAmount = (_locDisc / 100) * (_locSalesReturnMonitoring.TQty * _locSalesReturnMonitoring.UAmount);
                                        }
                                    }
                                    #endregion CountDiscount

                                    _locTAmount = (_locSalesReturnMonitoring.SalesOrderMonitoring.UAmount * _locSalesReturnMonitoring.TQty) + _locTxAmount - _locDiscAmount;
                                    //Tempat untuk menghitung multitax dan multidiscount
                                    DebitMemoLine _saveDataDebitMemoLine = new DebitMemoLine(_currSession)
                                    {
                                        SignCode = _currSignCode,
                                        OrderType = _locSalesReturnMonitoring.SalesReturnLine.OrderType,
                                        Item = _locSalesReturnMonitoring.Item,
                                        Description = _locSalesReturnMonitoring.SalesReturnLine.Description,
                                        MxDQty = _locSalesReturnMonitoring.DQty,
                                        MxDUOM = _locSalesReturnMonitoring.DUOM,
                                        MxQty = _locSalesReturnMonitoring.Qty,
                                        MxUOM = _locSalesReturnMonitoring.UOM,
                                        MxTQty = _locSalesReturnMonitoring.TQty,
                                        MxUAmount = _locSalesReturnMonitoring.UAmount,
                                        DQty = _locSalesReturnMonitoring.DQty,
                                        DUOM = _locSalesReturnMonitoring.DUOM,
                                        Qty = _locSalesReturnMonitoring.Qty,
                                        UOM = _locSalesReturnMonitoring.UOM,
                                        TQty = _locSalesReturnMonitoring.TQty,
                                        Currency = _locSalesReturnMonitoring.Currency,
                                        PriceGroup = _locSalesReturnMonitoring.PriceGroup,
                                        Price = _locSalesReturnMonitoring.Price,
                                        PriceLine = _locSalesReturnMonitoring.PriceLine,
                                        UAmount = _locSalesReturnMonitoring.UAmount,
                                        TUAmount = _locSalesReturnMonitoring.TQty * _locSalesReturnMonitoring.UAmount,
                                        MultiTax = _locSalesReturnMonitoring.MultiTax,
                                        Tax = _locSalesReturnMonitoring.Tax,
                                        TxValue = _locTxValue,
                                        TxAmount = _locTxAmount,
                                        MultiDiscount = _locSalesReturnMonitoring.MultiDiscount,
                                        Discount = _locSalesReturnMonitoring.Discount,
                                        Disc = _locDisc,
                                        DiscAmount = _locDiscAmount,
                                        TAmount = _locTAmount,
                                        SalesReturnMonitoring = _locSalesReturnMonitoring,
                                        ReturnType = _locSalesReturnMonitoring.ReturnType,
                                    };
                                    _saveDataDebitMemoLine.Save();
                                    _saveDataDebitMemoLine.Session.CommitTransaction();

                                    DebitMemoLine _locDebMemoLine = _currSession.FindObject<DebitMemoLine>
                                                                (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("SignCode", _currSignCode)));
                                    if (_locDebMemoLine != null)
                                    {
                                        #region TaxLine
                                        if (_locDebMemoLine.MultiTax == true && _locSalesReturnMonitoring.SalesReturnLine != null)
                                        {
                                            XPCollection<TaxLine> _locTaxLines = new XPCollection<TaxLine>(_currSession, new GroupOperator( GroupOperatorType.And,
                                                                                new BinaryOperator("SalesReturnLine", _locSalesReturnMonitoring.SalesReturnLine)));
                                            if (_locTaxLines != null && _locTaxLines.Count() > 0)
                                            {
                                                foreach (TaxLine _locTaxLine in _locTaxLines)
                                                {
                                                    TaxLine _saveDataTaxLine = new TaxLine(_currSession)
                                                    {
                                                        Tax = _locTaxLine.Tax,
                                                        Name = _locTaxLine.Name,
                                                        TaxType = _locTaxLine.TaxType,
                                                        TaxGroup = _locTaxLine.TaxGroup,
                                                        TaxGroupType = _locTaxLine.TaxGroupType,
                                                        TaxAccountGroup = _locTaxLine.TaxAccountGroup,
                                                        TaxNature = _locTaxLine.TaxNature,
                                                        TaxMode = _locTaxLine.TaxMode,
                                                        TxValue = _locTaxLine.TxValue,
                                                        Description = _locTaxLine.Description,
                                                        UAmount = _locDebMemoLine.UAmount,
                                                        TUAmount = _locDebMemoLine.TUAmount,
                                                        TxAmount = _locDebMemoLine.TxAmount,
                                                        DebitMemoLine = _locDebMemoLine,
                                                    };
                                                    _saveDataTaxLine.Save();
                                                    _saveDataTaxLine.Session.CommitTransaction();
                                                }
                                            }
                                        }
                                        #endregion TaxLine
                                        #region DiscountLine
                                        if (_locDebMemoLine.MultiDiscount == true)
                                        {
                                            XPCollection<DiscountLine> _locDiscountLines = new XPCollection<DiscountLine>(_currSession,
                                                                                new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("SalesReturnLine", _locSalesReturnMonitoring.SalesReturnLine)));
                                            if (_locDiscountLines != null && _locDiscountLines.Count() > 0)
                                            {
                                                foreach (DiscountLine _locDiscountLine in _locDiscountLines)
                                                {
                                                    DiscountLine _saveDataDiscountLine = new DiscountLine(_currSession)
                                                    {
                                                        Discount = _locDiscountLine.Discount,
                                                        Name = _locDiscountLine.Name,
                                                        DiscountMode = _locDiscountLine.DiscountMode,
                                                        Description = _locDiscountLine.Description,
                                                        UAmount = _locDebMemoLine.UAmount,
                                                        Disc = _locDiscountLine.Disc,
                                                        DiscAmount = _locDebMemoLine.DiscAmount,
                                                        TUAmount = _locDebMemoLine.TUAmount,
                                                        DebitMemoLine = _locDebMemoLine,
                                                    };
                                                    _saveDataDiscountLine.Save();
                                                    _saveDataDiscountLine.Session.CommitTransaction();
                                                }
                                            }
                                        }
                                        #endregion DiscountLine

                                        _locSalesReturnMonitoring.DebitMemoLine = _locDebMemoLine;
                                        _locSalesReturnMonitoring.MemoStatus = Status.Close;
                                        _locSalesReturnMonitoring.MemoStatusDate = now;
                                        _locSalesReturnMonitoring.Save();
                                        _locSalesReturnMonitoring.Session.CommitTransaction();
                                    }
                                }
                            }
                        }
                    }

                    //================================================================================================================
                    //Harus mampu menghitung otomatis buat MultiTax dan MultiDiscount
                    
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = SalesReturn ", ex.ToString());
            }
        }

        private double GetSumTotalTaxValue(Session _currSession, SalesReturnLine _salesReturnLineXPO)
        {
            double _result = 0;
            try
            {
                double _locTxValue = 0;

                if (_salesReturnLineXPO != null)
                {
                    XPCollection<TaxLine> _locTaxLines = new XPCollection<TaxLine>(_currSession,
                                                     new BinaryOperator("SalesReturnLine", _salesReturnLineXPO));

                    if (_locTaxLines.Count() > 0)
                    {
                        foreach (TaxLine _locTaxLine in _locTaxLines)
                        {
                            if (_locTaxLine.TxValue > 0 && _locTaxLine.TxAmount > 0)
                            {
                                if (_locTaxLine.TaxNature == TaxNature.Increase)
                                {
                                    _locTxValue = _locTxValue + _locTaxLine.TxValue;

                                }
                                else if (_locTaxLine.TaxNature == TaxNature.Decrease)
                                {
                                    _locTxValue = _locTxValue - _locTaxLine.TxValue;
                                }
                            }
                        }
                        _result = _locTxValue;
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = DebitMemo " + ex.ToString());
            }
            return _result;
        }

        private double GetSumTotalTaxAmount(Session _currSession, SalesReturnLine _salesReturnLineXPO, SalesReturnMonitoring _salesReturnMonitoringXPO)
        {
            double _return = 0;
            try
            {
                double _locTxAmount = 0;

                if (_salesReturnLineXPO != null)
                {
                    XPCollection<TaxLine> _locTaxLines = new XPCollection<TaxLine>(_currSession,
                                                     new BinaryOperator("SalesReturnLine", _salesReturnLineXPO));

                    if (_locTaxLines.Count() > 0)
                    {
                        foreach (TaxLine _locTaxLine in _locTaxLines)
                        {
                            if (_locTaxLine.TxValue > 0)
                            {
                                if (_locTaxLine.TaxNature == TaxNature.Increase)
                                {
                                    _locTxAmount = _locTxAmount + (_salesReturnLineXPO.UAmount * _salesReturnMonitoringXPO.TQty * _locTaxLine.TxValue / 100);
                                }
                                else if (_locTaxLine.TaxNature == TaxNature.Decrease)
                                {
                                    _locTxAmount = _locTxAmount - (_salesReturnLineXPO.UAmount * _salesReturnMonitoringXPO.TQty * _locTaxLine.TxValue / 100);
                                }
                            }
                        }
                        _return = _locTxAmount;
                    }
                }


            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = DebitMemo " + ex.ToString());
            }
            return _return;
        }

        private double GetSumTotalDisc(Session _currSession, SalesReturnLine _salesReturnLineXPO)
        {
            double _result = 0;
            try
            {
                double _locDisc = 0;


                if (_salesReturnLineXPO != null)
                {
                    XPCollection<DiscountLine> _locDiscountLines = new XPCollection<DiscountLine>(_currSession,
                                                               new BinaryOperator("SalesReturnLine", _salesReturnLineXPO));

                    if (_locDiscountLines.Count() > 0)
                    {
                        foreach (DiscountLine _locDiscountLine in _locDiscountLines)
                        {
                            if (_locDiscountLine.Disc > 0 && _locDiscountLine.DiscAmount > 0)
                            {
                                if (_locDiscountLine.Disc > 0 && _locDiscountLine.DiscAmount > 0)
                                {
                                    _locDisc = _locDisc + _locDiscountLine.Disc;
                                }
                            }
                        }
                        _result = _locDisc;
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = DebitMemo " + ex.ToString());
            }
            return _result;
        }

        private double GetSumTotalDiscAmount(Session _currSession, SalesReturnLine _salesReturnLineXPO, SalesReturnMonitoring _salesReturnMonitoringXPO)
        {
            double _result = 0;
            try
            {
                double _locDiscAmount = 0;

                if (_salesReturnLineXPO != null)
                {
                    XPCollection<DiscountLine> _locDiscountLines = new XPCollection<DiscountLine>(_currSession,
                                                               new BinaryOperator("SalesReturnLine", _salesReturnLineXPO));

                    if (_locDiscountLines.Count() > 0)
                    {
                        foreach (DiscountLine _locDiscountLine in _locDiscountLines)
                        {
                            if (_locDiscountLine.Disc > 0)
                            {
                                _locDiscAmount = _locDiscAmount + (_salesReturnLineXPO.UAmount * _salesReturnMonitoringXPO.TQty * _locDiscountLine.Disc / 100);
                            }
                        }
                        _result = _locDiscAmount;
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = DebitMemo " + ex.ToString());
            }
            return _result;
        }

        #endregion GetITO

        #region Posting

        private void SetReverseGoodsReceiveReturnJournal(Session _currSession, DebitMemo _locDebitMemoInXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                double _locTotalUnitAmount = 0;

                if (_locDebitMemoInXPO != null)
                {
                    XPCollection<DebitMemoLine> _locDebitMemoLines = new XPCollection<DebitMemoLine>
                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("DebitMemo", _locDebitMemoInXPO),
                                                                     new BinaryOperator("Select", true),
                                                                     new BinaryOperator("ReturnType", ReturnType.DebitMemo),
                                                                     new GroupOperator(GroupOperatorType.Or,
                                                                     new BinaryOperator("Status", Status.Progress),
                                                                     new BinaryOperator("Status", Status.Posted)
                                                                     )));
                    if (_locDebitMemoLines != null && _locDebitMemoLines.Count() > 0)
                    {
                        foreach (DebitMemoLine _locDebitMemoLine in _locDebitMemoLines)
                        {
                            XPQuery<SalesReturnMonitoring> _salesReturnMonitoringsQuery = new XPQuery<SalesReturnMonitoring>(_currSession);

                            var _salesReturnMonitorings = from srm in _salesReturnMonitoringsQuery
                                                          where (srm.DebitMemo == _locDebitMemoInXPO
                                                          && srm.DebitMemoLine == _locDebitMemoLine
                                                          )
                                                          group srm by srm.SalesReturn into g
                                                          select new { SalesReturn = g.Key };

                            if (_salesReturnMonitorings != null && _salesReturnMonitorings.Count() > 0)
                            {
                                foreach (var _salesReturnMonitoring in _salesReturnMonitorings)
                                {
                                    if (_salesReturnMonitoring.SalesReturn != null)
                                    {
                                        XPCollection<SalesReturnMonitoring> _locSalesReturnMonitorings = new XPCollection<SalesReturnMonitoring>
                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                           new BinaryOperator("DebitMemo", _locDebitMemoInXPO),
                                                                                           new BinaryOperator("DebitMemoLine", _locDebitMemoLine),
                                                                                           new BinaryOperator("SalesReturn", _salesReturnMonitoring.SalesReturn)));

                                        if (_locSalesReturnMonitorings != null && _locSalesReturnMonitorings.Count() > 0)
                                        {
                                            foreach (SalesReturnMonitoring _locSalesReturnMonitoring in _locSalesReturnMonitorings)
                                            {
                                                if (_locSalesReturnMonitoring.SalesReturnLine != null)
                                                {
                                                    //Create purchaseorderline based Item   
                                                    _locTotalUnitAmount = _locSalesReturnMonitoring.TUAmount;

                                                }

                                                #region CreateGoodsReceiveJournal

                                                #region JournalMapByItems
                                                if (_locDebitMemoLine.Item != null)
                                                {
                                                    if (_locDebitMemoLine.Item.ItemAccountGroup != null)
                                                    {
                                                        XPCollection<JournalMap> _locJournalMapItems = new XPCollection<JournalMap>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("ItemAccountGroup", _locDebitMemoLine.Item.ItemAccountGroup)));

                                                        if (_locJournalMapItems != null && _locJournalMapItems.Count() > 0)
                                                        {
                                                            foreach (JournalMap _locJournalMapItem in _locJournalMapItems)
                                                            {
                                                                XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                    new BinaryOperator("JournalMap", _locJournalMapItem)));

                                                                if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                                {
                                                                    foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                                    {
                                                                        AccountMap _locAccountMap = _currSession.FindObject<AccountMap>
                                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                                     new BinaryOperator("Code", _locJournalMapLine.AccountMap.Code),
                                                                                                     new BinaryOperator("PostingType", PostingType.Sales),
                                                                                                     new BinaryOperator("OrderType", _locSalesReturnMonitoring.OrderType),
                                                                                                     new BinaryOperator("PostingMethod", PostingMethod.Receipt),
                                                                                                     new BinaryOperator("PostingMethodType", PostingMethodType.Return)));

                                                                        if (_locAccountMap != null)
                                                                        {
                                                                            XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                                new BinaryOperator("AccountMap", _locAccountMap),
                                                                                                                                new BinaryOperator("Active", true)));

                                                                            if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                            {
                                                                                double _locTotalAmountDebit = 0;
                                                                                double _locTotalAmountCredit = 0;
                                                                                double _locTotalBalance = 0;

                                                                                foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                                {
                                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                                    {
                                                                                        _locTotalAmountCredit = _locTotalUnitAmount;
                                                                                    }
                                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                                    {
                                                                                        _locTotalAmountDebit = _locTotalUnitAmount;
                                                                                    }

                                                                                    GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                                    {
                                                                                        PostingDate = now,
                                                                                        PostingType = PostingType.Sales,
                                                                                        OrderType = _locSalesReturnMonitoring.OrderType,
                                                                                        PostingMethod = PostingMethod.Receipt,
                                                                                        PostingMethodType = PostingMethodType.Return,
                                                                                        Account = _locAccountMapLine.Account,
                                                                                        Debit = _locTotalAmountDebit,
                                                                                        Credit = _locTotalAmountCredit,
                                                                                        SalesReturn = _locSalesReturnMonitoring.SalesReturn,
                                                                                        SalesReturnLine = _locSalesReturnMonitoring.SalesReturnLine,
                                                                                        DebitMemo = _locDebitMemoInXPO,
                                                                                        DebitMemoLine = _locDebitMemoLine,
                                                                                        Company = _locDebitMemoInXPO.Company,
                                                                                    };

                                                                                    _saveGeneralJournal.Save();
                                                                                    _saveGeneralJournal.Session.CommitTransaction();

                                                                                    #region COA
                                                                                    if (_locAccountMapLine.Account.Code != null)
                                                                                    {
                                                                                        ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                                 new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                                                                                        if (_locCOA != null)
                                                                                        {
                                                                                            if (_locCOA.BalanceType == BalanceType.Change)
                                                                                            {
                                                                                                if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                                                {
                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                                                    }
                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                                                    }
                                                                                                }
                                                                                                if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                                                {
                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                                                    }
                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                                                    }
                                                                                                }
                                                                                            }

                                                                                            _locCOA.Balance = _locTotalBalance;
                                                                                            _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                                            _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                                            _locCOA.Save();
                                                                                            _locCOA.Session.CommitTransaction();
                                                                                        }
                                                                                    }
                                                                                    #endregion COA
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }

                                                #endregion JournalMapByItems

                                                #region JournalMapByBusinessPartner
                                                if (_locDebitMemoInXPO.SalesToCustomer != null)
                                                {
                                                    if (_locDebitMemoInXPO.SalesToCustomer.BusinessPartnerAccountGroup != null)
                                                    {
                                                        XPCollection<JournalMap> _locJournalMapBusinessPartners = new XPCollection<JournalMap>
                                                                                                                  (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                   new BinaryOperator("BusinessPartnerAccountGroup", _locDebitMemoInXPO.SalesToCustomer.BusinessPartnerAccountGroup)));

                                                        if (_locJournalMapBusinessPartners != null && _locJournalMapBusinessPartners.Count() > 0)
                                                        {
                                                            foreach (JournalMap _locJournalMapBusinessPartner in _locJournalMapBusinessPartners)
                                                            {
                                                                XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                    new BinaryOperator("JournalMap", _locJournalMapBusinessPartner)));

                                                                if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                                {
                                                                    foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                                    {
                                                                        AccountMap _locAccountMap = _currSession.FindObject<AccountMap>
                                                                                                     (new GroupOperator(GroupOperatorType.And,
                                                                                                      new BinaryOperator("Code", _locJournalMapLine.AccountMap.Code),
                                                                                                      new BinaryOperator("PostingType", PostingType.Sales),
                                                                                                      new BinaryOperator("OrderType", _locSalesReturnMonitoring.OrderType),
                                                                                                      new BinaryOperator("PostingMethod", PostingMethod.Receipt),
                                                                                                      new BinaryOperator("PostingMethodType", PostingMethodType.Return)));

                                                                        if (_locAccountMap != null)
                                                                        {
                                                                            XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                             new BinaryOperator("AccountMap", _locAccountMap),
                                                                                                             new BinaryOperator("Active", true)));

                                                                            if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                            {
                                                                                double _locTotalAmountDebit = 0;
                                                                                double _locTotalAmountCredit = 0;
                                                                                double _locTotalBalance = 0;

                                                                                foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                                {
                                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                                    {
                                                                                        _locTotalAmountCredit = _locTotalUnitAmount;
                                                                                    }
                                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                                    {
                                                                                        _locTotalAmountDebit = _locTotalUnitAmount;
                                                                                    }

                                                                                    GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                                    {
                                                                                        PostingDate = now,
                                                                                        PostingType = PostingType.Sales,
                                                                                        OrderType = _locSalesReturnMonitoring.OrderType,
                                                                                        PostingMethod = PostingMethod.Receipt,
                                                                                        PostingMethodType = PostingMethodType.Return,
                                                                                        Account = _locAccountMapLine.Account,
                                                                                        Debit = _locTotalAmountDebit,
                                                                                        Credit = _locTotalAmountCredit,
                                                                                        SalesReturn = _locSalesReturnMonitoring.SalesReturn,
                                                                                        SalesReturnLine = _locSalesReturnMonitoring.SalesReturnLine,
                                                                                        DebitMemo = _locDebitMemoInXPO,
                                                                                        DebitMemoLine = _locDebitMemoLine,
                                                                                        Company = _locDebitMemoInXPO.Company,
                                                                                    };

                                                                                    _saveGeneralJournal.Save();
                                                                                    _saveGeneralJournal.Session.CommitTransaction();

                                                                                    #region COA
                                                                                    if (_locAccountMapLine.Account.Code != null)
                                                                                    {
                                                                                        ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                                 new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                                                                                        if (_locCOA != null)
                                                                                        {
                                                                                            if (_locCOA.BalanceType == BalanceType.Change)
                                                                                            {
                                                                                                if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                                                {
                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                                                    }
                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                                                    }
                                                                                                }
                                                                                                if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                                                {
                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                                                    }
                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                                                    }
                                                                                                }
                                                                                            }

                                                                                            _locCOA.Balance = _locTotalBalance;
                                                                                            _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                                            _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                                            _locCOA.Save();
                                                                                            _locCOA.Session.CommitTransaction();
                                                                                        }
                                                                                    }
                                                                                    #endregion COA
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                #endregion JournalMapByBusinessPartner

                                                #endregion CreateGoodsIssueJournal
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = DebitMemo ", ex.ToString());
            }
        }

        private void SetReturnJournal(Session _currSession, DebitMemo _locDebitMemoInXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                double _locTotalUnitAmount = 0;

                if (_locDebitMemoInXPO != null)
                {
                    XPCollection<DebitMemoLine> _locDebitMemoLines = new XPCollection<DebitMemoLine>
                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("DebitMemo", _locDebitMemoInXPO),
                                                                     new BinaryOperator("Select", true),
                                                                     new BinaryOperator("ReturnType", ReturnType.DebitMemo),
                                                                     new GroupOperator(GroupOperatorType.Or,
                                                                     new BinaryOperator("Status", Status.Progress),
                                                                     new BinaryOperator("Status", Status.Posted)
                                                                     )));
                    if (_locDebitMemoLines != null && _locDebitMemoLines.Count() > 0)
                    {
                        foreach (DebitMemoLine _locDebitMemoLine in _locDebitMemoLines)
                        {
                            XPQuery<SalesReturnMonitoring> _salesReturnMonitoringsQuery = new XPQuery<SalesReturnMonitoring>(_currSession);

                            var _salesReturnMonitorings = from srm in _salesReturnMonitoringsQuery
                                                          where (srm.DebitMemo == _locDebitMemoInXPO
                                                          && srm.DebitMemoLine == _locDebitMemoLine
                                                          )
                                                          group srm by srm.SalesReturn into g
                                                          select new { SalesReturn = g.Key };

                            if (_salesReturnMonitorings != null && _salesReturnMonitorings.Count() > 0)
                            {
                                foreach (var _salesReturnMonitoring in _salesReturnMonitorings)
                                {
                                    if (_salesReturnMonitoring.SalesReturn != null)
                                    {
                                        XPCollection<SalesReturnMonitoring> _locSalesReturnMonitorings = new XPCollection<SalesReturnMonitoring>
                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                           new BinaryOperator("DebitMemo", _locDebitMemoInXPO),
                                                                                           new BinaryOperator("DebitMemoLine", _locDebitMemoLine),
                                                                                           new BinaryOperator("SalesReturn", _salesReturnMonitoring.SalesReturn)));

                                        if (_locSalesReturnMonitorings != null && _locSalesReturnMonitorings.Count() > 0)
                                        {
                                            foreach (SalesReturnMonitoring _locSalesReturnMonitoring in _locSalesReturnMonitorings)
                                            {
                                                if (_locSalesReturnMonitoring.SalesReturnLine != null)
                                                {
                                                    //Create purchaseorderline based Item   
                                                    _locTotalUnitAmount = _locSalesReturnMonitoring.TUAmount;

                                                }

                                                #region CreateGoodsReceiveJournal

                                                #region JournalMapByItems
                                                if (_locDebitMemoLine.Item != null)
                                                {
                                                    if (_locDebitMemoLine.Item.ItemAccountGroup != null)
                                                    {
                                                        XPCollection<JournalMap> _locJournalMapItems = new XPCollection<JournalMap>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("ItemAccountGroup", _locDebitMemoLine.Item.ItemAccountGroup)));

                                                        if (_locJournalMapItems != null && _locJournalMapItems.Count() > 0)
                                                        {
                                                            foreach (JournalMap _locJournalMapItem in _locJournalMapItems)
                                                            {
                                                                XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                    new BinaryOperator("JournalMap", _locJournalMapItem)));

                                                                if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                                {
                                                                    foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                                    {
                                                                        AccountMap _locAccountMap = _currSession.FindObject<AccountMap>
                                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                                     new BinaryOperator("Code", _locJournalMapLine.AccountMap.Code),
                                                                                                     new BinaryOperator("PostingType", PostingType.Sales),
                                                                                                     new BinaryOperator("OrderType", _locSalesReturnMonitoring.OrderType),
                                                                                                     new BinaryOperator("PostingMethod", PostingMethod.Return),
                                                                                                     new BinaryOperator("PostingMethodType", PostingMethodType.Normal)));

                                                                        if (_locAccountMap != null)
                                                                        {
                                                                            XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                                new BinaryOperator("AccountMap", _locAccountMap),
                                                                                                                                new BinaryOperator("Active", true)));

                                                                            if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                            {
                                                                                double _locTotalAmountDebit = 0;
                                                                                double _locTotalAmountCredit = 0;
                                                                                double _locTotalBalance = 0;

                                                                                foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                                {
                                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                                    {
                                                                                        _locTotalAmountDebit = _locTotalUnitAmount;
                                                                                    }
                                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                                    {
                                                                                        _locTotalAmountCredit = _locTotalUnitAmount;
                                                                                    }

                                                                                    GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                                    {
                                                                                        PostingDate = now,
                                                                                        PostingType = PostingType.Sales,
                                                                                        OrderType = _locSalesReturnMonitoring.OrderType,
                                                                                        PostingMethod = PostingMethod.Return,
                                                                                        PostingMethodType = PostingMethodType.Normal,
                                                                                        Account = _locAccountMapLine.Account,
                                                                                        Debit = _locTotalAmountDebit,
                                                                                        Credit = _locTotalAmountCredit,
                                                                                        SalesReturn = _locSalesReturnMonitoring.SalesReturn,
                                                                                        SalesReturnLine = _locSalesReturnMonitoring.SalesReturnLine,
                                                                                        DebitMemo = _locDebitMemoInXPO,
                                                                                        DebitMemoLine = _locDebitMemoLine,
                                                                                        Company = _locDebitMemoInXPO.Company,
                                                                                    };

                                                                                    _saveGeneralJournal.Save();
                                                                                    _saveGeneralJournal.Session.CommitTransaction();

                                                                                    #region COA
                                                                                    if (_locAccountMapLine.Account.Code != null)
                                                                                    {
                                                                                        ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                                 new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                                                                                        if (_locCOA != null)
                                                                                        {
                                                                                            if (_locCOA.BalanceType == BalanceType.Change)
                                                                                            {
                                                                                                if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                                                {
                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                                                    }
                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                                                    }
                                                                                                }
                                                                                                if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                                                {
                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                                                    }
                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                                                    }
                                                                                                }
                                                                                            }

                                                                                            _locCOA.Balance = _locTotalBalance;
                                                                                            _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                                            _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                                            _locCOA.Save();
                                                                                            _locCOA.Session.CommitTransaction();
                                                                                        }
                                                                                    }
                                                                                    #endregion COA
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }

                                                #endregion JournalMapByItems

                                                #region JournalMapByBusinessPartner
                                                if (_locDebitMemoInXPO.SalesToCustomer != null)
                                                {
                                                    if (_locDebitMemoInXPO.SalesToCustomer.BusinessPartnerAccountGroup != null)
                                                    {
                                                        XPCollection<JournalMap> _locJournalMapBusinessPartners = new XPCollection<JournalMap>
                                                                                                                  (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                   new BinaryOperator("BusinessPartnerAccountGroup", _locDebitMemoInXPO.SalesToCustomer.BusinessPartnerAccountGroup)));

                                                        if (_locJournalMapBusinessPartners != null && _locJournalMapBusinessPartners.Count() > 0)
                                                        {
                                                            foreach (JournalMap _locJournalMapBusinessPartner in _locJournalMapBusinessPartners)
                                                            {
                                                                XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                    new BinaryOperator("JournalMap", _locJournalMapBusinessPartner)));

                                                                if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                                {
                                                                    foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                                    {
                                                                        AccountMap _locAccountMap = _currSession.FindObject<AccountMap>
                                                                                                     (new GroupOperator(GroupOperatorType.And,
                                                                                                      new BinaryOperator("Code", _locJournalMapLine.AccountMap.Code),
                                                                                                      new BinaryOperator("PostingType", PostingType.Sales),
                                                                                                      new BinaryOperator("OrderType", _locSalesReturnMonitoring.OrderType),
                                                                                                      new BinaryOperator("PostingMethod", PostingMethod.Return),
                                                                                                      new BinaryOperator("PostingMethodType", PostingMethodType.Normal)));

                                                                        if (_locAccountMap != null)
                                                                        {
                                                                            XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                             new BinaryOperator("AccountMap", _locAccountMap),
                                                                                                             new BinaryOperator("Active", true)));

                                                                            if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                            {
                                                                                double _locTotalAmountDebit = 0;
                                                                                double _locTotalAmountCredit = 0;
                                                                                double _locTotalBalance = 0;

                                                                                foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                                {
                                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                                    {
                                                                                        _locTotalAmountDebit = _locTotalUnitAmount;
                                                                                    }
                                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                                    {
                                                                                        _locTotalAmountCredit = _locTotalUnitAmount;
                                                                                    }

                                                                                    GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                                    {
                                                                                        PostingDate = now,
                                                                                        PostingType = PostingType.Sales,
                                                                                        OrderType = _locSalesReturnMonitoring.OrderType,
                                                                                        PostingMethod = PostingMethod.Return,
                                                                                        PostingMethodType = PostingMethodType.Normal,
                                                                                        Account = _locAccountMapLine.Account,
                                                                                        Debit = _locTotalAmountDebit,
                                                                                        Credit = _locTotalAmountCredit,
                                                                                        SalesReturn = _locSalesReturnMonitoring.SalesReturn,
                                                                                        SalesReturnLine = _locSalesReturnMonitoring.SalesReturnLine,
                                                                                        DebitMemo = _locDebitMemoInXPO,
                                                                                        DebitMemoLine = _locDebitMemoLine,
                                                                                        Company = _locDebitMemoInXPO.Company,
                                                                                    };

                                                                                    _saveGeneralJournal.Save();
                                                                                    _saveGeneralJournal.Session.CommitTransaction();

                                                                                    #region COA
                                                                                    if (_locAccountMapLine.Account.Code != null)
                                                                                    {
                                                                                        ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                                 new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                                                                                        if (_locCOA != null)
                                                                                        {
                                                                                            if (_locCOA.BalanceType == BalanceType.Change)
                                                                                            {
                                                                                                if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                                                {
                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                                                    }
                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                                                    }
                                                                                                }
                                                                                                if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                                                {
                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                                                    }
                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                                                    }
                                                                                                }
                                                                                            }

                                                                                            _locCOA.Balance = _locTotalBalance;
                                                                                            _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                                            _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                                            _locCOA.Save();
                                                                                            _locCOA.Session.CommitTransaction();
                                                                                        }
                                                                                    }
                                                                                    #endregion COA
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                #endregion JournalMapByBusinessPartner

                                                #endregion CreateGoodsIssueJournal
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = DebitMemo ", ex.ToString());
            }
        }

        private void SetInvoiceARReturnJournal(Session _currSession, DebitMemo _locDebitMemoXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                double _locTotalUnitPrice = 0;

                #region CreateInvoiceAPReturnJournal

                if (_locDebitMemoXPO != null)
                {
                    XPQuery<DebitMemoLine> _debitMemoLinesQuery = new XPQuery<DebitMemoLine>(_currSession);

                    var _debitMemoLines = from cml in _debitMemoLinesQuery
                                          where (cml.DebitMemo == _locDebitMemoXPO && (cml.Status == Status.Progress || cml.Status == Status.Posted))
                                          group cml by cml.Item.ItemAccountGroup into g
                                          select new { ItemAccountGroup = g.Key };

                    if (_debitMemoLines != null && _debitMemoLines.Count() > 0)
                    {
                        foreach (var _debitMemoLine in _debitMemoLines)
                        {
                            XPCollection<DebitMemoLine> _locDeMemLines = new XPCollection<DebitMemoLine>(_currSession,
                                                                         new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("DebitMemo", _locDebitMemoXPO),
                                                                         new BinaryOperator("Item.ItemAccountGroup", _debitMemoLine.ItemAccountGroup)));

                            if (_locDeMemLines != null && _locDeMemLines.Count() > 0)
                            {
                                #region MenghitungTotalUnitPriceBasedOnItemAccountGroup
                                foreach (DebitMemoLine _locDeMemLine in _locDeMemLines)
                                {
                                    if (_locDeMemLine.Status == Status.Progress || _locDeMemLine.Status == Status.Posted)
                                    {
                                        _locTotalUnitPrice = _locTotalUnitPrice + _locDeMemLine.TUAmount;
                                    }
                                }
                                #endregion MenghitungTotalUnitPriceBasedOnItemAccountGroup

                                #region CreateInvoiceAPReturnJournalByItems

                                XPCollection<JournalMap> _locJournalMapByItems = new XPCollection<JournalMap>
                                                                                 (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                  new BinaryOperator("ItemAccountGroup", _debitMemoLine.ItemAccountGroup)));

                                if (_locJournalMapByItems != null && _locJournalMapByItems.Count() > 0)
                                {
                                    foreach (JournalMap _locJournalMapByItem in _locJournalMapByItems)
                                    {
                                        XPCollection<JournalMapLine> _locJournalMapLineByItems = new XPCollection<JournalMapLine>
                                                                                                 (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                  new BinaryOperator("JournalMap", _locJournalMapByItem)));

                                        if (_locJournalMapLineByItems != null && _locJournalMapLineByItems.Count() > 0)
                                        {
                                            foreach (JournalMapLine _locJournalMapLineByItem in _locJournalMapLineByItems)
                                            {
                                                AccountMap _locAccountMapByItem = _currSession.FindObject<AccountMap>
                                                                                  (new GroupOperator(GroupOperatorType.And,
                                                                                   new BinaryOperator("Code", _locJournalMapLineByItem.AccountMap.Code),
                                                                                   new BinaryOperator("PostingType", PostingType.Sales),
                                                                                   new BinaryOperator("OrderType", OrderType.Item),
                                                                                   new BinaryOperator("PostingMethod", PostingMethod.DebitMemo),
                                                                                   new BinaryOperator("PostingMethodType", PostingMethodType.Normal)));

                                                if (_locAccountMapByItem != null)
                                                {
                                                    XPCollection<AccountMapLine> _locAccountMapLineByItems = new XPCollection<AccountMapLine>
                                                                                                             (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                              new BinaryOperator("AccountMap", _locAccountMapByItem),
                                                                                                              new BinaryOperator("Active", true)));

                                                    if (_locAccountMapLineByItems != null && _locAccountMapLineByItems.Count() > 0)
                                                    {
                                                        double _locTotalAmountDebit = 0;
                                                        double _locTotalAmountCredit = 0;
                                                        double _locTotalBalance = 0;

                                                        foreach (AccountMapLine _locAccountMapLineByItem in _locAccountMapLineByItems)
                                                        {
                                                            if (_locAccountMapLineByItem.AccountCharge == AccountCharge.Debit)
                                                            {
                                                                _locTotalAmountDebit = _locTotalUnitPrice;
                                                            }
                                                            if (_locAccountMapLineByItem.AccountCharge == AccountCharge.Credit)
                                                            {
                                                                _locTotalAmountCredit = _locTotalUnitPrice;
                                                            }

                                                            GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                            {
                                                                PostingDate = now,
                                                                PostingType = PostingType.Sales,
                                                                PostingMethod = PostingMethod.InvoiceAP,
                                                                Account = _locAccountMapLineByItem.Account,
                                                                Debit = _locTotalAmountDebit,
                                                                Credit = _locTotalAmountCredit,
                                                                SalesOrder = _locDebitMemoXPO.SalesOrder,
                                                                DebitMemo = _locDebitMemoXPO,
                                                            };
                                                            _saveGeneralJournal.Save();
                                                            _saveGeneralJournal.Session.CommitTransaction();

                                                            if (_locAccountMapLineByItem.Account.Code != null)
                                                            {
                                                                ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("Code", _locAccountMapLineByItem.Account.Code)));
                                                                if (_locCOA != null)
                                                                {
                                                                    if (_locCOA.BalanceType == BalanceType.Change)
                                                                    {
                                                                        if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                        {
                                                                            if (_locTotalAmountDebit > 0)
                                                                            {
                                                                                _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                            }
                                                                            if (_locTotalAmountCredit > 0)
                                                                            {
                                                                                _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                            }
                                                                        }
                                                                        if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                        {
                                                                            if (_locTotalAmountDebit > 0)
                                                                            {
                                                                                _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                            }
                                                                            if (_locTotalAmountCredit > 0)
                                                                            {
                                                                                _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                            }
                                                                        }
                                                                    }

                                                                    _locCOA.Balance = _locTotalBalance;
                                                                    _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                    _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                    _locCOA.Save();
                                                                    _locCOA.Session.CommitTransaction();
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                #endregion CreateInvoiceAPReturnJournalByItems

                                #region CreateInvoiceAPJournalByBusinessPartner
                                if (_locDebitMemoXPO.SalesToCustomer != null)
                                {
                                    if (_locDebitMemoXPO.SalesToCustomer.BusinessPartnerAccountGroup != null)
                                    {
                                        XPCollection<JournalMap> _locJournalMapByBusinessPartners = new XPCollection<JournalMap>
                                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                     new BinaryOperator("BusinessPartnerAccountGroup", _locDebitMemoXPO.SalesToCustomer.BusinessPartnerAccountGroup)));

                                        if (_locJournalMapByBusinessPartners != null && _locJournalMapByBusinessPartners.Count() > 0)
                                        {
                                            foreach (JournalMap _locJournalMapByBusinessPartner in _locJournalMapByBusinessPartners)
                                            {
                                                XPCollection<JournalMapLine> _locJournalMapLineByBusinessPartners = new XPCollection<JournalMapLine>
                                                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                     new BinaryOperator("JournalMap", _locJournalMapByBusinessPartner)));

                                                if (_locJournalMapLineByBusinessPartners != null && _locJournalMapLineByBusinessPartners.Count() > 0)
                                                {
                                                    foreach (JournalMapLine _locJournalMapLineByBusinessPartner in _locJournalMapLineByBusinessPartners)
                                                    {
                                                        AccountMap _locAccountMapByBusinessPartner = _currSession.FindObject<AccountMap>
                                                                                                     (new GroupOperator(GroupOperatorType.And,
                                                                                                      new BinaryOperator("Code", _locJournalMapLineByBusinessPartner.AccountMap.Code),
                                                                                                      new BinaryOperator("PostingType", PostingType.Sales),
                                                                                                      new BinaryOperator("OrderType", OrderType.Item),
                                                                                                      new BinaryOperator("PostingMethod", PostingMethod.InvoiceAP),
                                                                                                      new BinaryOperator("PostingMethodType", PostingMethodType.Normal)));

                                                        if (_locAccountMapByBusinessPartner != null)
                                                        {
                                                            XPCollection<AccountMapLine> _locAccountMapLineByBusinessPartners = new XPCollection<AccountMapLine>
                                                                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                                 new BinaryOperator("AccountMap", _locAccountMapByBusinessPartner),
                                                                                                                                 new BinaryOperator("Active", true)));

                                                            if (_locAccountMapLineByBusinessPartners != null && _locAccountMapLineByBusinessPartners.Count() > 0)
                                                            {
                                                                double _locTotalAmountDebit = 0;
                                                                double _locTotalAmountCredit = 0;
                                                                double _locTotalBalance = 0;

                                                                foreach (AccountMapLine _locAccountMapLineByBusinessPartner in _locAccountMapLineByBusinessPartners)
                                                                {
                                                                    if (_locAccountMapLineByBusinessPartner.AccountCharge == AccountCharge.Debit)
                                                                    {
                                                                        _locTotalAmountDebit = _locTotalUnitPrice;
                                                                    }
                                                                    if (_locAccountMapLineByBusinessPartner.AccountCharge == AccountCharge.Credit)
                                                                    {
                                                                        _locTotalAmountCredit = _locTotalUnitPrice;
                                                                    }

                                                                    GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                    {
                                                                        PostingDate = now,
                                                                        PostingType = PostingType.Purchase,
                                                                        PostingMethod = PostingMethod.InvoiceAP,
                                                                        Account = _locAccountMapLineByBusinessPartner.Account,
                                                                        Debit = _locTotalAmountDebit,
                                                                        Credit = _locTotalAmountCredit,
                                                                        SalesOrder = _locDebitMemoXPO.SalesOrder,
                                                                        DebitMemo = _locDebitMemoXPO,
                                                                    };
                                                                    _saveGeneralJournal.Save();
                                                                    _saveGeneralJournal.Session.CommitTransaction();

                                                                    if (_locAccountMapLineByBusinessPartner.Account.Code != null)
                                                                    {
                                                                        ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("Code", _locAccountMapLineByBusinessPartner.Account.Code)));
                                                                        if (_locCOA != null)
                                                                        {
                                                                            if (_locCOA.BalanceType == BalanceType.Change)
                                                                            {
                                                                                if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                                {
                                                                                    if (_locTotalAmountDebit > 0)
                                                                                    {
                                                                                        _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                                    }
                                                                                    if (_locTotalAmountCredit > 0)
                                                                                    {
                                                                                        _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                                    }
                                                                                }
                                                                                if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                                {
                                                                                    if (_locTotalAmountDebit > 0)
                                                                                    {
                                                                                        _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                                    }
                                                                                    if (_locTotalAmountCredit > 0)
                                                                                    {
                                                                                        _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                                    }
                                                                                }
                                                                            }

                                                                            _locCOA.Balance = _locTotalBalance;
                                                                            _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                            _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                            _locCOA.Save();
                                                                            _locCOA.Session.CommitTransaction();
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                #endregion CreateInvoiceAPJournalByBusinessPartner
                            }
                        }
                    }
                }

                #endregion CreateInvoiceAPReturnJournal
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = CreditMemo ", ex.ToString());
            }
        }

        private void SetSalesReturnMonitoring(Session _currSession, DebitMemo _locDebitMemoXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locDebitMemoXPO != null)
                {
                    XPCollection<DebitMemoLine> _locDebitMemoLines = new XPCollection<DebitMemoLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("DebitMemo", _locDebitMemoXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Progress),
                                                            new BinaryOperator("Status", Status.Posted))));

                    if (_locDebitMemoLines != null && _locDebitMemoLines.Count() > 0)
                    {
                        foreach (DebitMemoLine _locDebitMemoLine in _locDebitMemoLines)
                        {
                            DebitMemoMonitoring _saveDataDebitMemoMonitoring = new DebitMemoMonitoring(_currSession)
                            {
                                DebitMemo = _locDebitMemoXPO,
                                DebitMemoLine = _locDebitMemoLine,
                                OrderType = _locDebitMemoLine.OrderType,
                                Item = _locDebitMemoLine.Item,
                                DQty = _locDebitMemoLine.DQty,
                                DUOM = _locDebitMemoLine.DUOM,
                                Qty = _locDebitMemoLine.Qty,
                                TQty = _locDebitMemoLine.TQty,
                                UOM = _locDebitMemoLine.UOM,
                                Currency = _locDebitMemoLine.Currency,
                                PriceGroup = _locDebitMemoLine.PriceGroup,
                                Price = _locDebitMemoLine.Price,
                                PriceLine = _locDebitMemoLine.PriceLine,
                                UAmount = _locDebitMemoLine.UAmount,
                                TUAmount = _locDebitMemoLine.TUAmount,
                                MultiTax = _locDebitMemoLine.MultiTax,
                                Tax = _locDebitMemoLine.Tax,
                                TxValue = _locDebitMemoLine.TxValue,
                                TxAmount = _locDebitMemoLine.TxAmount,
                                MultiDiscount = _locDebitMemoLine.MultiDiscount,
                                Discount = _locDebitMemoLine.Discount,
                                Disc = _locDebitMemoLine.Disc,
                                DiscAmount = _locDebitMemoLine.DiscAmount,
                                TAmount = _locDebitMemoLine.TAmount,
                                ReturnType = _locDebitMemoLine.ReturnType,
                            };
                            _saveDataDebitMemoMonitoring.Save();
                            _saveDataDebitMemoMonitoring.Session.CommitTransaction();

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = DebitMemo " + ex.ToString());
            }
        }

        private void SetRemainQty(Session _currSession, DebitMemo _locDebitMemoXPO)
        {
            try
            {
                if (_locDebitMemoXPO != null)
                {
                    XPCollection<DebitMemoLine> _locDebitMemoLines = new XPCollection<DebitMemoLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("DebitMemo", _locDebitMemoXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locDebitMemoLines != null && _locDebitMemoLines.Count > 0)
                    {
                        double _locRmDQty = 0;
                        double _locRmQty = 0;
                        double _locInvLineTotal = 0;
                        ItemUnitOfMeasure _locItemUOM = null;

                        foreach (DebitMemoLine _locDebitMemoLine in _locDebitMemoLines)
                        {
                            #region ProcessCount=0
                            if (_locDebitMemoLine.PostedCount == 0)
                            {
                                #region MaxQuantity
                                if (_locDebitMemoLine.MxDQty > 0)
                                {
                                    if (_locDebitMemoLine.DQty > 0 && _locDebitMemoLine.DQty <= _locDebitMemoLine.MxDQty)
                                    {
                                        _locRmDQty = _locDebitMemoLine.MxDQty - _locDebitMemoLine.DQty;
                                    }

                                    if (_locDebitMemoLine.Qty > 0 && _locDebitMemoLine.Qty <= _locDebitMemoLine.MxQty)
                                    {
                                        _locRmQty = _locDebitMemoLine.MxQty - _locDebitMemoLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locDebitMemoLine.Item),
                                                                new BinaryOperator("UOM", _locDebitMemoLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locDebitMemoLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty + _locRmDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }

                                }
                                #endregion MaxQuantity
                                #region NonMaxQuantity
                                else
                                {
                                    if (_locDebitMemoLine.DQty > 0)
                                    {
                                        _locRmDQty = 0;
                                    }

                                    if (_locDebitMemoLine.Qty > 0)
                                    {
                                        _locRmQty = 0;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locDebitMemoLine.Item),
                                                                new BinaryOperator("UOM", _locDebitMemoLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locDebitMemoLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty + _locRmDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion ProcessCount=0

                            #region ProcessCount>0
                            if (_locDebitMemoLine.PostedCount > 0)
                            {
                                if (_locDebitMemoLine.RmDQty > 0)
                                {
                                    _locRmDQty = _locDebitMemoLine.RmDQty - _locDebitMemoLine.DQty;
                                }

                                if (_locDebitMemoLine.RmQty > 0)
                                {
                                    _locRmQty = _locDebitMemoLine.RmQty - _locDebitMemoLine.Qty;
                                }

                                if (_locDebitMemoLine.MxDQty > 0 || _locDebitMemoLine.MxQty > 0)
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locDebitMemoLine.Item),
                                                                new BinaryOperator("UOM", _locDebitMemoLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locDebitMemoLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                }
                                else
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locDebitMemoLine.Item),
                                                                new BinaryOperator("UOM", _locDebitMemoLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locDebitMemoLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                }


                                if (_locItemUOM != null)
                                {
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locRmQty + _locRmDQty;
                                }

                            }
                            #endregion ProcessCount>0

                            _locDebitMemoLine.RmDQty = _locRmDQty;
                            _locDebitMemoLine.RmQty = _locRmQty;
                            _locDebitMemoLine.RmTQty = _locInvLineTotal;
                            _locDebitMemoLine.Save();
                            _locDebitMemoLine.Session.CommitTransaction();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = DebitMemo " + ex.ToString());
            }
        }

        private void SetPostingQty(Session _currSession, DebitMemo _locDebitMemoXPO)
        {
            try
            {
                if (_locDebitMemoXPO != null)
                {
                    XPCollection<DebitMemoLine> _locDebitMemoLines = new XPCollection<DebitMemoLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("DebitMemo", _locDebitMemoXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locDebitMemoLines != null && _locDebitMemoLines.Count > 0)
                    {
                        double _locPDQty = 0;
                        double _locPQty = 0;
                        double _locInvLineTotal = 0;
                        ItemUnitOfMeasure _locItemUOM = null;

                        foreach (DebitMemoLine _locDebitMemoLine in _locDebitMemoLines)
                        {
                            #region ProcessCount=0
                            if (_locDebitMemoLine.PostedCount == 0)
                            {
                                #region MaxQuantity
                                if (_locDebitMemoLine.MxDQty > 0)
                                {
                                    if (_locDebitMemoLine.DQty > 0 && _locDebitMemoLine.DQty <= _locDebitMemoLine.MxDQty)
                                    {
                                        _locPDQty = _locDebitMemoLine.DQty;
                                    }

                                    if (_locDebitMemoLine.Qty > 0 && _locDebitMemoLine.Qty <= _locDebitMemoLine.MxQty)
                                    {
                                        _locPQty = _locDebitMemoLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locDebitMemoLine.Item),
                                                                new BinaryOperator("UOM", _locDebitMemoLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locDebitMemoLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty + _locPDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }

                                }
                                #endregion MaxQuantity
                                #region NonMaxQuantity
                                else
                                {
                                    if (_locDebitMemoLine.DQty > 0)
                                    {
                                        _locPDQty = _locDebitMemoLine.DQty;
                                    }

                                    if (_locDebitMemoLine.Qty > 0)
                                    {
                                        _locPQty = _locDebitMemoLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locDebitMemoLine.Item),
                                                                new BinaryOperator("UOM", _locDebitMemoLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locDebitMemoLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty + _locPDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion ProcessCount=0

                            #region ProcessCount>0
                            if (_locDebitMemoLine.PostedCount > 0)
                            {
                                if (_locDebitMemoLine.PDQty > 0)
                                {
                                    _locPDQty = _locDebitMemoLine.PDQty + _locDebitMemoLine.DQty;
                                }

                                if (_locDebitMemoLine.PQty > 0)
                                {
                                    _locPQty = _locDebitMemoLine.PQty + _locDebitMemoLine.Qty;
                                }

                                if (_locDebitMemoLine.MxDQty > 0 || _locDebitMemoLine.MxQty > 0)
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                           (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Item", _locDebitMemoLine.Item),
                                                            new BinaryOperator("UOM", _locDebitMemoLine.MxUOM),
                                                            new BinaryOperator("DefaultUOM", _locDebitMemoLine.MxDUOM),
                                                            new BinaryOperator("Active", true)));
                                }
                                else
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                           (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Item", _locDebitMemoLine.Item),
                                                            new BinaryOperator("UOM", _locDebitMemoLine.UOM),
                                                            new BinaryOperator("DefaultUOM", _locDebitMemoLine.DUOM),
                                                            new BinaryOperator("Active", true)));
                                }

                                if (_locItemUOM != null)
                                {
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locPQty + _locPDQty;
                                }

                            }
                            #endregion ProcessCount>0

                            _locDebitMemoLine.PDQty = _locPDQty;
                            _locDebitMemoLine.PDUOM = _locDebitMemoLine.DUOM;
                            _locDebitMemoLine.PQty = _locPQty;
                            _locDebitMemoLine.PUOM = _locDebitMemoLine.UOM;
                            _locDebitMemoLine.PTQty = _locInvLineTotal;
                            _locDebitMemoLine.Save();
                            _locDebitMemoLine.Session.CommitTransaction();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = DebitMemo " + ex.ToString());
            }

        }

        private void SetProcessCount(Session _currSession, DebitMemo _locDebitMemoXPO)
        {
            try
            {
                if (_locDebitMemoXPO != null)
                {
                    XPCollection<DebitMemoLine> _locDebitMemoLines = new XPCollection<DebitMemoLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("DebitMemo", _locDebitMemoXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locDebitMemoLines != null && _locDebitMemoLines.Count > 0)
                    {

                        foreach (DebitMemoLine _locDebitMemoLine in _locDebitMemoLines)
                        {
                            if (_locDebitMemoLine.Status == Status.Progress || _locDebitMemoLine.Status == Status.Posted)
                            {
                                _locDebitMemoLine.PostedCount = _locDebitMemoLine.PostedCount + 1;
                                _locDebitMemoLine.Save();
                                _locDebitMemoLine.Session.CommitTransaction();
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = DebitMemo " + ex.ToString());
            }
        }

        private void SetStatusSalesReturnLine(Session _currSession, DebitMemo _locDebitMemoXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locDebitMemoXPO != null)
                {
                    XPCollection<DebitMemoLine> _locDebitMemoLines = new XPCollection<DebitMemoLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("DebitMemo", _locDebitMemoXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locDebitMemoLines != null && _locDebitMemoLines.Count > 0)
                    {

                        foreach (DebitMemoLine _locDebitMemoLine in _locDebitMemoLines)
                        {
                            if (_locDebitMemoLine.Status == Status.Progress || _locDebitMemoLine.Status == Status.Posted)
                            {
                                if (_locDebitMemoLine.RmDQty == 0 && _locDebitMemoLine.RmQty == 0 && _locDebitMemoLine.RmTQty == 0)
                                {
                                    _locDebitMemoLine.Status = Status.Close;
                                    _locDebitMemoLine.ActivationPosting = true;
                                    _locDebitMemoLine.StatusDate = now;
                                }
                                else
                                {
                                    _locDebitMemoLine.Status = Status.Posted;
                                    _locDebitMemoLine.StatusDate = now;
                                }
                                _locDebitMemoLine.Save();
                                _locDebitMemoLine.Session.CommitTransaction();
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesReturn " + ex.ToString());
            }
        }

        private void SetNormalQuantity(Session _currSession, DebitMemo _locDebitMemoXPO)
        {
            try
            {
                if (_locDebitMemoXPO != null)
                {
                    XPCollection<DebitMemoLine> _locDebitMemoLines = new XPCollection<DebitMemoLine>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("DebitMemo", _locDebitMemoXPO),
                                                                                     new BinaryOperator("Select", true)));

                    if (_locDebitMemoLines != null && _locDebitMemoLines.Count > 0)
                    {
                        foreach (DebitMemoLine _locDebitMemoLine in _locDebitMemoLines)
                        {
                            if (_locDebitMemoLine.Status == Status.Progress || _locDebitMemoLine.Status == Status.Posted || _locDebitMemoLine.Status == Status.Close)
                            {
                                if (_locDebitMemoLine.DQty > 0 || _locDebitMemoLine.Qty > 0)
                                {
                                    _locDebitMemoLine.Select = false;
                                    _locDebitMemoLine.DQty = 0;
                                    _locDebitMemoLine.Qty = 0;
                                    _locDebitMemoLine.Save();
                                    _locDebitMemoLine.Session.CommitTransaction();
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = DebitMemo " + ex.ToString());
            }
        }

        private void SetFinalStatusSalesReturn(Session _currSession, DebitMemo _locDebitMemoXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locStatusCount = 0;
                int _locSalesQuotationLineCount = 0;

                if (_locDebitMemoXPO != null)
                {

                    XPCollection<DebitMemoLine> _locDebitMemoLines = new XPCollection<DebitMemoLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("DebitMemo", _locDebitMemoXPO)));

                    if (_locDebitMemoLines != null && _locDebitMemoLines.Count() > 0)
                    {
                        _locSalesQuotationLineCount = _locDebitMemoLines.Count();

                        foreach (DebitMemoLine _locDebitMemoLine in _locDebitMemoLines)
                        {
                            if (_locDebitMemoLine.Status == Status.Close)
                            {
                                _locStatusCount = _locStatusCount + 1;
                            }
                        }
                    }

                    if (_locStatusCount == _locSalesQuotationLineCount)
                    {
                        _locDebitMemoXPO.ActivationPosting = true;
                        _locDebitMemoXPO.Status = Status.Close;
                        _locDebitMemoXPO.StatusDate = now;
                        _locDebitMemoXPO.Save();
                        _locDebitMemoXPO.Session.CommitTransaction();
                    }
                    else
                    {
                        _locDebitMemoXPO.Status = Status.Posted;
                        _locDebitMemoXPO.StatusDate = now;
                        _locDebitMemoXPO.Save();
                        _locDebitMemoXPO.Session.CommitTransaction();
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesReturn " + ex.ToString());
            }
        }

        #endregion Posting

        #region Approval Process

        private void SetApprovalLine(Session _currentSession, DebitMemo _locDebitMemoXPO, ApprovalLevel _locApprovalLevel)
        {
            try
            {
                DateTime now = DateTime.Now;

                ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("DebitMemo", _locDebitMemoXPO),
                                                                         new BinaryOperator("ApprovalLevel", _locApprovalLevel)));

                if (_locApprovalLineXPO == null)
                {
                    ApprovalLine _saveDataAL2 = new ApprovalLine(_currentSession)
                    {
                        ApprovalDate = now,
                        ApprovalStatus = Status.Approved,
                        ApprovalLevel = _locApprovalLevel,
                        DebitMemo = _locDebitMemoXPO,
                    };
                    _saveDataAL2.Save();
                    _saveDataAL2.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = DebitMemo " + ex.ToString());
            }
        }

        #endregion Approval Process

        #region Email

        private string BackgroundBody(Session _currentSession, DebitMemo _locDebitMemoXPO, MailSetupDetail _locMailSetDetail)
        {
            string body = string.Empty;

            if (_locDebitMemoXPO != null)
            {
                ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                               (new GroupOperator(GroupOperatorType.And,
                                                new BinaryOperator("DebitMemo", _locDebitMemoXPO)));

                string Status = _locApprovalLineXPO.ApprovalStatus.ToString();

                using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Email/EmailTemplate.html")))
                {
                    body = reader.ReadToEnd();
                }
                body = body.Replace("{Code}", _locDebitMemoXPO.Code);

                #region Level
                if (_locDebitMemoXPO.ActiveApproved1 == true)
                {
                    string Active = "Level 1";
                    body = body.Replace("{Level}", Active);
                }
                else if (_locDebitMemoXPO.ActiveApproved2 == true)
                {
                    string Active = "Level 2";
                    body = body.Replace("{Level}", Active);
                }
                else if (_locDebitMemoXPO.ActiveApproved3 == true)
                {
                    string Active = "Level 3";
                    body = body.Replace("{Level}", Active);
                }
                else
                {
                    string Active = "Not Available";
                    body = body.Replace("{Level}", Active);
                }
                #endregion Level

                body = body.Replace("{Status}", Status);
                body = body.Replace("{Message}", _locMailSetDetail.MessageBody);
            }

            return body;
        }

        protected void SendEmail(Session _currentSession, DebitMemo _locDebitMemoXPO, Status _locStatus, ApprovalLine _locApprovalLineXPO2)
        {
            DateTime now = DateTime.Now;
            var User = SecuritySystem.CurrentUserName;
            GlobalFunction _globFunc = new GlobalFunction();
            UserAccess _numLineUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", User));
            string _locMessageBody = null;

            try
            {
                if (_locDebitMemoXPO != null)
                {
                    XPCollection<MailSetupDetail> _locMailSetDetails = new XPCollection<MailSetupDetail>
                                                                       (_currentSession, new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("ObjectList", ObjectList.DebitMemo),
                                                                        new BinaryOperator("UserAccess", _numLineUserAccess),
                                                                        new BinaryOperator("Active", true)));

                    if (_locMailSetDetails != null && _locMailSetDetails.Count > 0)
                    {
                        foreach (MailSetupDetail _locMailSetDetail in _locMailSetDetails)
                        {
                            if (_locMailSetDetail.MessageBody != null)
                            {
                                if (_locApprovalLineXPO2.EndApproval == true)
                                {
                                    _locMessageBody = this.BackgroundBody(_currentSession, _locDebitMemoXPO, _locMailSetDetail);
                                }
                                else
                                {
                                    _locMessageBody = this.BackgroundBody(_currentSession, _locDebitMemoXPO, _locMailSetDetail);
                                }
                            }

                            _globFunc.SetAndSendMail(_locMailSetDetail.SmtpHost, _locMailSetDetail.SmtpPort,
                            _locMailSetDetail.MailFrom, _locMailSetDetail.MailFromPassword, _locMailSetDetail.MailTo, _locMailSetDetail.MailSubject, _locMessageBody);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = DebitMemo " + ex.ToString());
            }
        }

        #endregion Email

        #region Global Method

        private void SuccessMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 4000;
            options.Message = string.Format("{0}", _locMessage);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }


        #endregion Global Method

        
    }
}
