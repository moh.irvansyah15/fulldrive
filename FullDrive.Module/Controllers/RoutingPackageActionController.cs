﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using FullDrive.Module.BusinessObjects;
using DevExpress.ExpressApp.Xpo;
using FullDrive.Module.CustomProcess;
using System.Collections;
using DevExpress.Xpo;

using System.Web;
using System.IO;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class RoutingPackageActionController : ViewController
    {
        public RoutingPackageActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void RoutingPackageSelectAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        RoutingPackage _locRoutingPackageOS = (RoutingPackage)_objectSpace.GetObject(obj);

                        if (_locRoutingPackageOS != null)
                        {
                            if (_locRoutingPackageOS.Code != null)
                            {
                                _currObjectId = _locRoutingPackageOS.Code;

                                XPCollection<RoutingPackage> _locRoutingPackages = new XPCollection<RoutingPackage>
                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Code", _currObjectId),
                                                                            new GroupOperator(GroupOperatorType.Or,
                                                                            new BinaryOperator("Status", Status.Progress),
                                                                            new BinaryOperator("Status", Status.Open)
                                                                            )));

                                if (_locRoutingPackages != null && _locRoutingPackages.Count > 0)
                                {
                                    foreach (RoutingPackage _locRoutingPackage in _locRoutingPackages)
                                    {
                                        _locRoutingPackage.Select = true;
                                        _locRoutingPackage.Save();
                                        _locRoutingPackage.Session.CommitTransaction();
                                    }

                                    SuccessMessageShow("RoutingPackage has been Select");
                                }
                                else
                                {
                                    ErrorMessageShow("Data RoutingPackage Not Available");
                                }
                            }
                        }
                    }
                    if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                    {
                        _objectSpace.CommitChanges();
                        _objectSpace.Refresh();
                    }
                    if (View is ListView)
                    {
                        _objectSpace.CommitChanges();
                        View.ObjectSpace.Refresh();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = RoutingPackage " + ex.ToString());
            }
        }

        private void RoutingPackageUnselectAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        RoutingPackage _locRoutingPackageOS = (RoutingPackage)_objectSpace.GetObject(obj);

                        if (_locRoutingPackageOS != null)
                        {
                            if (_locRoutingPackageOS.Code != null)
                            {
                                _currObjectId = _locRoutingPackageOS.Code;

                                XPCollection<RoutingPackage> _locRoutingPackages = new XPCollection<RoutingPackage>
                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Code", _currObjectId),
                                                                            new GroupOperator(GroupOperatorType.Or,
                                                                            new BinaryOperator("Status", Status.Progress),
                                                                            new BinaryOperator("Status", Status.Open)
                                                                            )));

                                if (_locRoutingPackages != null && _locRoutingPackages.Count > 0)
                                {
                                    foreach (RoutingPackage _locRoutingPackage in _locRoutingPackages)
                                    {
                                        _locRoutingPackage.Select = false;
                                        _locRoutingPackage.Save();
                                        _locRoutingPackage.Session.CommitTransaction();
                                    }

                                    SuccessMessageShow("RoutingPackage has been Unselect");
                                }
                                else
                                {
                                    ErrorMessageShow("Data RoutingPackage Not Available");
                                }
                            }
                        }
                    }
                    if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                    {
                        _objectSpace.CommitChanges();
                        _objectSpace.Refresh();
                    }
                    if (View is ListView)
                    {
                        _objectSpace.CommitChanges();
                        View.ObjectSpace.Refresh();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = RoutingPackage " + ex.ToString());
            }
        }

        private void RoutingPackageGetChargeableAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        RoutingPackage _locRoutingPackageOS = (RoutingPackage)_objectSpace.GetObject(obj);

                        if (_locRoutingPackageOS != null)
                        {
                            if (_locRoutingPackageOS.Status == Status.Progress || _locRoutingPackageOS.Status == Status.Posted)
                            {
                                if (_locRoutingPackageOS.Code != null)
                                {
                                    _currObjectId = _locRoutingPackageOS.Code;

                                    RoutingPackage _locRoutingPackageXPO = _currSession.FindObject<RoutingPackage>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("Code", _currObjectId)));

                                    if (_locRoutingPackageXPO != null)
                                    {
                                        GetChargeableAmount(_currSession, _locRoutingPackageXPO);
                                    }
                                    else
                                    {
                                        ErrorMessageShow("RoutingLine Data Not Available");
                                    }
                                }
                            }
                            else
                            {
                                ErrorMessageShow("RoutingLine Data Not Available");
                            }
                        }
                    }
                }

                //auto refresh
                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = RoutingLine " + ex.ToString());
            }
        }

        //================================================ Code Only =========================================

        #region Get Chargeable

        private void GetChargeableAmount(Session _currSession, RoutingPackage _locRoutingPackage)
        {
            try
            {
                DateTime now = DateTime.Now;
             
                if (_locRoutingPackage != null)
                {
                    if (_locRoutingPackage.Routing != null)
                    {
                        
                        if (_locRoutingPackage.TransactionTerm != TransactionTerm.None)
                        {
                            _locRoutingPackage.Amount = GetSellingRate(_currSession, _locRoutingPackage).Price;
                            _locRoutingPackage.Status = Status.Chargeable;
                            _locRoutingPackage.StatusDate = now;
                            _locRoutingPackage.Save();
                            _locRoutingPackage.Session.CommitTransaction();
                        }
                        
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = RoutingPackage " + ex.ToString());
            }
        }

        private Rate GetSellingRate(Session _currSession, RoutingPackage _locRoutingPackageXPO)
        {
            string _fullNameRoutes = null;
            Rate _currLocRateLine = null;
            SellingRate _currLocSellingRate = null;
            Rate _result = null;
            try
            {
                if(_locRoutingPackageXPO != null)
                {
                    if (_locRoutingPackageXPO.DeliveryType != null)
                    {
                        _fullNameRoutes = GetFullRoute(_currSession, _locRoutingPackageXPO);
                    }


                    if (_locRoutingPackageXPO.PriceType != CustomProcess.PriceType.None)
                    {
                        _currLocSellingRate = _currSession.FindObject<SellingRate>(new GroupOperator(GroupOperatorType.And,
                                                                              new BinaryOperator("Customer", _locRoutingPackageXPO.BillToCustomer)));

                        if (_currLocSellingRate != null)
                        {
                            _currLocRateLine = _currSession.FindObject<Rate>
                                                   (new GroupOperator(GroupOperatorType.And,
                                                   new BinaryOperator("ShipmentType", _locRoutingPackageXPO.ShipmentType),
                                                    new BinaryOperator("PriceType", _locRoutingPackageXPO.PriceType),
                                                    new BinaryOperator("SellingRate", _currLocSellingRate),
                                                    new BinaryOperator("TransportType", _locRoutingPackageXPO.TransportType),
                                                    new BinaryOperator("DeliveryType", _locRoutingPackageXPO.DeliveryType),
                                                    new BinaryOperator("TransactionTerm", _locRoutingPackageXPO.TransactionTerm),
                                                    new BinaryOperator("ContainerType", _locRoutingPackageXPO.ContainerType),
                                                    new BinaryOperator("Quantity", _locRoutingPackageXPO.TQty),
                                                    new BinaryOperator("UOM", _locRoutingPackageXPO.DUOM),
                                                    new BinaryOperator("Active", true),
                                                    new BinaryOperator("FullName", _fullNameRoutes)));

                            if (_currLocRateLine != null)
                            {
                                _result = _currLocRateLine;
                            }
                        }
                    }
                }
                
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = RoutingPackage " + ex.ToString());
            }
            return _result;
        }

        private string GetFullRoute(Session _currSession, RoutingPackage _locRoutingPackageXPO)
        {
            string _locFullString = "";
            string _locCountryFrom = "";
            string _locCityFrom = "";
            string _locAreaFrom = "";
            string _locLocationFrom = "";
            string _locTransportLocationFrom = "";
            string _locCountryTo = "";
            string _locCityTo = "";
            string _locAreaTo = "";
            string _locLocationTo = "";
            string _locTransportLocationTo = "";

            try
            {
                if (_locRoutingPackageXPO != null)
                {
                    if (_locRoutingPackageXPO.CountryFrom != null) { _locCountryFrom = _locRoutingPackageXPO.CountryFrom.Name; }
                    if (_locRoutingPackageXPO.CityFrom != null) { _locCityFrom = _locRoutingPackageXPO.CityFrom.Name; }
                    if (_locRoutingPackageXPO.AreaFrom != null) { _locAreaFrom = _locRoutingPackageXPO.AreaFrom.Name; }
                    if (_locRoutingPackageXPO.LocationFrom != null) { _locLocationFrom = _locRoutingPackageXPO.LocationFrom.Name; }
                    if (_locRoutingPackageXPO.TransportLocationFrom != null) { _locTransportLocationFrom = _locRoutingPackageXPO.TransportLocationFrom.Name; }
                    if (_locRoutingPackageXPO.CountryTo != null) { _locCountryTo = _locRoutingPackageXPO.CountryTo.Name; }
                    if (_locRoutingPackageXPO.CityTo != null) { _locCityTo = _locRoutingPackageXPO.CityTo.Name; }
                    if (_locRoutingPackageXPO.AreaTo != null) { _locAreaTo = _locRoutingPackageXPO.AreaTo.Name; }
                    if (_locRoutingPackageXPO.LocationTo != null) { _locLocationTo = _locRoutingPackageXPO.LocationTo.Name; }
                    if (_locRoutingPackageXPO.TransportLocationTo != null) { _locTransportLocationTo = _locRoutingPackageXPO.TransportLocationTo.Name; }

                }

                _locFullString = String.Format("{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}", _locCountryFrom, _locCityFrom, _locAreaFrom, _locLocationFrom, _locTransportLocationFrom,
                                 _locCountryTo, _locCityTo, _locAreaTo, _locLocationTo, _locTransportLocationTo).Trim().Replace(" ", "").ToLower();
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = RoutingPackage " + ex.ToString());
            }
            return _locFullString;
        }

        #endregion Get Chargeable

        #region Global Method

        private void SuccessMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 4000;
            options.Message = string.Format("{0}", _locMessage);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }


        #endregion Global Method

        
    }
}
