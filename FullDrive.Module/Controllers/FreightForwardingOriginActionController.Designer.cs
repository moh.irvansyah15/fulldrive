﻿namespace FullDrive.Module.Controllers
{
    partial class FreightForwardingOriginActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.FreightForwardingOriginProgressAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.FreightForwardingOriginApprovalAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.FreightForwardinOriginListviewFilterSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.FreightForwardingOriginListviewFilterApprovalSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.FreightForwardinOriginGetSBMAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.FreightForwardingOriginPostingPackageAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // FreightForwardingOriginProgressAction
            // 
            this.FreightForwardingOriginProgressAction.Caption = "Progress";
            this.FreightForwardingOriginProgressAction.ConfirmationMessage = null;
            this.FreightForwardingOriginProgressAction.Id = "FreightForwardingOriginProgressActionId";
            this.FreightForwardingOriginProgressAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.FreightForwardingOrigin);
            this.FreightForwardingOriginProgressAction.ToolTip = null;
            this.FreightForwardingOriginProgressAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.FreightForwardingOriginProgressAction_Execute);
            // 
            // FreightForwardingOriginApprovalAction
            // 
            this.FreightForwardingOriginApprovalAction.Caption = "Approval";
            this.FreightForwardingOriginApprovalAction.ConfirmationMessage = null;
            this.FreightForwardingOriginApprovalAction.Id = "FreightForwardingOriginApprovalActionId";
            this.FreightForwardingOriginApprovalAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.FreightForwardingOriginApprovalAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.FreightForwardingOrigin);
            this.FreightForwardingOriginApprovalAction.ToolTip = null;
            this.FreightForwardingOriginApprovalAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.FreightForwardingOriginApprovalAction_Execute);
            // 
            // FreightForwardinOriginListviewFilterSelectionAction
            // 
            this.FreightForwardinOriginListviewFilterSelectionAction.Caption = "Filter";
            this.FreightForwardinOriginListviewFilterSelectionAction.ConfirmationMessage = null;
            this.FreightForwardinOriginListviewFilterSelectionAction.Id = "FreightForwardinOriginListviewFilterSelectionActionId";
            this.FreightForwardinOriginListviewFilterSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.FreightForwardinOriginListviewFilterSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.FreightForwardingOrigin);
            this.FreightForwardinOriginListviewFilterSelectionAction.ToolTip = null;
            this.FreightForwardinOriginListviewFilterSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.FreightForwardinOriginListviewFilterSelectionAction_Execute);
            // 
            // FreightForwardingOriginListviewFilterApprovalSelectionAction
            // 
            this.FreightForwardingOriginListviewFilterApprovalSelectionAction.Caption = "Filter Approval";
            this.FreightForwardingOriginListviewFilterApprovalSelectionAction.ConfirmationMessage = null;
            this.FreightForwardingOriginListviewFilterApprovalSelectionAction.Id = "FreightForwardingOriginListviewFilterApprovalSelectionActionId";
            this.FreightForwardingOriginListviewFilterApprovalSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.FreightForwardingOriginListviewFilterApprovalSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.FreightForwardingOrigin);
            this.FreightForwardingOriginListviewFilterApprovalSelectionAction.ToolTip = null;
            this.FreightForwardingOriginListviewFilterApprovalSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.FreightForwardingOriginListviewFilterApprovalSelectionAction_Execute);
            // 
            // FreightForwardinOriginGetSBMAction
            // 
            this.FreightForwardinOriginGetSBMAction.Caption = "Get SBM";
            this.FreightForwardinOriginGetSBMAction.ConfirmationMessage = null;
            this.FreightForwardinOriginGetSBMAction.Id = "FreightForwardinOriginGetSBMActionId";
            this.FreightForwardinOriginGetSBMAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.FreightForwardingOrigin);
            this.FreightForwardinOriginGetSBMAction.ToolTip = null;
            this.FreightForwardinOriginGetSBMAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.FreightForwardinOriginGetSBMAction_Execute);
            // 
            // FreightForwardingOriginPostingPackageAction
            // 
            this.FreightForwardingOriginPostingPackageAction.Caption = "Posting Package";
            this.FreightForwardingOriginPostingPackageAction.ConfirmationMessage = null;
            this.FreightForwardingOriginPostingPackageAction.Id = "FreightForwardingOriginPostingPackageActionId";
            this.FreightForwardingOriginPostingPackageAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.FreightForwardingOrigin);
            this.FreightForwardingOriginPostingPackageAction.ToolTip = null;
            this.FreightForwardingOriginPostingPackageAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.FreightForwardingOriginPostingPackageAction_Execute);
            // 
            // FreightForwardingOriginActionController
            // 
            this.Actions.Add(this.FreightForwardingOriginProgressAction);
            this.Actions.Add(this.FreightForwardingOriginApprovalAction);
            this.Actions.Add(this.FreightForwardinOriginListviewFilterSelectionAction);
            this.Actions.Add(this.FreightForwardingOriginListviewFilterApprovalSelectionAction);
            this.Actions.Add(this.FreightForwardinOriginGetSBMAction);
            this.Actions.Add(this.FreightForwardingOriginPostingPackageAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction FreightForwardingOriginProgressAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction FreightForwardingOriginApprovalAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction FreightForwardinOriginListviewFilterSelectionAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction FreightForwardingOriginListviewFilterApprovalSelectionAction;
        private DevExpress.ExpressApp.Actions.SimpleAction FreightForwardinOriginGetSBMAction;
        private DevExpress.ExpressApp.Actions.SimpleAction FreightForwardingOriginPostingPackageAction;
    }
}
