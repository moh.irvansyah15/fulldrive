﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using FullDrive.Module.BusinessObjects;
using DevExpress.Xpo;
using DevExpress.ExpressApp.Xpo;
using FullDrive.Module.CustomProcess;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class SalesReturnLineDetailViewFilterController : ViewController
    {
        public SalesReturnLineDetailViewFilterController()
        {
            InitializeComponent();
            RegisterActions(components);
            TargetViewId = "SalesReturnLine_DetailView";
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }

        protected override void OnActivated()
        {
            base.OnActivated();
            GlobalFunction _globFunc = new GlobalFunction();
            string User = SecuritySystem.CurrentUserName;
            Session _locCurrentSession = ((XPObjectSpace)this.ObjectSpace).Session;
            SalesReturnLine _locSalesReturnLine = (SalesReturnLine)View.CurrentObject;
            View.ObjectSpace.SetModified(_locSalesReturnLine);

            if (_globFunc.GetAdministrationAccessing(_locCurrentSession, User) == false)
            {
                if (GetActivationQuantityAccess() == true)
                {
                    _locSalesReturnLine.ActivationPosting = false;
                }
            }
            else
            {
                _locSalesReturnLine.ActivationPosting = false;
            }

            if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
            {
                View.ObjectSpace.CommitChanges();
            }
            // Perform various tasks depending on the target View.
        }

        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }

        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        public bool GetActivationQuantityAccess()
        {
            bool _result = false;
            try
            {
                Session _locCurrentSession = ((XPObjectSpace)this.ObjectSpace).Session;
                var _locUserName = SecuritySystem.CurrentUserName;

                UserAccess _numLineUserAccess = _locCurrentSession.FindObject<UserAccess>
                                                (new BinaryOperator("UserName", _locUserName));

                if (_numLineUserAccess != null)
                {
                    ApplicationSetup _numLineAppSetup = _locCurrentSession.FindObject<ApplicationSetup>(new GroupOperator
                                                (GroupOperatorType.And, new BinaryOperator("Active", true),
                                                new BinaryOperator("DefaultSystem", true)));

                    if (_numLineAppSetup != null)
                    {
                        SalesSetupDetail _locSalesSetupDetail = _locCurrentSession.FindObject<SalesSetupDetail>
                                                                (new GroupOperator(GroupOperatorType.And,
                                                                 new BinaryOperator("UserAccess", _numLineUserAccess),
                                                                 new BinaryOperator("ObjectList", ObjectList.SalesReturnLine),
                                                                 new BinaryOperator("Active", true),
                                                                 new BinaryOperator("ActivationQuantity", true),
                                                                 new BinaryOperator("ApplicationSetup", _numLineAppSetup)));

                        if (_locSalesSetupDetail != null)
                        {
                            _result = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesReturnLine " + ex.ToString());
            }
            return _result;
        }
    }
}
