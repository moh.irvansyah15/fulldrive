﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;
using System.Web;
using System.IO;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class PurchaseReturnActionController : ViewController
    {
        private ChoiceActionItem _selectionListviewFilter;
        private ChoiceActionItem _setApprovalLevel;

        public PurchaseReturnActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            PurchaseReturnListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {

                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                PurchaseReturnListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
        }

        protected override void OnActivated()
        {
            TypeOfView = typeof(ObjectView);
            base.OnActivated();
            // Perform various tasks depending on the target View.
            #region Approval
            var _locUser = SecuritySystem.CurrentUserName;
            GlobalFunction _globalFunction = new GlobalFunction();
            Session _currentSession = null;
            if (this.ObjectSpace != null)
            {
                _currentSession = ((XPObjectSpace)this.ObjectSpace).Session;
            }

            if (_currentSession != null)
            {
                UserAccess _locUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", _locUser));

                if (_locUserAccess != null)
                {
                    PurchaseReturnApprovalAction.Items.Clear();

                    XPCollection<ApplicationSetupDetail> _locAppSetupDetails = new XPCollection<ApplicationSetupDetail>
                                                                                 (_currentSession, new GroupOperator(GroupOperatorType.And,
                                                                                  new BinaryOperator("UserAccess", _locUserAccess),
                                                                                  new BinaryOperator("ObjectList", CustomProcess.ObjectList.PurchaseReturn),
                                                                                  new BinaryOperator("FunctionList", CustomProcess.FunctionList.Approval),
                                                                                  new BinaryOperator("Active", true)));

                    if (_locAppSetupDetails != null && _locAppSetupDetails.Count > 0)
                    {

                        foreach (ApplicationSetupDetail _locAppSetupDetail in _locAppSetupDetails)
                        {
                            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.ApprovalLevel)))
                            {
                                if (_locAppSetupDetail.ApprovalLevel != CustomProcess.ApprovalLevel.None)
                                {
                                    EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.ApprovalLevel));
                                    _setApprovalLevel = new ChoiceActionItem(_ed.GetCaption(_locAppSetupDetail.ApprovalLevel), _locAppSetupDetail.ApprovalLevel);
                                }
                            }
                            PurchaseReturnApprovalAction.Items.Add(_setApprovalLevel);
                        }
                    }
                }
            }
            #endregion Approval
        }

        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }

        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void PurchaseReturnProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        PurchaseReturn _locPurchaseReturnOS = (PurchaseReturn)_objectSpace.GetObject(obj);

                        if (_locPurchaseReturnOS != null)
                        {
                            if (_locPurchaseReturnOS.Code != null)
                            {
                                _currObjectId = _locPurchaseReturnOS.Code;

                                PurchaseReturn _locPurchaseReturnXPO = _currSession.FindObject<PurchaseReturn>
                                                                 (new GroupOperator(GroupOperatorType.And,
                                                                  new BinaryOperator("Code", _currObjectId)));

                                if (_locPurchaseReturnXPO != null)
                                {
                                    if (_locPurchaseReturnXPO.Status == Status.Open || _locPurchaseReturnXPO.Status == Status.Progress)
                                    {
                                        if (_locPurchaseReturnXPO.Status == Status.Open)
                                        {
                                            _locPurchaseReturnXPO.Status = Status.Progress;
                                            _locPurchaseReturnXPO.StatusDate = now;
                                            _locPurchaseReturnXPO.Save();
                                            _locPurchaseReturnXPO.Session.CommitTransaction();
                                        }

                                        XPCollection<PurchaseReturnLine> _locPurchaseReturnLines = new XPCollection<PurchaseReturnLine>
                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                             new BinaryOperator("PurchaseReturn", _locPurchaseReturnXPO),
                                                                                             new BinaryOperator("Status", Status.Open)));

                                        if (_locPurchaseReturnLines != null && _locPurchaseReturnLines.Count > 0)
                                        {
                                            foreach (PurchaseReturnLine _locPurchaseReturnLine in _locPurchaseReturnLines)
                                            {
                                                _locPurchaseReturnLine.Status = Status.Progress;
                                                _locPurchaseReturnLine.StatusDate = now;
                                                _locPurchaseReturnLine.Save();
                                                _locPurchaseReturnLine.Session.CommitTransaction();
                                            }
                                        }

                                        XPCollection<ReturnPurchaseCollection> _locReturnPurchaseCollections = new XPCollection<ReturnPurchaseCollection>(_currSession,
                                                                                                        new GroupOperator(GroupOperatorType.And,
                                                                                                        new BinaryOperator("PurchaseReturn", _locPurchaseReturnXPO),
                                                                                                        new BinaryOperator("Status", Status.Open)));
                                        if (_locReturnPurchaseCollections != null && _locReturnPurchaseCollections.Count() > 0)
                                        {
                                            foreach (ReturnPurchaseCollection _locReturnPurchaseCollection in _locReturnPurchaseCollections)
                                            {
                                                _locReturnPurchaseCollection.Status = Status.Progress;
                                                _locReturnPurchaseCollection.StatusDate = now;
                                                _locReturnPurchaseCollection.Save();
                                                _locReturnPurchaseCollection.Session.CommitTransaction();
                                            }
                                        }
                                    }
                                    SuccessMessageShow("Purchase Return has been successfully updated to progress");
                                }
                                else
                                {
                                    ErrorMessageShow("Data Purchase Return Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Purchase Return Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseReturn " + ex.ToString());
            }
        }

        private void PurchaseReturnPostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        PurchaseReturn _locPurchaseReturnOS = (PurchaseReturn)_objectSpace.GetObject(obj);

                        if (_locPurchaseReturnOS != null)
                        {
                            if (_locPurchaseReturnOS.Code != null)
                            {
                                _currObjectId = _locPurchaseReturnOS.Code;

                                PurchaseReturn _locPurchaseReturnXPO = _currSession.FindObject<PurchaseReturn>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locPurchaseReturnXPO != null)
                                {
                                    if (_locPurchaseReturnXPO.Status == Status.Progress || _locPurchaseReturnXPO.Status == Status.Posted)
                                    {
                                        SetPurchaseReturnMonitoring(_currSession, _locPurchaseReturnXPO);
                                        SetRemainQty(_currSession, _locPurchaseReturnXPO);
                                        SetPostingQty(_currSession, _locPurchaseReturnXPO);
                                        SetProcessCount(_currSession, _locPurchaseReturnXPO);
                                        SetStatusPurchaseReturnLine(_currSession, _locPurchaseReturnXPO);
                                        SetNormalQuantity(_currSession, _locPurchaseReturnXPO);
                                        SetFinalStatusPurchaseReturn(_currSession, _locPurchaseReturnXPO);
                                    }

                                    SuccessMessageShow("Purchase Return has been successfully Posted");
                                }
                                else
                                {
                                    ErrorMessageShow("Data Purchase Return Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Purchase Return Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseReturn " + ex.ToString());
            }
        }

        private void PurchaseReturnGetITIMAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;


                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        PurchaseReturn _locPurchaseReturnOS = (PurchaseReturn)_objectSpace.GetObject(obj);

                        if (_locPurchaseReturnOS != null)
                        {
                            if (_locPurchaseReturnOS.Code != null)
                            {
                                _currObjectId = _locPurchaseReturnOS.Code;

                                PurchaseReturn _locPurchaseReturnXPO = _currSession.FindObject<PurchaseReturn>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locPurchaseReturnXPO != null)
                                {
                                    if (_locPurchaseReturnXPO.Status == Status.Open || _locPurchaseReturnXPO.Status == Status.Progress)
                                    {
                                        XPCollection<ReturnPurchaseCollection> _locReturnPurchaseCollections = new XPCollection<ReturnPurchaseCollection>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("PurchaseReturn", _locPurchaseReturnXPO),
                                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("Status", Status.Open),
                                                                                        new BinaryOperator("Status", Status.Progress))));

                                        if (_locReturnPurchaseCollections != null && _locReturnPurchaseCollections.Count() > 0)
                                        {
                                            foreach (ReturnPurchaseCollection _locReturnPurchaseCollection in _locReturnPurchaseCollections)
                                            {
                                                if (_locReturnPurchaseCollection.PurchaseReturn != null && (_locReturnPurchaseCollection.InventoryTransferIn != null || _locReturnPurchaseCollection.PurchaseOrder != null))
                                                {
                                                    GetInventoryTransferInMonitoring(_currSession, _locReturnPurchaseCollection.InventoryTransferIn, _locReturnPurchaseCollection.PurchaseOrder, _locPurchaseReturnXPO);
                                                }
                                            }
                                            SuccessMessageShow("Inventory Transfer In Has Been Successfully Getting into Purchase Return");
                                        }
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Inventory Transfer In Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Inventory Transfer In Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseReturn " + ex.ToString());
            }
        }

        private void PurchaseReturnApprovalAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                ApplicationSetupDetail _locAppSetDetail = null;
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                var User = SecuritySystem.CurrentUserName;
                Session _currentSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currentSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                UserAccess _locUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", User));

                foreach (Object obj in _objectsToProcess)
                {
                    PurchaseReturn _objInNewObjectSpace = (PurchaseReturn)_objectSpace.GetObject(obj);

                    if (_objInNewObjectSpace != null)
                    {
                        if (_objInNewObjectSpace.Code != null)
                        {
                            _currObjectId = _objInNewObjectSpace.Code;
                        }
                    }

                    if (_currObjectId != null)
                    {
                        PurchaseReturn _locPurchaseReturnXPO = _currentSession.FindObject<PurchaseReturn>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("Code", _currObjectId)));

                        if (_locPurchaseReturnXPO != null)
                        {
                            if (_locPurchaseReturnXPO.Status == Status.Progress)
                            {
                                ApprovalLine _locApprovalLine = _currentSession.FindObject<ApprovalLine>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("PurchaseReturn", _locPurchaseReturnXPO),
                                                                             new BinaryOperator("EndApproval", true)));
                                if (_locApprovalLine == null)
                                {
                                    #region Approval Level 1
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level1)
                                    {
                                        _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level1, ObjectList.PurchaseReturn);
                                        if (_locAppSetDetail != null)
                                        {
                                            //Buat bs input langsung ke approvalline
                                            ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("PurchaseReturn", _locPurchaseReturnXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level1)));
                                            if (_locApprovalLineXPO == null)
                                            {

                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level1,
                                                        EndApproval = true,
                                                        PurchaseReturn = _locPurchaseReturnXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locPurchaseReturnXPO.ActivationPosting = true;
                                                    _locPurchaseReturnXPO.ActiveApproved1 = false;
                                                    _locPurchaseReturnXPO.ActiveApproved2 = false;
                                                    _locPurchaseReturnXPO.ActiveApproved3 = true;
                                                    _locPurchaseReturnXPO.Save();
                                                    _locPurchaseReturnXPO.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level1,
                                                        PurchaseReturn = _locPurchaseReturnXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locPurchaseReturnXPO.ActiveApproved1 = true;
                                                    _locPurchaseReturnXPO.ActiveApproved2 = false;
                                                    _locPurchaseReturnXPO.ActiveApproved3 = false;
                                                    _locPurchaseReturnXPO.Save();
                                                    _locPurchaseReturnXPO.Session.CommitTransaction();
                                                }

                                                //Send Email
                                                ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("PurchaseReturn", _locPurchaseReturnXPO)));

                                                if (_locApprovalLineXPO2 != null)
                                                {
                                                    SendEmail(_currentSession, _locPurchaseReturnXPO, Status.Progress, _locApprovalLineXPO2);
                                                }

                                                SuccessMessageShow("Purchase Return has successfully Approve");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 1

                                    #region Approval Level 2
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level2)
                                    {
                                        _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level2, ObjectList.PurchaseReturn);

                                        if (_locAppSetDetail != null)
                                        {
                                            ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("PurchaseReturn", _locPurchaseReturnXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level2)));

                                            if (_locApprovalLineXPO == null)
                                            {
                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level2,
                                                        EndApproval = true,
                                                        PurchaseReturn = _locPurchaseReturnXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locPurchaseReturnXPO.ActivationPosting = true;
                                                    _locPurchaseReturnXPO.ActiveApproved1 = false;
                                                    _locPurchaseReturnXPO.ActiveApproved2 = false;
                                                    _locPurchaseReturnXPO.ActiveApproved3 = true;
                                                    _locPurchaseReturnXPO.Save();
                                                    _locPurchaseReturnXPO.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level2,
                                                        PurchaseReturn = _locPurchaseReturnXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    SetApprovalLine(_currentSession, _locPurchaseReturnXPO, ApprovalLevel.Level1);

                                                    _locPurchaseReturnXPO.ActiveApproved1 = false;
                                                    _locPurchaseReturnXPO.ActiveApproved2 = true;
                                                    _locPurchaseReturnXPO.ActiveApproved3 = false;
                                                    _locPurchaseReturnXPO.Save();
                                                    _locPurchaseReturnXPO.Session.CommitTransaction();
                                                }

                                                //Send Email
                                                ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("PurchaseReturn", _locPurchaseReturnXPO)));

                                                if (_locApprovalLineXPO2 != null)
                                                {
                                                    SendEmail(_currentSession, _locPurchaseReturnXPO, Status.Progress, _locApprovalLineXPO2);
                                                }

                                                SuccessMessageShow("Purchase Return has successfully Approve");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 2

                                    #region Approval Level 3
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level3)
                                    {
                                        _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level3, ObjectList.PurchaseReturn);

                                        if (_locAppSetDetail != null)
                                        {
                                            ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("PurchaseReturn", _locPurchaseReturnXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level3)));

                                            if (_locApprovalLineXPO == null)
                                            {
                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level3,
                                                        EndApproval = true,
                                                        PurchaseReturn = _locPurchaseReturnXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locPurchaseReturnXPO.ActivationPosting = true;
                                                    _locPurchaseReturnXPO.ActiveApproved1 = false;
                                                    _locPurchaseReturnXPO.ActiveApproved2 = false;
                                                    _locPurchaseReturnXPO.ActiveApproved3 = true;
                                                    _locPurchaseReturnXPO.Save();
                                                    _locPurchaseReturnXPO.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level3,
                                                        PurchaseReturn = _locPurchaseReturnXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    SetApprovalLine(_currentSession, _locPurchaseReturnXPO, ApprovalLevel.Level2);
                                                    SetApprovalLine(_currentSession, _locPurchaseReturnXPO, ApprovalLevel.Level1);

                                                    _locPurchaseReturnXPO.ActiveApproved1 = false;
                                                    _locPurchaseReturnXPO.ActiveApproved2 = false;
                                                    _locPurchaseReturnXPO.ActiveApproved3 = true;
                                                    _locPurchaseReturnXPO.Save();
                                                    _locPurchaseReturnXPO.Session.CommitTransaction();
                                                }

                                                //Send Email
                                                ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("PurchaseReturn", _locPurchaseReturnXPO)));

                                                if (_locApprovalLineXPO2 != null)
                                                {
                                                    SendEmail(_currentSession, _locPurchaseReturnXPO, Status.Progress, _locApprovalLineXPO2);
                                                }

                                                SuccessMessageShow("Purchase Return has successfully Approve");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 3
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Purchase Return Status Not Available");
                            }
                        }
                        else
                        {
                            ErrorMessageShow("Purchase Return Not Available");
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseReturn " + ex.ToString());
            }
        }

        private void PurchaseReturnListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(PurchaseReturn)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseReturn " + ex.ToString());
            }
        }

        //============================================ Code Only ==========================================

        #region GetITI

        private void GetInventoryTransferInMonitoring(Session _currSession, InventoryTransferIn _locInventoryTransferInXPO, PurchaseOrder _locPurchaseOrderXPO, PurchaseReturn _locPurchaseReturnXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _currSignCode = null;
                double _locTxValue = 0;
                double _locTxAmount = 0;
                double _locDisc = 0;
                double _locDiscAmount = 0;
                double _locTAmount = 0;
                XPCollection<InventoryTransferInMonitoring> _locInventoryTransferInMonitorings = null;

                if (_locPurchaseReturnXPO != null)
                {
                    //Harus mampu menghitung otomatis buat MultiTax dan MultiDiscount
                    if (_locInventoryTransferInXPO != null && _locPurchaseOrderXPO != null)
                    {
                        _locInventoryTransferInMonitorings = new XPCollection<InventoryTransferInMonitoring>
                                                         (_currSession, new GroupOperator(GroupOperatorType.And,
                                                          new BinaryOperator("InventoryTransferIn", _locInventoryTransferInXPO),
                                                          new BinaryOperator("PurchaseOrderMonitoring.PurchaseOrder", _locPurchaseOrderXPO),
                                                          new BinaryOperator("PurchaseReturn", _locPurchaseReturnXPO),
                                                          new BinaryOperator("Select", true),
                                                          new GroupOperator(GroupOperatorType.Or,
                                                          new BinaryOperator("ReturnType", ReturnType.ReplacedFull),
                                                          new BinaryOperator("ReturnType", ReturnType.CreditMemo))
                                                          ));

                    }
                    else if (_locInventoryTransferInXPO != null && _locPurchaseOrderXPO == null)
                    {
                        _locInventoryTransferInMonitorings = new XPCollection<InventoryTransferInMonitoring>
                                                         (_currSession, new GroupOperator(GroupOperatorType.And,
                                                          new BinaryOperator("InventoryTransferIn", _locInventoryTransferInXPO),
                                                          new BinaryOperator("PurchaseReturn", _locPurchaseReturnXPO),
                                                          new BinaryOperator("Select", true),
                                                          new GroupOperator(GroupOperatorType.Or,
                                                          new BinaryOperator("ReturnType", ReturnType.ReplacedFull),
                                                          new BinaryOperator("ReturnType", ReturnType.CreditMemo))
                                                          ));
                    }
                    else if (_locInventoryTransferInXPO == null && _locPurchaseOrderXPO != null)
                    {
                        _locInventoryTransferInMonitorings = new XPCollection<InventoryTransferInMonitoring>
                                                         (_currSession, new GroupOperator(GroupOperatorType.And,
                                                          new BinaryOperator("PurchaseOrderMonitoring.PurchaseOrder", _locPurchaseOrderXPO),
                                                          new BinaryOperator("PurchaseReturn", _locPurchaseReturnXPO),
                                                          new BinaryOperator("Select", true),
                                                          new GroupOperator(GroupOperatorType.Or,
                                                          new BinaryOperator("ReturnType", ReturnType.ReplacedFull),
                                                          new BinaryOperator("ReturnType", ReturnType.CreditMemo))
                                                          ));
                    }


                    if (_locInventoryTransferInMonitorings != null && _locInventoryTransferInMonitorings.Count() > 0)
                    {
                        foreach (InventoryTransferInMonitoring _locInventoryTransferInMonitoring in _locInventoryTransferInMonitorings)
                        {
                            if (_locInventoryTransferInMonitoring.InventoryTransferInLine != null && _locInventoryTransferInMonitoring.PurchaseOrderMonitoring != null
                                && _locInventoryTransferInMonitoring.PurchaseOrderMonitoring.PurchaseOrderLine != null)
                            {
                                _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.PurchaseReturnLine);

                                if (_currSignCode != null)
                                {
                                    #region CountTax
                                    if (_locInventoryTransferInMonitoring.PurchaseOrderMonitoring.MultiTax == true)
                                    {
                                        _locTxValue = GetSumTotalTaxValue(_currSession, _locInventoryTransferInMonitoring.PurchaseOrderMonitoring.PurchaseOrderLine);
                                        _locTxAmount = GetSumTotalTaxAmount(_currSession, _locInventoryTransferInMonitoring.PurchaseOrderMonitoring.PurchaseOrderLine, _locInventoryTransferInMonitoring);
                                    }
                                    else
                                    {
                                        if (_locInventoryTransferInMonitoring.PurchaseOrderMonitoring.TxValue > 0)
                                        {
                                            _locTxValue = _locInventoryTransferInMonitoring.PurchaseOrderMonitoring.PurchaseOrderLine.TxValue;
                                            _locTxAmount = (_locTxValue / 100) * (_locInventoryTransferInMonitoring.TQty * _locInventoryTransferInMonitoring.PurchaseOrderMonitoring.UAmount);
                                        }
                                    }
                                    #endregion CountTax

                                    #region CountDiscount
                                    if (_locInventoryTransferInMonitoring.PurchaseOrderMonitoring.MultiDiscount == true)
                                    {
                                        _locDisc = GetSumTotalDisc(_currSession, _locInventoryTransferInMonitoring.PurchaseOrderMonitoring.PurchaseOrderLine);
                                        _locDiscAmount = GetSumTotalDiscAmount(_currSession, _locInventoryTransferInMonitoring.PurchaseOrderMonitoring.PurchaseOrderLine, _locInventoryTransferInMonitoring);
                                    }
                                    else
                                    {
                                        if (_locInventoryTransferInMonitoring.PurchaseOrderMonitoring.Disc > 0)
                                        {
                                            _locDisc = _locInventoryTransferInMonitoring.PurchaseOrderMonitoring.PurchaseOrderLine.Disc;
                                            _locDiscAmount = (_locDisc / 100) * (_locInventoryTransferInMonitoring.TQty * _locInventoryTransferInMonitoring.PurchaseOrderMonitoring.UAmount);
                                        }
                                    }
                                    #endregion CountDiscount

                                    _locTAmount = (_locInventoryTransferInMonitoring.PurchaseOrderMonitoring.UAmount * _locInventoryTransferInMonitoring.TQty) + _locTxAmount - _locDiscAmount;
                                    //Tempat untuk menghitung multitax dan multidiscount
                                    PurchaseReturnLine _saveDataPurchaseReturnLine = new PurchaseReturnLine(_currSession)
                                    {
                                        SignCode = _currSignCode,
                                        OrderType = _locInventoryTransferInMonitoring.PurchaseOrderMonitoring.PurchaseOrderLine.PurchaseType,
                                        Item = _locInventoryTransferInMonitoring.Item,
                                        Description = _locInventoryTransferInMonitoring.InventoryTransferInLine.Description,
                                        DQty = _locInventoryTransferInMonitoring.DQty,
                                        DUOM = _locInventoryTransferInMonitoring.DUOM,
                                        Qty = _locInventoryTransferInMonitoring.Qty,
                                        UOM = _locInventoryTransferInMonitoring.UOM,
                                        TQty = _locInventoryTransferInMonitoring.TQty,
                                        Currency = _locInventoryTransferInMonitoring.PurchaseOrderMonitoring.Currency,
                                        UAmount = _locInventoryTransferInMonitoring.PurchaseOrderMonitoring.UAmount,
                                        TUAmount = _locInventoryTransferInMonitoring.TQty * _locInventoryTransferInMonitoring.PurchaseOrderMonitoring.UAmount,
                                        MultiTax = _locInventoryTransferInMonitoring.PurchaseOrderMonitoring.MultiTax,
                                        Tax = _locInventoryTransferInMonitoring.PurchaseOrderMonitoring.Tax,
                                        TxValue = _locTxValue,
                                        TxAmount = _locTxAmount,
                                        MultiDiscount = _locInventoryTransferInMonitoring.PurchaseOrderMonitoring.MultiDiscount,
                                        Discount = _locInventoryTransferInMonitoring.PurchaseOrderMonitoring.Discount,
                                        Disc = _locDisc,
                                        DiscAmount = _locDiscAmount,
                                        TAmount = _locTAmount,
                                        InventoryTransferInMonitoring = _locInventoryTransferInMonitoring,
                                        PurchaseOrderMonitoring = _locInventoryTransferInMonitoring.PurchaseOrderMonitoring,
                                        PurchaseReturn = _locPurchaseReturnXPO,
                                        ReturnType = _locInventoryTransferInMonitoring.ReturnType,
                                    };
                                    _saveDataPurchaseReturnLine.Save();
                                    _saveDataPurchaseReturnLine.Session.CommitTransaction();

                                    PurchaseReturnLine _locPurchaseReturnLine = _currSession.FindObject<PurchaseReturnLine>
                                                                (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("SignCode", _currSignCode)));
                                    if (_locPurchaseReturnLine != null)
                                    {
                                        #region TaxLine
                                        if (_locInventoryTransferInMonitoring.PurchaseOrderMonitoring.PurchaseOrderLine.MultiTax == true)
                                        {
                                            XPCollection<TaxLine> _locTaxLines = new XPCollection<TaxLine>(_currSession, new GroupOperator(
                                                                                GroupOperatorType.And,
                                                                                new BinaryOperator("PurchaseOrderLine", _locInventoryTransferInMonitoring.PurchaseOrderMonitoring.PurchaseOrderLine)));
                                            if (_locTaxLines != null && _locTaxLines.Count() > 0)
                                            {
                                                foreach (TaxLine _locTaxLine in _locTaxLines)
                                                {
                                                    TaxLine _saveDataTaxLine = new TaxLine(_currSession)
                                                    {
                                                        Tax = _locTaxLine.Tax,
                                                        Name = _locTaxLine.Name,
                                                        TaxType = _locTaxLine.TaxType,
                                                        TaxGroup = _locTaxLine.TaxGroup,
                                                        TaxGroupType = _locTaxLine.TaxGroupType,
                                                        TaxAccountGroup = _locTaxLine.TaxAccountGroup,
                                                        TaxNature = _locTaxLine.TaxNature,
                                                        TaxMode = _locTaxLine.TaxMode,
                                                        TxValue = _locTaxLine.TxValue,
                                                        Description = _locTaxLine.Description,
                                                        UAmount = _locPurchaseReturnLine.UAmount,
                                                        TUAmount = _locPurchaseReturnLine.TUAmount,
                                                        TxAmount = _locPurchaseReturnLine.TxAmount,
                                                        PurchaseReturnLine = _locPurchaseReturnLine,
                                                    };
                                                    _saveDataTaxLine.Save();
                                                    _saveDataTaxLine.Session.CommitTransaction();
                                                }
                                            }
                                        }
                                        #endregion TaxLine

                                        #region DiscountLine
                                        if (_locInventoryTransferInMonitoring.PurchaseOrderMonitoring.PurchaseOrderLine.MultiDiscount == true)
                                        {
                                            XPCollection<DiscountLine> _locDiscountLines = new XPCollection<DiscountLine>(_currSession,
                                                                                new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("PurchaseOrderLine", _locInventoryTransferInMonitoring.PurchaseOrderMonitoring.PurchaseOrderLine)));
                                            if (_locDiscountLines != null && _locDiscountLines.Count() > 0)
                                            {
                                                foreach (DiscountLine _locDiscountLine in _locDiscountLines)
                                                {
                                                    DiscountLine _saveDataDiscountLine = new DiscountLine(_currSession)
                                                    {
                                                        Discount = _locDiscountLine.Discount,
                                                        Name = _locDiscountLine.Name,
                                                        DiscountMode = _locDiscountLine.DiscountMode,
                                                        Description = _locDiscountLine.Description,
                                                        UAmount = _locPurchaseReturnLine.UAmount,
                                                        Disc = _locDiscountLine.Disc,
                                                        DiscAmount = _locPurchaseReturnLine.DiscAmount,
                                                        TUAmount = _locPurchaseReturnLine.TUAmount,
                                                        PurchaseReturnLine = _locPurchaseReturnLine,
                                                    };
                                                    _saveDataDiscountLine.Save();
                                                    _saveDataDiscountLine.Session.CommitTransaction();
                                                }
                                            }
                                        }
                                        #endregion DiscountLine

                                        _locInventoryTransferInMonitoring.PurchaseReturnLine = _locPurchaseReturnLine;
                                        _locInventoryTransferInMonitoring.ReturnStatus = Status.Close;
                                        _locInventoryTransferInMonitoring.ReturnStatusDate = now;
                                        _locInventoryTransferInMonitoring.Save();
                                        _locInventoryTransferInMonitoring.Session.CommitTransaction();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PurchaseReturn ", ex.ToString());
            }
        }

        private double GetSumTotalTaxValue(Session _currSession, PurchaseOrderLine _purchaseOrderLineXPO)
        {
            double _result = 0;
            try
            {
                double _locTxValue = 0;

                if (_purchaseOrderLineXPO != null)
                {
                    XPCollection<TaxLine> _locTaxLines = new XPCollection<TaxLine>(_currSession,
                                                     new BinaryOperator("PurchaseOrderLine", _purchaseOrderLineXPO));

                    if (_locTaxLines.Count() > 0)
                    {
                        foreach (TaxLine _locTaxLine in _locTaxLines)
                        {
                            if (_locTaxLine.TxValue > 0 && _locTaxLine.TxAmount > 0)
                            {
                                if (_locTaxLine.TaxNature == TaxNature.Increase)
                                {
                                    _locTxValue = _locTxValue + _locTaxLine.TxValue;

                                }
                                else if (_locTaxLine.TaxNature == TaxNature.Decrease)
                                {
                                    _locTxValue = _locTxValue - _locTaxLine.TxValue;
                                }
                            }
                        }
                        _result = _locTxValue;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PurchaseReturn " + ex.ToString());
            }
            return _result;
        }

        private double GetSumTotalTaxAmount(Session _currSession, PurchaseOrderLine _purchaseOrderLineXPO, InventoryTransferInMonitoring _inventoryTransferInMonitoringXPO)
        {
            double _return = 0;
            try
            {
                double _locTxAmount = 0;

                if (_purchaseOrderLineXPO != null)
                {
                    XPCollection<TaxLine> _locTaxLines = new XPCollection<TaxLine>(_currSession,
                                                     new BinaryOperator("PurchaseOrderLine", _purchaseOrderLineXPO));

                    if (_locTaxLines.Count() > 0)
                    {
                        foreach (TaxLine _locTaxLine in _locTaxLines)
                        {
                            if (_locTaxLine.TxValue > 0)
                            {
                                if (_locTaxLine.TaxNature == TaxNature.Increase)
                                {
                                    _locTxAmount = _locTxAmount + (_purchaseOrderLineXPO.UAmount * _inventoryTransferInMonitoringXPO.TQty * _locTaxLine.TxValue / 100);
                                }
                                else if (_locTaxLine.TaxNature == TaxNature.Decrease)
                                {
                                    _locTxAmount = _locTxAmount - (_purchaseOrderLineXPO.UAmount * _inventoryTransferInMonitoringXPO.TQty * _locTaxLine.TxValue / 100);
                                }
                            }
                        }
                        _return = _locTxAmount;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PurchaseReturn " + ex.ToString());
            }
            return _return;
        }

        private double GetSumTotalDisc(Session _currSession, PurchaseOrderLine _purchaseOrderLineXPO)
        {
            double _result = 0;
            try
            {
                double _locDisc = 0;


                if (_purchaseOrderLineXPO != null)
                {
                    XPCollection<DiscountLine> _locDiscountLines = new XPCollection<DiscountLine>(_currSession,
                                                               new BinaryOperator("PurchaseOrderLine", _purchaseOrderLineXPO));

                    if (_locDiscountLines.Count() > 0)
                    {
                        foreach (DiscountLine _locDiscountLine in _locDiscountLines)
                        {
                            if (_locDiscountLine.Disc > 0 && _locDiscountLine.DiscAmount > 0)
                            {
                                if (_locDiscountLine.Disc > 0 && _locDiscountLine.DiscAmount > 0)
                                {
                                    _locDisc = _locDisc + _locDiscountLine.Disc;
                                }
                            }
                        }
                        _result = _locDisc;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PurchaseReturn " + ex.ToString());
            }
            return _result;
        }

        private double GetSumTotalDiscAmount(Session _currSession, PurchaseOrderLine _purchaseOrderLineXPO, InventoryTransferInMonitoring _inventoryTransferInMonitoringXPO)
        {
            double _result = 0;
            try
            {
                double _locDiscAmount = 0;

                if (_purchaseOrderLineXPO != null)
                {
                    XPCollection<DiscountLine> _locDiscountLines = new XPCollection<DiscountLine>(_currSession,
                                                               new BinaryOperator("PurchaseOrderLine", _purchaseOrderLineXPO));

                    if (_locDiscountLines.Count() > 0)
                    {
                        foreach (DiscountLine _locDiscountLine in _locDiscountLines)
                        {
                            if (_locDiscountLine.Disc > 0)
                            {
                                _locDiscAmount = _locDiscAmount + (_purchaseOrderLineXPO.UAmount * _inventoryTransferInMonitoringXPO.TQty * _locDiscountLine.Disc / 100);
                            }
                        }
                        _result = _locDiscAmount;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PurchaseReturn " + ex.ToString());
            }
            return _result;
        }

        #endregion GetITI

        #region Posting

        private void SetPurchaseReturnMonitoring(Session _currSession, PurchaseReturn _locPurchaseReturnXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locPurchaseReturnXPO != null)
                {
                    XPCollection<PurchaseReturnLine> _locPurchaseReturnLines = new XPCollection<PurchaseReturnLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseReturn", _locPurchaseReturnXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Progress),
                                                            new BinaryOperator("Status", Status.Posted))));

                    if (_locPurchaseReturnLines != null && _locPurchaseReturnLines.Count() > 0)
                    {
                        foreach (PurchaseReturnLine _locPurchaseReturnLine in _locPurchaseReturnLines)
                        {
                            PurchaseReturnMonitoring _saveDataPurchaseReturnMonitoring = new PurchaseReturnMonitoring(_currSession)
                            {
                                PurchaseReturn = _locPurchaseReturnXPO,
                                PurchaseReturnLine = _locPurchaseReturnLine,
                                OrderType = _locPurchaseReturnLine.OrderType,
                                Item = _locPurchaseReturnLine.Item,
                                DQty = _locPurchaseReturnLine.DQty,
                                DUOM = _locPurchaseReturnLine.DUOM,
                                Qty = _locPurchaseReturnLine.Qty,
                                TQty = _locPurchaseReturnLine.TQty,
                                UOM = _locPurchaseReturnLine.UOM,
                                Currency = _locPurchaseReturnLine.Currency,
                                UAmount = _locPurchaseReturnLine.UAmount,
                                TUAmount = _locPurchaseReturnLine.TUAmount,
                                MultiTax = _locPurchaseReturnLine.MultiTax,
                                Tax = _locPurchaseReturnLine.Tax,
                                TxValue = _locPurchaseReturnLine.TxValue,
                                TxAmount = _locPurchaseReturnLine.TxAmount,
                                MultiDiscount = _locPurchaseReturnLine.MultiDiscount,
                                Discount = _locPurchaseReturnLine.Discount,
                                Disc = _locPurchaseReturnLine.Disc,
                                DiscAmount = _locPurchaseReturnLine.DiscAmount,
                                TAmount = _locPurchaseReturnLine.TAmount,
                                ReturnType = _locPurchaseReturnLine.ReturnType,
                            };
                            _saveDataPurchaseReturnMonitoring.Save();
                            _saveDataPurchaseReturnMonitoring.Session.CommitTransaction();

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseReturn " + ex.ToString());
            }
        }

        private void SetRemainQty(Session _currSession, PurchaseReturn _locPurchaseReturnXPO)
        {
            try
            {
                if (_locPurchaseReturnXPO != null)
                {
                    XPCollection<PurchaseReturnLine> _locPurchaseReturnLines = new XPCollection<PurchaseReturnLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseReturn", _locPurchaseReturnXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locPurchaseReturnLines != null && _locPurchaseReturnLines.Count > 0)
                    {
                        double _locRmDQty = 0;
                        double _locRmQty = 0;
                        double _locInvLineTotal = 0;
                        ItemUnitOfMeasure _locItemUOM = null;

                        foreach (PurchaseReturnLine _locPurchaseReturnLine in _locPurchaseReturnLines)
                        {
                            #region ProcessCount=0
                            if (_locPurchaseReturnLine.PostedCount == 0)
                            {
                                #region MaxQuantity
                                if (_locPurchaseReturnLine.MxDQty > 0)
                                {
                                    if (_locPurchaseReturnLine.DQty > 0 && _locPurchaseReturnLine.DQty <= _locPurchaseReturnLine.MxDQty)
                                    {
                                        _locRmDQty = _locPurchaseReturnLine.MxDQty - _locPurchaseReturnLine.DQty;
                                    }

                                    if (_locPurchaseReturnLine.Qty > 0 && _locPurchaseReturnLine.Qty <= _locPurchaseReturnLine.MxQty)
                                    {
                                        _locRmQty = _locPurchaseReturnLine.MxQty - _locPurchaseReturnLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locPurchaseReturnLine.Item),
                                                                new BinaryOperator("UOM", _locPurchaseReturnLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locPurchaseReturnLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty + _locRmDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }

                                }
                                #endregion MaxQuantity

                                #region NonMaxQuantity
                                else
                                {
                                    if (_locPurchaseReturnLine.DQty > 0)
                                    {
                                        _locRmDQty = 0;
                                    }

                                    if (_locPurchaseReturnLine.Qty > 0)
                                    {
                                        _locRmQty = 0;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locPurchaseReturnLine.Item),
                                                                new BinaryOperator("UOM", _locPurchaseReturnLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locPurchaseReturnLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty + _locRmDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion ProcessCount=0

                            #region ProcessCount>0
                            if (_locPurchaseReturnLine.PostedCount > 0)
                            {
                                if (_locPurchaseReturnLine.RmDQty > 0)
                                {
                                    _locRmDQty = _locPurchaseReturnLine.RmDQty - _locPurchaseReturnLine.DQty;
                                }

                                if (_locPurchaseReturnLine.RmQty > 0)
                                {
                                    _locRmQty = _locPurchaseReturnLine.RmQty - _locPurchaseReturnLine.Qty;
                                }

                                if (_locPurchaseReturnLine.MxDQty > 0 || _locPurchaseReturnLine.MxQty > 0)
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locPurchaseReturnLine.Item),
                                                                new BinaryOperator("UOM", _locPurchaseReturnLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locPurchaseReturnLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                }
                                else
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locPurchaseReturnLine.Item),
                                                                new BinaryOperator("UOM", _locPurchaseReturnLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locPurchaseReturnLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                }


                                if (_locItemUOM != null)
                                {
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locRmQty + _locRmDQty;
                                }

                            }
                            #endregion ProcessCount>0

                            _locPurchaseReturnLine.RmDQty = _locRmDQty;
                            _locPurchaseReturnLine.RmQty = _locRmQty;
                            _locPurchaseReturnLine.RmTQty = _locInvLineTotal;
                            _locPurchaseReturnLine.Save();
                            _locPurchaseReturnLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseReturn " + ex.ToString());
            }
        }

        private void SetPostingQty(Session _currSession, PurchaseReturn _locPurchaseReturnXPO)
        {
            try
            {
                if (_locPurchaseReturnXPO != null)
                {
                    XPCollection<PurchaseReturnLine> _locPurchaseReturnLines = new XPCollection<PurchaseReturnLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseReturn", _locPurchaseReturnXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locPurchaseReturnLines != null && _locPurchaseReturnLines.Count > 0)
                    {
                        double _locPDQty = 0;
                        double _locPQty = 0;
                        double _locInvLineTotal = 0;
                        ItemUnitOfMeasure _locItemUOM = null;

                        foreach (PurchaseReturnLine _locPurchaseReturnLine in _locPurchaseReturnLines)
                        {
                            #region ProcessCount=0
                            if (_locPurchaseReturnLine.PostedCount == 0)
                            {
                                #region MaxQuantity
                                if (_locPurchaseReturnLine.MxDQty > 0)
                                {
                                    if (_locPurchaseReturnLine.DQty > 0 && _locPurchaseReturnLine.DQty <= _locPurchaseReturnLine.MxDQty)
                                    {
                                        _locPDQty = _locPurchaseReturnLine.DQty;
                                    }

                                    if (_locPurchaseReturnLine.Qty > 0 && _locPurchaseReturnLine.Qty <= _locPurchaseReturnLine.MxQty)
                                    {
                                        _locPQty = _locPurchaseReturnLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locPurchaseReturnLine.Item),
                                                                new BinaryOperator("UOM", _locPurchaseReturnLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locPurchaseReturnLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty + _locPDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }

                                }
                                #endregion MaxQuantity

                                #region NonMaxQuantity
                                else
                                {
                                    if (_locPurchaseReturnLine.DQty > 0)
                                    {
                                        _locPDQty = _locPurchaseReturnLine.DQty;
                                    }

                                    if (_locPurchaseReturnLine.Qty > 0)
                                    {
                                        _locPQty = _locPurchaseReturnLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locPurchaseReturnLine.Item),
                                                                new BinaryOperator("UOM", _locPurchaseReturnLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locPurchaseReturnLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty + _locPDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion ProcessCount=0

                            #region ProcessCount>0
                            if (_locPurchaseReturnLine.PostedCount > 0)
                            {
                                if (_locPurchaseReturnLine.PDQty > 0)
                                {
                                    _locPDQty = _locPurchaseReturnLine.PDQty + _locPurchaseReturnLine.DQty;
                                }

                                if (_locPurchaseReturnLine.PQty > 0)
                                {
                                    _locPQty = _locPurchaseReturnLine.PQty + _locPurchaseReturnLine.Qty;
                                }

                                if (_locPurchaseReturnLine.MxDQty > 0 || _locPurchaseReturnLine.MxQty > 0)
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                           (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Item", _locPurchaseReturnLine.Item),
                                                            new BinaryOperator("UOM", _locPurchaseReturnLine.MxUOM),
                                                            new BinaryOperator("DefaultUOM", _locPurchaseReturnLine.MxDUOM),
                                                            new BinaryOperator("Active", true)));
                                }
                                else
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                           (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Item", _locPurchaseReturnLine.Item),
                                                            new BinaryOperator("UOM", _locPurchaseReturnLine.UOM),
                                                            new BinaryOperator("DefaultUOM", _locPurchaseReturnLine.DUOM),
                                                            new BinaryOperator("Active", true)));
                                }

                                if (_locItemUOM != null)
                                {
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locPQty + _locPDQty;
                                }
                            }
                            #endregion ProcessCount>0

                            _locPurchaseReturnLine.PDQty = _locPDQty;
                            _locPurchaseReturnLine.PDUOM = _locPurchaseReturnLine.DUOM;
                            _locPurchaseReturnLine.PQty = _locPQty;
                            _locPurchaseReturnLine.PUOM = _locPurchaseReturnLine.UOM;
                            _locPurchaseReturnLine.PTQty = _locInvLineTotal;
                            _locPurchaseReturnLine.Save();
                            _locPurchaseReturnLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseReturn " + ex.ToString());
            }
        }

        private void SetProcessCount(Session _currSession, PurchaseReturn _locPurchaseReturnXPO)
        {
            try
            {
                if (_locPurchaseReturnXPO != null)
                {
                    XPCollection<PurchaseReturnLine> _locPurchaseReturnLines = new XPCollection<PurchaseReturnLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseReturn", _locPurchaseReturnXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locPurchaseReturnLines != null && _locPurchaseReturnLines.Count > 0)
                    {

                        foreach (PurchaseReturnLine _locPurchaseReturnLine in _locPurchaseReturnLines)
                        {
                            if (_locPurchaseReturnLine.Status == Status.Progress || _locPurchaseReturnLine.Status == Status.Posted)
                            {
                                _locPurchaseReturnLine.PostedCount = _locPurchaseReturnLine.PostedCount + 1;
                                _locPurchaseReturnLine.Save();
                                _locPurchaseReturnLine.Session.CommitTransaction();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseReturn " + ex.ToString());
            }
        }

        private void SetStatusPurchaseReturnLine(Session _currSession, PurchaseReturn _locPurchaseReturnXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locPurchaseReturnXPO != null)
                {
                    XPCollection<PurchaseReturnLine> _locPurchaseReturnLines = new XPCollection<PurchaseReturnLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseReturn", _locPurchaseReturnXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locPurchaseReturnLines != null && _locPurchaseReturnLines.Count > 0)
                    {

                        foreach (PurchaseReturnLine _locPurchaseReturnLine in _locPurchaseReturnLines)
                        {
                            if (_locPurchaseReturnLine.Status == Status.Progress || _locPurchaseReturnLine.Status == Status.Posted)
                            {
                                if (_locPurchaseReturnLine.RmDQty == 0 && _locPurchaseReturnLine.RmQty == 0 && _locPurchaseReturnLine.RmTQty == 0)
                                {
                                    _locPurchaseReturnLine.Status = Status.Close;
                                    _locPurchaseReturnLine.ActivationPosting = true;
                                    _locPurchaseReturnLine.ActivationQuantity = true;
                                    _locPurchaseReturnLine.StatusDate = now;
                                }
                                else
                                {
                                    _locPurchaseReturnLine.Status = Status.Posted;
                                    _locPurchaseReturnLine.StatusDate = now;
                                }
                                _locPurchaseReturnLine.Save();
                                _locPurchaseReturnLine.Session.CommitTransaction();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseReturn " + ex.ToString());
            }
        }

        private void SetNormalQuantity(Session _currSession, PurchaseReturn _locPurchaseReturnXPO)
        {
            try
            {
                if (_locPurchaseReturnXPO != null)
                {
                    XPCollection<PurchaseReturnLine> _locPurchaseReturnLines = new XPCollection<PurchaseReturnLine>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("PurchaseReturn", _locPurchaseReturnXPO),
                                                                                     new BinaryOperator("Select", true)));

                    if (_locPurchaseReturnLines != null && _locPurchaseReturnLines.Count > 0)
                    {
                        foreach (PurchaseReturnLine _locPurchaseReturnLine in _locPurchaseReturnLines)
                        {
                            if (_locPurchaseReturnLine.Status == Status.Progress || _locPurchaseReturnLine.Status == Status.Posted || _locPurchaseReturnLine.Status == Status.Close)
                            {
                                if (_locPurchaseReturnLine.DQty > 0 || _locPurchaseReturnLine.Qty > 0)
                                {
                                    _locPurchaseReturnLine.Select = false;
                                    _locPurchaseReturnLine.DQty = 0;
                                    _locPurchaseReturnLine.Qty = 0;
                                    _locPurchaseReturnLine.Save();
                                    _locPurchaseReturnLine.Session.CommitTransaction();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseReturn " + ex.ToString());
            }
        }

        private void SetFinalStatusPurchaseReturn(Session _currSession, PurchaseReturn _locPurchaseReturnXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locStatusCount = 0;
                int _locPurchaseReturnLineCount = 0;

                if (_locPurchaseReturnXPO != null)
                {

                    XPCollection<PurchaseReturnLine> _locPurchaseReturnLines = new XPCollection<PurchaseReturnLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseReturn", _locPurchaseReturnXPO)));

                    if (_locPurchaseReturnLines != null && _locPurchaseReturnLines.Count() > 0)
                    {
                        _locPurchaseReturnLineCount = _locPurchaseReturnLines.Count();

                        foreach (PurchaseReturnLine _locPurchaseReturnLine in _locPurchaseReturnLines)
                        {
                            if (_locPurchaseReturnLine.Status == Status.Close)
                            {
                                _locStatusCount = _locStatusCount + 1;
                            }
                        }
                    }

                    if (_locStatusCount == _locPurchaseReturnLineCount)
                    {
                        _locPurchaseReturnXPO.ActivationPosting = true;
                        _locPurchaseReturnXPO.Status = Status.Close;
                        _locPurchaseReturnXPO.StatusDate = now;
                        _locPurchaseReturnXPO.Save();
                        _locPurchaseReturnXPO.Session.CommitTransaction();
                    }
                    else
                    {
                        _locPurchaseReturnXPO.Status = Status.Posted;
                        _locPurchaseReturnXPO.StatusDate = now;
                        _locPurchaseReturnXPO.Save();
                        _locPurchaseReturnXPO.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseReturn " + ex.ToString());
            }
        }

        #endregion Posting

        #region Posting MethodOld

        private void SetInventoryTransferReplacedFullAndCreditMemoOld(Session _currSession, PurchaseReturn _locPurchaseReturnXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _locDocCode = null;
                InventoryTransferOut _locInventoryTransferOut = null;

                XPCollection<PurchaseReturnLine> _locPurchaseReturnLines = new XPCollection<PurchaseReturnLine>(_currSession,
                                                                new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("PurchaseReturn", _locPurchaseReturnXPO)));

                if (_locPurchaseReturnLines != null && _locPurchaseReturnLines.Count() > 0)
                {
                    if (_locPurchaseReturnXPO.PurchaseOrder != null && _locPurchaseReturnXPO != null)
                    {
                        _locInventoryTransferOut = _currSession.FindObject<InventoryTransferOut>
                                                    (new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("PurchaseReturn", _locPurchaseReturnXPO),
                                                    new BinaryOperator("PurchaseOrder", _locPurchaseReturnXPO.PurchaseOrder)));
                    }
                    else
                    {
                        _locInventoryTransferOut = _currSession.FindObject<InventoryTransferOut>
                                                    (new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("PurchaseReturn", _locPurchaseReturnXPO)));
                    }


                    if (_locInventoryTransferOut != null)
                    {
                        #region UpdateInventoryTransfer

                        foreach (PurchaseReturnLine _locPurchaseReturnLine in _locPurchaseReturnLines)
                        {
                            if (_locPurchaseReturnLine.Status == Status.Progress || _locPurchaseReturnLine.Status == Status.Posted)
                            {
                                if (_locPurchaseReturnLine.Select == true)
                                {
                                    InventoryTransferOutLine _locInventoryTransferOutLine = _currSession.FindObject<InventoryTransferOutLine>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("InventoryTransferOut", _locInventoryTransferOut),
                                                                            new BinaryOperator("Item", _locPurchaseReturnLine.Item)));
                                                                            //new BinaryOperator("Location", _locPurchaseReturnLine.Location),
                                                                            //new BinaryOperator("BinLocation", _locPurchaseReturnLine.BinLocation)));
                                    if (_locInventoryTransferOutLine == null)
                                    {
                                        InventoryTransferOutLine _locSaveDataInventoryTransferOutLine = new InventoryTransferOutLine(_currSession)
                                        {
                                            Item = _locPurchaseReturnLine.Item,
                                            Description = _locPurchaseReturnLine.Description,
                                            MxDQty = _locPurchaseReturnLine.DQty,
                                            MxDUOM = _locPurchaseReturnLine.DUOM,
                                            MxQty = _locPurchaseReturnLine.Qty,
                                            MxUOM = _locPurchaseReturnLine.UOM,
                                            ReturnType = _locPurchaseReturnLine.ReturnType,
                                            //Location = _locPurchaseReturnLine.Location,
                                            //BinLocation = _locPurchaseReturnLine.BinLocation,
                                            //StockType = _locPurchaseReturnLine.StockType,
                                            EstimatedDate = _locPurchaseReturnLine.EstimatedDate,
                                            InventoryTransferOut = _locInventoryTransferOut,
                                        };
                                        _locSaveDataInventoryTransferOutLine.Save();
                                        _locSaveDataInventoryTransferOutLine.Session.CommitTransaction();
                                    }
                                }
                            }
                        }

                        #endregion UpdateInventoryTransfer
                    }
                    else
                    {
                        #region CreateNewInventoryTransfer

                        DocumentType _locDocumentTypeXPO = _currSession.FindObject<DocumentType>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("DirectionType", DirectionType.External),
                                                                     new BinaryOperator("InventoryMovingType", InventoryMovingType.Return),
                                                                     new BinaryOperator("ObjectList", ObjectList.InventoryTransferOut),
                                                                     new BinaryOperator("DocumentRule", DocumentRule.Vendor),
                                                                     new BinaryOperator("Active", true),
                                                                     new BinaryOperator("Default", true)));

                        if (_locDocumentTypeXPO != null)
                        {
                            _locDocCode = _globFunc.GetDocumentNumberingUnlockOptimisticRecord(_currSession.DataLayer, _locDocumentTypeXPO);

                            if (_locDocCode != null)
                            {
                                InventoryTransferOut _locSaveDataInventoryOutTransfer = new InventoryTransferOut(_currSession)
                                {
                                    TransferType = _locDocumentTypeXPO.TransferType,
                                    InventoryMovingType = _locDocumentTypeXPO.InventoryMovingType,
                                    ObjectList = _locDocumentTypeXPO.ObjectList,
                                    DocumentType = _locDocumentTypeXPO,
                                    DocNo = _locDocCode,
                                    BusinessPartner = _locPurchaseReturnXPO.BuyFromVendor,
                                    BusinessPartnerCountry = _locPurchaseReturnXPO.BuyFromVendor.Country,
                                    BusinessPartnerCity = _locPurchaseReturnXPO.BuyFromVendor.City,
                                    BusinessPartnerAddress = _locPurchaseReturnXPO.BuyFromAddress,
                                    EstimatedDate = _locPurchaseReturnXPO.EstimatedDate,
                                    PurchaseReturn = _locPurchaseReturnXPO,
                                    PurchaseOrder = _locPurchaseReturnXPO.PurchaseOrder,

                                };
                                _locSaveDataInventoryOutTransfer.Save();
                                _locSaveDataInventoryOutTransfer.Session.CommitTransaction();

                                InventoryTransferOut _locInventoryTransferOut2x = _currSession.FindObject<InventoryTransferOut>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("DocNo", _locDocCode)));

                                if (_locInventoryTransferOut2x != null)
                                {
                                    foreach (PurchaseReturnLine _locPurchaseReturnLine in _locPurchaseReturnLines)
                                    {
                                        if (_locPurchaseReturnLine.Status == Status.Progress || _locPurchaseReturnLine.Status == Status.Posted)
                                        {
                                            if (_locPurchaseReturnLine.Select == true)
                                            {
                                                InventoryTransferOutLine _locSaveDataInventoryTransferOutLine = new InventoryTransferOutLine(_currSession)
                                                {
                                                    Item = _locPurchaseReturnLine.Item,
                                                    Description = _locPurchaseReturnLine.Description,
                                                    MxDQty = _locPurchaseReturnLine.DQty,
                                                    MxDUOM = _locPurchaseReturnLine.DUOM,
                                                    MxQty = _locPurchaseReturnLine.Qty,
                                                    MxUOM = _locPurchaseReturnLine.UOM,
                                                    ReturnType = _locPurchaseReturnLine.ReturnType,
                                                    //Location = _locPurchaseReturnLine.Location,
                                                    //BinLocation = _locPurchaseReturnLine.BinLocation,
                                                    //StockType = _locPurchaseReturnLine.StockType,
                                                    EstimatedDate = _locPurchaseReturnLine.EstimatedDate,
                                                    InventoryTransferOut = _locInventoryTransferOut2x,
                                                };
                                                _locSaveDataInventoryTransferOutLine.Save();
                                                _locSaveDataInventoryTransferOutLine.Session.CommitTransaction();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        #endregion CreateNewInventoryTransfer
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseReturn " + ex.ToString());
            }
        }

        private void SetCreditMemoOld(Session _currSession, PurchaseReturn _locPurchaseReturnXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _locSignCode = null;
                CreditMemo _locCreditMemo = null;
                double _locUnitPrice = 0;

                //Grouping based on ReturnType

                XPQuery<PurchaseReturnLine> _purchReturnLineQuery = new XPQuery<PurchaseReturnLine>(_currSession);


                var _purchReturnLines = from prlQ in _purchReturnLineQuery
                                        where (prlQ.Select == true && prlQ.Status == Status.Progress
                                        && prlQ.ReturnType == ReturnType.CreditMemo
                                        && prlQ.PurchaseReturn == _locPurchaseReturnXPO)
                                        group prlQ by prlQ.ReturnType into g
                                        select new { ReturnType = g.Key };

                if (_purchReturnLines != null && _purchReturnLines.Count() > 0)
                {
                    foreach (var _purchReturnLine in _purchReturnLines)
                    {
                        XPCollection<PurchaseReturnLine> _locPurchaseReturnLines = new XPCollection<PurchaseReturnLine>(_currSession,
                                                                new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Select", true),
                                                                new BinaryOperator("PurchaseReturn", _locPurchaseReturnXPO),
                                                                new BinaryOperator("ReturnType", _purchReturnLine.ReturnType)));

                        if (_locPurchaseReturnLines != null && _locPurchaseReturnLines.Count() > 0)
                        {
                            if (_locPurchaseReturnXPO.PurchaseOrder != null && _locPurchaseReturnXPO != null)
                            {
                                _locCreditMemo = _currSession.FindObject<CreditMemo>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseReturn", _locPurchaseReturnXPO),
                                                            new BinaryOperator("PurchaseOrder", _locPurchaseReturnXPO.PurchaseOrder)));
                            }
                            else
                            {
                                _locCreditMemo = _currSession.FindObject<CreditMemo>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseReturn", _locPurchaseReturnXPO)));
                            }


                            if (_locCreditMemo != null)
                            {
                                #region UpdateCreditMemo

                                foreach (PurchaseReturnLine _locPurchaseReturnLine in _locPurchaseReturnLines)
                                {
                                    if (_locPurchaseReturnLine.Status == Status.Progress || _locPurchaseReturnLine.Status == Status.Posted)
                                    {
                                        if (_locPurchaseReturnLine.Select == true)
                                        {
                                            CreditMemoLine _locCreditMemoLine = _currSession.FindObject<CreditMemoLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("CreditMemo", _locCreditMemo),
                                                                                    new BinaryOperator("Item", _locPurchaseReturnLine.Item)));
                                                                                    //new BinaryOperator("Location", _locPurchaseReturnLine.Location),
                                                                                    //new BinaryOperator("BinLocation", _locPurchaseReturnLine.BinLocation)));
                                            if (_locCreditMemoLine == null)
                                            {
                                                if (_locPurchaseReturnXPO.PurchaseOrder != null && _locPurchaseReturnLine.Item != null)
                                                {
                                                    //_locUnitPrice = GetUnitPriceByPurchaseOrder(_currSession, _locPurchaseReturnXPO.PurchaseOrder, _locPurchaseReturnLine.Item);
                                                }

                                                CreditMemoLine _locSaveDataCreditMemoLine = new CreditMemoLine(_currSession)
                                                {
                                                    Item = _locPurchaseReturnLine.Item,
                                                    Description = _locPurchaseReturnLine.Description,
                                                    MxDQty = _locPurchaseReturnLine.DQty,
                                                    MxDUOM = _locPurchaseReturnLine.DUOM,
                                                    MxQty = _locPurchaseReturnLine.Qty,
                                                    MxUOM = _locPurchaseReturnLine.UOM,
                                                    DQty = _locPurchaseReturnLine.DQty,
                                                    DUOM = _locPurchaseReturnLine.DUOM,
                                                    Qty = _locPurchaseReturnLine.Qty,
                                                    UOM = _locPurchaseReturnLine.UOM,
                                                    TQty = _locPurchaseReturnLine.TQty,
                                                    UAmount = _locUnitPrice,
                                                    TUAmount = _locUnitPrice * _locPurchaseReturnLine.TQty,
                                                    //Location = _locPurchaseReturnLine.Location,
                                                    //BinLocation = _locPurchaseReturnLine.BinLocation,
                                                    //StockType = _locPurchaseReturnLine.StockType,
                                                    CreditMemo = _locCreditMemo,
                                                };
                                                _locSaveDataCreditMemoLine.Save();
                                                _locSaveDataCreditMemoLine.Session.CommitTransaction();
                                            }
                                        }
                                    }
                                }

                                #endregion UpdateCreditMemo
                            }
                            else
                            {
                                #region CreateNewCreditMemo

                                _locSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.CreditMemo);

                                if (_locSignCode != null)
                                {
                                    CreditMemo _locSaveDataCreditMemo = new CreditMemo(_currSession)
                                    {
                                        BuyFromVendor = _locPurchaseReturnXPO.BuyFromVendor,
                                        BuyFromAddress = _locPurchaseReturnXPO.BuyFromAddress,
                                        BuyFromCity = _locPurchaseReturnXPO.BuyFromCity,
                                        BuyFromContact = _locPurchaseReturnXPO.BuyFromContact,
                                        BuyFromCountry = _locPurchaseReturnXPO.BuyFromCountry,
                                        TOP = _locPurchaseReturnXPO.PurchaseOrder.TOP,
                                        SignCode = _locSignCode,
                                        PurchaseReturn = _locPurchaseReturnXPO,
                                        PurchaseOrder = _locPurchaseReturnXPO.PurchaseOrder,

                                    };
                                    _locSaveDataCreditMemo.Save();
                                    _locSaveDataCreditMemo.Session.CommitTransaction();

                                    CreditMemo _locCreditMemo2x = _currSession.FindObject<CreditMemo>
                                                                (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("SignCode", _locSignCode)));

                                    if (_locCreditMemo2x != null)
                                    {
                                        foreach (PurchaseReturnLine _locPurchaseReturnLine in _locPurchaseReturnLines)
                                        {
                                            if (_locPurchaseReturnLine.Status == Status.Progress || _locPurchaseReturnLine.Status == Status.Posted)
                                            {
                                                if (_locPurchaseReturnLine.Select == true)
                                                {
                                                    if (_locPurchaseReturnXPO.PurchaseOrder != null && _locPurchaseReturnLine.Item != null)
                                                    {
                                                        //_locUnitPrice = GetUnitPriceByPurchaseOrder(_currSession, _locPurchaseReturnXPO.PurchaseOrder, _locPurchaseReturnLine.Item);
                                                    }

                                                    CreditMemoLine _locSaveDataCreditMemoLine = new CreditMemoLine(_currSession)
                                                    {
                                                        Item = _locPurchaseReturnLine.Item,
                                                        Description = _locPurchaseReturnLine.Description,
                                                        MxDQty = _locPurchaseReturnLine.DQty,
                                                        MxDUOM = _locPurchaseReturnLine.DUOM,
                                                        MxQty = _locPurchaseReturnLine.Qty,
                                                        MxUOM = _locPurchaseReturnLine.UOM,
                                                        DQty = _locPurchaseReturnLine.DQty,
                                                        DUOM = _locPurchaseReturnLine.DUOM,
                                                        Qty = _locPurchaseReturnLine.Qty,
                                                        UOM = _locPurchaseReturnLine.UOM,
                                                        TQty = _locPurchaseReturnLine.TQty,
                                                        UAmount = _locUnitPrice,
                                                        TUAmount = _locUnitPrice * _locPurchaseReturnLine.TQty,
                                                        //Location = _locPurchaseReturnLine.Location,
                                                        //BinLocation = _locPurchaseReturnLine.BinLocation,
                                                        //StockType = _locPurchaseReturnLine.StockType,
                                                        CreditMemo = _locCreditMemo2x,
                                                    };
                                                    _locSaveDataCreditMemoLine.Save();
                                                    _locSaveDataCreditMemoLine.Session.CommitTransaction();

                                                }
                                            }
                                        }
                                    }
                                }

                                #endregion CreateNewInventoryTransfer
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseReturn " + ex.ToString());
            }
        }

        private void SetChangeStatusPurchaseReturnLineOld(Session _currSession, PurchaseReturn _locPurchaseReturnXPO)
        {
            try
            {
                DateTime now = DateTime.Now;

                if (_locPurchaseReturnXPO != null)
                {
                    XPCollection<PurchaseRequisitionLine> _locPurchaseReturnLines = new XPCollection<PurchaseRequisitionLine>
                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseReturn", _locPurchaseReturnXPO)));
                    if (_locPurchaseReturnLines != null && _locPurchaseReturnLines.Count() > 0)
                    {
                        foreach (PurchaseRequisitionLine _locPurchaseReturnLine in _locPurchaseReturnLines)
                        {
                            if (_locPurchaseReturnLine.Select == true)
                            {
                                if (_locPurchaseReturnLine.Status == Status.Progress || _locPurchaseReturnLine.Status == Status.Posted)
                                {
                                    _locPurchaseReturnLine.ActivationPosting = true;
                                    _locPurchaseReturnLine.Status = Status.Close;
                                    _locPurchaseReturnLine.StatusDate = now;
                                    _locPurchaseReturnLine.Save();
                                    _locPurchaseReturnLine.Session.CommitTransaction();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseReturn " + ex.ToString());
            }
        }

        private void SetFinalStatusOld(Session _currSession, PurchaseReturn _locPurchaseReturnXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locCountStatus = 0;

                if (_locPurchaseReturnXPO != null)
                {
                    XPCollection<PurchaseRequisitionLine> _locPurchaseReturnLines = new XPCollection<PurchaseRequisitionLine>
                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseReturn", _locPurchaseReturnXPO)));
                    if (_locPurchaseReturnLines != null && _locPurchaseReturnLines.Count() > 0)
                    {
                        foreach (PurchaseRequisitionLine _locPurchaseReturnLine in _locPurchaseReturnLines)
                        {
                            if (_locPurchaseReturnLine.Status != Status.Close)
                            {
                                _locCountStatus = _locCountStatus + 1;
                            }
                        }
                    }

                    if (_locCountStatus == 0)
                    {
                        _locPurchaseReturnXPO.Status = Status.Close;
                        _locPurchaseReturnXPO.StatusDate = now;
                        _locPurchaseReturnXPO.ActivationPosting = true;
                        _locPurchaseReturnXPO.Save();
                        _locPurchaseReturnXPO.Session.CommitTransaction();
                    }
                    else
                    {
                        _locPurchaseReturnXPO.Status = Status.Posted;
                        _locPurchaseReturnXPO.StatusDate = now;
                        _locPurchaseReturnXPO.Save();
                        _locPurchaseReturnXPO.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseReturn " + ex.ToString());
            }
        }

        private double GetUnitPriceByPurchaseOrderOld(Session _currSession, PurchaseOrder _locPurchaseOrder, Item _locItem)
        {
            double _result = 0;
            try
            {
                if (_locPurchaseOrder != null)
                {
                    PurchaseOrderLine _locPurchaseOrderLine = _currSession.FindObject<PurchaseOrderLine>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseOrder", _locPurchaseOrder),
                                                            new BinaryOperator("Item", _locItem)));
                    if (_locPurchaseOrderLine != null)
                    {
                        _result = _locPurchaseOrderLine.UAmount;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseReturn " + ex.ToString());
            }
            return _result;
        }

        #endregion Posting MethodOld

        #region Approval Process

        private void SetApprovalLine(Session _currentSession, PurchaseReturn _locPurchaseReturnXPO, ApprovalLevel _locApprovalLevel)
        {
            try
            {
                DateTime now = DateTime.Now;

                ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("PurchaseReturn", _locPurchaseReturnXPO),
                                                                         new BinaryOperator("ApprovalLevel", _locApprovalLevel)));

                if (_locApprovalLineXPO == null)
                {
                    ApprovalLine _saveDataAL2 = new ApprovalLine(_currentSession)
                    {
                        ApprovalDate = now,
                        ApprovalStatus = Status.Approved,
                        ApprovalLevel = _locApprovalLevel,
                        PurchaseReturn = _locPurchaseReturnXPO,
                    };
                    _saveDataAL2.Save();
                    _saveDataAL2.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseReturn " + ex.ToString());
            }
        }

        #endregion Approval Process

        #region Email

        private string BackgroundBody(Session _currentSession, PurchaseReturn _locPurchaseReturnXPO, MailSetupDetail _locMailSetDetail)
        {
            string body = string.Empty;

            if (_locPurchaseReturnXPO != null)
            {
                ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                               (new GroupOperator(GroupOperatorType.And,
                                                new BinaryOperator("PurchaseReturn", _locPurchaseReturnXPO)));

                string Status = _locApprovalLineXPO.ApprovalStatus.ToString();

                using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Email/EmailTemplate.html")))
                {
                    body = reader.ReadToEnd();
                }
                body = body.Replace("{Code}", _locPurchaseReturnXPO.Code);

                #region Level
                if (_locPurchaseReturnXPO.ActiveApproved1 == true)
                {
                    string Active = "Level 1";
                    body = body.Replace("{Level}", Active);
                }
                else if (_locPurchaseReturnXPO.ActiveApproved2 == true)
                {
                    string Active = "Level 2";
                    body = body.Replace("{Level}", Active);
                }
                else if (_locPurchaseReturnXPO.ActiveApproved3 == true)
                {
                    string Active = "Level 3";
                    body = body.Replace("{Level}", Active);
                }
                else
                {
                    string Active = "Not Available";
                    body = body.Replace("{Level}", Active);
                }
                #endregion Level

                body = body.Replace("{Status}", Status);
                body = body.Replace("{Message}", _locMailSetDetail.MessageBody);
            }

            return body;
        }

        protected void SendEmail(Session _currentSession, PurchaseReturn _locPurchaseReturnXPO, Status _locStatus, ApprovalLine _locApprovalLineXPO2)
        {
            DateTime now = DateTime.Now;
            var User = SecuritySystem.CurrentUserName;
            GlobalFunction _globFunc = new GlobalFunction();
            UserAccess _numLineUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", User));
            string _locMessageBody = null;

            try
            {
                if (_locPurchaseReturnXPO != null)
                {
                    XPCollection<MailSetupDetail> _locMailSetDetails = new XPCollection<MailSetupDetail>
                                                                       (_currentSession, new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("ObjectList", ObjectList.PurchaseReturn),
                                                                        new BinaryOperator("UserAccess", _numLineUserAccess),
                                                                        new BinaryOperator("Active", true)));

                    if (_locMailSetDetails != null && _locMailSetDetails.Count > 0)
                    {
                        foreach (MailSetupDetail _locMailSetDetail in _locMailSetDetails)
                        {
                            if (_locMailSetDetail.MessageBody != null)
                            {
                                if (_locApprovalLineXPO2.EndApproval == true)
                                {
                                    _locMessageBody = this.BackgroundBody(_currentSession, _locPurchaseReturnXPO, _locMailSetDetail);
                                }
                                else
                                {
                                    _locMessageBody = this.BackgroundBody(_currentSession, _locPurchaseReturnXPO, _locMailSetDetail);
                                }
                            }

                            _globFunc.SetAndSendMail(_locMailSetDetail.SmtpHost, _locMailSetDetail.SmtpPort,
                            _locMailSetDetail.MailFrom, _locMailSetDetail.MailFromPassword, _locMailSetDetail.MailTo, _locMailSetDetail.MailSubject, _locMessageBody);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseReturn " + ex.ToString());
            }
        }

        #endregion Email

        #region Global Method

        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0} !", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method
        
    }
}
