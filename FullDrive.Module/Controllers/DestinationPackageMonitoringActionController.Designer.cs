﻿namespace FullDrive.Module.Controllers
{
    partial class DestinationPackageMonitoringActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.DestinationPackageMonitoringSelectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.DestinationPackageMonitoringUnselectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // DestinationPackageMonitoringSelectAction
            // 
            this.DestinationPackageMonitoringSelectAction.Caption = "Select";
            this.DestinationPackageMonitoringSelectAction.ConfirmationMessage = null;
            this.DestinationPackageMonitoringSelectAction.Id = "DestinationPackageMonitoringSelectActionId";
            this.DestinationPackageMonitoringSelectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.DestinationPackageMonitoring);
            this.DestinationPackageMonitoringSelectAction.ToolTip = null;
            this.DestinationPackageMonitoringSelectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.DestinationPackageMonitoringSelectAction_Execute);
            // 
            // DestinationPackageMonitoringUnselectAction
            // 
            this.DestinationPackageMonitoringUnselectAction.Caption = "Unselect";
            this.DestinationPackageMonitoringUnselectAction.ConfirmationMessage = null;
            this.DestinationPackageMonitoringUnselectAction.Id = "DestinationPackageMonitoringUnselectActionId";
            this.DestinationPackageMonitoringUnselectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.DestinationPackageMonitoring);
            this.DestinationPackageMonitoringUnselectAction.ToolTip = null;
            this.DestinationPackageMonitoringUnselectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.DestinationPackageMonitoringUnselectAction_Execute);
            // 
            // DestinationPackageMonitoringActionController
            // 
            this.Actions.Add(this.DestinationPackageMonitoringSelectAction);
            this.Actions.Add(this.DestinationPackageMonitoringUnselectAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction DestinationPackageMonitoringSelectAction;
        private DevExpress.ExpressApp.Actions.SimpleAction DestinationPackageMonitoringUnselectAction;
    }
}
