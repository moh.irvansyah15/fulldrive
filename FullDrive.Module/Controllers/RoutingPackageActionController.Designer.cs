﻿namespace FullDrive.Module.Controllers
{
    partial class RoutingPackageActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.RoutingPackageSelectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.RoutingPackageUnselectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.RoutingPackageGetChargeableAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // RoutingPackageSelectAction
            // 
            this.RoutingPackageSelectAction.Caption = "Select";
            this.RoutingPackageSelectAction.ConfirmationMessage = null;
            this.RoutingPackageSelectAction.Id = "RoutingPackageSelectActionId";
            this.RoutingPackageSelectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.RoutingPackage);
            this.RoutingPackageSelectAction.ToolTip = null;
            this.RoutingPackageSelectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.RoutingPackageSelectAction_Execute);
            // 
            // RoutingPackageUnselectAction
            // 
            this.RoutingPackageUnselectAction.Caption = "Unselect";
            this.RoutingPackageUnselectAction.ConfirmationMessage = null;
            this.RoutingPackageUnselectAction.Id = "RoutingPackageUnselectActionId";
            this.RoutingPackageUnselectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.RoutingPackage);
            this.RoutingPackageUnselectAction.ToolTip = null;
            this.RoutingPackageUnselectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.RoutingPackageUnselectAction_Execute);
            // 
            // RoutingPackageGetChargeableAction
            // 
            this.RoutingPackageGetChargeableAction.Caption = "Get Chargeable";
            this.RoutingPackageGetChargeableAction.ConfirmationMessage = null;
            this.RoutingPackageGetChargeableAction.Id = "RoutingPackageGetChargeableActionId";
            this.RoutingPackageGetChargeableAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.RoutingPackage);
            this.RoutingPackageGetChargeableAction.ToolTip = null;
            this.RoutingPackageGetChargeableAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.RoutingPackageGetChargeableAction_Execute);
            // 
            // RoutingPackageActionController
            // 
            this.Actions.Add(this.RoutingPackageSelectAction);
            this.Actions.Add(this.RoutingPackageUnselectAction);
            this.Actions.Add(this.RoutingPackageGetChargeableAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction RoutingPackageSelectAction;
        private DevExpress.ExpressApp.Actions.SimpleAction RoutingPackageUnselectAction;
        private DevExpress.ExpressApp.Actions.SimpleAction RoutingPackageGetChargeableAction;
    }
}
