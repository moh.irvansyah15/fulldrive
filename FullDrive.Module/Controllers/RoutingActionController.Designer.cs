﻿namespace FullDrive.Module.Controllers
{
    partial class RoutingActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.RoutingProgressAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.RoutingPostingPackageAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.RoutingPostingRouteAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // RoutingProgressAction
            // 
            this.RoutingProgressAction.Caption = "Progress";
            this.RoutingProgressAction.ConfirmationMessage = null;
            this.RoutingProgressAction.Id = "RoutingProgressActionId";
            this.RoutingProgressAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.Routing);
            this.RoutingProgressAction.ToolTip = null;
            this.RoutingProgressAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.RoutingProgressAction_Execute);
            // 
            // RoutingPostingPackageAction
            // 
            this.RoutingPostingPackageAction.Caption = "Posting Package";
            this.RoutingPostingPackageAction.ConfirmationMessage = null;
            this.RoutingPostingPackageAction.Id = "RoutingPostingPackageActionId";
            this.RoutingPostingPackageAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.Routing);
            this.RoutingPostingPackageAction.ToolTip = null;
            this.RoutingPostingPackageAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.RoutingPostingPackageAction_Execute);
            // 
            // RoutingPostingRouteAction
            // 
            this.RoutingPostingRouteAction.Caption = "Posting Route";
            this.RoutingPostingRouteAction.ConfirmationMessage = null;
            this.RoutingPostingRouteAction.Id = "RoutingPostingRouteActionId";
            this.RoutingPostingRouteAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.Routing);
            this.RoutingPostingRouteAction.ToolTip = null;
            this.RoutingPostingRouteAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.RoutingPostingRouteAction_Execute);
            // 
            // RoutingActionController
            // 
            this.Actions.Add(this.RoutingProgressAction);
            this.Actions.Add(this.RoutingPostingPackageAction);
            this.Actions.Add(this.RoutingPostingRouteAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction RoutingProgressAction;
        private DevExpress.ExpressApp.Actions.SimpleAction RoutingPostingPackageAction;
        private DevExpress.ExpressApp.Actions.SimpleAction RoutingPostingRouteAction;
    }
}
