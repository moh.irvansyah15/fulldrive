﻿namespace FullDrive.Module.Controllers
{
    partial class ReturnPurchaseCollectionActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ReturnPurchaseCollectionShowITIMAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // ReturnPurchaseCollectionShowITIMAction
            // 
            this.ReturnPurchaseCollectionShowITIMAction.Caption = "Show ITIM";
            this.ReturnPurchaseCollectionShowITIMAction.ConfirmationMessage = null;
            this.ReturnPurchaseCollectionShowITIMAction.Id = "ReturnPurchaseCollectionShowITIMActionId";
            this.ReturnPurchaseCollectionShowITIMAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.ReturnPurchaseCollection);
            this.ReturnPurchaseCollectionShowITIMAction.ToolTip = null;
            this.ReturnPurchaseCollectionShowITIMAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.ReturnPurchaseCollectionShowITIMAction_Execute);
            // 
            // ReturnPurchaseCollectionActionController
            // 
            this.Actions.Add(this.ReturnPurchaseCollectionShowITIMAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction ReturnPurchaseCollectionShowITIMAction;
    }
}
