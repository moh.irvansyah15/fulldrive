﻿namespace FullDrive.Module.Controllers
{
    partial class TransferOrderActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.TransferOrderProgressAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.TransferOrderPostingAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.TransferOrderListviewFilterSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.TransferOrderListviewFilterApprovalSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.TransferOrderApprovalAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // TransferOrderProgressAction
            // 
            this.TransferOrderProgressAction.Caption = "Progress";
            this.TransferOrderProgressAction.ConfirmationMessage = null;
            this.TransferOrderProgressAction.Id = "TransferOrderProgressActionId";
            this.TransferOrderProgressAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.TransferOrder);
            this.TransferOrderProgressAction.ToolTip = null;
            this.TransferOrderProgressAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.TransferOrderProgressAction_Execute);
            // 
            // TransferOrderPostingAction
            // 
            this.TransferOrderPostingAction.Caption = "Posting";
            this.TransferOrderPostingAction.ConfirmationMessage = null;
            this.TransferOrderPostingAction.Id = "TransferOrderPostingActionId";
            this.TransferOrderPostingAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.TransferOrder);
            this.TransferOrderPostingAction.ToolTip = null;
            this.TransferOrderPostingAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.TransferOrderPostingAction_Execute);
            // 
            // TransferOrderListviewFilterSelectionAction
            // 
            this.TransferOrderListviewFilterSelectionAction.Caption = "Filter";
            this.TransferOrderListviewFilterSelectionAction.ConfirmationMessage = null;
            this.TransferOrderListviewFilterSelectionAction.Id = "TransferOrderListviewFilterSelectionActionId";
            this.TransferOrderListviewFilterSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.TransferOrderListviewFilterSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.TransferOrder);
            this.TransferOrderListviewFilterSelectionAction.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.TransferOrderListviewFilterSelectionAction.ToolTip = null;
            this.TransferOrderListviewFilterSelectionAction.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.TransferOrderListviewFilterSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.TransferOrderListviewFilterSelectionAction_Execute);
            // 
            // TransferOrderListviewFilterApprovalSelectionAction
            // 
            this.TransferOrderListviewFilterApprovalSelectionAction.Caption = "Filter Approval";
            this.TransferOrderListviewFilterApprovalSelectionAction.Category = "View";
            this.TransferOrderListviewFilterApprovalSelectionAction.ConfirmationMessage = null;
            this.TransferOrderListviewFilterApprovalSelectionAction.Id = "TransferOrderListviewFilterApprovalSelectionActionId";
            this.TransferOrderListviewFilterApprovalSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.TransferOrderListviewFilterApprovalSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.TransferOrder);
            this.TransferOrderListviewFilterApprovalSelectionAction.ToolTip = null;
            this.TransferOrderListviewFilterApprovalSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.TransferOrderListviewFilterApprovalSelectionAction_Execute);
            // 
            // TransferOrderApprovalAction
            // 
            this.TransferOrderApprovalAction.Caption = "Approval";
            this.TransferOrderApprovalAction.ConfirmationMessage = null;
            this.TransferOrderApprovalAction.Id = "TransferOrderApprovalActionId";
            this.TransferOrderApprovalAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.TransferOrderApprovalAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.TransferOrder);
            this.TransferOrderApprovalAction.ToolTip = null;
            this.TransferOrderApprovalAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.TransferOrderApprovalAction_Execute);
            // 
            // TransferOrderActionController
            // 
            this.Actions.Add(this.TransferOrderProgressAction);
            this.Actions.Add(this.TransferOrderPostingAction);
            this.Actions.Add(this.TransferOrderListviewFilterSelectionAction);
            this.Actions.Add(this.TransferOrderListviewFilterApprovalSelectionAction);
            this.Actions.Add(this.TransferOrderApprovalAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction TransferOrderProgressAction;
        private DevExpress.ExpressApp.Actions.SimpleAction TransferOrderPostingAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction TransferOrderListviewFilterSelectionAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction TransferOrderListviewFilterApprovalSelectionAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction TransferOrderApprovalAction;
    }
}
