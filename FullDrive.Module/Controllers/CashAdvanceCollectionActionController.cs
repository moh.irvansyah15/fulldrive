﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using System.Collections;
using DevExpress.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class CashAdvanceCollectionActionController : ViewController
    {
        public CashAdvanceCollectionActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void CashAdvanceCollectionShowCAMAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
            var ListView = View as ListView;
            IObjectSpace os = Application.CreateObjectSpace();
            string listViewId = Application.FindListViewId(typeof(CashAdvanceMonitoring));
            CollectionSourceBase cs = Application.CreateCollectionSource(os, typeof(CashAdvanceMonitoring), listViewId);
            //Tinggal buat 
            ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
            string _stringCAM = null;
            if (_objectsToProcess != null)
            {
                foreach (Object obj in _objectsToProcess)
                {
                    CashAdvanceCollection _locCashAdvanceCollection = (CashAdvanceCollection)_objectSpace.GetObject(obj);
                    if (_locCashAdvanceCollection != null)
                    {
                        if (_locCashAdvanceCollection.CashAdvance != null)
                        {
                            if (_stringCAM == null)
                            {
                                if (_locCashAdvanceCollection.CashAdvance.Code != null)
                                {
                                    _stringCAM = "( [CashAdvance.Code]=='" + _locCashAdvanceCollection.CashAdvance.Code + "' AND [Status] != 4 )";
                                }

                            }
                            else
                            {
                                if (_locCashAdvanceCollection.CashAdvance.Code != null)
                                {
                                    _stringCAM = _stringCAM + " OR ( [CashAdvance.Code]=='" + _locCashAdvanceCollection.CashAdvance.Code + "' AND [Status] != 4 )";
                                }
                            }
                        }
                    }
                }
            }
            if (_stringCAM != null)
            {
                cs.Criteria["CashAdvanceCollectionFilterCA"] = CriteriaOperator.Parse(_stringCAM);
            }
            e.ShowViewParameters.CreatedView = Application.CreateListView(
                listViewId,
                cs,
                true
            );
            e.ShowViewParameters.Context = TemplateContext.View;
            e.ShowViewParameters.TargetWindow = TargetWindow.NewModalWindow;
            DialogController dc = Application.CreateController<DialogController>();
            dc.Accepting += new EventHandler<DialogControllerAcceptingEventArgs>(dc_Accepting);
            e.ShowViewParameters.Controllers.Add(dc);
        }

        void dc_Accepting(object sender, DialogControllerAcceptingEventArgs e)
        {
            try
            {
                PaymentRealization _locPaymentRealization = null;
                View popupView = ((Controller)sender).Frame.View;
                ArrayList _objectsToProcess = new ArrayList(View.SelectedObjects);
                if (_objectsToProcess != null)
                {
                    foreach (Object _obj in _objectsToProcess)
                    {
                        CashAdvanceCollection _locCashAdvanceCollection = (CashAdvanceCollection)popupView.ObjectSpace.GetObject(_obj);
                        if (_locCashAdvanceCollection != null)
                        {
                            if (_locCashAdvanceCollection.PaymentRealization != null)
                            {
                                _locPaymentRealization = _locCashAdvanceCollection.PaymentRealization;
                            }
                        }
                    }
                }
                if (_locPaymentRealization != null)
                {
                    ArrayList _objectsToProcess2 = new ArrayList(popupView.SelectedObjects);
                    if (_objectsToProcess2 != null)
                    {
                        foreach (Object _obj2 in _objectsToProcess2)
                        {
                            CashAdvanceMonitoring _locCashAdvanceMonitoring = (CashAdvanceMonitoring)popupView.ObjectSpace.GetObject(_obj2);
                            if (_locCashAdvanceMonitoring != null && _locCashAdvanceMonitoring.Select == true && _locCashAdvanceMonitoring.Status != Status.Close)
                            {
                                _locCashAdvanceMonitoring.PaymentRealization = _locPaymentRealization;
                            }
                        }
                    }
                }
                popupView.ObjectSpace.CommitChanges();
            }
            catch (ValidationException)
            {
            }
        }
    }
}
