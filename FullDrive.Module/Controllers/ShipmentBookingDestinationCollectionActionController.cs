﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using System.Collections;
using DevExpress.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class ShipmentBookingDestinationCollectionActionController : ViewController
    {
        public ShipmentBookingDestinationCollectionActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void ShipmentBookingDestinationCollectionShowSBMAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
            var ListView = View as ListView;
            IObjectSpace os = Application.CreateObjectSpace();
            string listViewId = Application.FindListViewId(typeof(ShipmentBookingMonitoring));
            CollectionSourceBase cs = Application.CreateCollectionSource(os, typeof(ShipmentBookingMonitoring), listViewId);
            //Tinggal buat 
            ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
            string _stringSB = null;
            if (_objectsToProcess != null)
            {
                foreach (Object obj in _objectsToProcess)
                {
                    ShipmentBookingDestinationCollection _locShipmentBookingDestinationCollection = (ShipmentBookingDestinationCollection)_objectSpace.GetObject(obj);
                    if (_locShipmentBookingDestinationCollection != null)
                    {
                        if (_locShipmentBookingDestinationCollection.ShipmentBooking != null)
                        {
                            if (_stringSB == null)
                            {
                                if (_locShipmentBookingDestinationCollection.ShipmentBooking.Code != null)
                                {
                                    _stringSB = "( [ShipmentBooking.Code]=='" + _locShipmentBookingDestinationCollection.ShipmentBooking.Code + "' )";
                                }

                            }
                            else
                            {
                                if (_locShipmentBookingDestinationCollection.ShipmentBooking.Code != null)
                                {
                                    _stringSB = _stringSB + " OR ( [ShipmentBooking.Code]=='" + _locShipmentBookingDestinationCollection.ShipmentBooking.Code + "' )";
                                }
                            }
                        }
                    }
                }
            }
            if (_stringSB != null)
            {
                cs.Criteria["ShipmentBookingDestinationCollectionFilter"] = CriteriaOperator.Parse(_stringSB);
            }
            e.ShowViewParameters.CreatedView = Application.CreateListView(
                listViewId,
                cs,
                true
            );
            e.ShowViewParameters.Context = TemplateContext.View;
            e.ShowViewParameters.TargetWindow = TargetWindow.NewModalWindow;
            DialogController dc = Application.CreateController<DialogController>();
            dc.Accepting += new EventHandler<DialogControllerAcceptingEventArgs>(dc_Accepting);
            e.ShowViewParameters.Controllers.Add(dc);
        }

        void dc_Accepting(object sender, DialogControllerAcceptingEventArgs e)
        {
            try
            {
                FreightForwardingDestination _locFreightForwardingDestination = null;
                View popupView = ((Controller)sender).Frame.View;
                ArrayList _objectsToProcess = new ArrayList(View.SelectedObjects);
                if (_objectsToProcess != null)
                {
                    foreach (Object _obj in _objectsToProcess)
                    {
                        ShipmentBookingDestinationCollection _locShipmentBookingDestinationCollection = (ShipmentBookingDestinationCollection)popupView.ObjectSpace.GetObject(_obj);
                        if (_locShipmentBookingDestinationCollection != null)
                        {
                            if (_locShipmentBookingDestinationCollection.FreightForwardingDestination != null)
                            {
                                _locFreightForwardingDestination = _locShipmentBookingDestinationCollection.FreightForwardingDestination;
                            }
                        }
                    }
                }
                if (_locFreightForwardingDestination != null)
                {
                    ArrayList _objectsToProcess2 = new ArrayList(popupView.SelectedObjects);
                    if (_objectsToProcess2 != null)
                    {
                        foreach (Object _obj2 in _objectsToProcess2)
                        {
                            ShipmentBookingMonitoring _locShipBookingMonitoring = (ShipmentBookingMonitoring)popupView.ObjectSpace.GetObject(_obj2);
                            if (_locShipBookingMonitoring != null && _locShipBookingMonitoring.Select == true && (_locShipBookingMonitoring.Status == Status.Open || _locShipBookingMonitoring.Status == Status.Posted))
                            {
                                _locShipBookingMonitoring.FreightForwardingDestination = _locFreightForwardingDestination;
                            }
                        }
                    }
                }
                popupView.ObjectSpace.CommitChanges();
            }
            catch (ValidationException)
            {
            }
        }
    }
}
