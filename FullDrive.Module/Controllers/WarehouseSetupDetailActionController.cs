﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using System.Collections;
using DevExpress.Xpo;
using DevExpress.ExpressApp.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class WarehouseSetupDetailActionController : ViewController
    {
        public WarehouseSetupDetailActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }

        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }

        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }

        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void WarehouseSetupDetailGetPreviousAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                ApplicationSetup _locApplicationSetupXPO = null;
                string _locApplicationSetupCode = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        WarehouseSetupDetail _locWarehouseSetupDetailOS = (WarehouseSetupDetail)_objectSpace.GetObject(obj);


                        if (_locWarehouseSetupDetailOS != null)
                        {
                            if (_locWarehouseSetupDetailOS.ApplicationSetup != null)
                            {
                                if (_locWarehouseSetupDetailOS.ApplicationSetup.SetupCode != null)
                                {
                                    _locApplicationSetupCode = _locWarehouseSetupDetailOS.ApplicationSetup.SetupCode;  
                                }
                            }

                            if (_locWarehouseSetupDetailOS.Code != null)
                            {
                                _currObjectId = _locWarehouseSetupDetailOS.Code;

                                WarehouseSetupDetail _locWarehouseSetupDetailXPO = _currSession.FindObject<WarehouseSetupDetail>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locWarehouseSetupDetailXPO == null)
                                {
                                    if (_locApplicationSetupCode != null)
                                    {
                                        _locApplicationSetupXPO = _currSession.FindObject<ApplicationSetup>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("SetupCode", _locApplicationSetupCode)));
                                    }

                                    if (_locApplicationSetupXPO != null)
                                    {
                                        XPCollection<WarehouseSetupDetail> _numLines = new XPCollection<WarehouseSetupDetail>
                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("ApplicationSetup", _locApplicationSetupXPO)),
                                                                            new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));

                                        if (_numLines != null && _numLines.Count > 0)
                                        {
                                            foreach (WarehouseSetupDetail _numLine in _numLines)
                                            {
                                                if (_numLine.No == _numLines.Count())
                                                {
                                                    _locWarehouseSetupDetailOS.UserAccess = _numLine.UserAccess;
                                                    _locWarehouseSetupDetailOS.Company = _numLine.Company;
                                                    _locWarehouseSetupDetailOS.Division = _numLine.Division;
                                                    _locWarehouseSetupDetailOS.Department = _numLine.Department;
                                                    _locWarehouseSetupDetailOS.Section = _numLine.Section;
                                                    _locWarehouseSetupDetailOS.ApplicationSetup = _numLine.ApplicationSetup;
                                                    _locWarehouseSetupDetailOS.Location = _numLine.Location;
                                                    _locWarehouseSetupDetailOS.BinLocation = _numLine.BinLocation;
                                                    _locWarehouseSetupDetailOS.Owner = _numLine.Owner;
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data Warehouse Setup Detail Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Warehouse Setup Detail Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = WarehouseSetupDetail " + ex.ToString());
            }
        }

        #region Global Method

        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0}", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method

    }
}
