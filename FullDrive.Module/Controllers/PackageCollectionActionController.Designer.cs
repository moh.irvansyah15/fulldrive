﻿namespace FullDrive.Module.Controllers
{
    partial class PackageCollectionActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.PackageCollectionShowOPMAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PackageCollectionShowDPMAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // PackageCollectionShowOPMAction
            // 
            this.PackageCollectionShowOPMAction.Caption = "Show OPM";
            this.PackageCollectionShowOPMAction.ConfirmationMessage = null;
            this.PackageCollectionShowOPMAction.Id = "PackageCollectionShowOPMActionId";
            this.PackageCollectionShowOPMAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PackageCollection);
            this.PackageCollectionShowOPMAction.ToolTip = null;
            this.PackageCollectionShowOPMAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PackageCollectionShowOPMAction_Execute);
            // 
            // PackageCollectionShowDPMAction
            // 
            this.PackageCollectionShowDPMAction.Caption = "Show DPM";
            this.PackageCollectionShowDPMAction.ConfirmationMessage = null;
            this.PackageCollectionShowDPMAction.Id = "PackageCollectionShowDPMActionId";
            this.PackageCollectionShowDPMAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PackageCollection);
            this.PackageCollectionShowDPMAction.ToolTip = null;
            this.PackageCollectionShowDPMAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PackageCollectionShowDPMAction_Execute);
            // 
            // PackageCollectionActionController
            // 
            this.Actions.Add(this.PackageCollectionShowOPMAction);
            this.Actions.Add(this.PackageCollectionShowDPMAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction PackageCollectionShowOPMAction;
        private DevExpress.ExpressApp.Actions.SimpleAction PackageCollectionShowDPMAction;
    }
}
