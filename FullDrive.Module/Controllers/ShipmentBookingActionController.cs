﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using FullDrive.Module.BusinessObjects;
using DevExpress.ExpressApp.Xpo;
using FullDrive.Module.CustomProcess;
using System.Collections;
using DevExpress.Xpo;

using System.Web;
using System.IO;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class ShipmentBookingActionController : ViewController
    {
        private ChoiceActionItem _selectionListviewFilter;
        private ChoiceActionItem _selectionApprovalListviewFilter;
        private ChoiceActionItem _setApprovalLevel;

        public ShipmentBookingActionController()
        {
            InitializeComponent();
            #region FilterStatus
            ShipmentBookingListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                ShipmentBookingListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
            #endregion FilterStatus

            #region FilterApproval
            ShipmentBookingListviewFilterApprovalSelectionAction.Items.Clear();
            foreach (object _currApprovalFilter in Enum.GetValues(typeof(CustomProcess.ApprovalFilter)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.ApprovalFilter));
                _selectionApprovalListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApprovalFilter), _currApprovalFilter);
                ShipmentBookingListviewFilterApprovalSelectionAction.Items.Add(_selectionApprovalListviewFilter);
            }
            #endregion FilterApproval
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }

        protected override void OnActivated()
        {
            TypeOfView = typeof(ObjectView);
            base.OnActivated();
            // Perform various tasks depending on the target View.
            #region Approval

            var _locUser = SecuritySystem.CurrentUserName;
            GlobalFunction _globalFunction = new GlobalFunction();
            Session _currentSession = null;
            if (this.ObjectSpace != null)
            {
                _currentSession = ((XPObjectSpace)this.ObjectSpace).Session;
            }

            if (_currentSession != null)
            {
                UserAccess _locUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", _locUser));

                if (_locUserAccess != null)
                {
                    ShipmentBookingApprovalAction.Items.Clear();

                    XPCollection<ApplicationSetupDetail> _locAppSetupDetails = new XPCollection<ApplicationSetupDetail>
                                                                                 (_currentSession, new GroupOperator(GroupOperatorType.And,
                                                                                  new BinaryOperator("UserAccess", _locUserAccess),
                                                                                  new BinaryOperator("ObjectList", CustomProcess.ObjectList.ShipmentBooking),
                                                                                  new BinaryOperator("FunctionList", CustomProcess.FunctionList.Approval),
                                                                                  new BinaryOperator("Active", true)));

                    if (_locAppSetupDetails != null && _locAppSetupDetails.Count > 0)
                    {

                        foreach (ApplicationSetupDetail _locAppSetupDetail in _locAppSetupDetails)
                        {
                            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.ApprovalLevel)))
                            {
                                if (_locAppSetupDetail.ApprovalLevel != CustomProcess.ApprovalLevel.None)
                                {
                                    EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.ApprovalLevel));
                                    _setApprovalLevel = new ChoiceActionItem(_ed.GetCaption(_locAppSetupDetail.ApprovalLevel), _locAppSetupDetail.ApprovalLevel);
                                }
                            }
                            ShipmentBookingApprovalAction.Items.Add(_setApprovalLevel);
                        }
                    }
                }
            }
            #endregion Approval
        }

        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }

        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void ShipmentBookingProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        ShipmentBooking _locShipmentBookingOS = (ShipmentBooking)_objectSpace.GetObject(obj);

                        if (_locShipmentBookingOS != null)
                        {
                            if (_locShipmentBookingOS.Code != null)
                            {
                                _currObjectId = _locShipmentBookingOS.Code;

                                ShipmentBooking _locShipmentBookingXPO = _currSession.FindObject<ShipmentBooking>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locShipmentBookingXPO != null)
                                {
                                    #region Status
                                    if (_locShipmentBookingXPO.Status == Status.Open || _locShipmentBookingXPO.Status == Status.Progress || _locShipmentBookingXPO.Status == Status.Posted)
                                    {
                                        if (_locShipmentBookingXPO.Status == Status.Open)
                                        {
                                            _locShipmentBookingXPO.Status = Status.Progress;
                                            _locShipmentBookingXPO.StatusDate = now;
                                            _locShipmentBookingXPO.Save();
                                            _locShipmentBookingXPO.Session.CommitTransaction();
                                        }

                                        XPCollection<ShipmentBookingLine> _locShipmentBookingLines = new XPCollection<ShipmentBookingLine>
                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("ShipmentBooking", _locShipmentBookingXPO),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Open),
                                                            new BinaryOperator("Status", Status.Progress))
                                                            ));

                                        if (_locShipmentBookingLines != null && _locShipmentBookingLines.Count > 0)
                                        {
                                            foreach (ShipmentBookingLine _locShipmentBookingLine in _locShipmentBookingLines)
                                            {
                                                _locShipmentBookingLine.Status = Status.Progress;
                                                _locShipmentBookingLine.StatusDate = now;
                                                _locShipmentBookingLine.Save();
                                                _locShipmentBookingLine.Session.CommitTransaction();
                                            }
                                        } 
                                    }
                                    #endregion Status

                                    SuccessMessageShow("ShipmentBooking has successfully updated to progress");
                                }
                                else
                                {
                                    ErrorMessageShow("Data ShipmentBooking Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data ShipmentBooking Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesQuotation " + ex.ToString());
            }
        }

        private void ShipmentBookingApprovalAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                ApplicationSetupDetail _locAppSetDetail = null;
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                var User = SecuritySystem.CurrentUserName;
                Session _currentSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currentSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                UserAccess _locUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", User));

                foreach (Object obj in _objectsToProcess)
                {
                    ShipmentBooking _objInNewObjectSpace = (ShipmentBooking)_objectSpace.GetObject(obj);

                    if (_objInNewObjectSpace != null)
                    {
                        if (_objInNewObjectSpace.Code != null)
                        {
                            _currObjectId = _objInNewObjectSpace.Code;
                        }
                    }

                    if (_currObjectId != null)
                    {
                        ShipmentBooking _locShipmentBookingXPO = _currentSession.FindObject<ShipmentBooking>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("Code", _currObjectId)));

                        if (_locShipmentBookingXPO != null)
                        {
                            if (_locShipmentBookingXPO.Status == Status.Progress)
                            {
                                ShipmentApprovalLine _locShipmentApprovalLine = _currentSession.FindObject<ShipmentApprovalLine>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("ShipmentBooking", _locShipmentBookingXPO),
                                                                             new BinaryOperator("EndApproval", true)));
                                if (_locShipmentApprovalLine == null)
                                {
                                    #region Approval Level 1
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level1)
                                    {
                                        _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level1, ObjectList.ShipmentBooking);

                                        if (_locAppSetDetail != null)
                                        {
                                            //Buat bs input langsung ke approvalline
                                            ShipmentApprovalLine _locShipmentApprovalLineXPO = _currentSession.FindObject<ShipmentApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("ShipmentBooking", _locShipmentBookingXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level1)));
                                            if (_locShipmentApprovalLineXPO == null)
                                            {

                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ShipmentApprovalLine _saveDataAL = new ShipmentApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level1,
                                                        EndApproval = true,
                                                        ShipmentBooking = _locShipmentBookingXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locShipmentBookingXPO.ActivationPosting = true;
                                                    _locShipmentBookingXPO.ActiveApproved1 = false;
                                                    _locShipmentBookingXPO.ActiveApproved2 = false;
                                                    _locShipmentBookingXPO.ActiveApproved3 = true;
                                                    _locShipmentBookingXPO.Save();
                                                    _locShipmentBookingXPO.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ShipmentApprovalLine _saveDataAL = new ShipmentApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level1,
                                                        ShipmentBooking = _locShipmentBookingXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locShipmentBookingXPO.ActiveApproved1 = true;
                                                    _locShipmentBookingXPO.ActiveApproved2 = false;
                                                    _locShipmentBookingXPO.ActiveApproved3 = false;
                                                    _locShipmentBookingXPO.Save();
                                                    _locShipmentBookingXPO.Session.CommitTransaction();
                                                }

                                                //Send Email
                                                ShipmentApprovalLine _locShipmentApprovalLineXPO2 = _currentSession.FindObject<ShipmentApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("ShipmentBooking", _locShipmentBookingXPO)));
                                                if (_locShipmentApprovalLineXPO2 != null)
                                                {
                                                    SendEmail(_currentSession, _locShipmentBookingXPO, Status.Progress, _locShipmentApprovalLineXPO2);
                                                }

                                                SuccessMessageShow("ShipmentBooking has successfully Approve");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 1

                                    #region Approval Level 2
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level2)
                                    {
                                        _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level2, ObjectList.ShipmentBooking);

                                        if (_locAppSetDetail != null)
                                        {
                                            ShipmentApprovalLine _locShipmentApprovalLineXPO = _currentSession.FindObject<ShipmentApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("ShipmentBooking", _locShipmentBookingXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level2)));

                                            if (_locShipmentApprovalLineXPO == null)
                                            {
                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ShipmentApprovalLine _saveDataAL = new ShipmentApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level2,
                                                        EndApproval = true,
                                                        ShipmentBooking = _locShipmentBookingXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locShipmentBookingXPO.ActivationPosting = true;
                                                    _locShipmentBookingXPO.ActiveApproved1 = false;
                                                    _locShipmentBookingXPO.ActiveApproved2 = false;
                                                    _locShipmentBookingXPO.ActiveApproved3 = true;
                                                    _locShipmentBookingXPO.Save();
                                                    _locShipmentBookingXPO.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ShipmentApprovalLine _saveDataAL = new ShipmentApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level2,
                                                        ShipmentBooking = _locShipmentBookingXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    SetApprovalLine(_currentSession, _locShipmentBookingXPO, ApprovalLevel.Level1);

                                                    _locShipmentBookingXPO.ActiveApproved1 = false;
                                                    _locShipmentBookingXPO.ActiveApproved2 = true;
                                                    _locShipmentBookingXPO.ActiveApproved3 = false;
                                                    _locShipmentBookingXPO.Save();
                                                    _locShipmentBookingXPO.Session.CommitTransaction();
                                                }

                                                //Send Email
                                                ShipmentApprovalLine _locShipmentApprovalLineXPO2 = _currentSession.FindObject<ShipmentApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("ShipmentBooking", _locShipmentBookingXPO)));
                                                if (_locShipmentApprovalLineXPO2 != null)
                                                {
                                                    SendEmail(_currentSession, _locShipmentBookingXPO, Status.Progress, _locShipmentApprovalLineXPO2);
                                                }

                                                SuccessMessageShow("ShipmentBooking has successfully Approve");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 2

                                    #region Approval Level 3
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level3)
                                    {
                                        _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level3, ObjectList.ShipmentBooking);

                                        if (_locAppSetDetail != null)
                                        {
                                            ShipmentApprovalLine _locShipmentApprovalLineXPO = _currentSession.FindObject<ShipmentApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("ShipmentBooking", _locShipmentBookingXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level3)));

                                            if (_locShipmentApprovalLineXPO == null)
                                            {
                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ShipmentApprovalLine _saveDataAL = new ShipmentApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level3,
                                                        EndApproval = true,
                                                        ShipmentBooking = _locShipmentBookingXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locShipmentBookingXPO.ActivationPosting = true;
                                                    _locShipmentBookingXPO.ActiveApproved1 = false;
                                                    _locShipmentBookingXPO.ActiveApproved2 = false;
                                                    _locShipmentBookingXPO.ActiveApproved3 = true;
                                                    _locShipmentBookingXPO.Save();
                                                    _locShipmentBookingXPO.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ShipmentApprovalLine _saveDataAL = new ShipmentApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level3,
                                                        ShipmentBooking = _locShipmentBookingXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    SetApprovalLine(_currentSession, _locShipmentBookingXPO, ApprovalLevel.Level2);
                                                    SetApprovalLine(_currentSession, _locShipmentBookingXPO, ApprovalLevel.Level1);

                                                    _locShipmentBookingXPO.ActiveApproved1 = false;
                                                    _locShipmentBookingXPO.ActiveApproved2 = false;
                                                    _locShipmentBookingXPO.ActiveApproved3 = true;
                                                    _locShipmentBookingXPO.Save();
                                                    _locShipmentBookingXPO.Session.CommitTransaction();
                                                }

                                                //Send Email
                                                ShipmentApprovalLine _locShipmentApprovalLineXPO2 = _currentSession.FindObject<ShipmentApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("ShipmentBooking", _locShipmentBookingXPO)));
                                                if (_locShipmentApprovalLineXPO2 != null)
                                                {
                                                    SendEmail(_currentSession, _locShipmentBookingXPO, Status.Progress, _locShipmentApprovalLineXPO2);
                                                }

                                                SuccessMessageShow("ShipmentBooking has successfully Approve");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 3
                                }

                            }
                            else
                            {
                                ErrorMessageShow("ShipmentBooking Status Not Available");
                            }
                        }
                        else
                        {
                            ErrorMessageShow("ShipmentBooking Not Available");
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ShipmentBooking " + ex.ToString());
            }
        }

        private void ShipmentBookingPostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        ShipmentBooking _locShipmentBookingOS = (ShipmentBooking)_objectSpace.GetObject(obj);

                        if (_locShipmentBookingOS != null)
                        {
                            if (_locShipmentBookingOS.Code != null)
                            {
                                _currObjectId = _locShipmentBookingOS.Code;

                                ShipmentBooking _locShipmentBookingXPO = _currSession.FindObject<ShipmentBooking>
                                                                       (new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Code", _currObjectId)));

                                if (_locShipmentBookingXPO != null)
                                {
                                    if (_locShipmentBookingXPO.Status == Status.Progress || _locShipmentBookingXPO.Status == Status.Posted)
                                    {
                                        ShipmentApprovalLine _locShipmentApprovalLineXPO = _currSession.FindObject<ShipmentApprovalLine>
                                                                           (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("EndApproval", true),
                                                                            new BinaryOperator("ShipmentBooking", _locShipmentBookingXPO)));
                                        if (_locShipmentApprovalLineXPO != null)
                                        {
                                            SetShipmentBookingMonitoring(_currSession, _locShipmentBookingXPO);
                                            SetRemainQty(_currSession, _locShipmentBookingXPO);
                                            SetPostingQty(_currSession, _locShipmentBookingXPO);
                                            SetProcessCount(_currSession, _locShipmentBookingXPO);
                                            SetStatusShipmentBookingLine(_currSession, _locShipmentBookingXPO);
                                            SetNormalQuantity(_currSession, _locShipmentBookingXPO);
                                            SetFinalStatusShipmentBooking(_currSession, _locShipmentBookingXPO);
                                            SuccessMessageShow("ShipmentBooking has successfully posted into Sales Booking Monitoring");
                                        }
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data ShipmentBooking Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data ShipmentBooking Not Available");
                            }
                        }
                    }
                }
                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ShipmentBooking " + ex.ToString());
            }
        }

        private void ShipmentBookingListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(ShipmentBooking)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ShipmentBooking " + ex.ToString());
            }
        }

        private void ShipmentBookingListviewFilterApprovalSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(ShipmentBooking)))
                {
                    if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.Approval)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("ActiveApproved3", false, BinaryOperatorType.Equal),
                                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("ActiveApproved1", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", true, BinaryOperatorType.Equal)));
                    }
                    else if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.EndApproval)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new BinaryOperator("ActiveApproved3", true, BinaryOperatorType.Equal);
                    }
                    else if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.AllData)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("ActiveApproved1", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved3", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved1", false, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", false, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved3", false, BinaryOperatorType.Equal));
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ShipmentBooking " + ex.ToString());
            }
        }

        //=============================================================================== Code Only =========================================================================================

        #region Posting

        private void SetShipmentBookingMonitoring(Session _currSession, ShipmentBooking _locShipmentBookingXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locShipmentBookingXPO != null)
                {
                    XPCollection<ShipmentBookingLine> _locShipmentBookingLines = new XPCollection<ShipmentBookingLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("ShipmentBooking", _locShipmentBookingXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locShipmentBookingLines != null && _locShipmentBookingLines.Count() > 0)
                    {
                        
                        foreach (ShipmentBookingLine _locShipmentBookingLine in _locShipmentBookingLines)
                        {
                            if (_locShipmentBookingLine.Status == Status.Progress || _locShipmentBookingLine.Status == Status.Posted)
                            {
                                if (_locShipmentBookingXPO.ShipmentType == ShipmentType.ExportAndImport)
                                {
                                    #region Export
                                    ShipmentBookingMonitoring _saveDataShipmentBookingMonitoringExport = new ShipmentBookingMonitoring(_currSession)
                                    {
                                        ShipmentBooking = _locShipmentBookingXPO,
                                        ShipmentBookingLine = _locShipmentBookingLine,
                                        ShipmentType = ShipmentType.Export,
                                        ServiceModa = _locShipmentBookingLine.ServiceModa,
                                        TransportType = _locShipmentBookingLine.TransportType,
                                        DeliveryType = _locShipmentBookingLine.DeliveryType,
                                        TransactionTerm = _locShipmentBookingLine.TransactionTerm,
                                        ItemGroup = _locShipmentBookingLine.ItemGroup,
                                        Item = _locShipmentBookingLine.Item,
                                        MxDQty = _locShipmentBookingLine.DQty,
                                        MxDUOM = _locShipmentBookingLine.DUOM,
                                        MxQty = _locShipmentBookingLine.Qty,
                                        MxUOM = _locShipmentBookingLine.UOM,
                                        MxTQty = _locShipmentBookingLine.TQty,
                                        DQty = _locShipmentBookingLine.DQty,
                                        DUOM = _locShipmentBookingLine.DUOM,
                                        Qty = _locShipmentBookingLine.Qty,
                                        UOM = _locShipmentBookingLine.UOM,
                                        TQty = _locShipmentBookingLine.TQty,
                                        Currency = _locShipmentBookingLine.Currency,
                                        PriceGroup = _locShipmentBookingLine.PriceGroup,
                                        Price = _locShipmentBookingLine.Price,
                                        PriceLine = _locShipmentBookingLine.PriceLine,
                                        UAmount = _locShipmentBookingLine.UAmount,
                                        TUAmount = _locShipmentBookingLine.TUAmount,
                                        MultiTax = _locShipmentBookingLine.MultiTax,
                                        Tax = _locShipmentBookingLine.Tax,
                                        TxValue = _locShipmentBookingLine.TxValue,
                                        TxAmount = _locShipmentBookingLine.TxAmount,
                                        MultiDiscount = _locShipmentBookingLine.MultiDiscount,
                                        Discount = _locShipmentBookingLine.Discount,
                                        Disc = _locShipmentBookingLine.Disc,
                                        DiscAmount = _locShipmentBookingLine.DiscAmount,
                                        TAmount = _locShipmentBookingLine.TAmount,
                                        CountryFrom = _locShipmentBookingLine.CountryFrom,
                                        CityFrom = _locShipmentBookingLine.CityFrom,
                                        AreaFrom = _locShipmentBookingLine.AreaFrom,
                                        LocationFrom = _locShipmentBookingLine.LocationFrom,
                                        TransportLocationFrom = _locShipmentBookingLine.TransportLocationFrom,
                                        CountryTo = _locShipmentBookingLine.CountryTo,
                                        CityTo = _locShipmentBookingLine.CityTo,
                                        AreaTo = _locShipmentBookingLine.AreaTo,
                                        LocationTo = _locShipmentBookingLine.LocationTo,
                                        TransportLocationTo = _locShipmentBookingLine.TransportLocationTo,
                                    };
                                    _saveDataShipmentBookingMonitoringExport.Save();
                                    _saveDataShipmentBookingMonitoringExport.Session.CommitTransaction();

                                    #endregion Export
                                    #region Import
                                    ShipmentBookingMonitoring _saveDataShipmentBookingMonitoringImport = new ShipmentBookingMonitoring(_currSession)
                                    {
                                        ShipmentBooking = _locShipmentBookingXPO,
                                        ShipmentBookingLine = _locShipmentBookingLine,
                                        ShipmentType = ShipmentType.Import,
                                        ServiceModa = _locShipmentBookingLine.ServiceModa,
                                        TransportType = _locShipmentBookingLine.TransportType,
                                        DeliveryType = _locShipmentBookingLine.DeliveryType,
                                        TransactionTerm = _locShipmentBookingLine.TransactionTerm,
                                        ItemGroup = _locShipmentBookingLine.ItemGroup,
                                        Item = _locShipmentBookingLine.Item,
                                        MxDQty = _locShipmentBookingLine.DQty,
                                        MxDUOM = _locShipmentBookingLine.DUOM,
                                        MxQty = _locShipmentBookingLine.Qty,
                                        MxUOM = _locShipmentBookingLine.UOM,
                                        MxTQty = _locShipmentBookingLine.TQty,
                                        DQty = _locShipmentBookingLine.DQty,
                                        DUOM = _locShipmentBookingLine.DUOM,
                                        Qty = _locShipmentBookingLine.Qty,
                                        UOM = _locShipmentBookingLine.UOM,
                                        TQty = _locShipmentBookingLine.TQty,
                                        Currency = _locShipmentBookingLine.Currency,
                                        PriceGroup = _locShipmentBookingLine.PriceGroup,
                                        Price = _locShipmentBookingLine.Price,
                                        PriceLine = _locShipmentBookingLine.PriceLine,
                                        UAmount = _locShipmentBookingLine.UAmount,
                                        TUAmount = _locShipmentBookingLine.TUAmount,
                                        MultiTax = _locShipmentBookingLine.MultiTax,
                                        Tax = _locShipmentBookingLine.Tax,
                                        TxValue = _locShipmentBookingLine.TxValue,
                                        TxAmount = _locShipmentBookingLine.TxAmount,
                                        MultiDiscount = _locShipmentBookingLine.MultiDiscount,
                                        Discount = _locShipmentBookingLine.Discount,
                                        Disc = _locShipmentBookingLine.Disc,
                                        DiscAmount = _locShipmentBookingLine.DiscAmount,
                                        TAmount = _locShipmentBookingLine.TAmount,
                                        CountryFrom = _locShipmentBookingLine.CountryFrom,
                                        CityFrom = _locShipmentBookingLine.CityFrom,
                                        AreaFrom = _locShipmentBookingLine.AreaFrom,
                                        LocationFrom = _locShipmentBookingLine.LocationFrom,
                                        TransportLocationFrom = _locShipmentBookingLine.TransportLocationFrom,
                                        CountryTo = _locShipmentBookingLine.CountryTo,
                                        CityTo = _locShipmentBookingLine.CityTo,
                                        AreaTo = _locShipmentBookingLine.AreaTo,
                                        LocationTo = _locShipmentBookingLine.LocationTo,
                                        TransportLocationTo = _locShipmentBookingLine.TransportLocationTo,
                                    };
                                    _saveDataShipmentBookingMonitoringImport.Save();
                                    _saveDataShipmentBookingMonitoringImport.Session.CommitTransaction();
                                    #endregion Import
                                }else
                                {
                                    ShipmentBookingMonitoring _saveDataShipmentBookingMonitoring = new ShipmentBookingMonitoring(_currSession)
                                    {
                                        ShipmentBooking = _locShipmentBookingXPO,
                                        ShipmentBookingLine = _locShipmentBookingLine,
                                        ShipmentType = _locShipmentBookingLine.ShipmentType,
                                        ServiceModa = _locShipmentBookingLine.ServiceModa,
                                        TransportType = _locShipmentBookingLine.TransportType,
                                        DeliveryType = _locShipmentBookingLine.DeliveryType,
                                        TransactionTerm = _locShipmentBookingLine.TransactionTerm,
                                        ItemGroup = _locShipmentBookingLine.ItemGroup,
                                        Item = _locShipmentBookingLine.Item,
                                        MxDQty = _locShipmentBookingLine.DQty,
                                        MxDUOM = _locShipmentBookingLine.DUOM,
                                        MxQty = _locShipmentBookingLine.Qty,
                                        MxUOM = _locShipmentBookingLine.UOM,
                                        MxTQty = _locShipmentBookingLine.TQty,
                                        DQty = _locShipmentBookingLine.DQty,
                                        DUOM = _locShipmentBookingLine.DUOM,
                                        Qty = _locShipmentBookingLine.Qty,
                                        UOM = _locShipmentBookingLine.UOM,
                                        TQty = _locShipmentBookingLine.TQty,
                                        Currency = _locShipmentBookingLine.Currency,
                                        PriceGroup = _locShipmentBookingLine.PriceGroup,
                                        Price = _locShipmentBookingLine.Price,
                                        PriceLine = _locShipmentBookingLine.PriceLine,
                                        UAmount = _locShipmentBookingLine.UAmount,
                                        TUAmount = _locShipmentBookingLine.TUAmount,
                                        MultiTax = _locShipmentBookingLine.MultiTax,
                                        Tax = _locShipmentBookingLine.Tax,
                                        TxValue = _locShipmentBookingLine.TxValue,
                                        TxAmount = _locShipmentBookingLine.TxAmount,
                                        MultiDiscount = _locShipmentBookingLine.MultiDiscount,
                                        Discount = _locShipmentBookingLine.Discount,
                                        Disc = _locShipmentBookingLine.Disc,
                                        DiscAmount = _locShipmentBookingLine.DiscAmount,
                                        TAmount = _locShipmentBookingLine.TAmount,
                                        CountryFrom = _locShipmentBookingLine.CountryFrom,
                                        CityFrom = _locShipmentBookingLine.CityFrom,
                                        AreaFrom = _locShipmentBookingLine.AreaFrom,
                                        LocationFrom = _locShipmentBookingLine.LocationFrom,
                                        TransportLocationFrom = _locShipmentBookingLine.TransportLocationFrom,
                                        CountryTo = _locShipmentBookingLine.CountryTo,
                                        CityTo = _locShipmentBookingLine.CityTo,
                                        AreaTo = _locShipmentBookingLine.AreaTo,
                                        LocationTo = _locShipmentBookingLine.LocationTo,
                                        TransportLocationTo = _locShipmentBookingLine.TransportLocationTo,
                                    };
                                    _saveDataShipmentBookingMonitoring.Save();
                                    _saveDataShipmentBookingMonitoring.Session.CommitTransaction();
                                }
                                
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ShipmentBooking " + ex.ToString());
            }
        }

        private void SetRemainQty(Session _currSession, ShipmentBooking _locShipmentBookingXPO)
        {
            try
            {
                if (_locShipmentBookingXPO != null)
                {
                    XPCollection<ShipmentBookingLine> _locShipmentBookingLines = new XPCollection<ShipmentBookingLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("ShipmentBooking", _locShipmentBookingXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Progress),
                                                            new BinaryOperator("Status", Status.Posted))));

                    if (_locShipmentBookingLines != null && _locShipmentBookingLines.Count > 0)
                    {
                        double _locRmDQty = 0;
                        double _locRmQty = 0;
                        double _locInvLineTotal = 0;
                        ItemUnitOfMeasure _locItemUOM = null;

                        foreach (ShipmentBookingLine _locShipmentBookingLine in _locShipmentBookingLines)
                        {
                            #region ProcessCount=0
                            if (_locShipmentBookingLine.PostedCount == 0)
                            {
                                #region MaxQuantity
                                if (_locShipmentBookingLine.MxDQty > 0)
                                {
                                    if (_locShipmentBookingLine.DQty > 0 && _locShipmentBookingLine.DQty <= _locShipmentBookingLine.MxDQty)
                                    {
                                        _locRmDQty = _locShipmentBookingLine.MxDQty - _locShipmentBookingLine.DQty;
                                    }

                                    if (_locShipmentBookingLine.Qty > 0 && _locShipmentBookingLine.Qty <= _locShipmentBookingLine.MxQty)
                                    {
                                        _locRmQty = _locShipmentBookingLine.MxQty - _locShipmentBookingLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locShipmentBookingLine.Item),
                                                                new BinaryOperator("UOM", _locShipmentBookingLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locShipmentBookingLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty + _locRmDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }

                                }
                                #endregion MaxQuantity
                                #region NonMaxQuantity
                                else
                                {
                                    if (_locShipmentBookingLine.DQty > 0)
                                    {
                                        _locRmDQty = 0;
                                    }

                                    if (_locShipmentBookingLine.Qty > 0)
                                    {
                                        _locRmQty = 0;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locShipmentBookingLine.Item),
                                                                new BinaryOperator("UOM", _locShipmentBookingLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locShipmentBookingLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty + _locRmDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion ProcessCount=0

                            #region ProcessCount>0
                            if (_locShipmentBookingLine.PostedCount > 0)
                            {
                                if (_locShipmentBookingLine.RmDQty > 0)
                                {
                                    _locRmDQty = _locShipmentBookingLine.RmDQty - _locShipmentBookingLine.DQty;
                                }

                                if (_locShipmentBookingLine.RmQty > 0)
                                {
                                    _locRmQty = _locShipmentBookingLine.RmQty - _locShipmentBookingLine.Qty;
                                }

                                if (_locShipmentBookingLine.MxDQty > 0 || _locShipmentBookingLine.MxQty > 0)
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locShipmentBookingLine.Item),
                                                                new BinaryOperator("UOM", _locShipmentBookingLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locShipmentBookingLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                }
                                else
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locShipmentBookingLine.Item),
                                                                new BinaryOperator("UOM", _locShipmentBookingLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locShipmentBookingLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                }


                                if (_locItemUOM != null)
                                {
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locRmQty + _locRmDQty;
                                }

                            }
                            #endregion ProcessCount>0

                            _locShipmentBookingLine.RmDQty = _locRmDQty;
                            _locShipmentBookingLine.RmQty = _locRmQty;
                            _locShipmentBookingLine.RmTQty = _locInvLineTotal;
                            _locShipmentBookingLine.Save();
                            _locShipmentBookingLine.Session.CommitTransaction();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ShipmentBooking " + ex.ToString());
            }
        }

        private void SetPostingQty(Session _currSession, ShipmentBooking _locShipmentBookingXPO)
        {
            try
            {
                if (_locShipmentBookingXPO != null)
                {
                    XPCollection<ShipmentBookingLine> _locShipmentBookingLines = new XPCollection<ShipmentBookingLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("ShipmentBooking", _locShipmentBookingXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Progress),
                                                            new BinaryOperator("Status", Status.Posted))));

                    if (_locShipmentBookingLines != null && _locShipmentBookingLines.Count > 0)
                    {
                        double _locPDQty = 0;
                        double _locPQty = 0;
                        double _locInvLineTotal = 0;
                        ItemUnitOfMeasure _locItemUOM = null;

                        foreach (ShipmentBookingLine _locShipmentBookingLine in _locShipmentBookingLines)
                        {
                            #region ProcessCount=0
                            if (_locShipmentBookingLine.PostedCount == 0)
                            {
                                #region MaxQuantity
                                if (_locShipmentBookingLine.MxDQty > 0)
                                {
                                    if (_locShipmentBookingLine.DQty > 0 && _locShipmentBookingLine.DQty <= _locShipmentBookingLine.MxDQty)
                                    {
                                        _locPDQty = _locShipmentBookingLine.DQty;
                                    }

                                    if (_locShipmentBookingLine.Qty > 0 && _locShipmentBookingLine.Qty <= _locShipmentBookingLine.MxQty)
                                    {
                                        _locPQty = _locShipmentBookingLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locShipmentBookingLine.Item),
                                                                new BinaryOperator("UOM", _locShipmentBookingLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locShipmentBookingLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty + _locPDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }

                                }
                                #endregion MaxQuantity
                                #region NonMaxQuantity
                                else
                                {
                                    if (_locShipmentBookingLine.DQty > 0)
                                    {
                                        _locPDQty = _locShipmentBookingLine.DQty;
                                    }

                                    if (_locShipmentBookingLine.Qty > 0)
                                    {
                                        _locPQty = _locShipmentBookingLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locShipmentBookingLine.Item),
                                                                new BinaryOperator("UOM", _locShipmentBookingLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locShipmentBookingLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty + _locPDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion ProcessCount=0

                            #region ProcessCount>0
                            if (_locShipmentBookingLine.PostedCount > 0)
                            {
                                if (_locShipmentBookingLine.PDQty > 0)
                                {
                                    _locPDQty = _locShipmentBookingLine.PDQty + _locShipmentBookingLine.DQty;
                                }

                                if (_locShipmentBookingLine.PQty > 0)
                                {
                                    _locPQty = _locShipmentBookingLine.PQty + _locShipmentBookingLine.Qty;
                                }

                                if (_locShipmentBookingLine.MxDQty > 0 || _locShipmentBookingLine.MxQty > 0)
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                           (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Item", _locShipmentBookingLine.Item),
                                                            new BinaryOperator("UOM", _locShipmentBookingLine.MxUOM),
                                                            new BinaryOperator("DefaultUOM", _locShipmentBookingLine.MxDUOM),
                                                            new BinaryOperator("Active", true)));
                                }
                                else
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                           (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Item", _locShipmentBookingLine.Item),
                                                            new BinaryOperator("UOM", _locShipmentBookingLine.UOM),
                                                            new BinaryOperator("DefaultUOM", _locShipmentBookingLine.DUOM),
                                                            new BinaryOperator("Active", true)));
                                }

                                if (_locItemUOM != null)
                                {
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locPQty + _locPDQty;
                                }

                            }
                            #endregion ProcessCount>0

                            _locShipmentBookingLine.PDQty = _locPDQty;
                            _locShipmentBookingLine.PDUOM = _locShipmentBookingLine.DUOM;
                            _locShipmentBookingLine.PQty = _locPQty;
                            _locShipmentBookingLine.PUOM = _locShipmentBookingLine.UOM;
                            _locShipmentBookingLine.PTQty = _locInvLineTotal;
                            _locShipmentBookingLine.Save();
                            _locShipmentBookingLine.Session.CommitTransaction();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ShipmentBooking " + ex.ToString());
            }

        }

        private void SetProcessCount(Session _currSession, ShipmentBooking _locShipmentBookingXPO)
        {
            try
            {
                if (_locShipmentBookingXPO != null)
                {
                    XPCollection<ShipmentBookingLine> _locShipmentBookingLines = new XPCollection<ShipmentBookingLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("ShipmentBooking", _locShipmentBookingXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locShipmentBookingLines != null && _locShipmentBookingLines.Count > 0)
                    {

                        foreach (ShipmentBookingLine _locShipmentBookingLine in _locShipmentBookingLines)
                        {
                            if (_locShipmentBookingLine.Status == Status.Progress || _locShipmentBookingLine.Status == Status.Posted)
                            {
                                _locShipmentBookingLine.PostedCount = _locShipmentBookingLine.PostedCount + 1;
                                _locShipmentBookingLine.Save();
                                _locShipmentBookingLine.Session.CommitTransaction();
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ShipmentBooking " + ex.ToString());
            }
        }

        private void SetStatusShipmentBookingLine(Session _currSession, ShipmentBooking _locShipmentBookingXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locShipmentBookingXPO != null)
                {
                    XPCollection<ShipmentBookingLine> _locShipmentBookingLines = new XPCollection<ShipmentBookingLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("ShipmentBooking", _locShipmentBookingXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locShipmentBookingLines != null && _locShipmentBookingLines.Count > 0)
                    {

                        foreach (ShipmentBookingLine _locShipmentBookingLine in _locShipmentBookingLines)
                        {
                            if (_locShipmentBookingLine.Status == Status.Progress || _locShipmentBookingLine.Status == Status.Posted)
                            {
                                if (_locShipmentBookingLine.RmDQty == 0 && _locShipmentBookingLine.RmQty == 0 && _locShipmentBookingLine.RmTQty == 0)
                                {
                                    _locShipmentBookingLine.Status = Status.Close;
                                    _locShipmentBookingLine.ActivationPosting = true;
                                    _locShipmentBookingLine.StatusDate = now;
                                }
                                else
                                {
                                    _locShipmentBookingLine.Status = Status.Posted;
                                    _locShipmentBookingLine.StatusDate = now;
                                }
                                _locShipmentBookingLine.Save();
                                _locShipmentBookingLine.Session.CommitTransaction();
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ShipmentBooking " + ex.ToString());
            }
        }

        private void SetNormalQuantity(Session _currSession, ShipmentBooking _locShipmentBookingXPO)
        {
            try
            {
                if (_locShipmentBookingXPO != null)
                {
                    XPCollection<ShipmentBookingLine> _locShipmentBookingLines = new XPCollection<ShipmentBookingLine>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("ShipmentBooking", _locShipmentBookingXPO),
                                                                                     new BinaryOperator("Select", true)));

                    if (_locShipmentBookingLines != null && _locShipmentBookingLines.Count > 0)
                    {
                        foreach (ShipmentBookingLine _locShipmentBookingLine in _locShipmentBookingLines)
                        {
                            if (_locShipmentBookingLine.Status == Status.Progress || _locShipmentBookingLine.Status == Status.Posted || _locShipmentBookingLine.Status == Status.Close)
                            {
                                if (_locShipmentBookingLine.DQty > 0 || _locShipmentBookingLine.Qty > 0)
                                {
                                    _locShipmentBookingLine.Select = false;
                                    _locShipmentBookingLine.DQty = 0;
                                    _locShipmentBookingLine.Qty = 0;
                                    _locShipmentBookingLine.Save();
                                    _locShipmentBookingLine.Session.CommitTransaction();
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ShipmentBooking " + ex.ToString());
            }
        }

        private void SetFinalStatusShipmentBooking(Session _currSession, ShipmentBooking _locShipmentBookingXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locStatusCount = 0;
                int _locShipmentBookingLineCount = 0;

                if (_locShipmentBookingXPO != null)
                {

                    XPCollection<ShipmentBookingLine> _locShipmentBookingLines = new XPCollection<ShipmentBookingLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("ShipmentBooking", _locShipmentBookingXPO)));

                    if (_locShipmentBookingLines != null && _locShipmentBookingLines.Count() > 0)
                    {
                        _locShipmentBookingLineCount = _locShipmentBookingLines.Count();

                        foreach (ShipmentBookingLine _locShipmentBookingLine in _locShipmentBookingLines)
                        {
                            if (_locShipmentBookingLine.Status == Status.Close)
                            {
                                _locStatusCount = _locStatusCount + 1;
                            }
                        }
                    }

                    if (_locStatusCount == _locShipmentBookingLineCount)
                    {
                        _locShipmentBookingXPO.ActivationPosting = true;
                        _locShipmentBookingXPO.Status = Status.Close;
                        _locShipmentBookingXPO.StatusDate = now;
                        _locShipmentBookingXPO.Save();
                        _locShipmentBookingXPO.Session.CommitTransaction();
                    }
                    else
                    {
                        _locShipmentBookingXPO.Status = Status.Posted;
                        _locShipmentBookingXPO.StatusDate = now;
                        _locShipmentBookingXPO.Save();
                        _locShipmentBookingXPO.Session.CommitTransaction();
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ShipmentBooking " + ex.ToString());
            }
        }

        #endregion Posting

        #region Approval Process

        private void SetApprovalLine(Session _currentSession, ShipmentBooking _locShipmentBookingXPO, ApprovalLevel _locApprovalLevel)
        {
            try
            {
                DateTime now = DateTime.Now;

                ShipmentApprovalLine _locShipmentApprovalLineXPO = _currentSession.FindObject<ShipmentApprovalLine>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("ShipmentBooking", _locShipmentBookingXPO),
                                                                         new BinaryOperator("ApprovalLevel", _locApprovalLevel)));

                if (_locShipmentApprovalLineXPO == null)
                {
                    ShipmentApprovalLine _saveDataAL2 = new ShipmentApprovalLine(_currentSession)
                    {
                        ApprovalDate = now,
                        ApprovalStatus = Status.Approved,
                        ApprovalLevel = _locApprovalLevel,
                        ShipmentBooking = _locShipmentBookingXPO,
                    };
                    _saveDataAL2.Save();
                    _saveDataAL2.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ShipmentBooking " + ex.ToString());
            }
        }

        #endregion Approval Process

        #region Email

        private string BackgroundBody(Session _currentSession, ShipmentBooking _locShipmentBookingXPO, MailSetupDetail _locMailSetDetail)
        {
            string body = string.Empty;

            if(_locShipmentBookingXPO != null)
            {
                
                ShipmentApprovalLine _locShipmentApprovalLineXPO = _currentSession.FindObject<ShipmentApprovalLine>
                                                   (new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("ShipmentBooking", _locShipmentBookingXPO)));

                string Status = _locShipmentApprovalLineXPO.ApprovalStatus.ToString();

                using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Email/EmailTemplate.html")))
                {
                    body = reader.ReadToEnd();
                }
                body = body.Replace("{Code}", _locShipmentBookingXPO.Code);

                #region Level
                if (_locShipmentBookingXPO.ActiveApproved1 == true)
                {
                    string Active = "Level 1";
                    body = body.Replace("{Level}", Active);
                }
                else if (_locShipmentBookingXPO.ActiveApproved2 == true)
                {
                    string Active = "Level 2";
                    body = body.Replace("{Level}", Active);
                }
                else if (_locShipmentBookingXPO.ActiveApproved3 == true)
                {
                    string Active = "Level 3";
                    body = body.Replace("{Level}", Active);
                }
                else
                {
                    string Active = "Not Available";
                    body = body.Replace("{Level}", Active);
                }
                #endregion Level

                body = body.Replace("{Status}", Status);
                body = body.Replace("{Message}", _locMailSetDetail.MessageBody);
            }
            
            return body;
        }

        protected void SendEmail(Session _currentSession, ShipmentBooking _locShipmentBookingXPO, Status _locStatus, ShipmentApprovalLine _locShipmentApprovalLineXPO2)
        {
            DateTime now = DateTime.Now;
            var User = SecuritySystem.CurrentUserName;
            GlobalFunction _globFunc = new GlobalFunction();
            UserAccess _numLineUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", User));
            string _locMessageBody = null;

            try
            {
                if (_locShipmentBookingXPO != null)
                {
                    XPCollection<MailSetupDetail> _locMailSetDetails = new XPCollection<MailSetupDetail>
                                                                       (_currentSession, new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("ObjectList", ObjectList.ShipmentBooking),
                                                                        new BinaryOperator("UserAccess", _numLineUserAccess),
                                                                        new BinaryOperator("Active", true)));

                    if (_locMailSetDetails != null && _locMailSetDetails.Count > 0)
                    {
                        foreach (MailSetupDetail _locMailSetDetail in _locMailSetDetails)
                        {
                            if (_locMailSetDetail.MessageBody != null)
                            {
                                if (_locShipmentApprovalLineXPO2.EndApproval == true)
                                {
                                    _locMessageBody = this.BackgroundBody(_currentSession, _locShipmentBookingXPO, _locMailSetDetail);
                                }
                                else
                                {
                                    _locMessageBody = this.BackgroundBody(_currentSession, _locShipmentBookingXPO, _locMailSetDetail);
                                }
                            }

                            _globFunc.SetAndSendMail(_locMailSetDetail.SmtpHost, _locMailSetDetail.SmtpPort,
                            _locMailSetDetail.MailFrom, _locMailSetDetail.MailFromPassword, _locMailSetDetail.MailTo, _locMailSetDetail.MailSubject, _locMessageBody);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ShipmentBooking " + ex.ToString());
            }
        }

        #endregion Email

        #region Global

        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0} !", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }


        #endregion Global

        
    }
}
