﻿namespace FullDrive.Module.BusinessObjects
{
    partial class ShipmentPurchaseCollectionActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ShipmentPurchaseCollectionShowSPMAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // ShipmentPurchaseCollectionShowSPMAction
            // 
            this.ShipmentPurchaseCollectionShowSPMAction.Caption = "Show SPM";
            this.ShipmentPurchaseCollectionShowSPMAction.ConfirmationMessage = null;
            this.ShipmentPurchaseCollectionShowSPMAction.Id = "ShipmentPurchaseCollectionShowSPMActionId";
            this.ShipmentPurchaseCollectionShowSPMAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.ShipmentPurchaseCollection);
            this.ShipmentPurchaseCollectionShowSPMAction.ToolTip = null;
            this.ShipmentPurchaseCollectionShowSPMAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.ShipmentPurchaseCollectionShowSPMAction_Execute);
            // 
            // ShipmentPurchaseCollectionActionController
            // 
            this.Actions.Add(this.ShipmentPurchaseCollectionShowSPMAction);

        }

        #endregion
        private DevExpress.ExpressApp.Actions.SimpleAction ShipmentPurchaseCollectionShowSPMAction;
    }
}
