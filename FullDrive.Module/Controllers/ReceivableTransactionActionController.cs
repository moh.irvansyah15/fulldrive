﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using FullDrive.Module.BusinessObjects;
using DevExpress.ExpressApp.Xpo;
using FullDrive.Module.CustomProcess;
using System.Collections;
using DevExpress.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class ReceivableTransactionActionController : ViewController
    {
        private ChoiceActionItem _selectionlistViewFilter;

        public ReceivableTransactionActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            ReceivableTransactionListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionlistViewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                ReceivableTransactionListviewFilterSelectionAction.Items.Add(_selectionlistViewFilter);
            }
        }

        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }

        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }

        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void ReceivableTransactionProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        ReceivableTransaction _locReceivTransactionOS = (ReceivableTransaction)_objectSpace.GetObject(obj);

                        if (_locReceivTransactionOS != null)
                        {
                            if (_locReceivTransactionOS.Code != null)
                            {
                                _currObjectId = _locReceivTransactionOS.Code;

                                ReceivableTransaction _locReceivTransactionXPO = _currSession.FindObject<ReceivableTransaction>
                                                                                 (new GroupOperator(GroupOperatorType.And,
                                                                                  new BinaryOperator("Code", _currObjectId)));

                                if (_locReceivTransactionXPO != null)
                                {
                                    if (_locReceivTransactionXPO.Status == Status.Open)
                                    {
                                        _locReceivTransactionXPO.Status = Status.Progress;
                                        _locReceivTransactionXPO.StatusDate = now;
                                        _locReceivTransactionXPO.Save();
                                        _locReceivTransactionXPO.Session.CommitTransaction();

                                        XPCollection<ReceivableTransactionLine> _locReceivableTransactionLines = new XPCollection<ReceivableTransactionLine>
                                                                                                                 (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                  new BinaryOperator("ReceivableTransaction", _locReceivTransactionXPO)));

                                        if (_locReceivableTransactionLines != null && _locReceivableTransactionLines.Count > 0)
                                        {
                                            foreach (ReceivableTransactionLine _locReceivableTransactionLine in _locReceivableTransactionLines)
                                            {
                                                if (_locReceivableTransactionLine.Status == Status.Open)
                                                {
                                                    _locReceivableTransactionLine.Status = Status.Progress;
                                                    _locReceivableTransactionLine.StatusDate = now;
                                                    _locReceivableTransactionLine.Save();
                                                    _locReceivableTransactionLine.Session.CommitTransaction();
                                                }
                                            }
                                        }

                                        XPCollection<ReceivableSalesCollection> _locReceivableTransactionCollections = new XPCollection<ReceivableSalesCollection>
                                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                            new BinaryOperator("ReceivableTransaction", _locReceivTransactionXPO)));
                                        if(_locReceivableTransactionCollections != null && _locReceivableTransactionCollections.Count() > 0)
                                        {
                                            foreach(ReceivableSalesCollection _locReceivableTransactionCollection in _locReceivableTransactionCollections)
                                            {
                                                if(_locReceivableTransactionCollection.Status == Status.Open)
                                                {
                                                    _locReceivableTransactionCollection.Status = Status.Progress;
                                                    _locReceivableTransactionCollection.StatusDate = now;
                                                    _locReceivableTransactionCollection.Save();
                                                    _locReceivableTransactionCollection.Session.CommitTransaction();
                                                }
                                            }
                                        }
                                        SuccessMessageShow(_locReceivTransactionXPO.Code + "Has been change successfully to Progress");
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data Receivable Transaction not available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Receivable Transaction not available");
                            }
                        }
                    }
                }
                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business = ReceivableTransaction " + ex.ToString());
            }
        }

        private void ReceivableTransactionPostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        ReceivableTransaction _locReceivableTransactionOS = (ReceivableTransaction)_objectSpace.GetObject(obj);

                        if (_locReceivableTransactionOS != null)
                        {
                            if (_locReceivableTransactionOS.Code != null)
                            {
                                _currObjectId = _locReceivableTransactionOS.Code;

                                ReceivableTransaction _locReceivableTransactionXPO = _currSession.FindObject<ReceivableTransaction>
                                                                                     (new GroupOperator(GroupOperatorType.And,
                                                                                      new BinaryOperator("Code", _currObjectId),
                                                                                      new GroupOperator(GroupOperatorType.Or,
                                                                                      new BinaryOperator("Status", Status.Progress),
                                                                                      new BinaryOperator("Status", Status.Posted))));
                                if (_locReceivableTransactionXPO != null)
                                {
                                    if(_locReceivableTransactionXPO.PaymentMethodType == PaymentMethodType.Normal )
                                    {
                                        if (CheckMaksAmountDebitBasedSalesInvoiceMonitoring(_currSession, _locReceivableTransactionXPO) == true)
                                        {
                                            if (CheckAmountDebitCreditBasedSalesInvoiceMonitoring(_currSession, _locReceivableTransactionXPO) == true)
                                            {
                                                SetReceivableTransactionMonitoringBasedSalesInvoiceMonitoring(_currSession, _locReceivableTransactionXPO);
                                                SetReceivableJournalBasedSalesInvoiceMonitoring(_currSession, _locReceivableTransactionXPO);
                                                SetNormalSalesInvoiceMonitoring(_currSession, _locReceivableTransactionXPO);
                                                SetStatusReceivableTransactionLine(_currSession, _locReceivableTransactionXPO);
                                                SetFinalStatusReceivableTransaction(_currSession, _locReceivableTransactionXPO);
                                                SetCloseAllSalesProcess(_currSession, _locReceivableTransactionXPO);
                                                SuccessMessageShow(_locReceivableTransactionXPO.Code + " has been change successfully to Progress");
                                            }
                                            else
                                            {
                                                ErrorMessageShow("Please Check Amount Transaction");
                                            }
                                        }
                                        else
                                        {
                                            ErrorMessageShow("Please Check Amount Transaction");
                                        }
                                    }else if(_locReceivableTransactionXPO.PaymentMethodType == PaymentMethodType.DownPayment || _locReceivableTransactionXPO.PaymentMethodType == PaymentMethodType.CashBeforeDelivery)
                                    {
                                        if (CheckMaksAmountDebitBasedSalesPrePaymentInvoice(_currSession, _locReceivableTransactionXPO) == true)
                                        {
                                            if (CheckAmountDebitCreditBasedSalesPrePaymentInvoice(_currSession, _locReceivableTransactionXPO) == true)
                                            {
                                                SetReceivableTransactionMonitoringBasedSalesPrePaymentInvoice(_currSession, _locReceivableTransactionXPO);
                                                SetReceivableJournalBasedSalesPrePaymentInvoice(_currSession, _locReceivableTransactionXPO);
                                                SetNormalSalesInvoiceMonitoring(_currSession, _locReceivableTransactionXPO);
                                                SetStatusReceivableTransactionLine(_currSession, _locReceivableTransactionXPO);
                                                SetFinalStatusReceivableTransaction(_currSession, _locReceivableTransactionXPO);
                                                SetCloseAllSalesProcess(_currSession, _locReceivableTransactionXPO);
                                                SuccessMessageShow(_locReceivableTransactionXPO.Code + " has been change successfully to Progress");
                                            }
                                            else
                                            {
                                                ErrorMessageShow("Please Check Amount Transaction");
                                            }
                                        }
                                        else
                                        {
                                            ErrorMessageShow("Please Check Amount Transaction");
                                        }
                                    }
                                    
                                }
                                else
                                {
                                    ErrorMessageShow("Data Receivable Transaction Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Receivable Transaction Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ReceivableTransaction " + ex.ToString());
            }
        }

        private void ReceivableTransactionGetSIAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;


                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        ReceivableTransaction _locReceivableTransactionOS = (ReceivableTransaction)_objectSpace.GetObject(obj);

                        if (_locReceivableTransactionOS != null)
                        {
                            if (_locReceivableTransactionOS.Code != null)
                            {
                                _currObjectId = _locReceivableTransactionOS.Code;

                                ReceivableTransaction _locReceivableTransactionXPO = _currSession.FindObject<ReceivableTransaction>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locReceivableTransactionXPO != null)
                                {
                                    if (_locReceivableTransactionXPO.Status == Status.Open || _locReceivableTransactionXPO.Status == Status.Progress)
                                    {
                                        XPCollection<ReceivableSalesCollection> _locReceivableSalesCollections = new XPCollection<ReceivableSalesCollection>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("ReceivableTransaction", _locReceivableTransactionXPO),
                                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("Status", Status.Open),
                                                                                        new BinaryOperator("Status", Status.Progress))));

                                        if (_locReceivableSalesCollections != null && _locReceivableSalesCollections.Count() > 0)
                                        {
                                            foreach (ReceivableSalesCollection _locReceivableSalesCollection in _locReceivableSalesCollections)
                                            {
                                                if (_locReceivableSalesCollection.ReceivableTransaction != null && _locReceivableSalesCollection.SalesInvoice != null )
                                                {
                                                    GetSalesInvoiceMonitoring(_currSession, _locReceivableSalesCollection.SalesInvoice, _locReceivableTransactionXPO);
                                                }
                                            }
                                            SuccessMessageShow("SalesInvoice Has Been Successfully Getting into ReceivableTransaction");
                                        }
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("SalesInvoice Order Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("SalesInvoice Order Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ReceivableTransaction " + ex.ToString());
            }
        }

        private void ReceivableTransactionGetSPIAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;


                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        ReceivableTransaction _locReceivableTransactionOS = (ReceivableTransaction)_objectSpace.GetObject(obj);

                        if (_locReceivableTransactionOS != null)
                        {
                            if (_locReceivableTransactionOS.Code != null)
                            {
                                _currObjectId = _locReceivableTransactionOS.Code;

                                ReceivableTransaction _locReceivableTransactionXPO = _currSession.FindObject<ReceivableTransaction>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locReceivableTransactionXPO != null)
                                {
                                    if (_locReceivableTransactionXPO.Status == Status.Open || _locReceivableTransactionXPO.Status == Status.Progress)
                                    {
                                        XPCollection<ReceivableSalesCollection> _locReceivableSalesCollections = new XPCollection<ReceivableSalesCollection>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("ReceivableTransaction", _locReceivableTransactionXPO),
                                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("Status", Status.Open),
                                                                                        new BinaryOperator("Status", Status.Progress))));

                                        if (_locReceivableSalesCollections != null && _locReceivableSalesCollections.Count() > 0)
                                        {
                                            foreach (ReceivableSalesCollection _locReceivableSalesCollection in _locReceivableSalesCollections)
                                            {
                                                if (_locReceivableSalesCollection.ReceivableTransaction != null && _locReceivableSalesCollection.SalesPrePaymentInvoice != null)
                                                {
                                                    GetSalesPrePaymentInvoice(_currSession, _locReceivableSalesCollection.SalesPrePaymentInvoice, _locReceivableTransactionXPO);
                                                }
                                            }
                                            SuccessMessageShow("SalesPrePaymentInvoice Has Been Successfully Getting into ReceivableTransaction");
                                        }
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("SalesPrePaymentInvoice Order Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("SalesPrePaymentInvoice Order Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ReceivableTransaction " + ex.ToString());
            }
        }

        private void ReceivableTransactionListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(ReceivableTransaction)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ReceivableTransaction " + ex.ToString());
            }
        }

        //============================================ Code Only ==============================================

        #region GetSI

        private void GetSalesInvoiceMonitoring(Session _currSession, SalesInvoice _locSalesInvoiceXPO, ReceivableTransaction _locReceivableTransactionXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                ChartOfAccount _locAccountForCompany = null;
                ChartOfAccount _locAccountForCustomer = null;
                if (_locSalesInvoiceXPO != null && _locReceivableTransactionXPO != null)
                {
                    //Cek 
                    //Harus mampu menghitung otomatis buat MultiTax dan MultiDiscount

                    XPCollection<SalesInvoiceMonitoring> _locSalesInvoiceMonitorings = new XPCollection<SalesInvoiceMonitoring>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("SalesInvoice", _locSalesInvoiceXPO),
                                                                                        new BinaryOperator("ReceivableTransaction", _locReceivableTransactionXPO),
                                                                                        new BinaryOperator("Select", true),
                                                                                        new BinaryOperator("PostedCount", 0)));

                    if (_locSalesInvoiceMonitorings != null && _locSalesInvoiceMonitorings.Count() > 0)
                    {
                        //Cek Total PIP dan Sales Order Maks Bill
                        foreach (SalesInvoiceMonitoring _locSalesInvoiceMonitoring in _locSalesInvoiceMonitorings)
                        {
                            if (_locSalesInvoiceMonitoring.SalesOrderMonitoring != null
                                && _locSalesInvoiceMonitoring.SalesOrderMonitoring.SalesOrder != null)
                            {
                                #region Company
                                if(_locSalesInvoiceMonitoring.SalesInvoice.BankAccountCompany != null && _locSalesInvoiceMonitoring.SalesInvoice.Company != null)
                                {
                                    _locAccountForCompany = GetAccountFromBankAccountForCompany(_currSession, _locSalesInvoiceMonitoring.SalesInvoice.BankAccountCompany, _locSalesInvoiceMonitoring.SalesInvoice.Company);
                                }

                                ReceivableTransactionLine _saveDataReceivableTransactionLine1 = new ReceivableTransactionLine(_currSession)
                                {
                                    PostingDate = now,
                                    SalesType = PostingType.Sales,
                                    PostingMethod = PostingMethod.Bill,
                                    OpenCompany = true,
                                    Company = _locSalesInvoiceMonitoring.SalesInvoice.Company,
                                    BankAccount = _locSalesInvoiceMonitoring.SalesInvoice.BankAccountCompany,
                                    AccountNo = _locSalesInvoiceMonitoring.SalesInvoice.CompanyAccountNo,
                                    AccountName = _locSalesInvoiceMonitoring.SalesInvoice.CompanyAccountName,
                                    Account = _locAccountForCompany,
                                    Debit = _locSalesInvoiceMonitoring.Bill,
                                    CloseCredit = true,
                                    SalesInvoice = _locSalesInvoiceMonitoring.SalesInvoice,
                                    SalesOrder = _locSalesInvoiceMonitoring.SalesOrderMonitoring.SalesOrder,
                                    SalesInvoiceMonitoring = _locSalesInvoiceMonitoring,
                                    ReceivableTransaction = _locReceivableTransactionXPO,
                                };
                                _saveDataReceivableTransactionLine1.Save();
                                _saveDataReceivableTransactionLine1.Session.CommitTransaction();

                                #endregion Company

                                #region Customer 
                                if(_locSalesInvoiceMonitoring.SalesInvoice.BankAccount != null && _locSalesInvoiceMonitoring.SalesInvoice.SalesToCustomer != null)
                                {
                                    _locAccountForCustomer = GetAccountFromBankAccountForCustomer(_currSession, _locSalesInvoiceMonitoring.SalesInvoice.BankAccount, _locSalesInvoiceMonitoring.SalesInvoice.SalesToCustomer);
                                }
                                ReceivableTransactionLine _saveDataReceivableTransactionLine2 = new ReceivableTransactionLine(_currSession)
                                {
                                    PostingDate = now,
                                    SalesType = PostingType.Sales,
                                    PostingMethod = PostingMethod.Bill,
                                    OpenCustomer = true,
                                    Customer = _locSalesInvoiceMonitoring.SalesInvoice.BillToCustomer,
                                    BankAccount = _locSalesInvoiceMonitoring.SalesInvoice.BankAccount,
                                    AccountNo = _locSalesInvoiceMonitoring.SalesInvoice.AccountNo,
                                    AccountName = _locSalesInvoiceMonitoring.SalesInvoice.AccountName,
                                    Account = _locAccountForCustomer,
                                    Credit = _locSalesInvoiceMonitoring.Bill,
                                    CloseDebit = true,
                                    SalesInvoice = _locSalesInvoiceMonitoring.SalesInvoice,
                                    SalesOrder = _locSalesInvoiceMonitoring.SalesOrderMonitoring.SalesOrder,
                                    SalesInvoiceMonitoring = _locSalesInvoiceMonitoring,
                                    ReceivableTransaction = _locReceivableTransactionXPO,
                                };
                                _saveDataReceivableTransactionLine2.Save();
                                _saveDataReceivableTransactionLine2.Session.CommitTransaction();
                                #endregion Customer

                                _locSalesInvoiceMonitoring.Status = Status.Posted;
                                _locSalesInvoiceMonitoring.StatusDate = now;
                                _locSalesInvoiceMonitoring.PostedCount = _locSalesInvoiceMonitoring.PostedCount + 1;
                                _locSalesInvoiceMonitoring.Save();
                                _locSalesInvoiceMonitoring.Session.CommitTransaction();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = ReceivableTransaction ", ex.ToString());
            }
        }

        private ChartOfAccount GetAccountFromBankAccountForCompany(Session _currSession, BankAccount _locBankAccountXPO, Company _locCompanyXPO)
        {
            ChartOfAccount _result = null;
           
            try
            {
                if(_locBankAccountXPO != null && _locCompanyXPO != null)
                {
                    #region JournalMapBankAccountGroupWithCompany
                    if (_locBankAccountXPO.BankAccountGroup != null )
                    {
                        XPCollection<JournalMap> _locJournalMaps = new XPCollection<JournalMap>
                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                               new BinaryOperator("BankAccountGroup", _locBankAccountXPO.BankAccountGroup)));

                        if (_locJournalMaps != null && _locJournalMaps.Count() > 0)
                        {
                            foreach (JournalMap _locJournalMap in _locJournalMaps)
                            {
                                XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("JournalMap", _locJournalMap)));

                                if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                {
                                    foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                    {
                                        AccountMap _locAccountMap = _currSession.FindObject<AccountMap>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _locJournalMapLine.AccountMap.Code),
                                                                     new BinaryOperator("PostingType", PostingType.Sales),
                                                                     new BinaryOperator("PostingMethod", PostingMethod.Bill),
                                                                     new BinaryOperator("PostingMethodType", PostingMethodType.Normal)));

                                        if (_locAccountMap != null)
                                        {
                                            XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("AccountMap", _locAccountMap),
                                                                                                new BinaryOperator("Active", true)));

                                            if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                            {
                                                

                                                foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                {
                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                    {
                                                        _result = _locAccountMapLine.Account;
                                                    } 
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    #endregion JournalMapBankAccountGroupWithCompany
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = ReceivableTransaction ", ex.ToString());
            }

            return _result;
        }

        private ChartOfAccount GetAccountFromBankAccountForCustomer(Session _currSession, BankAccount _locBankAccountXPO, BusinessPartner _locCustomerXPO)
        {
            ChartOfAccount _result = null;

            try
            {
                if (_locBankAccountXPO != null && _locCustomerXPO != null)
                {
                    #region JournalMapBankAccountGroupWithCompany
                    if (_locBankAccountXPO.BankAccountGroup != null)
                    {
                        XPCollection<JournalMap> _locJournalMaps = new XPCollection<JournalMap>
                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                               new BinaryOperator("BankAccountGroup", _locBankAccountXPO.BankAccountGroup)));

                        if (_locJournalMaps != null && _locJournalMaps.Count() > 0)
                        {
                            foreach (JournalMap _locJournalMap in _locJournalMaps)
                            {
                                XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("JournalMap", _locJournalMap)));

                                if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                {
                                    foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                    {
                                        AccountMap _locAccountMap = _currSession.FindObject<AccountMap>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _locJournalMapLine.AccountMap.Code),
                                                                     new BinaryOperator("PostingType", PostingType.Sales),
                                                                     new BinaryOperator("PostingMethod", PostingMethod.Bill),
                                                                     new BinaryOperator("PostingMethodType", PostingMethodType.Normal)));

                                        if (_locAccountMap != null)
                                        {
                                            XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("AccountMap", _locAccountMap),
                                                                                                new BinaryOperator("Active", true)));

                                            if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                            {
                                                foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                {
                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                    {
                                                        _result = _locAccountMapLine.Account;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    #endregion JournalMapBankAccountGroupWithCompany
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = ReceivableTransaction ", ex.ToString());
            }

            return _result;
        }

        #endregion GetSI

        #region GetSPI

        private void GetSalesPrePaymentInvoice(Session _currSession, SalesPrePaymentInvoice _locSalesPrePaymentInvoiceXPO, ReceivableTransaction _locReceivableTransactionXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                ChartOfAccount _locAccountForCompany = null;
                ChartOfAccount _locAccountForCustomer = null;
                if (_locSalesPrePaymentInvoiceXPO != null && _locReceivableTransactionXPO != null)
                {
                    if (_locSalesPrePaymentInvoiceXPO.Code != null)
                    {
                        XPCollection<SalesPrePaymentInvoice> _locSalesPrePaymentInvoices = new XPCollection<SalesPrePaymentInvoice>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("Code", _locSalesPrePaymentInvoiceXPO.Code),
                                                                                        new BinaryOperator("ReceivableTransaction", _locReceivableTransactionXPO),
                                                                                        new BinaryOperator("Select", true),
                                                                                        new BinaryOperator("Status", Status.Close, BinaryOperatorType.NotEqual)));

                        if (_locSalesPrePaymentInvoices != null && _locSalesPrePaymentInvoices.Count() > 0)
                        {
                            //Cek Total PIP dan Sales Order Maks Bill
                            foreach (SalesPrePaymentInvoice _locSalesPrePaymentInvoice in _locSalesPrePaymentInvoices)
                            {
                                if (_locSalesPrePaymentInvoice.SalesOrder != null)
                                {
                                    #region Company

                                    if (_locSalesPrePaymentInvoice.BankAccountCompany != null && _locSalesPrePaymentInvoice.Company != null)
                                    {
                                        _locAccountForCompany = GetAccountFromBankAccountForCompany(_currSession, _locSalesPrePaymentInvoice.BankAccountCompany, _locSalesPrePaymentInvoice.Company);
                                    }

                                    ReceivableTransactionLine _saveDataReceivableTransactionLine1 = new ReceivableTransactionLine(_currSession)
                                    {
                                        PostingDate = now,
                                        SalesType = PostingType.Sales,
                                        PostingMethod = PostingMethod.Bill,
                                        OpenCompany = true,
                                        Company = _locSalesPrePaymentInvoice.Company,
                                        BankAccount = _locSalesPrePaymentInvoice.BankAccountCompany,
                                        AccountNo = _locSalesPrePaymentInvoice.CompanyAccountNo,
                                        AccountName = _locSalesPrePaymentInvoice.CompanyAccountName,
                                        Account = _locAccountForCompany,
                                        Debit = _locSalesPrePaymentInvoice.DP_Amount,
                                        CloseCredit = true,
                                        SalesPrePaymentInvoice = _locSalesPrePaymentInvoice,
                                        SalesOrder = _locSalesPrePaymentInvoice.SalesOrder,
                                        ReceivableTransaction = _locReceivableTransactionXPO,
                                    };
                                    _saveDataReceivableTransactionLine1.Save();
                                    _saveDataReceivableTransactionLine1.Session.CommitTransaction();

                                    #endregion Company

                                    #region Customer

                                    if (_locSalesPrePaymentInvoice.BankAccount != null && _locSalesPrePaymentInvoice.SalesToCustomer != null)
                                    {
                                        _locAccountForCustomer = GetAccountFromBankAccountForCustomer(_currSession, _locSalesPrePaymentInvoice.BankAccount, _locSalesPrePaymentInvoice.SalesToCustomer);
                                    }
                                    ReceivableTransactionLine _saveDataReceivableTransactionLine2 = new ReceivableTransactionLine(_currSession)
                                    {
                                        PostingDate = now,
                                        SalesType = PostingType.Sales,
                                        PostingMethod = PostingMethod.Bill,
                                        OpenCustomer = true,
                                        Customer = _locSalesPrePaymentInvoice.BillToCustomer,
                                        BankAccount = _locSalesPrePaymentInvoice.BankAccount,
                                        AccountNo = _locSalesPrePaymentInvoice.AccountNo,
                                        AccountName = _locSalesPrePaymentInvoice.AccountName,
                                        Account = _locAccountForCustomer,
                                        Credit = _locSalesPrePaymentInvoice.DP_Amount,
                                        CloseDebit = true,
                                        SalesPrePaymentInvoice = _locSalesPrePaymentInvoice,
                                        SalesOrder = _locSalesPrePaymentInvoice.SalesOrder,
                                        ReceivableTransaction = _locReceivableTransactionXPO,
                                    };
                                    _saveDataReceivableTransactionLine2.Save();
                                    _saveDataReceivableTransactionLine2.Session.CommitTransaction();

                                    #endregion Customer

                                    _locSalesPrePaymentInvoice.Status = Status.Posted;
                                    _locSalesPrePaymentInvoice.StatusDate = now;
                                    _locSalesPrePaymentInvoice.PostedCount = _locSalesPrePaymentInvoice.PostedCount + 1;
                                    _locSalesPrePaymentInvoice.Save();
                                    _locSalesPrePaymentInvoice.Session.CommitTransaction();
                                }
                            }
                        }
                    }
                    //Cek 
                    //Harus mampu menghitung otomatis buat MultiTax dan MultiDiscount


                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = ReceivableTransaction ", ex.ToString());
            }
        }

        #endregion GetSPI

        #region Posting

        #region PostingSalesInvoice

        private bool CheckMaksAmountDebitBasedSalesInvoiceMonitoring(Session _currSession, ReceivableTransaction _locReceivableTransactionXPO)
        {
            bool _result = true;

            try
            {
                double _totDebit = 0;

                XPQuery<ReceivableTransactionLine> _receivableTransactionLineQuerys = new XPQuery<ReceivableTransactionLine>(_currSession);

                var _receivableTransactionLines = from rtl in _receivableTransactionLineQuerys
                                                  where (rtl.ReceivableTransaction == _locReceivableTransactionXPO
                                                  && rtl.Select == true
                                                  && (rtl.Status == Status.Progress || rtl.Status == Status.Posted))
                                                  group rtl by rtl.SalesInvoiceMonitoring into g
                                                  select new { SalesInvoiceMonitoring = g.Key };

                if (_receivableTransactionLines != null && _receivableTransactionLines.Count() > 0)
                {
                    foreach (var _receivableTransactionLine in _receivableTransactionLines)
                    {
                        if (_receivableTransactionLine.SalesInvoiceMonitoring != null)
                        {
                            XPCollection<ReceivableTransactionLine> _locReceivableTransactionLines = new XPCollection<ReceivableTransactionLine>
                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("ReceivableTransaction", _locReceivableTransactionXPO),
                                                                                            new BinaryOperator("SalesInvoiceMonitoring", _receivableTransactionLine.SalesInvoiceMonitoring),
                                                                                            new GroupOperator(GroupOperatorType.Or,
                                                                                            new BinaryOperator("Status", Status.Progress),
                                                                                            new BinaryOperator("Status", Status.Posted))));
                            if (_locReceivableTransactionLines != null && _locReceivableTransactionLines.Count() > 0)
                            {
                                foreach (ReceivableTransactionLine _locReceivableTransactionLine in _locReceivableTransactionLines)
                                {
                                    if (_locReceivableTransactionLine.CloseCredit == true && _locReceivableTransactionLine.Debit > 0)
                                    {
                                        _totDebit = _totDebit + _locReceivableTransactionLine.Debit;
                                    }

                                }

                                if (_totDebit != _receivableTransactionLine.SalesInvoiceMonitoring.Bill)
                                {
                                    _result = false;
                                }

                                _totDebit = 0;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = ReceivableTransaction ", ex.ToString());
            }

            return _result;
        }

        private bool CheckAmountDebitCreditBasedSalesInvoiceMonitoring(Session _currSession, ReceivableTransaction _locReceivableTransactionXPO)
        {
            bool _result = true;

            try
            {
                double _totDebit = 0;
                double _totCredit = 0;
                XPQuery<ReceivableTransactionLine> _receivableTransactionLineQuerys = new XPQuery<ReceivableTransactionLine>(_currSession);

                var _receivableTransactionLines = from rtl in _receivableTransactionLineQuerys
                                                  where (rtl.ReceivableTransaction == _locReceivableTransactionXPO
                                                  && rtl.Select == true
                                                  && (rtl.Status == Status.Progress || rtl.Status == Status.Posted))
                                                  group rtl by rtl.SalesInvoiceMonitoring into g
                                                  select new { SalesInvoiceMonitoring = g.Key };

                if (_receivableTransactionLines != null && _receivableTransactionLines.Count() > 0)
                {
                    foreach (var _receivableTransactionLine in _receivableTransactionLines)
                    {
                        if (_receivableTransactionLine.SalesInvoiceMonitoring != null)
                        {
                            XPCollection<ReceivableTransactionLine> _locReceivableTransactionLines = new XPCollection<ReceivableTransactionLine>
                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("ReceivableTransaction", _locReceivableTransactionXPO),
                                                                                            new BinaryOperator("SalesInvoiceMonitoring", _receivableTransactionLine.SalesInvoiceMonitoring),
                                                                                            new GroupOperator(GroupOperatorType.Or,
                                                                                            new BinaryOperator("Status", Status.Progress),
                                                                                            new BinaryOperator("Status", Status.Posted))));
                            if (_locReceivableTransactionLines != null && _locReceivableTransactionLines.Count() > 0)
                            {
                                foreach (ReceivableTransactionLine _locReceivableTransactionLine in _locReceivableTransactionLines)
                                {
                                    if (_locReceivableTransactionLine.CloseCredit == true && _locReceivableTransactionLine.Debit > 0)
                                    {
                                        _totDebit = _totDebit + _locReceivableTransactionLine.Debit;
                                    }
                                    else if (_locReceivableTransactionLine.CloseDebit == true && _locReceivableTransactionLine.Credit > 0)
                                    {
                                        _totCredit = _totCredit + _locReceivableTransactionLine.Credit;
                                    }
                                }

                                if (_totDebit != _totCredit)
                                {
                                    _result = false;
                                }

                                _totDebit = 0;
                                _totCredit = 0;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = ReceivableTransaction ", ex.ToString());
            }

            return _result;
        }

        private void SetReceivableTransactionMonitoringBasedSalesInvoiceMonitoring(Session _currSession, ReceivableTransaction _locReceivableTransactionXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                double _totDebit = 0;
                double _totCredit = 0;
                string _currSignCode = null;
                DateTime now = DateTime.Now;

                XPQuery<ReceivableTransactionLine> _receivableTransactionLineQuerys = new XPQuery<ReceivableTransactionLine>(_currSession);

                var _receivableTransactionLines = from rtl in _receivableTransactionLineQuerys
                                                  where (rtl.ReceivableTransaction == _locReceivableTransactionXPO
                                                  && rtl.Select == true
                                                  && (rtl.Status == Status.Progress || rtl.Status == Status.Posted))
                                                  group rtl by rtl.SalesInvoiceMonitoring into g
                                                  select new { SalesInvoiceMonitoring = g.Key };

                if (_receivableTransactionLines != null && _receivableTransactionLines.Count() > 0)
                {
                    foreach (var _receivableTransactionLine in _receivableTransactionLines)
                    {
                        if (_receivableTransactionLine.SalesInvoiceMonitoring != null)
                        {
                            XPCollection<ReceivableTransactionLine> _locReceivableTransactionLines = new XPCollection<ReceivableTransactionLine>
                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("ReceivableTransaction", _locReceivableTransactionXPO),
                                                                                            new BinaryOperator("SalesInvoiceMonitoring", _receivableTransactionLine.SalesInvoiceMonitoring),
                                                                                            new GroupOperator(GroupOperatorType.Or,
                                                                                            new BinaryOperator("Status", Status.Progress),
                                                                                            new BinaryOperator("Status", Status.Posted))));

                            if (_locReceivableTransactionLines != null && _locReceivableTransactionLines.Count() > 0)
                            {
                                foreach (ReceivableTransactionLine _locReceivableTransactionLine in _locReceivableTransactionLines)
                                {
                                    if (_locReceivableTransactionLine.CloseCredit == true && _locReceivableTransactionLine.Debit > 0)
                                    {
                                        _totDebit = _totDebit + _locReceivableTransactionLine.Debit;
                                    }
                                    else if (_locReceivableTransactionLine.CloseDebit == true && _locReceivableTransactionLine.Credit > 0)
                                    {
                                        _totCredit = _totCredit + _locReceivableTransactionLine.Credit;
                                    }
                                }

                                _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.ReceivableTransactionMonitoring);

                                if (_currSignCode != null)
                                {
                                    ReceivableTransactionMonitoring _saveDataReceivableTransactionMonitoring = new ReceivableTransactionMonitoring(_currSession)
                                    {
                                        SignCode = _currSignCode,
                                        ReceivableTransaction = _locReceivableTransactionXPO,
                                        SalesInvoiceMonitoring = _receivableTransactionLine.SalesInvoiceMonitoring,
                                        SalesInvoice = _receivableTransactionLine.SalesInvoiceMonitoring.SalesInvoice,
                                        SalesOrder = _receivableTransactionLine.SalesInvoiceMonitoring.SalesOrderMonitoring.SalesOrder,
                                        Currency = _locReceivableTransactionXPO.Currency,
                                        TotAmountDebit = _totDebit,
                                        TotAmountCredit = _totCredit,
                                    };
                                    _saveDataReceivableTransactionMonitoring.Save();
                                    _saveDataReceivableTransactionMonitoring.Session.CommitTransaction();

                                    ReceivableTransactionMonitoring _locReceivableTransactionMonitoring = _currSession.FindObject<ReceivableTransactionMonitoring>
                                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                                    new BinaryOperator("SignCode", _currSignCode)));
                                    if (_locReceivableTransactionMonitoring != null)
                                    {
                                        #region PaymentInPlan

                                        double _locReceived = 0;
                                        double _locOutstanding = 0;
                                        Status _locStatus = Status.Open;
                                        PaymentInPlan _locPaymentInPlan = _currSession.FindObject<PaymentInPlan>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("SalesInvoiceMonitoring", _locReceivableTransactionMonitoring.SalesInvoiceMonitoring)));
                                        if (_locPaymentInPlan != null)
                                        {
                                            _locReceived = _locReceivableTransactionMonitoring.TotAmountCredit;
                                            _locOutstanding = _locPaymentInPlan.Plan - _locReceivableTransactionMonitoring.TotAmountCredit;
                                            if (_locOutstanding == 0)
                                            {
                                                _locStatus = Status.Close;
                                            }
                                            else
                                            {
                                                _locStatus = Status.Posted;
                                            }

                                            _locPaymentInPlan.Received = _locPaymentInPlan.Received + _locReceived;
                                            _locPaymentInPlan.Outstanding = _locOutstanding;
                                            _locPaymentInPlan.Status = _locStatus;
                                            _locPaymentInPlan.StatusDate = now;
                                            _locPaymentInPlan.Save();
                                            _locPaymentInPlan.Session.CommitTransaction();
                                        }

                                        #endregion PaymentInPlan

                                        #region SalesInvoiceMonitoring

                                        if (_locReceivableTransactionMonitoring.SalesInvoiceMonitoring != null)
                                        {
                                            if (_locReceivableTransactionMonitoring.SalesInvoiceMonitoring.Code != null)
                                            {
                                                SalesInvoiceMonitoring _locSalesInvoiceMonitoring = _currSession.FindObject<SalesInvoiceMonitoring>
                                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("Code", _locReceivableTransactionMonitoring.SalesInvoiceMonitoring.Code)));
                                                if (_locSalesInvoiceMonitoring != null)
                                                {
                                                    if (_locSalesInvoiceMonitoring.TAmount == _locReceivableTransactionMonitoring.TotAmountCredit)
                                                    {
                                                        _locSalesInvoiceMonitoring.Status = Status.Close;
                                                        _locSalesInvoiceMonitoring.StatusDate = now;
                                                        _locSalesInvoiceMonitoring.Save();
                                                        _locSalesInvoiceMonitoring.Session.CommitTransaction();
                                                    }
                                                    else
                                                    {
                                                        _locSalesInvoiceMonitoring.Status = Status.Posted;
                                                        _locSalesInvoiceMonitoring.StatusDate = now;
                                                        _locSalesInvoiceMonitoring.Save();
                                                        _locSalesInvoiceMonitoring.Session.CommitTransaction();
                                                    }
                                                }
                                            }
                                        }

                                        #endregion SalesInvoiceMonitoring
                                    }
                                }
                                _totDebit = 0;
                                _totCredit = 0;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = ReceivableTransaction ", ex.ToString());
            }

        }

        private void SetReceivableJournalBasedSalesInvoiceMonitoring(Session _currSession, ReceivableTransaction _locReceivableTransactionXPO)
        {
            try
            {
                DateTime now = DateTime.Now;

                SalesOrder _locSalesOrder = null;
                SalesInvoice _locSalesInvoice = null;

                if (_locReceivableTransactionXPO != null)
                {
                    #region JournalReceivableBankAccountByCompany

                    if (_locReceivableTransactionXPO.CompanyDefault != null)
                    {
                        double _locTotDebit = 0;
                        double _locTotCredit = 0;
                        XPQuery<ReceivableTransactionLine> _receivableTransactionLineQuery1 = new XPQuery<ReceivableTransactionLine>(_currSession);

                        var _receivableTransactionLine1s = from rtl in _receivableTransactionLineQuery1
                                                           where (rtl.ReceivableTransaction == _locReceivableTransactionXPO
                                                           && rtl.Company == _locReceivableTransactionXPO.CompanyDefault
                                                           && rtl.Select == true
                                                           && (rtl.Status == Status.Progress || rtl.Status == Status.Posted))
                                                           group rtl by rtl.SalesInvoiceMonitoring into g
                                                           select new { SalesInvoiceMonitoring = g.Key };

                        if (_receivableTransactionLine1s != null && _receivableTransactionLine1s.Count() > 0)
                        {
                            foreach (var _receivableTransactionLine1 in _receivableTransactionLine1s)
                            {
                                if (_receivableTransactionLine1.SalesInvoiceMonitoring != null)
                                {
                                    XPQuery<ReceivableTransactionLine> _receivableTransactionLineQuery1a = new XPQuery<ReceivableTransactionLine>(_currSession);

                                    var _receivableTransactionLine1as = from rtl in _receivableTransactionLineQuery1a
                                                                        where (rtl.ReceivableTransaction == _locReceivableTransactionXPO
                                                                        && rtl.Company == _locReceivableTransactionXPO.CompanyDefault
                                                                        && rtl.SalesInvoiceMonitoring == _receivableTransactionLine1.SalesInvoiceMonitoring
                                                                        && rtl.Select == true
                                                                        && (rtl.Status == Status.Progress || rtl.Status == Status.Posted))
                                                                        group rtl by rtl.BankAccount into g
                                                                        select new { BankAccount = g.Key };

                                    if (_receivableTransactionLine1as != null && _receivableTransactionLine1as.Count() > 0)
                                    {
                                        foreach (var _receivableTransactionLine1a in _receivableTransactionLine1as)
                                        {
                                            if (_receivableTransactionLine1a.BankAccount != null)
                                            {
                                                XPCollection<ReceivableTransactionLine> _locReceivableTransactionLines = new XPCollection<ReceivableTransactionLine>
                                                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                new BinaryOperator("ReceivableTransaction", _locReceivableTransactionXPO),
                                                                                                                new BinaryOperator("SalesInvoiceMonitoring", _receivableTransactionLine1.SalesInvoiceMonitoring),
                                                                                                                new BinaryOperator("Company", _locReceivableTransactionXPO.CompanyDefault),
                                                                                                                new BinaryOperator("Select", true),
                                                                                                                new BinaryOperator("BankAccount", _receivableTransactionLine1a.BankAccount),
                                                                                                                new GroupOperator(GroupOperatorType.Or,
                                                                                                                new BinaryOperator("Status", Status.Progress),
                                                                                                                new BinaryOperator("Status", Status.Posted))));

                                                if (_locReceivableTransactionLines != null && _locReceivableTransactionLines.Count() > 0)
                                                {
                                                    foreach (ReceivableTransactionLine _locReceivableTransactionLine in _locReceivableTransactionLines)
                                                    {
                                                        if (_locReceivableTransactionLine.OpenCompany == true && _locReceivableTransactionLine.CloseCredit == true && _locReceivableTransactionLine.Debit > 0)
                                                        {
                                                            _locTotDebit = _locTotDebit + _locReceivableTransactionLine.Debit;
                                                        }
                                                        if (_locReceivableTransactionLine.SalesInvoiceMonitoring != null)
                                                        {
                                                            if (_locReceivableTransactionLine.SalesInvoiceMonitoring.SalesOrderMonitoring != null)
                                                            {
                                                                if (_locReceivableTransactionLine.SalesInvoiceMonitoring.SalesOrderMonitoring.SalesOrder != null)
                                                                {
                                                                    _locSalesOrder = _locReceivableTransactionLine.SalesInvoiceMonitoring.SalesOrderMonitoring.SalesOrder;
                                                                }

                                                            }
                                                            if (_locReceivableTransactionLine.SalesInvoiceMonitoring.SalesInvoice != null)
                                                            {
                                                                _locSalesInvoice = _locReceivableTransactionLine.SalesInvoiceMonitoring.SalesInvoice;
                                                            }
                                                        }
                                                    }

                                                    #region JournalMapBankAccountGroup

                                                    if (_receivableTransactionLine1a.BankAccount.BankAccountGroup != null)
                                                    {
                                                        XPCollection<JournalMap> _locJournalMaps = new XPCollection<JournalMap>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("BankAccountGroup", _receivableTransactionLine1a.BankAccount.BankAccountGroup)));

                                                        if (_locJournalMaps != null && _locJournalMaps.Count() > 0)
                                                        {
                                                            foreach (JournalMap _locJournalMap in _locJournalMaps)
                                                            {
                                                                XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                    new BinaryOperator("JournalMap", _locJournalMap)));

                                                                if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                                {
                                                                    foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                                    {
                                                                        AccountMap _locAccountMap = _currSession.FindObject<AccountMap>
                                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                                     new BinaryOperator("Code", _locJournalMapLine.AccountMap.Code),
                                                                                                     new BinaryOperator("PostingType", PostingType.Sales),
                                                                                                     new BinaryOperator("PostingMethod", PostingMethod.Bill),
                                                                                                     new BinaryOperator("PostingMethodType", PostingMethodType.Normal)));

                                                                        if (_locAccountMap != null)
                                                                        {
                                                                            XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                                new BinaryOperator("AccountMap", _locAccountMap),
                                                                                                                                new BinaryOperator("Active", true)));

                                                                            if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                            {
                                                                                double _locTotalAmountDebit = 0;
                                                                                double _locTotalAmountCredit = 0;
                                                                                double _locTotalBalance = 0;

                                                                                foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                                {
                                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                                    {
                                                                                        _locTotalAmountDebit = _locTotDebit;
                                                                                    }
                                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                                    {
                                                                                        _locTotalAmountCredit = _locTotCredit;
                                                                                    }

                                                                                    GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                                    {
                                                                                        PostingDate = now,
                                                                                        PostingType = PostingType.Sales,
                                                                                        PostingMethod = PostingMethod.Bill,
                                                                                        PostingMethodType = PostingMethodType.Normal,
                                                                                        Account = _locAccountMapLine.Account,
                                                                                        Debit = _locTotalAmountDebit,
                                                                                        Credit = _locTotalAmountCredit,
                                                                                        SalesOrder = _locSalesOrder,
                                                                                        SalesInvoice = _locSalesInvoice,
                                                                                        ReceivableTransaction = _locReceivableTransactionXPO,
                                                                                        Company = _locReceivableTransactionXPO.CompanyDefault,
                                                                                    };
                                                                                    _saveGeneralJournal.Save();
                                                                                    _saveGeneralJournal.Session.CommitTransaction();

                                                                                    if (_locAccountMapLine.Account.Code != null)
                                                                                    {
                                                                                        ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                                                 (new GroupOperator(GroupOperatorType.And,
                                                                                                                  new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                                                                                        if (_locCOA != null)
                                                                                        {
                                                                                            if (_locCOA.BalanceType == BalanceType.Change)
                                                                                            {
                                                                                                if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                                                {
                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                                                    }
                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                                                    }

                                                                                                }
                                                                                                if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                                                {
                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                                                    }
                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                                                    }
                                                                                                }
                                                                                            }

                                                                                            _locCOA.Balance = _locTotalBalance;
                                                                                            _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                                            _locCOA.Save();
                                                                                            _locCOA.Session.CommitTransaction();
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            ErrorMessageShow("Data Journal Map Not Available");
                                                        }
                                                    }


                                                    #endregion JournalMapBankAccountGroup

                                                    _locTotDebit = 0;

                                                }
                                            }
                                        }
                                    }
                                }

                            }
                        }
                    }
                    //Buat GroupBy SalesInvoiceMonitoring


                    #endregion JournalReceivableBankAccountByCompany

                    #region JournalReceivableByBusinessPartner

                    if (_locReceivableTransactionXPO.BillToCustomer != null && _locReceivableTransactionXPO.SalesToCustomer != null)
                    {
                        double _locTotDebit = 0;
                        double _locTotCredit = 0;
                        //Total Credit dari BillToCustomer
                        XPQuery<ReceivableTransactionLine> _receivableTransactionLineQuery2 = new XPQuery<ReceivableTransactionLine>(_currSession);

                        var _receivableTransactionLine2s = from rtl in _receivableTransactionLineQuery2
                                                           where (rtl.ReceivableTransaction == _locReceivableTransactionXPO
                                                           && rtl.Select == true
                                                           && rtl.Customer == _locReceivableTransactionXPO.BillToCustomer
                                                           && (rtl.Status == Status.Progress || rtl.Status == Status.Posted))
                                                           group rtl by rtl.SalesInvoiceMonitoring into g
                                                           select new { SalesInvoiceMonitoring = g.Key };

                        if (_receivableTransactionLine2s != null && _receivableTransactionLine2s.Count() > 0)
                        {
                            foreach (var _receivableTransactionLine2 in _receivableTransactionLine2s)
                            {
                                if (_receivableTransactionLine2.SalesInvoiceMonitoring != null)
                                {
                                    XPCollection<ReceivableTransactionLine> _locReceivableTransactionLines = new XPCollection<ReceivableTransactionLine>
                                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                    new BinaryOperator("ReceivableTransaction", _locReceivableTransactionXPO),
                                                                                                    new BinaryOperator("Select", true),
                                                                                                    new BinaryOperator("Customer", _locReceivableTransactionXPO.BillToCustomer),
                                                                                                    new BinaryOperator("SalesInvoiceMonitoring", _receivableTransactionLine2.SalesInvoiceMonitoring),
                                                                                                    new GroupOperator(GroupOperatorType.Or,
                                                                                                    new BinaryOperator("Status", Status.Progress),
                                                                                                    new BinaryOperator("Status", Status.Posted))));
                                    if (_locReceivableTransactionLines != null && _locReceivableTransactionLines.Count() > 0)
                                    {
                                        foreach (ReceivableTransactionLine _locReceivableTransactionLine in _locReceivableTransactionLines)
                                        {
                                            if (_locReceivableTransactionLine.OpenCustomer == true && _locReceivableTransactionLine.CloseDebit == true && _locReceivableTransactionLine.Credit > 0)
                                            {
                                                _locTotCredit = _locTotCredit + _locReceivableTransactionLine.Credit;
                                            }

                                            if (_locReceivableTransactionLine.SalesInvoiceMonitoring != null)
                                            {
                                                if (_locReceivableTransactionLine.SalesInvoiceMonitoring.SalesOrderMonitoring != null)
                                                {
                                                    if (_locReceivableTransactionLine.SalesInvoiceMonitoring.SalesOrderMonitoring.SalesOrder != null)
                                                    {
                                                        _locSalesOrder = _locReceivableTransactionLine.SalesInvoiceMonitoring.SalesOrderMonitoring.SalesOrder;
                                                    }

                                                }
                                                if (_locReceivableTransactionLine.SalesInvoiceMonitoring.SalesInvoice != null)
                                                {
                                                    _locSalesInvoice = _locReceivableTransactionLine.SalesInvoiceMonitoring.SalesInvoice;
                                                }
                                            }
                                        }

                                        #region JournalMapBusinessPartnerAcccountGroup

                                        if (_locReceivableTransactionXPO.SalesToCustomer.BusinessPartnerAccountGroup != null)
                                        {
                                            XPCollection<JournalMap> _locJournalMaps = new XPCollection<JournalMap>
                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("BusinessPartnerAccountGroup", _locReceivableTransactionXPO.SalesToCustomer.BusinessPartnerAccountGroup)));

                                            if (_locJournalMaps != null && _locJournalMaps.Count() > 0)
                                            {
                                                foreach (JournalMap _locJournalMap in _locJournalMaps)
                                                {
                                                    XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                        new BinaryOperator("JournalMap", _locJournalMap)));

                                                    if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                    {
                                                        foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                        {
                                                            AccountMap _locAccountMap = _currSession.FindObject<AccountMap>
                                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                                         new BinaryOperator("Code", _locJournalMapLine.AccountMap.Code),
                                                                                         new BinaryOperator("PostingType", PostingType.Sales),
                                                                                         new BinaryOperator("PostingMethod", PostingMethod.Bill),
                                                                                         new BinaryOperator("PostingMethodType", PostingMethodType.Normal)));

                                                            if (_locAccountMap != null)
                                                            {
                                                                XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                    new BinaryOperator("AccountMap", _locAccountMap),
                                                                                                                    new BinaryOperator("Active", true)));

                                                                if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                {
                                                                    double _locTotalAmountDebit = 0;
                                                                    double _locTotalAmountCredit = 0;
                                                                    double _locTotalBalance = 0;

                                                                    foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                    {
                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                        {
                                                                            _locTotalAmountDebit = _locTotDebit;
                                                                        }
                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                        {
                                                                            _locTotalAmountCredit = _locTotCredit;
                                                                        }

                                                                        GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                        {
                                                                            PostingDate = now,
                                                                            PostingType = PostingType.Sales,
                                                                            PostingMethod = PostingMethod.Bill,
                                                                            PostingMethodType = PostingMethodType.Normal,
                                                                            Account = _locAccountMapLine.Account,
                                                                            Debit = _locTotalAmountDebit,
                                                                            Credit = _locTotalAmountCredit,
                                                                            SalesOrder = _locSalesOrder,
                                                                            SalesInvoice = _locSalesInvoice,
                                                                            ReceivableTransaction = _locReceivableTransactionXPO,
                                                                            Company = _locReceivableTransactionXPO.CompanyDefault,
                                                                        };
                                                                        _saveGeneralJournal.Save();
                                                                        _saveGeneralJournal.Session.CommitTransaction();

                                                                        if (_locAccountMapLine.Account.Code != null)
                                                                        {
                                                                            ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                                     (new GroupOperator(GroupOperatorType.And,
                                                                                                      new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                                                                            if (_locCOA != null)
                                                                            {
                                                                                if (_locCOA.BalanceType == BalanceType.Change)
                                                                                {
                                                                                    if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                                    {
                                                                                        if (_locTotalAmountDebit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                                        }
                                                                                        if (_locTotalAmountCredit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                                        }
                                                                                    }
                                                                                    if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                                    {
                                                                                        if (_locTotalAmountDebit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                                        }
                                                                                        if (_locTotalAmountCredit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                                        }
                                                                                    }
                                                                                }

                                                                                _locCOA.Balance = _locTotalBalance;
                                                                                _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                                _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                                _locCOA.Save();
                                                                                _locCOA.Session.CommitTransaction();
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        #endregion JournalMapBankAccountGroup

                                        _locTotCredit = 0;
                                    }
                                }
                            }

                        }

                    }


                    #endregion JournalReceivableByBusinessPartner
                }
                else
                {
                    ErrorMessageShow("Data Receivable Transaction Not Available");
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = SalesInvoice ", ex.ToString());
            }
        }

        private void SetNormalSalesInvoiceMonitoring(Session _currSession, ReceivableTransaction _locReceivableTransactionXPO)
        {
            try
            {
                XPQuery<ReceivableTransactionLine> _receivableTransactionLineQuery1 = new XPQuery<ReceivableTransactionLine>(_currSession);

                var _receivableTransactionLine1s = from rtl in _receivableTransactionLineQuery1
                                                   where (rtl.ReceivableTransaction == _locReceivableTransactionXPO
                                                   && rtl.Select == true
                                                   && (rtl.Status == Status.Progress || rtl.Status == Status.Posted))
                                                   group rtl by rtl.SalesInvoiceMonitoring into g
                                                   select new { SalesInvoiceMonitoring = g.Key };
                if (_receivableTransactionLine1s != null && _receivableTransactionLine1s.Count() > 0)
                {
                    foreach (var _receivableTransactionLine1 in _receivableTransactionLine1s)
                    {
                        if (_receivableTransactionLine1.SalesInvoiceMonitoring != null)
                        {
                            if (_receivableTransactionLine1.SalesInvoiceMonitoring.Code != null)
                            {
                                SalesInvoiceMonitoring _locSalesInvoiceMonitoring = _currSession.FindObject<SalesInvoiceMonitoring>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Code", _receivableTransactionLine1.SalesInvoiceMonitoring.Code)));
                                if (_locSalesInvoiceMonitoring != null)
                                {
                                    _locSalesInvoiceMonitoring.ReceivableTransaction = null;
                                    _locSalesInvoiceMonitoring.Save();
                                    _locSalesInvoiceMonitoring.Session.CommitTransaction();
                                }
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = SalesInvoice ", ex.ToString());
            }
        }

        #endregion PostingSalesInvoice

        #region PostingSalesPrePaymentInvoice

        private bool CheckMaksAmountDebitBasedSalesPrePaymentInvoice(Session _currSession, ReceivableTransaction _locReceivableTransactionXPO)
        {
            bool _result = true;

            try
            {
                double _totDebit = 0;

                XPQuery<ReceivableTransactionLine> _receivableTransactionLineQuerys = new XPQuery<ReceivableTransactionLine>(_currSession);

                var _receivableTransactionLines = from rtl in _receivableTransactionLineQuerys
                                                  where (rtl.ReceivableTransaction == _locReceivableTransactionXPO
                                                  && rtl.Select == true
                                                  && (rtl.Status == Status.Progress || rtl.Status == Status.Posted))
                                                  group rtl by rtl.SalesPrePaymentInvoice into g
                                                  select new { SalesPrePaymentInvoice = g.Key };

                if (_receivableTransactionLines != null && _receivableTransactionLines.Count() > 0)
                {
                    foreach (var _receivableTransactionLine in _receivableTransactionLines)
                    {
                        if (_receivableTransactionLine.SalesPrePaymentInvoice != null)
                        {
                            XPCollection<ReceivableTransactionLine> _locReceivableTransactionLines = new XPCollection<ReceivableTransactionLine>
                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("ReceivableTransaction", _locReceivableTransactionXPO),
                                                                                            new BinaryOperator("SalesPrePaymentInvoice", _receivableTransactionLine.SalesPrePaymentInvoice),
                                                                                            new GroupOperator(GroupOperatorType.Or,
                                                                                            new BinaryOperator("Status", Status.Progress),
                                                                                            new BinaryOperator("Status", Status.Posted))));
                            if (_locReceivableTransactionLines != null && _locReceivableTransactionLines.Count() > 0)
                            {
                                foreach (ReceivableTransactionLine _locReceivableTransactionLine in _locReceivableTransactionLines)
                                {
                                    if (_locReceivableTransactionLine.CloseCredit == true && _locReceivableTransactionLine.Debit > 0)
                                    {
                                        _totDebit = _totDebit + _locReceivableTransactionLine.Debit;
                                    }

                                }

                                if (_totDebit != _receivableTransactionLine.SalesPrePaymentInvoice.DP_Amount)
                                {
                                    _result = false;
                                }

                                _totDebit = 0;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = ReceivableTransaction ", ex.ToString());
            }

            return _result;
        }

        private bool CheckAmountDebitCreditBasedSalesPrePaymentInvoice(Session _currSession, ReceivableTransaction _locReceivableTransactionXPO)
        {
            bool _result = true;

            try
            {
                double _totDebit = 0;
                double _totCredit = 0;
                XPQuery<ReceivableTransactionLine> _receivableTransactionLineQuerys = new XPQuery<ReceivableTransactionLine>(_currSession);

                var _receivableTransactionLines = from rtl in _receivableTransactionLineQuerys
                                                  where (rtl.ReceivableTransaction == _locReceivableTransactionXPO
                                                  && rtl.Select == true
                                                  && (rtl.Status == Status.Progress || rtl.Status == Status.Posted))
                                                  group rtl by rtl.SalesPrePaymentInvoice into g
                                                  select new { SalesPrePaymentInvoice = g.Key };

                if (_receivableTransactionLines != null && _receivableTransactionLines.Count() > 0)
                {
                    foreach (var _receivableTransactionLine in _receivableTransactionLines)
                    {
                        if (_receivableTransactionLine.SalesPrePaymentInvoice != null)
                        {
                            XPCollection<ReceivableTransactionLine> _locReceivableTransactionLines = new XPCollection<ReceivableTransactionLine>
                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("ReceivableTransaction", _locReceivableTransactionXPO),
                                                                                            new BinaryOperator("SalesPrePaymentInvoice", _receivableTransactionLine.SalesPrePaymentInvoice),
                                                                                            new GroupOperator(GroupOperatorType.Or,
                                                                                            new BinaryOperator("Status", Status.Progress),
                                                                                            new BinaryOperator("Status", Status.Posted))));
                            if (_locReceivableTransactionLines != null && _locReceivableTransactionLines.Count() > 0)
                            {
                                foreach (ReceivableTransactionLine _locReceivableTransactionLine in _locReceivableTransactionLines)
                                {
                                    if (_locReceivableTransactionLine.CloseCredit == true && _locReceivableTransactionLine.Debit > 0)
                                    {
                                        _totDebit = _totDebit + _locReceivableTransactionLine.Debit;
                                    }
                                    else if (_locReceivableTransactionLine.CloseDebit == true && _locReceivableTransactionLine.Credit > 0)
                                    {
                                        _totCredit = _totCredit + _locReceivableTransactionLine.Credit;
                                    }
                                }

                                if (_totDebit != _totCredit)
                                {
                                    _result = false;
                                }
                                _totDebit = 0;
                                _totCredit = 0;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = ReceivableTransaction ", ex.ToString());
            }

            return _result;
        }

        private void SetReceivableTransactionMonitoringBasedSalesPrePaymentInvoice(Session _currSession, ReceivableTransaction _locReceivableTransactionXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                double _totDebit = 0;
                double _totCredit = 0;
                string _currSignCode = null;
                DateTime now = DateTime.Now;

                XPQuery<ReceivableTransactionLine> _receivableTransactionLineQuerys = new XPQuery<ReceivableTransactionLine>(_currSession);

                var _receivableTransactionLines = from rtl in _receivableTransactionLineQuerys
                                                  where (rtl.ReceivableTransaction == _locReceivableTransactionXPO
                                                  && rtl.Select == true
                                                  && (rtl.Status == Status.Progress || rtl.Status == Status.Posted))
                                                  group rtl by rtl.SalesPrePaymentInvoice into g
                                                  select new { SalesPrePaymentInvoice = g.Key };

                if (_receivableTransactionLines != null && _receivableTransactionLines.Count() > 0)
                {
                    foreach (var _receivableTransactionLine in _receivableTransactionLines)
                    {
                        if (_receivableTransactionLine.SalesPrePaymentInvoice != null)
                        {
                            XPCollection<ReceivableTransactionLine> _locReceivableTransactionLines = new XPCollection<ReceivableTransactionLine>
                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("ReceivableTransaction", _locReceivableTransactionXPO),
                                                                                            new BinaryOperator("SalesPrePaymentInvoice", _receivableTransactionLine.SalesPrePaymentInvoice),
                                                                                            new GroupOperator(GroupOperatorType.Or,
                                                                                            new BinaryOperator("Status", Status.Progress),
                                                                                            new BinaryOperator("Status", Status.Posted))));

                            if (_locReceivableTransactionLines != null && _locReceivableTransactionLines.Count() > 0)
                            {
                                foreach (ReceivableTransactionLine _locReceivableTransactionLine in _locReceivableTransactionLines)
                                {
                                    if (_locReceivableTransactionLine.CloseCredit == true && _locReceivableTransactionLine.Debit > 0)
                                    {
                                        _totDebit = _totDebit + _locReceivableTransactionLine.Debit;
                                    }
                                    else if (_locReceivableTransactionLine.CloseDebit == true && _locReceivableTransactionLine.Credit > 0)
                                    {
                                        _totCredit = _totCredit + _locReceivableTransactionLine.Credit;
                                    }
                                }

                                _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.ReceivableTransactionMonitoring);

                                if (_currSignCode != null)
                                {
                                    ReceivableTransactionMonitoring _saveDataReceivableTransactionMonitoring = new ReceivableTransactionMonitoring(_currSession)
                                    {
                                        SignCode = _currSignCode,
                                        ReceivableTransaction = _locReceivableTransactionXPO,
                                        SalesPrePaymentInvoice = _receivableTransactionLine.SalesPrePaymentInvoice,
                                        SalesOrder = _receivableTransactionLine.SalesPrePaymentInvoice.SalesOrder,
                                        Currency = _locReceivableTransactionXPO.Currency,
                                        TotAmountDebit = _totDebit,
                                        TotAmountCredit = _totCredit,
                                    };
                                    _saveDataReceivableTransactionMonitoring.Save();
                                    _saveDataReceivableTransactionMonitoring.Session.CommitTransaction();

                                    ReceivableTransactionMonitoring _locReceivableTransactionMonitoring = _currSession.FindObject<ReceivableTransactionMonitoring>
                                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                                    new BinaryOperator("SignCode", _currSignCode)));
                                    if (_locReceivableTransactionMonitoring != null)
                                    {
                                        double _locReceived = 0;
                                        double _locOutstanding = 0;
                                        Status _locStatus = Status.Open;
                                        PaymentInPlan _locPaymentInPlan = _currSession.FindObject<PaymentInPlan>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("SalesPrePaymentInvoice", _locReceivableTransactionMonitoring.SalesPrePaymentInvoice)));
                                        if (_locPaymentInPlan != null)
                                        {
                                            _locReceived = _locReceivableTransactionMonitoring.TotAmountCredit;
                                            _locOutstanding = _locPaymentInPlan.Plan - _locReceivableTransactionMonitoring.TotAmountCredit;
                                            if (_locOutstanding == 0)
                                            {
                                                _locStatus = Status.Close;
                                            }
                                            else
                                            {
                                                _locStatus = Status.Posted;
                                            }

                                            _locPaymentInPlan.Received = _locReceived;
                                            _locPaymentInPlan.Outstanding = _locOutstanding;
                                            _locPaymentInPlan.Status = _locStatus;
                                            _locPaymentInPlan.StatusDate = now;
                                        }

                                        if (_locReceivableTransactionMonitoring.SalesPrePaymentInvoice != null)
                                        {
                                            if (_locReceivableTransactionMonitoring.SalesPrePaymentInvoice.Code != null)
                                            {
                                                SalesPrePaymentInvoice _locSalesPrePaymentInvoice = _currSession.FindObject<SalesPrePaymentInvoice>
                                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("Code", _locReceivableTransactionMonitoring.SalesPrePaymentInvoice.Code)));
                                                if (_locSalesPrePaymentInvoice != null)
                                                {
                                                    if (_locSalesPrePaymentInvoice.DP_Amount == _locReceivableTransactionMonitoring.TotAmountCredit)
                                                    {
                                                        _locSalesPrePaymentInvoice.Status = Status.Close;
                                                        _locSalesPrePaymentInvoice.StatusDate = now;
                                                        _locSalesPrePaymentInvoice.Save();
                                                        _locSalesPrePaymentInvoice.Session.CommitTransaction();
                                                    }
                                                    else
                                                    {
                                                        _locSalesPrePaymentInvoice.Status = Status.Posted;
                                                        _locSalesPrePaymentInvoice.StatusDate = now;
                                                        _locSalesPrePaymentInvoice.Save();
                                                        _locSalesPrePaymentInvoice.Session.CommitTransaction();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                _totDebit = 0;
                                _totCredit = 0;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = ReceivableTransaction ", ex.ToString());
            }

        }

        private void SetReceivableJournalBasedSalesPrePaymentInvoice(Session _currSession, ReceivableTransaction _locReceivableTransactionXPO)
        {
            try
            {
                DateTime now = DateTime.Now;

                SalesOrder _locSalesOrder = null;
                SalesPrePaymentInvoice _locSalesPrePaymentInvoice = null;

                if (_locReceivableTransactionXPO != null)
                {
                    #region JournalReceivableBankAccountByCompany

                    if (_locReceivableTransactionXPO.CompanyDefault != null)
                    {
                        double _locTotDebit = 0;
                        double _locTotCredit = 0;
                        XPQuery<ReceivableTransactionLine> _receivableTransactionLineQuery1 = new XPQuery<ReceivableTransactionLine>(_currSession);

                        var _receivableTransactionLine1s = from rtl in _receivableTransactionLineQuery1
                                                           where (rtl.ReceivableTransaction == _locReceivableTransactionXPO
                                                           && rtl.Company == _locReceivableTransactionXPO.CompanyDefault
                                                           && rtl.Select == true
                                                           && (rtl.Status == Status.Progress || rtl.Status == Status.Posted))
                                                           group rtl by rtl.SalesPrePaymentInvoice into g
                                                           select new { SalesPrePaymentInvoice = g.Key };

                        if (_receivableTransactionLine1s != null && _receivableTransactionLine1s.Count() > 0)
                        {
                            foreach (var _receivableTransactionLine1 in _receivableTransactionLine1s)
                            {
                                if (_receivableTransactionLine1.SalesPrePaymentInvoice != null)
                                {
                                    XPQuery<ReceivableTransactionLine> _receivableTransactionLineQuery1a = new XPQuery<ReceivableTransactionLine>(_currSession);

                                    var _receivableTransactionLine1as = from rtl in _receivableTransactionLineQuery1a
                                                                        where (rtl.ReceivableTransaction == _locReceivableTransactionXPO
                                                                        && rtl.Company == _locReceivableTransactionXPO.CompanyDefault
                                                                        && rtl.SalesPrePaymentInvoice == _receivableTransactionLine1.SalesPrePaymentInvoice
                                                                        && rtl.Select == true
                                                                        && (rtl.Status == Status.Progress || rtl.Status == Status.Posted))
                                                                        group rtl by rtl.BankAccount into g
                                                                        select new { BankAccount = g.Key };

                                    if (_receivableTransactionLine1as != null && _receivableTransactionLine1as.Count() > 0)
                                    {
                                        foreach (var _receivableTransactionLine1a in _receivableTransactionLine1as)
                                        {
                                            if (_receivableTransactionLine1a.BankAccount != null)
                                            {
                                                XPCollection<ReceivableTransactionLine> _locReceivableTransactionLines = new XPCollection<ReceivableTransactionLine>
                                                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                new BinaryOperator("ReceivableTransaction", _locReceivableTransactionXPO),
                                                                                                                new BinaryOperator("SalesPrePaymentInvoice", _receivableTransactionLine1.SalesPrePaymentInvoice),
                                                                                                                new BinaryOperator("Company", _locReceivableTransactionXPO.CompanyDefault),
                                                                                                                new BinaryOperator("Select", true),
                                                                                                                new BinaryOperator("BankAccount", _receivableTransactionLine1a.BankAccount),
                                                                                                                new GroupOperator(GroupOperatorType.Or,
                                                                                                                new BinaryOperator("Status", Status.Progress),
                                                                                                                new BinaryOperator("Status", Status.Posted))));

                                                if (_locReceivableTransactionLines != null && _locReceivableTransactionLines.Count() > 0)
                                                {
                                                    foreach (ReceivableTransactionLine _locReceivableTransactionLine in _locReceivableTransactionLines)
                                                    {
                                                        if (_locReceivableTransactionLine.OpenCompany == true && _locReceivableTransactionLine.CloseCredit == true && _locReceivableTransactionLine.Debit > 0)
                                                        {
                                                            _locTotDebit = _locTotDebit + _locReceivableTransactionLine.Debit;
                                                        }
                                                        if (_locReceivableTransactionLine.SalesPrePaymentInvoice.SalesOrder  != null)
                                                        {
                                                            _locSalesOrder = _locReceivableTransactionLine.SalesPrePaymentInvoice.SalesOrder;

                                                        }

                                                        _locSalesPrePaymentInvoice = _locReceivableTransactionLine.SalesPrePaymentInvoice;
                                                    }

                                                    #region JournalMapBankAccountGroup

                                                    if (_receivableTransactionLine1a.BankAccount.BankAccountGroup != null)
                                                    {
                                                        XPCollection<JournalMap> _locJournalMaps = new XPCollection<JournalMap>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("BankAccountGroup", _receivableTransactionLine1a.BankAccount.BankAccountGroup)));

                                                        if (_locJournalMaps != null && _locJournalMaps.Count() > 0)
                                                        {
                                                            foreach (JournalMap _locJournalMap in _locJournalMaps)
                                                            {
                                                                XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                    new BinaryOperator("JournalMap", _locJournalMap)));

                                                                if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                                {
                                                                    foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                                    {
                                                                        AccountMap _locAccountMap = _currSession.FindObject<AccountMap>
                                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                                     new BinaryOperator("Code", _locJournalMapLine.AccountMap.Code),
                                                                                                     new BinaryOperator("PostingType", PostingType.Sales),
                                                                                                     new BinaryOperator("PostingMethod", PostingMethod.Bill),
                                                                                                     new BinaryOperator("PostingMethodType", PostingMethodType.Normal)));

                                                                        if (_locAccountMap != null)
                                                                        {
                                                                            XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                                new BinaryOperator("AccountMap", _locAccountMap),
                                                                                                                                new BinaryOperator("Active", true)));

                                                                            if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                            {
                                                                                double _locTotalAmountDebit = 0;
                                                                                double _locTotalAmountCredit = 0;
                                                                                double _locTotalBalance = 0;

                                                                                foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                                {
                                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                                    {
                                                                                        _locTotalAmountDebit = _locTotDebit;
                                                                                    }
                                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                                    {
                                                                                        _locTotalAmountCredit = _locTotCredit;
                                                                                    }

                                                                                    GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                                    {
                                                                                        PostingDate = now,
                                                                                        PostingType = PostingType.Sales,
                                                                                        PostingMethod = PostingMethod.Bill,
                                                                                        PostingMethodType = PostingMethodType.Normal,
                                                                                        Account = _locAccountMapLine.Account,
                                                                                        Debit = _locTotalAmountDebit,
                                                                                        Credit = _locTotalAmountCredit,
                                                                                        SalesOrder = _locSalesOrder,
                                                                                        SalesPrePaymentInvoice = _locSalesPrePaymentInvoice,
                                                                                        ReceivableTransaction = _locReceivableTransactionXPO,
                                                                                        Company = _locReceivableTransactionXPO.CompanyDefault,
                                                                                    };
                                                                                    _saveGeneralJournal.Save();
                                                                                    _saveGeneralJournal.Session.CommitTransaction();

                                                                                    if (_locAccountMapLine.Account.Code != null)
                                                                                    {
                                                                                        ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                                                 (new GroupOperator(GroupOperatorType.And,
                                                                                                                  new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                                                                                        if (_locCOA != null)
                                                                                        {
                                                                                            if (_locCOA.BalanceType == BalanceType.Change)
                                                                                            {
                                                                                                if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                                                {
                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                                                    }
                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                                                    }

                                                                                                }
                                                                                                if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                                                {
                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                                                    }
                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                                                    }
                                                                                                }
                                                                                            }

                                                                                            _locCOA.Balance = _locTotalBalance;
                                                                                            _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                                            _locCOA.Save();
                                                                                            _locCOA.Session.CommitTransaction();
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            ErrorMessageShow("Data Journal Map Not Available");
                                                        }
                                                    }


                                                    #endregion JournalMapBankAccountGroup

                                                    _locTotDebit = 0;

                                                }
                                            }
                                        }
                                    }
                                }

                            }
                        }
                    }


                    #endregion JournalReceivableBankAccountByCompany

                    #region JournalReceivableByBusinessPartner

                    if (_locReceivableTransactionXPO.BillToCustomer != null && _locReceivableTransactionXPO.SalesToCustomer != null)
                    {
                        double _locTotDebit = 0;
                        double _locTotCredit = 0;
                        //Total Credit dari BillToCustomer
                        XPQuery<ReceivableTransactionLine> _receivableTransactionLineQuery2 = new XPQuery<ReceivableTransactionLine>(_currSession);

                        var _receivableTransactionLine2s = from rtl in _receivableTransactionLineQuery2
                                                           where (rtl.ReceivableTransaction == _locReceivableTransactionXPO
                                                           && rtl.Select == true
                                                           && rtl.Customer == _locReceivableTransactionXPO.BillToCustomer
                                                           && (rtl.Status == Status.Progress || rtl.Status == Status.Posted))
                                                           group rtl by rtl.SalesPrePaymentInvoice into g
                                                           select new { SalesPrePaymentInvoice = g.Key };

                        if (_receivableTransactionLine2s != null && _receivableTransactionLine2s.Count() > 0)
                        {
                            foreach (var _receivableTransactionLine2 in _receivableTransactionLine2s)
                            {
                                if (_receivableTransactionLine2.SalesPrePaymentInvoice != null)
                                {
                                    XPCollection<ReceivableTransactionLine> _locReceivableTransactionLines = new XPCollection<ReceivableTransactionLine>
                                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                    new BinaryOperator("ReceivableTransaction", _locReceivableTransactionXPO),
                                                                                                    new BinaryOperator("Select", true),
                                                                                                    new BinaryOperator("Customer", _locReceivableTransactionXPO.BillToCustomer),
                                                                                                    new BinaryOperator("SalesPrePaymentInvoice", _receivableTransactionLine2.SalesPrePaymentInvoice),
                                                                                                    new GroupOperator(GroupOperatorType.Or,
                                                                                                    new BinaryOperator("Status", Status.Progress),
                                                                                                    new BinaryOperator("Status", Status.Posted))));

                                    if (_locReceivableTransactionLines != null && _locReceivableTransactionLines.Count() > 0)
                                    {
                                        foreach (ReceivableTransactionLine _locReceivableTransactionLine in _locReceivableTransactionLines)
                                        {
                                            if (_locReceivableTransactionLine.OpenCustomer == true && _locReceivableTransactionLine.CloseDebit == true && _locReceivableTransactionLine.Credit > 0)
                                            {
                                                _locTotCredit = _locTotCredit + _locReceivableTransactionLine.Credit;
                                            }

                                            if (_locReceivableTransactionLine.SalesPrePaymentInvoice.SalesOrder != null)
                                            {
                                                _locSalesOrder = _locReceivableTransactionLine.SalesPrePaymentInvoice.SalesOrder;                                        
                                            }
                                            else
                                            {
                                                _locSalesPrePaymentInvoice = _locReceivableTransactionLine.SalesPrePaymentInvoice;
                                            }
                                        }

                                        #region JournalMapBusinessPartnerAcccountGroup

                                        if (_locReceivableTransactionXPO.SalesToCustomer.BusinessPartnerAccountGroup != null)
                                        {
                                            XPCollection<JournalMap> _locJournalMaps = new XPCollection<JournalMap>
                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("BusinessPartnerAccountGroup", _locReceivableTransactionXPO.SalesToCustomer.BusinessPartnerAccountGroup)));

                                            if (_locJournalMaps != null && _locJournalMaps.Count() > 0)
                                            {
                                                foreach (JournalMap _locJournalMap in _locJournalMaps)
                                                {
                                                    XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                        new BinaryOperator("JournalMap", _locJournalMap)));

                                                    if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                    {
                                                        foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                        {
                                                            AccountMap _locAccountMap = _currSession.FindObject<AccountMap>
                                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                                         new BinaryOperator("Code", _locJournalMapLine.AccountMap.Code),
                                                                                         new BinaryOperator("PostingType", PostingType.Sales),
                                                                                         new BinaryOperator("PostingMethod", PostingMethod.Bill),
                                                                                         new BinaryOperator("PostingMethodType", PostingMethodType.Normal)));

                                                            if (_locAccountMap != null)
                                                            {
                                                                XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                    new BinaryOperator("AccountMap", _locAccountMap),
                                                                                                                    new BinaryOperator("Active", true)));

                                                                if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                {
                                                                    double _locTotalAmountDebit = 0;
                                                                    double _locTotalAmountCredit = 0;
                                                                    double _locTotalBalance = 0;

                                                                    foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                    {
                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                        {
                                                                            _locTotalAmountDebit = _locTotDebit;
                                                                        }
                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                        {
                                                                            _locTotalAmountCredit = _locTotCredit;
                                                                        }

                                                                        GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                        {
                                                                            PostingDate = now,
                                                                            PostingType = PostingType.Sales,
                                                                            PostingMethod = PostingMethod.Bill,
                                                                            PostingMethodType = PostingMethodType.Normal,
                                                                            Account = _locAccountMapLine.Account,
                                                                            Debit = _locTotalAmountDebit,
                                                                            Credit = _locTotalAmountCredit,
                                                                            SalesOrder = _locSalesOrder,
                                                                            SalesPrePaymentInvoice = _locSalesPrePaymentInvoice,
                                                                            ReceivableTransaction = _locReceivableTransactionXPO,
                                                                            Company = _locReceivableTransactionXPO.CompanyDefault,
                                                                        };
                                                                        _saveGeneralJournal.Save();
                                                                        _saveGeneralJournal.Session.CommitTransaction();

                                                                        if (_locAccountMapLine.Account.Code != null)
                                                                        {
                                                                            ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                                     (new GroupOperator(GroupOperatorType.And,
                                                                                                      new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                                                                            if (_locCOA != null)
                                                                            {
                                                                                if (_locCOA.BalanceType == BalanceType.Change)
                                                                                {
                                                                                    if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                                    {
                                                                                        if (_locTotalAmountDebit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                                        }
                                                                                        if (_locTotalAmountCredit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                                        }
                                                                                    }
                                                                                    if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                                    {
                                                                                        if (_locTotalAmountDebit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                                        }
                                                                                        if (_locTotalAmountCredit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                                        }
                                                                                    }
                                                                                }

                                                                                _locCOA.Balance = _locTotalBalance;
                                                                                _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                                _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                                _locCOA.Save();
                                                                                _locCOA.Session.CommitTransaction();
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        #endregion JournalMapBankAccountGroup

                                        _locTotCredit = 0;
                                    }
                                }
                            }

                        }

                    }


                    #endregion JournalReceivableByBusinessPartner
                }
                else
                {
                    ErrorMessageShow("Data Receivable Transaction Not Available");
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = SalesInvoice ", ex.ToString());
            }
        }

        private void SetNormalSalesPrePaymentInvoice(Session _currSession, ReceivableTransaction _locReceivableTransactionXPO)
        {
            try
            {
                XPQuery<ReceivableTransactionLine> _receivableTransactionLineQuery1 = new XPQuery<ReceivableTransactionLine>(_currSession);

                var _receivableTransactionLine1s = from rtl in _receivableTransactionLineQuery1
                                                   where (rtl.ReceivableTransaction == _locReceivableTransactionXPO
                                                   && rtl.Select == true
                                                   && (rtl.Status == Status.Progress || rtl.Status == Status.Posted))
                                                   group rtl by rtl.SalesPrePaymentInvoice into g
                                                   select new { SalesPrePaymentInvoice = g.Key };
                if (_receivableTransactionLine1s != null && _receivableTransactionLine1s.Count() > 0)
                {
                    foreach (var _receivableTransactionLine1 in _receivableTransactionLine1s)
                    {
                        if (_receivableTransactionLine1.SalesPrePaymentInvoice != null)
                        {
                            if (_receivableTransactionLine1.SalesPrePaymentInvoice.Code != null)
                            {
                                SalesPrePaymentInvoice _locSalesPrePaymentInvoice = _currSession.FindObject<SalesPrePaymentInvoice>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Code", _receivableTransactionLine1.SalesPrePaymentInvoice.Code)));
                                if (_locSalesPrePaymentInvoice != null)
                                {
                                    _locSalesPrePaymentInvoice.ReceivableTransaction = null;
                                    _locSalesPrePaymentInvoice.Save();
                                    _locSalesPrePaymentInvoice.Session.CommitTransaction();
                                }
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = SalesInvoice ", ex.ToString());
            }
        }

        #endregion PostingSalesPrePaymentInvoice

        private void SetStatusReceivableTransactionLine(Session _currSession, ReceivableTransaction _locReceivableTransactionXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locReceivableTransactionXPO != null)
                {
                    XPCollection<ReceivableTransactionLine> _locReceivableTransactionLines = new XPCollection<ReceivableTransactionLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("ReceivableTransaction", _locReceivableTransactionXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locReceivableTransactionLines != null && _locReceivableTransactionLines.Count > 0)
                    {

                        foreach (ReceivableTransactionLine _locReceivableTransactionLine in _locReceivableTransactionLines)
                        {
                            if (_locReceivableTransactionLine.Status == Status.Progress || _locReceivableTransactionLine.Status == Status.Posted)
                            {
                                _locReceivableTransactionLine.Status = Status.Close;
                                _locReceivableTransactionLine.ActivationPosting = true;
                                _locReceivableTransactionLine.StatusDate = now;
                                _locReceivableTransactionLine.Save();
                                _locReceivableTransactionLine.Session.CommitTransaction();
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesInvoice " + ex.ToString());
            }
        }

        private void SetFinalStatusReceivableTransaction(Session _currSession, ReceivableTransaction _locReceivableTransactionXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locCount = 0;
                if (_locReceivableTransactionXPO != null)
                {
                    XPCollection<ReceivableTransactionLine> _locReceivableTransactionLines = new XPCollection<ReceivableTransactionLine>(_currSession,
                                                                                             new GroupOperator(GroupOperatorType.And,
                                                                                             new BinaryOperator("ReceivableTransaction", _locReceivableTransactionXPO),
                                                                                             new BinaryOperator("Select", true)));

                    if (_locReceivableTransactionLines != null && _locReceivableTransactionLines.Count() > 0)
                    {

                        foreach (ReceivableTransactionLine _locReceivableTransactionLine in _locReceivableTransactionLines)
                        {
                            if (_locReceivableTransactionLine.Status == Status.Close)
                            {
                                _locCount = _locCount + 1;
                            }
                        }

                        if (_locCount == _locReceivableTransactionLines.Count())
                        {
                            _locReceivableTransactionXPO.ActivationPosting = true;
                            _locReceivableTransactionXPO.Status = Status.Close;
                            _locReceivableTransactionXPO.StatusDate = now;
                            _locReceivableTransactionXPO.Save();
                            _locReceivableTransactionXPO.Session.CommitTransaction();
                        }
                        else
                        {
                            _locReceivableTransactionXPO.Status = Status.Posted;
                            _locReceivableTransactionXPO.StatusDate = now;
                            _locReceivableTransactionXPO.Save();
                            _locReceivableTransactionXPO.Session.CommitTransaction();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesInvoice " + ex.ToString());
            }
        }

        private void SetCloseAllSalesProcess(Session _currSession, ReceivableTransaction _locReceivableTransactionXPO)
        {
            //GroupByPaymentOutPlanBasedOnPurchaseInvoice
            //Cari Receivable Transaction Monitoring 
            DateTime now = DateTime.Now;
            double _locTotBillSO = 0;
            double _locTotBillSI = 0;
            if (_locReceivableTransactionXPO != null)
            {
                XPCollection<ReceivableTransactionMonitoring> _locReceivableTransactionMonitorings = new XPCollection<ReceivableTransactionMonitoring>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                               new BinaryOperator("ReceivableTransaction", _locReceivableTransactionXPO)));

                if (_locReceivableTransactionMonitorings != null && _locReceivableTransactionMonitorings.Count() > 0)
                {
                    foreach (ReceivableTransactionMonitoring _locReceivableTransactionMonitoring in _locReceivableTransactionMonitorings)
                    {
                        #region SalesOrderClose
                        if (_locReceivableTransactionMonitoring.SalesOrder != null)
                        {
                            XPCollection<PaymentInPlan> _locPaymentInPlans = new XPCollection<PaymentInPlan>
                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("SalesOrder", _locReceivableTransactionMonitoring.SalesOrder)));

                            if (_locPaymentInPlans != null && _locPaymentInPlans.Count() > 0)
                            {
                                foreach (PaymentInPlan _locPaymentInPlan in _locPaymentInPlans)
                                {
                                    _locTotBillSO = _locTotBillSO + _locPaymentInPlan.Received;
                                }

                                if (_locTotBillSO > 0)
                                {
                                    if (_locTotBillSO == _locReceivableTransactionMonitoring.SalesOrder.MaxBill)
                                    {
                                        _locReceivableTransactionMonitoring.SalesOrder.Status = Status.Close;
                                        _locReceivableTransactionMonitoring.SalesOrder.StatusDate = now;
                                        _locReceivableTransactionMonitoring.SalesOrder.Save();
                                        _locReceivableTransactionMonitoring.SalesOrder.Session.CommitTransaction();
                                    }
                                }
                            }
                        }
                        #endregion SalesOrderClose

                        #region SalesInvoiceClose
                        if (_locReceivableTransactionMonitoring.SalesInvoice != null)
                        {
                            XPCollection<PaymentInPlan> _locPaymentInPlans = new XPCollection<PaymentInPlan>
                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("SalesInvoice", _locReceivableTransactionMonitoring.SalesInvoice)));

                            if (_locPaymentInPlans != null && _locPaymentInPlans.Count() > 0)
                            {
                                foreach (PaymentInPlan _locPaymentInPlan in _locPaymentInPlans)
                                {
                                    _locTotBillSI = _locTotBillSI + _locPaymentInPlan.Received;
                                }

                                if (_locTotBillSI > 0)
                                {
                                    if (_locTotBillSI == _locReceivableTransactionMonitoring.SalesInvoice.MaxBill)
                                    {
                                        _locReceivableTransactionMonitoring.SalesOrder.Status = Status.Close;
                                        _locReceivableTransactionMonitoring.SalesOrder.StatusDate = now;
                                        _locReceivableTransactionMonitoring.SalesOrder.Save();
                                        _locReceivableTransactionMonitoring.SalesOrder.Session.CommitTransaction();
                                    }
                                }
                            }
                        }
                        #endregion SalesInvoiceClose
                    }
                }
            }
        }

        #endregion Posting

        #region Global

        private void SuccessMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 4000;
            options.Message = string.Format("{0} !", _locMessage);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }


        #endregion Global

        
    }
}
