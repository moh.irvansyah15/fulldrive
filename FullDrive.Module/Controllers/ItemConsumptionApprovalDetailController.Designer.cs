﻿namespace FullDrive.Module.Controllers
{
    partial class ItemConsumptionApprovalDetailController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ItemConsumptionApprovalDetail = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // ItemConsumptionApprovalDetail
            // 
            this.ItemConsumptionApprovalDetail.Caption = "Approval";
            this.ItemConsumptionApprovalDetail.ConfirmationMessage = null;
            this.ItemConsumptionApprovalDetail.Id = "ItemConsumptionApprovalDetailId";
            this.ItemConsumptionApprovalDetail.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.ItemConsumptionApprovalDetail.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.ItemConsumption);
            this.ItemConsumptionApprovalDetail.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.ItemConsumptionApprovalDetail.ToolTip = null;
            this.ItemConsumptionApprovalDetail.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.ItemConsumptionApprovalDetail.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.ItemConsumptionApprovalDetail_Execute);
            // 
            // ItemConsumptionApprovalDetailController
            // 
            this.Actions.Add(this.ItemConsumptionApprovalDetail);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SingleChoiceAction ItemConsumptionApprovalDetail;
    }
}
