﻿namespace FullDrive.Module.Controllers
{
    partial class PurchaseReturnActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.PurchaseReturnProgressAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PurchaseReturnPostingAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PurchaseReturnListviewFilterSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.PurchaseReturnApprovalAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.PurchaseReturnGetITIMAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // PurchaseReturnProgressAction
            // 
            this.PurchaseReturnProgressAction.Caption = "Progress";
            this.PurchaseReturnProgressAction.ConfirmationMessage = null;
            this.PurchaseReturnProgressAction.Id = "PurchaseReturnProgressActionId";
            this.PurchaseReturnProgressAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchaseReturn);
            this.PurchaseReturnProgressAction.ToolTip = null;
            this.PurchaseReturnProgressAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PurchaseReturnProgressAction_Execute);
            // 
            // PurchaseReturnPostingAction
            // 
            this.PurchaseReturnPostingAction.Caption = "Posting";
            this.PurchaseReturnPostingAction.ConfirmationMessage = null;
            this.PurchaseReturnPostingAction.Id = "PurchaseReturnPostingActionId";
            this.PurchaseReturnPostingAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchaseReturn);
            this.PurchaseReturnPostingAction.ToolTip = null;
            this.PurchaseReturnPostingAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PurchaseReturnPostingAction_Execute);
            // 
            // PurchaseReturnListviewFilterSelectionAction
            // 
            this.PurchaseReturnListviewFilterSelectionAction.Caption = "Filter";
            this.PurchaseReturnListviewFilterSelectionAction.ConfirmationMessage = null;
            this.PurchaseReturnListviewFilterSelectionAction.Id = "PurchaseReturnListviewFilterSelectionActionId";
            this.PurchaseReturnListviewFilterSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.PurchaseReturnListviewFilterSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchaseReturn);
            this.PurchaseReturnListviewFilterSelectionAction.ToolTip = null;
            this.PurchaseReturnListviewFilterSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.PurchaseReturnListviewFilterSelectionAction_Execute);
            // 
            // PurchaseReturnApprovalAction
            // 
            this.PurchaseReturnApprovalAction.Caption = "Approval";
            this.PurchaseReturnApprovalAction.ConfirmationMessage = null;
            this.PurchaseReturnApprovalAction.Id = "PurchaseReturnApprovalActionId";
            this.PurchaseReturnApprovalAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.PurchaseReturnApprovalAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchaseReturn);
            this.PurchaseReturnApprovalAction.ToolTip = null;
            this.PurchaseReturnApprovalAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.PurchaseReturnApprovalAction_Execute);
            // 
            // PurchaseReturnGetITIMAction
            // 
            this.PurchaseReturnGetITIMAction.Caption = "Get ITIM";
            this.PurchaseReturnGetITIMAction.ConfirmationMessage = null;
            this.PurchaseReturnGetITIMAction.Id = "PurchaseReturnGetITIMActionId";
            this.PurchaseReturnGetITIMAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchaseReturn);
            this.PurchaseReturnGetITIMAction.ToolTip = null;
            this.PurchaseReturnGetITIMAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PurchaseReturnGetITIMAction_Execute);
            // 
            // PurchaseReturnActionController
            // 
            this.Actions.Add(this.PurchaseReturnProgressAction);
            this.Actions.Add(this.PurchaseReturnPostingAction);
            this.Actions.Add(this.PurchaseReturnListviewFilterSelectionAction);
            this.Actions.Add(this.PurchaseReturnApprovalAction);
            this.Actions.Add(this.PurchaseReturnGetITIMAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction PurchaseReturnProgressAction;
        private DevExpress.ExpressApp.Actions.SimpleAction PurchaseReturnPostingAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction PurchaseReturnListviewFilterSelectionAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction PurchaseReturnApprovalAction;
        private DevExpress.ExpressApp.Actions.SimpleAction PurchaseReturnGetITIMAction;
    }
}
