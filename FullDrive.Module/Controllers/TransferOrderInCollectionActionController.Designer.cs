﻿namespace FullDrive.Module.Controllers
{
    partial class TransferOrderInCollectionActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.TransferOrderInCollectionShowTOMAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // TransferOrderInCollectionShowTOMAction
            // 
            this.TransferOrderInCollectionShowTOMAction.Caption = "Show TOM";
            this.TransferOrderInCollectionShowTOMAction.ConfirmationMessage = null;
            this.TransferOrderInCollectionShowTOMAction.Id = "TransferOrderInCollectionShowTOMActionId";
            this.TransferOrderInCollectionShowTOMAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.TransferOrderInCollection);
            this.TransferOrderInCollectionShowTOMAction.ToolTip = null;
            this.TransferOrderInCollectionShowTOMAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.TransferOrderInCollectionShowTOMAction_Execute);
            // 
            // TransferOrderInCollectionActionController
            // 
            this.Actions.Add(this.TransferOrderInCollectionShowTOMAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction TransferOrderInCollectionShowTOMAction;
    }
}
