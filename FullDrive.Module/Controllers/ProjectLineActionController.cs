﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using System.Collections;
using DevExpress.Xpo;
using DevExpress.ExpressApp.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class ProjectLineActionController : ViewController
    {
        private ChoiceActionItem _selectionListviewFilter;

        public ProjectLineActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            ProjectLineListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                ProjectLineListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
        }

        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }

        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }

        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void ProjectLineGetPreviousAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                ProjectHeader _locProjectHeaderXPO = null;
                string _locProjectHeaderCode = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        ProjectLine _locProjectLineOS = (ProjectLine)_objectSpace.GetObject(obj);


                        if (_locProjectLineOS != null)
                        {
                            if (_locProjectLineOS.ProjectHeader != null)
                            {
                                if (_locProjectLineOS.ProjectHeader.Code != null)
                                {
                                    _locProjectHeaderCode = _locProjectLineOS.ProjectHeader.Code;
                                }
                            }

                            if (_locProjectLineOS.Code != null)
                            {
                                _currObjectId = _locProjectLineOS.Code;

                                ProjectLine _locProjectLineXPO = _currSession.FindObject<ProjectLine>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locProjectLineXPO == null)
                                {
                                    if (_locProjectHeaderCode != null)
                                    {
                                        _locProjectHeaderXPO = _currSession.FindObject<ProjectHeader>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _locProjectHeaderCode)));
                                    }

                                    if (_locProjectHeaderXPO != null)
                                    {
                                        XPCollection<ProjectLine> _numLines = new XPCollection<ProjectLine>
                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("ProjectHeader", _locProjectHeaderXPO)),
                                                                            new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));

                                        if (_numLines != null && _numLines.Count > 0)
                                        {
                                            foreach (ProjectLine _numLine in _numLines)
                                            {
                                                if (_numLine.No == _numLines.Count())
                                                {
                                                    _locProjectLineOS.Title = _numLine.Title;
                                                    _locProjectLineOS.Title2 = _numLine.Title2;
                                                    _locProjectLineOS.Title3 = _numLine.Title3;
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data Project Line Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Project Line Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ProjectLine " + ex.ToString());
            }
        }

        private void ProjectLineMirrorAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                ProjectHeader _locProjectHeaderXPO = null;
                string _locProjectHeaderCode = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        ProjectLine _locProjectLineOS = (ProjectLine)_objectSpace.GetObject(obj);


                        if (_locProjectLineOS != null)
                        {
                            if (_locProjectLineOS.ProjectHeader != null)
                            {
                                if (_locProjectLineOS.ProjectHeader.Code != null)
                                {
                                    _locProjectHeaderCode = _locProjectLineOS.ProjectHeader.Code;
                                }
                            }

                            if (_locProjectLineOS.Code != null)
                            {

                                if (_locProjectHeaderCode != null)
                                {
                                    _locProjectHeaderXPO = _currSession.FindObject<ProjectHeader>
                                                                (new GroupOperator(GroupOperatorType.And,
                                                                 new BinaryOperator("Code", _locProjectHeaderCode)));

                                    if (_locProjectHeaderXPO != null)
                                    {
                                        _currObjectId = _locProjectLineOS.Code;

                                        ProjectLine _locProjectLineXPO = _currSession.FindObject<ProjectLine>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("Code", _currObjectId),
                                                                             new BinaryOperator("ProjectHeader", _locProjectHeaderXPO)));


                                        if (_locProjectLineXPO != null)
                                        {

                                            #region Approximate

                                            XPCollection<ProjectLineItem> _numProjectLineItems = new XPCollection<ProjectLineItem>
                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("ProjectHeader", _locProjectHeaderXPO),
                                                                                new BinaryOperator("ProjectLine", _locProjectLineXPO)),
                                                                                new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));

                                            if (_numProjectLineItems != null && _numProjectLineItems.Count > 0)
                                            {
                                                foreach (ProjectLineItem _numProjectLineItem in _numProjectLineItems)
                                                {

                                                    if (_numProjectLineItem.Item != null && (_numProjectLineItem.Status == Status.Open || _numProjectLineItem.Status == Status.Progress || _numProjectLineItem.Status == Status.Lock))
                                                    {
                                                        if (_numProjectLineItem.Item.OrderType == OrderType.Service)
                                                        {
                                                            //Create ProjectLineService Direct From ProjectLineItem
                                                            SetProjectLineServiceDirect(_currSession, _locProjectHeaderXPO, _locProjectLineXPO, _numProjectLineItem);
                                                        }
                                                        else
                                                        {
                                                            //Create ProjectLine2
                                                            SetProjectLineItem2(_currSession, _locProjectHeaderXPO, _locProjectLineXPO, _numProjectLineItem);

                                                            //Create ProjectLineService Indirect From ProjectLineItem
                                                            SetProjectLineServiceIndirect(_currSession, _locProjectHeaderXPO, _locProjectLineXPO, _numProjectLineItem);
                                                        }
                                                    }
                                                }
                                            }

                                            #endregion Approximate

                                            #region Offer

                                            ProjectLine2 _locProjectLine2 = _currSession.FindObject<ProjectLine2>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("ProjectLine", _locProjectLineXPO),
                                                                            new BinaryOperator("ProjectHeader", _locProjectHeaderXPO)));

                                            if (_locProjectLine2 != null)
                                            {
                                                _locProjectLine2.Title = _locProjectLineXPO.Title;
                                                _locProjectLine2.Title2 = _locProjectLineXPO.Title2;
                                                _locProjectLine2.Title3 = _locProjectLineXPO.Title3;
                                                _locProjectLine2.Status = _locProjectLineXPO.Status;
                                                _locProjectLine2.StatusDate = _locProjectLineXPO.StatusDate;
                                                _locProjectLine2.Description = _locProjectLineXPO.Description;
                                                _locProjectLine2.Save();
                                                _locProjectLine2.Session.CommitTransaction();
                                            }
                                            else
                                            {
                                                ProjectLine2 _saveProjectLine2 = new ProjectLine2(_currSession)
                                                {
                                                    Title = _locProjectLineXPO.Title,
                                                    Title2 = _locProjectLineXPO.Title2,
                                                    Title3 = _locProjectLineXPO.Title3,
                                                    Status = _locProjectLineXPO.Status,
                                                    StatusDate = _locProjectLineXPO.StatusDate,
                                                    Description = _locProjectLineXPO.Description,
                                                    ProjectHeader = _locProjectHeaderXPO,
                                                    ProjectLine = _locProjectLineXPO,
                                                    Company = _locProjectLineXPO.Company,
                                                };
                                                _saveProjectLine2.Save();
                                                _saveProjectLine2.Session.CommitTransaction();
                                            }

                                            ProjectLine2 _locProjectLine2x = _currSession.FindObject<ProjectLine2>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("ProjectLine", _locProjectLineXPO),
                                                                            new BinaryOperator("ProjectHeader", _locProjectHeaderXPO)));
                                            if (_locProjectLine2x != null)
                                            {

                                                //Create ProjectLine2Item
                                                SetProjectLine2Item(_currSession, _locProjectHeaderXPO, _locProjectLineXPO, _locProjectLine2x);

                                                //Create ProjectLine2Item2
                                                SetProjectLine2Item2(_currSession, _locProjectHeaderXPO, _locProjectLineXPO, _locProjectLine2x);

                                                //Create ProjectLine2Service
                                                SetProjectLine2Service(_currSession, _locProjectHeaderXPO, _locProjectLineXPO, _locProjectLine2x);

                                            }

                                            #endregion Offer

                                        }
                                        SuccessMessageShow("Project Line Item Has Successfully Mirror");
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data Project Line Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Project Line Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ProjectLine " + ex.ToString());
            }
        }

        private void ProjectLineListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(ProjectLine)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ProjectLine " + ex.ToString());
            }
        }

        #region GlobalProjectLineMethod

        private void SetProjectLineItem2(Session _currSession, ProjectHeader _locProjectHeaderXPO, ProjectLine _locProjectLineXPO, ProjectLineItem _numProjectLineItem)
        {
            try
            {
                double _locUnitAmount = 0;
                double _locTotalQty = 0;

                #region Mirror ProjectLineItem2
                XPCollection<ItemComponent> _locItemComponents = new XPCollection<ItemComponent>(_currSession, new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("Item", _numProjectLineItem.Item),
                                    new BinaryOperator("Company", _locProjectLineXPO.Company),
                                    new BinaryOperator("Active", true)));

                if (_locItemComponents != null && _locItemComponents.Count() > 0)
                {

                    foreach (ItemComponent _locItemComponent in _locItemComponents)
                    {
                        ProjectLineItem2 _locProjectLineItem2 = _currSession.FindObject<ProjectLineItem2>(
                                        new GroupOperator(GroupOperatorType.And,
                                        new BinaryOperator("ProjectHeader", _locProjectHeaderXPO),
                                        new BinaryOperator("ProjectLine", _locProjectLineXPO),
                                        new BinaryOperator("ProjectLineItem", _numProjectLineItem),
                                        new BinaryOperator("Item", _locItemComponent.ItemComp)
                                        ));

                        if (_locProjectLineItem2 == null)
                        {

                            _locUnitAmount = GetPriceLineAmount1ByItem(_currSession, _locItemComponent.ItemComp, _locProjectLineXPO);

                            if (_numProjectLineItem.Item == _locItemComponent.ItemComp)
                            {
                                _locTotalQty = GetTotalQty(_currSession, _numProjectLineItem);

                                ProjectLineItem2 _saveDataPLI2 = new ProjectLineItem2(_currSession)
                                {
                                    Item = _numProjectLineItem.Item,
                                    DUOM = _numProjectLineItem.DUOM,
                                    DQty = _numProjectLineItem.DQty,
                                    TQty = _numProjectLineItem.TQty,
                                    UnitAmount = _numProjectLineItem.UnitAmount,
                                    TotalUnitAmount = _numProjectLineItem.TotalUnitAmount,
                                    ProjectHeader = _locProjectHeaderXPO,
                                    ProjectLine = _locProjectLineXPO,
                                    ProjectLineItem = _numProjectLineItem,
                                    Company = _locProjectLineXPO.Company,
                                };
                                _saveDataPLI2.Save();
                                _saveDataPLI2.Session.CommitTransaction();
                            }
                            else
                            {
                                ProjectLineItem2 _saveDataPLI2 = new ProjectLineItem2(_currSession)
                                {
                                    Item = _locItemComponent.ItemComp,
                                    DUOM = _locItemComponent.UOM,
                                    DQty = _locItemComponent.Qty,
                                    TQty = _locItemComponent.Qty,
                                    UnitAmount = _locUnitAmount,
                                    TotalUnitAmount = _locItemComponent.Qty * _locUnitAmount,
                                    ProjectHeader = _locProjectHeaderXPO,
                                    ProjectLine = _locProjectLineXPO,
                                    ProjectLineItem = _numProjectLineItem,
                                    Company = _locProjectLineXPO.Company,
                                };
                                _saveDataPLI2.Save();
                                _saveDataPLI2.Session.CommitTransaction();
                            }
                        }
                    }
                }
                else
                {
                    ProjectLineItem2 _locProjectLineItem2 = _currSession.FindObject<ProjectLineItem2>(
                                        new GroupOperator(GroupOperatorType.And,
                                        new BinaryOperator("ProjectHeader", _locProjectHeaderXPO),
                                        new BinaryOperator("ProjectLine", _locProjectLineXPO),
                                        new BinaryOperator("ProjectLineItem", _numProjectLineItem),
                                        new BinaryOperator("Item", _numProjectLineItem.Item)
                                        ));

                    if (_locProjectLineItem2 != null)
                    {
                        _locProjectLineItem2.Name = _numProjectLineItem.Name;
                        _locProjectLineItem2.Item = _numProjectLineItem.Item;
                        _locProjectLineItem2.DQty = _numProjectLineItem.DQty;
                        _locProjectLineItem2.DUOM = _numProjectLineItem.DUOM;
                        _locProjectLineItem2.Qty = _numProjectLineItem.Qty;
                        _locProjectLineItem2.UOM = _numProjectLineItem.UOM;
                        _locProjectLineItem2.TQty = _numProjectLineItem.TQty;
                        _locProjectLineItem2.UnitAmount = _numProjectLineItem.UnitAmount;
                        _locProjectLineItem2.TotalUnitAmount = _numProjectLineItem.TQty * _numProjectLineItem.UnitAmount;
                        _locProjectLineItem2.Description = _numProjectLineItem.Description;
                        _locProjectLineItem2.ProjectHeader = _locProjectHeaderXPO;
                        _locProjectLineItem2.ProjectLine = _locProjectLineXPO;
                        _locProjectLineItem2.Save();
                        _locProjectLineItem2.Session.CommitTransaction();
                    }
                    else
                    {
                        ProjectLineItem2 _saveDataPLI2 = new ProjectLineItem2(_currSession)
                        {
                            Item = _numProjectLineItem.Item,
                            DUOM = _numProjectLineItem.DUOM,
                            DQty = _numProjectLineItem.DQty,
                            Qty = _numProjectLineItem.Qty,
                            UOM = _numProjectLineItem.UOM,
                            TQty = _numProjectLineItem.TQty,
                            UnitAmount = _numProjectLineItem.UnitAmount,
                            TotalUnitAmount = _numProjectLineItem.TotalUnitAmount,
                            ProjectHeader = _locProjectHeaderXPO,
                            ProjectLine = _locProjectLineXPO,
                            ProjectLineItem = _numProjectLineItem,
                            Company = _locProjectLineXPO.Company,
                        };
                        _saveDataPLI2.Save();
                        _saveDataPLI2.Session.CommitTransaction();
                    }
                }
                #endregion Mirror ProjectLineItem2

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ProjectLine " + ex.ToString());
            }
        }

        private void SetProjectLineServiceDirect(Session _currSession, ProjectHeader _locProjectHeaderXPO, ProjectLine _locProjectLineXPO, ProjectLineItem _numProjectLineItem)
        {
            try
            {
                double _locUnitAmount = 0;

                #region Mirror ProjectLineServices

                ProjectLineService _locProjectLineService = _currSession.FindObject<ProjectLineService>
                                                           (new GroupOperator(GroupOperatorType.And,
                                                           new BinaryOperator("Item", _numProjectLineItem.Item),
                                                           new BinaryOperator("ProjectHeader", _locProjectHeaderXPO),
                                                           new BinaryOperator("ProjectLine", _locProjectLineXPO),
                                                           new BinaryOperator("ProjectLineItem", _numProjectLineItem)));
                if (_numProjectLineItem.UnitAmount > 0)
                {
                    _locUnitAmount = _numProjectLineItem.UnitAmount;
                }
                else
                {
                    _locUnitAmount = GetPriceLineAmount1ByService(_currSession, _numProjectLineItem.Item, _locProjectLineXPO);
                }

                if (_locProjectLineService != null)
                {
                    _locProjectLineService.Qty = _numProjectLineItem.TQty;
                    _locProjectLineService.UOM = _numProjectLineItem.DUOM;
                    _locProjectLineService.UnitAmount = _locUnitAmount;
                    _locProjectLineService.TotalUnitAmount = _numProjectLineItem.TQty * _locUnitAmount;
                    _locProjectLineService.ProjectHeader = _locProjectHeaderXPO;
                    _locProjectLineService.ProjectLine = _locProjectLineXPO;
                    _locProjectLineService.ProjectLineItem = _numProjectLineItem;
                }
                else
                {
                    if (_locProjectLineService == null)
                    {
                        ProjectLineService _saveDataPLS = new ProjectLineService(_currSession)
                        {
                            Item = _numProjectLineItem.Item,
                            Qty = _numProjectLineItem.TQty,
                            UOM = _numProjectLineItem.DUOM,
                            UnitAmount = _locUnitAmount,
                            TotalUnitAmount = _numProjectLineItem.TQty * _locUnitAmount,
                            ProjectHeader = _locProjectHeaderXPO,
                            ProjectLine = _locProjectLineXPO,
                            ProjectLineItem = _numProjectLineItem,
                            Company = _locProjectLineXPO.Company,
                        };
                        _saveDataPLS.Save();
                        _saveDataPLS.Session.CommitTransaction();
                    }
                }

                #endregion Mirror ProjectLineServices

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ProjectLine " + ex.ToString());
            }
        }

        private void SetProjectLineServiceIndirect(Session _currSession, ProjectHeader _locProjectHeaderXPO, ProjectLine _locProjectLineXPO, ProjectLineItem _numProjectLineItem)
        {
            try
            {
                double _locUnitAmount = 0;

                #region Mirror ProjectLineServices

                Item _itemService = _currSession.FindObject<Item>(new GroupOperator(GroupOperatorType.And,
                                        new BinaryOperator("OrderType", OrderType.Service),
                                        new BinaryOperator("PartItemService", _numProjectLineItem.Item),
                                        new BinaryOperator("Company", _numProjectLineItem.Company)));

                if (_itemService != null)
                {
                    ProjectLineService _locProjectLineService = _currSession.FindObject<ProjectLineService>
                                                           (new GroupOperator(GroupOperatorType.And,
                                                           new BinaryOperator("Item", _itemService),
                                                           new BinaryOperator("ProjectHeader", _locProjectHeaderXPO),
                                                           new BinaryOperator("ProjectLine", _locProjectLineXPO),
                                                           new BinaryOperator("ProjectLineItem", _numProjectLineItem)));

                    _locUnitAmount = GetPriceLineAmount1ByService(_currSession, _itemService, _locProjectLineXPO);

                    if (_locProjectLineService != null)
                    {
                        _locProjectLineService.Qty = _numProjectLineItem.TQty;
                        _locProjectLineService.UOM = _numProjectLineItem.DUOM;
                        _locProjectLineService.UnitAmount = _locUnitAmount;
                        _locProjectLineService.TotalUnitAmount = _numProjectLineItem.TQty * _locUnitAmount;
                        _locProjectLineService.ProjectHeader = _locProjectHeaderXPO;
                        _locProjectLineService.ProjectLine = _locProjectLineXPO;
                        _locProjectLineService.ProjectLineItem = _numProjectLineItem;
                    }

                    if (_locProjectLineService == null)
                    {
                        ProjectLineService _saveDataPLS = new ProjectLineService(_currSession)
                        {
                            Item = _itemService,
                            Qty = _numProjectLineItem.TQty,
                            UOM = _numProjectLineItem.DUOM,
                            UnitAmount = _locUnitAmount,
                            TotalUnitAmount = _numProjectLineItem.TQty * _locUnitAmount,
                            ProjectHeader = _locProjectHeaderXPO,
                            ProjectLine = _locProjectLineXPO,
                            ProjectLineItem = _numProjectLineItem,
                            Company = _locProjectLineXPO.Company,
                        };
                        _saveDataPLS.Save();
                        _saveDataPLS.Session.CommitTransaction();
                    }
                }

                #endregion Mirror ProjectLineServices

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ProjectLine " + ex.ToString());
            }
        }

        private void SetProjectLine2Item(Session _currSession, ProjectHeader _locProjectHeaderXPO, ProjectLine _locProjectLineXPO, ProjectLine2 _locProjectLine2x)
        {
            try
            {
                double _locUnitAmount = 0;
                #region ProjectLine2Item

                XPCollection<ProjectLine2Item> _locProjectLine2Items = new XPCollection<ProjectLine2Item>(_currSession,
                                                        new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("ProjectLine2", _locProjectLine2x),
                                                        new BinaryOperator("ProjectLine", _locProjectLineXPO),
                                                        new BinaryOperator("ProjectHeader", _locProjectHeaderXPO)));

                XPCollection<ProjectLineItem> _locProjectLineItems = new XPCollection<ProjectLineItem>(_currSession,
                                                    new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("ProjectLine", _locProjectLineXPO),
                                                    new BinaryOperator("ProjectHeader", _locProjectHeaderXPO)));

                if (_locProjectLine2Items != null && _locProjectLine2Items.Count() > 0)
                {
                    foreach (ProjectLine2Item _locProjectLine2Item in _locProjectLine2Items)
                    {
                        if (_locProjectLineItems != null && _locProjectLineItems.Count() > 0)
                        {
                            foreach (ProjectLineItem _locProjectLineItem in _locProjectLineItems)
                            {
                                _locUnitAmount = GetPriceLineAmount2ByItem(_currSession, _locProjectLineItem.Item, _locProjectLineXPO);

                                if (_locProjectLine2Item.Item == _locProjectLineItem.Item && _locProjectLine2Item.ProjectLineItem == _locProjectLineItem)
                                {
                                    _locProjectLine2Item.Name = _locProjectLineItem.Name;
                                    _locProjectLine2Item.Item = _locProjectLineItem.Item;
                                    _locProjectLine2Item.DQty = _locProjectLineItem.DQty;
                                    _locProjectLine2Item.DUOM = _locProjectLineItem.DUOM;
                                    _locProjectLine2Item.Qty = _locProjectLineItem.Qty;
                                    _locProjectLine2Item.TQty = _locProjectLineItem.TQty;
                                    _locProjectLine2Item.UnitAmount = _locUnitAmount;
                                    _locProjectLine2Item.TotalUnitAmount = _locUnitAmount * _locProjectLineItem.TQty;
                                    _locProjectLine2Item.Description = _locProjectLineItem.Description;
                                    _locProjectLine2Item.Save();
                                    _locProjectLine2Item.Session.CommitTransaction();
                                }
                                else
                                {
                                    ProjectLine2Item _saveProjectLine2Item = new ProjectLine2Item(_currSession)
                                    {
                                        Name = _locProjectLineItem.Name,
                                        Item = _locProjectLineItem.Item,
                                        DQty = _locProjectLineItem.DQty,
                                        DUOM = _locProjectLineItem.DUOM,
                                        Qty = _locProjectLineItem.Qty,
                                        TQty = _locProjectLineItem.TQty,
                                        UnitAmount = _locUnitAmount,
                                        TotalUnitAmount = _locUnitAmount * _locProjectLineItem.TQty,
                                        Description = _locProjectLineItem.Description,
                                        ProjectLine2 = _locProjectLine2x,
                                        ProjectHeader = _locProjectHeaderXPO,
                                        ProjectLineItem = _locProjectLineItem,
                                        Company = _locProjectLineXPO.Company,
                                    };
                                    _saveProjectLine2Item.Save();
                                    _saveProjectLine2Item.Session.CommitTransaction();
                                }
                            }
                        }
                    }

                }
                else
                {
                    if (_locProjectLineItems != null && _locProjectLineItems.Count() > 0)
                    {
                        foreach (ProjectLineItem _locProjectLineItem in _locProjectLineItems)
                        {

                            //cari Amount2 by Item
                            _locUnitAmount = GetPriceLineAmount2ByItem(_currSession, _locProjectLineItem.Item, _locProjectLineXPO);

                            ProjectLine2Item _saveProjectLine2Item = new ProjectLine2Item(_currSession)
                            {
                                Name = _locProjectLineItem.Name,
                                Item = _locProjectLineItem.Item,
                                DQty = _locProjectLineItem.DQty,
                                DUOM = _locProjectLineItem.DUOM,
                                Qty = _locProjectLineItem.Qty,
                                TQty = _locProjectLineItem.TQty,
                                UnitAmount = _locUnitAmount,
                                TotalUnitAmount = _locUnitAmount * _locProjectLineItem.TQty,
                                Description = _locProjectLineItem.Description,
                                ProjectLine2 = _locProjectLine2x,
                                ProjectHeader = _locProjectHeaderXPO,
                                ProjectLineItem = _locProjectLineItem,
                                Company = _locProjectLineXPO.Company,
                            };
                            _saveProjectLine2Item.Save();
                            _saveProjectLine2Item.Session.CommitTransaction();
                        }
                    }
                }

                #endregion ProjectLine2Item
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ProjectLine " + ex.ToString());
            }
        }

        private void SetProjectLine2Item2(Session _currSession, ProjectHeader _locProjectHeaderXPO, ProjectLine _locProjectLineXPO, ProjectLine2 _locProjectLine2x)
        {
            try
            {
                double _locUnitAmount = 0;
                #region ProjectLine2Item2

                XPCollection<ProjectLine2Item2> _locProjectLine2Item2s = new XPCollection<ProjectLine2Item2>(_currSession,
                                                    new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("ProjectLine2", _locProjectLine2x),
                                                    new BinaryOperator("ProjectLine", _locProjectLineXPO),
                                                    new BinaryOperator("ProjectHeader", _locProjectHeaderXPO)));

                XPCollection<ProjectLineItem2> _locProjectLineItem2s = new XPCollection<ProjectLineItem2>(_currSession,
                                                    new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("ProjectLine", _locProjectLineXPO),
                                                    new BinaryOperator("ProjectHeader", _locProjectHeaderXPO)));

                if (_locProjectLine2Item2s != null && _locProjectLine2Item2s.Count() > 0)
                {
                    foreach (ProjectLine2Item2 _locProjectLine2Item2 in _locProjectLine2Item2s)
                    {
                        if (_locProjectLineItem2s != null && _locProjectLineItem2s.Count() > 0)
                        {
                            foreach (ProjectLineItem2 _locProjectLineItem2 in _locProjectLineItem2s)
                            {
                                _locUnitAmount = GetPriceLineAmount2ByItem(_currSession, _locProjectLineItem2.Item, _locProjectLineXPO);

                                if (_locProjectLine2Item2.Item == _locProjectLineItem2.Item && _locProjectLine2Item2.ProjectLineItem2 == _locProjectLineItem2)
                                {
                                    _locProjectLine2Item2.Name = _locProjectLineItem2.Name;
                                    _locProjectLine2Item2.Item = _locProjectLineItem2.Item;
                                    _locProjectLine2Item2.DQty = _locProjectLineItem2.DQty;
                                    _locProjectLine2Item2.DUOM = _locProjectLineItem2.DUOM;
                                    _locProjectLine2Item2.Qty = _locProjectLineItem2.Qty;
                                    _locProjectLine2Item2.UOM = _locProjectLineItem2.UOM;
                                    _locProjectLine2Item2.Brand = _locProjectLineItem2.Brand;
                                    _locProjectLine2Item2.TQty = _locProjectLineItem2.TQty;
                                    _locProjectLine2Item2.UnitAmount = _locUnitAmount;
                                    _locProjectLine2Item2.TotalUnitAmount = _locUnitAmount * _locProjectLineItem2.TQty;
                                    _locProjectLine2Item2.ProjectLineItem2 = _locProjectLineItem2;
                                    _locProjectLine2Item2.ProjectLine = _locProjectLineXPO;
                                    _locProjectLine2Item2.ProjectLine2 = _locProjectLine2x;
                                    _locProjectLine2Item2.ProjectHeader = _locProjectHeaderXPO;
                                    _locProjectLine2Item2.Save();
                                    _locProjectLine2Item2.Session.CommitTransaction();
                                }
                                else
                                {
                                    ProjectLine2Item2 _locSaveProjectLine2Item2 = new ProjectLine2Item2(_currSession)
                                    {
                                        Name = _locProjectLineItem2.Name,
                                        Item = _locProjectLineItem2.Item,
                                        DQty = _locProjectLineItem2.DQty,
                                        DUOM = _locProjectLineItem2.DUOM,
                                        Qty = _locProjectLineItem2.Qty,
                                        UOM = _locProjectLineItem2.UOM,
                                        Brand = _locProjectLineItem2.Brand,
                                        TQty = _locProjectLineItem2.TQty,
                                        UnitAmount = _locUnitAmount,
                                        TotalUnitAmount = _locUnitAmount * _locProjectLineItem2.TQty,
                                        ProjectLineItem2 = _locProjectLineItem2,
                                        ProjectLine = _locProjectLineXPO,
                                        ProjectLine2 = _locProjectLine2x,
                                        ProjectHeader = _locProjectHeaderXPO,
                                        Company = _locProjectLineXPO.Company,
                                    };
                                    _locSaveProjectLine2Item2.Save();
                                    _locSaveProjectLine2Item2.Session.CommitTransaction();
                                }
                            }
                        }

                    }
                }
                else
                {
                    if (_locProjectLineItem2s != null && _locProjectLineItem2s.Count() > 0)
                    {
                        foreach (ProjectLineItem2 _locProjectLineItem2 in _locProjectLineItem2s)
                        {
                            _locUnitAmount = GetPriceLineAmount2ByItem(_currSession, _locProjectLineItem2.Item, _locProjectLineXPO);

                            ProjectLine2Item2 _locSaveProjectLine2Item2 = new ProjectLine2Item2(_currSession)
                            {
                                Name = _locProjectLineItem2.Name,
                                Item = _locProjectLineItem2.Item,
                                DQty = _locProjectLineItem2.DQty,
                                DUOM = _locProjectLineItem2.DUOM,
                                Qty = _locProjectLineItem2.Qty,
                                UOM = _locProjectLineItem2.UOM,
                                Brand = _locProjectLineItem2.Brand,
                                TQty = _locProjectLineItem2.TQty,
                                UnitAmount = _locUnitAmount,
                                TotalUnitAmount = _locUnitAmount * _locProjectLineItem2.TQty,
                                ProjectLineItem2 = _locProjectLineItem2,
                                ProjectLine = _locProjectLineXPO,
                                ProjectLine2 = _locProjectLine2x,
                                ProjectHeader = _locProjectHeaderXPO,
                                Company = _locProjectLineXPO.Company,
                            };
                            _locSaveProjectLine2Item2.Save();
                            _locSaveProjectLine2Item2.Session.CommitTransaction();
                        }
                    }
                }

                #endregion ProjectLine2Item2

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ProjectLine " + ex.ToString());
            }
        }

        private void SetProjectLine2Service(Session _currSession, ProjectHeader _locProjectHeaderXPO, ProjectLine _locProjectLineXPO, ProjectLine2 _locProjectLine2x)
        {
            try
            {
                double _locUnitAmount = 0;
                #region ProjectLine2Service

                XPCollection<ProjectLine2Service> _locProjectLine2Services = new XPCollection<ProjectLine2Service>
                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                new BinaryOperator("ProjectLine2", _locProjectLine2x),
                                                new BinaryOperator("ProjectLine", _locProjectLineXPO),
                                                new BinaryOperator("ProjectHeader", _locProjectHeaderXPO)));

                XPCollection<ProjectLineService> _locProjectLineServices = new XPCollection<ProjectLineService>
                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                new BinaryOperator("ProjectLine", _locProjectLineXPO),
                                                new BinaryOperator("ProjectHeader", _locProjectHeaderXPO)));

                if (_locProjectLine2Services != null && _locProjectLine2Services.Count() > 0)
                {
                    foreach (ProjectLine2Service _locProjectLine2Service in _locProjectLine2Services)
                    {
                        if (_locProjectLineServices != null && _locProjectLineServices.Count() > 0)
                        {
                            foreach (ProjectLineService _locProjectLineService in _locProjectLineServices)
                            {
                                _locUnitAmount = GetPriceLineAmount2ByService(_currSession, _locProjectLineService.Item, _locProjectLineXPO);

                                if (_locProjectLineService.Item == _locProjectLine2Service.Item && _locProjectLine2Service.ProjectLineService == _locProjectLineService)
                                {
                                    _locProjectLine2Service.Name = _locProjectLineService.Name;
                                    _locProjectLine2Service.Item = _locProjectLineService.Item;
                                    _locProjectLine2Service.Qty = _locProjectLineService.Qty;
                                    _locProjectLine2Service.UOM = _locProjectLineService.UOM;
                                    _locProjectLine2Service.UnitAmount = _locUnitAmount;
                                    _locProjectLine2Service.TotalUnitAmount = _locUnitAmount * _locProjectLineService.Qty;
                                    _locProjectLine2Service.ProjectLineService = _locProjectLineService;
                                    _locProjectLine2Service.ProjectLine = _locProjectLineXPO;
                                    _locProjectLine2Service.ProjectLine2 = _locProjectLine2x;
                                    _locProjectLine2Service.ProjectHeader = _locProjectHeaderXPO;
                                    _locProjectLine2Service.Save();
                                    _locProjectLine2Service.Session.CommitTransaction();
                                }
                                else
                                {
                                    ProjectLine2Service _locSaveProjectLine2Service = new ProjectLine2Service(_currSession)
                                    {
                                        Name = _locProjectLineService.Name,
                                        Item = _locProjectLineService.Item,
                                        Qty = _locProjectLineService.Qty,
                                        UOM = _locProjectLineService.UOM,
                                        UnitAmount = _locUnitAmount,
                                        TotalUnitAmount = _locUnitAmount * _locProjectLineService.Qty,
                                        ProjectLineService = _locProjectLineService,
                                        ProjectLine = _locProjectLineXPO,
                                        ProjectLine2 = _locProjectLine2x,
                                        ProjectHeader = _locProjectHeaderXPO,
                                        Company = _locProjectLineXPO.Company,
                                    };
                                    _locSaveProjectLine2Service.Save();
                                    _locSaveProjectLine2Service.Session.CommitTransaction();
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (_locProjectLineServices != null && _locProjectLineServices.Count() > 0)
                    {
                        foreach (ProjectLineService _locProjectLineService in _locProjectLineServices)
                        {
                            _locUnitAmount = GetPriceLineAmount2ByService(_currSession, _locProjectLineService.Item, _locProjectLineXPO);

                            ProjectLine2Service _locSaveProjectLine2Service = new ProjectLine2Service(_currSession)
                            {
                                Name = _locProjectLineService.Name,
                                Item = _locProjectLineService.Item,
                                Qty = _locProjectLineService.Qty,
                                UOM = _locProjectLineService.UOM,
                                UnitAmount = _locUnitAmount,
                                TotalUnitAmount = _locUnitAmount * _locProjectLineService.Qty,
                                ProjectLineService = _locProjectLineService,
                                ProjectLine = _locProjectLineXPO,
                                ProjectLine2 = _locProjectLine2x,
                                ProjectHeader = _locProjectHeaderXPO,
                                Company = _locProjectLineXPO.Company,
                            };
                            _locSaveProjectLine2Service.Save();
                            _locSaveProjectLine2Service.Session.CommitTransaction();
                        }
                    }
                }

                #endregion ProjectLine2Service
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ProjectLine " + ex.ToString());
            }
        }

        private double GetTotalQty(Session _currSession, ProjectLineItem _locProjectLineItem)
        {
            double _result = 0;
            try
            {
                ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                    (new GroupOperator(GroupOperatorType.And,
                                                     new BinaryOperator("Item", _locProjectLineItem.Item),
                                                     new BinaryOperator("UOM", _locProjectLineItem.UOM),
                                                     new BinaryOperator("DefaultUOM", _locProjectLineItem.DUOM),
                                                     new BinaryOperator("Company", _locProjectLineItem),
                                                     new BinaryOperator("Active", true)));
                if (_locItemUOM != null)
                {
                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                    {
                        _result = _locProjectLineItem.Qty * _locItemUOM.DefaultConversion + _locProjectLineItem.DQty;
                    }
                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                    {
                        _result = _locProjectLineItem.Qty / _locItemUOM.Conversion + _locProjectLineItem.DQty;
                    }
                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                    {
                        _result = _locProjectLineItem.Qty + _locProjectLineItem.DQty;
                    }
                }
                else
                {
                    _result = _locProjectLineItem.Qty + _locProjectLineItem.DQty;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ProjectLine " + ex.ToString());
            }
            return _result;
        }

        private double GetPriceLineAmount1ByItem(Session _currSession, Item _locItem, ProjectLine _locProjectLineXPO)
        {
            double _result = 0;
            try
            {
                if (_locItem != null)
                {
                    Price _locPrice = _currSession.FindObject<Price>(new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Item", _locItem),
                                                                     new BinaryOperator("Company", _locProjectLineXPO.Company)));
                    if (_locPrice != null)
                    {
                        PriceLine _locPriceLine = _currSession.FindObject<PriceLine>(new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("Price", _locPrice),
                                                    new BinaryOperator("Item", _locItem),
                                                    new BinaryOperator("PriceType", OrderType.Item),
                                                    new BinaryOperator("Company", _locProjectLineXPO.Company),
                                                    new BinaryOperator("Active", true)));
                        if (_locPriceLine != null)
                        {
                            _result = _locPriceLine.Amount1;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ProjectLine " + ex.ToString());
            }
            return _result;
        }

        private double GetPriceLineAmount2ByItem(Session _currSession, Item _locItem, ProjectLine _locProjectLineXPO)
        {
            double _result = 0;
            try
            {
                if (_locItem != null)
                {
                    Price _locPrice = _currSession.FindObject<Price>(new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Item", _locItem),
                                                                     new BinaryOperator("Company", _locProjectLineXPO.Company)));
                    if (_locPrice != null)
                    {
                        PriceLine _locPriceLine = _currSession.FindObject<PriceLine>(new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("Price", _locPrice),
                                                    new BinaryOperator("Item", _locItem),
                                                    new BinaryOperator("PriceType", OrderType.Item),
                                                    new BinaryOperator("Company", _locProjectLineXPO.Company),
                                                    new BinaryOperator("Active", true)));
                        if (_locPriceLine != null)
                        {
                            _result = _locPriceLine.Amount2;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ProjectLine " + ex.ToString());
            }
            return _result;
        }

        private double GetPriceLineAmount1ByService(Session _currSession, Item _locItem, ProjectLine _locProjectLineXPO)
        {
            double _result = 0;
            try
            {
                if (_locItem != null)
                {
                    Price _locPrice = _currSession.FindObject<Price>(new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Item", _locItem),
                                                                     new BinaryOperator("Company", _locProjectLineXPO.Company)));
                    if (_locPrice != null)
                    {
                        PriceLine _locPriceLine = _currSession.FindObject<PriceLine>(new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("Price", _locPrice),
                                                    new BinaryOperator("Item", _locItem),
                                                    new BinaryOperator("PriceType", OrderType.Service),
                                                    new BinaryOperator("Company", _locProjectLineXPO.Company),
                                                    new BinaryOperator("Active", true)));
                        if (_locPriceLine != null)
                        {
                            _result = _locPriceLine.Amount1;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ProjectLine " + ex.ToString());
            }
            return _result;
        }

        private double GetPriceLineAmount2ByService(Session _currSession, Item _locItem, ProjectLine _locProjectLineXPO)
        {
            double _result = 0;
            try
            {
                if (_locItem != null)
                {
                    Price _locPrice = _currSession.FindObject<Price>(new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Item", _locItem),
                                                                     new BinaryOperator("Company", _locProjectLineXPO.Company)));
                    if (_locPrice != null)
                    {
                        PriceLine _locPriceLine = _currSession.FindObject<PriceLine>(new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("Price", _locPrice),
                                                    new BinaryOperator("Item", _locItem),
                                                    new BinaryOperator("PriceType", OrderType.Service),
                                                    new BinaryOperator("Company", _locProjectLineXPO.Company),
                                                    new BinaryOperator("Active", true)));
                        if (_locPriceLine != null)
                        {
                            _result = _locPriceLine.Amount2;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ProjectLine " + ex.ToString());
            }
            return _result;
        }

        #endregion GlobalProjectLineMethod

        #region Global Method

        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0}", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method

    }
}
