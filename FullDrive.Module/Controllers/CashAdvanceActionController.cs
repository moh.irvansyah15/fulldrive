﻿#region Default

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

#endregion Default

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;
using System.Web;
using System.IO;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class CashAdvanceActionController : ViewController
    {
        #region Default

        private ChoiceActionItem _selectionListviewFilter;
        private ChoiceActionItem _setApprovalLevel;

        public CashAdvanceActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.

            #region FilterStatus
            CashAdvanceListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                CashAdvanceListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
            #endregion FilterStatus

            #region FilterApproval
            CashAdvanceListviewFilterApprovalSelectionAction.Items.Clear();
            foreach (object _currApprovalFilter in Enum.GetValues(typeof(CustomProcess.ApprovalFilter)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.ApprovalFilter));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApprovalFilter), _currApprovalFilter);
                CashAdvanceListviewFilterApprovalSelectionAction.Items.Add(_selectionListviewFilter);
            }
            #endregion FilterApproval
        }
        protected override void OnActivated()
        {
            TypeOfView = typeof(ObjectView);
            base.OnActivated();
            // Perform various tasks depending on the target View.
            #region Approval
            var _locUser = SecuritySystem.CurrentUserName;
            GlobalFunction _globalFunction = new GlobalFunction();
            Session _currentSession = null;
            if (this.ObjectSpace != null)
            {
                _currentSession = ((XPObjectSpace)this.ObjectSpace).Session;
            }

            if (_currentSession != null)
            {
                UserAccess _locUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", _locUser));

                if (_locUserAccess != null)
                {
                    CashAdvanceApprovalAction.Items.Clear();

                    XPCollection<ApplicationSetupDetail> _locAppSetupDetails = new XPCollection<ApplicationSetupDetail>
                                                                                 (_currentSession, new GroupOperator(GroupOperatorType.And,
                                                                                  new BinaryOperator("UserAccess", _locUserAccess),
                                                                                  new BinaryOperator("ObjectList", CustomProcess.ObjectList.CashAdvance),
                                                                                  new BinaryOperator("FunctionList", CustomProcess.FunctionList.Approval),
                                                                                  new BinaryOperator("Active", true)));

                    if (_locAppSetupDetails != null && _locAppSetupDetails.Count > 0)
                    {

                        foreach (ApplicationSetupDetail _locAppSetupDetail in _locAppSetupDetails)
                        {
                            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.ApprovalLevel)))
                            {
                                if (_locAppSetupDetail.ApprovalLevel != CustomProcess.ApprovalLevel.None)
                                {
                                    EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.ApprovalLevel));
                                    _setApprovalLevel = new ChoiceActionItem(_ed.GetCaption(_locAppSetupDetail.ApprovalLevel), _locAppSetupDetail.ApprovalLevel);
                                }
                            }
                            CashAdvanceApprovalAction.Items.Add(_setApprovalLevel);
                        }
                    }
                }
            }
            #endregion Approval
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        #endregion Default

        private void CashAdvanceProgressActionController_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        CashAdvance _locCashAdvanceOS = (CashAdvance)_objectSpace.GetObject(obj);

                        if (_locCashAdvanceOS != null)
                        {
                            if (_locCashAdvanceOS.Code != null)
                            {
                                _currObjectId = _locCashAdvanceOS.Code;

                                CashAdvance _locCashAdvanceXPO = _currSession.FindObject<CashAdvance>
                                                                  (new GroupOperator(GroupOperatorType.And,
                                                                  new BinaryOperator("Code", _currObjectId)));

                                if (_locCashAdvanceXPO != null)
                                {
                                    if (_locCashAdvanceXPO.Status == Status.Open || _locCashAdvanceXPO.Status == Status.Progress)
                                    {
                                        if (_locCashAdvanceXPO.Status == Status.Open)
                                        {
                                            _locCashAdvanceXPO.Status = Status.Progress;
                                            _locCashAdvanceXPO.Save();
                                            _locCashAdvanceXPO.Session.CommitTransaction();
                                        }

                                        XPCollection<CashAdvanceLine> _locCashAdvanceLines = new XPCollection<CashAdvanceLine>
                                                                                             (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                              new BinaryOperator("CashAdvance", _locCashAdvanceXPO),
                                                                                              new BinaryOperator("Status", Status.Open)));

                                        if (_locCashAdvanceLines != null && _locCashAdvanceLines.Count > 0)
                                        {
                                            foreach (CashAdvanceLine _locCashAdvanceLine in _locCashAdvanceLines)
                                            {
                                                _locCashAdvanceLine.Status = Status.Progress;
                                                _locCashAdvanceLine.StatusDate = now;
                                                _locCashAdvanceLine.Save();
                                                _locCashAdvanceLine.Session.CommitTransaction();
                                            }
                                        }
                                    }
                                    SuccessMessageShow("CashAdvance has successfully updated to progress");
                                }
                                else
                                {
                                    ErrorMessageShow("CashAdvance Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("CashAdvance Not Available");
                            }
                        }
                    }
                }
                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = CashAdvance" + ex.ToString());
            }
        }

        private void CashAdvancePostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        CashAdvance _locCashAdvanceOS = (CashAdvance)_objectSpace.GetObject(obj);

                        if (_locCashAdvanceOS != null)
                        {
                            if (_locCashAdvanceOS.Code != null)
                            {
                                _currObjectId = _locCashAdvanceOS.Code;

                                CashAdvance _locCashAdvanceXPO = _currSession.FindObject<CashAdvance>
                                                                 (new GroupOperator(GroupOperatorType.And,
                                                                  new BinaryOperator("Code", _currObjectId)));

                                if (_locCashAdvanceXPO != null)
                                {
                                    if (_locCashAdvanceXPO.Status == Status.Progress)
                                    {
                                        ApprovalLine _locApprovalLineXPO = _currSession.FindObject<ApprovalLine>
                                                                           (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("EndApproval", true),
                                                                            new BinaryOperator("CashAdvance", _locCashAdvanceXPO)));

                                        if (_locApprovalLineXPO != null)
                                        {
                                            if(GetMaxAmountRealizedCashAdvanceLine(_currSession, _locCashAdvanceXPO) <= _locCashAdvanceXPO.AmountRealized)
                                            {
                                                SetCashAdvanceMonitoring(_currSession, _locCashAdvanceXPO);
                                                SetJournalCashBon(_currSession, _locCashAdvanceXPO);
                                                SetProcessCountForPostingCashAdvance(_currSession, _locCashAdvanceXPO);
                                                SetStatusCashAdvanceLine(_currSession, _locCashAdvanceXPO);
                                                SetStatusCashAdvance(_currSession, _locCashAdvanceXPO);
                                                SuccessMessageShow(_locCashAdvanceXPO.Code + " has been change successfully to Posted");
                                            } 
                                        }
                                        else
                                        {
                                            ErrorMessageShow("Data CashAdvance Not Available");
                                        }
                                    }
                                    else
                                    {
                                        ErrorMessageShow("Data CashAdvance Not Available");
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data CashAdvance Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data CashAdvance Not Available");
                            }
                        }
                        else
                        {
                            ErrorMessageShow("Data CashAdvance Not Available");
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CashAdvance " + ex.ToString());
            }
        }

        private void CashAdvanceListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(CashAdvance)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CashAdvance " + ex.ToString());
            }
        }

        private void CashAdvanceListviewFilterApprovalSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(CashAdvance)))
                {
                    if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.Approval)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("ActiveApproved3", false, BinaryOperatorType.Equal),
                                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("ActiveApproved1", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", true, BinaryOperatorType.Equal)));
                    }
                    else if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.EndApproval)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new BinaryOperator("ActiveApproved3", true, BinaryOperatorType.Equal);
                    }
                    else if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.AllData)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("ActiveApproved1", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved3", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved1", false, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", false, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved3", false, BinaryOperatorType.Equal));
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CashAdvance " + ex.ToString());
            }
        }

        private void CashAdvanceApprovalAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                ApplicationSetupDetail _locAppSetDetail = null;
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                var User = SecuritySystem.CurrentUserName;
                Session _currentSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currentSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                UserAccess _locUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", User));

                foreach (Object obj in _objectsToProcess)
                {
                    CashAdvance _objInNewObjectSpace = (CashAdvance)_objectSpace.GetObject(obj);

                    if (_objInNewObjectSpace != null)
                    {
                        if (_objInNewObjectSpace.Code != null)
                        {
                            _currObjectId = _objInNewObjectSpace.Code;
                        }
                    }

                    if (_currObjectId != null)
                    {
                        CashAdvance _locCashAdvanceXPO = _currentSession.FindObject<CashAdvance>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("Code", _currObjectId)));

                        if (_locCashAdvanceXPO != null)
                        {
                            if (_locCashAdvanceXPO.Status == Status.Progress)
                            {
                                ApprovalLine _locApprovalLine = _currentSession.FindObject<ApprovalLine>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("CashAdvance", _locCashAdvanceXPO),
                                                                             new BinaryOperator("EndApproval", true)));
                                if (_locApprovalLine == null)
                                {
                                    #region Approval Level 1
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level1)
                                    {
                                        _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level1, ObjectList.CashAdvance);
                                        if (_locAppSetDetail != null)
                                        {
                                            //Buat bs input langsung ke approvalline
                                            ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("CashAdvance", _locCashAdvanceXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level1)));
                                            if (_locApprovalLineXPO == null)
                                            {

                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level1,
                                                        EndApproval = true,
                                                        CashAdvance = _locCashAdvanceXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locCashAdvanceXPO.ActivationPosting = true;
                                                    _locCashAdvanceXPO.ActiveApproved1 = false;
                                                    _locCashAdvanceXPO.ActiveApproved2 = false;
                                                    _locCashAdvanceXPO.ActiveApproved3 = true;
                                                    _locCashAdvanceXPO.Save();
                                                    _locCashAdvanceXPO.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level1,
                                                        CashAdvance = _locCashAdvanceXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locCashAdvanceXPO.ActiveApproved1 = true;
                                                    _locCashAdvanceXPO.ActiveApproved2 = false;
                                                    _locCashAdvanceXPO.ActiveApproved3 = false;
                                                    _locCashAdvanceXPO.Save();
                                                    _locCashAdvanceXPO.Session.CommitTransaction();
                                                }

                                                //Send Email
                                                ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("CashAdvance", _locCashAdvanceXPO)));

                                                if (_locApprovalLineXPO2 != null)
                                                {
                                                    SendEmail(_currentSession, _locCashAdvanceXPO, Status.Progress, _locApprovalLineXPO2);
                                                }

                                                SuccessMessageShow("CashAdvance has successfully Approve");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 1

                                    #region Approval Level 2
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level2)
                                    {
                                        _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level2, ObjectList.CashAdvance);

                                        if (_locAppSetDetail != null)
                                        {
                                            ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("CashAdvance", _locCashAdvanceXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level2)));

                                            if (_locApprovalLineXPO == null)
                                            {
                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level2,
                                                        EndApproval = true,
                                                        CashAdvance = _locCashAdvanceXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locCashAdvanceXPO.ActivationPosting = true;
                                                    _locCashAdvanceXPO.ActiveApproved1 = false;
                                                    _locCashAdvanceXPO.ActiveApproved2 = false;
                                                    _locCashAdvanceXPO.ActiveApproved3 = true;
                                                    _locCashAdvanceXPO.Save();
                                                    _locCashAdvanceXPO.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level2,
                                                        CashAdvance = _locCashAdvanceXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    SetApprovalLine(_currentSession, _locCashAdvanceXPO, ApprovalLevel.Level1);

                                                    _locCashAdvanceXPO.ActiveApproved1 = false;
                                                    _locCashAdvanceXPO.ActiveApproved2 = true;
                                                    _locCashAdvanceXPO.ActiveApproved3 = false;
                                                    _locCashAdvanceXPO.Save();
                                                    _locCashAdvanceXPO.Session.CommitTransaction();
                                                }

                                                //Send Email
                                                ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("CashAdvance", _locCashAdvanceXPO)));

                                                if (_locApprovalLineXPO2 != null)
                                                {
                                                    SendEmail(_currentSession, _locCashAdvanceXPO, Status.Progress, _locApprovalLineXPO2);
                                                }

                                                SuccessMessageShow("CashAdvance has successfully Approve");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 2

                                    #region Approval Level 3
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level3)
                                    {
                                        _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level3, ObjectList.CashAdvance);

                                        if (_locAppSetDetail != null)
                                        {
                                            ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("CashAdvance", _locCashAdvanceXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level3)));

                                            if (_locApprovalLineXPO == null)
                                            {
                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level3,
                                                        EndApproval = true,
                                                        CashAdvance = _locCashAdvanceXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locCashAdvanceXPO.ActivationPosting = true;
                                                    _locCashAdvanceXPO.ActiveApproved1 = false;
                                                    _locCashAdvanceXPO.ActiveApproved2 = false;
                                                    _locCashAdvanceXPO.ActiveApproved3 = true;
                                                    _locCashAdvanceXPO.Save();
                                                    _locCashAdvanceXPO.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level3,
                                                        CashAdvance = _locCashAdvanceXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    SetApprovalLine(_currentSession, _locCashAdvanceXPO, ApprovalLevel.Level2);
                                                    SetApprovalLine(_currentSession, _locCashAdvanceXPO, ApprovalLevel.Level1);

                                                    _locCashAdvanceXPO.ActiveApproved1 = false;
                                                    _locCashAdvanceXPO.ActiveApproved2 = false;
                                                    _locCashAdvanceXPO.ActiveApproved3 = true;
                                                    _locCashAdvanceXPO.Save();
                                                    _locCashAdvanceXPO.Session.CommitTransaction();
                                                }

                                                //Send Email
                                                ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("CashAdvance", _locCashAdvanceXPO)));

                                                if (_locApprovalLineXPO2 != null)
                                                {
                                                    SendEmail(_currentSession, _locCashAdvanceXPO, Status.Progress, _locApprovalLineXPO2);
                                                }

                                                SuccessMessageShow("CashAdvance has successfully Approve");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 3
                                }

                            }
                            else
                            {
                                ErrorMessageShow("CashAdvance Status Not Available");
                            }
                        }
                        else
                        {
                            ErrorMessageShow("CashAdvance Not Available");
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CashAdvance " + ex.ToString());
            }
        }

        //========================================================= Code Only ============================================

        #region Posting
        
        private double GetMaxAmountRealizedCashAdvanceLine(Session _currSession, CashAdvance _locCashAdvanceXPO)
        {
            double _result = 0;
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                double _locMaxAmountRealized = 0;

                if (_locCashAdvanceXPO != null)
                {
                    XPCollection<CashAdvanceLine> _locCashAdvanceLines = new XPCollection<CashAdvanceLine>(_currSession,
                                                                            new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("CashAdvance", _locCashAdvanceXPO)));
                    if (_locCashAdvanceLines != null && _locCashAdvanceLines.Count() > 0)
                    {
                        foreach (CashAdvanceLine _locCashAdvanceLine in _locCashAdvanceLines)
                        {
                            if (_locCashAdvanceLine.AmountRealized > 0)
                            {
                                _locMaxAmountRealized = _locMaxAmountRealized + _locCashAdvanceLine.AmountRealized;
                            }
                        }

                        _result = _locMaxAmountRealized;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = CashAdvance ", ex.ToString());
            }
            return _result;
        }

        private void SetCashAdvanceMonitoring(Session _currSession, CashAdvance _locCashAdvanceXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _currSignCode = null;
                

                if (_locCashAdvanceXPO != null)
                {
                    XPCollection<CashAdvanceLine> _locCashAdvanceLines = new XPCollection<CashAdvanceLine>(_currSession,
                                                                            new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("CashAdvance", _locCashAdvanceXPO),
                                                                            new BinaryOperator("Select", true),
                                                                            new GroupOperator(GroupOperatorType.Or,
                                                                            new BinaryOperator("Status", Status.Progress),
                                                                            new BinaryOperator("Status", Status.Posted))));
                    if (_locCashAdvanceLines != null && _locCashAdvanceLines.Count() > 0)
                    {
                        foreach (CashAdvanceLine _locCashAdvanceLine in _locCashAdvanceLines)
                        {
                            if(_locCashAdvanceLine.AmountRealized > 0)
                            {
                                _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.CashAdvanceMonitoring);

                                if(_currSignCode != null)
                                {
                                    CashAdvanceMonitoring _saveDataCashAdvanceMonitoring = new CashAdvanceMonitoring(_currSession)
                                    {
                                        SignCode = _currSignCode,
                                        CashAdvance = _locCashAdvanceXPO,
                                        CashAdvanceLine = _locCashAdvanceLine,
                                        Employee = _locCashAdvanceXPO.Employee,
                                        RequestDate = _locCashAdvanceLine.RequestDate,
                                        RequiredDate = _locCashAdvanceLine.RequiredDate,
                                        CashAdvanceType = _locCashAdvanceLine.CashAdvanceType,
                                        Amount = _locCashAdvanceLine.Amount,
                                        AmountRealized = _locCashAdvanceLine.AmountRealized,
                                        Description = _locCashAdvanceLine.Description,
                                        Company = _locCashAdvanceLine.Company,
                                        Division = _locCashAdvanceLine.Division,
                                        Department = _locCashAdvanceLine.Department,
                                        Section = _locCashAdvanceLine.Section,
                                    };
                                    _saveDataCashAdvanceMonitoring.Save();
                                    _saveDataCashAdvanceMonitoring.Session.CommitTransaction();

                                    CashAdvanceMonitoring _locCashAdvanceMonitoring = _currSession.FindObject<CashAdvanceMonitoring>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("SignCode", _currSignCode)));
                                    if(_locCashAdvanceMonitoring != null)
                                    {
                                        SetCashAdvancePaymentPlan(_currSession, _locCashAdvanceXPO, _locCashAdvanceMonitoring);
                                    }
                                }    
                            }     
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = CashAdvance ", ex.ToString());
            }
        }

        private void SetCashAdvancePaymentPlan(Session _currSession, CashAdvance _locCashAdvanceXPO, CashAdvanceMonitoring _locCashAdvanceMonitoringXPO)
        {
            try
            {
                DateTime now = DateTime.Now;

                if (_locCashAdvanceXPO != null && _locCashAdvanceMonitoringXPO != null)
                {
                    CashAdvancePaymentPlan _locCashAdvancePaymentPlan = _currSession.FindObject<CashAdvancePaymentPlan>(
                                                                            new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("CashAdvanceMonitoring", _locCashAdvanceMonitoringXPO),
                                                                            new BinaryOperator("CashAdvance", _locCashAdvanceXPO)));
                    if (_locCashAdvancePaymentPlan == null)
                    {
                        CashAdvancePaymentPlan _saveCashAdvancePaymentPlan = new CashAdvancePaymentPlan(_currSession)
                        {
                            CashAdvanceType = _locCashAdvanceMonitoringXPO.CashAdvanceType,
                            Employee = _locCashAdvanceMonitoringXPO.Employee,
                            Amount = _locCashAdvanceMonitoringXPO.AmountRealized,                      
                            Outstanding = _locCashAdvanceMonitoringXPO.AmountRealized,
                            Company = _locCashAdvanceMonitoringXPO.Company,
                            Division = _locCashAdvanceMonitoringXPO.Division,
                            Department = _locCashAdvanceMonitoringXPO.Department,
                            Section = _locCashAdvanceMonitoringXPO.Section,
                            CashAdvance = _locCashAdvanceMonitoringXPO.CashAdvance,
                            CashAdvanceMonitoring = _locCashAdvanceMonitoringXPO,
                        };
                        _saveCashAdvancePaymentPlan.Save();
                        _saveCashAdvancePaymentPlan.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = CashAdvance ", ex.ToString());
            }
        }

        private void SetJournalCashBon(Session _currSession, CashAdvance _locCashAdvanceXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                double _locSetCAApproved = 0;


                if (_locCashAdvanceXPO != null)
                {
                    XPCollection<CashAdvanceLine> _locCashAdvanceLines = new XPCollection<CashAdvanceLine>(_currSession,
                                                                             new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("CashAdvance", _locCashAdvanceXPO),
                                                                             new BinaryOperator("Select", true),
                                                                             new GroupOperator(GroupOperatorType.Or, 
                                                                             new BinaryOperator("Status", Status.Progress),
                                                                             new BinaryOperator("Status", Status.Posted)
                                                                             )));

                    if (_locCashAdvanceLines != null && _locCashAdvanceLines.Count() > 0)
                    {
                        foreach (CashAdvanceLine _locCashAdvanceLine in _locCashAdvanceLines)
                        {
                            if (_locCashAdvanceLine.AmountRealized > 0)
                            {
                                _locSetCAApproved = _locCashAdvanceLine.AmountRealized;

                                #region JournalMapByCompany
                                if (_locCashAdvanceXPO.Company != null)
                                {
                                    if (_locCashAdvanceXPO.Company.CompanyAccountGroup != null)
                                    {
                                        XPCollection<JournalMap> _locJournalCashAdvanceByCompanys = new XPCollection<JournalMap>
                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("CompanyAccountGroup", _locCashAdvanceXPO.Company.CompanyAccountGroup)));

                                        if (_locJournalCashAdvanceByCompanys != null && _locJournalCashAdvanceByCompanys.Count() > 0)
                                        {
                                            foreach (JournalMap _locJournalCashAdvanceByCompany in _locJournalCashAdvanceByCompanys)
                                            {
                                                XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                    new BinaryOperator("JournalMap", _locJournalCashAdvanceByCompany)));

                                                if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                {
                                                    foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                    {
                                                        AccountMap _locAccountMap = _currSession.FindObject<AccountMap>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("Code", _locJournalMapLine.AccountMap.Code),
                                                                                     new BinaryOperator("PostingType", PostingType.Advances),
                                                                                     new BinaryOperator("OrderType", OrderType.Account),
                                                                                     new BinaryOperator("PostingMethod", PostingMethod.CashAdvance),
                                                                                     new BinaryOperator("PostingMethodType", PostingMethodType.Advances)));

                                                        if (_locAccountMap != null)
                                                        {
                                                            XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                new BinaryOperator("AccountMap", _locAccountMap),
                                                                                                                new BinaryOperator("Active", true)));

                                                            if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                            {
                                                                double _locTotalAmountDebit = 0;
                                                                double _locTotalAmountCredit = 0;
                                                                double _locTotalBalance = 0;

                                                                foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                {
                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                    {
                                                                        _locTotalAmountDebit = _locSetCAApproved;
                                                                    }
                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                    {
                                                                        _locTotalAmountCredit = _locSetCAApproved;
                                                                    }

                                                                    GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                    {
                                                                        PostingDate = now,
                                                                        PostingType = PostingType.Advances,
                                                                        OrderType = OrderType.Account,
                                                                        PostingMethod = PostingMethod.CashAdvance,
                                                                        PostingMethodType = PostingMethodType.Advances,
                                                                        Account = _locAccountMapLine.Account,
                                                                        Debit = _locTotalAmountDebit,
                                                                        Credit = _locTotalAmountCredit,
                                                                        Company = _locCashAdvanceXPO.Company,
                                                                        CashAdvance = _locCashAdvanceXPO,
                                                                    };
                                                                    _saveGeneralJournal.Save();
                                                                    _saveGeneralJournal.Session.CommitTransaction();

                                                                    if (_locAccountMapLine.Account.Code != null)
                                                                    {
                                                                        #region COA
                                                                        ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("Code", _locAccountMapLine.Account.Code)));

                                                                        if (_locCOA != null)
                                                                        {
                                                                            if (_locCOA.BalanceType == BalanceType.Change)
                                                                            {
                                                                                if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                                {
                                                                                    if (_locTotalAmountDebit > 0)
                                                                                    {
                                                                                        _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                                    }
                                                                                    if (_locTotalAmountCredit > 0)
                                                                                    {
                                                                                        _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                                    }
                                                                                }
                                                                                if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                                {
                                                                                    if (_locTotalAmountDebit > 0)
                                                                                    {
                                                                                        _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                                    }
                                                                                    if (_locTotalAmountCredit > 0)
                                                                                    {
                                                                                        _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                                    }
                                                                                }
                                                                            }

                                                                            _locCOA.Balance = _locTotalBalance;
                                                                            _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                            _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                            _locCOA.Save();
                                                                            _locCOA.Session.CommitTransaction();
                                                                        }
                                                                        #endregion COA
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                #endregion JournalMap

                                #region JournalMapByEmployee
                                if (_locCashAdvanceXPO.Employee != null)
                                {
                                    OrganizationAccountGroup _locOrganizationAccountGroup = _currSession.FindObject<OrganizationAccountGroup>
                                                                                           (new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("Employee", _locCashAdvanceXPO.Employee),
                                                                                            new BinaryOperator("Active", true)));
                                    if (_locOrganizationAccountGroup != null)
                                    {
                                        XPCollection<JournalMap> _locJournalMaps = new XPCollection<JournalMap>
                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("OrganizationAccountGroup", _locOrganizationAccountGroup)));

                                        if (_locJournalMaps != null && _locJournalMaps.Count() > 0)
                                        {
                                            foreach (JournalMap _locJournalMap in _locJournalMaps)
                                            {
                                                XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                    new BinaryOperator("JournalMap", _locJournalMap)));

                                                if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                {
                                                    foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                    {
                                                        AccountMap _locAccountMap = _currSession.FindObject<AccountMap>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("Code", _locJournalMapLine.AccountMap.Code),
                                                                                     new BinaryOperator("PostingType", PostingType.Advances),
                                                                                     new BinaryOperator("OrderType", OrderType.Account),
                                                                                     new BinaryOperator("PostingMethod", PostingMethod.CashAdvance),
                                                                                     new BinaryOperator("PostingMethodType", PostingMethodType.Advances)));

                                                        if (_locAccountMap != null)
                                                        {
                                                            XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                new BinaryOperator("AccountMap", _locAccountMap),
                                                                                                                new BinaryOperator("Active", true)));

                                                            if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                            {
                                                                double _locTotalAmountDebit = 0;
                                                                double _locTotalAmountCredit = 0;
                                                                double _locTotalBalance = 0;

                                                                foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                {
                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                    {
                                                                        _locTotalAmountDebit = _locSetCAApproved;
                                                                    }
                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                    {
                                                                        _locTotalAmountCredit = _locSetCAApproved;
                                                                    }

                                                                    GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                    {
                                                                        PostingDate = now,
                                                                        PostingType = PostingType.Advances,
                                                                        OrderType = OrderType.Account,
                                                                        PostingMethod = PostingMethod.CashAdvance,
                                                                        PostingMethodType = PostingMethodType.Advances,
                                                                        Account = _locAccountMapLine.Account,
                                                                        Debit = _locTotalAmountDebit,
                                                                        Credit = _locTotalAmountCredit,
                                                                        Company = _locCashAdvanceXPO.Company,
                                                                        CashAdvance = _locCashAdvanceXPO
                                                                    };
                                                                    _saveGeneralJournal.Save();
                                                                    _saveGeneralJournal.Session.CommitTransaction();

                                                                    if (_locAccountMapLine.Account.Code != null)
                                                                    {
                                                                        #region COA
                                                                        ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("Code", _locAccountMapLine.Account.Code)));

                                                                        if (_locCOA != null)
                                                                        {
                                                                            if (_locCOA.BalanceType == BalanceType.Change)
                                                                            {
                                                                                if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                                {
                                                                                    if (_locTotalAmountDebit > 0)
                                                                                    {
                                                                                        _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                                    }
                                                                                    if (_locTotalAmountCredit > 0)
                                                                                    {
                                                                                        _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                                    }
                                                                                }
                                                                                if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                                {
                                                                                    if (_locTotalAmountDebit > 0)
                                                                                    {
                                                                                        _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                                    }
                                                                                    if (_locTotalAmountCredit > 0)
                                                                                    {
                                                                                        _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                                    }
                                                                                }
                                                                            }

                                                                            _locCOA.Balance = _locTotalBalance;
                                                                            _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                            _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                            _locCOA.Save();
                                                                            _locCOA.Session.CommitTransaction();
                                                                        }
                                                                        #endregion COA
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                #endregion JournalMapByEmployee

                                _locSetCAApproved = 0;
                            }
                        }
                    } 
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = CashAdvance ", ex.ToString());
            }
        }

        private void SetProcessCountForPostingCashAdvance(Session _currSession, CashAdvance _locCashAdvanceXPO)
        {
            try
            {
                if (_locCashAdvanceXPO != null)
                {
                    XPCollection<CashAdvanceLine> _locCALines = new XPCollection<CashAdvanceLine>(_currSession,
                                                                new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("CashAdvance", _locCashAdvanceXPO),
                                                                new BinaryOperator("Select", true),
                                                                new GroupOperator(GroupOperatorType.Or,
                                                                new BinaryOperator("Status", Status.Progress),
                                                                new BinaryOperator("Status", Status.Posted))));

                    if (_locCALines != null && _locCALines.Count > 0)
                    {
                        foreach (CashAdvanceLine _locCALine in _locCALines)
                        {
                            _locCALine.ProcessCount = _locCALine.ProcessCount + 1;
                            _locCALine.Save();
                            _locCALine.Session.CommitTransaction();
                        }
                    }
                    else
                    {
                        ErrorMessageShow("Data CashAdvanceLine Not Available");
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CashAdvance " + ex.ToString());
            }
        }

        private void SetStatusCashAdvanceLine(Session _currSession, CashAdvance _locCashAdvanceXPO)
        {
            try
            {
                DateTime now = DateTime.Now;

                if (_locCashAdvanceXPO != null)
                {
                    XPCollection<CashAdvanceLine> _locCALines = new XPCollection<CashAdvanceLine>(_currSession,
                                                                new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("CashAdvance", _locCashAdvanceXPO),
                                                                new BinaryOperator("Select", true),
                                                                new GroupOperator(GroupOperatorType.Or, 
                                                                new BinaryOperator("Status", Status.Progress),
                                                                new BinaryOperator("Status", Status.Posted))));

                    if (_locCALines != null && _locCALines.Count > 0)
                    {
                        foreach (CashAdvanceLine _locCALine in _locCALines)
                        {
                            _locCALine.ActivationPosting = true;
                            _locCALine.Status = Status.Close;
                            _locCALine.StatusDate = now;
                            _locCALine.Save();
                            _locCALine.Session.CommitTransaction();
                            
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CashAdvance " + ex.ToString());
            }
        }

        private void SetStatusCashAdvance(Session _currSession, CashAdvance _locCashAdvanceXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locCountStatus = 0;
                bool _locActivationPosting = false;
            
                if (_locCashAdvanceXPO != null)
                {
                    XPCollection<CashAdvanceLine> _locCALines = new XPCollection<CashAdvanceLine>
                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                 new BinaryOperator("CashAdvance", _locCashAdvanceXPO)));

                    if (_locCALines != null && _locCALines.Count() > 0)
                    {
                        foreach (CashAdvanceLine _locCALine in _locCALines)
                        {
                            if (_locCALine.Status != Status.Close)
                            {
                                _locCountStatus = _locCountStatus + 1;
                            }
                        }
                    }

                    if (_locCountStatus > 0)
                    {
                        if (_locCALines.Count() == _locCountStatus)
                        {
                            _locActivationPosting = true;
                        }
                    }

                    _locCashAdvanceXPO.Status = Status.Posted;
                    _locCashAdvanceXPO.ActivationPosting = _locActivationPosting;
                    _locCashAdvanceXPO.StatusDate = now;
                    _locCashAdvanceXPO.Save();
                    _locCashAdvanceXPO.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CashAdvance " + ex.ToString());
            }
        }

        #endregion Posting

        #region Approval Process

        private void SetApprovalLine(Session _currentSession, CashAdvance _locCashAdvanceXPO, ApprovalLevel _locApprovalLevel)
        {
            try
            {
                DateTime now = DateTime.Now;

                ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("CashAdvance", _locCashAdvanceXPO),
                                                                         new BinaryOperator("ApprovalLevel", _locApprovalLevel)));

                if (_locApprovalLineXPO == null)
                {
                    ApprovalLine _saveDataAL2 = new ApprovalLine(_currentSession)
                    {
                        ApprovalDate = now,
                        ApprovalStatus = Status.Approved,
                        ApprovalLevel = _locApprovalLevel,
                        CashAdvance = _locCashAdvanceXPO,
                    };
                    _saveDataAL2.Save();
                    _saveDataAL2.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CashAdvance " + ex.ToString());
            }
        }

        #endregion Approval Process

        #region Email

        private string BackgroundBody(Session _currentSession, CashAdvance _locCashAdvanceXPO, MailSetupDetail _locMailSetDetail)
        {
            string body = string.Empty;

            if(_locCashAdvanceXPO != null)
            {
                ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                               (new GroupOperator(GroupOperatorType.And,
                                                new BinaryOperator("CashAdvance", _locCashAdvanceXPO)));

                string Status = _locApprovalLineXPO.ApprovalStatus.ToString();

                using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Email/EmailTemplate.html")))
                {
                    body = reader.ReadToEnd();
                }
                body = body.Replace("{Code}", _locCashAdvanceXPO.Code);

                #region Level
                if (_locCashAdvanceXPO.ActiveApproved1 == true)
                {
                    string Active = "Level 1";
                    body = body.Replace("{Level}", Active);
                }
                else if (_locCashAdvanceXPO.ActiveApproved2 == true)
                {
                    string Active = "Level 2";
                    body = body.Replace("{Level}", Active);
                }
                else if (_locCashAdvanceXPO.ActiveApproved3 == true)
                {
                    string Active = "Level 3";
                    body = body.Replace("{Level}", Active);
                }
                else
                {
                    string Active = "Not Available";
                    body = body.Replace("{Level}", Active);
                }
                #endregion Level

                body = body.Replace("{Status}", Status);
                body = body.Replace("{Message}", _locMailSetDetail.MessageBody);
            }
            return body;
        }

        protected void SendEmail(Session _currentSession, CashAdvance _locCashAdvanceXPO, Status _locStatus, ApprovalLine _locApprovalLineXPO2)
        {
            DateTime now = DateTime.Now;
            var User = SecuritySystem.CurrentUserName;
            GlobalFunction _globFunc = new GlobalFunction();
            UserAccess _numLineUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", User));
            string _locMessageBody = null;

            try
            {
                if (_locCashAdvanceXPO != null)
                {
                    XPCollection<MailSetupDetail> _locMailSetDetails = new XPCollection<MailSetupDetail>
                                                                       (_currentSession, new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("ObjectList", ObjectList.CashAdvance),
                                                                        new BinaryOperator("UserAccess", _numLineUserAccess),
                                                                        new BinaryOperator("Active", true)));

                    if (_locMailSetDetails != null && _locMailSetDetails.Count > 0)
                    {
                        foreach (MailSetupDetail _locMailSetDetail in _locMailSetDetails)
                        {
                            if (_locMailSetDetail.MessageBody != null)
                            {
                                if (_locApprovalLineXPO2.EndApproval == true)
                                {
                                    _locMessageBody = this.BackgroundBody(_currentSession, _locCashAdvanceXPO, _locMailSetDetail);
                                }
                                else
                                {
                                    _locMessageBody = this.BackgroundBody(_currentSession, _locCashAdvanceXPO, _locMailSetDetail);
                                }
                            }

                            _globFunc.SetAndSendMail(_locMailSetDetail.SmtpHost, _locMailSetDetail.SmtpPort,
                            _locMailSetDetail.MailFrom, _locMailSetDetail.MailFromPassword, _locMailSetDetail.MailTo, _locMailSetDetail.MailSubject, _locMessageBody);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CashAdvance " + ex.ToString());
            }
        }

        #endregion Email

        #region GlobalMethod

        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0} !", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion GlobalMethod

        
    }
}
