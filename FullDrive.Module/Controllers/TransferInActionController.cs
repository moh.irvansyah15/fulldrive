﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using System.Collections;
using DevExpress.Xpo;
using DevExpress.ExpressApp.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class TransferInActionController : ViewController
    {
        private ChoiceActionItem _selectionListviewFilter;

        public TransferInActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            TransferInListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {

                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                TransferInListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
        }

        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }

        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }

        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void TransferInProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                Status _locStatus = Status.None;
                DateTime _locNow;
                Status _locStatus2 = Status.None;
                DateTime _locNow2;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        TransferIn _locTransferInOS = (TransferIn)_objectSpace.GetObject(obj);

                        if (_locTransferInOS != null)
                        {
                            if (_locTransferInOS.Code != null)
                            {
                                _currObjectId = _locTransferInOS.Code;

                                TransferIn _locTransferInXPO = _currSession.FindObject<TransferIn>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locTransferInXPO != null)
                                {
                                    if (_locTransferInXPO.Status == Status.Open || _locTransferInXPO.Status == Status.Progress || _locTransferInXPO.Status == Status.Posted)
                                    {
                                        if (_locTransferInXPO.Status == Status.Open)
                                        {
                                            _locStatus = Status.Progress;
                                            _locNow = now;
                                        }
                                        else
                                        {
                                            _locStatus = _locTransferInXPO.Status;
                                            _locNow = _locTransferInXPO.StatusDate;
                                        }
                                        _locTransferInXPO.Status = _locStatus;
                                        _locTransferInXPO.StatusDate = _locNow;
                                        _locTransferInXPO.Save();
                                        _locTransferInXPO.Session.CommitTransaction();

                                        #region TransferInLine
                                        XPCollection<TransferInLine> _locTransferInLines = new XPCollection<TransferInLine>
                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("TransferIn", _locTransferInXPO)));

                                        if (_locTransferInLines != null && _locTransferInLines.Count > 0)
                                        {
                                            foreach (TransferInLine _locTransferInLine in _locTransferInLines)
                                            {
                                                if (_locTransferInLine.Status == Status.Open || _locTransferInLine.Status == Status.Progress || _locTransferInLine.Status == Status.Posted)
                                                {
                                                    if (_locTransferInLine.Status == Status.Open)
                                                    {
                                                        _locStatus2 = Status.Progress;
                                                        _locNow2 = now;
                                                    }
                                                    else
                                                    {
                                                        _locStatus2 = _locTransferInLine.Status;
                                                        _locNow2 = _locTransferInLine.StatusDate;
                                                    }

                                                    _locTransferInLine.Status = _locStatus2;
                                                    _locTransferInLine.StatusDate = _locNow2;
                                                    _locTransferInLine.Save();
                                                    _locTransferInLine.Session.CommitTransaction();
                                                }

                                                XPCollection<TransferInLot> _locTransferInLots = new XPCollection<TransferInLot>
                                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                        new BinaryOperator("TransferInLine", _locTransferInLine),
                                                                                                        new BinaryOperator("Status", Status.Open)));

                                                if (_locTransferInLots != null && _locTransferInLots.Count() > 0)
                                                {
                                                    foreach (TransferInLot _locTransferInLot in _locTransferInLots)
                                                    {
                                                        _locTransferInLot.Status = Status.Progress;
                                                        _locTransferInLot.StatusDate = now;
                                                        _locTransferInLot.Save();
                                                        _locTransferInLot.Session.CommitTransaction();
                                                    }
                                                }
                                            }
                                        }
                                        #endregion TransferInLine

                                        #region TransferOrderInCollection
                                        XPCollection<TransferOrderInCollection> _locTransferOrderInCollections = new XPCollection<TransferOrderInCollection>
                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("TransferIn", _locTransferInXPO),
                                                                new BinaryOperator("Status", Status.Open)));

                                        if (_locTransferOrderInCollections != null && _locTransferOrderInCollections.Count > 0)
                                        {
                                            foreach (TransferOrderInCollection _locTransferOrderInCollection in _locTransferOrderInCollections)
                                            {
                                                _locTransferOrderInCollection.Status = Status.Progress;
                                                _locTransferOrderInCollection.StatusDate = now;
                                                _locTransferOrderInCollection.Save();
                                                _locTransferOrderInCollection.Session.CommitTransaction();
                                            }
                                        }
                                        #endregion TransferOrderInCollection

                                        SuccessMessageShow(_locTransferInXPO.Code + " has been change successfully to Progress");
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data Transfer In Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Transfer In Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferIn " + ex.ToString());
            }
        }

        private void TransferInPostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;


                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        TransferIn _locTransferInOS = (TransferIn)_objectSpace.GetObject(obj);

                        if (_locTransferInOS != null)
                        {
                            if (_locTransferInOS.Status == Status.Progress || _locTransferInOS.Status == Status.Posted)
                            {
                                if (_locTransferInOS.Code != null)
                                {
                                    _currObjectId = _locTransferInOS.Code;

                                    TransferIn _locTransferInXPO = _currSession.FindObject<TransferIn>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("Code", _currObjectId)));

                                    if (_locTransferInXPO != null)
                                    {
                                        SetTransferInMonitoring(_currSession, _locTransferInXPO);
                                        SetReceiveBeginingInventory(_currSession, _locTransferInXPO);
                                        SetReceiveInventoryJournal(_currSession, _locTransferInXPO);
                                        SetRemainReceiveQty(_currSession, _locTransferInXPO);
                                        SetPostingReceiveQty(_currSession, _locTransferInXPO);
                                        SetProcessReceiveCount(_currSession, _locTransferInXPO);

                                        SetStatusReceiveTransferInLine(_currSession, _locTransferInXPO);
                                        SetNormalReceiveQuantity(_currSession, _locTransferInXPO);
                                        SetFinalStatusReceiveTransferIn(_currSession, _locTransferInXPO);
                                        SuccessMessageShow(_locTransferInXPO.Code + " has been change successfully to Receive");
                                    }
                                    else
                                    {
                                        ErrorMessageShow("Inventory Transfer Data Not Available");
                                    }
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Inventory Transfer Data Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferIn " + ex.ToString());
            }
        }

        private void TransferInGetTOMAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        TransferIn _locTransferInOS = (TransferIn)_objectSpace.GetObject(obj);

                        if (_locTransferInOS != null)
                        {
                            if (_locTransferInOS.Code != null)
                            {
                                _currObjectId = _locTransferInOS.Code;

                                TransferIn _locTransferInXPO = _currSession.FindObject<TransferIn>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locTransferInXPO != null)
                                {
                                    if (_locTransferInXPO.Status == Status.Open || _locTransferInXPO.Status == Status.Progress)
                                    {
                                        XPCollection<TransferOrderInCollection> _locTransferOrderInCollections = new XPCollection<TransferOrderInCollection>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("TransferIn", _locTransferInXPO),
                                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("Status", Status.Open),
                                                                                        new BinaryOperator("Status", Status.Progress))));

                                        if (_locTransferOrderInCollections != null && _locTransferOrderInCollections.Count() > 0)
                                        {
                                            foreach (TransferOrderInCollection _locTransferOrderInCollection in _locTransferOrderInCollections)
                                            {
                                                if (_locTransferOrderInCollection.TransferIn != null)
                                                {
                                                    ApprovalLine _locApprovalLineXPO = _currSession.FindObject<ApprovalLine>
                                                                       (new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("EndApproval", true),
                                                                        new BinaryOperator("TransferOrder", _locTransferOrderInCollection.TransferOrder)));
                                                    if (_locApprovalLineXPO != null)
                                                    {
                                                        GetTransferOrderMonitoring(_currSession, _locTransferOrderInCollection.TransferOrder, _locTransferInXPO);
                                                    }
                                                }
                                            }
                                            SuccessMessageShow("Transfer Order Monitoring Has Been Successfully Getting into Transfer In");
                                        }
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data Transfer In Not Available");
                                }

                            }
                            else
                            {
                                ErrorMessageShow("Data Transfer In Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferIn " + ex.ToString());
            }
        }

        private void TransferInListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(TransferIn)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferIn " + ex.ToString());
            }
        }

        #region Get TOM

        private void GetTransferOrderMonitoring(Session _currSession, TransferOrder _locTransferOrderXPO, TransferIn _locTransferInXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _currSignCode = null;

                if (_locTransferOrderXPO != null && _locTransferInXPO != null)
                {
                    //Hanya memindahkan TransferOrderMonitoring ke TransferInLine
                    XPCollection<TransferOrderMonitoring> _locTransferOrderMonitorings = new XPCollection<TransferOrderMonitoring>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("TransferOrder", _locTransferOrderXPO),
                                                                                        new BinaryOperator("TransferIn", _locTransferInXPO),
                                                                                        new BinaryOperator("Select", true),
                                                                                        new BinaryOperator("InventoryMovingType", InventoryMovingType.TransferIn),
                                                                                        new BinaryOperator("Status", Status.Close, BinaryOperatorType.NotEqual)));

                    if (_locTransferOrderMonitorings != null && _locTransferOrderMonitorings.Count() > 0)
                    {
                        foreach (TransferOrderMonitoring _locTransferOrderMonitoring in _locTransferOrderMonitorings)
                        {
                            if (_locTransferOrderMonitoring.TransferOrderLine != null)
                            {
                                _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.TransferInLine);

                                if (_currSignCode != null)
                                {
                                    #region SaveTransferInLine

                                    TransferInLine _saveDataTransferInLine = new TransferInLine(_currSession)
                                    {
                                        SignCode = _currSignCode,
                                        Item = _locTransferOrderMonitoring.Item,
                                        MxDQty = _locTransferOrderMonitoring.DQty,
                                        MxDUOM = _locTransferOrderMonitoring.DUOM,
                                        MxQty = _locTransferOrderMonitoring.Qty,
                                        MxUOM = _locTransferOrderMonitoring.UOM,
                                        MxTQty = _locTransferOrderMonitoring.TQty,
                                        DQty = _locTransferOrderMonitoring.DQty,
                                        DUOM = _locTransferOrderMonitoring.DUOM,
                                        Qty = _locTransferOrderMonitoring.Qty,
                                        UOM = _locTransferOrderMonitoring.UOM,
                                        TQty = _locTransferOrderMonitoring.TQty,
                                        LocationFrom = _locTransferOrderMonitoring.LocationFrom,
                                        LocationTo = _locTransferOrderMonitoring.LocationTo,
                                        TransferOrderMonitoring = _locTransferOrderMonitoring,
                                        TransferIn = _locTransferInXPO,
                                    };
                                    _saveDataTransferInLine.Save();
                                    _saveDataTransferInLine.Session.CommitTransaction();

                                    #endregion SaveTransferInLine

                                    TransferInLine _locTransferInLine = _currSession.FindObject<TransferInLine>
                                                                (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("SignCode", _currSignCode)));
                                    if (_locTransferInLine != null)
                                    {
                                        SetRemainQty(_currSession, _locTransferOrderMonitoring);
                                        SetPostingQty(_currSession, _locTransferOrderMonitoring);
                                        SetProcessCount(_currSession, _locTransferOrderMonitoring);
                                        SetStatusTransferOrderMonitoring(_currSession, _locTransferOrderMonitoring);
                                        SetNormalQuantity(_currSession, _locTransferOrderMonitoring);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = TransferIn ", ex.ToString());
            }
        }

        private void SetRemainQty(Session _currSession, TransferOrderMonitoring _locTransferOrderMonitoringXPO)
        {
            try
            {
                if (_locTransferOrderMonitoringXPO != null)
                {
                    double _locRmDQty = 0;
                    double _locRmQty = 0;
                    double _locInvLineTotal = 0;
                    ItemUnitOfMeasure _locItemUOM = null;

                    #region ProcessCount=0
                    if (_locTransferOrderMonitoringXPO.PostedCount == 0)
                    {
                        #region MaxQuantity
                        if (_locTransferOrderMonitoringXPO.MxDQty > 0)
                        {
                            if (_locTransferOrderMonitoringXPO.DQty > 0 && _locTransferOrderMonitoringXPO.DQty <= _locTransferOrderMonitoringXPO.MxDQty)
                            {
                                _locRmDQty = _locTransferOrderMonitoringXPO.MxDQty - _locTransferOrderMonitoringXPO.DQty;
                            }

                            if (_locTransferOrderMonitoringXPO.Qty > 0 && _locTransferOrderMonitoringXPO.Qty <= _locTransferOrderMonitoringXPO.MxQty)
                            {
                                _locRmQty = _locTransferOrderMonitoringXPO.MxQty - _locTransferOrderMonitoringXPO.Qty;
                            }


                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                       (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Item", _locTransferOrderMonitoringXPO.Item),
                                                        new BinaryOperator("UOM", _locTransferOrderMonitoringXPO.MxUOM),
                                                        new BinaryOperator("DefaultUOM", _locTransferOrderMonitoringXPO.MxDUOM),
                                                        new BinaryOperator("Active", true)));
                            if (_locItemUOM != null)
                            {
                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                }
                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                }
                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty + _locRmDQty;
                                }
                            }
                            else
                            {
                                _locInvLineTotal = _locRmQty + _locRmDQty;
                            }

                        }
                        #endregion MaxQuantity
                        #region NonMaxQuantity
                        else
                        {
                            if (_locTransferOrderMonitoringXPO.DQty > 0)
                            {
                                _locRmDQty = 0;
                            }

                            if (_locTransferOrderMonitoringXPO.Qty > 0)
                            {
                                _locRmQty = 0;
                            }


                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                       (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Item", _locTransferOrderMonitoringXPO.Item),
                                                        new BinaryOperator("UOM", _locTransferOrderMonitoringXPO.UOM),
                                                        new BinaryOperator("DefaultUOM", _locTransferOrderMonitoringXPO.DUOM),
                                                        new BinaryOperator("Active", true)));
                            if (_locItemUOM != null)
                            {
                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                }
                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                }
                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty + _locRmDQty;
                                }
                            }
                            else
                            {
                                _locInvLineTotal = _locRmQty + _locRmDQty;
                            }
                        }
                        #endregion NonMaxQuantity
                    }
                    #endregion ProcessCount=0

                    #region ProcessCount>0
                    if (_locTransferOrderMonitoringXPO.PostedCount > 0)
                    {
                        if (_locTransferOrderMonitoringXPO.RmDQty > 0)
                        {
                            _locRmDQty = _locTransferOrderMonitoringXPO.RmDQty - _locTransferOrderMonitoringXPO.DQty;
                        }

                        if (_locTransferOrderMonitoringXPO.RmQty > 0)
                        {
                            _locRmQty = _locTransferOrderMonitoringXPO.RmQty - _locTransferOrderMonitoringXPO.Qty;
                        }

                        if (_locTransferOrderMonitoringXPO.MxDQty > 0 || _locTransferOrderMonitoringXPO.MxQty > 0)
                        {
                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                       (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Item", _locTransferOrderMonitoringXPO.Item),
                                                        new BinaryOperator("UOM", _locTransferOrderMonitoringXPO.MxUOM),
                                                        new BinaryOperator("DefaultUOM", _locTransferOrderMonitoringXPO.MxDUOM),
                                                        new BinaryOperator("Active", true)));
                        }
                        else
                        {
                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                       (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Item", _locTransferOrderMonitoringXPO.Item),
                                                        new BinaryOperator("UOM", _locTransferOrderMonitoringXPO.UOM),
                                                        new BinaryOperator("DefaultUOM", _locTransferOrderMonitoringXPO.DUOM),
                                                        new BinaryOperator("Active", true)));
                        }


                        if (_locItemUOM != null)
                        {
                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                            }
                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                            }
                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = _locRmQty + _locRmDQty;
                            }
                        }
                        else
                        {
                            _locInvLineTotal = _locRmQty + _locRmDQty;
                        }

                    }
                    #endregion ProcessCount>0

                    _locTransferOrderMonitoringXPO.RmDQty = _locRmDQty;
                    _locTransferOrderMonitoringXPO.RmQty = _locRmQty;
                    _locTransferOrderMonitoringXPO.RmTQty = _locInvLineTotal;
                    _locTransferOrderMonitoringXPO.Save();
                    _locTransferOrderMonitoringXPO.Session.CommitTransaction();

                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferIn " + ex.ToString());
            }
        }

        private void SetPostingQty(Session _currSession, TransferOrderMonitoring _locTransferOrderMonitoringXPO)
        {
            try
            {
                if (_locTransferOrderMonitoringXPO != null)
                {
                    double _locPDQty = 0;
                    double _locPQty = 0;
                    double _locInvLineTotal = 0;
                    ItemUnitOfMeasure _locItemUOM = null;

                    #region ProcessCount=0
                    if (_locTransferOrderMonitoringXPO.PostedCount == 0)
                    {
                        #region MaxQuantity
                        if (_locTransferOrderMonitoringXPO.MxDQty > 0)
                        {
                            if (_locTransferOrderMonitoringXPO.DQty > 0 && _locTransferOrderMonitoringXPO.DQty <= _locTransferOrderMonitoringXPO.MxDQty)
                            {
                                _locPDQty = _locTransferOrderMonitoringXPO.DQty;
                            }

                            if (_locTransferOrderMonitoringXPO.Qty > 0 && _locTransferOrderMonitoringXPO.Qty <= _locTransferOrderMonitoringXPO.MxQty)
                            {
                                _locPQty = _locTransferOrderMonitoringXPO.Qty;
                            }


                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                       (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Item", _locTransferOrderMonitoringXPO.Item),
                                                        new BinaryOperator("UOM", _locTransferOrderMonitoringXPO.MxUOM),
                                                        new BinaryOperator("DefaultUOM", _locTransferOrderMonitoringXPO.MxDUOM),
                                                        new BinaryOperator("Active", true)));
                            if (_locItemUOM != null)
                            {
                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                }
                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                }
                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locPQty + _locPDQty;
                                }
                            }
                            else
                            {
                                _locInvLineTotal = _locPQty + _locPDQty;
                            }

                        }
                        #endregion MaxQuantity
                        #region NonMaxQuantity
                        else
                        {
                            if (_locTransferOrderMonitoringXPO.DQty > 0)
                            {
                                _locPDQty = _locTransferOrderMonitoringXPO.DQty;
                            }

                            if (_locTransferOrderMonitoringXPO.Qty > 0)
                            {
                                _locPQty = _locTransferOrderMonitoringXPO.Qty;
                            }


                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                       (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Item", _locTransferOrderMonitoringXPO.Item),
                                                        new BinaryOperator("UOM", _locTransferOrderMonitoringXPO.UOM),
                                                        new BinaryOperator("DefaultUOM", _locTransferOrderMonitoringXPO.DUOM),
                                                        new BinaryOperator("Active", true)));
                            if (_locItemUOM != null)
                            {
                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                }
                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                }
                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locPQty + _locPDQty;
                                }
                            }
                            else
                            {
                                _locInvLineTotal = _locPQty + _locPDQty;
                            }
                        }
                        #endregion NonMaxQuantity
                    }
                    #endregion ProcessCount=0

                    #region ProcessCount>0
                    if (_locTransferOrderMonitoringXPO.PostedCount > 0)
                    {
                        if (_locTransferOrderMonitoringXPO.PDQty > 0)
                        {
                            _locPDQty = _locTransferOrderMonitoringXPO.PDQty + _locTransferOrderMonitoringXPO.DQty;
                        }

                        if (_locTransferOrderMonitoringXPO.PQty > 0)
                        {
                            _locPQty = _locTransferOrderMonitoringXPO.PQty + _locTransferOrderMonitoringXPO.Qty;
                        }

                        if (_locTransferOrderMonitoringXPO.MxDQty > 0 || _locTransferOrderMonitoringXPO.MxQty > 0)
                        {
                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                   (new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("Item", _locTransferOrderMonitoringXPO.Item),
                                                    new BinaryOperator("UOM", _locTransferOrderMonitoringXPO.MxUOM),
                                                    new BinaryOperator("DefaultUOM", _locTransferOrderMonitoringXPO.MxDUOM),
                                                    new BinaryOperator("Active", true)));
                        }
                        else
                        {
                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                   (new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("Item", _locTransferOrderMonitoringXPO.Item),
                                                    new BinaryOperator("UOM", _locTransferOrderMonitoringXPO.UOM),
                                                    new BinaryOperator("DefaultUOM", _locTransferOrderMonitoringXPO.DUOM),
                                                    new BinaryOperator("Active", true)));
                        }

                        if (_locItemUOM != null)
                        {
                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                            }
                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                            }
                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = _locPQty + _locPDQty;
                            }
                        }
                        else
                        {
                            _locInvLineTotal = _locPQty + _locPDQty;
                        }

                    }
                    #endregion ProcessCount>0

                    _locTransferOrderMonitoringXPO.PDQty = _locPDQty;
                    _locTransferOrderMonitoringXPO.PDUOM = _locTransferOrderMonitoringXPO.DUOM;
                    _locTransferOrderMonitoringXPO.PQty = _locPQty;
                    _locTransferOrderMonitoringXPO.PUOM = _locTransferOrderMonitoringXPO.UOM;
                    _locTransferOrderMonitoringXPO.PTQty = _locInvLineTotal;
                    _locTransferOrderMonitoringXPO.Save();
                    _locTransferOrderMonitoringXPO.Session.CommitTransaction();

                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferIn " + ex.ToString());
            }
        }

        private void SetProcessCount(Session _currSession, TransferOrderMonitoring _locTransferOrderMonitoringXPO)
        {
            try
            {
                if (_locTransferOrderMonitoringXPO != null)
                {
                    if (_locTransferOrderMonitoringXPO.Status == Status.Progress || _locTransferOrderMonitoringXPO.Status == Status.Posted)
                    {
                        _locTransferOrderMonitoringXPO.PostedCount = _locTransferOrderMonitoringXPO.PostedCount + 1;
                        _locTransferOrderMonitoringXPO.Save();
                        _locTransferOrderMonitoringXPO.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferIn " + ex.ToString());
            }
        }

        private void SetStatusTransferOrderMonitoring(Session _currSession, TransferOrderMonitoring _locTransferOrderMonitoringXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locTransferOrderMonitoringXPO != null)
                {
                    if (_locTransferOrderMonitoringXPO.Status == Status.Progress || _locTransferOrderMonitoringXPO.Status == Status.Posted)
                    {
                        if (_locTransferOrderMonitoringXPO.RmDQty == 0 && _locTransferOrderMonitoringXPO.RmQty == 0 && _locTransferOrderMonitoringXPO.RmTQty == 0)
                        {
                            _locTransferOrderMonitoringXPO.Status = Status.Close;
                            _locTransferOrderMonitoringXPO.ActivationPosting = true;
                            _locTransferOrderMonitoringXPO.StatusDate = now;
                        }
                        else
                        {
                            _locTransferOrderMonitoringXPO.Status = Status.Posted;
                            _locTransferOrderMonitoringXPO.StatusDate = now;
                        }
                        _locTransferOrderMonitoringXPO.Save();
                        _locTransferOrderMonitoringXPO.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferIn " + ex.ToString());
            }
        }

        private void SetNormalQuantity(Session _currSession, TransferOrderMonitoring _locTransferOrderMonitoringXPO)
        {
            try
            {
                if (_locTransferOrderMonitoringXPO != null)
                {
                    if (_locTransferOrderMonitoringXPO.Status == Status.Progress || _locTransferOrderMonitoringXPO.Status == Status.Posted || _locTransferOrderMonitoringXPO.Status == Status.Close)
                    {
                        if (_locTransferOrderMonitoringXPO.DQty > 0 || _locTransferOrderMonitoringXPO.Qty > 0)
                        {
                            _locTransferOrderMonitoringXPO.Select = false;
                            _locTransferOrderMonitoringXPO.DQty = 0;
                            _locTransferOrderMonitoringXPO.Qty = 0;
                            _locTransferOrderMonitoringXPO.TransferOut = null;
                            _locTransferOrderMonitoringXPO.Save();
                            _locTransferOrderMonitoringXPO.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferIn " + ex.ToString());
            }
        }

        #endregion Get TOM

        #region Posting

        private void SetTransferInMonitoring(Session _currSession, TransferIn _locTransferInXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;

                if (_locTransferInXPO != null)
                {
                    if (_locTransferInXPO.Status == Status.Progress || _locTransferInXPO.Status == Status.Posted)
                    {
                        XPCollection<TransferInLine> _locTransferInLines = new XPCollection<TransferInLine>
                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("TransferIn", _locTransferInXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Progress),
                                                            new BinaryOperator("Status", Status.Posted))));

                        if (_locTransferInLines != null && _locTransferInLines.Count() > 0)
                        {
                            foreach (TransferInLine _locTransferInLine in _locTransferInLines)
                            {
                                TransferInMonitoring _saveDataTransferInMonitoring = new TransferInMonitoring(_currSession)
                                {
                                    TransferIn = _locTransferInXPO,
                                    TransferInLine = _locTransferInLine,
                                    Item = _locTransferInLine.Item,
                                    DQty = _locTransferInLine.DQty,
                                    DUOM = _locTransferInLine.DUOM,
                                    Qty = _locTransferInLine.Qty,
                                    UOM = _locTransferInLine.UOM,
                                    TQty = _locTransferInLine.TQty,
                                    TransferOrderMonitoring = _locTransferInLine.TransferOrderMonitoring,
                                };
                                _saveDataTransferInMonitoring.Save();
                                _saveDataTransferInMonitoring.Session.CommitTransaction();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = TransferIn ", ex.ToString());
            }
        }

        private void SetReceiveBeginingInventory(Session _currSession, TransferIn _locTransferInXPO)
        {
            try
            {
                XPCollection<TransferInLine> _locTransferInLines = new XPCollection<TransferInLine>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("TransferIn", _locTransferInXPO),
                                                                        new BinaryOperator("Select", true)));

                if (_locTransferInLines != null && _locTransferInLines.Count() > 0)
                {
                    GlobalFunction _globFunc = new GlobalFunction();
                    double _locInvLineTotal = 0;
                    XPCollection<BeginingInventoryLine> _locBegInventoryLines = null;
                    ItemUnitOfMeasure _locItemUOM = null;
                    string _fullString = null;
                    string _locItemParse = null;
                    string _locLocationParse = null;
                    string _locBinLocationParse = null;
                    string _locDUOMParse = null;
                    string _locStockTypeParse = null;
                    string _locProjectHeader = null;
                    string _locActiveParse = null;

                    foreach (TransferInLine _locTransferInLine in _locTransferInLines)
                    {
                        if (_locTransferInLine.Status == Status.Progress || _locTransferInLine.Status == Status.Posted)
                        {
                            if (_locTransferInLine.DQty > 0 || _locTransferInLine.Qty > 0)
                            {
                                XPCollection<TransferInLot> _locTransferInLots = new XPCollection<TransferInLot>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("TransferInLine", _locTransferInLine)));
                                #region LotNumber
                                if (_locTransferInLots != null && _locTransferInLots.Count() > 0)
                                {
                                    foreach (TransferInLot _locTransferInLot in _locTransferInLots)
                                    {
                                        if (_locTransferInLot.Select == true)
                                        {
                                            if (_locTransferInLot.BegInvLine != null)
                                            {
                                                if (_locTransferInLot.BegInvLine.Code != null)
                                                {
                                                    BeginingInventoryLine _locBegInvLine = _currSession.FindObject<BeginingInventoryLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("Code", _locTransferInLot.BegInvLine.Code)));
                                                    if (_locBegInvLine != null)
                                                    {
                                                        if (_locBegInvLine.QtyAvailable >= _locTransferInLot.TQty)
                                                        {
                                                            _locBegInvLine.QtyAvailable = _locBegInvLine.QtyAvailable + _locTransferInLot.TQty;
                                                            _locBegInvLine.Save();
                                                            _locBegInvLine.Session.CommitTransaction();
                                                        }

                                                        if (_locBegInvLine.BeginingInventory != null)
                                                        {
                                                            if (_locBegInvLine.BeginingInventory.Code != null)
                                                            {
                                                                BeginingInventory _locBegInv = _currSession.FindObject<BeginingInventory>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("Code", _locBegInvLine.BeginingInventory.Code)));
                                                                if (_locBegInv != null)
                                                                {
                                                                    _locBegInv.QtyAvailable = _locBegInv.QtyAvailable + _locTransferInLot.TQty;
                                                                    _locBegInv.Save();
                                                                    _locBegInv.Session.CommitTransaction();
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                #endregion LotNumber

                                #region NonLotNumber
                                else
                                {
                                    if (_locTransferInLine.Item != null && _locTransferInLine.UOM != null && _locTransferInLine.DUOM != null)
                                    {
                                        //for conversion
                                        _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                             new BinaryOperator("Item", _locTransferInLine.Item),
                                                             new BinaryOperator("UOM", _locTransferInLine.UOM),
                                                             new BinaryOperator("DefaultUOM", _locTransferInLine.DUOM),
                                                             new BinaryOperator("Active", true)));
                                    }

                                    //for query
                                    BeginingInventory _locBeginingInventory = _currSession.FindObject<BeginingInventory>(new GroupOperator(GroupOperatorType.And,
                                                                              new BinaryOperator("Item", _locTransferInLine.Item)));

                                    #region UpdateBeginingInventory
                                    if (_locBeginingInventory != null)
                                    {
                                        #region code
                                        if (_locTransferInLine.Item != null) { _locItemParse = "[Item.Code]=='" + _locTransferInLine.Item.Code + "'"; }
                                        else { _locItemParse = ""; }

                                        if (_locTransferInLine.LocationFrom != null && (_locTransferInLine.Item != null))
                                        { _locLocationParse = " AND [Location.Code]=='" + _locTransferInLine.LocationFrom.Code + "'"; }
                                        else if (_locTransferInLine.LocationFrom != null && _locTransferInLine.Item == null)
                                        { _locLocationParse = " [Location.Code]=='" + _locTransferInLine.LocationFrom.Code + "'"; }
                                        else { _locLocationParse = ""; }

                                        if (_locTransferInLine.BinLocationFrom != null && (_locTransferInLine.Item != null || _locTransferInLine.LocationFrom != null))
                                        { _locBinLocationParse = " AND [BinLocation.Code]=='" + _locTransferInLine.BinLocationFrom.Code + "'"; }
                                        else if (_locTransferInLine.BinLocationFrom != null && _locTransferInLine.Item == null && _locTransferInLine.LocationFrom == null)
                                        { _locBinLocationParse = " [BinLocation.Code]=='" + _locTransferInLine.BinLocationFrom.Code + "'"; }
                                        else { _locBinLocationParse = ""; }

                                        if (_locTransferInLine.DUOM != null && (_locTransferInLine.Item != null || _locTransferInLine.LocationFrom != null || _locTransferInLine.BinLocationFrom != null))
                                        { _locDUOMParse = " AND [DefaultUOM.Code]=='" + _locTransferInLine.DUOM.Code + "'"; }
                                        else if (_locTransferInLine.DUOM != null && _locTransferInLine.Item == null && _locTransferInLine.LocationFrom == null && _locTransferInLine.BinLocationFrom == null)
                                        { _locDUOMParse = " [DefaultUOM.Code]=='" + _locTransferInLine.DUOM.Code + "'"; }
                                        else { _locDUOMParse = ""; }

                                        if (_locTransferInLine.StockTypeFrom != StockType.None && (_locTransferInLine.Item != null || _locTransferInLine.LocationFrom != null || _locTransferInLine.BinLocationFrom != null
                                            || _locTransferInLine.DUOM != null))
                                        { _locStockTypeParse = " AND [StockType]=='" + GetStockType(_locTransferInLine.StockTypeFrom).ToString() + "'"; }
                                        else if (_locTransferInLine.StockTypeFrom != StockType.None && _locTransferInLine.Item == null && _locTransferInLine.LocationFrom == null && _locTransferInLine.BinLocationFrom == null
                                            && _locTransferInLine.DUOM == null)
                                        { _locStockTypeParse = " [StockType]=='" + GetStockType(_locTransferInLine.StockTypeFrom).ToString() + "'"; }
                                        else { _locStockTypeParse = ""; }

                                        if (_locTransferInLine.Item == null && _locTransferInLine.LocationFrom == null && _locTransferInLine.BinLocationFrom == null
                                            && _locTransferInLine.DUOM == null && _locTransferInLine.StockTypeFrom != StockType.None)
                                        { _locActiveParse = " [Active]=='" + GetActive(true).ToString() + "'"; }
                                        else { _locActiveParse = " AND [Active]=='" + GetActive(true).ToString() + "'"; }

                                        if (_locTransferInXPO.ProjectHeader != null)
                                        { _locProjectHeader = " AND [ProjectHeader.Code]=='" + _locTransferInXPO.ProjectHeader.Code + "'"; }
                                        else
                                        { _locProjectHeader = ""; }

                                        if (_locItemParse != null || _locLocationParse != null || _locBinLocationParse != null || _locDUOMParse != null || _locStockTypeParse != null || _locProjectHeader != null)
                                        {
                                            _fullString = _locItemParse + _locLocationParse + _locBinLocationParse + _locDUOMParse + _locStockTypeParse + _locActiveParse + _locProjectHeader;
                                        }
                                        else
                                        {
                                            _fullString = _locActiveParse;
                                        }
                                        #endregion code

                                        _locBegInventoryLines = new XPCollection<BeginingInventoryLine>(_currSession, CriteriaOperator.Parse(_fullString));

                                        #region UpdateBeginingInventoryLine
                                        if (_locBegInventoryLines != null && _locBegInventoryLines.Count() > 0)
                                        {

                                            foreach (BeginingInventoryLine _locBegInventoryLine in _locBegInventoryLines)
                                            {
                                                if (_locItemUOM != null)
                                                {
                                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                    {
                                                        _locInvLineTotal = _locTransferInLine.Qty * _locItemUOM.DefaultConversion + _locTransferInLine.DQty;
                                                    }
                                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                    {
                                                        _locInvLineTotal = _locTransferInLine.Qty / _locItemUOM.Conversion + _locTransferInLine.DQty;
                                                    }
                                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                    {
                                                        _locInvLineTotal = _locTransferInLine.Qty + _locTransferInLine.DQty;
                                                    }
                                                }

                                                else
                                                {
                                                    _locInvLineTotal = _locTransferInLine.Qty + _locTransferInLine.DQty;
                                                }

                                                _locBegInventoryLine.QtyAvailable = _locBegInventoryLine.QtyAvailable + _locInvLineTotal;
                                                _locBegInventoryLine.Save();
                                                _locBegInventoryLine.Session.CommitTransaction();

                                                _locBeginingInventory.QtyAvailable = _locBeginingInventory.QtyAvailable + _locInvLineTotal;
                                                _locBeginingInventory.Save();
                                                _locBeginingInventory.Session.CommitTransaction();
                                            }
                                        }
                                        #endregion UpdateBeginingInventoryLine

                                    }
                                    #endregion UpdateBeginingInventory
                                }
                                #endregion NonLotNumber

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = TransferIn ", ex.ToString());
            }
        }

        private void SetReceiveInventoryJournal(Session _currSession, TransferIn _locTransferInXPO)
        {
            try
            {
                XPCollection<TransferInLine> _locTransferInLines = new XPCollection<TransferInLine>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("TransferIn", _locTransferInXPO),
                                                                        new BinaryOperator("Select", true)));

                if (_locTransferInLines != null && _locTransferInLines.Count > 0)
                {
                    double _locInvLineTotal = 0;
                    DateTime now = DateTime.Now;

                    foreach (TransferInLine _locTransferInLine in _locTransferInLines)
                    {
                        if (_locTransferInLine.Status == Status.Progress || _locTransferInLine.Status == Status.Posted)
                        {
                            if (_locTransferInLine.DQty > 0 || _locTransferInLine.Qty > 0)
                            {
                                XPCollection<TransferInLot> _locTransferInLots = new XPCollection<TransferInLot>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("TransferInLine", _locTransferInLine)));

                                #region LotNumber
                                if (_locTransferInLots != null && _locTransferInLots.Count() > 0)
                                {
                                    foreach (TransferInLot _locTransferInLot in _locTransferInLots)
                                    {
                                        if (_locTransferInLot.Select == true)
                                        {
                                            if (_locTransferInLot.BegInvLine != null)
                                            {
                                                if (_locTransferInLot.BegInvLine.Code != null)
                                                {
                                                    BeginingInventoryLine _locBegInvLine = _currSession.FindObject<BeginingInventoryLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("Code", _locTransferInLot.BegInvLine.Code)));
                                                    if (_locBegInvLine != null)
                                                    {
                                                        InventoryJournal _locPositiveInventoryJournal = new InventoryJournal(_currSession)
                                                        {
                                                            DocumentType = _locTransferInXPO.DocumentType,
                                                            DocNo = _locTransferInXPO.DocNo,
                                                            Location = _locTransferInLot.LocationFrom,
                                                            BinLocation = _locTransferInLot.BinLocationFrom,
                                                            StockType = _locTransferInLot.StockTypeFrom,
                                                            Item = _locTransferInLot.Item,
                                                            Company = _locTransferInLot.Company,
                                                            QtyPos = _locTransferInLot.TQty,
                                                            QtyNeg = 0,
                                                            DUOM = _locTransferInLot.DUOM,
                                                            JournalDate = now,
                                                            LotNumber = _locTransferInLot.LotNumber,
                                                            ProjectHeader = _locTransferInXPO.ProjectHeader,
                                                            TransferIn = _locTransferInXPO,
                                                        };
                                                        _locPositiveInventoryJournal.Save();
                                                        _locPositiveInventoryJournal.Session.CommitTransaction();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                #endregion LotNumber

                                #region NonLotNumber
                                else
                                {
                                    if (_locTransferInLine.Item != null && _locTransferInLine.UOM != null && _locTransferInLine.DUOM != null)
                                    {
                                        ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                             new BinaryOperator("Item", _locTransferInLine.Item),
                                                             new BinaryOperator("UOM", _locTransferInLine.UOM),
                                                             new BinaryOperator("DefaultUOM", _locTransferInLine.DUOM),
                                                             new BinaryOperator("Active", true)));
                                        if (_locItemUOM != null)
                                        {
                                            #region Code
                                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                            {
                                                _locInvLineTotal = _locTransferInLine.Qty * _locItemUOM.DefaultConversion + _locTransferInLine.DQty;
                                            }
                                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                            {
                                                _locInvLineTotal = _locTransferInLine.Qty / _locItemUOM.Conversion + _locTransferInLine.DQty;
                                            }
                                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                            {
                                                _locInvLineTotal = _locTransferInLine.Qty + _locTransferInLine.DQty;
                                            }
                                            #endregion Code

                                            InventoryJournal _locPositiveInventoryJournal = new InventoryJournal(_currSession)
                                            {
                                                DocumentType = _locTransferInXPO.DocumentType,
                                                DocNo = _locTransferInXPO.DocNo,
                                                Location = _locTransferInLine.LocationFrom,
                                                BinLocation = _locTransferInLine.BinLocationFrom,
                                                StockType = _locTransferInLine.StockTypeFrom,
                                                Item = _locTransferInLine.Item,
                                                Company = _locTransferInLine.Company,
                                                QtyPos = _locInvLineTotal,
                                                QtyNeg = 0,
                                                DUOM = _locTransferInLine.DUOM,
                                                JournalDate = now,
                                                ProjectHeader = _locTransferInXPO.ProjectHeader,
                                                TransferIn = _locTransferInXPO,
                                            };
                                            _locPositiveInventoryJournal.Save();
                                            _locPositiveInventoryJournal.Session.CommitTransaction();

                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locTransferInLine.Qty + _locTransferInLine.DQty;

                                        InventoryJournal _locPositiveInventoryJournal = new InventoryJournal(_currSession)
                                        {
                                            DocumentType = _locTransferInXPO.DocumentType,
                                            DocNo = _locTransferInXPO.DocNo,
                                            Location = _locTransferInLine.LocationFrom,
                                            BinLocation = _locTransferInLine.BinLocationFrom,
                                            StockType = _locTransferInLine.StockTypeFrom,
                                            Item = _locTransferInLine.Item,
                                            Company = _locTransferInLine.Company,
                                            QtyPos = _locInvLineTotal,
                                            QtyNeg = 0,
                                            DUOM = _locTransferInLine.DUOM,
                                            JournalDate = now,
                                            ProjectHeader = _locTransferInXPO.ProjectHeader,
                                            TransferIn = _locTransferInXPO,
                                        };
                                        _locPositiveInventoryJournal.Save();
                                        _locPositiveInventoryJournal.Session.CommitTransaction();
                                    }
                                }
                                #endregion NonLotNumber
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = TransferIn ", ex.ToString());
            }
        }

        private void SetRemainReceiveQty(Session _currSession, TransferIn _locTransferInXPO)
        {
            try
            {
                if (_locTransferInXPO != null)
                {
                    XPCollection<TransferInLine> _locTransferInLines = new XPCollection<TransferInLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("TransferIn", _locTransferInXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locTransferInLines != null && _locTransferInLines.Count > 0)
                    {
                        double _locRmDQty = 0;
                        double _locRmQty = 0;
                        double _locInvLineTotal = 0;
                        ItemUnitOfMeasure _locItemUOM = null;

                        foreach (TransferInLine _locTransferInLine in _locTransferInLines)
                        {
                            #region ProcessCount=0
                            if (_locTransferInLine.ProcessCount == 0)
                            {
                                #region MaxQuantity
                                if (_locTransferInLine.MxDQty > 0)
                                {
                                    if (_locTransferInLine.DQty > 0 && _locTransferInLine.DQty <= _locTransferInLine.MxDQty)
                                    {
                                        _locRmDQty = _locTransferInLine.MxDQty - _locTransferInLine.DQty;
                                    }

                                    if (_locTransferInLine.Qty > 0 && _locTransferInLine.Qty <= _locTransferInLine.MxQty)
                                    {
                                        _locRmQty = _locTransferInLine.MxQty - _locTransferInLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locTransferInLine.Item),
                                                                new BinaryOperator("UOM", _locTransferInLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locTransferInLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty + _locRmDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }

                                }
                                #endregion MaxQuantity

                                #region NonMaxQuantity
                                else
                                {
                                    if (_locTransferInLine.DQty > 0)
                                    {
                                        _locRmDQty = 0;
                                    }

                                    if (_locTransferInLine.Qty > 0)
                                    {
                                        _locRmQty = 0;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locTransferInLine.Item),
                                                                new BinaryOperator("UOM", _locTransferInLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locTransferInLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty + _locRmDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion ProcessCount=0

                            #region ProcessCount>0
                            if (_locTransferInLine.ProcessCount > 0)
                            {
                                if (_locTransferInLine.RmDQty > 0)
                                {
                                    _locRmDQty = _locTransferInLine.RmDQty - _locTransferInLine.DQty;
                                }

                                if (_locTransferInLine.RmQty > 0)
                                {
                                    _locRmQty = _locTransferInLine.RmQty - _locTransferInLine.Qty;
                                }

                                if (_locTransferInLine.MxDQty > 0 || _locTransferInLine.MxQty > 0)
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locTransferInLine.Item),
                                                                new BinaryOperator("UOM", _locTransferInLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locTransferInLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                }
                                else
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locTransferInLine.Item),
                                                                new BinaryOperator("UOM", _locTransferInLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locTransferInLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                }


                                if (_locItemUOM != null)
                                {
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locRmQty + _locRmDQty;
                                }

                            }
                            #endregion ProcessCount>0

                            _locTransferInLine.RmDQty = _locRmDQty;
                            _locTransferInLine.RmQty = _locRmQty;
                            _locTransferInLine.RmTQty = _locInvLineTotal;
                            _locTransferInLine.Save();
                            _locTransferInLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferIn " + ex.ToString());
            }
        }

        private void SetPostingReceiveQty(Session _currSession, TransferIn _locTransferInXPO)
        {
            try
            {
                if (_locTransferInXPO != null)
                {
                    XPCollection<TransferInLine> _locTransferInLines = new XPCollection<TransferInLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("TransferIn", _locTransferInXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locTransferInLines != null && _locTransferInLines.Count > 0)
                    {
                        double _locPDQty = 0;
                        double _locPQty = 0;
                        double _locInvLineTotal = 0;
                        ItemUnitOfMeasure _locItemUOM = null;

                        foreach (TransferInLine _locTransferInLine in _locTransferInLines)
                        {
                            #region ProcessCount=0
                            if (_locTransferInLine.ProcessCount == 0)
                            {
                                #region MaxQuantity
                                if (_locTransferInLine.MxDQty > 0)
                                {
                                    if (_locTransferInLine.DQty > 0 && _locTransferInLine.DQty <= _locTransferInLine.MxDQty)
                                    {
                                        _locPDQty = _locTransferInLine.DQty;
                                    }

                                    if (_locTransferInLine.Qty > 0 && _locTransferInLine.Qty <= _locTransferInLine.MxQty)
                                    {
                                        _locPQty = _locTransferInLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locTransferInLine.Item),
                                                                new BinaryOperator("UOM", _locTransferInLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locTransferInLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty + _locPDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }

                                }
                                #endregion MaxQuantity

                                #region NonMaxQuantity
                                else
                                {
                                    if (_locTransferInLine.DQty > 0)
                                    {
                                        _locPDQty = _locTransferInLine.DQty;
                                    }

                                    if (_locTransferInLine.Qty > 0)
                                    {
                                        _locPQty = _locTransferInLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locTransferInLine.Item),
                                                                new BinaryOperator("UOM", _locTransferInLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locTransferInLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty + _locPDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion ProcessCount=0

                            #region ProcessCount>0
                            if (_locTransferInLine.ProcessCount > 0)
                            {
                                if (_locTransferInLine.PDQty > 0)
                                {
                                    _locPDQty = _locTransferInLine.PDQty + _locTransferInLine.DQty;
                                }

                                if (_locTransferInLine.PQty > 0)
                                {
                                    _locPQty = _locTransferInLine.PQty + _locTransferInLine.Qty;
                                }

                                if (_locTransferInLine.MxDQty > 0 || _locTransferInLine.MxQty > 0)
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                           (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Item", _locTransferInLine.Item),
                                                            new BinaryOperator("UOM", _locTransferInLine.MxUOM),
                                                            new BinaryOperator("DefaultUOM", _locTransferInLine.MxDUOM),
                                                            new BinaryOperator("Active", true)));
                                }
                                else
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                           (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Item", _locTransferInLine.Item),
                                                            new BinaryOperator("UOM", _locTransferInLine.UOM),
                                                            new BinaryOperator("DefaultUOM", _locTransferInLine.DUOM),
                                                            new BinaryOperator("Active", true)));
                                }

                                if (_locItemUOM != null)
                                {
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locPQty + _locPDQty;
                                }

                            }
                            #endregion ProcessCount>0

                            _locTransferInLine.PDQty = _locPDQty;
                            _locTransferInLine.PDUOM = _locTransferInLine.DUOM;
                            _locTransferInLine.PQty = _locPQty;
                            _locTransferInLine.PUOM = _locTransferInLine.UOM;
                            _locTransferInLine.PTQty = _locInvLineTotal;
                            _locTransferInLine.Save();
                            _locTransferInLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferIn " + ex.ToString());
            }
        }

        private void SetProcessReceiveCount(Session _currSession, TransferIn _locTransferInXPO)
        {
            try
            {
                if (_locTransferInXPO != null)
                {
                    XPCollection<TransferInLine> _locTransferInLines = new XPCollection<TransferInLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("TransferIn", _locTransferInXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locTransferInLines != null && _locTransferInLines.Count > 0)
                    {
                        foreach (TransferInLine _locTransferInLine in _locTransferInLines)
                        {
                            if (_locTransferInLine.Status == Status.Progress || _locTransferInLine.Status == Status.Posted)
                            {
                                _locTransferInLine.ProcessCount = _locTransferInLine.ProcessCount + 1;
                                _locTransferInLine.Save();
                                _locTransferInLine.Session.CommitTransaction();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferIn " + ex.ToString());
            }
        }

        private void SetStatusReceiveTransferInLine(Session _currSession, TransferIn _locTransferInXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locTransferInXPO != null)
                {
                    XPCollection<TransferInLine> _locTransferInLines = new XPCollection<TransferInLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("TransferIn", _locTransferInXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locTransferInLines != null && _locTransferInLines.Count > 0)
                    {
                        foreach (TransferInLine _locTransferInLine in _locTransferInLines)
                        {
                            if (_locTransferInLine.Status == Status.Progress || _locTransferInLine.Status == Status.Posted)
                            {
                                if (_locTransferInLine.RmDQty == 0 && _locTransferInLine.RmQty == 0 && _locTransferInLine.RmTQty == 0)
                                {
                                    _locTransferInLine.Status = Status.Close;
                                    _locTransferInLine.ActivationPosting = true;
                                    _locTransferInLine.StatusDate = now;
                                }
                                else
                                {
                                    _locTransferInLine.Status = Status.Posted;
                                    _locTransferInLine.StatusDate = now;
                                }

                                XPCollection<TransferInLot> _locTransferInLots = new XPCollection<TransferInLot>(_currSession,
                                                                                         new GroupOperator(GroupOperatorType.And,
                                                                                         new BinaryOperator("TransferInLine", _locTransferInLine),
                                                                                         new BinaryOperator("Select", true)));

                                if (_locTransferInLots != null && _locTransferInLots.Count() > 0)
                                {
                                    foreach (TransferInLot _locTransferInLot in _locTransferInLots)
                                    {
                                        if (_locTransferInLot.Status == Status.Progress || _locTransferInLot.Status == Status.Posted)
                                        {
                                            _locTransferInLot.Status = Status.Close;
                                            _locTransferInLot.ActivationPosting = true;
                                            _locTransferInLot.StatusDate = now;
                                            _locTransferInLot.Save();
                                            _locTransferInLot.Session.CommitTransaction();
                                        }
                                    }
                                }
                                _locTransferInLine.Save();
                                _locTransferInLine.Session.CommitTransaction();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferIn " + ex.ToString());
            }
        }

        private void SetNormalReceiveQuantity(Session _currSession, TransferIn _locTransferInXPO)
        {
            try
            {
                if (_locTransferInXPO != null)
                {
                    XPCollection<TransferInLine> _locTransferInLines = new XPCollection<TransferInLine>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("TransferIn", _locTransferInXPO),
                                                                                     new BinaryOperator("Select", true)));

                    if (_locTransferInLines != null && _locTransferInLines.Count > 0)
                    {
                        foreach (TransferInLine _locTransferInLine in _locTransferInLines)
                        {
                            if (_locTransferInLine.Status == Status.Progress || _locTransferInLine.Status == Status.Posted || _locTransferInLine.Status == Status.Close)
                            {
                                if (_locTransferInLine.DQty > 0 || _locTransferInLine.Qty > 0)
                                {
                                    _locTransferInLine.Select = false;
                                    _locTransferInLine.DQty = 0;
                                    _locTransferInLine.Qty = 0;
                                    _locTransferInLine.Save();
                                    _locTransferInLine.Session.CommitTransaction();

                                    XPCollection<TransferInLot> _locTransferInLots = new XPCollection<TransferInLot>(_currSession,
                                                                                         new GroupOperator(GroupOperatorType.And,
                                                                                         new BinaryOperator("TransferInLine", _locTransferInLine),
                                                                                         new BinaryOperator("Select", true)));

                                    if (_locTransferInLots != null && _locTransferInLots.Count() > 0)
                                    {
                                        foreach (TransferInLot _locTransferInLot in _locTransferInLots)
                                        {
                                            if (_locTransferInLot.Status == Status.Progress || _locTransferInLot.Status == Status.Posted)
                                            {
                                                _locTransferInLot.Select = false;
                                                _locTransferInLot.Save();
                                                _locTransferInLot.Session.CommitTransaction();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferIn " + ex.ToString());
            }
        }

        private void SetFinalStatusReceiveTransferIn(Session _currSession, TransferIn _locTransferInXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locCount = 0;

                if (_locTransferInXPO != null)
                {
                    XPCollection<TransferInLine> _locTransferInLines = new XPCollection<TransferInLine>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("TransferIn", _locTransferInXPO)));

                    if (_locTransferInLines != null && _locTransferInLines.Count() > 0)
                    {
                        foreach (TransferInLine _locTransferInLine in _locTransferInLines)
                        {
                            if (_locTransferInLine.Status == Status.Close)
                            {
                                _locCount = _locCount + 1;
                            }
                        }

                        if (_locCount == _locTransferInLines.Count())
                        {
                            _locTransferInXPO.ActivationPosting = true;
                            _locTransferInXPO.Status = Status.Close;
                            _locTransferInXPO.StatusDate = now;
                            _locTransferInXPO.Save();
                            _locTransferInXPO.Session.CommitTransaction();
                        }
                        else
                        {
                            _locTransferInXPO.Status = Status.Posted;
                            _locTransferInXPO.StatusDate = now;
                            _locTransferInXPO.Save();
                            _locTransferInXPO.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferIn " + ex.ToString());
            }
        }

        #endregion Posting

        #region TransferIn Old

        //Menambahkan Qty Available ke Begining Inventory
        private void SetReceiveBeginingInventoryOld(Session _currSession, TransferIn _transferInXPO)
        {
            try
            {
                XPCollection<TransferInLine> _locTransInLines = new XPCollection<TransferInLine>
                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("TransferIn", _transferInXPO),
                                                                    new BinaryOperator("Select", true)));

                if (_locTransInLines != null && _locTransInLines.Count() > 0)
                {
                    GlobalFunction _globFunc = new GlobalFunction();
                    double _locInvLineTotal = 0;
                    XPCollection<BeginingInventoryLine> _locBegInventoryLines = null;
                    ItemUnitOfMeasure _locItemUOM = null;
                    string _fullString = null;
                    string _locItemParse = null;
                    string _locLocationParse = null;
                    string _locBinLocationParse = null;
                    string _locDUOMParse = null;
                    string _locStockTypeParse = null;
                    string _locProjectHeader = null;
                    string _locActiveParse = null;
                    string _locSignCode = null;

                    foreach (TransferInLine _locTransInLine in _locTransInLines)
                    {
                        if (_locTransInLine.Status == Status.Progress || _locTransInLine.Status == Status.Posted)
                        {
                            if (_locTransInLine.DQty > 0 || _locTransInLine.Qty > 0)
                            {
                                XPCollection<TransferInLot> _locTransferInLots = new XPCollection<TransferInLot>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("TransferInLine", _locTransInLine)));
                                #region LotNumber
                                if(_locTransferInLots != null && _locTransferInLots.Count() > 0)
                                {
                                    foreach(TransferInLot _locTransferInLot in _locTransferInLots)
                                    {
                                        if(_locTransferInLot.Select == true)
                                        {
                                            if (_locTransferInLot.BegInvLine != null)
                                            {
                                                if (_locTransferInLot.BegInvLine.Code != null)
                                                {
                                                    BeginingInventoryLine _locBegInvLine = _currSession.FindObject<BeginingInventoryLine>
                                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                                         new BinaryOperator("Code", _locTransferInLot.BegInvLine.Code)));
                                                    if (_locBegInvLine != null)
                                                    {
                                                        _locBegInvLine.Location = _locTransferInLot.LocationTo;
                                                        _locBegInvLine.BinLocation = _locTransferInLot.BinLocationTo;
                                                        _locBegInvLine.Save();
                                                        _locBegInvLine.Session.CommitTransaction();
                                                    }
                                                }
                                            }
                                        }  
                                    }
                                }
                                #endregion LotNumber
                                #region NonLotNumber
                                else
                                {
                                    if (_locTransInLine.Item != null && _locTransInLine.UOM != null && _locTransInLine.DUOM != null)
                                    {
                                        _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                             new BinaryOperator("Item", _locTransInLine.Item),
                                                             new BinaryOperator("UOM", _locTransInLine.UOM),
                                                             new BinaryOperator("DefaultUOM", _locTransInLine.DUOM),
                                                             new BinaryOperator("Active", true)));
                                    }


                                    BeginingInventory _locBeginingInventory = _currSession.FindObject<BeginingInventory>(new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Item", _locTransInLine.Item)));

                                    #region UpdateBeginingInventory
                                    if (_locBeginingInventory != null)
                                    {
                                        #region Code
                                        if (_locTransInLine.Item != null) { _locItemParse = "[Item.Code]=='" + _locTransInLine.Item.Code + "'"; }
                                        else { _locItemParse = ""; }

                                        if (_locTransInLine.LocationTo != null && (_locTransInLine.Item != null))
                                        { _locLocationParse = " AND [Location.Code]=='" + _locTransInLine.LocationTo.Code + "'"; }
                                        else if (_locTransInLine.LocationTo != null && _locTransInLine.Item == null)
                                        { _locLocationParse = " [Location.Code]=='" + _locTransInLine.LocationTo.Code + "'"; }
                                        else { _locLocationParse = ""; }

                                        if (_locTransInLine.BinLocationTo != null && (_locTransInLine.Item != null || _locTransInLine.LocationTo != null))
                                        { _locBinLocationParse = " AND [BinLocation.Code]=='" + _locTransInLine.BinLocationTo.Code + "'"; }
                                        else if (_locTransInLine.BinLocationTo != null && _locTransInLine.Item == null && _locTransInLine.LocationTo == null)
                                        { _locBinLocationParse = " [BinLocation.Code]=='" + _locTransInLine.BinLocationTo.Code + "'"; }
                                        else { _locBinLocationParse = ""; }

                                        if (_locTransInLine.DUOM != null && (_locTransInLine.Item != null || _locTransInLine.LocationTo != null || _locTransInLine.BinLocationTo != null))
                                        { _locDUOMParse = " AND [DefaultUOM.Code]=='" + _locTransInLine.DUOM.Code + "'"; }
                                        else if (_locTransInLine.DUOM != null && _locTransInLine.Item == null && _locTransInLine.LocationTo == null && _locTransInLine.BinLocationTo == null)
                                        { _locDUOMParse = " [DefaultUOM.Code]=='" + _locTransInLine.DUOM.Code + "'"; }
                                        else { _locDUOMParse = ""; }

                                        if (_locTransInLine.StockTypeTo != StockType.None && (_locTransInLine.Item != null || _locTransInLine.LocationTo != null || _locTransInLine.BinLocationTo != null
                                            || _locTransInLine.DUOM != null))
                                        { _locStockTypeParse = " AND [StockType]=='" + GetStockType(_locTransInLine.StockTypeTo).ToString() + "'"; }
                                        else if (_locTransInLine.StockTypeTo != StockType.None && _locTransInLine.Item == null && _locTransInLine.LocationTo == null && _locTransInLine.BinLocationTo == null
                                            && _locTransInLine.DUOM == null)
                                        { _locStockTypeParse = " [StockType]=='" + GetStockType(_locTransInLine.StockTypeTo).ToString() + "'"; }
                                        else { _locStockTypeParse = ""; }

                                        if (_locTransInLine.Item == null && _locTransInLine.LocationTo == null && _locTransInLine.BinLocationTo == null
                                            && _locTransInLine.DUOM == null && _locTransInLine.StockTypeTo != StockType.None)
                                        { _locActiveParse = " [Active]=='" + GetActive(true).ToString() + "'"; }
                                        else { _locActiveParse = " AND [Active]=='" + GetActive(true).ToString() + "'"; }

                                        if (_transferInXPO.ProjectHeader != null)
                                        { _locProjectHeader = " AND [ProjectHeader.Code]=='" + _transferInXPO.ProjectHeader.Code + "'"; }
                                        else
                                        { _locProjectHeader = ""; }

                                        if (_locItemParse != null || _locLocationParse != null || _locBinLocationParse != null || _locDUOMParse != null || _locStockTypeParse != null || _locProjectHeader != null)
                                        {
                                            _fullString = _locItemParse + _locLocationParse + _locBinLocationParse + _locDUOMParse + _locStockTypeParse + _locActiveParse + _locProjectHeader;
                                        }
                                        else
                                        {
                                            _fullString = _locActiveParse;
                                        }

                                        _locBegInventoryLines = new XPCollection<BeginingInventoryLine>(_currSession, CriteriaOperator.Parse(_fullString));

                                        #endregion Code

                                        #region UpdateBeginingInventoryLine
                                        if (_locBegInventoryLines != null && _locBegInventoryLines.Count() > 0)
                                        {

                                            foreach (BeginingInventoryLine _locBegInventoryLine in _locBegInventoryLines)
                                            {
                                                if (_locItemUOM != null)
                                                {
                                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                    {
                                                        _locInvLineTotal = _locTransInLine.Qty * _locItemUOM.DefaultConversion + _locTransInLine.DQty;
                                                    }
                                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                    {
                                                        _locInvLineTotal = _locTransInLine.Qty / _locItemUOM.Conversion + _locTransInLine.DQty;
                                                    }
                                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                    {
                                                        _locInvLineTotal = _locTransInLine.Qty + _locTransInLine.DQty;
                                                    }

                                                }
                                                else
                                                {
                                                    _locInvLineTotal = _locTransInLine.Qty + _locTransInLine.DQty;
                                                }
                                                _locBegInventoryLine.QtyAvailable = _locBegInventoryLine.QtyAvailable + _locInvLineTotal;
                                                _locBegInventoryLine.Save();
                                                _locBegInventoryLine.Session.CommitTransaction();

                                                _locBeginingInventory.QtyAvailable = _locBeginingInventory.QtyAvailable + _locInvLineTotal;
                                                _locBeginingInventory.Save();
                                                _locBeginingInventory.Session.CommitTransaction();

                                            }
                                        }
                                        #endregion UpdateBeginingInventoryLine

                                        #region NewBeginingInventoryLine
                                        else
                                        {
                                            if (_locItemUOM != null)
                                            {
                                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                {
                                                    _locInvLineTotal = _locTransInLine.Qty * _locItemUOM.DefaultConversion + _locTransInLine.DQty;
                                                }
                                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                {
                                                    _locInvLineTotal = _locTransInLine.Qty / _locItemUOM.Conversion + _locTransInLine.DQty;
                                                }
                                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                {
                                                    _locInvLineTotal = _locTransInLine.Qty + _locTransInLine.DQty;
                                                }
                                            }
                                            else
                                            {
                                                _locInvLineTotal = _locTransInLine.Qty + _locTransInLine.DQty;
                                            }

                                            BeginingInventoryLine _locSaveDataBeginingInventory = new BeginingInventoryLine(_currSession)
                                            {
                                                Item = _locTransInLine.Item,
                                                Location = _locTransInLine.LocationTo,
                                                BinLocation = _locTransInLine.BinLocationTo,
                                                QtyAvailable = _locInvLineTotal,
                                                DefaultUOM = _locTransInLine.DUOM,
                                                StockType = _locTransInLine.StockTypeTo,
                                                Active = true,
                                                BeginingInventory = _locBeginingInventory,
                                            };
                                            _locSaveDataBeginingInventory.Save();
                                            _locSaveDataBeginingInventory.Session.CommitTransaction();
                                        }
                                        #endregion NewBeginingInventoryLine
                                    }
                                    #endregion UpdateBeginingInventory

                                    #region CreateNewBeginingInventory
                                    else
                                    {
                                        _locSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.BeginingInventory);

                                        if (_locSignCode != null)
                                        {
                                            if (_locItemUOM != null)
                                            {
                                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                {
                                                    _locInvLineTotal = _locTransInLine.Qty * _locItemUOM.DefaultConversion + _locTransInLine.DQty;
                                                }
                                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                {
                                                    _locInvLineTotal = _locTransInLine.Qty / _locItemUOM.Conversion + _locTransInLine.DQty;
                                                }
                                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                {
                                                    _locInvLineTotal = _locTransInLine.Qty + _locTransInLine.DQty;
                                                }

                                            }
                                            else
                                            {
                                                _locInvLineTotal = _locTransInLine.Qty + _locTransInLine.DQty;
                                            }

                                            BeginingInventory _saveDataBegInv = new BeginingInventory(_currSession)
                                            {
                                                Item = _locTransInLine.Item,
                                                QtyAvailable = _locInvLineTotal,
                                                DefaultUOM = _locTransInLine.DUOM,
                                                Active = true,
                                                SignCode = _locSignCode,
                                                Company = _transferInXPO.Company,
                                            };
                                            _saveDataBegInv.Save();
                                            _saveDataBegInv.Session.CommitTransaction();

                                            BeginingInventory _locBeginingInventory2 = _currSession.FindObject<BeginingInventory>(new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Item", _locTransInLine.Item),
                                                                        new BinaryOperator("SignCode", _locSignCode)));

                                            if (_locBeginingInventory2 != null)
                                            {
                                                BeginingInventoryLine _saveDataBegInvLine = new BeginingInventoryLine(_currSession)
                                                {
                                                    Item = _locTransInLine.Item,
                                                    Location = _locTransInLine.LocationTo,
                                                    BinLocation = _locTransInLine.BinLocationTo,
                                                    QtyAvailable = _locBeginingInventory2.QtyAvailable,
                                                    DefaultUOM = _locBeginingInventory2.DefaultUOM,
                                                    StockType = _locTransInLine.StockTypeTo,
                                                    ProjectHeader = _transferInXPO.ProjectHeader,
                                                    Active = true,
                                                    BeginingInventory = _locBeginingInventory2,
                                                    Company = _locBeginingInventory2.Company,
                                                };
                                                _saveDataBegInvLine.Save();
                                                _saveDataBegInvLine.Session.CommitTransaction();
                                            }
                                        }
                                    }
                                    #endregion CreateNewBeginingInventory
                                }
                                #endregion NonLotNumber

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = TransferIn ", ex.ToString());
            }
        }

        //Membuat jurnal positif di inventory journal
        private void SetReceiveInventoryJournalOld(Session _currSession, TransferIn _transferInXPO)
        {
            try
            {
                XPCollection<TransferInLine> _locTransInLines = new XPCollection<TransferInLine>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("TransferIn", _transferInXPO),
                                                                        new BinaryOperator("Select", true)));

                if (_locTransInLines != null && _locTransInLines.Count > 0)
                {
                    double _locInvLineTotal = 0;
                    DateTime now = DateTime.Now;

                    foreach (TransferInLine _locTransInLine in _locTransInLines)
                    {
                        if (_locTransInLine.Status == Status.Progress || _locTransInLine.Status == Status.Posted)
                        {
                            if (_locTransInLine.DQty > 0 || _locTransInLine.Qty > 0)
                            {
                                //Journal Bertambah di locationTo
                                XPCollection<TransferInLot> _locTransferInLots = new XPCollection<TransferInLot>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("TransferInLine", _locTransInLine)));
                                #region LotNumber
                                if (_locTransferInLots != null && _locTransferInLots.Count() > 0)
                                {
                                    foreach (TransferInLot _locTransferInLot in _locTransferInLots)
                                    {
                                        if (_locTransferInLot.Select == true)
                                        {
                                            InventoryJournal _locPositifInventoryJournal = new InventoryJournal(_currSession)
                                            {
                                                DocumentType = _transferInXPO.DocumentType,
                                                DocNo = _transferInXPO.DocNo,
                                                Location = _locTransferInLot.LocationTo,
                                                BinLocation = _locTransferInLot.BinLocationTo,
                                                StockType = _locTransferInLot.StockTypeTo,
                                                Item = _locTransInLine.Item,
                                                QtyNeg = 0,
                                                QtyPos = _locTransferInLot.TQty,
                                                DUOM = _locTransferInLot.DUOM,
                                                JournalDate = now,
                                                ProjectHeader = _transferInXPO.ProjectHeader,
                                                Company = _transferInXPO.Company,
                                                TransferIn = _transferInXPO,
                                            };
                                            _locPositifInventoryJournal.Save();
                                            _locPositifInventoryJournal.Session.CommitTransaction();
                                        }
                                    }
                                }
                                #endregion LotNumber
                                #region NonLotNumber
                                else
                                {
                                    if (_locTransInLine.Item != null && _locTransInLine.UOM != null && _locTransInLine.DUOM != null)
                                    {
                                        ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                             new BinaryOperator("Item", _locTransInLine.Item),
                                                             new BinaryOperator("UOM", _locTransInLine.UOM),
                                                             new BinaryOperator("DefaultUOM", _locTransInLine.DUOM),
                                                             new BinaryOperator("Active", true)));
                                        if (_locItemUOM != null)
                                        {

                                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                            {
                                                _locInvLineTotal = _locTransInLine.Qty * _locItemUOM.DefaultConversion + _locTransInLine.DQty;
                                            }
                                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                            {
                                                _locInvLineTotal = _locTransInLine.Qty / _locItemUOM.Conversion + _locTransInLine.DQty;
                                            }
                                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                            {
                                                _locInvLineTotal = _locTransInLine.Qty + _locTransInLine.DQty;
                                            }

                                            InventoryJournal _locPositifInventoryJournal = new InventoryJournal(_currSession)
                                            {
                                                DocumentType = _transferInXPO.DocumentType,
                                                DocNo = _transferInXPO.DocNo,
                                                Location = _locTransInLine.LocationTo,
                                                BinLocation = _locTransInLine.BinLocationTo,
                                                StockType = _locTransInLine.StockTypeTo,
                                                Item = _locTransInLine.Item,
                                                QtyNeg = 0,
                                                QtyPos = _locInvLineTotal,
                                                DUOM = _locTransInLine.DUOM,
                                                JournalDate = now,
                                                ProjectHeader = _transferInXPO.ProjectHeader,
                                                Company = _transferInXPO.Company,
                                                TransferIn = _transferInXPO,
                                            };
                                            _locPositifInventoryJournal.Save();
                                            _locPositifInventoryJournal.Session.CommitTransaction();

                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locTransInLine.Qty + _locTransInLine.DQty;

                                        InventoryJournal _locPositifInventoryJournal = new InventoryJournal(_currSession)
                                        {

                                            DocumentType = _transferInXPO.DocumentType,
                                            DocNo = _transferInXPO.DocNo,
                                            Location = _locTransInLine.LocationTo,
                                            BinLocation = _locTransInLine.BinLocationTo,
                                            StockType = _locTransInLine.StockTypeTo,
                                            Item = _locTransInLine.Item,
                                            QtyNeg = 0,
                                            QtyPos = _locInvLineTotal,
                                            DUOM = _locTransInLine.DUOM,
                                            JournalDate = now,
                                            ProjectHeader = _transferInXPO.ProjectHeader,
                                            Company = _transferInXPO.Company,
                                            TransferIn = _transferInXPO,
                                        };
                                        _locPositifInventoryJournal.Save();
                                        _locPositifInventoryJournal.Session.CommitTransaction();
                                    }
                                }
                                #endregion NonLotNumber
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = TransferIn ", ex.ToString());
            }
        }

        //Menentukan sisa dari transaksi receive -> Done 10 Dec 2019
        private void SetRemainReceivedQtyOld(Session _currSession, TransferIn _transferInXPO)
        {
            try
            {
                if (_transferInXPO != null)
                {
                    XPCollection<TransferInLine> _locTransInLines = new XPCollection<TransferInLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("TransferIn", _transferInXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locTransInLines != null && _locTransInLines.Count > 0)
                    {
                        double _locRmDQty = 0;
                        double _locRmQty = 0;
                        double _locInvLineTotal = 0;
                        ItemUnitOfMeasure _locItemUOM = null;

                        foreach (TransferInLine _locTransInLine in _locTransInLines)
                        {
                            #region ProcessCount=0
                            if (_locTransInLine.ProcessCount == 0)
                            {
                                #region MaxQuantity
                                if (_locTransInLine.MxDQty > 0)
                                {
                                    if (_locTransInLine.DQty > 0 && _locTransInLine.DQty <= _locTransInLine.MxDQty)
                                    {
                                        _locRmDQty = _locTransInLine.MxDQty - _locTransInLine.DQty;
                                    }

                                    if (_locTransInLine.Qty > 0 && _locTransInLine.Qty <= _locTransInLine.MxQty)
                                    {
                                        _locRmQty = _locTransInLine.MxQty - _locTransInLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locTransInLine.Item),
                                                                new BinaryOperator("UOM", _locTransInLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locTransInLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty + _locRmDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }

                                }
                                #endregion MaxQuantity
                                #region NonMaxQuantity
                                else
                                {
                                    if (_locTransInLine.DQty > 0)
                                    {
                                        _locRmDQty = 0;
                                    }

                                    if (_locTransInLine.Qty > 0)
                                    {
                                        _locRmQty = 0;
                                    }

                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locTransInLine.Item),
                                                                new BinaryOperator("UOM", _locTransInLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locTransInLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty + _locRmDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion ProcessCount=0
                            #region ProcessCount>0
                            if (_locTransInLine.ProcessCount > 0)
                            {
                                if (_locTransInLine.RmDQty > 0)
                                {
                                    _locRmDQty = _locTransInLine.RmDQty - _locTransInLine.DQty;
                                }

                                if (_locTransInLine.RmQty > 0)
                                {
                                    _locRmQty = _locTransInLine.RmQty - _locTransInLine.Qty;
                                }

                                if (_locTransInLine.MxDQty > 0 || _locTransInLine.MxQty > 0)
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locTransInLine.Item),
                                                                new BinaryOperator("UOM", _locTransInLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locTransInLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                }
                                else
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locTransInLine.Item),
                                                                new BinaryOperator("UOM", _locTransInLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locTransInLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                }

                                if (_locItemUOM != null)
                                {
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locRmQty + _locRmDQty;
                                }

                            }
                            #endregion ProcessCount>0

                            _locTransInLine.RmDQty = _locRmDQty;
                            _locTransInLine.RmQty = _locRmQty;
                            _locTransInLine.RmTQty = _locInvLineTotal;
                            _locTransInLine.Save();
                            _locTransInLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferIn " + ex.ToString());
            }
        }

        //Menentukan jumlah quantity yang di posting -> Done 10 Dec 2019
        private void SetPostingReceivedQtyOld(Session _currSession, TransferIn _transferInXPO)
        {
            try
            {
                if (_transferInXPO != null)
                {
                    XPCollection<TransferInLine> _locTransInLines = new XPCollection<TransferInLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("TransferIn", _transferInXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locTransInLines != null && _locTransInLines.Count > 0)
                    {
                        double _locPDQty = 0;
                        double _locPQty = 0;
                        double _locInvLineTotal = 0;
                        ItemUnitOfMeasure _locItemUOM = null;

                        foreach (TransferInLine _locTransInLine in _locTransInLines)
                        {
                            #region ProcessCount=0
                            if (_locTransInLine.ProcessCount == 0)
                            {
                                #region MaxQuantity
                                if (_locTransInLine.MxDQty > 0)
                                {
                                    if (_locTransInLine.DQty > 0 && _locTransInLine.DQty <= _locTransInLine.MxDQty)
                                    {
                                        _locPDQty = _locTransInLine.DQty;
                                    }

                                    if (_locTransInLine.Qty > 0 && _locTransInLine.Qty <= _locTransInLine.MxQty)
                                    {
                                        _locPQty = _locTransInLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locTransInLine.Item),
                                                                new BinaryOperator("UOM", _locTransInLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locTransInLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty + _locPDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }

                                }
                                #endregion MaxQuantity
                                #region NonMaxQuantity
                                else
                                {
                                    if (_locTransInLine.DQty > 0)
                                    {
                                        _locPDQty = _locTransInLine.DQty;
                                    }

                                    if (_locTransInLine.Qty > 0)
                                    {
                                        _locPQty = _locTransInLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locTransInLine.Item),
                                                                new BinaryOperator("UOM", _locTransInLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locTransInLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty + _locPDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion ProcessCount=0

                            #region ProcessCount>0
                            if (_locTransInLine.ProcessCount > 0)
                            {
                                if (_locTransInLine.PDQty > 0)
                                {
                                    _locPDQty = _locTransInLine.PDQty + _locTransInLine.DQty;
                                }

                                if (_locTransInLine.PQty > 0)
                                {
                                    _locPQty = _locTransInLine.PQty + _locTransInLine.Qty;
                                }

                                if (_locTransInLine.MxDQty > 0 || _locTransInLine.MxQty > 0)
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locTransInLine.Item),
                                                                new BinaryOperator("UOM", _locTransInLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locTransInLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                }
                                else
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locTransInLine.Item),
                                                                new BinaryOperator("UOM", _locTransInLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locTransInLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                }


                                if (_locItemUOM != null)
                                {
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locPQty + _locPDQty;
                                }

                            }
                            #endregion ProcessCount>0

                            _locTransInLine.PDQty = _locPDQty;
                            _locTransInLine.PDUOM = _locTransInLine.DUOM;
                            _locTransInLine.PQty = _locPQty;
                            _locTransInLine.PUOM = _locTransInLine.UOM;
                            _locTransInLine.PTQty = _locInvLineTotal;
                            _locTransInLine.Save();
                            _locTransInLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferIn " + ex.ToString());
            }
        }

        //Menentukan Banyak process receive
        private void SetProcessCountReceiveOld(Session _currSession, TransferIn _transferInXPO)
        {
            try
            {
                if (_transferInXPO != null)
                {
                    XPCollection<TransferInLine> _locTransInLines = new XPCollection<TransferInLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("TransferIn", _transferInXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locTransInLines != null && _locTransInLines.Count > 0)
                    {

                        foreach (TransferInLine _locTransInLine in _locTransInLines)
                        {
                            if (_locTransInLine.Status == Status.Progress || _locTransInLine.Status == Status.Posted)
                            {
                                _locTransInLine.ProcessCount = _locTransInLine.ProcessCount + 1;
                                _locTransInLine.Save();
                                _locTransInLine.Session.CommitTransaction();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferIn " + ex.ToString());
            }

        }

        //Menentukan Status Receive Pada Transfer In Line
        private void SetStatusReceiveTransferInLineOld(Session _currSession, TransferIn _transferInXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_transferInXPO != null)
                {
                    XPCollection<TransferInLine> _locTransInLines = new XPCollection<TransferInLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("TransferIn", _transferInXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locTransInLines != null && _locTransInLines.Count > 0)
                    {

                        foreach (TransferInLine _locTransInLine in _locTransInLines)
                        {
                            if (_locTransInLine.Status == Status.Progress || _locTransInLine.Status == Status.Posted)
                            {
                                if (_locTransInLine.RmDQty == 0 && _locTransInLine.RmQty == 0 && _locTransInLine.RmTQty == 0)
                                {
                                    _locTransInLine.Status = Status.Close;
                                    _locTransInLine.ActivationPosting = true;
                                    _locTransInLine.StatusDate = now;
                                }
                                else
                                {
                                    _locTransInLine.Status = Status.Posted;
                                    _locTransInLine.StatusDate = now;
                                }

                                XPCollection<TransferInLot> _locTransferInLots = new XPCollection<TransferInLot>(_currSession,
                                                                                         new GroupOperator(GroupOperatorType.And,
                                                                                         new BinaryOperator("TransferInLine", _locTransInLine),
                                                                                         new BinaryOperator("Select", true)));

                                if (_locTransferInLots != null && _locTransferInLots.Count() > 0)
                                {
                                    foreach (TransferInLot _locTransferInLot in _locTransferInLots)
                                    {
                                        if (_locTransferInLot.Status == Status.Progress || _locTransferInLot.Status == Status.Posted)
                                        {
                                            _locTransferInLot.Status = Status.Close;
                                            _locTransferInLot.ActivationPosting = true;
                                            _locTransferInLot.StatusDate = now;
                                            _locTransferInLot.Save();
                                            _locTransferInLot.Session.CommitTransaction();
                                        }
                                    }
                                }

                                _locTransInLine.Save();
                                _locTransInLine.Session.CommitTransaction();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferIn " + ex.ToString());
            }
        }

        //Menormalkan Quantity
        private void SetNormalQuantityReceiveOld(Session _currSession, TransferIn _transferInXPO)
        {
            try
            {
                if (_transferInXPO != null)
                {
                    XPCollection<TransferInLine> _locTransInLines = new XPCollection<TransferInLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("TransferIn", _transferInXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locTransInLines != null && _locTransInLines.Count > 0)
                    {

                        foreach (TransferInLine _locTransInLine in _locTransInLines)
                        {
                            if (_locTransInLine.Status == Status.Progress || _locTransInLine.Status == Status.Posted || _locTransInLine.Status == Status.Close)
                            {
                                if (_locTransInLine.DQty > 0 || _locTransInLine.Qty > 0)
                                {
                                    _locTransInLine.Select = false;
                                    _locTransInLine.DQty = 0;
                                    _locTransInLine.Qty = 0;
                                    _locTransInLine.Save();
                                    _locTransInLine.Session.CommitTransaction();

                                    XPCollection<TransferInLot> _locTransferInLots = new XPCollection<TransferInLot>(_currSession,
                                                                                         new GroupOperator(GroupOperatorType.And,
                                                                                         new BinaryOperator("TransferInLine", _locTransInLine),
                                                                                         new BinaryOperator("Select", true)));

                                    if (_locTransferInLots != null && _locTransferInLots.Count() > 0)
                                    {
                                        foreach (TransferInLot _locTransferInLot in _locTransferInLots)
                                        {
                                            if (_locTransferInLot.Status == Status.Progress || _locTransferInLot.Status == Status.Posted)
                                            {
                                                _locTransferInLot.Select = false;
                                                _locTransferInLot.Save();
                                                _locTransferInLot.Session.CommitTransaction();
                                            }
                                        }
                                    }
                                }


                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferIn " + ex.ToString());
            }
        }

        //Menentukan Status Receive Pada Transfer In
        private void SetFinalStatusReceiveTransferInOld(Session _currSession, TransferIn _transferInXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locCount = 0;
                if (_transferInXPO != null)
                {
                    XPCollection<TransferInLine> _locTransInLines = new XPCollection<TransferInLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("TransferIn", _transferInXPO)));

                    if (_locTransInLines != null && _locTransInLines.Count() > 0)
                    {

                        foreach (TransferInLine _locTransInLine in _locTransInLines)
                        {
                            if (_locTransInLine.Status == Status.Close)
                            {
                                _locCount = _locCount + 1;
                            }
                        }

                        if (_locCount == _locTransInLines.Count())
                        {
                            _transferInXPO.ActivationPosting = true;
                            _transferInXPO.Status = Status.Close;
                            _transferInXPO.StatusDate = now;
                            _transferInXPO.Save();
                            _transferInXPO.Session.CommitTransaction();
                        }
                        else
                        {
                            _transferInXPO.Status = Status.Posted;
                            _transferInXPO.StatusDate = now;
                            _transferInXPO.Save();
                            _transferInXPO.Session.CommitTransaction();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferIn " + ex.ToString());
            }
        }

        #endregion TransferIn

        #region Global Method

        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0} !", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private int GetStockType(StockType objectName)
        {
            int _result = 0;
            try
            {
                if (objectName == StockType.Good)
                {
                    _result = 1;
                }
                else if (objectName == StockType.Bad)
                {
                    _result = 2;
                }
                else
                {
                    _result = 0;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = TransferIn " + ex.ToString());
            }
            return _result;
        }

        private int GetActive(bool objectName)
        {
            int _result = 0;
            try
            {
                if (objectName == true)
                {
                    _result = 1;
                }
                else
                {
                    _result = 0;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = TransferIn " + ex.ToString());
            }
            return _result;
        }

        #endregion Global Method

        
    }
}
