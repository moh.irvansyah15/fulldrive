﻿namespace FullDrive.Module.Controllers
{
    partial class DebitMemoLineActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.DebitMemoLineTaxCalculationAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.DebitMemoLineDiscCalculationAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // DebitMemoLineTaxCalculationAction
            // 
            this.DebitMemoLineTaxCalculationAction.Caption = "Tax Calculation";
            this.DebitMemoLineTaxCalculationAction.ConfirmationMessage = null;
            this.DebitMemoLineTaxCalculationAction.Id = "DebitMemoLineTaxCalculationActionId";
            this.DebitMemoLineTaxCalculationAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.DebitMemoLine);
            this.DebitMemoLineTaxCalculationAction.ToolTip = null;
            this.DebitMemoLineTaxCalculationAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.DebitMemoLineTaxCalculationAction_Execute);
            // 
            // DebitMemoLineDiscCalculationAction
            // 
            this.DebitMemoLineDiscCalculationAction.Caption = "Disc Calculation";
            this.DebitMemoLineDiscCalculationAction.ConfirmationMessage = null;
            this.DebitMemoLineDiscCalculationAction.Id = "DebitMemoLineDiscCalculationActionId";
            this.DebitMemoLineDiscCalculationAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.DebitMemoLine);
            this.DebitMemoLineDiscCalculationAction.ToolTip = null;
            this.DebitMemoLineDiscCalculationAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.DebitMemoLineDiscCalculationAction_Execute);
            // 
            // DebitMemoLineActionController
            // 
            this.Actions.Add(this.DebitMemoLineTaxCalculationAction);
            this.Actions.Add(this.DebitMemoLineDiscCalculationAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction DebitMemoLineTaxCalculationAction;
        private DevExpress.ExpressApp.Actions.SimpleAction DebitMemoLineDiscCalculationAction;
    }
}
