﻿namespace FullDrive.Module.Controllers
{
    partial class ShipmentBookingOriginCollectionActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ShipmentBookingOriginCollectionShowSBMAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // ShipmentBookingOriginCollectionShowSBMAction
            // 
            this.ShipmentBookingOriginCollectionShowSBMAction.Caption = "Show SBM";
            this.ShipmentBookingOriginCollectionShowSBMAction.ConfirmationMessage = null;
            this.ShipmentBookingOriginCollectionShowSBMAction.Id = "ShipmentBookingOriginCollectionShowSBMActionId";
            this.ShipmentBookingOriginCollectionShowSBMAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.ShipmentBookingOriginCollection);
            this.ShipmentBookingOriginCollectionShowSBMAction.ToolTip = null;
            this.ShipmentBookingOriginCollectionShowSBMAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.ShipmentBookingOriginCollectionShowSBMAction_Execute);
            // 
            // ShipmentBookingOriginCollectionActionController
            // 
            this.Actions.Add(this.ShipmentBookingOriginCollectionShowSBMAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction ShipmentBookingOriginCollectionShowSBMAction;
    }
}
