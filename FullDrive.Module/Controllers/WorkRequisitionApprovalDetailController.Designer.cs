﻿namespace FullDrive.Module.Controllers
{
    partial class WorkRequisitionApprovalDetailController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.WorkRequisitionApprovalDetail = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // WorkRequisitionApprovalDetail
            // 
            this.WorkRequisitionApprovalDetail.Caption = "Approval";
            this.WorkRequisitionApprovalDetail.ConfirmationMessage = null;
            this.WorkRequisitionApprovalDetail.Id = "WorkRequisitionApprovalDetailId";
            this.WorkRequisitionApprovalDetail.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.WorkRequisitionApprovalDetail.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.WorkRequisition);
            this.WorkRequisitionApprovalDetail.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.WorkRequisitionApprovalDetail.ToolTip = null;
            this.WorkRequisitionApprovalDetail.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.WorkRequisitionApprovalDetail.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.WorkRequisitionApprovalDetail_Execute);
            // 
            // WorkRequisitionApprovalDetailController
            // 
            this.Actions.Add(this.WorkRequisitionApprovalDetail);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SingleChoiceAction WorkRequisitionApprovalDetail;
    }
}
