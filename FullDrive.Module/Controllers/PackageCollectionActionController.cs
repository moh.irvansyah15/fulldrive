﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using System.Collections;
using DevExpress.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class PackageCollectionActionController : ViewController
    {
        public PackageCollectionActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void PackageCollectionShowOPMAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
            var ListView = View as ListView;
            IObjectSpace os = Application.CreateObjectSpace();
            string listViewId = Application.FindListViewId(typeof(OriginPackageMonitoring));
            CollectionSourceBase cs = Application.CreateCollectionSource(os, typeof(OriginPackageMonitoring), listViewId);
            //Tinggal buat 
            ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
            string _stringOPM = null;
            if (_objectsToProcess != null)
            {
                foreach (Object obj in _objectsToProcess)
                {
                    PackageCollection _locPackageCollection = (PackageCollection)_objectSpace.GetObject(obj);
                    if (_locPackageCollection != null)
                    {
                        if (_locPackageCollection.FreightForwardingOrigin != null)
                        {
                            if (_stringOPM == null)
                            {
                                if (_locPackageCollection.FreightForwardingOrigin.Code != null)
                                {
                                    _stringOPM = "( [FreightForwardingOrigin.Code]=='" + _locPackageCollection.FreightForwardingOrigin.Code + "' AND [Status] != 4 )";
                                }

                            }
                            else
                            {
                                if (_locPackageCollection.FreightForwardingOrigin.Code != null)
                                {
                                    _stringOPM = _stringOPM + " OR ( [FreightForwardingOrigin.Code]=='" + _locPackageCollection.FreightForwardingOrigin.Code + "' AND [Status] != 4 )";
                                }
                            }
                        }
                    }
                }
            }
            if (_stringOPM != null)
            {
                cs.Criteria["PackageCollectionFilterOPM"] = CriteriaOperator.Parse(_stringOPM);
            }
            e.ShowViewParameters.CreatedView = Application.CreateListView(
                listViewId,
                cs,
                true
            );
            e.ShowViewParameters.Context = TemplateContext.View;
            e.ShowViewParameters.TargetWindow = TargetWindow.NewModalWindow;
            DialogController dc = Application.CreateController<DialogController>();
            dc.Accepting += new EventHandler<DialogControllerAcceptingEventArgs>(dc_Accepting1);
            e.ShowViewParameters.Controllers.Add(dc);
        }

        void dc_Accepting1(object sender, DialogControllerAcceptingEventArgs e)
        {
            try
            {
                Routing _locRouting = null;
                View popupView = ((Controller)sender).Frame.View;
                ArrayList _objectsToProcess = new ArrayList(View.SelectedObjects);
                if (_objectsToProcess != null)
                {
                    foreach (Object _obj in _objectsToProcess)
                    {
                        PackageCollection _locPackCollection = (PackageCollection)popupView.ObjectSpace.GetObject(_obj);
                        if (_locPackCollection != null)
                        {
                            if (_locPackCollection.Routing != null)
                            {
                                _locRouting = _locPackCollection.Routing;
                            }
                        }
                    }
                }
                if (_locRouting != null)
                {
                    ArrayList _objectsToProcess2 = new ArrayList(popupView.SelectedObjects);
                    if (_objectsToProcess2 != null)
                    {
                        foreach (Object _obj2 in _objectsToProcess2)
                        {
                            OriginPackageMonitoring _locOriPackMonitoring = (OriginPackageMonitoring)popupView.ObjectSpace.GetObject(_obj2);
                            if (_locOriPackMonitoring != null && _locOriPackMonitoring.Select == true && _locOriPackMonitoring.PostedCount != 4)
                            {
                                _locOriPackMonitoring.Routing = _locRouting;
                            }
                        }
                    }
                }
                popupView.ObjectSpace.CommitChanges();
            }
            catch (ValidationException)
            {
            }
        }

        private void PackageCollectionShowDPMAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
            var ListView = View as ListView;
            IObjectSpace os = Application.CreateObjectSpace();
            string listViewId = Application.FindListViewId(typeof(DestinationPackageMonitoring));
            CollectionSourceBase cs = Application.CreateCollectionSource(os, typeof(DestinationPackageMonitoring), listViewId);
            //Tinggal buat 
            ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
            string _stringDPM = null;
            if (_objectsToProcess != null)
            {
                foreach (Object obj in _objectsToProcess)
                {
                    PackageCollection _locPackageCollection = (PackageCollection)_objectSpace.GetObject(obj);
                    if (_locPackageCollection != null)
                    {
                        if (_locPackageCollection.FreightForwardingDestination != null)
                        {
                            if (_stringDPM == null)
                            {
                                if (_locPackageCollection.FreightForwardingDestination.Code != null)
                                {
                                    _stringDPM = "( [FreightForwardingDestination.Code]=='" + _locPackageCollection.FreightForwardingDestination.Code + "' AND [Status] != 4 )";
                                }

                            }
                            else
                            {
                                if (_locPackageCollection.FreightForwardingOrigin.Code != null)
                                {
                                    _stringDPM = _stringDPM + " OR ( [FreightForwardingDestination.Code]=='" + _locPackageCollection.FreightForwardingDestination.Code + "' AND [Status] != 4 )";
                                }
                            }
                        }
                    }
                }
            }
            if (_stringDPM != null)
            {
                cs.Criteria["PackageCollectionFilterDPM"] = CriteriaOperator.Parse(_stringDPM);
            }
            e.ShowViewParameters.CreatedView = Application.CreateListView(
                listViewId,
                cs,
                true
            );
            e.ShowViewParameters.Context = TemplateContext.View;
            e.ShowViewParameters.TargetWindow = TargetWindow.NewModalWindow;
            DialogController dc = Application.CreateController<DialogController>();
            dc.Accepting += new EventHandler<DialogControllerAcceptingEventArgs>(dc_Accepting2);
            e.ShowViewParameters.Controllers.Add(dc);
        }

        void dc_Accepting2(object sender, DialogControllerAcceptingEventArgs e)
        {
            try
            {
                Routing _locRouting = null;
                View popupView = ((Controller)sender).Frame.View;
                ArrayList _objectsToProcess = new ArrayList(View.SelectedObjects);
                if (_objectsToProcess != null)
                {
                    foreach (Object _obj in _objectsToProcess)
                    {
                        PackageCollection _locPackCollection = (PackageCollection)popupView.ObjectSpace.GetObject(_obj);
                        if (_locPackCollection != null)
                        {
                            if (_locPackCollection.Routing != null)
                            {
                                _locRouting = _locPackCollection.Routing;
                            }
                        }
                    }
                }
                if (_locRouting != null)
                {
                    ArrayList _objectsToProcess2 = new ArrayList(popupView.SelectedObjects);
                    if (_objectsToProcess2 != null)
                    {
                        foreach (Object _obj2 in _objectsToProcess2)
                        {
                            DestinationPackageMonitoring _locDestPackMonitoring = (DestinationPackageMonitoring)popupView.ObjectSpace.GetObject(_obj2);
                            if (_locDestPackMonitoring != null && _locDestPackMonitoring.Select == true && _locDestPackMonitoring.PostedCount != 4)
                            {
                                _locDestPackMonitoring.Routing = _locRouting;
                            }
                        }
                    }
                }
                popupView.ObjectSpace.CommitChanges();
            }
            catch (ValidationException)
            {
            }
        }
    }
}
