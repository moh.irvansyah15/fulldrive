﻿#region Default

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

#endregion Default

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class OutputProductionActionController : ViewController
    {
        #region Default

        private ChoiceActionItem _selectionListviewFilter;
        public OutputProductionActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            OutputProductionFilterAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                OutputProductionFilterAction.Items.Add(_selectionListviewFilter);
            }
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        #endregion Default

        private void OutputProductionProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        OutputProduction _locOutputProductionOS = (OutputProduction)_objectSpace.GetObject(obj);

                        if (_locOutputProductionOS != null)
                        {
                            if (_locOutputProductionOS.Code != null)
                            {
                                _currObjectId = _locOutputProductionOS.Code;

                                OutputProduction _locOutputProductionXPO = _currSession.FindObject<OutputProduction>
                                                                           (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Code", _currObjectId)));

                                if (_locOutputProductionXPO != null)
                                {
                                    if (_locOutputProductionXPO.Status == Status.Open)
                                    {
                                        _locOutputProductionXPO.Status = Status.Progress;
                                        _locOutputProductionXPO.StatusDate = now;
                                        _locOutputProductionXPO.Save();
                                        _locOutputProductionXPO.Session.CommitTransaction();

                                        XPCollection<OutputProductionLine> _locOutputProductionLines = new XPCollection<OutputProductionLine>
                                                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                        new BinaryOperator("OutputProduction", _locOutputProductionXPO)));

                                        if (_locOutputProductionLines != null && _locOutputProductionLines.Count > 0)
                                        {
                                            foreach (OutputProductionLine _locOutputProductionLine in _locOutputProductionLines)
                                            {
                                                if (_locOutputProductionLine.Status == Status.Open)
                                                {
                                                    _locOutputProductionLine.Status = Status.Progress;
                                                    _locOutputProductionLine.StatusDate = now;
                                                    _locOutputProductionLine.Save();
                                                    _locOutputProductionLine.Session.CommitTransaction();
                                                }
                                            }
                                        }
                                        SuccessMessageShow(_locOutputProductionXPO.Code + " has been change successfully to Progress");
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data OutputProduction Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data OutputProduction Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = OutputProduction " + ex.ToString());
            }
        }

        private void OutputProductionPostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        OutputProduction _locOutputProductionOS = (OutputProduction)_objectSpace.GetObject(obj);

                        if (_locOutputProductionOS != null)
                        {
                            if (_locOutputProductionOS.Code != null)
                            {
                                _currObjectId = _locOutputProductionOS.Code;

                                OutputProduction _locOutputProductionXPO = _currSession.FindObject<OutputProduction>
                                                                           (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Code", _currObjectId)));

                                if (_locOutputProductionXPO != null)
                                {
                                    if (_locOutputProductionXPO.Status == Status.Progress || _locOutputProductionXPO.Status == Status.Posted)
                                    {
                                        SetPostingQty(_currSession, _locOutputProductionXPO);
                                        SetRemainQty(_currSession, _locOutputProductionXPO);
                                        SetFinishProduction(_currSession, _locOutputProductionXPO);
                                        SetOutputJournal(_currSession, _locOutputProductionXPO);
                                        SetProcessCountForPostingFinishProd(_currSession, _locOutputProductionXPO);
                                        SetStatusOutProdLine(_currSession, _locOutputProductionXPO);
                                        SetNormalQty(_currSession, _locOutputProductionXPO);
                                        SetStatusOutProd(_currSession, _locOutputProductionXPO);
                                        SuccessMessageShow(_locOutputProductionXPO.Code + " has been change successfully to Posted");
                                    }
                                    else
                                    {
                                        ErrorMessageShow("Data OutputProduction Not Available");
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data OutputProduction Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data OutputProduction Not Available");
                            }
                        }
                        else
                        {
                            ErrorMessageShow("Data OutputProduction Not Available");
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = OutputProduction " + ex.ToString());
            }
        }

        private void OutputProductionFilterAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(OutputProduction)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = OutputProduction " + ex.ToString());
            }
        }

        //==== Code Only =====

        #region PostingforFinishProduction

        //Menentukan sisa dari transaksi
        private void SetRemainQty(Session _currSession, OutputProduction _locOutputProductionXPO)
        {
            try
            {
                if (_locOutputProductionXPO != null)
                {
                    XPCollection<OutputProductionLine> _locOutputProductionLines = new XPCollection<OutputProductionLine>(_currSession,
                                                                                   new GroupOperator(GroupOperatorType.And,
                                                                                   new BinaryOperator("OutputProduction", _locOutputProductionXPO),
                                                                                   new BinaryOperator("Select", true)));

                    if (_locOutputProductionLines != null && _locOutputProductionLines.Count > 0)
                    {
                        double _locRmDQty = 0;
                        double _locRmQty = 0;
                        double _locInvLineTotal = 0;
                        ItemUnitOfMeasure _locItemUOM = null;

                        foreach (OutputProductionLine _locOutputProductionLine in _locOutputProductionLines)
                        {
                            #region ProcessCount=0
                            if (_locOutputProductionLine.ProcessCount == 0)
                            {
                                #region MaxQuantity
                                if (_locOutputProductionLine.MxDQty > 0 || _locOutputProductionLine.MxQty > 0)
                                {
                                    if (_locOutputProductionLine.DQty > 0 && _locOutputProductionLine.DQty <= _locOutputProductionLine.MxDQty)
                                    {
                                        _locRmDQty = _locOutputProductionLine.MxDQty - _locOutputProductionLine.DQty;
                                    }

                                    if (_locOutputProductionLine.Qty > 0 && _locOutputProductionLine.Qty <= _locOutputProductionLine.MxQty)
                                    {
                                        _locRmQty = _locOutputProductionLine.MxQty - _locOutputProductionLine.Qty;
                                    }

                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                  (new GroupOperator(GroupOperatorType.And,
                                                   new BinaryOperator("Item", _locOutputProductionLine.Item),
                                                   new BinaryOperator("UOM", _locOutputProductionLine.MxUOM),
                                                   new BinaryOperator("DefaultUOM", _locOutputProductionLine.MxDUOM),
                                                   new BinaryOperator("Active", true)));

                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty + _locRmDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }

                                }
                                #endregion MaxQuantity

                                #region NonMaxQuantity
                                else
                                {
                                    if (_locOutputProductionLine.DQty > 0)
                                    {
                                        _locRmDQty = _locOutputProductionLine.DQty;
                                    }

                                    if (_locOutputProductionLine.Qty > 0)
                                    {
                                        _locRmQty = _locOutputProductionLine.Qty;
                                    }

                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                  (new GroupOperator(GroupOperatorType.And,
                                                   new BinaryOperator("Item", _locOutputProductionLine.Item),
                                                   new BinaryOperator("UOM", _locOutputProductionLine.UOM),
                                                   new BinaryOperator("DefaultUOM", _locOutputProductionLine.DUOM),
                                                   new BinaryOperator("Active", true)));

                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty + _locRmDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion ProcessCount=0

                            #region ProcessCount>0
                            if (_locOutputProductionLine.ProcessCount > 0)
                            {
                                if (_locOutputProductionLine.RmDQty > 0)
                                {
                                    _locRmDQty = _locOutputProductionLine.RmDQty - _locOutputProductionLine.DQty;
                                }

                                if (_locOutputProductionLine.RmQty > 0)
                                {
                                    _locRmQty = _locOutputProductionLine.RmQty - _locOutputProductionLine.Qty;
                                }

                                if (_locOutputProductionLine.MxDQty > 0 || _locOutputProductionLine.MxQty > 0)
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                  (new GroupOperator(GroupOperatorType.And,
                                                   new BinaryOperator("Item", _locOutputProductionLine.Item),
                                                   new BinaryOperator("UOM", _locOutputProductionLine.MxUOM),
                                                   new BinaryOperator("DefaultUOM", _locOutputProductionLine.MxDUOM),
                                                   new BinaryOperator("Active", true)));
                                }
                                else
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                  (new GroupOperator(GroupOperatorType.And,
                                                   new BinaryOperator("Item", _locOutputProductionLine.Item),
                                                   new BinaryOperator("UOM", _locOutputProductionLine.UOM),
                                                   new BinaryOperator("DefaultUOM", _locOutputProductionLine.DUOM),
                                                   new BinaryOperator("Active", true)));
                                }

                                if (_locItemUOM != null)
                                {
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locRmQty + _locRmDQty;
                                }

                            }
                            #endregion ProcessCount>0

                            _locOutputProductionLine.RmDQty = _locRmDQty;
                            _locOutputProductionLine.RmQty = _locRmQty;
                            _locOutputProductionLine.RmTQty = _locInvLineTotal;
                            _locOutputProductionLine.Save();
                            _locOutputProductionLine.Session.CommitTransaction();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = OutputProduction " + ex.ToString());
            }
        }

        //Menentukan jumlah quantity yang di posting
        private void SetPostingQty(Session _currSession, OutputProduction _locOutputProductionXPO)
        {
            try
            {
                if (_locOutputProductionXPO != null)
                {
                    XPCollection<OutputProductionLine> _locOutputProductionLines = new XPCollection<OutputProductionLine>(_currSession,
                                                                                   new GroupOperator(GroupOperatorType.And,
                                                                                   new BinaryOperator("OutputProduction", _locOutputProductionXPO),
                                                                                   new BinaryOperator("Select", true)));

                    if (_locOutputProductionLines != null && _locOutputProductionLines.Count > 0)
                    {
                        double _locPDQty = 0;
                        double _locPQty = 0;
                        double _locInvLineTotal = 0;
                        ItemUnitOfMeasure _locItemUOM = null;

                        foreach (OutputProductionLine _locOutputProductionLine in _locOutputProductionLines)
                        {
                            #region ProcessCount=0
                            if (_locOutputProductionLine.ProcessCount == 0)
                            {
                                #region MaxQuantity
                                if (_locOutputProductionLine.MxDQty > 0 || _locOutputProductionLine.MxQty > 0)
                                {
                                    if (_locOutputProductionLine.DQty > 0 && _locOutputProductionLine.DQty <= _locOutputProductionLine.MxDQty)
                                    {
                                        _locPDQty = _locOutputProductionLine.DQty;
                                    }

                                    if (_locOutputProductionLine.Qty > 0 && _locOutputProductionLine.Qty <= _locOutputProductionLine.MxQty)
                                    {
                                        _locPQty = _locOutputProductionLine.Qty;
                                    }

                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                  (new GroupOperator(GroupOperatorType.And,
                                                   new BinaryOperator("Item", _locOutputProductionLine.Item),
                                                   new BinaryOperator("UOM", _locOutputProductionLine.MxUOM),
                                                   new BinaryOperator("DefaultUOM", _locOutputProductionLine.MxDUOM),
                                                   new BinaryOperator("Active", true)));

                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty + _locPDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }

                                }
                                #endregion MaxQuantity

                                #region NonMaxQuantity
                                else
                                {
                                    if (_locOutputProductionLine.DQty > 0)
                                    {
                                        _locPDQty = _locOutputProductionLine.DQty;
                                    }

                                    if (_locOutputProductionLine.Qty > 0)
                                    {
                                        _locPQty = _locOutputProductionLine.Qty;
                                    }

                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                  (new GroupOperator(GroupOperatorType.And,
                                                   new BinaryOperator("Item", _locOutputProductionLine.Item),
                                                   new BinaryOperator("UOM", _locOutputProductionLine.UOM),
                                                   new BinaryOperator("DefaultUOM", _locOutputProductionLine.DUOM),
                                                   new BinaryOperator("Active", true)));

                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty + _locPDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion ProcessCount=0

                            #region ProcessCount>0
                            if (_locOutputProductionLine.ProcessCount > 0)
                            {
                                if (_locOutputProductionLine.PDQty > 0)
                                {
                                    _locPDQty = _locOutputProductionLine.PDQty + _locOutputProductionLine.DQty;
                                }

                                if (_locOutputProductionLine.PQty > 0)
                                {
                                    _locPQty = _locOutputProductionLine.PQty + _locOutputProductionLine.Qty;
                                }

                                if (_locOutputProductionLine.MxDQty > 0 || _locOutputProductionLine.MxQty > 0)
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                  (new GroupOperator(GroupOperatorType.And,
                                                   new BinaryOperator("Item", _locOutputProductionLine.Item),
                                                   new BinaryOperator("UOM", _locOutputProductionLine.MxUOM),
                                                   new BinaryOperator("DefaultUOM", _locOutputProductionLine.MxDUOM),
                                                   new BinaryOperator("Active", true)));
                                }
                                else
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                  (new GroupOperator(GroupOperatorType.And,
                                                   new BinaryOperator("Item", _locOutputProductionLine.Item),
                                                   new BinaryOperator("UOM", _locOutputProductionLine.UOM),
                                                   new BinaryOperator("DefaultUOM", _locOutputProductionLine.DUOM),
                                                   new BinaryOperator("Active", true)));
                                }

                                if (_locItemUOM != null)
                                {
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locPQty + _locPDQty;
                                }

                            }
                            #endregion ProcessCount>0

                            _locOutputProductionLine.PDQty = _locPDQty;
                            _locOutputProductionLine.PDUOM = _locOutputProductionLine.DUOM;
                            _locOutputProductionLine.PQty = _locPQty;
                            _locOutputProductionLine.PUOM = _locOutputProductionLine.UOM;
                            _locOutputProductionLine.PTQty = _locInvLineTotal;
                            _locOutputProductionLine.Save();
                            _locOutputProductionLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = WorkOrder " + ex.ToString());
            }
        }

        //Membuat finishproduction
        private void SetFinishProduction(Session _currSession, OutputProduction _locOutputProductionXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _currSignCode = null;

                if (_locOutputProductionXPO != null)
                {
                    _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.FinishProduction);

                    if (_currSignCode != null)
                    {
                        FinishProduction _saveDataFinishProd = new FinishProduction(_currSession)
                        {
                            SignCode = _currSignCode,
                            Name = _locOutputProductionXPO.Name,
                            Description = _locOutputProductionXPO.Description,
                            StartDate = _locOutputProductionXPO.StartDate,
                            EndDate = _locOutputProductionXPO.EndDate,
                            EstimatedDate = _locOutputProductionXPO.EstimatedDate,
                            StatusDate = _locOutputProductionXPO.StatusDate,
                            OutputProduction = _locOutputProductionXPO,
                            Consumption = _locOutputProductionXPO.Consumption,
                            MachineMapVersion = _locOutputProductionXPO.MachineMapVersion,
                            MachineCost = _locOutputProductionXPO.MachineCost,
                            Production = _locOutputProductionXPO.Production,
                            WorkOrder = _locOutputProductionXPO.WorkOrder,
                            Company = _locOutputProductionXPO.Company,
                        };
                        _saveDataFinishProd.Save();
                        _saveDataFinishProd.Session.CommitTransaction();
                    }
                }

                XPCollection<OutputProductionLine> _numLineOutProdLines = new XPCollection<OutputProductionLine>(_currSession,
                                                                            new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("OutputProduction", _locOutputProductionXPO),
                                                                            new BinaryOperator("Select", true)));

                if (_numLineOutProdLines != null && _numLineOutProdLines.Count > 0)
                {
                    FinishProduction _locFinishProd2 = _currSession.FindObject<FinishProduction>
                                                    (new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("OutputProduction", _locOutputProductionXPO)));

                    if (_locFinishProd2 != null)
                    {
                        if (_numLineOutProdLines != null && _numLineOutProdLines.Count > 0)
                        {
                            foreach (OutputProductionLine _numLineOutProdLine in _numLineOutProdLines)
                            {
                                if (_numLineOutProdLine.Status == Status.Progress || _numLineOutProdLine.Status == Status.Posted)
                                {
                                    FinishProductionLine _saveDataFinishProdLine = new FinishProductionLine(_currSession)
                                    {
                                        Name = _numLineOutProdLine.Name,
                                        Item = _numLineOutProdLine.Item,
                                        Location = _numLineOutProdLine.Location,
                                        BinLocation = _numLineOutProdLine.BinLocation,
                                        Description = _numLineOutProdLine.Description,
                                        MxDQty = _numLineOutProdLine.DQty,
                                        MxDUOM = _numLineOutProdLine.DUOM,
                                        MxQty = _numLineOutProdLine.Qty,
                                        MxUOM = _numLineOutProdLine.UOM,
                                        MxTQty = _numLineOutProdLine.TQty,
                                        DQty = _numLineOutProdLine.DQty,
                                        DUOM = _numLineOutProdLine.DUOM,
                                        Qty = _numLineOutProdLine.Qty,
                                        UOM = _numLineOutProdLine.UOM,
                                        TQty = _numLineOutProdLine.TQty,
                                        ADUOM = _numLineOutProdLine.DUOM,
                                        AUOM = _numLineOutProdLine.UOM,
                                        BillOfMaterial = _numLineOutProdLine.BillOfMaterial,
                                        BillOfMaterialVersion = _numLineOutProdLine.BillOfMaterialVersion,
                                        Company = _numLineOutProdLine.Company,
                                        FinishProduction = _locFinishProd2,
                                    };
                                    _saveDataFinishProdLine.Save();
                                    _saveDataFinishProdLine.Session.CommitTransaction();
                                }
                            }
                        }
                    }
                    else
                    {
                        ErrorMessageShow("Data FinishProduction Not Available");
                    }
                }
                else
                {
                    ErrorMessageShow("Data OutputProductionLine Not Available");
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = OutputProduction ", ex.ToString());
            }
        }

        //Membuat output journal
        private void SetOutputJournal(Session _currSession, OutputProduction _locOutputProductionXPO)
        {
            try
            {
                XPCollection<OutputProductionLine> _locOutLines = new XPCollection<OutputProductionLine>(_currSession,
                                                                  new GroupOperator(GroupOperatorType.And,
                                                                  new BinaryOperator("OutputProduction", _locOutputProductionXPO),
                                                                  new BinaryOperator("Select", true)));

                if (_locOutLines != null && _locOutLines.Count > 0)
                {
                    double _locOutLineTotal = 0;
                    double _locOutLineTotalActual = 0;
                    DateTime now = DateTime.Now;

                    foreach (OutputProductionLine _locOutLine in _locOutLines)
                    {
                        if (_locOutLine.Status == Status.Progress)
                        {
                            if (_locOutLine.DQty > 0 || _locOutLine.Qty > 0)
                            {
                                if (_locOutLine.Item != null && _locOutLine.UOM != null && _locOutLine.DUOM != null)
                                {
                                    ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Item", _locOutLine.Item),
                                                                     new BinaryOperator("UOM", _locOutLine.UOM),
                                                                     new BinaryOperator("DefaultUOM", _locOutLine.DUOM),
                                                                     new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        #region Output
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locOutLineTotal = _locOutLine.Qty * _locItemUOM.DefaultConversion + _locOutLine.DQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locOutLineTotal = _locOutLine.Qty / _locItemUOM.Conversion + _locOutLine.DQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locOutLineTotal = _locOutLine.Qty + _locOutLine.DQty;
                                        }
                                        #endregion Output

                                        #region Actual
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locOutLineTotalActual = _locOutLine.AQty * _locItemUOM.DefaultConversion + _locOutLine.ADQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locOutLineTotalActual = _locOutLine.AQty / _locItemUOM.Conversion + _locOutLine.ADQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locOutLineTotalActual = _locOutLine.AQty + _locOutLine.ADQty;
                                        }
                                        #endregion Actual

                                        OutputProductionJournal _locOutJournal = new OutputProductionJournal(_currSession)
                                        {
                                            Location = _locOutLine.Location,
                                            BinLocation = _locOutLine.BinLocation,
                                            Item = _locOutLine.Item,
                                            Company = _locOutLine.Company,
                                            QtyOut = _locOutLineTotal,
                                            QtyActual = _locOutLineTotalActual,
                                            QtyReturn = _locOutLineTotal - _locOutLineTotalActual,
                                            DUOM = _locOutLine.DUOM,
                                            JournalDate = now,
                                            OutputProduction = _locOutputProductionXPO,
                                        };
                                        _locOutJournal.Save();
                                        _locOutJournal.Session.CommitTransaction();

                                    }
                                }
                                else
                                {
                                    _locOutLineTotal = _locOutLine.Qty + _locOutLine.DQty;
                                    _locOutLineTotalActual = _locOutLine.AQty + _locOutLine.ADQty;

                                    OutputProductionJournal _locOutJournal = new OutputProductionJournal(_currSession)
                                    {
                                        Location = _locOutLine.Location,
                                        BinLocation = _locOutLine.BinLocation,
                                        Item = _locOutLine.Item,
                                        Company = _locOutLine.Company,
                                        QtyOut = _locOutLineTotal,
                                        QtyActual = _locOutLineTotalActual,
                                        QtyReturn = _locOutLineTotal - _locOutLineTotalActual,
                                        DUOM = _locOutLine.DUOM,
                                        JournalDate = now,
                                        OutputProduction = _locOutputProductionXPO,
                                    };
                                    _locOutJournal.Save();
                                    _locOutJournal.Session.CommitTransaction();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = OutputProduction ", ex.ToString());
            }
        }

        //Menentukan banyak proses posting
        private void SetProcessCountForPostingFinishProd(Session _currSession, OutputProduction _locOutputProductionXPO)
        {
            try
            {
                if (_locOutputProductionXPO != null)
                {
                    XPCollection<OutputProductionLine> _locOutProdLines = new XPCollection<OutputProductionLine>(_currSession,
                                                                          new GroupOperator(GroupOperatorType.And,
                                                                          new BinaryOperator("OutputProduction", _locOutputProductionXPO),
                                                                          new BinaryOperator("Select", true)));

                    if (_locOutProdLines != null && _locOutProdLines.Count > 0)
                    {
                        foreach (OutputProductionLine _locOutProdLine in _locOutProdLines)
                        {
                            if (_locOutProdLine.Status == Status.Progress || _locOutProdLine.Status == Status.Posted)
                            {
                                _locOutProdLine.ProcessCount = _locOutProdLine.ProcessCount + 1;
                                _locOutProdLine.Save();
                                _locOutProdLine.Session.CommitTransaction();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = OutputProduction " + ex.ToString());
            }
        }

        //Menentukan status pada outputproductionline
        private void SetStatusOutProdLine(Session _currSession, OutputProduction _locOutputProductionXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locOutputProductionXPO != null)
                {
                    XPCollection<OutputProductionLine> _locOutProdLines = new XPCollection<OutputProductionLine>(_currSession,
                                                                          new GroupOperator(GroupOperatorType.And,
                                                                          new BinaryOperator("OutputProduction", _locOutputProductionXPO),
                                                                          new BinaryOperator("Select", true)));

                    if (_locOutProdLines != null && _locOutProdLines.Count > 0)
                    {
                        foreach (OutputProductionLine _locOutProdLine in _locOutProdLines)
                        {
                            if (_locOutProdLine.Status == Status.Progress || _locOutProdLine.Status == Status.Posted)
                            {
                                if (_locOutProdLine.DQty > 0 || _locOutProdLine.Qty > 0)
                                {
                                    if (_locOutProdLine.RmDQty == 0 && _locOutProdLine.RmQty == 0 && _locOutProdLine.RmTQty == 0)
                                    {
                                        _locOutProdLine.Status = Status.Close;
                                        _locOutProdLine.ActivationPosting = true;
                                        _locOutProdLine.StatusDate = now;
                                    }
                                    else
                                    {
                                        _locOutProdLine.Status = Status.Posted;
                                        _locOutProdLine.StatusDate = now;
                                    }
                                    _locOutProdLine.Save();
                                    _locOutProdLine.Session.CommitTransaction();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = OutputProduction " + ex.ToString());
            }
        }

        //Menormalkan Quantity
        private void SetNormalQty(Session _currSession, OutputProduction _locOutputProductionXPO)
        {
            try
            {
                if (_locOutputProductionXPO != null)
                {
                    XPCollection<OutputProductionLine> _locOutProdLines = new XPCollection<OutputProductionLine>(_currSession,
                                                                          new GroupOperator(GroupOperatorType.And,
                                                                          new BinaryOperator("OutputProduction", _locOutputProductionXPO),
                                                                          new BinaryOperator("Select", true)));

                    if (_locOutProdLines != null && _locOutProdLines.Count > 0)
                    {
                        foreach (OutputProductionLine _locOutProdLine in _locOutProdLines)
                        {
                            if (_locOutProdLine.Status == Status.Progress || _locOutProdLine.Status == Status.Posted || _locOutProdLine.Status == Status.Close)
                            {
                                if (_locOutProdLine.DQty > 0 || _locOutProdLine.Qty > 0)
                                {
                                    _locOutProdLine.Select = false;
                                    _locOutProdLine.DQty = 0;
                                    _locOutProdLine.Qty = 0;
                                    _locOutProdLine.Save();
                                    _locOutProdLine.Session.CommitTransaction();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = OutputProduction " + ex.ToString());
            }
        }

        //Menentukan status pada outputproduction
        private void SetStatusOutProd(Session _currSession, OutputProduction _locOutputProductionXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locCountStatus = 0;
                bool _locActivationPosting = false;

                if (_locOutputProductionXPO != null)
                {
                    XPCollection<OutputProductionLine> _locOutProdLines = new XPCollection<OutputProductionLine>
                                                                          (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                           new BinaryOperator("OutputProduction", _locOutputProductionXPO)));

                    if (_locOutProdLines != null && _locOutProdLines.Count() > 0)
                    {
                        foreach (OutputProductionLine _locOutProdLine in _locOutProdLines)
                        {
                            if (_locOutProdLine.Status != Status.Close)
                            {
                                _locCountStatus = _locCountStatus + 1;
                            }
                        }
                    }

                    if (_locCountStatus > 0)
                    {
                        if (_locOutProdLines.Count() == _locCountStatus)
                        {
                            _locActivationPosting = true;
                        }
                    }

                    _locOutputProductionXPO.Status = Status.Posted;
                    _locActivationPosting = true;
                    _locOutputProductionXPO.StatusDate = now;
                    _locOutputProductionXPO.ActivationPosting = _locActivationPosting;
                    _locOutputProductionXPO.Save();
                    _locOutputProductionXPO.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = OutputProduction " + ex.ToString());
            }
        }

        #endregion PostingforFinishProduction

        #region Global Method

        private void SuccessMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 4000;
            options.Message = string.Format("{0}", _locMessage);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method

    }
}
