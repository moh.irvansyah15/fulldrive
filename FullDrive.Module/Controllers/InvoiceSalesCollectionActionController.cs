﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using System.Collections;
using DevExpress.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class InvoiceSalesCollectionActionController : ViewController
    {
        public InvoiceSalesCollectionActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void InvoiceSalesCollectionShowITOMAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
            var ListView = View as ListView;
            IObjectSpace os = Application.CreateObjectSpace();
            string listViewId = Application.FindListViewId(typeof(InventoryTransferOutMonitoring));
            CollectionSourceBase cs = Application.CreateCollectionSource(os, typeof(InventoryTransferOutMonitoring), listViewId);
            //Tinggal buat 
            ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
            string _stringITOM = null;
            if (_objectsToProcess != null)
            {
                foreach (Object obj in _objectsToProcess)
                {
                    InvoiceSalesCollection _locInvoiceSalesCollection = (InvoiceSalesCollection)_objectSpace.GetObject(obj);
                    if (_locInvoiceSalesCollection != null)
                    {
                        if (_locInvoiceSalesCollection.InventoryTransferOut != null)
                        {
                            if (_stringITOM == null)
                            {
                                if (_locInvoiceSalesCollection.InventoryTransferOut.Code != null)
                                {
                                    _stringITOM = "( [InventoryTransferOut.Code]=='" + _locInvoiceSalesCollection.InventoryTransferOut.Code + "' AND [PostedCount] == 0 )";
                                }

                            }
                            else
                            {
                                if (_locInvoiceSalesCollection.InventoryTransferOut.Code != null)
                                {
                                    _stringITOM = _stringITOM + " OR ( [InventoryTransferOut.Code]=='" + _locInvoiceSalesCollection.InventoryTransferOut.Code + "' AND [PostedCount] == 0 )";
                                }
                            }
                        }
                    }
                }
            }
            if (_stringITOM != null)
            {
                cs.Criteria["InvoiceSalesCollectionFilterITO"] = CriteriaOperator.Parse(_stringITOM);
            }
            e.ShowViewParameters.CreatedView = Application.CreateListView(
                listViewId,
                cs,
                true
            );
            e.ShowViewParameters.Context = TemplateContext.View;
            e.ShowViewParameters.TargetWindow = TargetWindow.NewModalWindow;
            DialogController dc = Application.CreateController<DialogController>();
            dc.Accepting += new EventHandler<DialogControllerAcceptingEventArgs>(dc_Accepting);
            e.ShowViewParameters.Controllers.Add(dc);
        }

        void dc_Accepting(object sender, DialogControllerAcceptingEventArgs e)
        {
            try
            {
                SalesInvoice _locSalesInvoice = null;
                View popupView = ((Controller)sender).Frame.View;
                ArrayList _objectsToProcess = new ArrayList(View.SelectedObjects);
                if (_objectsToProcess != null)
                {
                    foreach (Object _obj in _objectsToProcess)
                    {
                        InvoiceSalesCollection _locInvSalesCollection = (InvoiceSalesCollection)popupView.ObjectSpace.GetObject(_obj);
                        if (_locInvSalesCollection != null)
                        {
                            if (_locInvSalesCollection.SalesInvoice != null)
                            {
                                _locSalesInvoice = _locInvSalesCollection.SalesInvoice;
                            }
                        }
                    }
                }
                if (_locSalesInvoice != null)
                {
                    ArrayList _objectsToProcess2 = new ArrayList(popupView.SelectedObjects);
                    if (_objectsToProcess2 != null)
                    {
                        foreach (Object _obj2 in _objectsToProcess2)
                        {
                            InventoryTransferOutMonitoring _locInvTransOutMonitoring = (InventoryTransferOutMonitoring)popupView.ObjectSpace.GetObject(_obj2);
                            if (_locInvTransOutMonitoring != null && _locInvTransOutMonitoring.Select == true && _locInvTransOutMonitoring.PostedCount == 0)
                            {
                                _locInvTransOutMonitoring.SalesInvoice = _locSalesInvoice;
                            }
                        }
                    }
                }
                popupView.ObjectSpace.CommitChanges();
            }
            catch (ValidationException)
            {
            }
        }
        
    }
}
