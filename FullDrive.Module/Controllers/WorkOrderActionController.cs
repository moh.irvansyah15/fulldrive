﻿#region Default

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

#endregion Default

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class WorkOrderActionController : ViewController
    {
        #region Default

        private ChoiceActionItem _selectionListviewFilter;
        public WorkOrderActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.

            #region FilterStatus
            WorkOrderListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                WorkOrderListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
            #endregion FilterStatus

            #region FilterApproval
            WorkOrderListviewFilterApprovalSelectionAction.Items.Clear();
            foreach (object _currApprovalFilter in Enum.GetValues(typeof(CustomProcess.ApprovalFilter)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.ApprovalFilter));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApprovalFilter), _currApprovalFilter);
                WorkOrderListviewFilterApprovalSelectionAction.Items.Add(_selectionListviewFilter);
            }
            #endregion FilterApproval

        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        #endregion Default

        private void WorkOrderProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        WorkOrder _locWorkOrderOS = (WorkOrder)_objectSpace.GetObject(obj);

                        if (_locWorkOrderOS != null)
                        {
                            if (_locWorkOrderOS.Code != null)
                            {
                                _currObjectId = _locWorkOrderOS.Code;

                                WorkOrder _locWorkOrderXPO = _currSession.FindObject<WorkOrder>
                                                             (new GroupOperator(GroupOperatorType.And,
                                                              new BinaryOperator("Code", _currObjectId)));

                                if (_locWorkOrderXPO != null)
                                {
                                    if (_locWorkOrderXPO.Status == Status.Open || _locWorkOrderXPO.Status == Status.Progress)
                                    {
                                        if (_locWorkOrderXPO.Status == Status.Open)
                                        {
                                            _locWorkOrderXPO.Status = Status.Progress;
                                            _locWorkOrderXPO.StatusDate = now;
                                            _locWorkOrderXPO.Save();
                                            _locWorkOrderXPO.Session.CommitTransaction();
                                        }

                                        XPCollection<WorkOrderLine> _locWorkOrderLines = new XPCollection<WorkOrderLine>
                                                                                         (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                          new BinaryOperator("WorkOrder", _locWorkOrderXPO),
                                                                                          new BinaryOperator("Status", Status.Open)));

                                        if (_locWorkOrderLines != null && _locWorkOrderLines.Count > 0)
                                        {
                                            foreach (WorkOrderLine _locWorkOrderLine in _locWorkOrderLines)
                                            {
                                                _locWorkOrderLine.Status = Status.Progress;
                                                _locWorkOrderLine.StatusDate = now;
                                                _locWorkOrderLine.Save();
                                                _locWorkOrderLine.Session.CommitTransaction();
                                            }
                                        }

                                    }

                                    SuccessMessageShow("WorkOrder has successfully updated to progress");
                                }
                                else
                                {
                                    ErrorMessageShow("Data WorkOrder Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data WorkOrder Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = WorkOrder " + ex.ToString());
            }
        }

        private void WorkOrderGetWRAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        WorkOrder _locWorkOrderOS = (WorkOrder)_objectSpace.GetObject(obj);

                        if (_locWorkOrderOS != null)
                        {
                            if (_locWorkOrderOS.Code != null)
                            {
                                _currObjectId = _locWorkOrderOS.Code;

                                WorkOrder _locWorkOrderXPO = _currSession.FindObject<WorkOrder>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locWorkOrderXPO != null)
                                {
                                    if (_locWorkOrderXPO.Status == Status.Open || _locWorkOrderXPO.Status == Status.Progress)
                                    {
                                        if (_locWorkOrderXPO.Collection == true)
                                        {
                                            XPCollection<WorkRequisitionCollection> _locWorkRequisitionCollections = new XPCollection<WorkRequisitionCollection>
                                                                                                                     (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                      new BinaryOperator("WorkOrder", _locWorkOrderXPO)));

                                            if (_locWorkRequisitionCollections != null && _locWorkRequisitionCollections.Count() > 0)
                                            {
                                                foreach (WorkRequisitionCollection _locWorkRequisitionCollection in _locWorkRequisitionCollections)
                                                {
                                                    if (_locWorkRequisitionCollection.WorkRequisition != null &&
                                                        (_locWorkRequisitionCollection.Status == Status.Open || _locWorkRequisitionCollection.Status == Status.Progress))
                                                    {
                                                        if (_locWorkRequisitionCollection.WorkRequisition.Code != null)
                                                        {
                                                            WorkRequisition _locWorkRequisition = _currSession.FindObject<WorkRequisition>
                                                                                                  (new GroupOperator(GroupOperatorType.And,
                                                                                                   new BinaryOperator("Code", _locWorkRequisitionCollection.WorkRequisition.Code)));

                                                            if (_locWorkRequisition != null)
                                                            {
                                                                XPCollection<WorkRequisitionLine> _locWorkRequisitionLines = new XPCollection<WorkRequisitionLine>
                                                                                                                             (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                              new BinaryOperator("WorkRequisition", _locWorkRequisition),
                                                                                                                              new BinaryOperator("Select", true)));

                                                                if (_locWorkRequisitionLines != null && _locWorkRequisitionLines.Count() > 0)
                                                                {
                                                                    foreach (WorkRequisitionLine _locWorkRequisitionLine in _locWorkRequisitionLines)
                                                                    {
                                                                        if (_locWorkRequisitionLine.Status == Status.Progress || _locWorkRequisitionLine.Status == Status.Posted)
                                                                        {
                                                                            WorkOrderLine _loWorkOrderLine = _currSession.FindObject<WorkOrderLine>
                                                                                                             (new GroupOperator(GroupOperatorType.And,
                                                                                                              new BinaryOperator("WorkOrder", _locWorkOrderXPO),
                                                                                                              new BinaryOperator("Item", _locWorkRequisitionLine.Item)));

                                                                            if (_loWorkOrderLine != null)
                                                                            {

                                                                                _loWorkOrderLine.MxDQty = _loWorkOrderLine.MxDQty + _locWorkRequisitionLine.DQty;
                                                                                _loWorkOrderLine.MxQty = _loWorkOrderLine.MxQty + _locWorkRequisitionLine.Qty;
                                                                                _loWorkOrderLine.MxTQty = _loWorkOrderLine.MxTQty + _locWorkRequisitionLine.TQty;
                                                                                _loWorkOrderLine.DQty = _loWorkOrderLine.DQty + _locWorkRequisitionLine.DQty;
                                                                                _loWorkOrderLine.Qty = _loWorkOrderLine.Qty + _locWorkRequisitionLine.Qty;
                                                                                _loWorkOrderLine.TQty = _loWorkOrderLine.TQty + _locWorkRequisitionLine.TQty;

                                                                                _loWorkOrderLine.Save();
                                                                                _loWorkOrderLine.Session.CommitTransaction();

                                                                                SetRemainQuantityWorkRequisitionLine(_currSession, _locWorkRequisitionLine);
                                                                                SetPostingQtyWorkRequisitionLine(_currSession, _locWorkRequisitionLine);
                                                                                SetProcessCountWorkRequisitionLine(_currSession, _locWorkRequisitionLine);
                                                                                SetStatusWorkRequisitionLine(_currSession, _locWorkRequisitionLine);
                                                                                SetNormalQuantityWorkRequisitionLine(_currSession, _locWorkRequisitionLine);
                                                                            }
                                                                            else
                                                                            {
                                                                                WorkOrderLine _saveDataWorkOrderLine = new WorkOrderLine(_currSession)
                                                                                {
                                                                                    Item = _locWorkRequisitionLine.Item,
                                                                                    Description = _locWorkRequisitionLine.Description,
                                                                                    MxDQty = _locWorkRequisitionLine.DQty,
                                                                                    MxDUOM = _locWorkRequisitionLine.DUOM,
                                                                                    MxQty = _locWorkRequisitionLine.Qty,
                                                                                    MxUOM = _locWorkRequisitionLine.UOM,
                                                                                    MxTQty = _locWorkRequisitionLine.TQty,
                                                                                    DQty = _locWorkRequisitionLine.DQty,
                                                                                    DUOM = _locWorkRequisitionLine.DUOM,
                                                                                    Qty = _locWorkRequisitionLine.Qty,
                                                                                    UOM = _locWorkRequisitionLine.UOM,
                                                                                    TQty = _locWorkRequisitionLine.TQty,
                                                                                    WorkOrder = _locWorkOrderXPO,
                                                                                };
                                                                                _saveDataWorkOrderLine.Save();
                                                                                _saveDataWorkOrderLine.Session.CommitTransaction();

                                                                                SetRemainQuantityWorkRequisitionLine(_currSession, _locWorkRequisitionLine);
                                                                                SetPostingQtyWorkRequisitionLine(_currSession, _locWorkRequisitionLine);
                                                                                SetProcessCountWorkRequisitionLine(_currSession, _locWorkRequisitionLine);
                                                                                SetStatusWorkRequisitionLine(_currSession, _locWorkRequisitionLine);
                                                                                SetNormalQuantityWorkRequisitionLine(_currSession, _locWorkRequisitionLine);
                                                                            }
                                                                        }
                                                                    }
                                                                }

                                                                SetStatusWorkRequisition(_currSession, _locWorkRequisition);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    SuccessMessageShow("WorkRequisition Has Been Successfully Getting into Work Order");
                                }
                                else
                                {
                                    ErrorMessageShow("Data WorkOrder Not Available");
                                }

                            }
                            else
                            {
                                ErrorMessageShow("Data WorkOrder Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = WorkOrder " + ex.ToString());
            }
        }

        private void WorkOrderGetMaterialAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        WorkOrder _locWorkOrderOS = (WorkOrder)_objectSpace.GetObject(obj);

                        if (_locWorkOrderOS != null)
                        {
                            if (_locWorkOrderOS.Code != null)
                            {
                                _currObjectId = _locWorkOrderOS.Code;

                                WorkOrder _locWorkOrderXPO = _currSession.FindObject<WorkOrder>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locWorkOrderXPO != null)
                                {
                                    if (_locWorkOrderXPO.Status == Status.Progress || _locWorkOrderXPO.Status == Status.Posted)
                                    {
                                        GetWorkOrderMaterial(_currSession, _locWorkOrderXPO);
                                    }

                                    SuccessMessageShow("WorkOrder Material has been successfully Get");
                                }
                                else
                                {
                                    ErrorMessageShow("Data WorkOrderLine Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data WorkOrderLine Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = WorkOrder " + ex.ToString());
            }
        }

        private void WorkOrderPostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        WorkOrder _locWorkOrderOS = (WorkOrder)_objectSpace.GetObject(obj);

                        if (_locWorkOrderOS != null)
                        {
                            if (_locWorkOrderOS.Code != null)
                            {
                                _currObjectId = _locWorkOrderOS.Code;

                                WorkOrder _locWorkOrderXPO = _currSession.FindObject<WorkOrder>
                                                             (new GroupOperator(GroupOperatorType.And,
                                                              new BinaryOperator("Code", _currObjectId)));

                                if (_locWorkOrderXPO != null)
                                {
                                    if (_locWorkOrderXPO.Status == Status.Progress || _locWorkOrderXPO.Status == Status.Posted)
                                    {
                                        ApprovalLine _locApprovalLineXPO = _currSession.FindObject<ApprovalLine>
                                                                           (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("EndApproval", true),
                                                                            new BinaryOperator("WorkOrder", _locWorkOrderXPO)));

                                        if (_locApprovalLineXPO != null)
                                        {
                                            SetProduction(_currSession, _locWorkOrderXPO);
                                            SetRemainQty(_currSession, _locWorkOrderXPO);
                                            SetPostingQty(_currSession, _locWorkOrderXPO);
                                            SetProcessCount(_currSession, _locWorkOrderXPO);
                                            SetStatusWorkOrderLine(_currSession, _locWorkOrderXPO);
                                            SetNormalQuantity(_currSession, _locWorkOrderXPO);
                                            SetFinalStatusWorkOrder(_currSession, _locWorkOrderXPO);
                                            SuccessMessageShow("WO has successfully posted");
                                        }
                                        else
                                        {
                                            ErrorMessageShow("Data Approval Not Available");
                                        }
                                    }
                                    else
                                    {
                                        ErrorMessageShow("Data WorkOrder Not Available");
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data WorkOrder Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data WorkOrder Not Available");
                            }
                        }
                        else
                        {
                            ErrorMessageShow("Data WorkOrder Not Available");
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = WorkOrder " + ex.ToString());
            }
        }

        private void WorkOrderPostingMaterialAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        WorkOrder _locWorkOrderOS = (WorkOrder)_objectSpace.GetObject(obj);

                        if (_locWorkOrderOS != null)
                        {
                            if (_locWorkOrderOS.Code != null)
                            {
                                _currObjectId = _locWorkOrderOS.Code;

                                WorkOrder _locWorkOrderXPO = _currSession.FindObject<WorkOrder>
                                                             (new GroupOperator(GroupOperatorType.And,
                                                              new BinaryOperator("Code", _currObjectId)));

                                if (_locWorkOrderXPO != null)
                                {
                                    if (_locWorkOrderXPO.Status == Status.Progress || _locWorkOrderXPO.Status == Status.Posted)
                                    {
                                        WorkOrderMaterial _locWorkOrderMaterialXPO = _currSession.FindObject<WorkOrderMaterial>
                                                                                     (new GroupOperator(GroupOperatorType.And,
                                                                                      new BinaryOperator("Select", true),
                                                                                      new BinaryOperator("WorkOrder", _locWorkOrderXPO)));

                                        if (_locWorkOrderMaterialXPO != null)
                                        {
                                            if (_locWorkOrderMaterialXPO.Status == Status.Progress || _locWorkOrderMaterialXPO.Status == Status.Posted)
                                            {
                                                ApprovalLine _locApprovalLineXPO = _currSession.FindObject<ApprovalLine>
                                                                                   (new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("EndApproval", true),
                                                                                    new BinaryOperator("WorkOrder", _locWorkOrderXPO)));

                                                if (_locApprovalLineXPO != null)
                                                {
                                                    SetPostingQtyForPostingMaterial(_currSession, _locWorkOrderXPO);
                                                    SetRemainQtyForPostingMaterial(_currSession, _locWorkOrderXPO);
                                                    SetProcessCountForPostingMaterial(_currSession, _locWorkOrderXPO);
                                                    SetMaterialRequisitionForPostingMaterial(_currSession, _locWorkOrderXPO);
                                                    SetProductionMaterial(_currSession, _locWorkOrderXPO);
                                                    SetStatusWorkOrderMaterialForPostingMaterial(_currSession, _locWorkOrderXPO);
                                                    SetNormalQuantityForPostingMaterial(_currSession, _locWorkOrderXPO);
                                                }
                                            }
                                            else
                                            {
                                                ErrorMessageShow("Data WorkOrder Material Not Available");
                                            }
                                        }

                                        SuccessMessageShow("WorkOrder Material has been successfully post");
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data WorkOrder Material Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data WorkOrder Material Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = WorkOrder " + ex.ToString());
            }
        }

        private void WorkOrderListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(WorkOrder)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = WorkOrder " + ex.ToString());
            }
        }

        private void WorkOrderListviewFilterApprovalSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(WorkOrder)))
                {
                    if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.Approval)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("ActiveApproved3", false, BinaryOperatorType.Equal),
                                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("ActiveApproved1", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", true, BinaryOperatorType.Equal)));
                    }
                    else if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.EndApproval)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new BinaryOperator("ActiveApproved3", true, BinaryOperatorType.Equal);
                    }
                    else if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.AllData)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("ActiveApproved1", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved3", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved1", false, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", false, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved3", false, BinaryOperatorType.Equal));
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = WorkOrder " + ex.ToString());
            }
        }

        //==== Code Only ====

        #region GetWR

        //Menentukan Sisa Quantity dari Work Requisition Line
        private void SetRemainQuantityWorkRequisitionLine(Session _currSession, WorkRequisitionLine _locWorkRequisitionLine)
        {
            try
            {
                DateTime now = DateTime.Now;
                double _locRmDqty = 0;
                double _locRmQty = 0;
                double _locInvLineTotal = 0;
                ItemUnitOfMeasure _locItemUOM = null;

                if (_locWorkRequisitionLine != null)
                {
                    #region PostedCount=0
                    if (_locWorkRequisitionLine.PostedCount == 0)
                    {
                        #region MaxQuantity

                        if (_locWorkRequisitionLine.MxDQty > 0 || _locWorkRequisitionLine.MxQty > 0)
                        {
                            if (_locWorkRequisitionLine.DQty > 0 && _locWorkRequisitionLine.DQty <= _locWorkRequisitionLine.MxDQty)
                            {
                                _locRmDqty = _locWorkRequisitionLine.MxDQty - _locWorkRequisitionLine.DQty;
                            }

                            if (_locWorkRequisitionLine.Qty > 0 && _locWorkRequisitionLine.Qty <= _locWorkRequisitionLine.MxQty)
                            {
                                _locRmQty = _locWorkRequisitionLine.MxQty - _locWorkRequisitionLine.Qty;
                            }

                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", _locWorkRequisitionLine.Item),
                                                         new BinaryOperator("UOM", _locWorkRequisitionLine.MxUOM),
                                                         new BinaryOperator("DefaultUOM", _locWorkRequisitionLine.MxDUOM),
                                                         new BinaryOperator("Active", true)));
                            if (_locItemUOM != null)
                            {
                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDqty;
                                }
                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDqty;
                                }
                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty + _locRmDqty;
                                }
                            }
                            else
                            {
                                _locInvLineTotal = _locRmQty + _locRmDqty;
                            }

                        }
                        #endregion MaxQuantity

                        #region NonMaxQuantity
                        else
                        {
                            if (_locWorkRequisitionLine.DQty > 0)
                            {
                                _locRmDqty = _locWorkRequisitionLine.DQty;
                            }

                            if (_locWorkRequisitionLine.Qty > 0)
                            {
                                _locRmQty = _locWorkRequisitionLine.Qty;
                            }


                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                       (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Item", _locWorkRequisitionLine.Item),
                                                        new BinaryOperator("UOM", _locWorkRequisitionLine.UOM),
                                                        new BinaryOperator("DefaultUOM", _locWorkRequisitionLine.DUOM),
                                                        new BinaryOperator("Active", true)));
                            if (_locItemUOM != null)
                            {
                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDqty;
                                }
                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDqty;
                                }
                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty + _locRmDqty;
                                }
                            }
                            else
                            {
                                _locInvLineTotal = _locRmQty + _locRmDqty;
                            }
                        }
                        #endregion NonMaxQuantity

                    }
                    #endregion PostedCount=0

                    #region PostedCount>0
                    if (_locWorkRequisitionLine.PostedCount > 0)
                    {
                        if (_locWorkRequisitionLine.RmDQty > 0)
                        {
                            _locRmDqty = _locWorkRequisitionLine.RmDQty - _locWorkRequisitionLine.DQty;
                        }

                        if (_locWorkRequisitionLine.RmQty > 0)
                        {
                            _locRmQty = _locWorkRequisitionLine.RmQty - _locWorkRequisitionLine.Qty;
                        }

                        if (_locWorkRequisitionLine.MxDQty > 0 || _locWorkRequisitionLine.MxQty > 0)
                        {
                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                          (new GroupOperator(GroupOperatorType.And,
                                           new BinaryOperator("Item", _locWorkRequisitionLine.Item),
                                           new BinaryOperator("UOM", _locWorkRequisitionLine.MxUOM),
                                           new BinaryOperator("DefaultUOM", _locWorkRequisitionLine.MxDUOM),
                                           new BinaryOperator("Active", true)));
                        }
                        else
                        {
                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                          (new GroupOperator(GroupOperatorType.And,
                                           new BinaryOperator("Item", _locWorkRequisitionLine.Item),
                                           new BinaryOperator("UOM", _locWorkRequisitionLine.UOM),
                                           new BinaryOperator("DefaultUOM", _locWorkRequisitionLine.DUOM),
                                           new BinaryOperator("Active", true)));
                        }


                        if (_locItemUOM != null)
                        {
                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDqty;
                            }
                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDqty;
                            }
                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = _locRmQty + _locRmDqty;
                            }
                        }
                        else
                        {
                            _locInvLineTotal = _locRmQty + _locRmDqty;
                        }

                    }
                    #endregion PostedCount>0
                }
                _locWorkRequisitionLine.RmDQty = _locRmDqty;
                _locWorkRequisitionLine.RmQty = _locRmQty;
                _locWorkRequisitionLine.RmTQty = _locInvLineTotal;
                _locWorkRequisitionLine.Save();
                _locWorkRequisitionLine.Session.CommitTransaction();

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = WorkOrder " + ex.ToString());
            }
        }

        //Menentukan jumlah quantity yang di posting pd Work Requisition Line 
        private void SetPostingQtyWorkRequisitionLine(Session _currSession, WorkRequisitionLine _locWorkRequisitionLine)
        {
            try
            {
                if (_locWorkRequisitionLine != null)
                {
                    double _locPDQty = 0;
                    double _locPQty = 0;
                    double _locInvLineTotal = 0;
                    ItemUnitOfMeasure _locItemUOM = null;

                    #region ProcessCount=0
                    if (_locWorkRequisitionLine.PostedCount == 0)
                    {
                        #region MaxQuantity
                        if (_locWorkRequisitionLine.MxDQty > 0 || _locWorkRequisitionLine.MxQty > 0)
                        {
                            if (_locWorkRequisitionLine.DQty > 0 && _locWorkRequisitionLine.DQty <= _locWorkRequisitionLine.MxDQty)
                            {
                                _locPDQty = _locWorkRequisitionLine.DQty;
                            }

                            if (_locWorkRequisitionLine.Qty > 0 && _locWorkRequisitionLine.Qty <= _locWorkRequisitionLine.MxQty)
                            {
                                _locPQty = _locWorkRequisitionLine.Qty;
                            }

                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                          (new GroupOperator(GroupOperatorType.And,
                                           new BinaryOperator("Item", _locWorkRequisitionLine.Item),
                                           new BinaryOperator("UOM", _locWorkRequisitionLine.MxUOM),
                                           new BinaryOperator("DefaultUOM", _locWorkRequisitionLine.MxDUOM),
                                           new BinaryOperator("Active", true)));

                            if (_locItemUOM != null)
                            {
                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                }
                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                }
                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locPQty + _locPDQty;
                                }
                            }
                            else
                            {
                                _locInvLineTotal = _locPQty + _locPDQty;
                            }

                        }
                        #endregion MaxQuantity

                        #region NonMaxQuantity
                        else
                        {
                            if (_locWorkRequisitionLine.DQty > 0)
                            {
                                _locPDQty = _locWorkRequisitionLine.DQty;
                            }

                            if (_locWorkRequisitionLine.Qty > 0)
                            {
                                _locPQty = _locWorkRequisitionLine.Qty;
                            }
                            
                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                          (new GroupOperator(GroupOperatorType.And,
                                           new BinaryOperator("Item", _locWorkRequisitionLine.Item),
                                           new BinaryOperator("UOM", _locWorkRequisitionLine.UOM),
                                           new BinaryOperator("DefaultUOM", _locWorkRequisitionLine.DUOM),
                                           new BinaryOperator("Active", true)));

                            if (_locItemUOM != null)
                            {
                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                }
                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                }
                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locPQty + _locPDQty;
                                }
                            }
                            else
                            {
                                _locInvLineTotal = _locPQty + _locPDQty;
                            }
                        }
                        #endregion NonMaxQuantity
                    }
                    #endregion ProcessCount=0

                    #region ProcessCount>0
                    if (_locWorkRequisitionLine.PostedCount > 0)
                    {
                        if (_locWorkRequisitionLine.PDQty > 0)
                        {
                            _locPDQty = _locWorkRequisitionLine.PDQty + _locWorkRequisitionLine.DQty;
                        }

                        if (_locWorkRequisitionLine.PQty > 0)
                        {
                            _locPQty = _locWorkRequisitionLine.PQty + _locWorkRequisitionLine.Qty;
                        }

                        if (_locWorkRequisitionLine.MxDQty > 0 || _locWorkRequisitionLine.MxQty > 0)
                        {
                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                          (new GroupOperator(GroupOperatorType.And,
                                           new BinaryOperator("Item", _locWorkRequisitionLine.Item),
                                           new BinaryOperator("UOM", _locWorkRequisitionLine.MxUOM),
                                           new BinaryOperator("DefaultUOM", _locWorkRequisitionLine.MxDUOM),
                                           new BinaryOperator("Active", true)));
                        }
                        else
                        {
                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                          (new GroupOperator(GroupOperatorType.And,
                                           new BinaryOperator("Item", _locWorkRequisitionLine.Item),
                                           new BinaryOperator("UOM", _locWorkRequisitionLine.UOM),
                                           new BinaryOperator("DefaultUOM", _locWorkRequisitionLine.DUOM),
                                           new BinaryOperator("Active", true)));
                        }

                        if (_locItemUOM != null)
                        {
                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                            }
                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                            }
                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = _locPQty + _locPDQty;
                            }
                        }
                        else
                        {
                            _locInvLineTotal = _locPQty + _locPDQty;
                        }

                    }
                    #endregion ProcessCount>0

                    _locWorkRequisitionLine.PDQty = _locPDQty;
                    _locWorkRequisitionLine.PDUOM = _locWorkRequisitionLine.DUOM;
                    _locWorkRequisitionLine.PQty = _locPQty;
                    _locWorkRequisitionLine.PUOM = _locWorkRequisitionLine.UOM;
                    _locWorkRequisitionLine.PTQty = _locInvLineTotal;
                    _locWorkRequisitionLine.Save();
                    _locWorkRequisitionLine.Session.CommitTransaction();
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = WorkOrder " + ex.ToString());
            }
        }

        //Menentukan Banyak process Work Requisition Line
        private void SetProcessCountWorkRequisitionLine(Session _currSession, WorkRequisitionLine _locWorkRequisitionLine)
        {
            try
            {

                if (_locWorkRequisitionLine != null)
                {
                    if (_locWorkRequisitionLine.Status == Status.Progress || _locWorkRequisitionLine.Status == Status.Posted)
                    {
                        if (_locWorkRequisitionLine.DQty > 0 || _locWorkRequisitionLine.Qty > 0)
                        {
                            _locWorkRequisitionLine.PostedCount = _locWorkRequisitionLine.PostedCount + 1;
                            _locWorkRequisitionLine.Save();
                            _locWorkRequisitionLine.Session.CommitTransaction();
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = WorkOrder " + ex.ToString());
            }
        }

        //Menentukan Status pd Work Requisition Line
        private void SetStatusWorkRequisitionLine(Session _currSession, WorkRequisitionLine _locWorkRequisitionLine)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locWorkRequisitionLine != null)
                {
                    if (_locWorkRequisitionLine.Status == Status.Progress || _locWorkRequisitionLine.Status == Status.Posted)
                    {
                        if (_locWorkRequisitionLine.DQty > 0 || _locWorkRequisitionLine.Qty > 0)
                        {
                            if (_locWorkRequisitionLine.RmDQty == 0 && _locWorkRequisitionLine.RmQty == 0 && _locWorkRequisitionLine.RmTQty == 0)
                            {
                                _locWorkRequisitionLine.Status = Status.Close;
                                _locWorkRequisitionLine.ActivationPosting = true;
                                _locWorkRequisitionLine.StatusDate = now;
                            }
                            else
                            {
                                _locWorkRequisitionLine.Status = Status.Posted;
                                _locWorkRequisitionLine.StatusDate = now;
                            }
                            _locWorkRequisitionLine.Save();
                            _locWorkRequisitionLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = WorkOrder " + ex.ToString());
            }
        }

        //Menormalkan Quantity Work Requisition Line
        private void SetNormalQuantityWorkRequisitionLine(Session _currSession, WorkRequisitionLine _locWorkRequisitionLine)
        {
            try
            {
                if (_locWorkRequisitionLine != null)
                {
                    if (_locWorkRequisitionLine.Status == Status.Progress || _locWorkRequisitionLine.Status == Status.Posted || _locWorkRequisitionLine.Status == Status.Close)
                    {
                        if (_locWorkRequisitionLine.DQty > 0 || _locWorkRequisitionLine.Qty > 0)
                        {
                            _locWorkRequisitionLine.DQty = 0;
                            _locWorkRequisitionLine.Qty = 0;
                            _locWorkRequisitionLine.Save();
                            _locWorkRequisitionLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = WorkOrder " + ex.ToString());
            }
        }

        //Menentukan Status Issue Pada Inventory Transfer
        private void SetStatusWorkRequisition(Session _currSession, WorkRequisition _locWorkRequisition)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locCount = 0;

                if (_locWorkRequisition != null)
                {
                    XPCollection<MaterialRequisitionLine> _locMaterialRequisitionLines = new XPCollection<MaterialRequisitionLine>(_currSession,
                                                                                         new GroupOperator(GroupOperatorType.And,
                                                                                         new BinaryOperator("WorkRequisition", _locWorkRequisition)));

                    if (_locMaterialRequisitionLines != null && _locMaterialRequisitionLines.Count() > 0)
                    {
                        foreach (MaterialRequisitionLine _locMaterialRequisitionLine in _locMaterialRequisitionLines)
                        {
                            if (_locMaterialRequisitionLine.Status == Status.Close)
                            {
                                _locCount = _locCount + 1;
                            }
                        }

                        if (_locCount == _locMaterialRequisitionLines.Count())
                        {
                            _locWorkRequisition.ActivationPosting = true;
                            _locWorkRequisition.Status = Status.Close;
                            _locWorkRequisition.StatusDate = now;
                            _locWorkRequisition.Save();
                            _locWorkRequisition.Session.CommitTransaction();
                        }
                        else
                        {
                            _locWorkRequisition.Status = Status.Posted;
                            _locWorkRequisition.StatusDate = now;
                            _locWorkRequisition.Save();
                            _locWorkRequisition.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = WorkOrder " + ex.ToString());
            }
        }

        #endregion GetWR

        #region GetWOM

        private void GetWorkOrderMaterial(Session _currSession, WorkOrder _locWorkOrderXPO)
        {
            try
            {
                ItemUnitOfMeasure _locItemUOM = null;
                double _locInvLineTotal = 0;
                double _locTotQty = 0;
                double _locTotDQty = 0;

                if (_locWorkOrderXPO != null)
                {
                    XPCollection<WorkOrderLine> _locWorkOrderLines = new XPCollection<WorkOrderLine>(_currSession,
                                                                     new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("WorkOrder", _locWorkOrderXPO)));

                    if (_locWorkOrderLines != null && _locWorkOrderLines.Count > 0)
                    {
                        foreach (WorkOrderLine _locWorkOrderLine in _locWorkOrderLines)
                        {
                            if (_locWorkOrderLine.BillOfMaterial != null && _locWorkOrderLine.Item != null)
                            {
                                //Menghitung total quantity default dari WOL
                                //Get BOM Line
                                XPCollection<BillOfMaterialLine> _locBillOfMaterialLines = new XPCollection<BillOfMaterialLine>(_currSession,
                                                                                           new GroupOperator(GroupOperatorType.And,
                                                                                           new BinaryOperator("BillOfMaterial", _locWorkOrderLine.BillOfMaterial)));

                                if (_locBillOfMaterialLines != null && _locBillOfMaterialLines.Count > 0)
                                {
                                    foreach (BillOfMaterialLine _locBillOfMaterialLine in _locBillOfMaterialLines)
                                    {
                                        #region
                                        if ((_locWorkOrderLine.MxDQty > 0 && _locWorkOrderLine.MxDUOM == _locBillOfMaterialLine.DUOM) ||
                                            (_locWorkOrderLine.MxQty > 0 && _locWorkOrderLine.MxUOM == _locBillOfMaterialLine.UOM))
                                        {
                                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                          (new GroupOperator(GroupOperatorType.And,
                                                           new BinaryOperator("Item", _locWorkOrderLine.Item),
                                                           new BinaryOperator("UOM", _locWorkOrderLine.MxUOM),
                                                           new BinaryOperator("DefaultUOM", _locWorkOrderLine.MxDUOM),
                                                           new BinaryOperator("Active", true)));
                                        }
                                        else
                                        {
                                            if ((_locWorkOrderLine.DQty > 0 && _locWorkOrderLine.DUOM == _locBillOfMaterialLine.DUOM) ||
                                                (_locWorkOrderLine.Qty > 0 && _locWorkOrderLine.UOM == _locBillOfMaterialLine.UOM))
                                            {
                                                _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                              (new GroupOperator(GroupOperatorType.And,
                                                               new BinaryOperator("Item", _locWorkOrderLine.Item),
                                                               new BinaryOperator("UOM", _locWorkOrderLine.UOM),
                                                               new BinaryOperator("DefaultUOM", _locWorkOrderLine.DUOM),
                                                               new BinaryOperator("Active", true)));
                                            }

                                        }
                                        #endregion

                                        //Membuat Perhitungan sebelum di masukan ke dalam 
                                        #region
                                        if (_locItemUOM != null)
                                        {
                                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                            {
                                                _locInvLineTotal = (_locWorkOrderLine.Qty * _locItemUOM.DefaultConversion + _locWorkOrderLine.DQty) * _locBillOfMaterialLine.TQty;
                                                _locTotQty = Math.Floor(_locInvLineTotal / _locItemUOM.DefaultConversion);
                                                _locTotDQty = _locInvLineTotal % _locItemUOM.DefaultConversion;
                                            }
                                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                            {
                                                _locInvLineTotal = (_locWorkOrderLine.Qty / _locItemUOM.Conversion + _locWorkOrderLine.DQty) * _locBillOfMaterialLine.TQty;
                                                _locTotQty = _locInvLineTotal % _locItemUOM.DefaultConversion;
                                                _locTotDQty = Math.Floor(_locInvLineTotal / _locItemUOM.DefaultConversion);
                                            }
                                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                            {
                                                _locInvLineTotal = (_locWorkOrderLine.Qty * _locBillOfMaterialLine.Qty) + (_locWorkOrderLine.DQty * _locBillOfMaterialLine.DQty);
                                            }
                                        }
                                        else
                                        {
                                            _locInvLineTotal = (_locWorkOrderLine.Qty * _locBillOfMaterialLine.Qty) + (_locWorkOrderLine.DQty * _locBillOfMaterialLine.DQty);
                                        }
                                        #endregion

                                        WorkOrderMaterial _saveDataWorkOrderMaterial = new WorkOrderMaterial(_currSession)
                                        {
                                            Select = true,
                                            Name = _locBillOfMaterialLine.Name,
                                            Item = _locBillOfMaterialLine.Item,
                                            MxDQty = _locTotDQty,
                                            MxDUOM = _locBillOfMaterialLine.DUOM,
                                            MxQty = _locTotQty,
                                            MxUOM = _locBillOfMaterialLine.UOM,
                                            MxTQty = _locInvLineTotal,
                                            DQty = _locTotDQty,
                                            DUOM = _locBillOfMaterialLine.DUOM,
                                            Qty = _locTotQty,
                                            UOM = _locBillOfMaterialLine.UOM,
                                            TQty = _locInvLineTotal,
                                            Company = _locWorkOrderLine.Company,
                                            WorkOrder = _locWorkOrderXPO,
                                        };
                                        _saveDataWorkOrderMaterial.Save();
                                        _saveDataWorkOrderMaterial.Session.CommitTransaction();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = WorkOrder " + ex.ToString());
            }
        }

        #endregion GetWOM

        #region Global Method

        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0} !", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method

        #region PostingForProduction

        //Membuat production
        private void SetProduction(Session _currSession, WorkOrder _locWorkOrderXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _currSignCode = null;

                if (_locWorkOrderXPO != null)
                {
                    if (_locWorkOrderXPO.Status == Status.Progress)
                    {
                        _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.Production);

                        if (_currSignCode != null)
                        {
                            Production _saveDataPro = new Production(_currSession)
                            {
                                SignCode = _currSignCode,
                                Name = _locWorkOrderXPO.Name,
                                Description = _locWorkOrderXPO.Description,
                                StartDate = _locWorkOrderXPO.StartDate,
                                EndDate = _locWorkOrderXPO.EndDate,
                                EstimatedDate = _locWorkOrderXPO.EstimatedDate,
                                Company = _locWorkOrderXPO.Company,
                                MachineMapVersion = _locWorkOrderXPO.MachineMapVersion,
                                WorkOrder = _locWorkOrderXPO,
                            };
                            _saveDataPro.Save();
                            _saveDataPro.Session.CommitTransaction();
                        }
                    }

                    XPCollection<WorkOrderLine> _numLineWorkOrderLines = new XPCollection<WorkOrderLine>(_currSession,
                                                                         new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("WorkOrder", _locWorkOrderXPO),
                                                                         new BinaryOperator("Select", true)));

                    if (_numLineWorkOrderLines != null && _numLineWorkOrderLines.Count > 0)
                    {
                        Production _locProduction = _currSession.FindObject<Production>
                                                    (new GroupOperator(GroupOperatorType.And,
                                                     new BinaryOperator("WorkOrder", _locWorkOrderXPO)));

                        if (_locProduction != null)
                        {
                            if (_numLineWorkOrderLines != null && _numLineWorkOrderLines.Count > 0)
                            {
                                foreach (WorkOrderLine _numLineWorkOrderLine in _numLineWorkOrderLines)
                                {
                                    if (_numLineWorkOrderLine.Status == Status.Progress || _numLineWorkOrderLine.Status == Status.Posted)
                                    {
                                        ProductionLine _saveDataProductionLine = new ProductionLine(_currSession)
                                        {
                                            Name = _numLineWorkOrderLine.Name,
                                            Item = _numLineWorkOrderLine.Item,
                                            Description = _numLineWorkOrderLine.Description,
                                            MxDQty = _numLineWorkOrderLine.DQty,
                                            MxDUOM = _numLineWorkOrderLine.DUOM,
                                            MxQty = _numLineWorkOrderLine.Qty,
                                            MxUOM = _numLineWorkOrderLine.UOM,
                                            MxTQty = _numLineWorkOrderLine.MxTQty,
                                            DQty = _numLineWorkOrderLine.DQty,
                                            DUOM = _numLineWorkOrderLine.DUOM,
                                            Qty = _numLineWorkOrderLine.Qty,
                                            UOM = _numLineWorkOrderLine.UOM,
                                            TQty = _numLineWorkOrderLine.TQty,
                                            BillOfMaterialVersion = _numLineWorkOrderLine.BillOfMaterialVersion,
                                            BillOfMaterial = _numLineWorkOrderLine.BillOfMaterial,
                                            Company = _numLineWorkOrderLine.Company,
                                            Production = _locProduction,
                                        };
                                        _saveDataProductionLine.Save();
                                        _saveDataProductionLine.Session.CommitTransaction();
                                    }
                                    else
                                    {
                                        ErrorMessageShow("Data Status WorkOrder Not Available");
                                    }
                                }
                            }
                        }
                        else
                        {
                            ErrorMessageShow("Data Production Not Available");
                        }
                    }
                    else
                    {
                        ErrorMessageShow("Data WorkOrderLine Not Available");
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = WorkOrder ", ex.ToString());
            }
        }

        //Menentukan sisa dari transaksi
        private void SetRemainQty(Session _currSession, WorkOrder _locWorkOrderXPO)
        {
            try
            {
                if (_locWorkOrderXPO != null)
                {
                    XPCollection<WorkOrderLine> _locWorkOrderLines = new XPCollection<WorkOrderLine>(_currSession,
                                                                     new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("WorkOrder", _locWorkOrderXPO),
                                                                     new BinaryOperator("Select", true)));

                    if (_locWorkOrderLines != null && _locWorkOrderLines.Count > 0)
                    {
                        double _locRmDQty = 0;
                        double _locRmQty = 0;
                        double _locInvLineTotal = 0;
                        ItemUnitOfMeasure _locItemUOM = null;

                        foreach (WorkOrderLine _locWorkOrderLine in _locWorkOrderLines)
                        {
                            #region ProcessCount=0
                            if (_locWorkOrderLine.ProcessCount == 0)
                            {
                                #region MaxQuantity
                                if (_locWorkOrderLine.MxDQty > 0)
                                {
                                    if (_locWorkOrderLine.DQty > 0 && _locWorkOrderLine.DQty <= _locWorkOrderLine.MxDQty)
                                    {
                                        _locRmDQty = _locWorkOrderLine.MxDQty - _locWorkOrderLine.DQty;
                                    }

                                    if (_locWorkOrderLine.Qty > 0 && _locWorkOrderLine.Qty <= _locWorkOrderLine.MxQty)
                                    {
                                        _locRmQty = _locWorkOrderLine.MxQty - _locWorkOrderLine.Qty;
                                    }

                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                  (new GroupOperator(GroupOperatorType.And,
                                                   new BinaryOperator("Item", _locWorkOrderLine.Item),
                                                   new BinaryOperator("UOM", _locWorkOrderLine.MxUOM),
                                                   new BinaryOperator("DefaultUOM", _locWorkOrderLine.MxDUOM),
                                                   new BinaryOperator("Active", true)));

                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty + _locRmDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }

                                }
                                #endregion MaxQuantity

                                #region NonMaxQuantity
                                else
                                {
                                    if (_locWorkOrderLine.DQty > 0)
                                    {
                                        _locRmDQty = 0;
                                    }

                                    if (_locWorkOrderLine.Qty > 0)
                                    {
                                        _locRmQty = 0;
                                    }

                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                  (new GroupOperator(GroupOperatorType.And,
                                                   new BinaryOperator("Item", _locWorkOrderLine.Item),
                                                   new BinaryOperator("UOM", _locWorkOrderLine.UOM),
                                                   new BinaryOperator("DefaultUOM", _locWorkOrderLine.DUOM),
                                                   new BinaryOperator("Active", true)));

                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty + _locRmDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion ProcessCount=0

                            #region ProcessCount>0
                            if (_locWorkOrderLine.ProcessCount > 0)
                            {
                                if (_locWorkOrderLine.RmDQty > 0)
                                {
                                    _locRmDQty = _locWorkOrderLine.RmDQty - _locWorkOrderLine.DQty;
                                }

                                if (_locWorkOrderLine.RmQty > 0)
                                {
                                    _locRmQty = _locWorkOrderLine.RmQty - _locWorkOrderLine.Qty;
                                }

                                if (_locWorkOrderLine.MxDQty > 0 || _locWorkOrderLine.MxQty > 0)
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                  (new GroupOperator(GroupOperatorType.And,
                                                   new BinaryOperator("Item", _locWorkOrderLine.Item),
                                                   new BinaryOperator("UOM", _locWorkOrderLine.MxUOM),
                                                   new BinaryOperator("DefaultUOM", _locWorkOrderLine.MxDUOM),
                                                   new BinaryOperator("Active", true)));
                                }
                                else
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                  (new GroupOperator(GroupOperatorType.And,
                                                   new BinaryOperator("Item", _locWorkOrderLine.Item),
                                                   new BinaryOperator("UOM", _locWorkOrderLine.UOM),
                                                   new BinaryOperator("DefaultUOM", _locWorkOrderLine.DUOM),
                                                   new BinaryOperator("Active", true)));
                                }

                                if (_locItemUOM != null)
                                {
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locRmQty + _locRmDQty;
                                }

                            }
                            #endregion ProcessCount>0

                            _locWorkOrderLine.RmDQty = _locRmDQty;
                            _locWorkOrderLine.RmQty = _locRmQty;
                            _locWorkOrderLine.RmTQty = _locInvLineTotal;
                            _locWorkOrderLine.Save();
                            _locWorkOrderLine.Session.CommitTransaction();
                        }
                    }
                    else
                    {
                        ErrorMessageShow("Data WorkOrderLine Not Available");
                    }
                }
                else
                {
                    ErrorMessageShow("Data WorkOrder Not Available");
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = WorkOrder " + ex.ToString());
            }
        }

        //Menentukan jumlah quantity yang di posting WO
        private void SetPostingQty(Session _currSession, WorkOrder _locWorkOrderXPO)
        {
            try
            {
                if (_locWorkOrderXPO != null)
                {
                    XPCollection<WorkOrderLine> _locWorkOrderLines = new XPCollection<WorkOrderLine>(_currSession,
                                                                     new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("WorkOrder", _locWorkOrderXPO),
                                                                     new BinaryOperator("Select", true)));

                    if (_locWorkOrderLines != null && _locWorkOrderLines.Count > 0)
                    {
                        double _locPDQty = 0;
                        double _locPQty = 0;
                        double _locInvLineTotal = 0;
                        ItemUnitOfMeasure _locItemUOM = null;

                        foreach (WorkOrderLine _locWorkOrderLine in _locWorkOrderLines)
                        {
                            #region ProcessCount=0
                            if (_locWorkOrderLine.ProcessCount == 0)
                            {
                                #region MaxQuantity
                                if (_locWorkOrderLine.MxDQty > 0)
                                {
                                    if (_locWorkOrderLine.DQty > 0 && _locWorkOrderLine.DQty <= _locWorkOrderLine.MxDQty)
                                    {
                                        _locPDQty = _locWorkOrderLine.DQty;
                                    }

                                    if (_locWorkOrderLine.Qty > 0 && _locWorkOrderLine.Qty <= _locWorkOrderLine.MxQty)
                                    {
                                        _locPQty = _locWorkOrderLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                  (new GroupOperator(GroupOperatorType.And,
                                                   new BinaryOperator("Item", _locWorkOrderLine.Item),
                                                   new BinaryOperator("UOM", _locWorkOrderLine.MxUOM),
                                                   new BinaryOperator("DefaultUOM", _locWorkOrderLine.MxDUOM),
                                                   new BinaryOperator("Active", true)));

                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty + _locPDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }

                                }
                                #endregion MaxQuantity

                                #region NonMaxQuantity
                                else
                                {
                                    if (_locWorkOrderLine.DQty > 0)
                                    {
                                        _locPDQty = _locWorkOrderLine.DQty;
                                    }

                                    if (_locWorkOrderLine.Qty > 0)
                                    {
                                        _locPQty = _locWorkOrderLine.Qty;
                                    }

                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                  (new GroupOperator(GroupOperatorType.And,
                                                   new BinaryOperator("Item", _locWorkOrderLine.Item),
                                                   new BinaryOperator("UOM", _locWorkOrderLine.UOM),
                                                   new BinaryOperator("DefaultUOM", _locWorkOrderLine.DUOM),
                                                   new BinaryOperator("Active", true)));

                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty + _locPDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion ProcessCount=0

                            #region ProcessCount>0
                            if (_locWorkOrderLine.ProcessCount > 0)
                            {
                                if (_locWorkOrderLine.PDQty > 0)
                                {
                                    _locPDQty = _locWorkOrderLine.PDQty + _locWorkOrderLine.DQty;
                                }

                                if (_locWorkOrderLine.PQty > 0)
                                {
                                    _locPQty = _locWorkOrderLine.PQty + _locWorkOrderLine.Qty;
                                }

                                if (_locWorkOrderLine.MxDQty > 0 || _locWorkOrderLine.MxQty > 0)
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                  (new GroupOperator(GroupOperatorType.And,
                                                   new BinaryOperator("Item", _locWorkOrderLine.Item),
                                                   new BinaryOperator("UOM", _locWorkOrderLine.MxUOM),
                                                   new BinaryOperator("DefaultUOM", _locWorkOrderLine.MxDUOM),
                                                   new BinaryOperator("Active", true)));
                                }
                                else
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                  (new GroupOperator(GroupOperatorType.And,
                                                   new BinaryOperator("Item", _locWorkOrderLine.Item),
                                                   new BinaryOperator("UOM", _locWorkOrderLine.UOM),
                                                   new BinaryOperator("DefaultUOM", _locWorkOrderLine.DUOM),
                                                   new BinaryOperator("Active", true)));
                                }

                                if (_locItemUOM != null)
                                {
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locPQty + _locPDQty;
                                }

                            }
                            #endregion ProcessCount>0

                            _locWorkOrderLine.PDQty = _locPDQty;
                            _locWorkOrderLine.PDUOM = _locWorkOrderLine.DUOM;
                            _locWorkOrderLine.PQty = _locPQty;
                            _locWorkOrderLine.PUOM = _locWorkOrderLine.UOM;
                            _locWorkOrderLine.PTQty = _locInvLineTotal;
                            _locWorkOrderLine.Save();
                            _locWorkOrderLine.Session.CommitTransaction();
                        }
                    }
                    else
                    {
                        ErrorMessageShow("Data WorkOrderLine Not Available");
                    }
                }
                else
                {
                    ErrorMessageShow("Data WorkOrder Not Available");
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = WorkOrder " + ex.ToString());
            }
        }

        //Menentukan banyak proses posting Production
        private void SetProcessCount(Session _currSession, WorkOrder _locWorkOrderXPO)
        {
            try
            {
                if (_locWorkOrderXPO != null)
                {
                    XPCollection<WorkOrderLine> _locWorkOrderLines = new XPCollection<WorkOrderLine>(_currSession,
                                                                     new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("WorkOrder", _locWorkOrderXPO),
                                                                     new BinaryOperator("Select", true)));

                    if (_locWorkOrderLines != null && _locWorkOrderLines.Count > 0)
                    {
                        foreach (WorkOrderLine _locWorkOrderLine in _locWorkOrderLines)
                        {
                            if (_locWorkOrderLine.Status == Status.Progress || _locWorkOrderLine.Status == Status.Posted)
                            {
                                if (_locWorkOrderLine.DQty > 0 || _locWorkOrderLine.Qty > 0)
                                {
                                    _locWorkOrderLine.ProcessCount = _locWorkOrderLine.ProcessCount + 1;
                                    _locWorkOrderLine.Save();
                                    _locWorkOrderLine.Session.CommitTransaction();
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Status WorkOrderLine Not Available");
                            }
                        }
                    }
                    else
                    {
                        ErrorMessageShow("Data WorkOrderLine Not Available");
                    }
                }
                else
                {
                    ErrorMessageShow("Data WorkOrder Not Available");
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = WorkOrder " + ex.ToString());
            }
        }

        //Menentukan status pada workorderline
        private void SetStatusWorkOrderLine(Session _currSession, WorkOrder _locWorkOrderXPO)
        {
            try
            {
                DateTime now = DateTime.Now;

                if (_locWorkOrderXPO != null)
                {
                    XPCollection<WorkOrderLine> _locWorkOrderLines = new XPCollection<WorkOrderLine>(_currSession,
                                                                     new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("WorkOrder", _locWorkOrderXPO),
                                                                     new BinaryOperator("Select", true)));

                    if (_locWorkOrderLines != null && _locWorkOrderLines.Count > 0)
                    {
                        foreach (WorkOrderLine _locWorkOrderLine in _locWorkOrderLines)
                        {
                            if (_locWorkOrderLine.Status == Status.Progress || _locWorkOrderLine.Status == Status.Posted)
                            {
                                if (_locWorkOrderLine.RmDQty == 0 && _locWorkOrderLine.RmQty == 0 && _locWorkOrderLine.RmTQty == 0)
                                {
                                    _locWorkOrderLine.Status = Status.Close;
                                    _locWorkOrderLine.ActivationPosting = true;
                                    _locWorkOrderLine.StatusDate = now;
                                }
                                else
                                {
                                    _locWorkOrderLine.Status = Status.Posted;
                                    _locWorkOrderLine.StatusDate = now;
                                }
                                _locWorkOrderLine.Save();
                                _locWorkOrderLine.Session.CommitTransaction();
                            }
                            else
                            {
                                ErrorMessageShow("Data Status WorkOrder Not Available");
                            }
                        }
                    }
                    else
                    {
                        ErrorMessageShow("Data WorkOrderLine Not Available");
                    }
                }
                else
                {
                    ErrorMessageShow("Data WorkOrder Not Available");
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = WorkOrder " + ex.ToString());
            }
        }

        //Menormalkan Quantity WorkOrder
        private void SetNormalQuantity(Session _currSession, WorkOrder _locWorkOrderXPO)
        {
            try
            {
                if (_locWorkOrderXPO != null)
                {
                    XPCollection<WorkOrderLine> _locWorkOrderLines = new XPCollection<WorkOrderLine>(_currSession,
                                                                     new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Select", true)));

                    if (_locWorkOrderLines != null && _locWorkOrderLines.Count > 0)
                    {
                        foreach (WorkOrderLine _locWorkOrderLine in _locWorkOrderLines)
                        {
                            if (_locWorkOrderLine.Status == Status.Progress || _locWorkOrderLine.Status == Status.Posted || _locWorkOrderLine.Status == Status.Close)
                            {
                                if (_locWorkOrderLine.DQty > 0 || _locWorkOrderLine.Qty > 0)
                                {
                                    _locWorkOrderLine.Select = false;
                                    _locWorkOrderLine.DQty = 0;
                                    _locWorkOrderLine.Qty = 0;
                                    _locWorkOrderLine.Save();
                                    _locWorkOrderLine.Session.CommitTransaction();
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Status WorkOrderLine Not Available");
                            }
                        }
                    }
                    else
                    {
                        ErrorMessageShow("Data WorkOrderLine Not Available");
                    }
                }
                else
                {
                    ErrorMessageShow("Data WorkOrder Not Available");
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = WorkOrder " + ex.ToString());
            }
        }

        //Menentukan status pada workorder
        private void SetFinalStatusWorkOrder(Session _currSession, WorkOrder _locWorkOrderXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locStatusCount = 0;
                int _locWorkOrderLineCount = 0;

                if (_locWorkOrderXPO != null)
                {
                    XPCollection<WorkOrderLine> _locWorkOrderLines = new XPCollection<WorkOrderLine>(_currSession,
                                                                     new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("WorkOrder", _locWorkOrderXPO)));

                    if (_locWorkOrderLines != null && _locWorkOrderLines.Count() > 0)
                    {
                        _locWorkOrderLineCount = _locWorkOrderLines.Count();

                        foreach (WorkOrderLine _locWorkOrderLine in _locWorkOrderLines)
                        {
                            if (_locWorkOrderLine.Status == Status.Close)
                            {
                                _locStatusCount = _locStatusCount + 1;
                            }
                        }
                    }

                    if (_locStatusCount == _locWorkOrderLineCount)
                    {
                        _locWorkOrderXPO.ActivationPosting = true;
                        _locWorkOrderXPO.Status = Status.Close;
                        _locWorkOrderXPO.StatusDate = now;
                        _locWorkOrderXPO.Save();
                        _locWorkOrderXPO.Session.CommitTransaction();
                    }
                    else
                    {
                        _locWorkOrderXPO.Status = Status.Posted;
                        _locWorkOrderXPO.StatusDate = now;
                        _locWorkOrderXPO.Save();
                        _locWorkOrderXPO.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = WorkOrder " + ex.ToString());
            }
        }

        #endregion PostingForProduction

        #region PostingForMaterialRequisition

        //Menentukan sisa dari transaksi Work Order Material
        private void SetRemainQtyForPostingMaterial(Session _currSession, WorkOrder _locWorkOrderXPO)
        {
            try
            {
                if (_locWorkOrderXPO != null)
                {
                    XPCollection<WorkOrderMaterial> _locWorkOrderMaterials = new XPCollection<WorkOrderMaterial>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("WorkOrder", _locWorkOrderXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locWorkOrderMaterials != null && _locWorkOrderMaterials.Count > 0)
                    {
                        double _locRmDQty = 0;
                        double _locRmQty = 0;
                        double _locInvLineTotal = 0;
                        ItemUnitOfMeasure _locItemUOM = null;

                        foreach (WorkOrderMaterial _locWorkOrderMaterial in _locWorkOrderMaterials)
                        {
                            #region ProcessCount=0
                            if (_locWorkOrderMaterial.ProcessCount == 0)
                            {
                                #region MaxQuantity
                                if (_locWorkOrderMaterial.MxDQty > 0 || _locWorkOrderMaterial.MxQty > 0)
                                {
                                    if (_locWorkOrderMaterial.DQty > 0 && _locWorkOrderMaterial.DQty <= _locWorkOrderMaterial.MxDQty)
                                    {
                                        _locRmDQty = _locWorkOrderMaterial.MxDQty - _locWorkOrderMaterial.DQty;
                                    }

                                    if (_locWorkOrderMaterial.Qty > 0 && _locWorkOrderMaterial.Qty <= _locWorkOrderMaterial.MxQty)
                                    {
                                        _locRmQty = _locWorkOrderMaterial.MxQty - _locWorkOrderMaterial.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locWorkOrderMaterial.Item),
                                                                new BinaryOperator("UOM", _locWorkOrderMaterial.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locWorkOrderMaterial.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty + _locRmDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }

                                }
                                #endregion MaxQuantity

                                #region NonMaxQuantity
                                else
                                {
                                    if (_locWorkOrderMaterial.DQty > 0)
                                    {
                                        _locRmDQty = _locWorkOrderMaterial.DQty;
                                    }

                                    if (_locWorkOrderMaterial.Qty > 0)
                                    {
                                        _locRmQty = _locWorkOrderMaterial.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locWorkOrderMaterial.Item),
                                                                new BinaryOperator("UOM", _locWorkOrderMaterial.UOM),
                                                                new BinaryOperator("DefaultUOM", _locWorkOrderMaterial.DUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty + _locRmDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion ProcessCount=0

                            #region ProcessCount>0
                            if (_locWorkOrderMaterial.ProcessCount > 0)
                            {
                                if (_locWorkOrderMaterial.RmDQty > 0)
                                {
                                    _locRmDQty = _locWorkOrderMaterial.RmDQty - _locWorkOrderMaterial.DQty;
                                }

                                if (_locWorkOrderMaterial.RmQty > 0)
                                {
                                    _locRmQty = _locWorkOrderMaterial.RmQty - _locWorkOrderMaterial.Qty;
                                }

                                if (_locWorkOrderMaterial.MxDQty > 0 || _locWorkOrderMaterial.MxQty > 0)
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locWorkOrderMaterial.Item),
                                                                new BinaryOperator("UOM", _locWorkOrderMaterial.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locWorkOrderMaterial.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                }
                                else
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locWorkOrderMaterial.Item),
                                                                new BinaryOperator("UOM", _locWorkOrderMaterial.UOM),
                                                                new BinaryOperator("DefaultUOM", _locWorkOrderMaterial.DUOM),
                                                                new BinaryOperator("Active", true)));
                                }


                                if (_locItemUOM != null)
                                {
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locRmQty + _locRmDQty;
                                }

                            }
                            #endregion ProcessCount>0

                            _locWorkOrderMaterial.RmDQty = _locRmDQty;
                            _locWorkOrderMaterial.RmQty = _locRmQty;
                            _locWorkOrderMaterial.RmTQty = _locInvLineTotal;
                            _locWorkOrderMaterial.Save();
                            _locWorkOrderMaterial.Session.CommitTransaction();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = WorkOrder " + ex.ToString());
            }
        }

        //Menentukan jumlah quantity yang di posting pd Work Order Material
        private void SetPostingQtyForPostingMaterial(Session _currSession, WorkOrder _locWorkOrderXPO)
        {
            try
            {
                if (_locWorkOrderXPO != null)
                {
                    XPCollection<WorkOrderMaterial> _locWorkOrderMaterials = new XPCollection<WorkOrderMaterial>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("WorkOrder", _locWorkOrderXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locWorkOrderMaterials != null && _locWorkOrderMaterials.Count > 0)
                    {
                        double _locPDQty = 0;
                        double _locPQty = 0;
                        double _locInvLineTotal = 0;
                        ItemUnitOfMeasure _locItemUOM = null;

                        foreach (WorkOrderMaterial _locWorkOrderMaterial in _locWorkOrderMaterials)
                        {
                            #region ProcessCount=0
                            if (_locWorkOrderMaterial.ProcessCount == 0)
                            {
                                #region MaxQuantity
                                if (_locWorkOrderMaterial.MxDQty > 0 || _locWorkOrderMaterial.MxQty > 0)
                                {
                                    if (_locWorkOrderMaterial.DQty > 0 && _locWorkOrderMaterial.DQty <= _locWorkOrderMaterial.MxDQty)
                                    {
                                        _locPDQty = _locWorkOrderMaterial.DQty;
                                    }

                                    if (_locWorkOrderMaterial.Qty > 0 && _locWorkOrderMaterial.Qty <= _locWorkOrderMaterial.MxQty)
                                    {
                                        _locPQty = _locWorkOrderMaterial.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locWorkOrderMaterial.Item),
                                                                new BinaryOperator("UOM", _locWorkOrderMaterial.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locWorkOrderMaterial.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty + _locPDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }

                                }
                                #endregion MaxQuantity

                                #region NonMaxQuantity
                                else
                                {
                                    if (_locWorkOrderMaterial.DQty > 0)
                                    {
                                        _locPDQty = _locWorkOrderMaterial.DQty;
                                    }

                                    if (_locWorkOrderMaterial.Qty > 0)
                                    {
                                        _locPQty = _locWorkOrderMaterial.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locWorkOrderMaterial.Item),
                                                                new BinaryOperator("UOM", _locWorkOrderMaterial.UOM),
                                                                new BinaryOperator("DefaultUOM", _locWorkOrderMaterial.DUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty + _locPDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion ProcessCount=0

                            #region ProcessCount>0
                            if (_locWorkOrderMaterial.ProcessCount > 0)
                            {
                                if (_locWorkOrderMaterial.PDQty > 0)
                                {
                                    _locPDQty = _locWorkOrderMaterial.PDQty + _locWorkOrderMaterial.DQty;
                                }

                                if (_locWorkOrderMaterial.PQty > 0)
                                {
                                    _locPQty = _locWorkOrderMaterial.PQty + _locWorkOrderMaterial.Qty;
                                }

                                if (_locWorkOrderMaterial.MxDQty > 0 || _locWorkOrderMaterial.MxQty > 0)
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                           (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Item", _locWorkOrderMaterial.Item),
                                                            new BinaryOperator("UOM", _locWorkOrderMaterial.MxUOM),
                                                            new BinaryOperator("DefaultUOM", _locWorkOrderMaterial.MxDUOM),
                                                            new BinaryOperator("Active", true)));
                                }
                                else
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                           (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Item", _locWorkOrderMaterial.Item),
                                                            new BinaryOperator("UOM", _locWorkOrderMaterial.UOM),
                                                            new BinaryOperator("DefaultUOM", _locWorkOrderMaterial.DUOM),
                                                            new BinaryOperator("Active", true)));
                                }

                                if (_locItemUOM != null)
                                {
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locPQty + _locPDQty;
                                }

                            }
                            #endregion ProcessCount>0

                            _locWorkOrderMaterial.PDQty = _locPDQty;
                            _locWorkOrderMaterial.PDUOM = _locWorkOrderMaterial.DUOM;
                            _locWorkOrderMaterial.PQty = _locPQty;
                            _locWorkOrderMaterial.PUOM = _locWorkOrderMaterial.UOM;
                            _locWorkOrderMaterial.PTQty = _locInvLineTotal;
                            _locWorkOrderMaterial.Save();
                            _locWorkOrderMaterial.Session.CommitTransaction();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = WorkOrder " + ex.ToString());
            }

        }

        //Menentukan Banyak process posting pd Work Order Material
        private void SetProcessCountForPostingMaterial(Session _currSession, WorkOrder _locWorkOrderXPO)
        {
            try
            {
                if (_locWorkOrderXPO != null)
                {
                    XPCollection<WorkOrderMaterial> _locWorkOrderMaterials = new XPCollection<WorkOrderMaterial>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("WorkOrder", _locWorkOrderXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locWorkOrderMaterials != null && _locWorkOrderMaterials.Count > 0)
                    {

                        foreach (WorkOrderMaterial _locWorkOrderMaterial in _locWorkOrderMaterials)
                        {
                            if (_locWorkOrderMaterial.Status == Status.Progress || _locWorkOrderMaterial.Status == Status.Posted)
                            {
                                if (_locWorkOrderMaterial.DQty > 0 || _locWorkOrderMaterial.Qty > 0)
                                {
                                    _locWorkOrderMaterial.ProcessCount = _locWorkOrderMaterial.ProcessCount + 1;
                                    _locWorkOrderMaterial.Save();
                                    _locWorkOrderMaterial.Session.CommitTransaction();
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = WorkOrder " + ex.ToString());
            }
        }

        //Membuat Material Requisition
        private void SetMaterialRequisitionForPostingMaterial(Session _currSession, WorkOrder _locWorkOrderXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _currSignCode = null;

                if (_locWorkOrderXPO != null)
                {
                    _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.MaterialRequisition);

                    if (_currSignCode != null)
                    {
                        MaterialRequisition _saveDataMR = new MaterialRequisition(_currSession)
                        {
                            SignCode = _currSignCode,
                            Name = _locWorkOrderXPO.Name,
                            Description = _locWorkOrderXPO.Description,
                            EstimatedDate = _locWorkOrderXPO.EstimatedDate,
                            Company = _locWorkOrderXPO.Company,
                            WorkOrder = _locWorkOrderXPO,
                        };
                        _saveDataMR.Save();
                        _saveDataMR.Session.CommitTransaction();

                        XPCollection<WorkOrderMaterial> _numLineWorkOrderMaterials = new XPCollection<WorkOrderMaterial>(_currSession,
                                                                                new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("WorkOrder", _locWorkOrderXPO),
                                                                                new BinaryOperator("Select", true)));

                        MaterialRequisition _locMaterialRequisition2 = _currSession.FindObject<MaterialRequisition>(new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("SignCode", _currSignCode),
                                                            new BinaryOperator("WorkOrder", _locWorkOrderXPO)));

                        if (_locMaterialRequisition2 != null)
                        {
                            if (_numLineWorkOrderMaterials != null && _numLineWorkOrderMaterials.Count > 0)
                            {
                                foreach (WorkOrderMaterial _numLineWorkOrderMaterial in _numLineWorkOrderMaterials)
                                {
                                    if (_numLineWorkOrderMaterial.Status == Status.Progress || _numLineWorkOrderMaterial.Status == Status.Posted)
                                    {
                                        MaterialRequisitionLine _saveDataMaterialRequisitionLine = new MaterialRequisitionLine(_currSession)
                                        {

                                            Item = _numLineWorkOrderMaterial.Item,
                                            Description = _numLineWorkOrderMaterial.Description,
                                            MxDQty = _numLineWorkOrderMaterial.DQty,
                                            MxDUOM = _numLineWorkOrderMaterial.DUOM,
                                            MxQty = _numLineWorkOrderMaterial.Qty,
                                            MxUOM = _numLineWorkOrderMaterial.UOM,
                                            MxTQty = _numLineWorkOrderMaterial.MxTQty,
                                            DQty = _numLineWorkOrderMaterial.DQty,
                                            DUOM = _numLineWorkOrderMaterial.DUOM,
                                            Qty = _numLineWorkOrderMaterial.Qty,
                                            UOM = _numLineWorkOrderMaterial.UOM,
                                            TQty = _numLineWorkOrderMaterial.TQty,
                                            Company = _numLineWorkOrderMaterial.Company,
                                            MaterialRequisition = _locMaterialRequisition2,
                                        };
                                        _saveDataMaterialRequisitionLine.Save();
                                        _saveDataMaterialRequisitionLine.Session.CommitTransaction();

                                    }
                                }
                            }
                        }
                    }


                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = WorkRequisition ", ex.ToString());
            }
        }

        //Membuat Material
        private void SetProductionMaterial(Session _currSession, WorkOrder _locWorkOrderXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _currSignCode = null;

                if (_locWorkOrderXPO != null)
                {
                    _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.ProductionMaterial);

                    if (_currSignCode != null)
                    {
                        XPCollection<WorkOrderMaterial> _numLineWOMLines = new XPCollection<WorkOrderMaterial>(_currSession,
                                                                           new GroupOperator(GroupOperatorType.And,
                                                                           new BinaryOperator("WorkOrder", _locWorkOrderXPO),
                                                                           new BinaryOperator("Select", true)));

                        Production _locProduction = _currSession.FindObject<Production>
                                                    (new GroupOperator(GroupOperatorType.And,
                                                     new BinaryOperator("WorkOrder", _locWorkOrderXPO)));

                        if (_locProduction != null)
                        {
                            if (_numLineWOMLines != null && _numLineWOMLines.Count > 0)
                            {
                                foreach (WorkOrderMaterial _numLineWOMLine in _numLineWOMLines)
                                {
                                    if (_numLineWOMLine.Status == Status.Progress || _numLineWOMLine.Status == Status.Posted)
                                    {
                                        ProductionMaterial _saveDataMPro = new ProductionMaterial(_currSession)
                                        {
                                            Name = _numLineWOMLine.Name,
                                            Description = _numLineWOMLine.Description,
                                            Item = _numLineWOMLine.Item,
                                            MxDQty = _numLineWOMLine.MxDQty,
                                            MxDUOM = _numLineWOMLine.MxDUOM,
                                            MxQty = _numLineWOMLine.MxQty,
                                            MxUOM = _numLineWOMLine.MxUOM,
                                            MxTQty = _numLineWOMLine.MxTQty,
                                            DQty = _numLineWOMLine.DQty,
                                            Qty = _numLineWOMLine.Qty,
                                            UOM = _numLineWOMLine.UOM,
                                            TQty = _numLineWOMLine.TQty,
                                            Company = _numLineWOMLine.Company,
                                            WorkOrder = _locWorkOrderXPO,
                                            Production = _locProduction,
                                        };
                                        _saveDataMPro.Save();
                                        _saveDataMPro.Session.CommitTransaction();
                                    }
                                }
                            }
                            else
                            {
                                ErrorMessageShow("WorkOrderMaterialLine Not Available");
                            }
                        }
                        else
                        {
                            ErrorMessageShow("Production Not Available");
                        }
                    }
                }
                else
                {
                    ErrorMessageShow("WorkOrder Not Available");
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = WorkOrder ", ex.ToString());
            }
        }

        //Menentukan Status Work Order Material
        private void SetStatusWorkOrderMaterialForPostingMaterial(Session _currSession, WorkOrder _locWorkOrderXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locWorkOrderXPO != null)
                {
                    XPCollection<WorkOrderMaterial> _locWorkOrderMaterials = new XPCollection<WorkOrderMaterial>(_currSession,
                                                                             new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("WorkOrder", _locWorkOrderXPO),
                                                                             new BinaryOperator("Select", true)));

                    if (_locWorkOrderMaterials != null && _locWorkOrderMaterials.Count > 0)
                    {

                        foreach (WorkOrderMaterial _locWorkOrderMaterial in _locWorkOrderMaterials)
                        {
                            if (_locWorkOrderMaterial.Status == Status.Progress || _locWorkOrderMaterial.Status == Status.Posted)
                            {
                                if (_locWorkOrderMaterial.DQty > 0 || _locWorkOrderMaterial.Qty > 0)
                                {
                                    if (_locWorkOrderMaterial.RmDQty == 0 && _locWorkOrderMaterial.RmQty == 0 && _locWorkOrderMaterial.RmTQty == 0)
                                    {
                                        _locWorkOrderMaterial.Status = Status.Close;
                                        _locWorkOrderMaterial.ActivationPosting = true;
                                        _locWorkOrderMaterial.StatusDate = now;
                                    }
                                    else
                                    {
                                        _locWorkOrderMaterial.Status = Status.Posted;
                                        _locWorkOrderMaterial.StatusDate = now;
                                    }
                                    _locWorkOrderMaterial.Save();
                                    _locWorkOrderMaterial.Session.CommitTransaction();
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = WorkOrder " + ex.ToString());
            }
        }

        //Menormalkan Quantity pd Work Order Material
        private void SetNormalQuantityForPostingMaterial(Session _currSession, WorkOrder _locWorkOrderXPO)
        {
            try
            {
                if (_locWorkOrderXPO != null)
                {
                    XPCollection<WorkOrderMaterial> _locWorkOrderMaterials = new XPCollection<WorkOrderMaterial>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("WorkOrder", _locWorkOrderXPO),
                                                                                     new BinaryOperator("Select", true)));

                    if (_locWorkOrderMaterials != null && _locWorkOrderMaterials.Count > 0)
                    {
                        foreach (WorkOrderMaterial _locWorkOrderMaterial in _locWorkOrderMaterials)
                        {
                            if (_locWorkOrderMaterial.Status == Status.Progress || _locWorkOrderMaterial.Status == Status.Posted || _locWorkOrderMaterial.Status == Status.Close)
                            {
                                if (_locWorkOrderMaterial.DQty > 0 || _locWorkOrderMaterial.Qty > 0)
                                {
                                    _locWorkOrderMaterial.Select = false;
                                    _locWorkOrderMaterial.DQty = 0;
                                    _locWorkOrderMaterial.Qty = 0;
                                    _locWorkOrderMaterial.Save();
                                    _locWorkOrderMaterial.Session.CommitTransaction();
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = WorkOrder " + ex.ToString());
            }
        }

        #endregion PostingForMaterialRequisition
        
    }
}
