﻿namespace FullDrive.Module.Controllers
{
    partial class ShipmentBookingActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ShipmentBookingProgressAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.ShipmentBookingApprovalAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.ShipmentBookingPostingAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.ShipmentBookingListviewFilterSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.ShipmentBookingListviewFilterApprovalSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // ShipmentBookingProgressAction
            // 
            this.ShipmentBookingProgressAction.Caption = "Progress";
            this.ShipmentBookingProgressAction.ConfirmationMessage = null;
            this.ShipmentBookingProgressAction.Id = "ShipmentBookingProgressActionId";
            this.ShipmentBookingProgressAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.ShipmentBooking);
            this.ShipmentBookingProgressAction.ToolTip = null;
            this.ShipmentBookingProgressAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.ShipmentBookingProgressAction_Execute);
            // 
            // ShipmentBookingApprovalAction
            // 
            this.ShipmentBookingApprovalAction.Caption = "Approval";
            this.ShipmentBookingApprovalAction.ConfirmationMessage = null;
            this.ShipmentBookingApprovalAction.Id = "ShipmentBookingApprovalActionId";
            this.ShipmentBookingApprovalAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.ShipmentBookingApprovalAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.ShipmentBooking);
            this.ShipmentBookingApprovalAction.ToolTip = null;
            this.ShipmentBookingApprovalAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.ShipmentBookingApprovalAction_Execute);
            // 
            // ShipmentBookingPostingAction
            // 
            this.ShipmentBookingPostingAction.Caption = "Posting";
            this.ShipmentBookingPostingAction.ConfirmationMessage = null;
            this.ShipmentBookingPostingAction.Id = "ShipmentBookingPostingActionId";
            this.ShipmentBookingPostingAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.ShipmentBooking);
            this.ShipmentBookingPostingAction.ToolTip = null;
            this.ShipmentBookingPostingAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.ShipmentBookingPostingAction_Execute);
            // 
            // ShipmentBookingListviewFilterSelectionAction
            // 
            this.ShipmentBookingListviewFilterSelectionAction.Caption = "Filter";
            this.ShipmentBookingListviewFilterSelectionAction.ConfirmationMessage = null;
            this.ShipmentBookingListviewFilterSelectionAction.Id = "ShipmentBookingListviewFilterSelectionActionId";
            this.ShipmentBookingListviewFilterSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.ShipmentBookingListviewFilterSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.ShipmentBooking);
            this.ShipmentBookingListviewFilterSelectionAction.ToolTip = null;
            this.ShipmentBookingListviewFilterSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.ShipmentBookingListviewFilterSelectionAction_Execute);
            // 
            // ShipmentBookingListviewFilterApprovalSelectionAction
            // 
            this.ShipmentBookingListviewFilterApprovalSelectionAction.Caption = "Filter Approval";
            this.ShipmentBookingListviewFilterApprovalSelectionAction.ConfirmationMessage = null;
            this.ShipmentBookingListviewFilterApprovalSelectionAction.Id = "ShipmentBookingListviewFilterApprovalSelectionActionId";
            this.ShipmentBookingListviewFilterApprovalSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.ShipmentBookingListviewFilterApprovalSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.ShipmentBooking);
            this.ShipmentBookingListviewFilterApprovalSelectionAction.ToolTip = null;
            this.ShipmentBookingListviewFilterApprovalSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.ShipmentBookingListviewFilterApprovalSelectionAction_Execute);
            // 
            // ShipmentBookingActionController
            // 
            this.Actions.Add(this.ShipmentBookingProgressAction);
            this.Actions.Add(this.ShipmentBookingApprovalAction);
            this.Actions.Add(this.ShipmentBookingPostingAction);
            this.Actions.Add(this.ShipmentBookingListviewFilterSelectionAction);
            this.Actions.Add(this.ShipmentBookingListviewFilterApprovalSelectionAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction ShipmentBookingProgressAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction ShipmentBookingApprovalAction;
        private DevExpress.ExpressApp.Actions.SimpleAction ShipmentBookingPostingAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction ShipmentBookingListviewFilterSelectionAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction ShipmentBookingListviewFilterApprovalSelectionAction;
    }
}
