﻿#region Default

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

#endregion Default

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class MaterialRequisitionActionController : ViewController
    {
        #region Default

        private ChoiceActionItem _selectionListviewFilter;
        public MaterialRequisitionActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.

            #region FilterStatus
            MaterialRequisitionListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                MaterialRequisitionListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
            #endregion FilterStatus

            #region FilterApproval
            MaterialRequisitionListviewFilterApprovalSelectionAction.Items.Clear();
            foreach (object _currApprovalFilter in Enum.GetValues(typeof(CustomProcess.ApprovalFilter)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.ApprovalFilter));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApprovalFilter), _currApprovalFilter);
                MaterialRequisitionListviewFilterApprovalSelectionAction.Items.Add(_selectionListviewFilter);
            }
            #endregion FilterApproval
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        #endregion Default

        private void MaterialRequisitionProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;


                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        MaterialRequisition _locMaterialRequisitionOS = (MaterialRequisition)_objectSpace.GetObject(obj);

                        if (_locMaterialRequisitionOS != null)
                        {
                            if (_locMaterialRequisitionOS.Code != null)
                            {
                                _currObjectId = _locMaterialRequisitionOS.Code;

                                MaterialRequisition _locMaterialRequisitionXPO = _currSession.FindObject<MaterialRequisition>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locMaterialRequisitionXPO != null)
                                {
                                    if (_locMaterialRequisitionXPO.Status == Status.Open || _locMaterialRequisitionXPO.Status == Status.Progress)
                                    {
                                        if (_locMaterialRequisitionXPO.Status == Status.Open)
                                        {
                                            _locMaterialRequisitionXPO.Status = Status.Progress;
                                            _locMaterialRequisitionXPO.StatusDate = now;
                                            _locMaterialRequisitionXPO.Save();
                                            _locMaterialRequisitionXPO.Session.CommitTransaction();
                                        }

                                        XPCollection<MaterialRequisitionLine> _locMaterialRequisitionLines = new XPCollection<MaterialRequisitionLine>
                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("MaterialRequisition", _locMaterialRequisitionXPO),
                                                            new BinaryOperator("Status", Status.Open)));

                                        if (_locMaterialRequisitionLines != null && _locMaterialRequisitionLines.Count > 0)
                                        {
                                            foreach (MaterialRequisitionLine _locMaterialRequisitionLine in _locMaterialRequisitionLines)
                                            {
                                                _locMaterialRequisitionLine.Status = Status.Progress;
                                                _locMaterialRequisitionLine.StatusDate = now;
                                                _locMaterialRequisitionLine.Save();
                                                _locMaterialRequisitionLine.Session.CommitTransaction();
                                            }
                                        }

                                    }

                                    SuccessMessageShow("MR has successfully updated to progress");
                                }
                                else
                                {
                                    ErrorMessageShow("Data Material Requisition Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Material Requisition Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = MaterialRequisition " + ex.ToString());
            }
        }

        private void MaterialRequisitionListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(MaterialRequisition)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = MaterialRequisition " + ex.ToString());
            }
        }

        private void MaterialRequisitionPostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        MaterialRequisition _locMaterialRequisitionOS = (MaterialRequisition)_objectSpace.GetObject(obj);

                        if (_locMaterialRequisitionOS != null)
                        {
                            if (_locMaterialRequisitionOS.Code != null)
                            {
                                _currObjectId = _locMaterialRequisitionOS.Code;

                                MaterialRequisition _locMaterialRequisitionXPO = _currSession.FindObject<MaterialRequisition>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locMaterialRequisitionXPO != null)
                                {
                                    //Get Approval dulu
                                    if (_locMaterialRequisitionXPO.Status == Status.Progress || _locMaterialRequisitionXPO.Status == Status.Posted)
                                    {
                                        ApprovalLine _locApprovalLineXPO = _currSession.FindObject<ApprovalLine>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("EndApproval", true),
                                                                     new BinaryOperator("MaterialRequisition", _locMaterialRequisitionXPO)));

                                        if (_locApprovalLineXPO != null)
                                        {
                                            SetPostingQty(_currSession, _locMaterialRequisitionXPO);
                                            SetRemainQty(_currSession, _locMaterialRequisitionXPO);
                                            SetPurchaseOrder(_currSession, _locMaterialRequisitionXPO);
                                            SetProcessCount(_currSession, _locMaterialRequisitionXPO);
                                            SetNormalQuantity(_currSession, _locMaterialRequisitionXPO);
                                            SetStatusMaterialRequisitionLine(_currSession, _locMaterialRequisitionXPO);
                                            SetStatusMaterialRequisition(_currSession, _locMaterialRequisitionXPO);
                                        }
                                    }

                                    SuccessMessageShow("Material Requisition has been successfully post");
                                }
                                else
                                {
                                    ErrorMessageShow("Data Material Requisition Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Material Requisition Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = MaterialRequisition " + ex.ToString());
            }
        }

        private void MaterialRequisitionListviewFilterApprovalSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(MaterialRequisition)))
                {
                    if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.Approval)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("ActiveApproved3", false, BinaryOperatorType.Equal),
                                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("ActiveApproved1", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", true, BinaryOperatorType.Equal)));
                    }
                    else if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.EndApproval)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new BinaryOperator("ActiveApproved3", true, BinaryOperatorType.Equal);
                    }
                    else if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.AllData)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("ActiveApproved1", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved3", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved1", false, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", false, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved3", false, BinaryOperatorType.Equal));
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = MaterialRequisition " + ex.ToString());
            }
        }

        //==== Code Only ====

        #region Posting

        //Menentukan sisa dari transaksi issue
        private void SetRemainQty(Session _currSession, MaterialRequisition _locMaterialRequisitionXPO)
        {
            try
            {
                if (_locMaterialRequisitionXPO != null)
                {
                    XPCollection<MaterialRequisitionLine> _locMaterialRequisitionLines = new XPCollection<MaterialRequisitionLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("MaterialRequisition", _locMaterialRequisitionXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locMaterialRequisitionLines != null && _locMaterialRequisitionLines.Count > 0)
                    {
                        double _locRmDQty = 0;
                        double _locRmQty = 0;
                        double _locInvLineTotal = 0;
                        ItemUnitOfMeasure _locItemUOM = null;

                        foreach (MaterialRequisitionLine _locMaterialRequisitionLine in _locMaterialRequisitionLines)
                        {
                            #region ProcessCount=0
                            if (_locMaterialRequisitionLine.ProcessCount == 0)
                            {
                                #region MaxQuantity
                                if (_locMaterialRequisitionLine.MxDQty > 0 || _locMaterialRequisitionLine.MxQty > 0)
                                {
                                    if (_locMaterialRequisitionLine.DQty > 0 && _locMaterialRequisitionLine.DQty <= _locMaterialRequisitionLine.MxDQty)
                                    {
                                        _locRmDQty = _locMaterialRequisitionLine.MxDQty - _locMaterialRequisitionLine.DQty;
                                    }

                                    if (_locMaterialRequisitionLine.Qty > 0 && _locMaterialRequisitionLine.Qty <= _locMaterialRequisitionLine.MxQty)
                                    {
                                        _locRmQty = _locMaterialRequisitionLine.MxQty - _locMaterialRequisitionLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locMaterialRequisitionLine.Item),
                                                                new BinaryOperator("UOM", _locMaterialRequisitionLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locMaterialRequisitionLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty + _locRmDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }

                                }
                                #endregion MaxQuantity
                                #region NonMaxQuantity
                                else
                                {
                                    if (_locMaterialRequisitionLine.DQty > 0)
                                    {
                                        _locRmDQty = _locMaterialRequisitionLine.DQty;
                                    }

                                    if (_locMaterialRequisitionLine.Qty > 0)
                                    {
                                        _locRmQty = _locMaterialRequisitionLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locMaterialRequisitionLine.Item),
                                                                new BinaryOperator("UOM", _locMaterialRequisitionLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locMaterialRequisitionLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty + _locRmDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion ProcessCount=0

                            #region ProcessCount>0
                            if (_locMaterialRequisitionLine.ProcessCount > 0)
                            {
                                if (_locMaterialRequisitionLine.RmDQty > 0)
                                {
                                    _locRmDQty = _locMaterialRequisitionLine.RmDQty - _locMaterialRequisitionLine.DQty;
                                }

                                if (_locMaterialRequisitionLine.RmQty > 0)
                                {
                                    _locRmQty = _locMaterialRequisitionLine.RmQty - _locMaterialRequisitionLine.Qty;
                                }

                                if (_locMaterialRequisitionLine.MxDQty > 0 || _locMaterialRequisitionLine.MxQty > 0)
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locMaterialRequisitionLine.Item),
                                                                new BinaryOperator("UOM", _locMaterialRequisitionLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locMaterialRequisitionLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                }
                                else
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locMaterialRequisitionLine.Item),
                                                                new BinaryOperator("UOM", _locMaterialRequisitionLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locMaterialRequisitionLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                }


                                if (_locItemUOM != null)
                                {
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locRmQty + _locRmDQty;
                                }

                            }
                            #endregion ProcessCount>0

                            _locMaterialRequisitionLine.RmDQty = _locRmDQty;
                            _locMaterialRequisitionLine.RmQty = _locRmQty;
                            _locMaterialRequisitionLine.RmTQty = _locInvLineTotal;
                            _locMaterialRequisitionLine.Save();
                            _locMaterialRequisitionLine.Session.CommitTransaction();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = MaterialRequisition " + ex.ToString());
            }
        }

        //Menentukan jumlah quantity yang di posting -> Done 10 Dec 2019
        private void SetPostingQty(Session _currSession, MaterialRequisition _locMaterialRequisitionXPO)
        {
            try
            {
                if (_locMaterialRequisitionXPO != null)
                {
                    XPCollection<MaterialRequisitionLine> _locMaterialRequisitionLines = new XPCollection<MaterialRequisitionLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("MaterialRequisition", _locMaterialRequisitionXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locMaterialRequisitionLines != null && _locMaterialRequisitionLines.Count > 0)
                    {
                        double _locPDQty = 0;
                        double _locPQty = 0;
                        double _locInvLineTotal = 0;
                        ItemUnitOfMeasure _locItemUOM = null;

                        foreach (MaterialRequisitionLine _locMaterialRequisitionLine in _locMaterialRequisitionLines)
                        {
                            #region ProcessCount=0
                            if (_locMaterialRequisitionLine.ProcessCount == 0)
                            {
                                #region MaxQuantity
                                if (_locMaterialRequisitionLine.MxDQty > 0 || _locMaterialRequisitionLine.MxQty > 0)
                                {
                                    if (_locMaterialRequisitionLine.DQty > 0 && _locMaterialRequisitionLine.DQty <= _locMaterialRequisitionLine.MxDQty)
                                    {
                                        _locPDQty = _locMaterialRequisitionLine.DQty;
                                    }

                                    if (_locMaterialRequisitionLine.Qty > 0 && _locMaterialRequisitionLine.Qty <= _locMaterialRequisitionLine.MxQty)
                                    {
                                        _locPQty = _locMaterialRequisitionLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locMaterialRequisitionLine.Item),
                                                                new BinaryOperator("UOM", _locMaterialRequisitionLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locMaterialRequisitionLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty + _locPDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }

                                }
                                #endregion MaxQuantity
                                #region NonMaxQuantity
                                else
                                {
                                    if (_locMaterialRequisitionLine.DQty > 0)
                                    {
                                        _locPDQty = _locMaterialRequisitionLine.DQty;
                                    }

                                    if (_locMaterialRequisitionLine.Qty > 0)
                                    {
                                        _locPQty = _locMaterialRequisitionLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locMaterialRequisitionLine.Item),
                                                                new BinaryOperator("UOM", _locMaterialRequisitionLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locMaterialRequisitionLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty + _locPDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion ProcessCount=0

                            #region ProcessCount>0
                            if (_locMaterialRequisitionLine.ProcessCount > 0)
                            {
                                if (_locMaterialRequisitionLine.PDQty > 0)
                                {
                                    _locPDQty = _locMaterialRequisitionLine.PDQty + _locMaterialRequisitionLine.DQty;
                                }

                                if (_locMaterialRequisitionLine.PQty > 0)
                                {
                                    _locPQty = _locMaterialRequisitionLine.PQty + _locMaterialRequisitionLine.Qty;
                                }

                                if (_locMaterialRequisitionLine.MxDQty > 0 || _locMaterialRequisitionLine.MxQty > 0)
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                           (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Item", _locMaterialRequisitionLine.Item),
                                                            new BinaryOperator("UOM", _locMaterialRequisitionLine.MxUOM),
                                                            new BinaryOperator("DefaultUOM", _locMaterialRequisitionLine.MxDUOM),
                                                            new BinaryOperator("Active", true)));
                                }
                                else
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                           (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Item", _locMaterialRequisitionLine.Item),
                                                            new BinaryOperator("UOM", _locMaterialRequisitionLine.UOM),
                                                            new BinaryOperator("DefaultUOM", _locMaterialRequisitionLine.DUOM),
                                                            new BinaryOperator("Active", true)));
                                }

                                if (_locItemUOM != null)
                                {
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locPQty + _locPDQty;
                                }

                            }
                            #endregion ProcessCount>0

                            _locMaterialRequisitionLine.PDQty = _locPDQty;
                            _locMaterialRequisitionLine.PDUOM = _locMaterialRequisitionLine.DUOM;
                            _locMaterialRequisitionLine.PQty = _locPQty;
                            _locMaterialRequisitionLine.PUOM = _locMaterialRequisitionLine.UOM;
                            _locMaterialRequisitionLine.PTQty = _locInvLineTotal;
                            _locMaterialRequisitionLine.Save();
                            _locMaterialRequisitionLine.Session.CommitTransaction();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = MaterialRequisition " + ex.ToString());
            }

        }

        //Menentukan Banyak process issue
        private void SetProcessCount(Session _currSession, MaterialRequisition _locMaterialRequisitionXPO)
        {
            try
            {
                if (_locMaterialRequisitionXPO != null)
                {
                    XPCollection<MaterialRequisitionLine> _locMaterialRequisitionLines = new XPCollection<MaterialRequisitionLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("MaterialRequisition", _locMaterialRequisitionXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locMaterialRequisitionLines != null && _locMaterialRequisitionLines.Count > 0)
                    {

                        foreach (MaterialRequisitionLine _locMaterialRequisitionLine in _locMaterialRequisitionLines)
                        {
                            if (_locMaterialRequisitionLine.Status == Status.Progress || _locMaterialRequisitionLine.Status == Status.Posted)
                            {
                                if (_locMaterialRequisitionLine.DQty > 0 || _locMaterialRequisitionLine.Qty > 0)
                                {
                                    _locMaterialRequisitionLine.ProcessCount = _locMaterialRequisitionLine.ProcessCount + 1;
                                    _locMaterialRequisitionLine.Save();
                                    _locMaterialRequisitionLine.Session.CommitTransaction();
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = MaterialRequisition " + ex.ToString());
            }
        }

        //Membuat Purchase Order
        private void SetPurchaseOrder(Session _currSession, MaterialRequisition _locMaterialRequisitionXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _currSignCode = null;
                
                if (_locMaterialRequisitionXPO != null)
                {
                    _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.PurchaseOrder);

                    if (_currSignCode != null)
                    {
                        PurchaseOrder _saveDataPO = new PurchaseOrder(_currSession)
                        {
                            SignCode = _currSignCode,
                            Company = _locMaterialRequisitionXPO.Company,
                            MaterialRequisition = _locMaterialRequisitionXPO,
                        };
                        _saveDataPO.Save();
                        _saveDataPO.Session.CommitTransaction();

                        XPCollection<MaterialRequisitionLine> _numLineMaterialRequisitionLines = new XPCollection<MaterialRequisitionLine>(_currSession,
                                                                                new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("MaterialRequisition", _locMaterialRequisitionXPO),
                                                                                new BinaryOperator("Select", true)));

                        PurchaseOrder _locPurchaseOrder2 = _currSession.FindObject<PurchaseOrder>(new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("SignCode", _currSignCode),
                                                            new BinaryOperator("MaterialRequisition", _locMaterialRequisitionXPO)));
                        if (_locPurchaseOrder2 != null)
                        {
                            if (_numLineMaterialRequisitionLines != null && _numLineMaterialRequisitionLines.Count > 0)
                            {
                                foreach (MaterialRequisitionLine _numLineMaterialRequisitionLine in _numLineMaterialRequisitionLines)
                                {
                                    if (_numLineMaterialRequisitionLine.Status == Status.Progress || _numLineMaterialRequisitionLine.Status == Status.Posted)
                                    {
                                        PurchaseOrderLine _saveDataPurchaseOrderLine = new PurchaseOrderLine(_currSession)
                                        {
                                            
                                            Item = _numLineMaterialRequisitionLine.Item,
                                            Description = _numLineMaterialRequisitionLine.Description,
                                            MxDQty = _numLineMaterialRequisitionLine.DQty,
                                            MxDUOM = _numLineMaterialRequisitionLine.DUOM,
                                            MxQty = _numLineMaterialRequisitionLine.Qty,
                                            MxUOM = _numLineMaterialRequisitionLine.UOM,
                                            MxTQty = _numLineMaterialRequisitionLine.MxTQty,
                                            DQty = _numLineMaterialRequisitionLine.DQty,
                                            DUOM = _numLineMaterialRequisitionLine.DUOM,
                                            Qty = _numLineMaterialRequisitionLine.Qty,
                                            UOM = _numLineMaterialRequisitionLine.UOM,
                                            TQty = _numLineMaterialRequisitionLine.TQty,
                                            Company = _numLineMaterialRequisitionLine.Company,
                                            PurchaseOrder = _locPurchaseOrder2,
                                        };
                                        _saveDataPurchaseOrderLine.Save();
                                        _saveDataPurchaseOrderLine.Session.CommitTransaction();

                                    }
                                }
                            }
                        }
                    }

                   
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = MaterialRequisition ", ex.ToString());
            }
        }

        //Menentukan Status Issue Pada Inventory Transfer Line
        private void SetStatusMaterialRequisitionLine(Session _currSession, MaterialRequisition _locMaterialRequisitionXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locMaterialRequisitionXPO != null)
                {
                    XPCollection<MaterialRequisitionLine> _locMaterialRequisitionLines = new XPCollection<MaterialRequisitionLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("MaterialRequisition", _locMaterialRequisitionXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locMaterialRequisitionLines != null && _locMaterialRequisitionLines.Count > 0)
                    {

                        foreach (MaterialRequisitionLine _locMaterialRequisitionLine in _locMaterialRequisitionLines)
                        {
                            if (_locMaterialRequisitionLine.Status == Status.Progress || _locMaterialRequisitionLine.Status == Status.Posted)
                            {
                                if (_locMaterialRequisitionLine.DQty > 0 || _locMaterialRequisitionLine.Qty > 0)
                                {
                                    if (_locMaterialRequisitionLine.RmDQty == 0 && _locMaterialRequisitionLine.RmQty == 0 && _locMaterialRequisitionLine.RmTQty == 0)
                                    {
                                        _locMaterialRequisitionLine.Status = Status.Close;
                                        _locMaterialRequisitionLine.ActivationPosting = true;
                                        _locMaterialRequisitionLine.StatusDate = now;
                                    }
                                    else
                                    {
                                        _locMaterialRequisitionLine.Status = Status.Posted;
                                        _locMaterialRequisitionLine.StatusDate = now;
                                    }
                                    _locMaterialRequisitionLine.Save();
                                    _locMaterialRequisitionLine.Session.CommitTransaction();
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = MaterialRequisition " + ex.ToString());
            }
        }

        //Menormalkan Quantity
        private void SetNormalQuantity(Session _currSession, MaterialRequisition _locMaterialRequisitionXPO)
        {
            try
            {
                if (_locMaterialRequisitionXPO != null)
                {
                    XPCollection<MaterialRequisitionLine> _locMaterialRequisitionLines = new XPCollection<MaterialRequisitionLine>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("MaterialRequisition", _locMaterialRequisitionXPO),
                                                                                     new BinaryOperator("Select", true)));

                    if (_locMaterialRequisitionLines != null && _locMaterialRequisitionLines.Count > 0)
                    {
                        foreach (MaterialRequisitionLine _locMaterialRequisitionLine in _locMaterialRequisitionLines)
                        {
                            if (_locMaterialRequisitionLine.Status == Status.Progress || _locMaterialRequisitionLine.Status == Status.Posted || _locMaterialRequisitionLine.Status == Status.Close)
                            {
                                if (_locMaterialRequisitionLine.DQty > 0 || _locMaterialRequisitionLine.Qty > 0)
                                {
                                    _locMaterialRequisitionLine.Select = false;
                                    _locMaterialRequisitionLine.DQty = 0;
                                    _locMaterialRequisitionLine.Qty = 0;
                                    _locMaterialRequisitionLine.Save();
                                    _locMaterialRequisitionLine.Session.CommitTransaction();
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = MaterialRequisition " + ex.ToString());
            }
        }

        //Menentukan Status Issue Pada Inventory Transfer
        private void SetStatusMaterialRequisition(Session _currSession, MaterialRequisition _locMaterialRequisitionXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locCount = 0;

                if (_locMaterialRequisitionXPO != null)
                {
                    XPCollection<MaterialRequisitionLine> _locMaterialRequisitionLines = new XPCollection<MaterialRequisitionLine>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("MaterialRequisition", _locMaterialRequisitionXPO)));

                    if (_locMaterialRequisitionLines != null && _locMaterialRequisitionLines.Count() > 0)
                    {
                        foreach (MaterialRequisitionLine _locMaterialRequisitionLine in _locMaterialRequisitionLines)
                        {
                            if (_locMaterialRequisitionLine.Status == Status.Close)
                            {
                                _locCount = _locCount + 1;
                            }
                        }

                        if (_locCount == _locMaterialRequisitionLines.Count())
                        {
                            _locMaterialRequisitionXPO.ActivationPosting = true;
                            _locMaterialRequisitionXPO.Status = Status.Close;
                            _locMaterialRequisitionXPO.StatusDate = now;
                            _locMaterialRequisitionXPO.Save();
                            _locMaterialRequisitionXPO.Session.CommitTransaction();
                        }else
                        {
                            _locMaterialRequisitionXPO.Status = Status.Posted;
                            _locMaterialRequisitionXPO.StatusDate = now;
                            _locMaterialRequisitionXPO.Save();
                            _locMaterialRequisitionXPO.Session.CommitTransaction();
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = MaterialRequisition " + ex.ToString());
            }
        }

        #endregion Posting

        #region Global Method

        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0} !", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }


        #endregion Global Method

    }
}
