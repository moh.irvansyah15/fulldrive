﻿namespace FullDrive.Module.Controllers
{
    partial class InventoryTransferOrderApprovalActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.InventoryTransferOrderApprovalAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // InventoryTransferOrderApprovalAction
            // 
            this.InventoryTransferOrderApprovalAction.Caption = "Approval";
            this.InventoryTransferOrderApprovalAction.ConfirmationMessage = null;
            this.InventoryTransferOrderApprovalAction.Id = "InventoryTransferOrderApprovalActionId";
            this.InventoryTransferOrderApprovalAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.InventoryTransferOrderApprovalAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.InventoryTransferOrder);
            this.InventoryTransferOrderApprovalAction.ToolTip = null;
            this.InventoryTransferOrderApprovalAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.InventoryTransferOrderApprovalAction_Execute);
            // 
            // InventoryTransferOrderApprovalActionController
            // 
            this.Actions.Add(this.InventoryTransferOrderApprovalAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SingleChoiceAction InventoryTransferOrderApprovalAction;
    }
}
