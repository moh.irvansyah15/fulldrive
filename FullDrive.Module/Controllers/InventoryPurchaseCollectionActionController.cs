﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using System.Collections;
using DevExpress.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class InventoryPurchaseCollectionActionController : ViewController
    {
        public InventoryPurchaseCollectionActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void InventoryPurchaseCollectionShowPOMAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
            var ListView = View as ListView;
            IObjectSpace os = Application.CreateObjectSpace();
            string listViewId = Application.FindListViewId(typeof(PurchaseOrderMonitoring));
            CollectionSourceBase cs = Application.CreateCollectionSource(os, typeof(PurchaseOrderMonitoring), listViewId);
            //Tinggal buat 
            ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
            string _stringPO = null;
            if (_objectsToProcess != null)
            {
                foreach (Object obj in _objectsToProcess)
                {
                    InventoryPurchaseCollection _locInventoryPurchaseCollection = (InventoryPurchaseCollection)_objectSpace.GetObject(obj);
                    if (_locInventoryPurchaseCollection != null)
                    {
                        if (_locInventoryPurchaseCollection.PurchaseOrder != null)
                        {
                            if (_stringPO == null)
                            {
                                if (_locInventoryPurchaseCollection.PurchaseOrder.Code != null)
                                {
                                    _stringPO = "( [PurchaseOrder.Code]=='" + _locInventoryPurchaseCollection.PurchaseOrder.Code + "' AND [PostedCount] == 0 )";
                                }
                            }
                            else
                            {
                                if (_locInventoryPurchaseCollection.PurchaseOrder.Code != null)
                                {
                                    _stringPO = _stringPO + " OR ( [PurchaseOrder.Code]=='" + _locInventoryPurchaseCollection.PurchaseOrder.Code + "' AND [PostedCount] == 0 )";
                                }
                            }
                        }
                    }
                }
            }
            if (_stringPO != null)
            {
                cs.Criteria["InventoryPurchaseCollectionFilter"] = CriteriaOperator.Parse(_stringPO);
            }
            e.ShowViewParameters.CreatedView = Application.CreateListView(
                listViewId,
                cs,
                true
            );
            e.ShowViewParameters.Context = TemplateContext.View;
            e.ShowViewParameters.TargetWindow = TargetWindow.NewModalWindow;
            DialogController dc = Application.CreateController<DialogController>();
            dc.Accepting += new EventHandler<DialogControllerAcceptingEventArgs>(dc_Accepting);
            e.ShowViewParameters.Controllers.Add(dc);
        }

        void dc_Accepting(object sender, DialogControllerAcceptingEventArgs e)
        {
            try
            {
                InventoryTransferIn _locInventoryTransferIn = null;
                View popupView = ((Controller)sender).Frame.View;
                ArrayList _objectsToProcess = new ArrayList(View.SelectedObjects);
                if (_objectsToProcess != null)
                {
                    foreach (Object _obj in _objectsToProcess)
                    {
                        InventoryPurchaseCollection _locInvPurchaseCollection = (InventoryPurchaseCollection)popupView.ObjectSpace.GetObject(_obj);
                        if (_locInvPurchaseCollection != null)
                        {
                            if (_locInvPurchaseCollection.InventoryTransferIn != null)
                            {
                                _locInventoryTransferIn = _locInvPurchaseCollection.InventoryTransferIn;
                            }
                        }
                    }
                }
                if (_locInventoryTransferIn != null)
                {
                    ArrayList _objectsToProcess2 = new ArrayList(popupView.SelectedObjects);
                    if (_objectsToProcess2 != null)
                    {
                        foreach (Object _obj2 in _objectsToProcess2)
                        {
                            PurchaseOrderMonitoring _locPurchaseOrderMonitoring = (PurchaseOrderMonitoring)popupView.ObjectSpace.GetObject(_obj2);
                            if (_locPurchaseOrderMonitoring != null && _locPurchaseOrderMonitoring.Select == true && _locPurchaseOrderMonitoring.PostedCount == 0)
                            {
                                _locPurchaseOrderMonitoring.InventoryTransferIn = _locInventoryTransferIn;
                            }
                        }
                    }
                }
                popupView.ObjectSpace.CommitChanges();
            }
            catch (ValidationException)
            {
            }
        }

        private void InventoryPurchaseCollectionShowPRMAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
            var ListView = View as ListView;
            IObjectSpace os = Application.CreateObjectSpace();
            string listViewId = Application.FindListViewId(typeof(PurchaseReturnMonitoring));
            CollectionSourceBase cs = Application.CreateCollectionSource(os, typeof(PurchaseReturnMonitoring), listViewId);
            //Tinggal buat 
            ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
            string _stringPR = null;
            if (_objectsToProcess != null)
            {
                foreach (Object obj in _objectsToProcess)
                {
                    InventoryPurchaseCollection _locInventoryPurchaseCollection = (InventoryPurchaseCollection)_objectSpace.GetObject(obj);
                    if (_locInventoryPurchaseCollection != null)
                    {
                        if (_locInventoryPurchaseCollection.PurchaseReturn != null)
                        {
                            if (_stringPR == null)
                            {
                                if (_locInventoryPurchaseCollection.PurchaseReturn.Code != null)
                                {
                                    _stringPR = "( [PurchaseReturn.Code]=='" + _locInventoryPurchaseCollection.PurchaseReturn.Code + "' )";
                                }
                            }
                            else
                            {
                                if (_locInventoryPurchaseCollection.PurchaseReturn.Code != null)
                                {
                                    _stringPR = _stringPR + " OR ( [PurchaseReturn.Code]=='" + _locInventoryPurchaseCollection.PurchaseReturn.Code + "' )";
                                }
                            }
                        }
                    }
                }
            }
            if (_stringPR != null)
            {
                cs.Criteria["InventoryPurchaseCollectionFilter"] = CriteriaOperator.Parse(_stringPR);
            }
            e.ShowViewParameters.CreatedView = Application.CreateListView(
                listViewId,
                cs,
                true
            );
            e.ShowViewParameters.Context = TemplateContext.View;
            e.ShowViewParameters.TargetWindow = TargetWindow.NewModalWindow;
            DialogController dc = Application.CreateController<DialogController>();
            dc.Accepting += new EventHandler<DialogControllerAcceptingEventArgs>(dc_Accepting2);
            e.ShowViewParameters.Controllers.Add(dc);
        }

        void dc_Accepting2(object sender, DialogControllerAcceptingEventArgs e)
        {
            try
            {
                InventoryTransferOut _locInventoryTransferOut = null;
                View popupView = ((Controller)sender).Frame.View;
                ArrayList _objectsToProcess = new ArrayList(View.SelectedObjects);
                if (_objectsToProcess != null)
                {
                    foreach (Object _obj in _objectsToProcess)
                    {
                        InventoryPurchaseCollection _locInventoryPurchaseCollection = (InventoryPurchaseCollection)popupView.ObjectSpace.GetObject(_obj);
                        if (_locInventoryPurchaseCollection != null)
                        {
                            if (_locInventoryPurchaseCollection.InventoryTransferOut != null)
                            {
                                _locInventoryTransferOut = _locInventoryPurchaseCollection.InventoryTransferOut;
                            }
                        }
                    }
                }
                if (_locInventoryTransferOut != null)
                {
                    ArrayList _objectsToProcess2 = new ArrayList(popupView.SelectedObjects);
                    if (_objectsToProcess2 != null)
                    {
                        foreach (Object _obj2 in _objectsToProcess2)
                        {
                            PurchaseReturnMonitoring _locPurchaseReturnMonitoring = (PurchaseReturnMonitoring)popupView.ObjectSpace.GetObject(_obj2);
                            if (_locPurchaseReturnMonitoring != null && _locPurchaseReturnMonitoring.Select == true && _locPurchaseReturnMonitoring.InventoryStatus != Status.Close)
                            {
                                _locPurchaseReturnMonitoring.InventoryTransferOut = _locInventoryTransferOut;
                            }
                        }
                    }
                }
                popupView.ObjectSpace.CommitChanges();
            }
            catch (ValidationException)
            {
            }
        }

    }
}
