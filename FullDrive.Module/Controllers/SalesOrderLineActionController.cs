﻿#region Default

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

#endregion Default

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class SalesOrderLineActionController : ViewController
    {
        #region Default
        private ChoiceActionItem _selectionListviewFilter;
        public SalesOrderLineActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            #region FilterStatus
            SalesOrderLineListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                if (_ed.GetCaption(_currApproval) != "Approved" && _ed.GetCaption(_currApproval) != "Lock")
                {
                    _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                }
                SalesOrderLineListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
            #endregion FilterStatus
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        #endregion Default

        private void SalesOrderLineTaxCalculationAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        SalesOrderLine _locSalesOrderLineOS = (SalesOrderLine)_objectSpace.GetObject(obj);

                        if (_locSalesOrderLineOS != null)
                        {
                            if (_locSalesOrderLineOS.TQty != 0 && _locSalesOrderLineOS.UAmount != 0)
                            {
                                if (_locSalesOrderLineOS.Code != null)
                                {
                                    _currObjectId = _locSalesOrderLineOS.Code;

                                    SalesOrderLine _locSalesOrderLineXPO = _currSession.FindObject<SalesOrderLine>
                                                                           (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Code", _currObjectId)));

                                    if (_locSalesOrderLineXPO != null)
                                    {
                                        GetSumTotalTax(_currSession, _locSalesOrderLineXPO);
                                        GetTotalPPNPPH(_currSession, _locSalesOrderLineXPO);
                                    }
                                }
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = SalesOrderLine" + ex.ToString());
            }
        }

        private void SalesOrderLineDiscCalculationAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        SalesOrderLine _locSalesOrderLineOS = (SalesOrderLine)_objectSpace.GetObject(obj);

                        if (_locSalesOrderLineOS != null)
                        {
                            if (_locSalesOrderLineOS.TQty != 0 && _locSalesOrderLineOS.UAmount != 0)
                            {
                                if (_locSalesOrderLineOS.Code != null)
                                {
                                    _currObjectId = _locSalesOrderLineOS.Code;

                                    SalesOrderLine _locSalesOrderLineXPO = _currSession.FindObject<SalesOrderLine>
                                                                           (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Code", _currObjectId)));

                                    if (_locSalesOrderLineXPO != null)
                                    {
                                        GetSumTotalDisc(_currSession, _locSalesOrderLineXPO);
                                    }
                                }
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = SalesOrderLine" + ex.ToString());
            }
        }

        private void SalesOrderLineListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(SalesOrderLine)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrderLine " + ex.ToString());
            }
        }

        private void SalesOrderLineSelectAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        SalesOrderLine _locSalesOrderLineOS = (SalesOrderLine)_objectSpace.GetObject(obj);

                        if (_locSalesOrderLineOS != null)
                        {
                            if (_locSalesOrderLineOS.Code != null)
                            {
                                _currObjectId = _locSalesOrderLineOS.Code;

                                XPCollection<SalesOrderLine> _locSalesOrderLines = new XPCollection<SalesOrderLine>
                                                                                                     (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                      new BinaryOperator("Code", _currObjectId),
                                                                                                      new GroupOperator(GroupOperatorType.Or,
                                                                                                      new BinaryOperator("Status", Status.Posted),
                                                                                                      new BinaryOperator("Status", Status.Progress),
                                                                                                      new BinaryOperator("Status", Status.Open)
                                                                                                      )));

                                if (_locSalesOrderLines != null && _locSalesOrderLines.Count > 0)
                                {
                                    foreach (SalesOrderLine _locSalesOrderLine in _locSalesOrderLines)
                                    {
                                        _locSalesOrderLine.Select = true;
                                        _locSalesOrderLine.Save();
                                        _locSalesOrderLine.Session.CommitTransaction();
                                    }

                                    SuccessMessageShow("SalesOrderLine has been Select");
                                }
                                else
                                {
                                    ErrorMessageShow("Data SalesOrderLine Not Available");
                                }
                            }
                        }
                    }
                    if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                    {
                        _objectSpace.CommitChanges();
                        _objectSpace.Refresh();
                    }
                    if (View is ListView)
                    {
                        _objectSpace.CommitChanges();
                        View.ObjectSpace.Refresh();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = SalesOrderLine" + ex.ToString());
            }
        }

        private void SalesOrderLineUnselectAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        SalesOrderLine _locSalesOrderLineOS = (SalesOrderLine)_objectSpace.GetObject(obj);

                        if (_locSalesOrderLineOS != null)
                        {
                            if (_locSalesOrderLineOS.Code != null)
                            {
                                _currObjectId = _locSalesOrderLineOS.Code;

                                XPCollection<SalesOrderLine> _locSalesOrderLines = new XPCollection<SalesOrderLine>
                                                                                                     (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                      new BinaryOperator("Code", _currObjectId),
                                                                                                      new GroupOperator(GroupOperatorType.Or,
                                                                                                      new BinaryOperator("Status", Status.Posted),
                                                                                                      new BinaryOperator("Status", Status.Progress),
                                                                                                      new BinaryOperator("Status", Status.Open)
                                                                                                      )));

                                if (_locSalesOrderLines != null && _locSalesOrderLines.Count > 0)
                                {
                                    foreach (SalesOrderLine _locSalesOrderLine in _locSalesOrderLines)
                                    {
                                        _locSalesOrderLine.Select = false;
                                        _locSalesOrderLine.Save();
                                        _locSalesOrderLine.Session.CommitTransaction();
                                    }

                                    SuccessMessageShow("SalesOrderLine has been Unselect");
                                }
                                else
                                {
                                    ErrorMessageShow("Data SalesOrderLine Not Available");
                                }
                            }
                        }
                    }
                    if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                    {
                        _objectSpace.CommitChanges();
                        _objectSpace.Refresh();
                    }
                    if (View is ListView)
                    {
                        _objectSpace.CommitChanges();
                        View.ObjectSpace.Refresh();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = SalesOrderLine" + ex.ToString());
            }
        }

        //================================== Code Only ====================================

        #region Get

        private void GetSumTotalTax(Session _currSession, SalesOrderLine _salesOrderLineXPO)
        {
            try
            {
                double _locTxAmount = 0;
                double _locTxValue = 0;

                if (_salesOrderLineXPO != null)
                {
                    XPCollection<TaxLine> _locTaxLines = new XPCollection<TaxLine>(_currSession,
                                                     new BinaryOperator("SalesOrderLine", _salesOrderLineXPO));

                    if (_locTaxLines.Count() > 0)
                    {
                        foreach (TaxLine _locTaxLine in _locTaxLines)
                        {
                            if (_locTaxLine.TxValue > 0 && _locTaxLine.TxAmount > 0)
                            {
                                if (_locTaxLine.TaxNature == TaxNature.Increase)
                                {
                                    _locTxAmount = _locTxAmount + _locTaxLine.TxAmount;
                                    _locTxValue = _locTxValue + _locTaxLine.TxValue;

                                }
                                else if (_locTaxLine.TaxNature == TaxNature.Decrease)
                                {
                                    _locTxAmount = _locTxAmount - _locTaxLine.TxAmount;
                                    _locTxValue = _locTxValue - _locTaxLine.TxValue;
                                }
                            }
                        }
                        _salesOrderLineXPO.TxValue = _locTxValue;
                        _salesOrderLineXPO.TxAmount = _locTxAmount;
                        _salesOrderLineXPO.Save();
                        _salesOrderLineXPO.Session.CommitTransaction();
                    }
                }


            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = SalesOrderLine" + ex.ToString());
            }
        }

        private void GetSumTotalDisc(Session _currSession, SalesOrderLine _salesOrderLineXPO)
        {
            try
            {
                double _locDisc = 0;
                double _locDiscAmount = 0;

                if (_salesOrderLineXPO != null)
                {
                    XPCollection<DiscountLine> _locDiscountLines = new XPCollection<DiscountLine>(_currSession,
                                                               new BinaryOperator("SalesOrderLine", _salesOrderLineXPO));

                    if (_locDiscountLines.Count() > 0)
                    {
                        foreach (DiscountLine _locDiscountLine in _locDiscountLines)
                        {
                            if (_locDiscountLine.Disc > 0 && _locDiscountLine.DiscAmount > 0)
                            {
                                if (_locDiscountLine.Disc > 0 && _locDiscountLine.DiscAmount > 0)
                                {
                                    _locDisc = _locDisc + _locDiscountLine.Disc;
                                    _locDiscAmount = _locDiscAmount + _locDiscountLine.DiscAmount;
                                }
                            }
                        }
                        _salesOrderLineXPO.Disc = _locDisc;
                        _salesOrderLineXPO.DiscAmount = _locDiscAmount;
                        _salesOrderLineXPO.Save();
                        _salesOrderLineXPO.Session.CommitTransaction();
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = SalesOrderLine" + ex.ToString());
            }
        }

        private void GetTotalPPNPPH(Session _currSession, SalesOrderLine _locSalesOrderLineXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                double _locTotalPPN = 0;
                double _locTotalPPH = 0;
                double _locTotalNPWP = 0;
                double _locTotalNonNPWP = 0;
                double _locTotalMaterai = 0;

                if (_locSalesOrderLineXPO != null)
                {
                    if (_locSalesOrderLineXPO.TaxLines != null)
                    {
                        XPQuery<TaxLine> _taxLinesQuery = new XPQuery<TaxLine>(_currSession);

                        var _taxLines = from txl in _taxLinesQuery
                                        where (txl.SalesOrderLine == _locSalesOrderLineXPO)
                                        group txl by txl.TaxGroup.TaxGroupType into g
                                        select new { TaxGroupType = g.Key };

                        if (_taxLines != null && _taxLines.Count() > 0)
                        {
                            foreach (var _taxLine in _taxLines)
                            {
                                XPCollection<TaxLine> _locTaxLines = new XPCollection<TaxLine>(_currSession,
                                                                     new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("SalesOrderLine", _locSalesOrderLineXPO),
                                                                     new BinaryOperator("TaxGroupType", _taxLine.TaxGroupType)));

                                if (_locTaxLines != null && _locTaxLines.Count > 0)
                                {
                                    foreach (TaxLine _locTaxLine in _locTaxLines)
                                    {
                                        if (_locTaxLine.TaxGroupType == TaxGroupType.PPN)
                                        {
                                            _locTotalPPN = _locTotalPPN + _locTaxLine.TxAmount;
                                        }
                                        if (_locTaxLine.TaxGroupType == TaxGroupType.PPH)
                                        {
                                            _locTotalPPH = _locTotalPPH + _locTaxLine.TxAmount;
                                        }
                                        if (_locTaxLine.TaxGroupType == TaxGroupType.NPWP)
                                        {
                                            _locTotalNPWP = _locTotalNPWP + _locTaxLine.TxAmount;
                                        }
                                        if (_locTaxLine.TaxGroupType == TaxGroupType.NonNPWP)
                                        {
                                            _locTotalNonNPWP = _locTotalNonNPWP + _locTaxLine.TxAmount;
                                        }
                                        if (_locTaxLine.TaxGroupType == TaxGroupType.Materai)
                                        {
                                            _locTotalMaterai = _locTotalMaterai + _locTaxLine.TxAmount;
                                        }
                                    }

                                    _locSalesOrderLineXPO.TotalPPN = _locTotalPPN;
                                    _locSalesOrderLineXPO.TotalPPH = _locTotalPPH;
                                    _locSalesOrderLineXPO.TotalNPWP = _locTotalNPWP;
                                    _locSalesOrderLineXPO.TotalNonNPWP = _locTotalNonNPWP;
                                    _locSalesOrderLineXPO.TotalMaterai = _locTotalMaterai;
                                    _locSalesOrderLineXPO.Save();
                                    _locSalesOrderLineXPO.Session.CommitTransaction();
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = SalesOrderLine" + ex.ToString());
            }
        }

        #endregion Get

        #region Global Method

        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0}", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }




        #endregion Global Method

        
    }
}
