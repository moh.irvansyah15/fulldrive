﻿namespace FullDrive.Module.Controllers
{
    partial class WorkRequisitionActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.WorkRequisitionProgressAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.WorkRequisitionListviewFilterSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.WorkRequisitionPostingAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.WorkRequisitionListviewFilterApprovalSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // WorkRequisitionProgressAction
            // 
            this.WorkRequisitionProgressAction.Caption = "Progress";
            this.WorkRequisitionProgressAction.ConfirmationMessage = null;
            this.WorkRequisitionProgressAction.Id = "WorkRequisitionProgressActionId";
            this.WorkRequisitionProgressAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.WorkRequisition);
            this.WorkRequisitionProgressAction.ToolTip = null;
            this.WorkRequisitionProgressAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.WorkRequisitionProgressAction_Execute);
            // 
            // WorkRequisitionListviewFilterSelectionAction
            // 
            this.WorkRequisitionListviewFilterSelectionAction.Caption = "Filter";
            this.WorkRequisitionListviewFilterSelectionAction.ConfirmationMessage = null;
            this.WorkRequisitionListviewFilterSelectionAction.Id = "WorkRequisitionListviewFilterSelectionActionId";
            this.WorkRequisitionListviewFilterSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.WorkRequisitionListviewFilterSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.WorkRequisition);
            this.WorkRequisitionListviewFilterSelectionAction.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.WorkRequisitionListviewFilterSelectionAction.ToolTip = null;
            this.WorkRequisitionListviewFilterSelectionAction.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.WorkRequisitionListviewFilterSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.WorkRequisitionListviewFilterSelectionAction_Execute);
            // 
            // WorkRequisitionPostingAction
            // 
            this.WorkRequisitionPostingAction.Caption = "Posting";
            this.WorkRequisitionPostingAction.ConfirmationMessage = null;
            this.WorkRequisitionPostingAction.Id = "WorkRequisitionPostingActionId";
            this.WorkRequisitionPostingAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.WorkRequisition);
            this.WorkRequisitionPostingAction.ToolTip = null;
            this.WorkRequisitionPostingAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.WorkRequisitionPostingAction_Execute);
            // 
            // WorkRequisitionListviewFilterApprovalSelectionAction
            // 
            this.WorkRequisitionListviewFilterApprovalSelectionAction.Caption = "Filter Approval";
            this.WorkRequisitionListviewFilterApprovalSelectionAction.ConfirmationMessage = null;
            this.WorkRequisitionListviewFilterApprovalSelectionAction.Id = "WorkRequisitionListviewFilterApprovalSelectionActionId";
            this.WorkRequisitionListviewFilterApprovalSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.WorkRequisitionListviewFilterApprovalSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.WorkRequisition);
            this.WorkRequisitionListviewFilterApprovalSelectionAction.ToolTip = null;
            this.WorkRequisitionListviewFilterApprovalSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.WorkRequisitionListviewFilterApprovalSelectionAction_Execute);
            // 
            // WorkRequisitionActionController
            // 
            this.Actions.Add(this.WorkRequisitionProgressAction);
            this.Actions.Add(this.WorkRequisitionListviewFilterSelectionAction);
            this.Actions.Add(this.WorkRequisitionPostingAction);
            this.Actions.Add(this.WorkRequisitionListviewFilterApprovalSelectionAction);
            this.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.WorkRequisition);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction WorkRequisitionProgressAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction WorkRequisitionListviewFilterSelectionAction;
        private DevExpress.ExpressApp.Actions.SimpleAction WorkRequisitionPostingAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction WorkRequisitionListviewFilterApprovalSelectionAction;
    }
}
