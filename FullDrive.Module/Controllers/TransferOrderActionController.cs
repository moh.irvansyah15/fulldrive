﻿#region Default

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

#endregion Default

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;
using System.IO;
using System.Web;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class TransferOrderActionController : ViewController
    {
        #region Default

        private ChoiceActionItem _selectionListviewFilter;
        private ChoiceActionItem _selectionApprovalListviewFilter;
        private ChoiceActionItem _setApprovalLevel;

        public TransferOrderActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.

            #region FilterStatus
            TransferOrderListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                TransferOrderListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
            #endregion FilterStatus

            #region FilterApproval
            TransferOrderListviewFilterApprovalSelectionAction.Items.Clear();
            foreach (object _currApprovalFilter in Enum.GetValues(typeof(CustomProcess.ApprovalFilter)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.ApprovalFilter));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApprovalFilter), _currApprovalFilter);
                TransferOrderListviewFilterApprovalSelectionAction.Items.Add(_selectionListviewFilter);
            }
            #endregion FilterApproval
        }

        protected override void OnActivated()
        {
            TypeOfView = typeof(ObjectView);
            base.OnActivated();
            // Perform various tasks depending on the target View.
            #region Approval
            var _locUser = SecuritySystem.CurrentUserName;
            GlobalFunction _globalFunction = new GlobalFunction();
            Session _currentSession = null;
            if (this.ObjectSpace != null)
            {
                _currentSession = ((XPObjectSpace)this.ObjectSpace).Session;
            }

            if (_currentSession != null)
            {
                UserAccess _locUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", _locUser));

                if (_locUserAccess != null)
                {
                    TransferOrderApprovalAction.Items.Clear();

                    XPCollection<ApplicationSetupDetail> _locAppSetupDetails = new XPCollection<ApplicationSetupDetail>
                                                                                 (_currentSession, new GroupOperator(GroupOperatorType.And,
                                                                                  new BinaryOperator("UserAccess", _locUserAccess),
                                                                                  new BinaryOperator("ObjectList", CustomProcess.ObjectList.TransferOrder),
                                                                                  new BinaryOperator("FunctionList", CustomProcess.FunctionList.Approval),
                                                                                  new BinaryOperator("Active", true)));

                    if (_locAppSetupDetails != null && _locAppSetupDetails.Count > 0)
                    {

                        foreach (ApplicationSetupDetail _locAppSetupDetail in _locAppSetupDetails)
                        {
                            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.ApprovalLevel)))
                            {
                                if (_locAppSetupDetail.ApprovalLevel != CustomProcess.ApprovalLevel.None)
                                {
                                    EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.ApprovalLevel));
                                    _setApprovalLevel = new ChoiceActionItem(_ed.GetCaption(_locAppSetupDetail.ApprovalLevel), _locAppSetupDetail.ApprovalLevel);
                                }
                            }
                            TransferOrderApprovalAction.Items.Add(_setApprovalLevel);
                        }
                    }
                }
            }
            #endregion Approval
        }

        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }

        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        #endregion Default

        private void TransferOrderProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                Status _locStatus = Status.None;
                DateTime _locNow;
                Status _locStatus2 = Status.None;
                DateTime _locNow2;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        TransferOrder _locTransferOrderOS = (TransferOrder)_objectSpace.GetObject(obj);

                        if (_locTransferOrderOS != null)
                        {
                            if (_locTransferOrderOS.Code != null)
                            {
                                _currObjectId = _locTransferOrderOS.Code;

                                TransferOrder _locTransferOrderXPO = _currSession.FindObject<TransferOrder>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locTransferOrderXPO != null)
                                {
                                    if (_locTransferOrderXPO.Status == Status.Open || _locTransferOrderXPO.Status == Status.Progress || _locTransferOrderXPO.Status == Status.Posted)
                                    {
                                        if (_locTransferOrderXPO.Status == Status.Open)
                                        {
                                            _locStatus = Status.Progress;
                                            _locNow = now;
                                        }
                                        else
                                        {
                                            _locStatus = _locTransferOrderXPO.Status;
                                            _locNow = _locTransferOrderXPO.StatusDate;
                                        }
                                        _locTransferOrderXPO.Status = _locStatus;
                                        _locTransferOrderXPO.StatusDate = _locNow;
                                        _locTransferOrderXPO.Save();
                                        _locTransferOrderXPO.Session.CommitTransaction();

                                        #region TransferOrderLine
                                        XPCollection<TransferOrderLine> _locTransferOrderLineLines = new XPCollection<TransferOrderLine>
                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("TransferOrder", _locTransferOrderXPO)));

                                        if (_locTransferOrderLineLines != null && _locTransferOrderLineLines.Count > 0)
                                        {
                                            foreach (TransferOrderLine _locTransferOrderLine in _locTransferOrderLineLines)
                                            {
                                                if (_locTransferOrderLine.Status == Status.Open || _locTransferOrderLine.Status == Status.Progress || _locTransferOrderLine.Status == Status.Posted)
                                                {
                                                    if (_locTransferOrderLine.Status == Status.Open)
                                                    {
                                                        _locStatus2 = Status.Progress;
                                                        _locNow2 = now;
                                                    }
                                                    else
                                                    {
                                                        _locStatus2 = _locTransferOrderLine.Status;
                                                        _locNow2 = _locTransferOrderLine.StatusDate;
                                                    }

                                                    _locTransferOrderLine.Status = _locStatus2;
                                                    _locTransferOrderLine.StatusDate = _locNow2;
                                                    _locTransferOrderLine.Save();
                                                    _locTransferOrderLine.Session.CommitTransaction();
                                                }

                                                XPCollection<TransferOrderLot> _locTransferOrderLots = new XPCollection<TransferOrderLot>
                                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                        new BinaryOperator("TransferOrderLine", _locTransferOrderLine),
                                                                                                        new BinaryOperator("Status", Status.Open)));

                                                if (_locTransferOrderLots != null && _locTransferOrderLots.Count() > 0)
                                                {
                                                    foreach (TransferOrderLot _locTransferOrderLot in _locTransferOrderLots)
                                                    {
                                                        _locTransferOrderLot.Status = Status.Progress;
                                                        _locTransferOrderLot.StatusDate = now;
                                                        _locTransferOrderLot.Save();
                                                        _locTransferOrderLot.Session.CommitTransaction();
                                                    }
                                                }
                                            }
                                        }
                                        #endregion TransferOrderLine
                                        SuccessMessageShow(_locTransferOrderXPO.Code + " has been change successfully to Progress");
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data Transfer Order Not Available");
                                }

                            }
                            else
                            {
                                ErrorMessageShow("Data Transfer Order Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferOrder " + ex.ToString());
            }
        }

        private void TransferOrderPostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        TransferOrder _locTransferOrderOS = (TransferOrder)_objectSpace.GetObject(obj);

                        if (_locTransferOrderOS != null)
                        {
                            if (_locTransferOrderOS.Code != null)
                            {
                                _currObjectId = _locTransferOrderOS.Code;

                                TransferOrder _locTransferOrderXPO = _currSession.FindObject<TransferOrder>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locTransferOrderXPO != null)
                                {
                                    if (_locTransferOrderXPO.Status == Status.Progress || _locTransferOrderXPO.Status == Status.Posted)
                                    {
                                        ApprovalLine _locApprovalLineXPO = _currSession.FindObject<ApprovalLine>
                                                                           (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("EndApproval", true),
                                                                            new BinaryOperator("TransferOrder", _locTransferOrderXPO)));

                                        if (_locApprovalLineXPO != null)
                                        {
                                            SetTransferOrderMonitoring(_currSession, _locTransferOrderXPO);
                                            SetRemainQty(_currSession, _locTransferOrderXPO);
                                            SetPostingQty(_currSession, _locTransferOrderXPO);
                                            SetProcessCount(_currSession, _locTransferOrderXPO);
                                            SetStatusTransferOrderLine(_currSession, _locTransferOrderXPO);
                                            SetNormalQuantity(_currSession, _locTransferOrderXPO);
                                            SetFinalStatusTransferOrder(_currSession, _locTransferOrderXPO);
                                            SuccessMessageShow("Transfer Order has been successfully post");
                                        }
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data Transfer Order Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Transfer Order Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferOrder " + ex.ToString());
            }
        }

        private void TransferOrderApprovalAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                ApplicationSetupDetail _locAppSetDetail = null;
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                var User = SecuritySystem.CurrentUserName;
                Session _currentSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currentSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                UserAccess _locUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", User));

                foreach (Object obj in _objectsToProcess)
                {
                    TransferOrder _objInNewObjectSpace = (TransferOrder)_objectSpace.GetObject(obj);

                    if (_objInNewObjectSpace != null)
                    {
                        if (_objInNewObjectSpace.Code != null)
                        {
                            _currObjectId = _objInNewObjectSpace.Code;
                        }
                    }

                    if (_currObjectId != null)
                    {
                        TransferOrder _locTransferOrderXPO = _currentSession.FindObject<TransferOrder>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("Code", _currObjectId)));

                        if (_locTransferOrderXPO != null)
                        {
                            if (_locTransferOrderXPO.Status == Status.Progress)
                            {
                                ApprovalLine _locApprovalLine = _currentSession.FindObject<ApprovalLine>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("TransferOrder", _locTransferOrderXPO),
                                                                             new BinaryOperator("EndApproval", true)));
                                if (_locApprovalLine == null)
                                {
                                    #region Approval Level 1
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level1)
                                    {
                                        _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level1, ObjectList.TransferOrder);
                                        if (_locAppSetDetail != null)
                                        {
                                            //Buat bs input langsung ke approvalline
                                            ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("TransferOrder", _locTransferOrderXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level1)));
                                            if (_locApprovalLineXPO == null)
                                            {
                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level1,
                                                        EndApproval = true,
                                                        TransferOrder = _locTransferOrderXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locTransferOrderXPO.ActivationPosting = true;
                                                    _locTransferOrderXPO.ActiveApproved1 = false;
                                                    _locTransferOrderXPO.ActiveApproved2 = false;
                                                    _locTransferOrderXPO.ActiveApproved3 = true;
                                                    _locTransferOrderXPO.Save();
                                                    _locTransferOrderXPO.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level1,
                                                        TransferOrder = _locTransferOrderXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locTransferOrderXPO.ActiveApproved1 = true;
                                                    _locTransferOrderXPO.ActiveApproved2 = false;
                                                    _locTransferOrderXPO.ActiveApproved3 = false;
                                                    _locTransferOrderXPO.Save();
                                                    _locTransferOrderXPO.Session.CommitTransaction();
                                                }

                                                //Send Email
                                                ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("TransferOrder", _locTransferOrderXPO)));

                                                if (_locApprovalLineXPO2 != null)
                                                {
                                                    SendEmail(_currentSession, _locTransferOrderXPO, Status.Progress, _locApprovalLineXPO2);
                                                }

                                                SuccessMessageShow("Transfer Order has successfully Approve");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 1

                                    #region Approval Level 2
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level2)
                                    {
                                        _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level2, ObjectList.TransferOrder);

                                        if (_locAppSetDetail != null)
                                        {
                                            ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("TransferOrder", _locTransferOrderXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level2)));

                                            if (_locApprovalLineXPO == null)
                                            {
                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level2,
                                                        EndApproval = true,
                                                        TransferOrder = _locTransferOrderXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locTransferOrderXPO.ActivationPosting = true;
                                                    _locTransferOrderXPO.ActiveApproved1 = false;
                                                    _locTransferOrderXPO.ActiveApproved2 = false;
                                                    _locTransferOrderXPO.ActiveApproved3 = true;
                                                    _locTransferOrderXPO.Save();
                                                    _locTransferOrderXPO.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level2,
                                                        TransferOrder = _locTransferOrderXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    SetApprovalLine(_currentSession, _locTransferOrderXPO, ApprovalLevel.Level1);

                                                    _locTransferOrderXPO.ActiveApproved1 = false;
                                                    _locTransferOrderXPO.ActiveApproved2 = true;
                                                    _locTransferOrderXPO.ActiveApproved3 = false;
                                                    _locTransferOrderXPO.Save();
                                                    _locTransferOrderXPO.Session.CommitTransaction();
                                                }

                                                //Send Email
                                                ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("TransferOrder", _locTransferOrderXPO)));

                                                if (_locApprovalLineXPO2 != null)
                                                {
                                                    SendEmail(_currentSession, _locTransferOrderXPO, Status.Progress, _locApprovalLineXPO2);
                                                }

                                                SuccessMessageShow("Transfer Order has successfully Approve");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 2

                                    #region Approval Level 3
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level3)
                                    {
                                        _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level3, ObjectList.TransferOrder);

                                        if (_locAppSetDetail != null)
                                        {
                                            ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("TransferOrder", _locTransferOrderXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level3)));

                                            if (_locApprovalLineXPO == null)
                                            {
                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level3,
                                                        EndApproval = true,
                                                        TransferOrder = _locTransferOrderXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locTransferOrderXPO.ActivationPosting = true;
                                                    _locTransferOrderXPO.ActiveApproved1 = false;
                                                    _locTransferOrderXPO.ActiveApproved2 = false;
                                                    _locTransferOrderXPO.ActiveApproved3 = true;
                                                    _locTransferOrderXPO.Save();
                                                    _locTransferOrderXPO.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level3,
                                                        TransferOrder = _locTransferOrderXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    SetApprovalLine(_currentSession, _locTransferOrderXPO, ApprovalLevel.Level2);
                                                    SetApprovalLine(_currentSession, _locTransferOrderXPO, ApprovalLevel.Level1);

                                                    _locTransferOrderXPO.ActiveApproved1 = false;
                                                    _locTransferOrderXPO.ActiveApproved2 = false;
                                                    _locTransferOrderXPO.ActiveApproved3 = true;
                                                    _locTransferOrderXPO.Save();
                                                    _locTransferOrderXPO.Session.CommitTransaction();
                                                }

                                                //Send Email
                                                ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("TransferOrder", _locTransferOrderXPO)));

                                                if (_locApprovalLineXPO2 != null)
                                                {
                                                    SendEmail(_currentSession, _locTransferOrderXPO, Status.Progress, _locApprovalLineXPO2);
                                                }

                                                SuccessMessageShow("Transfer Order has successfully Approve");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 3
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Transfer Order Status Not Available");
                            }
                        }
                        else
                        {
                            ErrorMessageShow("Transfer Order Not Available");
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferOrder " + ex.ToString());
            }
        }

        private void TransferOrderListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(TransferOrder)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferOrder " + ex.ToString());
            }
        }

        private void TransferOrderListviewFilterApprovalSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(TransferOrder)))
                {
                    if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.Approval)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("ActiveApproved3", false, BinaryOperatorType.Equal),
                                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("ActiveApproved1", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", true, BinaryOperatorType.Equal)));
                    }
                    else if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.EndApproval)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new BinaryOperator("ActiveApproved3", true, BinaryOperatorType.Equal);
                    }
                    else if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.AllData)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("ActiveApproved1", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved3", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved1", false, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", false, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved3", false, BinaryOperatorType.Equal));
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferOrder " + ex.ToString());
            }
        }

        //=========================================== Code Only =====================================================

        #region Posting

        private void SetTransferOrderMonitoring(Session _currSession, TransferOrder _locTransferOrderXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;

                if (_locTransferOrderXPO != null)
                {
                    if (_locTransferOrderXPO.Status == Status.Progress || _locTransferOrderXPO.Status == Status.Posted)
                    {
                        XPCollection<TransferOrderLine> _locTransferOrderLines = new XPCollection<TransferOrderLine>
                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("TransferOrder", _locTransferOrderXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Progress),
                                                            new BinaryOperator("Status", Status.Posted))));

                        if (_locTransferOrderLines != null && _locTransferOrderLines.Count() > 0)
                        {
                            foreach (TransferOrderLine _locTransferOrderLine in _locTransferOrderLines)
                            {
                                TransferOrderMonitoring _saveDataTransferOrderMonitoring = new TransferOrderMonitoring(_currSession)
                                {
                                    TransferOrder = _locTransferOrderXPO,
                                    TransferOrderLine = _locTransferOrderLine,
                                    TransferType = _locTransferOrderXPO.TransferType,
                                    InventoryMovingType = _locTransferOrderXPO.InventoryMovingType,
                                    Item = _locTransferOrderLine.Item,
                                    MxDQty = _locTransferOrderLine.DQty,
                                    MxDUOM = _locTransferOrderLine.DUOM,
                                    MxQty = _locTransferOrderLine.Qty,
                                    MxUOM = _locTransferOrderLine.UOM,
                                    MxTQty = _locTransferOrderLine.TQty,
                                    DQty = _locTransferOrderLine.DQty,
                                    DUOM = _locTransferOrderLine.DUOM,
                                    Qty = _locTransferOrderLine.Qty,
                                    UOM = _locTransferOrderLine.UOM,
                                    TQty = _locTransferOrderLine.TQty,
                                    LocationFrom = _locTransferOrderLine.LocationFrom,
                                    LocationTo = _locTransferOrderLine.LocationTo,
                                };
                                _saveDataTransferOrderMonitoring.Save();
                                _saveDataTransferOrderMonitoring.Session.CommitTransaction();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = SalesOrder ", ex.ToString());
            }
        }

        private void SetRemainQty(Session _currSession, TransferOrder _locTransferOrderXPO)
        {
            try
            {
                if (_locTransferOrderXPO != null)
                {
                    XPCollection<TransferOrderLine> _locTransferOrderLines = new XPCollection<TransferOrderLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("TransferOrder", _locTransferOrderXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locTransferOrderLines != null && _locTransferOrderLines.Count > 0)
                    {
                        double _locRmDQty = 0;
                        double _locRmQty = 0;
                        double _locInvLineTotal = 0;
                        ItemUnitOfMeasure _locItemUOM = null;

                        foreach (TransferOrderLine _locTransferOrderLine in _locTransferOrderLines)
                        {
                            #region ProcessCount=0
                            if (_locTransferOrderLine.PostedCount == 0)
                            {
                                #region MaxQuantity
                                if (_locTransferOrderLine.MxDQty > 0)
                                {
                                    if (_locTransferOrderLine.DQty > 0 && _locTransferOrderLine.DQty <= _locTransferOrderLine.MxDQty)
                                    {
                                        _locRmDQty = _locTransferOrderLine.MxDQty - _locTransferOrderLine.DQty;
                                    }

                                    if (_locTransferOrderLine.Qty > 0 && _locTransferOrderLine.Qty <= _locTransferOrderLine.MxQty)
                                    {
                                        _locRmQty = _locTransferOrderLine.MxQty - _locTransferOrderLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locTransferOrderLine.Item),
                                                                new BinaryOperator("UOM", _locTransferOrderLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locTransferOrderLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty + _locRmDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }

                                }
                                #endregion MaxQuantity

                                #region NonMaxQuantity
                                else
                                {
                                    if (_locTransferOrderLine.DQty > 0)
                                    {
                                        _locRmDQty = 0;
                                    }

                                    if (_locTransferOrderLine.Qty > 0)
                                    {
                                        _locRmQty = 0;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locTransferOrderLine.Item),
                                                                new BinaryOperator("UOM", _locTransferOrderLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locTransferOrderLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty + _locRmDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion ProcessCount=0

                            #region ProcessCount>0
                            if (_locTransferOrderLine.PostedCount > 0)
                            {
                                if (_locTransferOrderLine.RmDQty > 0)
                                {
                                    _locRmDQty = _locTransferOrderLine.RmDQty - _locTransferOrderLine.DQty;
                                }

                                if (_locTransferOrderLine.RmQty > 0)
                                {
                                    _locRmQty = _locTransferOrderLine.RmQty - _locTransferOrderLine.Qty;
                                }

                                if (_locTransferOrderLine.MxDQty > 0 || _locTransferOrderLine.MxQty > 0)
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locTransferOrderLine.Item),
                                                                new BinaryOperator("UOM", _locTransferOrderLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locTransferOrderLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                }
                                else
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locTransferOrderLine.Item),
                                                                new BinaryOperator("UOM", _locTransferOrderLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locTransferOrderLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                }


                                if (_locItemUOM != null)
                                {
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locRmQty + _locRmDQty;
                                }

                            }
                            #endregion ProcessCount>0

                            _locTransferOrderLine.RmDQty = _locRmDQty;
                            _locTransferOrderLine.RmQty = _locRmQty;
                            _locTransferOrderLine.RmTQty = _locInvLineTotal;
                            _locTransferOrderLine.Save();
                            _locTransferOrderLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferOrder " + ex.ToString());
            }
        }

        private void SetPostingQty(Session _currSession, TransferOrder _locTransferOrderXPO)
        {
            try
            {
                if (_locTransferOrderXPO != null)
                {
                    XPCollection<TransferOrderLine> _locTransferOrderLines = new XPCollection<TransferOrderLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("TransferOrder", _locTransferOrderXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locTransferOrderLines != null && _locTransferOrderLines.Count > 0)
                    {
                        double _locPDQty = 0;
                        double _locPQty = 0;
                        double _locInvLineTotal = 0;
                        ItemUnitOfMeasure _locItemUOM = null;

                        foreach (TransferOrderLine _locTransferOrderLine in _locTransferOrderLines)
                        {
                            #region ProcessCount=0
                            if (_locTransferOrderLine.PostedCount == 0)
                            {
                                #region MaxQuantity
                                if (_locTransferOrderLine.MxDQty > 0)
                                {
                                    if (_locTransferOrderLine.DQty > 0 && _locTransferOrderLine.DQty <= _locTransferOrderLine.MxDQty)
                                    {
                                        _locPDQty = _locTransferOrderLine.DQty;
                                    }

                                    if (_locTransferOrderLine.Qty > 0 && _locTransferOrderLine.Qty <= _locTransferOrderLine.MxQty)
                                    {
                                        _locPQty = _locTransferOrderLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locTransferOrderLine.Item),
                                                                new BinaryOperator("UOM", _locTransferOrderLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locTransferOrderLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty + _locPDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }

                                }
                                #endregion MaxQuantity

                                #region NonMaxQuantity
                                else
                                {
                                    if (_locTransferOrderLine.DQty > 0)
                                    {
                                        _locPDQty = _locTransferOrderLine.DQty;
                                    }

                                    if (_locTransferOrderLine.Qty > 0)
                                    {
                                        _locPQty = _locTransferOrderLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locTransferOrderLine.Item),
                                                                new BinaryOperator("UOM", _locTransferOrderLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locTransferOrderLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty + _locPDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion ProcessCount=0

                            #region ProcessCount>0
                            if (_locTransferOrderLine.PostedCount > 0)
                            {
                                if (_locTransferOrderLine.PDQty > 0)
                                {
                                    _locPDQty = _locTransferOrderLine.PDQty + _locTransferOrderLine.DQty;
                                }

                                if (_locTransferOrderLine.PQty > 0)
                                {
                                    _locPQty = _locTransferOrderLine.PQty + _locTransferOrderLine.Qty;
                                }

                                if (_locTransferOrderLine.MxDQty > 0 || _locTransferOrderLine.MxQty > 0)
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                           (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Item", _locTransferOrderLine.Item),
                                                            new BinaryOperator("UOM", _locTransferOrderLine.MxUOM),
                                                            new BinaryOperator("DefaultUOM", _locTransferOrderLine.MxDUOM),
                                                            new BinaryOperator("Active", true)));
                                }
                                else
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                           (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Item", _locTransferOrderLine.Item),
                                                            new BinaryOperator("UOM", _locTransferOrderLine.UOM),
                                                            new BinaryOperator("DefaultUOM", _locTransferOrderLine.DUOM),
                                                            new BinaryOperator("Active", true)));
                                }

                                if (_locItemUOM != null)
                                {
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locPQty + _locPDQty;
                                }
                            }
                            #endregion ProcessCount>0

                            _locTransferOrderLine.PDQty = _locPDQty;
                            _locTransferOrderLine.PDUOM = _locTransferOrderLine.DUOM;
                            _locTransferOrderLine.PQty = _locPQty;
                            _locTransferOrderLine.PUOM = _locTransferOrderLine.UOM;
                            _locTransferOrderLine.PTQty = _locInvLineTotal;
                            _locTransferOrderLine.Save();
                            _locTransferOrderLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferOrder " + ex.ToString());
            }
        }

        private void SetProcessCount(Session _currSession, TransferOrder _locTransferOrderXPO)
        {
            try
            {
                if (_locTransferOrderXPO != null)
                {
                    XPCollection<TransferOrderLine> _locTransferOrderLines = new XPCollection<TransferOrderLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("TransferOrder", _locTransferOrderXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locTransferOrderLines != null && _locTransferOrderLines.Count > 0)
                    {
                        foreach (TransferOrderLine _locTransferOrderLine in _locTransferOrderLines)
                        {
                            if (_locTransferOrderLine.Status == Status.Progress || _locTransferOrderLine.Status == Status.Posted)
                            {
                                _locTransferOrderLine.PostedCount = _locTransferOrderLine.PostedCount + 1;
                                _locTransferOrderLine.Save();
                                _locTransferOrderLine.Session.CommitTransaction();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferOrder " + ex.ToString());
            }
        }

        private void SetStatusTransferOrderLine(Session _currSession, TransferOrder _locTransferOrderXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locTransferOrderXPO != null)
                {
                    XPCollection<TransferOrderLine> _locTransferOrderLines = new XPCollection<TransferOrderLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("TransferOrder", _locTransferOrderXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locTransferOrderLines != null && _locTransferOrderLines.Count > 0)
                    {
                        foreach (TransferOrderLine _locTransferOrderLine in _locTransferOrderLines)
                        {
                            if (_locTransferOrderLine.Status == Status.Progress || _locTransferOrderLine.Status == Status.Posted)
                            {
                                if (_locTransferOrderLine.RmDQty == 0 && _locTransferOrderLine.RmQty == 0 && _locTransferOrderLine.RmTQty == 0)
                                {
                                    _locTransferOrderLine.Status = Status.Close;
                                    _locTransferOrderLine.ActivationQuantity = true;
                                    _locTransferOrderLine.ActivationPosting = true;
                                    _locTransferOrderLine.StatusDate = now;
                                }
                                else
                                {
                                    _locTransferOrderLine.Status = Status.Posted;
                                    _locTransferOrderLine.StatusDate = now;
                                }
                                _locTransferOrderLine.Save();
                                _locTransferOrderLine.Session.CommitTransaction();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferOrder " + ex.ToString());
            }
        }

        private void SetNormalQuantity(Session _currSession, TransferOrder _locTransferOrderXPO)
        {
            try
            {
                if (_locTransferOrderXPO != null)
                {
                    XPCollection<TransferOrderLine> _locTransferOrderLines = new XPCollection<TransferOrderLine>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("TransferOrder", _locTransferOrderXPO),
                                                                                     new BinaryOperator("Select", true)));

                    if (_locTransferOrderLines != null && _locTransferOrderLines.Count > 0)
                    {
                        foreach (TransferOrderLine _locTransferOrderLine in _locTransferOrderLines)
                        {
                            if (_locTransferOrderLine.Status == Status.Progress || _locTransferOrderLine.Status == Status.Posted || _locTransferOrderLine.Status == Status.Close)
                            {
                                if (_locTransferOrderLine.DQty > 0 || _locTransferOrderLine.Qty > 0)
                                {
                                    _locTransferOrderLine.Select = false;
                                    _locTransferOrderLine.DQty = 0;
                                    _locTransferOrderLine.Qty = 0;
                                    _locTransferOrderLine.Save();
                                    _locTransferOrderLine.Session.CommitTransaction();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferOrder " + ex.ToString());
            }
        }

        private void SetFinalStatusTransferOrder(Session _currSession, TransferOrder _locTransferOrderXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locStatusCount = 0;
                int _locTransferOrderLineCount = 0;

                if (_locTransferOrderXPO != null)
                {
                    XPCollection<TransferOrderLine> _locTransferOrderLines = new XPCollection<TransferOrderLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("TransferOrder", _locTransferOrderXPO)));

                    if (_locTransferOrderLines != null && _locTransferOrderLines.Count() > 0)
                    {
                        _locTransferOrderLineCount = _locTransferOrderLines.Count();

                        foreach (TransferOrderLine _locTransferOrderLine in _locTransferOrderLines)
                        {
                            if (_locTransferOrderLine.Status == Status.Close)
                            {
                                _locStatusCount = _locStatusCount + 1;
                            }
                        }
                    }

                    if (_locStatusCount == _locTransferOrderLineCount)
                    {
                        _locTransferOrderXPO.ActivationPosting = true;
                        _locTransferOrderXPO.Status = Status.Posted;
                        _locTransferOrderXPO.StatusDate = now;
                        _locTransferOrderXPO.Save();
                        _locTransferOrderXPO.Session.CommitTransaction();
                    }
                    else
                    {
                        _locTransferOrderXPO.Status = Status.Posted;
                        _locTransferOrderXPO.StatusDate = now;
                        _locTransferOrderXPO.Save();
                        _locTransferOrderXPO.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferOrder " + ex.ToString());
            }
        }

        #endregion Posting

        #region TransferOrderOld

        //Cek ketersediaan stock
        private bool CheckAvailableStockOld(Session _currSession, TransferOrder _transferOrderXPO)
        {
            bool _result = true;
            try
            {
                if(_transferOrderXPO != null)
                {
                    XPCollection<TransferOrderLine> _locTransOrderLines = new XPCollection<TransferOrderLine>(_currSession,
                                                                        new BinaryOperator("TransferOrder", _transferOrderXPO));

                    if (_locTransOrderLines != null && _locTransOrderLines.Count > 0)
                    {
                        double _locTransOutLineTotal = 0;
                        BeginingInventory _locBegInventory = null;
                        foreach (TransferOrderLine _locTransOrderLine in _locTransOrderLines)
                        {
                            XPCollection<TransferOrderLot> _locTransferOrderLots = new XPCollection<TransferOrderLot>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("TransferOrderLine", _locTransOrderLine)));

                            #region LotNumber
                            if (_locTransferOrderLots != null && _locTransferOrderLots.Count() > 0)
                            {
                                foreach (TransferOrderLot _locTransferOrderLot in _locTransferOrderLots)
                                {
                                    _locBegInventory = _currSession.FindObject<BeginingInventory>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Item", _locTransferOrderLot.Item)));
                                    if (_locBegInventory != null)
                                    {
                                        if (_locTransferOrderLot.BegInvLine != null)
                                        {
                                            if (_locTransferOrderLot.BegInvLine.Code != null)
                                            {
                                                BeginingInventoryLine _locBegInvLine = _currSession.FindObject<BeginingInventoryLine>(
                                                                                            new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("Code", _locTransferOrderLot.BegInvLine.Code)));
                                                if (_locBegInvLine != null)
                                                {
                                                    if (_locTransferOrderLot.TQty > _locBegInvLine.QtyAvailable)
                                                    {
                                                        _result = false;
                                                    }
                                                }
                                                else
                                                {
                                                    _result = false;
                                                }
                                            }
                                        }

                                    }
                                }
                            }
                            #endregion LotNumber
                            #region NonLotNumber
                            else
                            {
                                if (_locTransOrderLine.LocationFrom != null && _locTransOrderLine.BinLocationFrom != null)
                                {
                                    _locBegInventory = _currSession.FindObject<BeginingInventory>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Item", _locTransOrderLine.Item)));

                                    if (_locBegInventory != null)
                                    {
                                        XPCollection<BeginingInventoryLine> _locBegInvLines = new XPCollection<BeginingInventoryLine>(_currSession,
                                                                                            new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("BeginingInventory", _locBegInventory),
                                                                                            new BinaryOperator("Item", _locTransOrderLine.Item),
                                                                                            new BinaryOperator("Location", _locTransOrderLine.LocationFrom),
                                                                                            new BinaryOperator("BinLocation", _locTransOrderLine.BinLocationFrom),
                                                                                            new BinaryOperator("StockType", _locTransOrderLine.StockTypeFrom)));

                                        if (_locBegInvLines != null && _locBegInvLines.Count() > 0)
                                        {
                                            foreach (BeginingInventoryLine _locBegInvLine in _locBegInvLines)
                                            {
                                                ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                (new GroupOperator(GroupOperatorType.And,
                                                                 new BinaryOperator("Item", _locTransOrderLine.Item),
                                                                 new BinaryOperator("UOM", _locTransOrderLine.UOM),
                                                                 new BinaryOperator("DefaultUOM", _locTransOrderLine.DUOM),
                                                                 new BinaryOperator("Active", true)));

                                                if (_locItemUOM != null)
                                                {
                                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                    {
                                                        _locTransOutLineTotal = _locTransOrderLine.Qty * _locItemUOM.DefaultConversion + _locTransOrderLine.DQty;
                                                    }
                                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                    {
                                                        _locTransOutLineTotal = _locTransOrderLine.Qty / _locItemUOM.Conversion + _locTransOrderLine.DQty;
                                                    }
                                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                    {
                                                        _locTransOutLineTotal = _locTransOrderLine.Qty + _locTransOrderLine.DQty;
                                                    }

                                                    if (_locTransOutLineTotal > _locBegInventory.QtyAvailable)
                                                    {
                                                        _result = false;
                                                    }
                                                }
                                                else
                                                {
                                                    _locTransOutLineTotal = _locTransOrderLine.Qty + _locTransOrderLine.DQty;

                                                    if (_locTransOutLineTotal > _locBegInventory.QtyAvailable)
                                                    {
                                                        _result = false;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion NonLotNumber
                            
                        }
                    }
                }

                
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = TransferOrder ", ex.ToString());
            }
            return _result;
        }

        //Menentukan sisa quantity yang di posting -> Done 10 Dec 2019
        private void SetRemainDeliverQtyOld(Session _currSession, TransferOrder _transferOrderXPO)
        {
            try
            {
                if (_transferOrderXPO != null)
                {
                    XPCollection<TransferOrderLine> _locTransOrderLines = new XPCollection<TransferOrderLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("TransferOrder", _transferOrderXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locTransOrderLines != null && _locTransOrderLines.Count > 0)
                    {
                        double _locRmDQty = 0;
                        double _locRmQty = 0;
                        double _locInvLineTotal = 0;
                        ItemUnitOfMeasure _locItemUOM = null;

                        foreach (TransferOrderLine _locTransOrderLine in _locTransOrderLines)
                        {
                            #region ProcessCount=0
                            if (_locTransOrderLine.PostedCount == 0)
                            {
                                #region MaxQuantity
                                if (_locTransOrderLine.MxDQty > 0 || _locTransOrderLine.MxQty > 0)
                                {
                                    if (_locTransOrderLine.DQty > 0 && _locTransOrderLine.DQty <= _locTransOrderLine.MxDQty)
                                    {
                                        _locRmDQty = _locTransOrderLine.MxDQty - _locTransOrderLine.DQty;
                                    }

                                    if (_locTransOrderLine.Qty > 0 && _locTransOrderLine.Qty <= _locTransOrderLine.MxQty)
                                    {
                                        _locRmQty = _locTransOrderLine.MxQty - _locTransOrderLine.Qty;
                                    }

                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locTransOrderLine.Item),
                                                                new BinaryOperator("UOM", _locTransOrderLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locTransOrderLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty + _locRmDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                #endregion MaxQuantity
                                #region NonMaxQuantity
                                else
                                {
                                    if (_locTransOrderLine.DQty > 0)
                                    {
                                        _locRmDQty = 0;
                                    }

                                    if (_locTransOrderLine.Qty > 0)
                                    {
                                        _locRmQty = 0;
                                    }

                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locTransOrderLine.Item),
                                                                new BinaryOperator("UOM", _locTransOrderLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locTransOrderLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty + _locRmDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion ProcessCount=0

                            #region ProcessCount>0
                            if (_locTransOrderLine.PostedCount > 0)
                            {
                                if (_locTransOrderLine.RmDQty > 0)
                                {
                                    _locRmDQty = _locTransOrderLine.RmDQty - _locTransOrderLine.DQty;
                                }

                                if (_locTransOrderLine.RmQty > 0)
                                {
                                    _locRmQty = _locTransOrderLine.RmQty - _locTransOrderLine.Qty;
                                }

                                if(_locTransOrderLine.MxDQty > 0 || _locTransOrderLine.MxQty > 0)
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locTransOrderLine.Item),
                                                                new BinaryOperator("UOM", _locTransOrderLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locTransOrderLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                }else
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locTransOrderLine.Item),
                                                                new BinaryOperator("UOM", _locTransOrderLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locTransOrderLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                }

                             
                                if (_locItemUOM != null)
                                {
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locRmQty + _locRmDQty;
                                }

                            }
                            #endregion ProcessCount>0

                            _locTransOrderLine.RmDQty = _locRmDQty;
                            _locTransOrderLine.RmQty = _locRmQty;
                            _locTransOrderLine.RmTQty = _locInvLineTotal;
                            _locTransOrderLine.Save();
                            _locTransOrderLine.Session.CommitTransaction();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferOrder " + ex.ToString());
            }
        }

        //Menentukan jumlah quantity yang di posting -> Done 09 December 2019
        private void SetPostingDeliverQtyOld(Session _currSession, TransferOrder _transferOrderXPO)
        {
            try
            {
                if (_transferOrderXPO != null)
                {
                    XPCollection<TransferOrderLine> _locTransOrderLines = new XPCollection<TransferOrderLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("TransferOrder", _transferOrderXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locTransOrderLines != null && _locTransOrderLines.Count > 0)
                    {
                        double _locPDQty = 0;
                        double _locPQty = 0;
                        double _locInvLineTotal = 0;
                        ItemUnitOfMeasure _locItemUOM = null;

                        foreach (TransferOrderLine _locTransOrderLine in _locTransOrderLines)
                        {
                            #region ProcessCount=0
                            if (_locTransOrderLine.PostedCount == 0)
                            {
                                #region MaxQuantity
                                if (_locTransOrderLine.MxDQty > 0 || _locTransOrderLine.Qty > 0)
                                {
                                    if (_locTransOrderLine.DQty > 0 && _locTransOrderLine.DQty <= _locTransOrderLine.MxDQty)
                                    {
                                        _locPDQty = _locTransOrderLine.DQty;
                                    }

                                    if (_locTransOrderLine.Qty > 0 && _locTransOrderLine.Qty <= _locTransOrderLine.MxQty)
                                    {
                                        _locPQty = _locTransOrderLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locTransOrderLine.Item),
                                                                new BinaryOperator("UOM", _locTransOrderLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locTransOrderLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty + _locPDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }

                                }
                                #endregion MaxQuantity
                                #region NonMaxQuantity
                                else
                                {
                                    if (_locTransOrderLine.DQty > 0 )
                                    {
                                        _locPDQty = _locTransOrderLine.DQty;
                                    }

                                    if (_locTransOrderLine.Qty > 0 )
                                    {
                                        _locPQty = _locTransOrderLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locTransOrderLine.Item),
                                                                new BinaryOperator("UOM", _locTransOrderLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locTransOrderLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty + _locPDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion ProcessCount=0

                            #region ProcessCount>0
                            if (_locTransOrderLine.PostedCount > 0)
                            {
                                if (_locTransOrderLine.PDQty > 0)
                                {
                                    _locPDQty = _locTransOrderLine.PDQty + _locTransOrderLine.DQty;
                                }

                                if (_locTransOrderLine.PQty > 0)
                                {
                                    _locPQty = _locTransOrderLine.PQty + _locTransOrderLine.Qty;
                                }

                                if(_locTransOrderLine.MxDQty > 0 || _locTransOrderLine.MxQty > 0)
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locTransOrderLine.Item),
                                                                new BinaryOperator("UOM", _locTransOrderLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locTransOrderLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                }else
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locTransOrderLine.Item),
                                                                new BinaryOperator("UOM", _locTransOrderLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locTransOrderLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                }

                                
                                if (_locItemUOM != null)
                                {
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locPQty + _locPDQty;
                                }

                            }
                            #endregion ProcessCount>0

                            _locTransOrderLine.PDQty = _locPDQty;
                            _locTransOrderLine.PDUOM = _locTransOrderLine.DUOM;
                            _locTransOrderLine.PQty = _locPQty;
                            _locTransOrderLine.PUOM = _locTransOrderLine.UOM;
                            _locTransOrderLine.PTQty = _locInvLineTotal;
                            _locTransOrderLine.Save();
                            _locTransOrderLine.Session.CommitTransaction();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferOrder " + ex.ToString());
            }

        }

        //Menentukan Banyak process Deliver -> Done 09 December 2019
        private void SetProcessCountDeliverOld(Session _currSession, TransferOrder _transferOrderXPO)
        {
            try
            {
                if (_transferOrderXPO != null)
                {
                    XPCollection<TransferOrderLine> _locTransOrderLines = new XPCollection<TransferOrderLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("TransferOrder", _transferOrderXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locTransOrderLines != null && _locTransOrderLines.Count > 0)
                    {

                        foreach (TransferOrderLine _locTransOrderLine in _locTransOrderLines)
                        {
                            if (_locTransOrderLine.Status == Status.Progress || _locTransOrderLine.Status == Status.Posted)
                            {
                                _locTransOrderLine.PostedCount = _locTransOrderLine.PostedCount + 1;
                                _locTransOrderLine.Save();
                                _locTransOrderLine.Session.CommitTransaction();
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferOrder " + ex.ToString());
            }
        }

        //Membuat Transfer Out
        private void SetTransferOutOld(Session _currSession, TransferOrder _transferOrderXPO, UserAccess _locUserAccess)
        {
            try
            {

                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _locDocCode = null;
                string _locSignCodeTOL = null;
                ProjectHeader _locProjectHeader = null;
                bool _locSameWarehouse = false;
                DocumentType _locDocumentTypeXPO = null;
                DocumentType _locDocumentType = null;

                if (_transferOrderXPO != null)
                {
                    if (_transferOrderXPO.ProjectHeader != null)
                    {
                        _locProjectHeader = _transferOrderXPO.ProjectHeader;
                    }

                    if(_transferOrderXPO.DocumentType != null)
                    {
                        _locDocumentTypeXPO = _transferOrderXPO.DocumentType;
                    }

                    _locDocumentType = _currSession.FindObject<DocumentType>
                                           (new GroupOperator(GroupOperatorType.And,
                                            new BinaryOperator("TransferType", DirectionType.Internal),
                                            new BinaryOperator("InventoryMovingType", InventoryMovingType.TransferOut),
                                            new BinaryOperator("ObjectList", ObjectList.TransferOut),
                                            new BinaryOperator("Active", true)));

                    if(_locDocumentType != null)
                    {
                        if (_locDocumentTypeXPO != null && _locDocumentTypeXPO == _locDocumentType)
                        {
                            if (_transferOrderXPO.DocNo != null)
                            {
                                _locDocCode = _transferOrderXPO.DocNo;
                            }

                            if (_locDocCode != null)
                            {
                                //Menentukan same warehouse atau tidak
                                if (_transferOrderXPO != null)
                                {
                                    if (_transferOrderXPO.LocationFrom != null && _transferOrderXPO.LocationTo != null)
                                    {
                                        if (CheckAvailableWarehouseSetupDetailOld(_currSession, _transferOrderXPO.LocationFrom, _locUserAccess) == true
                                            && CheckAvailableWarehouseSetupDetailOld(_currSession, _transferOrderXPO.LocationTo, _locUserAccess) == true)
                                        {
                                            _locSameWarehouse = true;
                                        }
                                    }
                                }

                                TransferOut _locTransferOut = _currSession.FindObject<TransferOut>(new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("TransferOrder", _transferOrderXPO),
                                                    new BinaryOperator("DocumentType", _locDocumentTypeXPO),
                                                    new BinaryOperator("DocNo", _locDocCode)));

                                if (_locTransferOut == null)
                                {
                                    TransferOut _saveDataTI = new TransferOut(_currSession)
                                    {
                                        TransferType = DirectionType.Internal,
                                        InventoryMovingType = InventoryMovingType.TransferOut,
                                        ObjectList = ObjectList.TransferOut,
                                        DocumentType = _locDocumentTypeXPO,
                                        DocNo = _locDocCode,
                                        LocationFrom = _transferOrderXPO.LocationFrom,
                                        LocationTo = _transferOrderXPO.LocationTo,
                                        TransferOrder = _transferOrderXPO,
                                        ProjectHeader = _locProjectHeader,
                                        SameWarehouse = _locSameWarehouse,
                                    };
                                    _saveDataTI.Save();
                                    _saveDataTI.Session.CommitTransaction();

                                    TransferOut _locTransferOut2 = _currSession.FindObject<TransferOut>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("DocNo", _locDocCode),
                                                         new BinaryOperator("TransferOrder", _transferOrderXPO)));

                                    if (_locTransferOut2 != null)
                                    {
                                        XPCollection<TransferOrderLine> _locTransOrderLines = new XPCollection<TransferOrderLine>(_currSession,
                                                                                            new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("TransferOrder", _transferOrderXPO),
                                                                                            new BinaryOperator("Select", true)));

                                        if (_locTransOrderLines != null && _locTransOrderLines.Count > 0)
                                        {
                                            //PerTransferOutLine
                                            foreach (TransferOrderLine _locTransOrderLine in _locTransOrderLines)
                                            {
                                                if (_locTransOrderLine.Status == Status.Progress || _locTransOrderLine.Status == Status.Posted)
                                                {
                                                    _locSignCodeTOL = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.TransferOutLine);

                                                    if(_locSignCodeTOL != null)
                                                    {
                                                        TransferOutLine _saveDataTransferOutLine = new TransferOutLine(_currSession)
                                                        {
                                                            Select = true,
                                                            SignCode = _locSignCodeTOL,
                                                            LocationFrom = _locTransOrderLine.LocationFrom,
                                                            BinLocationFrom = _locTransOrderLine.BinLocationFrom,
                                                            StockTypeFrom = _locTransOrderLine.StockTypeFrom,
                                                            LocationTo = _locTransOrderLine.LocationTo,
                                                            BinLocationTo = _locTransOrderLine.BinLocationTo,
                                                            StockTypeTo = _locTransOrderLine.StockTypeTo,
                                                            Item = _locTransOrderLine.Item,
                                                            MxDQty = _locTransOrderLine.DQty,
                                                            MxDUOM = _locTransOrderLine.DUOM,
                                                            MxQty = _locTransOrderLine.Qty,
                                                            MxUOM = _locTransOrderLine.UOM,
                                                            MxTQty = _locTransOrderLine.TQty,
                                                            DQty = _locTransOrderLine.DQty,
                                                            DUOM = _locTransOrderLine.DUOM,
                                                            Qty = _locTransOrderLine.Qty,
                                                            UOM = _locTransOrderLine.UOM,
                                                            TQty = _locTransOrderLine.TQty,
                                                            TransferOut = _locTransferOut2,

                                                        };
                                                        _saveDataTransferOutLine.Save();
                                                        _saveDataTransferOutLine.Session.CommitTransaction();

                                                        TransferOutLine _locTransferOutLine = _currSession.FindObject<TransferOutLine>
                                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("SignCode", _locSignCodeTOL)));
                                                        if (_locTransferOutLine != null)
                                                        {
                                                            XPCollection<TransferOrderLot> _locTransferOrderLots = new XPCollection<TransferOrderLot>(_currSession,
                                                                                                                new GroupOperator(GroupOperatorType.And,
                                                                                                                new BinaryOperator("TransferOrderLine", _locTransOrderLine),
                                                                                                                new BinaryOperator("Select", true)));
                                                            if (_locTransferOrderLots != null && _locTransferOrderLots.Count() > 0)
                                                            {
                                                                foreach (TransferOrderLot _locTransferOrderLot in _locTransferOrderLots)
                                                                {
                                                                    TransferOutLot _saveDataTransferOutLot = new TransferOutLot(_currSession)
                                                                    {
                                                                        Select = true,
                                                                        LocationFrom = _locTransferOrderLot.LocationFrom,
                                                                        BinLocationFrom = _locTransferOrderLot.BinLocationFrom,
                                                                        StockTypeFrom = _locTransferOrderLot.StockTypeFrom,
                                                                        LocationTo = _locTransferOrderLot.LocationTo,
                                                                        BinLocationTo = _locTransferOrderLot.BinLocationTo,
                                                                        StockTypeTo = _locTransferOrderLot.StockTypeTo,
                                                                        Item = _locTransferOrderLot.Item,
                                                                        BegInvLine = _locTransferOrderLot.BegInvLine,
                                                                        LotNumber = _locTransferOrderLot.LotNumber,
                                                                        DQty = _locTransferOrderLot.DQty,
                                                                        DUOM = _locTransferOrderLot.DUOM,
                                                                        Qty = _locTransferOrderLot.Qty,
                                                                        UOM = _locTransferOrderLot.UOM,
                                                                        TQty = _locTransferOrderLot.TQty,
                                                                        TransferOutLine = _locTransferOutLine,
                                                                        UnitPack = _locTransferOrderLot.UnitPack,
                                                                    };
                                                                    _saveDataTransferOutLot.Save();
                                                                    _saveDataTransferOutLot.Session.CommitTransaction();
                                                                }
                                                            }
                                                        }
                                                    }   
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }     
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = TransferOrder ", ex.ToString());
            }
        }

        //Membuat Transfer In
        private void SetTransferInOld(Session _currSession, TransferOrder _transferOrderXPO)
        {
            try
            {

                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _locDocCode = null;
                string _locSignCodeTIL = null;
                ProjectHeader _locProjectHeader = null;
                DocumentType _locDocumentTypeXPO = null;
                DocumentType _locDocumentType = null;

                if (_transferOrderXPO != null)
                {
                    if (_transferOrderXPO.ProjectHeader != null)
                    {
                        _locProjectHeader = _transferOrderXPO.ProjectHeader;
                    }

                    if(_transferOrderXPO.DocumentType != null)
                    {
                        _locDocumentTypeXPO = _transferOrderXPO.DocumentType;
                    }

                    _locDocumentType = _currSession.FindObject<DocumentType>
                                           (new GroupOperator(GroupOperatorType.And,
                                            new BinaryOperator("TransferType", DirectionType.Internal),
                                            new BinaryOperator("InventoryMovingType", InventoryMovingType.TransferIn),
                                            new BinaryOperator("ObjectList", ObjectList.TransferIn),
                                            new BinaryOperator("Active", true)));

                    if(_locDocumentType != null)
                    {
                        if (_locDocumentTypeXPO != null && _locDocumentTypeXPO == _locDocumentType)
                        {
                            if (_transferOrderXPO.DocNo != null)
                            {
                                _locDocCode = _transferOrderXPO.DocNo;
                            }

                            if (_locDocCode != null)
                            {
                                TransferIn _locTransferIn = _currSession.FindObject<TransferIn>(new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("TransferOrder", _transferOrderXPO),
                                                    new BinaryOperator("DocumentType", _locDocumentTypeXPO),
                                                    new BinaryOperator("DocNo", _locDocCode)));

                                if (_locTransferIn == null)
                                {
                                    TransferIn _saveDataTI = new TransferIn(_currSession)
                                    {
                                        TransferType = DirectionType.Internal,
                                        InventoryMovingType = InventoryMovingType.TransferIn,
                                        ObjectList = ObjectList.TransferIn,
                                        DocumentType = _locDocumentTypeXPO,
                                        DocNo = _locDocCode,
                                        LocationFrom = _transferOrderXPO.LocationFrom,
                                        LocationTo = _transferOrderXPO.LocationTo,
                                        TransferOrder = _transferOrderXPO,
                                        ProjectHeader = _locProjectHeader,
                                    };
                                    _saveDataTI.Save();
                                    _saveDataTI.Session.CommitTransaction();

                                    TransferIn _locTransferIn2 = _currSession.FindObject<TransferIn>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("DocNo", _locDocCode),
                                                         new BinaryOperator("TransferOrder", _transferOrderXPO)));

                                    if (_locTransferIn2 != null)
                                    {
                                        XPCollection<TransferOrderLine> _locTransOrderLines = new XPCollection<TransferOrderLine>(_currSession,
                                                                                            new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("TransferOrder", _transferOrderXPO),
                                                                                            new BinaryOperator("Select", true)));

                                        if (_locTransOrderLines != null && _locTransOrderLines.Count > 0)
                                        {
                                            //PerTransferOutLine
                                            foreach (TransferOrderLine _locTransOrderLine in _locTransOrderLines)
                                            {
                                                if(_locTransOrderLine.Status == Status.Progress || _locTransOrderLine.Status == Status.Posted)
                                                {
                                                    _locSignCodeTIL = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.TransferInLine);

                                                    if(_locSignCodeTIL != null)
                                                    {
                                                        TransferInLine _saveDataTransferInLine = new TransferInLine(_currSession)
                                                        {
                                                            Select = true,
                                                            SignCode = _locSignCodeTIL,
                                                            LocationFrom = _locTransOrderLine.LocationFrom,
                                                            BinLocationFrom = _locTransOrderLine.BinLocationFrom,
                                                            StockTypeFrom = _locTransOrderLine.StockTypeFrom,
                                                            LocationTo = _locTransOrderLine.LocationTo,
                                                            BinLocationTo = _locTransOrderLine.BinLocationTo,
                                                            StockTypeTo = _locTransOrderLine.StockTypeTo,
                                                            Item = _locTransOrderLine.Item,
                                                            MxDQty = _locTransOrderLine.DQty,
                                                            MxDUOM = _locTransOrderLine.DUOM,
                                                            MxQty = _locTransOrderLine.Qty,
                                                            MxUOM = _locTransOrderLine.UOM,
                                                            MxTQty = _locTransOrderLine.TQty,
                                                            DQty = _locTransOrderLine.DQty,
                                                            DUOM = _locTransOrderLine.DUOM,
                                                            Qty = _locTransOrderLine.Qty,
                                                            UOM = _locTransOrderLine.UOM,
                                                            TQty = _locTransOrderLine.TQty,
                                                            TransferIn = _locTransferIn2,

                                                        };
                                                        _saveDataTransferInLine.Save();
                                                        _saveDataTransferInLine.Session.CommitTransaction();

                                                        TransferInLine _locTransferInLine = _currSession.FindObject<TransferInLine>
                                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("SignCode", _locSignCodeTIL)));
                                                        if (_locTransferInLine != null)
                                                        {
                                                            XPCollection<TransferOrderLot> _locTransferOrderLots = new XPCollection<TransferOrderLot>(_currSession,
                                                                                                                new GroupOperator(GroupOperatorType.And,
                                                                                                                new BinaryOperator("TransferOrderLine", _locTransOrderLine),
                                                                                                                new BinaryOperator("Select", true)));
                                                            if (_locTransferOrderLots != null && _locTransferOrderLots.Count() > 0)
                                                            {
                                                                foreach (TransferOrderLot _locTransferOrderLot in _locTransferOrderLots)
                                                                {
                                                                    TransferInLot _saveDataTransferInLot = new TransferInLot(_currSession)
                                                                    {
                                                                        Select = true,
                                                                        LocationFrom = _locTransferOrderLot.LocationFrom,
                                                                        BinLocationFrom = _locTransferOrderLot.BinLocationFrom,
                                                                        StockTypeFrom = _locTransferOrderLot.StockTypeFrom,
                                                                        LocationTo = _locTransferOrderLot.LocationTo,
                                                                        BinLocationTo = _locTransferOrderLot.BinLocationTo,
                                                                        StockTypeTo = _locTransferOrderLot.StockTypeTo,
                                                                        Item = _locTransferOrderLot.Item,
                                                                        BegInvLine = _locTransferOrderLot.BegInvLine,
                                                                        LotNumber = _locTransferOrderLot.LotNumber,
                                                                        DQty = _locTransferOrderLot.DQty,
                                                                        DUOM = _locTransferOrderLot.DUOM,
                                                                        Qty = _locTransferOrderLot.Qty,
                                                                        UOM = _locTransferOrderLot.UOM,
                                                                        TQty = _locTransferOrderLot.TQty,
                                                                        TransferInLine = _locTransferInLine,
                                                                        UnitPack = _locTransferOrderLot.UnitPack,
                                                                    };
                                                                    _saveDataTransferInLot.Save();
                                                                    _saveDataTransferInLot.Session.CommitTransaction();
                                                                }
                                                            }
                                                        }
                                                    }     
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }     
                }
                
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = TransferOrder ", ex.ToString());
            }
        }

        //Menormalkan Quantity
        private void SetNormalPostingQuantityOld(Session _currSession, TransferOrder _transferOrderXPO)
        {
            try
            {
                if (_transferOrderXPO != null)
                {
                    XPCollection<TransferOrderLine> _locTransOrderLines = new XPCollection<TransferOrderLine>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("TransferOrder", _transferOrderXPO),
                                                                                     new BinaryOperator("Select", true)));

                    if (_locTransOrderLines != null && _locTransOrderLines.Count > 0)
                    {
                        foreach (TransferOrderLine _locTransOrderLine in _locTransOrderLines)
                        {
                            if (_locTransOrderLine.Status == Status.Progress || _locTransOrderLine.Status == Status.Posted || _locTransOrderLine.Status == Status.Close)
                            {
                                if (_locTransOrderLine.DQty > 0 || _locTransOrderLine.Qty > 0)
                                {
                                    _locTransOrderLine.Select = false;
                                    _locTransOrderLine.DQty = 0;
                                    _locTransOrderLine.Qty = 0;
                                    _locTransOrderLine.Save();
                                    _locTransOrderLine.Session.CommitTransaction();

                                    XPCollection<TransferOrderLot> _locTransferOrderLots = new XPCollection<TransferOrderLot>(_currSession,
                                                                                         new GroupOperator(GroupOperatorType.And,
                                                                                         new BinaryOperator("TransferOrderLine", _locTransOrderLine),
                                                                                         new BinaryOperator("Select", true)));

                                    if (_locTransferOrderLots != null && _locTransferOrderLots.Count() > 0)
                                    {
                                        foreach (TransferOrderLot _locTransferOrderLot in _locTransferOrderLots)
                                        {
                                            if (_locTransferOrderLot.Status == Status.Progress || _locTransferOrderLot.Status == Status.Posted)
                                            {
                                                _locTransferOrderLot.Select = false;
                                                _locTransferOrderLot.Save();
                                                _locTransferOrderLot.Session.CommitTransaction();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferOrder " + ex.ToString());
            }
        }

        //Menentukan Status Deliver Pada Transfer Order Line
        private void SetStatusPostingTransferOrderLineOld(Session _currSession, TransferOrder _transferOrderXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_transferOrderXPO != null)
                {
                    XPCollection<TransferOrderLine> _locTransOrderLines = new XPCollection<TransferOrderLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("TransferOrder", _transferOrderXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locTransOrderLines != null && _locTransOrderLines.Count > 0)
                    {

                        foreach (TransferOrderLine _locTransOrderLine in _locTransOrderLines)
                        {
                            if (_locTransOrderLine.Status == Status.Progress || _locTransOrderLine.Status == Status.Posted)
                            {
                                if (_locTransOrderLine.RmDQty == 0 && _locTransOrderLine.RmQty == 0 && _locTransOrderLine.RmTQty == 0)
                                {
                                    _locTransOrderLine.Status = Status.Close;
                                    _locTransOrderLine.ActivationPosting = true;
                                    _locTransOrderLine.StatusDate = now;
                                }
                                else
                                {
                                    _locTransOrderLine.Status = Status.Posted;
                                    _locTransOrderLine.StatusDate = now;
                                }

                                XPCollection<TransferOrderLot> _locTransferOrderLots = new XPCollection<TransferOrderLot>(_currSession,
                                                                                         new GroupOperator(GroupOperatorType.And,
                                                                                         new BinaryOperator("TransferOrderLine", _locTransOrderLine),
                                                                                         new BinaryOperator("Select", true)));

                                if (_locTransferOrderLots != null && _locTransferOrderLots.Count() > 0)
                                {
                                    foreach (TransferOrderLot _locTransferOrderLot in _locTransferOrderLots)
                                    {
                                        if (_locTransferOrderLot.Status == Status.Progress || _locTransferOrderLot.Status == Status.Posted)
                                        {
                                            _locTransferOrderLot.Status = Status.Close;
                                            _locTransferOrderLot.ActivationPosting = true;
                                            _locTransferOrderLot.StatusDate = now;
                                            _locTransferOrderLot.Save();
                                            _locTransferOrderLot.Session.CommitTransaction();
                                        }
                                    }
                                }

                                _locTransOrderLine.Save();
                                _locTransOrderLine.Session.CommitTransaction();

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferOrder " + ex.ToString());
            }
        }

        //Menentukan Status Deliver Pada Transfer Order
        private void SetFinalStatusPostingTransferOrderOld(Session _currSession, TransferOrder _transferOrderXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locCount = 0;

                if (_transferOrderXPO != null)
                {
                    XPCollection<TransferOrderLine> _locTransOrderLines = new XPCollection<TransferOrderLine>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("TransferOrder", _transferOrderXPO)));

                    if (_locTransOrderLines != null && _locTransOrderLines.Count() > 0)
                    {
                        foreach (TransferOrderLine _locTransOrderLine in _locTransOrderLines)
                        {
                            if (_locTransOrderLine.Status == Status.Close)
                            {
                                _locCount = _locCount + 1;
                            }
                        }

                        if (_locCount == _locTransOrderLines.Count())
                        {
                            _transferOrderXPO.ActivationPosting = true;
                            _transferOrderXPO.Status = Status.Close;
                            _transferOrderXPO.StatusDate = now;
                            _transferOrderXPO.Save();
                            _transferOrderXPO.Session.CommitTransaction();
                        }
                        else
                        {
                            _transferOrderXPO.ActivationPosting = true;
                            _transferOrderXPO.Status = Status.Posted;
                            _transferOrderXPO.StatusDate = now;
                            _transferOrderXPO.Save();
                            _transferOrderXPO.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferOrder " + ex.ToString());
            }
        }

        private bool CheckAvailableWarehouseSetupDetailOld(Session _currSession, Location _locLocation, UserAccess _locUserAccess)
        {
            bool _result = false;
            int _countResult = 0;
            try
            {
                if (_locUserAccess != null)
                {
                    if (_locLocation != null)
                    {
                        
                        XPCollection<WarehouseSetupDetail> _locWhsSetupDetails = new XPCollection<WarehouseSetupDetail>(_currSession,
                                                                                        new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("UserAccess", _locUserAccess),
                                                                                        new BinaryOperator("Owner", true),
                                                                                        new BinaryOperator("Active", true)));
                        if (_locWhsSetupDetails != null && _locWhsSetupDetails.Count() > 0)
                        {
                            foreach(WarehouseSetupDetail _locWhsSetupDetail in _locWhsSetupDetails)
                            {
                                if (_locWhsSetupDetail.Location == _locLocation)
                                {
                                    _countResult = _countResult + 1;
                                } 
                            }
                        }

                        if (_countResult > 0)
                        {
                            _result = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = TransferOrder ", ex.ToString());
            }
            return _result;
        }

        #endregion TransferOrderOld

        #region Approval Process

        private void SetApprovalLine(Session _currentSession, TransferOrder _locTransferOrderXPO, ApprovalLevel _locApprovalLevel)
        {
            try
            {
                DateTime now = DateTime.Now;

                ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("TransferOrder", _locTransferOrderXPO),
                                                                         new BinaryOperator("ApprovalLevel", _locApprovalLevel)));

                if (_locApprovalLineXPO == null)
                {
                    ApprovalLine _saveDataAL2 = new ApprovalLine(_currentSession)
                    {
                        ApprovalDate = now,
                        ApprovalStatus = Status.Approved,
                        ApprovalLevel = _locApprovalLevel,
                        TransferOrder = _locTransferOrderXPO,
                    };
                    _saveDataAL2.Save();
                    _saveDataAL2.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferOrder " + ex.ToString());
            }
        }

        #endregion Approval Process

        #region Email

        private string BackgroundBody(Session _currentSession, TransferOrder _locTransferOrderXPO, MailSetupDetail _locMailSetDetail)
        {
            string body = string.Empty;

            TransferOrder _locTransferOrders = _currentSession.FindObject<TransferOrder>
                                           (new GroupOperator(GroupOperatorType.And,
                                            new BinaryOperator("Code", _locTransferOrderXPO.Code)));

            ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                               (new GroupOperator(GroupOperatorType.And,
                                                new BinaryOperator("TransferOrder", _locTransferOrders)));

            string Status = _locApprovalLineXPO.ApprovalStatus.ToString();

            using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Email/EmailTemplate.html")))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{Code}", _locTransferOrderXPO.Code);

            #region Level
            if (_locTransferOrders.ActiveApproved1 == true)
            {
                string Active = "Level 1";
                body = body.Replace("{Level}", Active);
            }
            else if (_locTransferOrders.ActiveApproved2 == true)
            {
                string Active = "Level 2";
                body = body.Replace("{Level}", Active);
            }
            else if (_locTransferOrders.ActiveApproved3 == true)
            {
                string Active = "Level 3";
                body = body.Replace("{Level}", Active);
            }
            else
            {
                string Active = "Not Available";
                body = body.Replace("{Level}", Active);
            }
            #endregion Level

            body = body.Replace("{Status}", Status);
            body = body.Replace("{Message}", _locMailSetDetail.MessageBody);
            return body;
        }

        protected void SendEmail(Session _currentSession, TransferOrder _locTransferOrderXPO, Status _locStatus, ApprovalLine _locApprovalLineXPO2)
        {
            DateTime now = DateTime.Now;
            var User = SecuritySystem.CurrentUserName;
            GlobalFunction _globFunc = new GlobalFunction();
            UserAccess _numLineUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", User));
            string _locMessageBody = null;

            try
            {
                if (_locTransferOrderXPO != null)
                {
                    XPCollection<MailSetupDetail> _locMailSetDetails = new XPCollection<MailSetupDetail>
                                                                       (_currentSession, new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("ObjectList", ObjectList.TransferOrder),
                                                                        new BinaryOperator("UserAccess", _numLineUserAccess),
                                                                        new BinaryOperator("Active", true)));

                    if (_locMailSetDetails != null && _locMailSetDetails.Count > 0)
                    {
                        foreach (MailSetupDetail _locMailSetDetail in _locMailSetDetails)
                        {
                            if (_locMailSetDetail.MessageBody != null)
                            {
                                if (_locApprovalLineXPO2.EndApproval == true)
                                {
                                    _locMessageBody = this.BackgroundBody(_currentSession, _locTransferOrderXPO, _locMailSetDetail);
                                }
                                else
                                {
                                    _locMessageBody = this.BackgroundBody(_currentSession, _locTransferOrderXPO, _locMailSetDetail);
                                }
                            }

                            _globFunc.SetAndSendMail(_locMailSetDetail.SmtpHost, _locMailSetDetail.SmtpPort,
                            _locMailSetDetail.MailFrom, _locMailSetDetail.MailFromPassword, _locMailSetDetail.MailTo, _locMailSetDetail.MailSubject, _locMessageBody);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferOrder " + ex.ToString());
            }
        }

        #endregion Email

        #region Global Method

        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0} !", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method

        
    }
}
