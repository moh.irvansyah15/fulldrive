﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;
using System.IO;
using System.Web;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class ShipmentSalesInvoiceActionController : ViewController
    {

        private ChoiceActionItem _selectionListviewFilter;
        private ChoiceActionItem _selectionApprovalListviewFilter;
        private ChoiceActionItem _setApprovalLevel;

        public ShipmentSalesInvoiceActionController()
        {
            InitializeComponent();
            #region FilterStatus
            ShipmentSalesInvoiceListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                ShipmentSalesInvoiceListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
            #endregion FilterStatus

            #region FilterApproval
            ShipmentSalesInvoiceListviewFilterApprovalSelectionAction.Items.Clear();
            foreach (object _currApprovalFilter in Enum.GetValues(typeof(CustomProcess.ApprovalFilter)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.ApprovalFilter));
                _selectionApprovalListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApprovalFilter), _currApprovalFilter);
                ShipmentSalesInvoiceListviewFilterApprovalSelectionAction.Items.Add(_selectionApprovalListviewFilter);
            }
            #endregion FilterApproval
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
            #region Approval

            var _locUser = SecuritySystem.CurrentUserName;
            GlobalFunction _globalFunction = new GlobalFunction();
            Session _currentSession = null;
            if (this.ObjectSpace != null)
            {
                _currentSession = ((XPObjectSpace)this.ObjectSpace).Session;
            }

            if (_currentSession != null)
            {
                UserAccess _locUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", _locUser));

                if (_locUserAccess != null)
                {
                    ShipmentSalesInvoiceApprovalAction.Items.Clear();

                    XPCollection<ApplicationSetupDetail> _locAppSetupDetails = new XPCollection<ApplicationSetupDetail>
                                                                                 (_currentSession, new GroupOperator(GroupOperatorType.And,
                                                                                  new BinaryOperator("UserAccess", _locUserAccess),
                                                                                  new BinaryOperator("ObjectList", CustomProcess.ObjectList.ShipmentPurchaseInvoice),
                                                                                  new BinaryOperator("FunctionList", CustomProcess.FunctionList.Approval),
                                                                                  new BinaryOperator("Active", true)));

                    if (_locAppSetupDetails != null && _locAppSetupDetails.Count > 0)
                    {

                        foreach (ApplicationSetupDetail _locAppSetupDetail in _locAppSetupDetails)
                        {
                            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.ApprovalLevel)))
                            {
                                if (_locAppSetupDetail.ApprovalLevel != CustomProcess.ApprovalLevel.None)
                                {
                                    EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.ApprovalLevel));
                                    _setApprovalLevel = new ChoiceActionItem(_ed.GetCaption(_locAppSetupDetail.ApprovalLevel), _locAppSetupDetail.ApprovalLevel);
                                }
                            }
                            ShipmentSalesInvoiceApprovalAction.Items.Add(_setApprovalLevel);
                        }
                    }
                }
            }
            #endregion Approval
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void ShipmentSalesInvoiceGetSSMAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;


                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        ShipmentSalesInvoice _locShipmentSalesInvoiceOS = (ShipmentSalesInvoice)_objectSpace.GetObject(obj);

                        if (_locShipmentSalesInvoiceOS != null)
                        {
                            if (_locShipmentSalesInvoiceOS.Code != null)
                            {
                                _currObjectId = _locShipmentSalesInvoiceOS.Code;

                                ShipmentSalesInvoice _locShipmentSalesInvoiceXPO = _currSession.FindObject<ShipmentSalesInvoice>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locShipmentSalesInvoiceXPO != null)
                                {
                                    if (_locShipmentSalesInvoiceXPO.Status == Status.Open || _locShipmentSalesInvoiceXPO.Status == Status.Progress)
                                    {
                                        XPCollection<ShipmentSalesCollection> _locShipmentSalesCollections = new XPCollection<ShipmentSalesCollection>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("ShipmentSalesInvoice", _locShipmentSalesInvoiceXPO),
                                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("Status", Status.Open),
                                                                                        new BinaryOperator("Status", Status.Progress))));

                                        if (_locShipmentSalesCollections != null && _locShipmentSalesCollections.Count() > 0)
                                        {
                                            foreach (ShipmentSalesCollection _locShipmentSalesCollection in _locShipmentSalesCollections)
                                            {
                                                if (_locShipmentSalesCollection.ShipmentSalesInvoice != null && _locShipmentSalesCollection.ShipmentSalesMonitoring != null)
                                                {
                                                    GetShipmentSalesMonitoring(_currSession, _locShipmentSalesCollection.ShipmentSalesMonitoring, _locShipmentSalesInvoiceXPO);
                                                    SetOutstandingAmount(_currSession, _locShipmentSalesCollection.ShipmentSalesMonitoring);
                                                    SetPostedAmount(_currSession, _locShipmentSalesCollection.ShipmentSalesMonitoring);
                                                    SetProcessCount(_currSession, _locShipmentSalesCollection.ShipmentSalesMonitoring);
                                                    SetStatusShipmentPurchaseMonitoring(_currSession, _locShipmentSalesCollection.ShipmentSalesMonitoring);
                                                    SetNormalAmount(_currSession, _locShipmentSalesCollection.ShipmentSalesMonitoring);
                                                }
                                            }
                                            SuccessMessageShow("ShipmentSalesCollection Has Been Successfully Getting into Shipment Sales Invoice");
                                        }
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("ShipmentSalesCollection Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("ShipmentSalesCollection Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ShipmentSalesCollection " + ex.ToString());
            }
        }

        private void ShipmentSalesInvoiceProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                Status _locStatus = Status.Open;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        ShipmentSalesInvoice _locShipmentSalesInvoiceOS = (ShipmentSalesInvoice)_objectSpace.GetObject(obj);

                        if (_locShipmentSalesInvoiceOS != null)
                        {
                            if (_locShipmentSalesInvoiceOS.Code != null)
                            {
                                _currObjectId = _locShipmentSalesInvoiceOS.Code;

                                ShipmentSalesInvoice _locShipmentSalesInvoiceXPO = _currSession.FindObject<ShipmentSalesInvoice>
                                                                   (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId)));

                                if (_locShipmentSalesInvoiceXPO != null)
                                {
                                    if (_locShipmentSalesInvoiceXPO.Status == Status.Open || _locShipmentSalesInvoiceXPO.Status == Status.Progress || _locShipmentSalesInvoiceXPO.Status == Status.Posted)
                                    {
                                        if (_locShipmentSalesInvoiceXPO.Status == Status.Open || _locShipmentSalesInvoiceXPO.Status == Status.Progress)
                                        {
                                            _locStatus = Status.Progress;

                                        }
                                        else if (_locShipmentSalesInvoiceXPO.Status == Status.Posted)
                                        {
                                            _locStatus = Status.Posted;
                                        }

                                        _locShipmentSalesInvoiceXPO.Status = _locStatus;
                                        _locShipmentSalesInvoiceXPO.StatusDate = now;
                                        _locShipmentSalesInvoiceXPO.Save();
                                        _locShipmentSalesInvoiceXPO.Session.CommitTransaction();

                                        #region ShipmentSalesInvoiceLine
                                        XPCollection<ShipmentSalesInvoiceLine> _locShipmentSalesInvoiceLines = new XPCollection<ShipmentSalesInvoiceLine>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("ShipmentSalesInvoice", _locShipmentSalesInvoiceXPO)));

                                        if (_locShipmentSalesInvoiceLines != null && _locShipmentSalesInvoiceLines.Count > 0)
                                        {
                                            foreach (ShipmentSalesInvoiceLine _locShipmentSalesInvoiceLine in _locShipmentSalesInvoiceLines)
                                            {
                                                if (_locShipmentSalesInvoiceLine.Status == Status.Open)
                                                {
                                                    _locShipmentSalesInvoiceLine.Status = Status.Progress;
                                                    _locShipmentSalesInvoiceLine.StatusDate = now;
                                                    _locShipmentSalesInvoiceLine.Save();
                                                    _locShipmentSalesInvoiceLine.Session.CommitTransaction();
                                                }
                                            }
                                        }
                                        #endregion ShipmentSalesInvoiceLine

                                        #region ShipmentSalesCollection
                                        {
                                            XPCollection<ShipmentSalesCollection> _locShipmentSalesCollections = new XPCollection<ShipmentSalesCollection>
                                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                            new BinaryOperator("ShipmentSalesInvoice", _locShipmentSalesInvoiceXPO)));
                                            if (_locShipmentSalesCollections != null && _locShipmentSalesCollections.Count() > 0)
                                            {
                                                foreach (ShipmentSalesCollection _locShipmentSalesCollection in _locShipmentSalesCollections)
                                                {
                                                    _locShipmentSalesCollection.Status = Status.Progress;
                                                    _locShipmentSalesCollection.StatusDate = now;
                                                    _locShipmentSalesCollection.Save();
                                                    _locShipmentSalesCollection.Session.CommitTransaction();
                                                }
                                            }
                                        }
                                        #endregion ShipmentSalesCollection

                                        SuccessMessageShow(_locShipmentSalesInvoiceXPO.Code + " has been change successfully to Progress ");
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("ShipmentSalesInvoice Data Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("ShipmentSalesInvoice Data Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ShipmentSalesInvoice " + ex.ToString());
            }
        }

        private void ShipmentSalesInvoiceGetBillAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                double _locTotalBill = 0;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        ShipmentSalesInvoice _locShipmentSalesInvoiceOS = (ShipmentSalesInvoice)_objectSpace.GetObject(obj);

                        if (_locShipmentSalesInvoiceOS != null)
                        {
                            if (_locShipmentSalesInvoiceOS.Code != null)
                            {
                                _currObjectId = _locShipmentSalesInvoiceOS.Code;

                                ShipmentSalesInvoice _locShipmentSalesInvoiceXPO = _currSession.FindObject<ShipmentSalesInvoice>
                                                                   (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId)));

                                if (_locShipmentSalesInvoiceXPO != null)
                                {

                                    if (_locShipmentSalesInvoiceXPO.Status == Status.Progress || _locShipmentSalesInvoiceXPO.Status == Status.Posted)
                                    {
                                        ApprovalLine _locApprovalLineXPO = _currSession.FindObject<ApprovalLine>
                                                                           (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("EndApproval", true),
                                                                            new BinaryOperator("ShipmentPurchaseInvoice", _locShipmentSalesInvoiceXPO)));

                                        if (_locApprovalLineXPO == null)
                                        {

                                            XPCollection<ShipmentSalesInvoiceLine> _locShipmentSalesInvoiceLines = new XPCollection<ShipmentSalesInvoiceLine>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("ShipmentPurchaseInvoice", _locShipmentSalesInvoiceXPO),
                                                                                                new BinaryOperator("Select", true)));

                                            if (_locShipmentSalesInvoiceLines != null && _locShipmentSalesInvoiceLines.Count > 0)
                                            {
                                                foreach (ShipmentSalesInvoiceLine _locShipmentSalesInvoiceLine in _locShipmentSalesInvoiceLines)
                                                {
                                                    if (_locShipmentSalesInvoiceLine.Status == Status.Progress)
                                                    {
                                                        _locTotalBill = _locTotalBill + _locShipmentSalesInvoiceLine.Amount;
                                                        _locShipmentSalesInvoiceLine.Status = Status.Lock;
                                                        _locShipmentSalesInvoiceLine.StatusDate = now;
                                                        _locShipmentSalesInvoiceLine.Save();
                                                        _locShipmentSalesInvoiceLine.Session.CommitTransaction();
                                                    }
                                                }
                                            }
                                        }

                                        SuccessMessageShow(_locShipmentSalesInvoiceXPO.Code + "Has been change successfully to Get Max Bill");
                                    }



                                    _locShipmentSalesInvoiceXPO.MaxAmount = _locTotalBill;
                                    _locShipmentSalesInvoiceXPO.Amount = _locTotalBill;
                                    _locShipmentSalesInvoiceXPO.Status = Status.Lock;
                                    _locShipmentSalesInvoiceXPO.StatusDate = now;
                                    _locShipmentSalesInvoiceXPO.Save();
                                    _locShipmentSalesInvoiceXPO.Session.CommitTransaction();
                                }
                                else
                                {
                                    ErrorMessageShow("Data ShipmentSalesInvoice Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data ShipmentSalesInvoice Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ShipmentSalesInvoice " + ex.ToString());
            }
        }

        private void ShipmentSalesInvoiceApprovalAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                ApplicationSetupDetail _locAppSetDetail = null;
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                var User = SecuritySystem.CurrentUserName;
                Session _currentSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currentSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                UserAccess _locUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", User));

                foreach (Object obj in _objectsToProcess)
                {
                    ShipmentPurchaseInvoice _objInNewObjectSpace = (ShipmentPurchaseInvoice)_objectSpace.GetObject(obj);

                    if (_objInNewObjectSpace != null)
                    {
                        if (_objInNewObjectSpace.Code != null)
                        {
                            _currObjectId = _objInNewObjectSpace.Code;
                        }
                    }

                    if (_currObjectId != null)
                    {
                        ShipmentSalesInvoice _locShipmentSalesInvoiceXPO = _currentSession.FindObject<ShipmentSalesInvoice>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("Code", _currObjectId)));

                        if (_locShipmentSalesInvoiceXPO != null)
                        {
                            if (_locShipmentSalesInvoiceXPO.Status == Status.Progress)
                            {
                                ShipmentApprovalLine _locShipmentApprovalLine = _currentSession.FindObject<ShipmentApprovalLine>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("ShipmentSalesInvoice", _locShipmentSalesInvoiceXPO),
                                                                             new BinaryOperator("EndApproval", true)));
                                if (_locShipmentApprovalLine == null)
                                {
                                    #region Approval Level 1
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level1)
                                    {
                                        _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level1, ObjectList.ShipmentSalesInvoice);

                                        if (_locAppSetDetail != null)
                                        {
                                            //Buat bs input langsung ke approvalline
                                            ShipmentApprovalLine _locShipmentApprovalLineXPO = _currentSession.FindObject<ShipmentApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("ShipmentSalesInvoice", _locShipmentSalesInvoiceXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level1)));
                                            if (_locShipmentApprovalLineXPO == null)
                                            {

                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ShipmentApprovalLine _saveDataAL = new ShipmentApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level1,
                                                        EndApproval = true,
                                                        ShipmentSalesInvoice = _locShipmentSalesInvoiceXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locShipmentSalesInvoiceXPO.ActivationPosting = true;
                                                    _locShipmentSalesInvoiceXPO.ActiveApproved1 = false;
                                                    _locShipmentSalesInvoiceXPO.ActiveApproved2 = false;
                                                    _locShipmentSalesInvoiceXPO.ActiveApproved3 = true;
                                                    _locShipmentSalesInvoiceXPO.Save();
                                                    _locShipmentSalesInvoiceXPO.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ShipmentApprovalLine _saveDataAL = new ShipmentApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level1,
                                                        ShipmentSalesInvoice = _locShipmentSalesInvoiceXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locShipmentSalesInvoiceXPO.ActiveApproved1 = true;
                                                    _locShipmentSalesInvoiceXPO.ActiveApproved2 = false;
                                                    _locShipmentSalesInvoiceXPO.ActiveApproved3 = false;
                                                    _locShipmentSalesInvoiceXPO.Save();
                                                    _locShipmentSalesInvoiceXPO.Session.CommitTransaction();
                                                }

                                                //Send Email
                                                ShipmentApprovalLine _locShipmentApprovalLineXPO2 = _currentSession.FindObject<ShipmentApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("ShipmentSalesInvoice", _locShipmentSalesInvoiceXPO)));
                                                if (_locShipmentApprovalLineXPO2 != null)
                                                {
                                                    SendEmail(_currentSession, _locShipmentSalesInvoiceXPO, Status.Progress, _locShipmentApprovalLineXPO2);
                                                }

                                                SuccessMessageShow("ShipmentSalesInvoice has successfully Approve");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 1

                                    #region Approval Level 2
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level2)
                                    {
                                        _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level2, ObjectList.ShipmentSalesInvoice);

                                        if (_locAppSetDetail != null)
                                        {
                                            ShipmentApprovalLine _locShipmentApprovalLineXPO = _currentSession.FindObject<ShipmentApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("ShipmentSalesInvoice", _locShipmentSalesInvoiceXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level2)));

                                            if (_locShipmentApprovalLineXPO == null)
                                            {
                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ShipmentApprovalLine _saveDataAL = new ShipmentApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level2,
                                                        EndApproval = true,
                                                        ShipmentSalesInvoice = _locShipmentSalesInvoiceXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locShipmentSalesInvoiceXPO.ActivationPosting = true;
                                                    _locShipmentSalesInvoiceXPO.ActiveApproved1 = false;
                                                    _locShipmentSalesInvoiceXPO.ActiveApproved2 = false;
                                                    _locShipmentSalesInvoiceXPO.ActiveApproved3 = true;
                                                    _locShipmentSalesInvoiceXPO.Save();
                                                    _locShipmentSalesInvoiceXPO.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ShipmentApprovalLine _saveDataAL = new ShipmentApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level2,
                                                        ShipmentSalesInvoice = _locShipmentSalesInvoiceXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    SetApprovalLine(_currentSession, _locShipmentSalesInvoiceXPO, ApprovalLevel.Level1);

                                                    _locShipmentSalesInvoiceXPO.ActiveApproved1 = false;
                                                    _locShipmentSalesInvoiceXPO.ActiveApproved2 = true;
                                                    _locShipmentSalesInvoiceXPO.ActiveApproved3 = false;
                                                    _locShipmentSalesInvoiceXPO.Save();
                                                    _locShipmentSalesInvoiceXPO.Session.CommitTransaction();
                                                }

                                                //Send Email
                                                ShipmentApprovalLine _locShipmentApprovalLineXPO2 = _currentSession.FindObject<ShipmentApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("ShipmentSalesInvoice", _locShipmentSalesInvoiceXPO)));
                                                if (_locShipmentApprovalLineXPO2 != null)
                                                {
                                                    SendEmail(_currentSession, _locShipmentSalesInvoiceXPO, Status.Progress, _locShipmentApprovalLineXPO2);
                                                }

                                                SuccessMessageShow("ShipmentSalesInvoice has successfully Approve");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 2

                                    #region Approval Level 3
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level3)
                                    {
                                        _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level3, ObjectList.ShipmentSalesInvoice);

                                        if (_locAppSetDetail != null)
                                        {
                                            ShipmentApprovalLine _locShipmentApprovalLineXPO = _currentSession.FindObject<ShipmentApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("ShipmentSalesInvoice", _locShipmentSalesInvoiceXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level3)));

                                            if (_locShipmentApprovalLineXPO == null)
                                            {
                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ShipmentApprovalLine _saveDataAL = new ShipmentApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level3,
                                                        EndApproval = true,
                                                        ShipmentSalesInvoice = _locShipmentSalesInvoiceXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locShipmentSalesInvoiceXPO.ActivationPosting = true;
                                                    _locShipmentSalesInvoiceXPO.ActiveApproved1 = false;
                                                    _locShipmentSalesInvoiceXPO.ActiveApproved2 = false;
                                                    _locShipmentSalesInvoiceXPO.ActiveApproved3 = true;
                                                    _locShipmentSalesInvoiceXPO.Save();
                                                    _locShipmentSalesInvoiceXPO.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ShipmentApprovalLine _saveDataAL = new ShipmentApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level3,
                                                        ShipmentSalesInvoice = _locShipmentSalesInvoiceXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    SetApprovalLine(_currentSession, _locShipmentSalesInvoiceXPO, ApprovalLevel.Level2);
                                                    SetApprovalLine(_currentSession, _locShipmentSalesInvoiceXPO, ApprovalLevel.Level1);

                                                    _locShipmentSalesInvoiceXPO.ActiveApproved1 = false;
                                                    _locShipmentSalesInvoiceXPO.ActiveApproved2 = false;
                                                    _locShipmentSalesInvoiceXPO.ActiveApproved3 = true;
                                                    _locShipmentSalesInvoiceXPO.Save();
                                                    _locShipmentSalesInvoiceXPO.Session.CommitTransaction();
                                                }

                                                //Send Email
                                                ShipmentApprovalLine _locShipmentApprovalLineXPO2 = _currentSession.FindObject<ShipmentApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("ShipmentSalesInvoice", _locShipmentSalesInvoiceXPO)));
                                                if (_locShipmentApprovalLineXPO2 != null)
                                                {
                                                    SendEmail(_currentSession, _locShipmentSalesInvoiceXPO, Status.Progress, _locShipmentApprovalLineXPO2);
                                                }

                                                SuccessMessageShow("ShipmentSalesInvoice has successfully Approve");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 3
                                }

                            }
                            else
                            {
                                ErrorMessageShow("ShipmentSalesInvoice Status Not Available");
                            }
                        }
                        else
                        {
                            ErrorMessageShow("ShipmentSalesInvoice Not Available");
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ShipmentPurchaseInvoice " + ex.ToString());
            }
        }

        private void ShipmentSalesInvoicePostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                //Mengirim informasi ke Payable Transaction --HutangDagang Dan --BankAccount Perusahaan yg aktif default
                //Meng-akumulasi semua hal perhitungan lokal Purchase Invoice termasuk close status

                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        ShipmentSalesInvoice _locShipmentSalesInvoiceOS = (ShipmentSalesInvoice)_objectSpace.GetObject(obj);

                        if (_locShipmentSalesInvoiceOS != null)
                        {
                            if (_locShipmentSalesInvoiceOS.Code != null)
                            {
                                _currObjectId = _locShipmentSalesInvoiceOS.Code;

                                ShipmentSalesInvoice _locShipmentSalesInvoiceXPO = _currSession.FindObject<ShipmentSalesInvoice>
                                                                   (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId)));

                                if (_locShipmentSalesInvoiceXPO != null)
                                {
                                    if (_locShipmentSalesInvoiceXPO.Status == Status.Lock || _locShipmentSalesInvoiceXPO.Status == Status.Posted)
                                    {
                                        ApprovalLine _locApprovalLineXPO = _currSession.FindObject<ApprovalLine>
                                                                           (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("EndApproval", true),
                                                                            new BinaryOperator("ShipmentSalesInvoice", _locShipmentSalesInvoiceXPO)));

                                        if (_locApprovalLineXPO != null)
                                        {
                                            if (_locShipmentSalesInvoiceXPO.Amount > 0)
                                            {
                                                //Berdasarkan Outstanding dari PaymentOutPlan yg mana di hitung berdasarkan jumlah mak Outstanding 
                                                if (_locShipmentSalesInvoiceXPO.Amount == GetShipmentSalesInvoiceLineTotalAmount(_currSession, _locShipmentSalesInvoiceXPO))
                                                {
                                                    SetShipmentSalesInvoiceMonitoring(_currSession, _locShipmentSalesInvoiceXPO);
                                                    SetInvoiceARJournal(_currSession, _locShipmentSalesInvoiceXPO);
                                                    SetOutstandinAmountPosting(_currSession, _locShipmentSalesInvoiceXPO);
                                                    SetPostedAmountPosting(_currSession, _locShipmentSalesInvoiceXPO);
                                                    SetProcessCountPosting(_currSession, _locShipmentSalesInvoiceXPO);
                                                    SetStatusShipmentPurchaseInvoiceLine(_currSession, _locShipmentSalesInvoiceXPO);
                                                    SetNormalPay(_currSession, _locShipmentSalesInvoiceXPO);
                                                    SetNormalPayLinePosting(_currSession, _locShipmentSalesInvoiceXPO);
                                                    SetFinalShipmentPurchaseInvoicePosting(_currSession, _locShipmentSalesInvoiceXPO);
                                                    SuccessMessageShow(_locShipmentSalesInvoiceXPO.Code + " has been change successfully to Posted");
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data ShipmentSalesInvoice Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data ShipmentSalesInvoice Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ShipmentSalesInvoice " + ex.ToString());
            }
        }

        private void ShipmentSalesInvoiceListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(ShipmentSalesInvoice)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ShipmentSalesInvoice " + ex.ToString());
            }
        }

        private void ShipmentSalesInvoiceListviewFilterApprovalSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(ShipmentSalesInvoice)))
                {
                    if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.Approval)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("ActiveApproved3", false, BinaryOperatorType.Equal),
                                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("ActiveApproved1", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", true, BinaryOperatorType.Equal)));
                    }
                    else if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.EndApproval)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new BinaryOperator("ActiveApproved3", true, BinaryOperatorType.Equal);
                    }
                    else if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.AllData)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("ActiveApproved1", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved3", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved1", false, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", false, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved3", false, BinaryOperatorType.Equal));
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ShipmentSalesInvoice " + ex.ToString());
            }
        }


        //===================================================== Code Only ====================================================

        #region GetSPM

        private void GetShipmentSalesMonitoring(Session _currSession, ShipmentSalesMonitoring _locShipmentSalesMonitoringXPO, ShipmentSalesInvoice _locShipmentSalesInvoiceXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _currSignCode = null;

                if (_locShipmentSalesMonitoringXPO != null && _locShipmentSalesInvoiceXPO != null)
                {
                    if (_locShipmentSalesMonitoringXPO.Select == true)
                    {
                        _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.ShipmentSalesInvoiceLine);

                        if (_currSignCode != null)
                        {
                            ShipmentSalesInvoiceLine _saveDataShipmentSalesInvoiceLine = new ShipmentSalesInvoiceLine(_currSession)
                            {
                                Routing = _locShipmentSalesMonitoringXPO.Routing,
                                RoutingPackage = _locShipmentSalesMonitoringXPO.RoutingPackage,
                                ShipmentType = _locShipmentSalesMonitoringXPO.ShipmentType,
                                TransportType = _locShipmentSalesMonitoringXPO.TransportType,
                                TransactionTerm = _locShipmentSalesMonitoringXPO.TransactionTerm,
                                DeliveryType = _locShipmentSalesMonitoringXPO.DeliveryType,
                                ContainerType = _locShipmentSalesMonitoringXPO.ContainerType,
                                PriceType = _locShipmentSalesMonitoringXPO.PriceType,
                                CountryFrom = _locShipmentSalesMonitoringXPO.CountryFrom,
                                CityFrom = _locShipmentSalesMonitoringXPO.CityFrom,
                                AreaFrom = _locShipmentSalesMonitoringXPO.AreaFrom,
                                LocationFrom = _locShipmentSalesMonitoringXPO.LocationFrom,
                                TransportLocationFrom = _locShipmentSalesMonitoringXPO.TransportLocationFrom,
                                Shipper = _locShipmentSalesMonitoringXPO.Shipper,
                                ShipperContact = _locShipmentSalesMonitoringXPO.ShipperContact,
                                ShipperAddress = _locShipmentSalesMonitoringXPO.ShipperAddress,
                                CountryTo = _locShipmentSalesMonitoringXPO.CountryTo,
                                CityTo = _locShipmentSalesMonitoringXPO.CityTo,
                                AreaTo = _locShipmentSalesMonitoringXPO.AreaTo,
                                LocationTo = _locShipmentSalesMonitoringXPO.LocationTo,
                                TransportLocationTo = _locShipmentSalesMonitoringXPO.TransportLocationTo,
                                Consignee = _locShipmentSalesMonitoringXPO.Consignee,
                                ConsigneeContact = _locShipmentSalesMonitoringXPO.ConsigneeContact,
                                ConsigneeAddress = _locShipmentSalesMonitoringXPO.ConsigneeAddress,
                                PriceGroup = _locShipmentSalesMonitoringXPO.PriceGroup,
                                ItemGroup = _locShipmentSalesMonitoringXPO.ItemGroup,
                                Item = _locShipmentSalesMonitoringXPO.Item,
                                Name = _locShipmentSalesMonitoringXPO.Name,
                                Description = _locShipmentSalesMonitoringXPO.Description,
                                DQty = _locShipmentSalesMonitoringXPO.DQty,
                                DUOM = _locShipmentSalesMonitoringXPO.DUOM,
                                Qty = _locShipmentSalesMonitoringXPO.Qty,
                                UOM = _locShipmentSalesMonitoringXPO.UOM,
                                TQty = _locShipmentSalesMonitoringXPO.TQty,
                                MaxAmount = _locShipmentSalesMonitoringXPO.Amount,
                                Amount = _locShipmentSalesMonitoringXPO.Amount,
                                ShipmentSalesMonitoring = _locShipmentSalesMonitoringXPO,
                                ShipmentSalesInvoice = _locShipmentSalesInvoiceXPO
                            };
                            _saveDataShipmentSalesInvoiceLine.Save();
                            _saveDataShipmentSalesInvoiceLine.Session.CommitTransaction();
                        }

                        ShipmentSalesInvoiceLine _locShipmentSalesInvoiceLine = _currSession.FindObject<ShipmentSalesInvoiceLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("SignCode", _currSignCode)));

                        if (_locShipmentSalesInvoiceLine != null)
                        {
                            _locShipmentSalesMonitoringXPO.ShipmentSalesInvoice = _locShipmentSalesInvoiceXPO;
                            _locShipmentSalesMonitoringXPO.ShipmentSalesInvoiceLine = _locShipmentSalesInvoiceLine;
                            _locShipmentSalesMonitoringXPO.Save();
                            _locShipmentSalesMonitoringXPO.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = ShipmentSalesInvoice ", ex.ToString());
            }
        }

        private void SetOutstandingAmount(Session _currSession, ShipmentSalesMonitoring _locShipmentSalesMonitoringXPO)
        {
            try
            {
                double _locOutstandingAmount = 0;
                if (_locShipmentSalesMonitoringXPO != null)
                {
                    if (_locShipmentSalesMonitoringXPO.Select == true)
                    {
                        #region PostedCount == 0
                        if (_locShipmentSalesMonitoringXPO.PostedCount == 0)
                        {
                            #region MaxAmount
                            if (_locShipmentSalesMonitoringXPO.MaxAmount > 0)
                            {
                                if (_locShipmentSalesMonitoringXPO.Amount > 0 && _locShipmentSalesMonitoringXPO.Amount <= _locShipmentSalesMonitoringXPO.MaxAmount)
                                {
                                    _locOutstandingAmount = _locShipmentSalesMonitoringXPO.MaxAmount - _locShipmentSalesMonitoringXPO.Amount;
                                }
                            }
                            #endregion MaxAmount
                            #region NonMaxAmount
                            else
                            {
                                _locOutstandingAmount = 0;
                            }
                            #endregion NonMaxAmount

                        }
                        #endregion PostedCount == 0

                        #region PostedCount > 0
                        if (_locShipmentSalesMonitoringXPO.PostedCount > 0)
                        {
                            if (_locShipmentSalesMonitoringXPO.OutstandingAmount > 0)
                            {
                                _locOutstandingAmount = _locShipmentSalesMonitoringXPO.OutstandingAmount - _locShipmentSalesMonitoringXPO.Amount;
                            }
                        }
                        #endregion PostedCount > 0

                        _locShipmentSalesMonitoringXPO.OutstandingAmount = _locOutstandingAmount;
                        _locShipmentSalesMonitoringXPO.Save();
                        _locShipmentSalesMonitoringXPO.Session.CommitTransaction();
                    }

                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ShipmentSalesInvoice " + ex.ToString());
            }
        }

        private void SetPostedAmount(Session _currSession, ShipmentSalesMonitoring _locShipmentSalesMonitoringXPO)
        {
            try
            {
                double _locPostedAmount = 0;
                if (_locShipmentSalesMonitoringXPO != null)
                {
                    if (_locShipmentSalesMonitoringXPO.Select == true)
                    {
                        #region PostedCount == 0
                        if (_locShipmentSalesMonitoringXPO.PostedCount == 0)
                        {
                            #region MaxAmount
                            if (_locShipmentSalesMonitoringXPO.MaxAmount > 0)
                            {
                                if (_locShipmentSalesMonitoringXPO.Amount > 0 && _locShipmentSalesMonitoringXPO.Amount <= _locShipmentSalesMonitoringXPO.MaxAmount)
                                {
                                    _locPostedAmount = _locShipmentSalesMonitoringXPO.Amount;
                                }
                            }
                            #endregion MaxAmount
                            #region NonMaxAmount
                            else
                            {
                                if (_locShipmentSalesMonitoringXPO.Amount > 0)
                                {
                                    _locPostedAmount = _locShipmentSalesMonitoringXPO.Amount;
                                }
                            }
                            #endregion NonMaxAmount
                        }
                        #endregion PostedCount == 0

                        #region PostedCount > 0
                        if (_locShipmentSalesMonitoringXPO.PostedCount > 0)
                        {
                            if (_locShipmentSalesMonitoringXPO.PostedAmount > 0)
                            {
                                _locPostedAmount = _locShipmentSalesMonitoringXPO.PostedAmount + _locShipmentSalesMonitoringXPO.Amount;
                            }
                        }
                        #endregion PostedCount > 0

                        _locShipmentSalesMonitoringXPO.PostedAmount = _locPostedAmount;
                        _locShipmentSalesMonitoringXPO.Save();
                        _locShipmentSalesMonitoringXPO.Session.CommitTransaction();
                    }

                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ShipmentSalesInvoice " + ex.ToString());
            }
        }

        private void SetProcessCount(Session _currSession, ShipmentSalesMonitoring _locShipmentSalesMonitoringXPO)
        {
            try
            {
                if (_locShipmentSalesMonitoringXPO != null)
                {
                    if (_locShipmentSalesMonitoringXPO.Select == true)
                    {
                        _locShipmentSalesMonitoringXPO.PostedCount = _locShipmentSalesMonitoringXPO.PostedCount + 1;
                        _locShipmentSalesMonitoringXPO.Save();
                        _locShipmentSalesMonitoringXPO.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ShipmentSalesInvoice " + ex.ToString());
            }
        }

        private void SetStatusShipmentPurchaseMonitoring(Session _currSession, ShipmentSalesMonitoring _locShipmentSalesMonitoringXPO)
        {
            try
            {
                DateTime now = DateTime.Now;

                if (_locShipmentSalesMonitoringXPO != null)
                {
                    if (_locShipmentSalesMonitoringXPO.Select == true)
                    {
                        if (_locShipmentSalesMonitoringXPO.Status == Status.Progress || _locShipmentSalesMonitoringXPO.Status == Status.Posted)
                        {
                            if (_locShipmentSalesMonitoringXPO.OutstandingAmount == 0)
                            {
                                _locShipmentSalesMonitoringXPO.Status = Status.Close;
                                _locShipmentSalesMonitoringXPO.ActivationPosting = true;
                                _locShipmentSalesMonitoringXPO.StatusDate = now;
                            }
                            else
                            {
                                _locShipmentSalesMonitoringXPO.Status = Status.Posted;
                                _locShipmentSalesMonitoringXPO.StatusDate = now;
                            }
                            _locShipmentSalesMonitoringXPO.Save();
                            _locShipmentSalesMonitoringXPO.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ShipmentSalesInvoice " + ex.ToString());
            }
        }

        private void SetNormalAmount(Session _currSession, ShipmentSalesMonitoring _locShipmentSalesMonitoringXPO)
        {
            try
            {
                if (_locShipmentSalesMonitoringXPO != null)
                {
                    if (_locShipmentSalesMonitoringXPO.Select == true)
                    {
                        if (_locShipmentSalesMonitoringXPO.Status == Status.Progress || _locShipmentSalesMonitoringXPO.Status == Status.Posted || _locShipmentSalesMonitoringXPO.Status == Status.Close)
                        {
                            if (_locShipmentSalesMonitoringXPO.Amount > 0)
                            {
                                _locShipmentSalesMonitoringXPO.Select = false;
                                _locShipmentSalesMonitoringXPO.ShipmentSalesInvoice = null;
                                _locShipmentSalesMonitoringXPO.ShipmentSalesInvoiceLine = null;
                                _locShipmentSalesMonitoringXPO.Amount = 0;
                                _locShipmentSalesMonitoringXPO.Save();
                                _locShipmentSalesMonitoringXPO.Session.CommitTransaction();
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ShipmentSalesInvoice " + ex.ToString());
            }
        }

        #endregion GetSPM

        #region Approval Process

        private void SetApprovalLine(Session _currentSession, ShipmentSalesInvoice _locShipmentSalesInvoiceXPO, ApprovalLevel _locApprovalLevel)
        {
            try
            {
                DateTime now = DateTime.Now;

                ShipmentApprovalLine _locShipmentApprovalLineXPO = _currentSession.FindObject<ShipmentApprovalLine>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("ShipmentSalesInvoice", _locShipmentSalesInvoiceXPO),
                                                                         new BinaryOperator("ApprovalLevel", _locApprovalLevel)));

                if (_locShipmentApprovalLineXPO == null)
                {
                    ShipmentApprovalLine _saveDataAL2 = new ShipmentApprovalLine(_currentSession)
                    {
                        ApprovalDate = now,
                        ApprovalStatus = Status.Approved,
                        ApprovalLevel = _locApprovalLevel,
                        ShipmentSalesInvoice = _locShipmentSalesInvoiceXPO,
                    };
                    _saveDataAL2.Save();
                    _saveDataAL2.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ShipmentSalesInvoice " + ex.ToString());
            }
        }

        #endregion Approval Process

        #region Posting Method

        private double GetShipmentSalesInvoiceLineTotalAmount(Session _currSession, ShipmentSalesInvoice _locShipmentSalesInvoiceXPO)
        {
            double _return = 0;
            try
            {
                if (_locShipmentSalesInvoiceXPO != null)
                {
                    XPCollection<ShipmentSalesInvoiceLine> _locShipmentSalesInvoiceLines = new XPCollection<ShipmentSalesInvoiceLine>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("ShipmentSalesInvoice", _locShipmentSalesInvoiceXPO),
                                                                                                new BinaryOperator("Select", true),
                                                                                                new GroupOperator(GroupOperatorType.Or,
                                                                                                new BinaryOperator("Status", Status.Lock),
                                                                                                new BinaryOperator("Status", Status.Posted))));

                    if (_locShipmentSalesInvoiceLines != null && _locShipmentSalesInvoiceLines.Count > 0)
                    {
                        foreach (ShipmentSalesInvoiceLine _locShipmentSalesInvoiceLine in _locShipmentSalesInvoiceLines)
                        {
                            _return = _return + _locShipmentSalesInvoiceLine.Amount;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = ShipmentSalesInvoice" + ex.ToString());
            }
            return _return;
        }

        private void SetShipmentSalesInvoiceMonitoring(Session _currSession, ShipmentSalesInvoice _locShipmentSalesInvoiceXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;

                if (_locShipmentSalesInvoiceXPO != null)
                {
                    if (_locShipmentSalesInvoiceXPO.Status == Status.Lock || _locShipmentSalesInvoiceXPO.Status == Status.Posted)
                    {
                        XPCollection<ShipmentSalesInvoiceLine> _locShipmentSalesInvoiceLines = new XPCollection<ShipmentSalesInvoiceLine>
                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("ShipmentSalesInvoice", _locShipmentSalesInvoiceXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Lock),
                                                            new BinaryOperator("Status", Status.Posted))));

                        if (_locShipmentSalesInvoiceLines != null && _locShipmentSalesInvoiceLines.Count() > 0)
                        {
                            foreach (ShipmentSalesInvoiceLine _locShipmentSalesInvoiceLine in _locShipmentSalesInvoiceLines)
                            {
                                if (_locShipmentSalesInvoiceLine.Amount > 0)
                                {
                                    ShipmentSalesInvoiceMonitoring _saveDataShipmentSalesInvoiceMonitoring = new ShipmentSalesInvoiceMonitoring(_currSession)
                                    {
                                        ShipmentSalesInvoice = _locShipmentSalesInvoiceXPO,
                                        ShipmentSalesInvoiceLine = _locShipmentSalesInvoiceLine,
                                        Routing = _locShipmentSalesInvoiceLine.Routing,
                                        RoutingPackage = _locShipmentSalesInvoiceLine.RoutingPackage,
                                        ShipmentType = _locShipmentSalesInvoiceXPO.ShipmentType,
                                        TransportType = _locShipmentSalesInvoiceLine.TransportType,
                                        TransactionTerm = _locShipmentSalesInvoiceLine.TransactionTerm,
                                        DeliveryType = _locShipmentSalesInvoiceLine.DeliveryType,
                                        ContainerType = _locShipmentSalesInvoiceLine.ContainerType,
                                        PriceType = _locShipmentSalesInvoiceLine.PriceType,
                                        CountryFrom = _locShipmentSalesInvoiceLine.CountryFrom,
                                        CityFrom = _locShipmentSalesInvoiceLine.CityFrom,
                                        AreaFrom = _locShipmentSalesInvoiceLine.AreaFrom,
                                        LocationFrom = _locShipmentSalesInvoiceLine.LocationFrom,
                                        TransportLocationFrom = _locShipmentSalesInvoiceLine.TransportLocationFrom,
                                        Shipper = _locShipmentSalesInvoiceXPO.Shipper,
                                        ShipperContact = _locShipmentSalesInvoiceXPO.ShipperContact,
                                        ShipperAddress = _locShipmentSalesInvoiceXPO.ShipperAddress,
                                        CountryTo = _locShipmentSalesInvoiceLine.CountryTo,
                                        CityTo = _locShipmentSalesInvoiceLine.CityTo,
                                        AreaTo = _locShipmentSalesInvoiceLine.AreaTo,
                                        LocationTo = _locShipmentSalesInvoiceLine.LocationTo,
                                        TransportLocationTo = _locShipmentSalesInvoiceLine.TransportLocationTo,
                                        Consignee = _locShipmentSalesInvoiceXPO.Consignee,
                                        ConsigneeContact = _locShipmentSalesInvoiceXPO.ConsigneeContact,
                                        ConsigneeAddress = _locShipmentSalesInvoiceXPO.ConsigneeAddress,
                                        PriceGroup = _locShipmentSalesInvoiceLine.PriceGroup,
                                        ItemGroup = _locShipmentSalesInvoiceLine.ItemGroup,
                                        Item = _locShipmentSalesInvoiceLine.Item,
                                        Name = _locShipmentSalesInvoiceLine.Name,
                                        Description = _locShipmentSalesInvoiceLine.Description,
                                        DQty = _locShipmentSalesInvoiceLine.DQty,
                                        DUOM = _locShipmentSalesInvoiceLine.DUOM,
                                        Qty = _locShipmentSalesInvoiceLine.Qty,
                                        UOM = _locShipmentSalesInvoiceLine.UOM,
                                        TQty = _locShipmentSalesInvoiceLine.TQty,
                                        MaxAmount = _locShipmentSalesInvoiceLine.Amount,
                                        Amount = _locShipmentSalesInvoiceLine.Amount,
                                        ShipmentSalesMonitoring = _locShipmentSalesInvoiceLine.ShipmentSalesMonitoring,
                                        
                                    };
                                    _saveDataShipmentSalesInvoiceMonitoring.Save();
                                    _saveDataShipmentSalesInvoiceMonitoring.Session.CommitTransaction();   
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = ShipmentSalesInvoice ", ex.ToString());
            }
        }

        private void SetInvoiceARJournal(Session _currSession, ShipmentSalesInvoice _locShipmentSalesInvoiceXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                double _locBillTUAmount = 0;

                if (_locShipmentSalesInvoiceXPO != null)
                {
                    #region CreateInvoiceARJournal

                    XPCollection<ShipmentSalesInvoiceLine> _locShipmentSalesInvoiceLines = new XPCollection<ShipmentSalesInvoiceLine>
                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("ShipmentSalesInvoice", _locShipmentSalesInvoiceXPO),
                                                                                new BinaryOperator("Select", true),
                                                                                new GroupOperator(GroupOperatorType.Or,
                                                                                new BinaryOperator("Status", Status.Lock),
                                                                                new BinaryOperator("Status", Status.Posted))));

                    if (_locShipmentSalesInvoiceLines != null && _locShipmentSalesInvoiceLines.Count() > 0)
                    {
                        foreach (ShipmentSalesInvoiceLine _locShipmentPurchaseInvoiceLine in _locShipmentSalesInvoiceLines)
                        {
                            if (_locShipmentPurchaseInvoiceLine.ShipmentSalesInvoice != null)
                            {
                                #region CreateNormalInvoiceAPJournal

                                _locBillTUAmount = _locShipmentPurchaseInvoiceLine.Amount;

                                #region JournalMapBusinessPartnerAccountGroup
                                if (_locShipmentSalesInvoiceXPO.BillToBusinessPartner != null)
                                {
                                    if (_locShipmentSalesInvoiceXPO.BillToBusinessPartner.BusinessPartnerAccountGroup != null)
                                    {
                                        //Settingan Business Partner lansung pake 2 account debit dan credit
                                        XPCollection<JournalMap> _locJournalMapByBusinessPartners = new XPCollection<JournalMap>
                                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                     new BinaryOperator("BusinessPartnerAccountGroup", _locShipmentSalesInvoiceXPO.BillToBusinessPartner.BusinessPartnerAccountGroup)));

                                        if (_locJournalMapByBusinessPartners != null && _locJournalMapByBusinessPartners.Count() > 0)
                                        {
                                            foreach (JournalMap _locJournalMapByBusinessPartner in _locJournalMapByBusinessPartners)
                                            {
                                                XPCollection<JournalMapLine> _locJournalMapLineByBusinessPartners = new XPCollection<JournalMapLine>
                                                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                     new BinaryOperator("JournalMap", _locJournalMapByBusinessPartner)));

                                                if (_locJournalMapLineByBusinessPartners != null && _locJournalMapLineByBusinessPartners.Count() > 0)
                                                {
                                                    foreach (JournalMapLine _locJournalMapLineByBusinessPartner in _locJournalMapLineByBusinessPartners)
                                                    {
                                                        AccountMap _locAccountMapByBusinessPartner = _currSession.FindObject<AccountMap>
                                                                                                     (new GroupOperator(GroupOperatorType.And,
                                                                                                      new BinaryOperator("Code", _locJournalMapLineByBusinessPartner.AccountMap.Code),
                                                                                                      new BinaryOperator("PostingType", PostingType.Sales),
                                                                                                      new BinaryOperator("OrderType", OrderType.Service),
                                                                                                      new BinaryOperator("PostingMethod", PostingMethod.InvoiceAR),
                                                                                                      new BinaryOperator("PostingMethodType", PostingMethodType.Normal)));

                                                        if (_locAccountMapByBusinessPartner != null)
                                                        {
                                                            XPCollection<AccountMapLine> _locAccountMapLineByBusinessPartners = new XPCollection<AccountMapLine>
                                                                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                                 new BinaryOperator("AccountMap", _locAccountMapByBusinessPartner),
                                                                                                                                 new BinaryOperator("Active", true)));

                                                            if (_locAccountMapLineByBusinessPartners != null && _locAccountMapLineByBusinessPartners.Count() > 0)
                                                            {
                                                                double _locTotalAmountDebit = 0;
                                                                double _locTotalAmountCredit = 0;
                                                                double _locTotalBalance = 0;

                                                                foreach (AccountMapLine _locAccountMapLine in _locAccountMapLineByBusinessPartners)
                                                                {
                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                    {
                                                                        _locTotalAmountDebit = _locBillTUAmount;
                                                                    }
                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                    {
                                                                        _locTotalAmountCredit = _locBillTUAmount;
                                                                    }

                                                                    GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                    {
                                                                        PostingDate = now,
                                                                        PostingType = PostingType.Sales,
                                                                        OrderType = OrderType.Service,
                                                                        PostingMethod = PostingMethod.InvoiceAR,
                                                                        PostingMethodType = PostingMethodType.Normal,
                                                                        Account = _locAccountMapLine.Account,
                                                                        Debit = _locTotalAmountDebit,
                                                                        Credit = _locTotalAmountCredit,
                                                                        ShipmentSalesInvoice = _locShipmentSalesInvoiceXPO,
                                                                        Company = _locShipmentSalesInvoiceXPO.Company,
                                                                    };
                                                                    _saveGeneralJournal.Save();
                                                                    _saveGeneralJournal.Session.CommitTransaction();

                                                                    if (_locAccountMapLine.Account.Code != null)
                                                                    {
                                                                        ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                                 (new GroupOperator(GroupOperatorType.And,
                                                                                                  new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                                                                        if (_locCOA != null)
                                                                        {
                                                                            if (_locCOA.BalanceType == BalanceType.Change)
                                                                            {
                                                                                if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                                {
                                                                                    if (_locTotalAmountDebit > 0)
                                                                                    {
                                                                                        _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                                    }
                                                                                    if (_locTotalAmountCredit > 0)
                                                                                    {
                                                                                        _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                                    }
                                                                                }
                                                                                if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                                {
                                                                                    if (_locTotalAmountDebit > 0)
                                                                                    {
                                                                                        _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                                    }
                                                                                    if (_locTotalAmountCredit > 0)
                                                                                    {
                                                                                        _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                                    }
                                                                                }
                                                                            }

                                                                            _locCOA.Balance = _locTotalBalance;
                                                                            _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                            _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                            _locCOA.Save();
                                                                            _locCOA.Session.CommitTransaction();
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                #endregion JournalMapBusinessPartnerAccountGroup

                                #endregion CreateNormalInvoiceAPJournal   
                            }
                        }
                    }

                    #endregion CreateInvoiceARJournal
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = ShipmentPurchaseInvoice ", ex.ToString());
            }
        }

        private void SetNormalPay(Session _currSession, ShipmentSalesInvoice _locShipmentSalesInvoiceXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;

                if (_locShipmentSalesInvoiceXPO != null)
                {
                    _locShipmentSalesInvoiceXPO.Amount = 0;
                    _locShipmentSalesInvoiceXPO.Save();
                    _locShipmentSalesInvoiceXPO.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = ShipmentSalesInvoice ", ex.ToString());
            }
        }

        private void SetOutstandinAmountPosting(Session _currSession, ShipmentSalesInvoice _locShipmentSalesInvoiceXPO)
        {
            try
            {
                if (_locShipmentSalesInvoiceXPO != null)
                {
                    XPCollection<ShipmentSalesInvoiceLine> _locShipmentSalesInvoiceLines = new XPCollection<ShipmentSalesInvoiceLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("ShipmentSalesInvoice", _locShipmentSalesInvoiceXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Lock),
                                                            new BinaryOperator("Status", Status.Posted))));

                    if (_locShipmentSalesInvoiceLines != null && _locShipmentSalesInvoiceLines.Count > 0)
                    {
                        double _locOutstandingAmount = 0;

                        foreach (ShipmentSalesInvoiceLine _locShipmentSalesInvoiceLine in _locShipmentSalesInvoiceLines)
                        {
                            #region ProcessCount=0
                            if (_locShipmentSalesInvoiceLine.PostedCount == 0)
                            {
                                #region MaxQuantity
                                if (_locShipmentSalesInvoiceLine.MaxAmount > 0)
                                {
                                    if (_locShipmentSalesInvoiceLine.Amount > 0 && _locShipmentSalesInvoiceLine.Amount <= _locShipmentSalesInvoiceLine.MaxAmount)
                                    {
                                        _locOutstandingAmount = _locShipmentSalesInvoiceLine.MaxAmount - _locShipmentSalesInvoiceLine.Amount;
                                    }

                                }
                                #endregion MaxQuantity

                                #region NonMaxQuantity
                                else
                                {
                                    if (_locShipmentSalesInvoiceLine.Amount > 0)
                                    {
                                        _locOutstandingAmount = 0;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion ProcessCount=0

                            #region ProcessCount>0
                            if (_locShipmentSalesInvoiceLine.PostedCount > 0)
                            {
                                if (_locShipmentSalesInvoiceLine.OutstandingAmount > 0)
                                {
                                    _locOutstandingAmount = _locShipmentSalesInvoiceLine.OutstandingAmount - _locShipmentSalesInvoiceLine.Amount;
                                }
                            }
                            #endregion ProcessCount>0

                            _locShipmentSalesInvoiceLine.OutstandingAmount = _locOutstandingAmount;
                            _locShipmentSalesInvoiceLine.Save();
                            _locShipmentSalesInvoiceLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ShipmentSalesInvoice " + ex.ToString());
            }
        }

        private void SetPostedAmountPosting(Session _currSession, ShipmentSalesInvoice _locShipmentSalesInvoiceXPO)
        {
            try
            {
                if (_locShipmentSalesInvoiceXPO != null)
                {
                    XPCollection<ShipmentSalesInvoiceLine> _locShipmentSalesInvoiceLines = new XPCollection<ShipmentSalesInvoiceLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("ShipmentSalesInvoice", _locShipmentSalesInvoiceXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Lock),
                                                            new BinaryOperator("Status", Status.Posted))));

                    if (_locShipmentSalesInvoiceLines != null && _locShipmentSalesInvoiceLines.Count > 0)
                    {
                        double _locPostedAmount = 0;

                        foreach (ShipmentSalesInvoiceLine _locShipmentSalesInvoiceLine in _locShipmentSalesInvoiceLines)
                        {
                            #region ProcessCount=0
                            if (_locShipmentSalesInvoiceLine.PostedCount == 0)
                            {
                                #region MaxQuantity
                                if (_locShipmentSalesInvoiceLine.MaxAmount > 0)
                                {
                                    if (_locShipmentSalesInvoiceLine.Amount > 0 && _locShipmentSalesInvoiceLine.Amount <= _locShipmentSalesInvoiceLine.MaxAmount)
                                    {
                                        _locPostedAmount = _locShipmentSalesInvoiceLine.Amount;
                                    }
                                }
                                #endregion MaxQuantity

                                #region NonMaxQuantity
                                else
                                {
                                    if (_locShipmentSalesInvoiceLine.Amount > 0)
                                    {
                                        _locPostedAmount = _locShipmentSalesInvoiceLine.Amount;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion ProcessCount=0

                            #region ProcessCount>0
                            if (_locShipmentSalesInvoiceLine.PostedCount > 0)
                            {
                                if (_locShipmentSalesInvoiceLine.PostedAmount > 0)
                                {
                                    _locPostedAmount = _locShipmentSalesInvoiceLine.PostedAmount + _locShipmentSalesInvoiceLine.Amount;
                                }
                            }
                            #endregion ProcessCount>0

                            _locShipmentSalesInvoiceLine.PostedAmount = _locPostedAmount;
                            _locShipmentSalesInvoiceLine.Save();
                            _locShipmentSalesInvoiceLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ShipmentSalesInvoice " + ex.ToString());
            }
        }

        private void SetProcessCountPosting(Session _currSession, ShipmentSalesInvoice _locShipmentSalesInvoiceXPO)
        {
            try
            {
                if (_locShipmentSalesInvoiceXPO != null)
                {
                    XPCollection<ShipmentSalesInvoiceLine> _locShipmentSalesInvoiceLines = new XPCollection<ShipmentSalesInvoiceLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("ShipmentSalesInvoice", _locShipmentSalesInvoiceXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Lock),
                                                            new BinaryOperator("Status", Status.Posted))));

                    if (_locShipmentSalesInvoiceLines != null && _locShipmentSalesInvoiceLines.Count > 0)
                    {

                        foreach (ShipmentSalesInvoiceLine _locShipmentSalesInvoiceLine in _locShipmentSalesInvoiceLines)
                        {
                            _locShipmentSalesInvoiceLine.PostedCount = _locShipmentSalesInvoiceLine.PostedCount + 1;
                            _locShipmentSalesInvoiceLine.Save();
                            _locShipmentSalesInvoiceLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ShipmentSalesInvoice " + ex.ToString());
            }
        }

        private void SetStatusShipmentPurchaseInvoiceLine(Session _currSession, ShipmentSalesInvoice _locShipmentSalesInvoiceXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locShipmentSalesInvoiceXPO != null)
                {
                    XPCollection<ShipmentSalesInvoiceLine> _locShipmentSalesInvoiceLines = new XPCollection<ShipmentSalesInvoiceLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("ShipmentSalesInvoice", _locShipmentSalesInvoiceXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Lock),
                                                            new BinaryOperator("Status", Status.Posted))));

                    if (_locShipmentSalesInvoiceLines != null && _locShipmentSalesInvoiceLines.Count > 0)
                    {

                        foreach (ShipmentSalesInvoiceLine _locShipmentSalesInvoiceLine in _locShipmentSalesInvoiceLines)
                        {
                            if (_locShipmentSalesInvoiceLine.OutstandingAmount == 0)
                            {
                                _locShipmentSalesInvoiceLine.Status = Status.Close;
                                _locShipmentSalesInvoiceLine.ActivationPosting = true;
                                _locShipmentSalesInvoiceLine.StatusDate = now;
                            }
                            else
                            {
                                _locShipmentSalesInvoiceLine.Status = Status.Posted;
                                _locShipmentSalesInvoiceLine.StatusDate = now;
                            }
                            _locShipmentSalesInvoiceLine.Save();
                            _locShipmentSalesInvoiceLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ShipmentSalesInvoice " + ex.ToString());
            }
        }

        private void SetNormalPayLinePosting(Session _currSession, ShipmentSalesInvoice _locShipmentSalesInvoiceXPO)
        {
            try
            {
                if (_locShipmentSalesInvoiceXPO != null)
                {
                    XPCollection<ShipmentSalesInvoiceLine> _locShipmentSalesInvoiceLines = new XPCollection<ShipmentSalesInvoiceLine>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("ShipmentSalesInvoice", _locShipmentSalesInvoiceXPO),
                                                                                     new BinaryOperator("Select", true),
                                                                                     new GroupOperator(GroupOperatorType.Or,
                                                                                     new BinaryOperator("Status", Status.Lock),
                                                                                     new BinaryOperator("Status", Status.Posted),
                                                                                     new BinaryOperator("Status", Status.Close))));

                    if (_locShipmentSalesInvoiceLines != null && _locShipmentSalesInvoiceLines.Count > 0)
                    {
                        foreach (ShipmentSalesInvoiceLine _locShipmentSalesInvoiceLine in _locShipmentSalesInvoiceLines)
                        {
                            if (_locShipmentSalesInvoiceLine.Amount > 0)
                            {
                                _locShipmentSalesInvoiceLine.Select = false;
                                _locShipmentSalesInvoiceLine.Amount = 0;
                                _locShipmentSalesInvoiceLine.Save();
                                _locShipmentSalesInvoiceLine.Session.CommitTransaction();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ShipmentSalesInvoice " + ex.ToString());
            }
        }

        private void SetFinalShipmentPurchaseInvoicePosting(Session _currSession, ShipmentSalesInvoice _locShipmentSalesInvoiceXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locStatusCount = 0;
                int _locPurchInvLineCount = 0;

                if (_locShipmentSalesInvoiceXPO != null)
                {

                    XPCollection<ShipmentSalesInvoiceLine> _locShipmentSalesInvoiceLines = new XPCollection<ShipmentSalesInvoiceLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("ShipmentSalesInvoice", _locShipmentSalesInvoiceXPO)));

                    if (_locShipmentSalesInvoiceLines != null && _locShipmentSalesInvoiceLines.Count() > 0)
                    {
                        _locPurchInvLineCount = _locShipmentSalesInvoiceLines.Count();

                        foreach (ShipmentSalesInvoiceLine _locShipmentSalesInvoiceLine in _locShipmentSalesInvoiceLines)
                        {
                            if (_locShipmentSalesInvoiceLine.Status == Status.Close)
                            {
                                _locStatusCount = _locStatusCount + 1;
                            }
                        }
                    }

                    if (_locStatusCount == _locPurchInvLineCount)
                    {
                        _locShipmentSalesInvoiceXPO.ActivationPosting = true;
                        _locShipmentSalesInvoiceXPO.Status = Status.Close;
                        _locShipmentSalesInvoiceXPO.StatusDate = now;
                        _locShipmentSalesInvoiceXPO.Save();
                        _locShipmentSalesInvoiceXPO.Session.CommitTransaction();
                    }
                    else
                    {
                        _locShipmentSalesInvoiceXPO.Status = Status.Posted;
                        _locShipmentSalesInvoiceXPO.StatusDate = now;
                        _locShipmentSalesInvoiceXPO.Save();
                        _locShipmentSalesInvoiceXPO.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ShipmentSalesInvoice " + ex.ToString());
            }
        }

        #endregion Posting Method

        #region Email

        private string BackgroundBody(Session _currentSession, ShipmentSalesInvoice _locShipmentSalesInvoiceXPO, MailSetupDetail _locMailSetDetail)
        {
            string body = string.Empty;

            if (_locShipmentSalesInvoiceXPO != null)
            {

                ShipmentApprovalLine _locShipmentApprovalLineXPO = _currentSession.FindObject<ShipmentApprovalLine>
                                                   (new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("ShipmentSalesInvoice", _locShipmentSalesInvoiceXPO)));

                string Status = _locShipmentApprovalLineXPO.ApprovalStatus.ToString();

                using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Email/EmailTemplate.html")))
                {
                    body = reader.ReadToEnd();
                }
                body = body.Replace("{Code}", _locShipmentSalesInvoiceXPO.Code);

                #region Level
                if (_locShipmentSalesInvoiceXPO.ActiveApproved1 == true)
                {
                    string Active = "Level 1";
                    body = body.Replace("{Level}", Active);
                }
                else if (_locShipmentSalesInvoiceXPO.ActiveApproved2 == true)
                {
                    string Active = "Level 2";
                    body = body.Replace("{Level}", Active);
                }
                else if (_locShipmentSalesInvoiceXPO.ActiveApproved3 == true)
                {
                    string Active = "Level 3";
                    body = body.Replace("{Level}", Active);
                }
                else
                {
                    string Active = "Not Available";
                    body = body.Replace("{Level}", Active);
                }
                #endregion Level

                body = body.Replace("{Status}", Status);
                body = body.Replace("{Message}", _locMailSetDetail.MessageBody);
            }

            return body;
        }

        protected void SendEmail(Session _currentSession, ShipmentSalesInvoice _locShipmentSalesInvoiceXPO, Status _locStatus, ShipmentApprovalLine _locShipmentApprovalLineXPO2)
        {
            DateTime now = DateTime.Now;
            var User = SecuritySystem.CurrentUserName;
            GlobalFunction _globFunc = new GlobalFunction();
            UserAccess _numLineUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", User));
            string _locMessageBody = null;

            try
            {
                if (_locShipmentSalesInvoiceXPO != null)
                {
                    XPCollection<MailSetupDetail> _locMailSetDetails = new XPCollection<MailSetupDetail>
                                                                       (_currentSession, new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("ObjectList", ObjectList.ShipmentSalesInvoice),
                                                                        new BinaryOperator("UserAccess", _numLineUserAccess),
                                                                        new BinaryOperator("Active", true)));

                    if (_locMailSetDetails != null && _locMailSetDetails.Count > 0)
                    {
                        foreach (MailSetupDetail _locMailSetDetail in _locMailSetDetails)
                        {
                            if (_locMailSetDetail.MessageBody != null)
                            {
                                if (_locShipmentApprovalLineXPO2.EndApproval == true)
                                {
                                    _locMessageBody = this.BackgroundBody(_currentSession, _locShipmentSalesInvoiceXPO, _locMailSetDetail);
                                }
                                else
                                {
                                    _locMessageBody = this.BackgroundBody(_currentSession, _locShipmentSalesInvoiceXPO, _locMailSetDetail);
                                }
                            }

                            _globFunc.SetAndSendMail(_locMailSetDetail.SmtpHost, _locMailSetDetail.SmtpPort,
                            _locMailSetDetail.MailFrom, _locMailSetDetail.MailFromPassword, _locMailSetDetail.MailTo, _locMailSetDetail.MailSubject, _locMessageBody);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ShipmentSalesInvoice " + ex.ToString());
            }
        }

        #endregion Email

        #region Global Method

        private void SuccessMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 4000;
            options.Message = string.Format("{0}", _locMessage);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method
    }
}
