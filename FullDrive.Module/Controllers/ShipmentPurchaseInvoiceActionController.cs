﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;
using System.IO;
using System.Web;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class ShipmentPurchaseInvoiceActionController : ViewController
    {
        private ChoiceActionItem _selectionListviewFilter;
        private ChoiceActionItem _selectionApprovalListviewFilter;
        private ChoiceActionItem _setApprovalLevel;

        public ShipmentPurchaseInvoiceActionController()
        {
            InitializeComponent();
            #region FilterStatus
            ShipmentPurchaseInvoiceListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                ShipmentPurchaseInvoiceListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
            #endregion FilterStatus

            #region FilterApproval
            ShipmentPurchaseInvoiceListviewFilterApprovalSelectionAction.Items.Clear();
            foreach (object _currApprovalFilter in Enum.GetValues(typeof(CustomProcess.ApprovalFilter)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.ApprovalFilter));
                _selectionApprovalListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApprovalFilter), _currApprovalFilter);
                ShipmentPurchaseInvoiceListviewFilterApprovalSelectionAction.Items.Add(_selectionApprovalListviewFilter);
            }
            #endregion FilterApproval
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            TypeOfView = typeof(ObjectView);
            base.OnActivated();
            // Perform various tasks depending on the target View.
            #region Approval

            var _locUser = SecuritySystem.CurrentUserName;
            GlobalFunction _globalFunction = new GlobalFunction();
            Session _currentSession = null;
            if (this.ObjectSpace != null)
            {
                _currentSession = ((XPObjectSpace)this.ObjectSpace).Session;
            }

            if (_currentSession != null)
            {
                UserAccess _locUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", _locUser));

                if (_locUserAccess != null)
                {
                    ShipmentPurchaseInvoiceApprovalAction.Items.Clear();

                    XPCollection<ApplicationSetupDetail> _locAppSetupDetails = new XPCollection<ApplicationSetupDetail>
                                                                                 (_currentSession, new GroupOperator(GroupOperatorType.And,
                                                                                  new BinaryOperator("UserAccess", _locUserAccess),
                                                                                  new BinaryOperator("ObjectList", CustomProcess.ObjectList.ShipmentPurchaseInvoice),
                                                                                  new BinaryOperator("FunctionList", CustomProcess.FunctionList.Approval),
                                                                                  new BinaryOperator("Active", true)));

                    if (_locAppSetupDetails != null && _locAppSetupDetails.Count > 0)
                    {

                        foreach (ApplicationSetupDetail _locAppSetupDetail in _locAppSetupDetails)
                        {
                            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.ApprovalLevel)))
                            {
                                if (_locAppSetupDetail.ApprovalLevel != CustomProcess.ApprovalLevel.None)
                                {
                                    EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.ApprovalLevel));
                                    _setApprovalLevel = new ChoiceActionItem(_ed.GetCaption(_locAppSetupDetail.ApprovalLevel), _locAppSetupDetail.ApprovalLevel);
                                }
                            }
                            ShipmentPurchaseInvoiceApprovalAction.Items.Add(_setApprovalLevel);
                        }
                    }
                }
            }
            #endregion Approval
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void ShipmentPurchaseInvoiceGetSPMAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;


                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        ShipmentPurchaseInvoice _locShipmentPurchaseInvoiceOS = (ShipmentPurchaseInvoice)_objectSpace.GetObject(obj);

                        if (_locShipmentPurchaseInvoiceOS != null)
                        {
                            if (_locShipmentPurchaseInvoiceOS.Code != null)
                            {
                                _currObjectId = _locShipmentPurchaseInvoiceOS.Code;

                                ShipmentPurchaseInvoice _locShipmentPurchaseInvoiceXPO = _currSession.FindObject<ShipmentPurchaseInvoice>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locShipmentPurchaseInvoiceXPO != null)
                                {
                                    if (_locShipmentPurchaseInvoiceXPO.Status == Status.Open || _locShipmentPurchaseInvoiceXPO.Status == Status.Progress)
                                    {
                                        XPCollection<ShipmentPurchaseCollection> _locShipmentPurchaseCollections = new XPCollection<ShipmentPurchaseCollection>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("ShipmentPurchaseInvoice", _locShipmentPurchaseInvoiceXPO),
                                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("Status", Status.Open),
                                                                                        new BinaryOperator("Status", Status.Progress))));

                                        if (_locShipmentPurchaseCollections != null && _locShipmentPurchaseCollections.Count() > 0)
                                        {
                                            foreach (ShipmentPurchaseCollection _locShipmentPurchaseCollection in _locShipmentPurchaseCollections)
                                            {
                                                if (_locShipmentPurchaseCollection.ShipmentPurchaseInvoice != null && _locShipmentPurchaseCollection.ShipmentPurchaseMonitoring != null)
                                                {
                                                    GetShipmentPurchaseMonitoring(_currSession, _locShipmentPurchaseCollection.ShipmentPurchaseMonitoring, _locShipmentPurchaseInvoiceXPO);
                                                    SetOutstandingAmount(_currSession, _locShipmentPurchaseCollection.ShipmentPurchaseMonitoring);
                                                    SetPostedAmount(_currSession, _locShipmentPurchaseCollection.ShipmentPurchaseMonitoring);
                                                    SetProcessCount(_currSession, _locShipmentPurchaseCollection.ShipmentPurchaseMonitoring);
                                                    SetStatusShipmentPurchaseMonitoring(_currSession, _locShipmentPurchaseCollection.ShipmentPurchaseMonitoring);
                                                    SetNormalAmount(_currSession, _locShipmentPurchaseCollection.ShipmentPurchaseMonitoring);
                                                }
                                            }
                                            SuccessMessageShow("ShipmentPurchaseMonitoring Has Been Successfully Getting into Shipment Purchase Invoice");
                                        }
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("ShipmentPurchaseMonitoring Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("ShipmentPurchaseMonitoring Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ShipmentPurchaseInvoice " + ex.ToString());
            }
        }

        private void ShipmentPurchaseInvoiceProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                Status _locStatus = Status.Open;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        ShipmentPurchaseInvoice _locShipmentPurchaseInvoiceOS = (ShipmentPurchaseInvoice)_objectSpace.GetObject(obj);

                        if (_locShipmentPurchaseInvoiceOS != null)
                        {
                            if (_locShipmentPurchaseInvoiceOS.Code != null)
                            {
                                _currObjectId = _locShipmentPurchaseInvoiceOS.Code;

                                ShipmentPurchaseInvoice _locShipmentPurchaseInvoiceXPO = _currSession.FindObject<ShipmentPurchaseInvoice>
                                                                   (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId)));

                                if (_locShipmentPurchaseInvoiceXPO != null)
                                {
                                    if (_locShipmentPurchaseInvoiceXPO.Status == Status.Open || _locShipmentPurchaseInvoiceXPO.Status == Status.Progress || _locShipmentPurchaseInvoiceXPO.Status == Status.Posted)
                                    {
                                        if (_locShipmentPurchaseInvoiceXPO.Status == Status.Open || _locShipmentPurchaseInvoiceXPO.Status == Status.Progress)
                                        {
                                            _locStatus = Status.Progress;

                                        }
                                        else if (_locShipmentPurchaseInvoiceXPO.Status == Status.Posted)
                                        {
                                            _locStatus = Status.Posted;
                                        }

                                        _locShipmentPurchaseInvoiceXPO.Status = _locStatus;
                                        _locShipmentPurchaseInvoiceXPO.StatusDate = now;
                                        _locShipmentPurchaseInvoiceXPO.Save();
                                        _locShipmentPurchaseInvoiceXPO.Session.CommitTransaction();

                                        #region ShipmentPurchaseInvoiceLine
                                        XPCollection<ShipmentPurchaseInvoiceLine> _locShipmentPurchaseInvoiceLines = new XPCollection<ShipmentPurchaseInvoiceLine>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("ShipmentPurchaseInvoice", _locShipmentPurchaseInvoiceXPO)));

                                        if (_locShipmentPurchaseInvoiceLines != null && _locShipmentPurchaseInvoiceLines.Count > 0)
                                        {
                                            foreach (ShipmentPurchaseInvoiceLine _locShipmentPurchaseInvoiceLine in _locShipmentPurchaseInvoiceLines)
                                            {
                                                if (_locShipmentPurchaseInvoiceLine.Status == Status.Open)
                                                {
                                                    _locShipmentPurchaseInvoiceLine.Status = Status.Progress;
                                                    _locShipmentPurchaseInvoiceLine.StatusDate = now;
                                                    _locShipmentPurchaseInvoiceLine.Save();
                                                    _locShipmentPurchaseInvoiceLine.Session.CommitTransaction();
                                                }
                                            }
                                        }
                                        #endregion ShipmentPurchaseInvoiceLine

                                        #region ShipmentPurchaseCollection
                                        {
                                            XPCollection<ShipmentPurchaseCollection> _locShipmentPurchaseCollections = new XPCollection<ShipmentPurchaseCollection>
                                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                            new BinaryOperator("ShipmentPurchaseInvoice", _locShipmentPurchaseInvoiceXPO)));
                                            if (_locShipmentPurchaseCollections != null && _locShipmentPurchaseCollections.Count() > 0)
                                            {
                                                foreach (ShipmentPurchaseCollection _locShipmentPurchaseCollection in _locShipmentPurchaseCollections)
                                                {
                                                    _locShipmentPurchaseCollection.Status = Status.Progress;
                                                    _locShipmentPurchaseCollection.StatusDate = now;
                                                    _locShipmentPurchaseCollection.Save();
                                                    _locShipmentPurchaseCollection.Session.CommitTransaction();
                                                }
                                            }
                                        }
                                        #endregion ShipmentPurchaseCollection

                                        SuccessMessageShow(_locShipmentPurchaseInvoiceXPO.Code + " has been change successfully to Progress ");
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("ShipmentPurchaseInvoice Data Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("ShipmentPurchaseInvoice Data Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ShipmentPurchaseInvoice " + ex.ToString());
            }
        }

        private void ShipmentPurchaseInvoiceGetPayAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                double _locTotalPay = 0;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        ShipmentPurchaseInvoice _locShipmentPurchaseInvoiceOS = (ShipmentPurchaseInvoice)_objectSpace.GetObject(obj);

                        if (_locShipmentPurchaseInvoiceOS != null)
                        {
                            if (_locShipmentPurchaseInvoiceOS.Code != null)
                            {
                                _currObjectId = _locShipmentPurchaseInvoiceOS.Code;

                                ShipmentPurchaseInvoice _locShipmentPurchaseInvoiceXPO = _currSession.FindObject<ShipmentPurchaseInvoice>
                                                                   (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId)));

                                if (_locShipmentPurchaseInvoiceXPO != null)
                                {
                                    
                                    if (_locShipmentPurchaseInvoiceXPO.Status == Status.Progress || _locShipmentPurchaseInvoiceXPO.Status == Status.Posted)
                                    {
                                        ApprovalLine _locApprovalLineXPO = _currSession.FindObject<ApprovalLine>
                                                                           (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("EndApproval", true),
                                                                            new BinaryOperator("ShipmentPurchaseInvoice", _locShipmentPurchaseInvoiceXPO)));

                                        if (_locApprovalLineXPO == null)
                                        {
                                            
                                            XPCollection<ShipmentPurchaseInvoiceLine> _locShipmentPurchaseInvoiceLines = new XPCollection<ShipmentPurchaseInvoiceLine>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("ShipmentPurchaseInvoice", _locShipmentPurchaseInvoiceXPO),
                                                                                                new BinaryOperator("Select", true)));

                                            if (_locShipmentPurchaseInvoiceLines != null && _locShipmentPurchaseInvoiceLines.Count > 0)
                                            {
                                                foreach (ShipmentPurchaseInvoiceLine _locShipmentPurchaseInvoiceLine in _locShipmentPurchaseInvoiceLines)
                                                {
                                                    if (_locShipmentPurchaseInvoiceLine.Status == Status.Progress)
                                                    {
                                                        _locTotalPay = _locTotalPay + _locShipmentPurchaseInvoiceLine.Amount;
                                                        _locShipmentPurchaseInvoiceLine.Status = Status.Lock;
                                                        _locShipmentPurchaseInvoiceLine.StatusDate = now;
                                                        _locShipmentPurchaseInvoiceLine.Save();
                                                        _locShipmentPurchaseInvoiceLine.Session.CommitTransaction();
                                                    }
                                                }
                                            }
                                        }

                                        SuccessMessageShow(_locShipmentPurchaseInvoiceXPO.Code + "Has been change successfully to Get Max Pay");
                                    }



                                    _locShipmentPurchaseInvoiceXPO.MaxAmount = _locTotalPay;
                                    _locShipmentPurchaseInvoiceXPO.Amount = _locTotalPay;
                                    _locShipmentPurchaseInvoiceXPO.Status = Status.Lock;
                                    _locShipmentPurchaseInvoiceXPO.StatusDate = now;
                                    _locShipmentPurchaseInvoiceXPO.Save();
                                    _locShipmentPurchaseInvoiceXPO.Session.CommitTransaction();
                                }
                                else
                                {
                                    ErrorMessageShow("Data ShipmentPurchaseInvoice Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data ShipmentPurchaseInvoice Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ShipmentPurchaseInvoice " + ex.ToString());
            }
        }

        private void ShipmentPurchaseInvoiceApprovalAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                ApplicationSetupDetail _locAppSetDetail = null;
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                var User = SecuritySystem.CurrentUserName;
                Session _currentSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currentSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                UserAccess _locUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", User));

                foreach (Object obj in _objectsToProcess)
                {
                    ShipmentPurchaseInvoice _objInNewObjectSpace = (ShipmentPurchaseInvoice)_objectSpace.GetObject(obj);

                    if (_objInNewObjectSpace != null)
                    {
                        if (_objInNewObjectSpace.Code != null)
                        {
                            _currObjectId = _objInNewObjectSpace.Code;
                        }
                    }

                    if (_currObjectId != null)
                    {
                        ShipmentPurchaseInvoice _locShipmentPurchaseInvoiceXPO = _currentSession.FindObject<ShipmentPurchaseInvoice>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("Code", _currObjectId)));

                        if (_locShipmentPurchaseInvoiceXPO != null)
                        {
                            if (_locShipmentPurchaseInvoiceXPO.Status == Status.Progress)
                            {
                                ShipmentApprovalLine _locShipmentApprovalLine = _currentSession.FindObject<ShipmentApprovalLine>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("ShipmentPurchaseInvoice", _locShipmentPurchaseInvoiceXPO),
                                                                             new BinaryOperator("EndApproval", true)));
                                if (_locShipmentApprovalLine == null)
                                {
                                    #region Approval Level 1
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level1)
                                    {
                                        _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level1, ObjectList.ShipmentPurchaseInvoice);

                                        if (_locAppSetDetail != null)
                                        {
                                            //Buat bs input langsung ke approvalline
                                            ShipmentApprovalLine _locShipmentApprovalLineXPO = _currentSession.FindObject<ShipmentApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("ShipmentPurchaseInvoice", _locShipmentPurchaseInvoiceXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level1)));
                                            if (_locShipmentApprovalLineXPO == null)
                                            {

                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ShipmentApprovalLine _saveDataAL = new ShipmentApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level1,
                                                        EndApproval = true,
                                                        ShipmentPurchaseInvoice = _locShipmentPurchaseInvoiceXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locShipmentPurchaseInvoiceXPO.ActivationPosting = true;
                                                    _locShipmentPurchaseInvoiceXPO.ActiveApproved1 = false;
                                                    _locShipmentPurchaseInvoiceXPO.ActiveApproved2 = false;
                                                    _locShipmentPurchaseInvoiceXPO.ActiveApproved3 = true;
                                                    _locShipmentPurchaseInvoiceXPO.Save();
                                                    _locShipmentPurchaseInvoiceXPO.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ShipmentApprovalLine _saveDataAL = new ShipmentApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level1,
                                                        ShipmentPurchaseInvoice = _locShipmentPurchaseInvoiceXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locShipmentPurchaseInvoiceXPO.ActiveApproved1 = true;
                                                    _locShipmentPurchaseInvoiceXPO.ActiveApproved2 = false;
                                                    _locShipmentPurchaseInvoiceXPO.ActiveApproved3 = false;
                                                    _locShipmentPurchaseInvoiceXPO.Save();
                                                    _locShipmentPurchaseInvoiceXPO.Session.CommitTransaction();
                                                }

                                                //Send Email
                                                ShipmentApprovalLine _locShipmentApprovalLineXPO2 = _currentSession.FindObject<ShipmentApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("ShipmentPurchaseInvoice", _locShipmentPurchaseInvoiceXPO)));
                                                if (_locShipmentApprovalLineXPO2 != null)
                                                {
                                                    SendEmail(_currentSession, _locShipmentPurchaseInvoiceXPO, Status.Progress, _locShipmentApprovalLineXPO2);
                                                }

                                                SuccessMessageShow("ShipmentPurchaseInvoice has successfully Approve");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 1

                                    #region Approval Level 2
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level2)
                                    {
                                        _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level2, ObjectList.ShipmentPurchaseInvoice);

                                        if (_locAppSetDetail != null)
                                        {
                                            ShipmentApprovalLine _locShipmentApprovalLineXPO = _currentSession.FindObject<ShipmentApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("ShipmentPurchaseInvoice", _locShipmentPurchaseInvoiceXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level2)));

                                            if (_locShipmentApprovalLineXPO == null)
                                            {
                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ShipmentApprovalLine _saveDataAL = new ShipmentApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level2,
                                                        EndApproval = true,
                                                        ShipmentPurchaseInvoice = _locShipmentPurchaseInvoiceXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locShipmentPurchaseInvoiceXPO.ActivationPosting = true;
                                                    _locShipmentPurchaseInvoiceXPO.ActiveApproved1 = false;
                                                    _locShipmentPurchaseInvoiceXPO.ActiveApproved2 = false;
                                                    _locShipmentPurchaseInvoiceXPO.ActiveApproved3 = true;
                                                    _locShipmentPurchaseInvoiceXPO.Save();
                                                    _locShipmentPurchaseInvoiceXPO.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ShipmentApprovalLine _saveDataAL = new ShipmentApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level2,
                                                        ShipmentPurchaseInvoice = _locShipmentPurchaseInvoiceXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    SetApprovalLine(_currentSession, _locShipmentPurchaseInvoiceXPO, ApprovalLevel.Level1);

                                                    _locShipmentPurchaseInvoiceXPO.ActiveApproved1 = false;
                                                    _locShipmentPurchaseInvoiceXPO.ActiveApproved2 = true;
                                                    _locShipmentPurchaseInvoiceXPO.ActiveApproved3 = false;
                                                    _locShipmentPurchaseInvoiceXPO.Save();
                                                    _locShipmentPurchaseInvoiceXPO.Session.CommitTransaction();
                                                }

                                                //Send Email
                                                ShipmentApprovalLine _locShipmentApprovalLineXPO2 = _currentSession.FindObject<ShipmentApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("ShipmentPurchaseInvoice", _locShipmentPurchaseInvoiceXPO)));
                                                if (_locShipmentApprovalLineXPO2 != null)
                                                {
                                                    SendEmail(_currentSession, _locShipmentPurchaseInvoiceXPO, Status.Progress, _locShipmentApprovalLineXPO2);
                                                }

                                                SuccessMessageShow("ShipmentPurchaseInvoice has successfully Approve");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 2

                                    #region Approval Level 3
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level3)
                                    {
                                        _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level3, ObjectList.ShipmentPurchaseInvoice);

                                        if (_locAppSetDetail != null)
                                        {
                                            ShipmentApprovalLine _locShipmentApprovalLineXPO = _currentSession.FindObject<ShipmentApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("ShipmentPurchaseInvoice", _locShipmentPurchaseInvoiceXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level3)));

                                            if (_locShipmentApprovalLineXPO == null)
                                            {
                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ShipmentApprovalLine _saveDataAL = new ShipmentApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level3,
                                                        EndApproval = true,
                                                        ShipmentPurchaseInvoice = _locShipmentPurchaseInvoiceXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locShipmentPurchaseInvoiceXPO.ActivationPosting = true;
                                                    _locShipmentPurchaseInvoiceXPO.ActiveApproved1 = false;
                                                    _locShipmentPurchaseInvoiceXPO.ActiveApproved2 = false;
                                                    _locShipmentPurchaseInvoiceXPO.ActiveApproved3 = true;
                                                    _locShipmentPurchaseInvoiceXPO.Save();
                                                    _locShipmentPurchaseInvoiceXPO.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ShipmentApprovalLine _saveDataAL = new ShipmentApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level3,
                                                        ShipmentPurchaseInvoice = _locShipmentPurchaseInvoiceXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    SetApprovalLine(_currentSession, _locShipmentPurchaseInvoiceXPO, ApprovalLevel.Level2);
                                                    SetApprovalLine(_currentSession, _locShipmentPurchaseInvoiceXPO, ApprovalLevel.Level1);

                                                    _locShipmentPurchaseInvoiceXPO.ActiveApproved1 = false;
                                                    _locShipmentPurchaseInvoiceXPO.ActiveApproved2 = false;
                                                    _locShipmentPurchaseInvoiceXPO.ActiveApproved3 = true;
                                                    _locShipmentPurchaseInvoiceXPO.Save();
                                                    _locShipmentPurchaseInvoiceXPO.Session.CommitTransaction();
                                                }

                                                //Send Email
                                                ShipmentApprovalLine _locShipmentApprovalLineXPO2 = _currentSession.FindObject<ShipmentApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("ShipmentPurchaseInvoice", _locShipmentPurchaseInvoiceXPO)));
                                                if (_locShipmentApprovalLineXPO2 != null)
                                                {
                                                    SendEmail(_currentSession, _locShipmentPurchaseInvoiceXPO, Status.Progress, _locShipmentApprovalLineXPO2);
                                                }

                                                SuccessMessageShow("ShipmentPurchaseInvoice has successfully Approve");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 3
                                }

                            }
                            else
                            {
                                ErrorMessageShow("ShipmentPurchaseInvoice Status Not Available");
                            }
                        }
                        else
                        {
                            ErrorMessageShow("ShipmentPurchaseInvoice Not Available");
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ShipmentPurchaseInvoice " + ex.ToString());
            }
        }

        private void ShipmentPurchaseInvoicePostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                //Mengirim informasi ke Payable Transaction --HutangDagang Dan --BankAccount Perusahaan yg aktif default
                //Meng-akumulasi semua hal perhitungan lokal Purchase Invoice termasuk close status

                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        ShipmentPurchaseInvoice _locShipmentPurchaseInvoiceOS = (ShipmentPurchaseInvoice)_objectSpace.GetObject(obj);

                        if (_locShipmentPurchaseInvoiceOS != null)
                        {
                            if (_locShipmentPurchaseInvoiceOS.Code != null)
                            {
                                _currObjectId = _locShipmentPurchaseInvoiceOS.Code;

                                ShipmentPurchaseInvoice _locShipmentPurchaseInvoiceXPO = _currSession.FindObject<ShipmentPurchaseInvoice>
                                                                   (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId)));

                                if (_locShipmentPurchaseInvoiceXPO != null)
                                {
                                    if (_locShipmentPurchaseInvoiceXPO.Status == Status.Lock || _locShipmentPurchaseInvoiceXPO.Status == Status.Posted)
                                    {
                                        ApprovalLine _locApprovalLineXPO = _currSession.FindObject<ApprovalLine>
                                                                           (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("EndApproval", true),
                                                                            new BinaryOperator("ShipmentPurchaseInvoice", _locShipmentPurchaseInvoiceXPO)));

                                        if (_locApprovalLineXPO != null)
                                        {
                                            if (_locShipmentPurchaseInvoiceXPO.Amount > 0)
                                            {
                                                //Berdasarkan Outstanding dari PaymentOutPlan yg mana di hitung berdasarkan jumlah mak Outstanding 
                                                if (_locShipmentPurchaseInvoiceXPO.Amount == GetShipmentPurchaseInvoiceLineTotalAmount(_currSession, _locShipmentPurchaseInvoiceXPO))
                                                {
                                                    SetShipmentPurchaseInvoiceMonitoring(_currSession, _locShipmentPurchaseInvoiceXPO);
                                                    SetInvoiceAPJournal(_currSession, _locShipmentPurchaseInvoiceXPO);
                                                    SetOutstandinAmountPosting(_currSession, _locShipmentPurchaseInvoiceXPO);
                                                    SetPostedAmountPosting(_currSession, _locShipmentPurchaseInvoiceXPO);
                                                    SetProcessCountPosting(_currSession, _locShipmentPurchaseInvoiceXPO);
                                                    SetStatusShipmentPurchaseInvoiceLine(_currSession, _locShipmentPurchaseInvoiceXPO);
                                                    SetNormalPay(_currSession, _locShipmentPurchaseInvoiceXPO);
                                                    SetNormalPayLinePosting(_currSession, _locShipmentPurchaseInvoiceXPO);
                                                    SetFinalShipmentPurchaseInvoicePosting(_currSession, _locShipmentPurchaseInvoiceXPO);
                                                    SuccessMessageShow(_locShipmentPurchaseInvoiceXPO.Code + " has been change successfully to Posted");
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data ShipmentPurchaseInvoice Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data ShipmentPurchaseInvoice Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ShipmentPurchaseInvoice " + ex.ToString());
            }
        }

        private void ShipmentPurchaseInvoiceListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(ShipmentPurchaseInvoice)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ShipmentPurchaseInvoice " + ex.ToString());
            }
        }

        private void ShipmentPurchaseInvoiceListviewFilterApprovalSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(ShipmentPurchaseInvoice)))
                {
                    if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.Approval)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("ActiveApproved3", false, BinaryOperatorType.Equal),
                                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("ActiveApproved1", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", true, BinaryOperatorType.Equal)));
                    }
                    else if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.EndApproval)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new BinaryOperator("ActiveApproved3", true, BinaryOperatorType.Equal);
                    }
                    else if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.AllData)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("ActiveApproved1", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved3", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved1", false, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", false, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved3", false, BinaryOperatorType.Equal));
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ShipmentPurchaseInvoice " + ex.ToString());
            }
        }

        //===================================================== Code Only ====================================================

        #region GetSPM

        private void GetShipmentPurchaseMonitoring(Session _currSession, ShipmentPurchaseMonitoring _locShipmentPurchaseMonitoringXPO, ShipmentPurchaseInvoice _locShipmentPurchaseInvoiceXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _currSignCode = null;
                
                if (_locShipmentPurchaseMonitoringXPO != null && _locShipmentPurchaseInvoiceXPO != null)
                {
                    if(_locShipmentPurchaseMonitoringXPO.Select == true)
                    {
                        _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.ShipmentPurchaseInvoiceLine);

                        if (_currSignCode != null)
                        {
                            ShipmentPurchaseInvoiceLine _saveDataShipmentPurchaseInvoiceLine = new ShipmentPurchaseInvoiceLine(_currSession)
                            {
                                Routing = _locShipmentPurchaseMonitoringXPO.Routing,
                                ShipmentType = _locShipmentPurchaseMonitoringXPO.ShipmentType,
                                TransactionTerm = _locShipmentPurchaseMonitoringXPO.TransactionTerm,
                                TransportType = _locShipmentPurchaseMonitoringXPO.TransportType,
                                ContainerType = _locShipmentPurchaseMonitoringXPO.ContainerType,
                                PriceType = _locShipmentPurchaseMonitoringXPO.PriceType,
                                DeliveryType = _locShipmentPurchaseMonitoringXPO.DeliveryType,
                                WorkOrder = _locShipmentPurchaseMonitoringXPO.WorkOrder,
                                Carrier = _locShipmentPurchaseMonitoringXPO.Carrier,
                                VehicleMode = _locShipmentPurchaseMonitoringXPO.VehicleMode,
                                Vehicle = _locShipmentPurchaseMonitoringXPO.Vehicle,
                                VehicleType = _locShipmentPurchaseMonitoringXPO.VehicleType,
                                ActualQuantity = _locShipmentPurchaseMonitoringXPO.ActualQuantity,
                                UOM = _locShipmentPurchaseMonitoringXPO.UOM,
                                CountryFrom = _locShipmentPurchaseMonitoringXPO.CountryFrom,
                                CityFrom = _locShipmentPurchaseMonitoringXPO.CityFrom,
                                AreaFrom = _locShipmentPurchaseMonitoringXPO.AreaFrom,
                                LocationFrom = _locShipmentPurchaseMonitoringXPO.LocationFrom,
                                TransportLocationFrom = _locShipmentPurchaseMonitoringXPO.TransportLocationFrom,
                                CountryTo = _locShipmentPurchaseMonitoringXPO.CountryTo,
                                CityTo = _locShipmentPurchaseMonitoringXPO.CityTo,
                                AreaTo = _locShipmentPurchaseMonitoringXPO.AreaTo,
                                LocationTo = _locShipmentPurchaseMonitoringXPO.LocationTo,
                                TransportLocationTo = _locShipmentPurchaseMonitoringXPO.TransportLocationTo,
                                MaxAmount = _locShipmentPurchaseMonitoringXPO.Amount,
                                Amount = _locShipmentPurchaseMonitoringXPO.Amount,
                                ShipmentPurchaseMonitoring = _locShipmentPurchaseMonitoringXPO,
                                ShipmentPurchaseInvoice = _locShipmentPurchaseInvoiceXPO
                            };
                            _saveDataShipmentPurchaseInvoiceLine.Save();
                            _saveDataShipmentPurchaseInvoiceLine.Session.CommitTransaction();
                        }

                        ShipmentPurchaseInvoiceLine _locShipmentPurchaseInvoiceLine = _currSession.FindObject<ShipmentPurchaseInvoiceLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("SignCode", _currSignCode)));

                        if (_locShipmentPurchaseInvoiceLine != null)
                        {
                            _locShipmentPurchaseMonitoringXPO.ShipmentPurchaseInvoice = _locShipmentPurchaseInvoiceXPO;
                            _locShipmentPurchaseMonitoringXPO.ShipmentPurchaseInvoiceLine = _locShipmentPurchaseInvoiceLine;
                            _locShipmentPurchaseMonitoringXPO.Save();
                            _locShipmentPurchaseMonitoringXPO.Session.CommitTransaction();
                        }
                    }  
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = ShipmentPurchaseInvoice ", ex.ToString());
            }
        }

        private void SetOutstandingAmount(Session _currSession, ShipmentPurchaseMonitoring _locShipmentPurchaseMonitoringXPO)
        {
            try
            {
                double _locOutstandingAmount = 0;
                if(_locShipmentPurchaseMonitoringXPO != null )
                {
                    if(_locShipmentPurchaseMonitoringXPO.Select == true)
                    {
                        #region PostedCount == 0
                        if (_locShipmentPurchaseMonitoringXPO.PostedCount == 0)
                        {
                            #region MaxAmount
                            if (_locShipmentPurchaseMonitoringXPO.MaxAmount > 0)
                            {
                                if (_locShipmentPurchaseMonitoringXPO.Amount > 0 && _locShipmentPurchaseMonitoringXPO.Amount <= _locShipmentPurchaseMonitoringXPO.MaxAmount)
                                {
                                    _locOutstandingAmount = _locShipmentPurchaseMonitoringXPO.MaxAmount - _locShipmentPurchaseMonitoringXPO.Amount;
                                }
                            }
                            #endregion MaxAmount
                            #region NonMaxAmount
                            else
                            {
                                _locOutstandingAmount = 0;
                            }
                            #endregion NonMaxAmount

                        }
                        #endregion PostedCount == 0

                        #region PostedCount > 0
                        if (_locShipmentPurchaseMonitoringXPO.PostedCount > 0)
                        {
                            if (_locShipmentPurchaseMonitoringXPO.OutstandingAmount > 0)
                            {
                                _locOutstandingAmount = _locShipmentPurchaseMonitoringXPO.OutstandingAmount - _locShipmentPurchaseMonitoringXPO.Amount;
                            }
                        }
                        #endregion PostedCount > 0

                        _locShipmentPurchaseMonitoringXPO.OutstandingAmount = _locOutstandingAmount;
                        _locShipmentPurchaseMonitoringXPO.Save();
                        _locShipmentPurchaseMonitoringXPO.Session.CommitTransaction();
                    }
                    
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ShipmentPurchaseInvoice " + ex.ToString());
            }
        }

        private void SetPostedAmount(Session _currSession, ShipmentPurchaseMonitoring _locShipmentPurchaseMonitoringXPO)
        {
            try
            {
                double _locPostedAmount = 0;
                if (_locShipmentPurchaseMonitoringXPO != null)
                {
                    if (_locShipmentPurchaseMonitoringXPO.Select == true)
                    {
                        #region PostedCount == 0
                        if (_locShipmentPurchaseMonitoringXPO.PostedCount == 0)
                        {
                            #region MaxAmount
                            if (_locShipmentPurchaseMonitoringXPO.MaxAmount > 0)
                            {
                                if (_locShipmentPurchaseMonitoringXPO.Amount > 0 && _locShipmentPurchaseMonitoringXPO.Amount <= _locShipmentPurchaseMonitoringXPO.MaxAmount)
                                {
                                    _locPostedAmount = _locShipmentPurchaseMonitoringXPO.Amount;
                                }
                            }
                            #endregion MaxAmount
                            #region NonMaxAmount
                            else
                            {
                                if (_locShipmentPurchaseMonitoringXPO.Amount > 0)
                                {
                                    _locPostedAmount = _locShipmentPurchaseMonitoringXPO.Amount;
                                }
                            }
                            #endregion NonMaxAmount
                        }
                        #endregion PostedCount == 0

                        #region PostedCount > 0
                        if (_locShipmentPurchaseMonitoringXPO.PostedCount > 0)
                        {
                            if (_locShipmentPurchaseMonitoringXPO.PostedAmount > 0)
                            {
                                _locPostedAmount = _locShipmentPurchaseMonitoringXPO.PostedAmount + _locShipmentPurchaseMonitoringXPO.Amount;
                            }
                        }
                        #endregion PostedCount > 0

                        _locShipmentPurchaseMonitoringXPO.PostedAmount = _locPostedAmount;
                        _locShipmentPurchaseMonitoringXPO.Save();
                        _locShipmentPurchaseMonitoringXPO.Session.CommitTransaction();
                    }
                    
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ShipmentPurchaseInvoice " + ex.ToString());
            }
        }

        private void SetProcessCount(Session _currSession, ShipmentPurchaseMonitoring _locShipmentPurchaseMonitoringXPO )
        {
            try
            {
                if (_locShipmentPurchaseMonitoringXPO != null)
                {
                    if(_locShipmentPurchaseMonitoringXPO.Select == true)
                    {
                        _locShipmentPurchaseMonitoringXPO.PostedCount = _locShipmentPurchaseMonitoringXPO.PostedCount + 1;
                        _locShipmentPurchaseMonitoringXPO.Save();
                        _locShipmentPurchaseMonitoringXPO.Session.CommitTransaction();
                    }   
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ShipmentPurchaseInvoice " + ex.ToString());
            }
        }

        private void SetStatusShipmentPurchaseMonitoring (Session _currSession, ShipmentPurchaseMonitoring _locShipmentPurchaseMonitoringXPO )
        {
            try
            {
                DateTime now = DateTime.Now;

                if (_locShipmentPurchaseMonitoringXPO != null)
                {
                    if(_locShipmentPurchaseMonitoringXPO.Select == true)
                    {
                        if (_locShipmentPurchaseMonitoringXPO.Status == Status.Progress || _locShipmentPurchaseMonitoringXPO.Status == Status.Posted)
                        {
                            if (_locShipmentPurchaseMonitoringXPO.OutstandingAmount == 0)
                            {
                                _locShipmentPurchaseMonitoringXPO.Status = Status.Close;
                                _locShipmentPurchaseMonitoringXPO.ActivationPosting = true;
                                _locShipmentPurchaseMonitoringXPO.StatusDate = now;
                            }
                            else
                            {
                                _locShipmentPurchaseMonitoringXPO.Status = Status.Posted;
                                _locShipmentPurchaseMonitoringXPO.StatusDate = now;
                            }
                            _locShipmentPurchaseMonitoringXPO.Save();
                            _locShipmentPurchaseMonitoringXPO.Session.CommitTransaction();
                        }
                    }  
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ShipmentPurchaseInvoice " + ex.ToString());
            }
        }

        private void SetNormalAmount(Session _currSession, ShipmentPurchaseMonitoring _locShipmentPurchaseMonitoringXPO )
        {
            try
            {
                if (_locShipmentPurchaseMonitoringXPO != null)
                {
                    if(_locShipmentPurchaseMonitoringXPO.Select == true)
                    {
                        if (_locShipmentPurchaseMonitoringXPO.Status == Status.Progress || _locShipmentPurchaseMonitoringXPO.Status == Status.Posted || _locShipmentPurchaseMonitoringXPO.Status == Status.Close)
                        {
                            if (_locShipmentPurchaseMonitoringXPO.Amount > 0)
                            {
                                _locShipmentPurchaseMonitoringXPO.Select = false;
                                _locShipmentPurchaseMonitoringXPO.ShipmentPurchaseInvoice = null;
                                _locShipmentPurchaseMonitoringXPO.ShipmentPurchaseInvoiceLine = null;
                                _locShipmentPurchaseMonitoringXPO.Amount = 0;
                                _locShipmentPurchaseMonitoringXPO.Save();
                                _locShipmentPurchaseMonitoringXPO.Session.CommitTransaction();
                            }

                        }
                    }  
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ShipmentPurchaseInvoice " + ex.ToString());
            }
        }

        #endregion GetSPM

        #region Approval Process

        private void SetApprovalLine(Session _currentSession, ShipmentPurchaseInvoice _locShipmentPurchaseInvoiceXPO, ApprovalLevel _locApprovalLevel)
        {
            try
            {
                DateTime now = DateTime.Now;

                ShipmentApprovalLine _locShipmentApprovalLineXPO = _currentSession.FindObject<ShipmentApprovalLine>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("ShipmentPurchaseInvoice", _locShipmentPurchaseInvoiceXPO),
                                                                         new BinaryOperator("ApprovalLevel", _locApprovalLevel)));

                if (_locShipmentApprovalLineXPO == null)
                {
                    ShipmentApprovalLine _saveDataAL2 = new ShipmentApprovalLine(_currentSession)
                    {
                        ApprovalDate = now,
                        ApprovalStatus = Status.Approved,
                        ApprovalLevel = _locApprovalLevel,
                        ShipmentPurchaseInvoice = _locShipmentPurchaseInvoiceXPO,
                    };
                    _saveDataAL2.Save();
                    _saveDataAL2.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ShipmentPurchaseInvoice " + ex.ToString());
            }
        }

        #endregion Approval Process

        #region Posting Method

        private double GetShipmentPurchaseInvoiceLineTotalAmount(Session _currSession, ShipmentPurchaseInvoice _locShipmentPurchaseInvoiceXPO)
        {
            double _return = 0;
            try
            {
                if (_locShipmentPurchaseInvoiceXPO != null)
                {
                    XPCollection<ShipmentPurchaseInvoiceLine> _locShipmentPurchaseInvoiceLines = new XPCollection<ShipmentPurchaseInvoiceLine>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("ShipmentPurchaseInvoice", _locShipmentPurchaseInvoiceXPO),
                                                                                                new BinaryOperator("Select", true),
                                                                                                new GroupOperator(GroupOperatorType.Or,
                                                                                                new BinaryOperator("Status", Status.Lock),
                                                                                                new BinaryOperator("Status", Status.Posted))));

                    if (_locShipmentPurchaseInvoiceLines != null && _locShipmentPurchaseInvoiceLines.Count > 0)
                    {
                        foreach (ShipmentPurchaseInvoiceLine _locShipmentPurchaseInvoiceLine in _locShipmentPurchaseInvoiceLines)
                        {
                            _return = _return + _locShipmentPurchaseInvoiceLine.Amount;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = ShipmentPurchaseInvoice" + ex.ToString());
            }
            return _return;
        }

        private void SetShipmentPurchaseInvoiceMonitoring(Session _currSession, ShipmentPurchaseInvoice _locShipmentPurchaseInvoiceXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _currSignCode = null;

                if (_locShipmentPurchaseInvoiceXPO != null)
                {
                    if (_locShipmentPurchaseInvoiceXPO.Status == Status.Lock || _locShipmentPurchaseInvoiceXPO.Status == Status.Posted)
                    {
                        XPCollection<ShipmentPurchaseInvoiceLine> _locShipmentPurchaseInvoiceLines = new XPCollection<ShipmentPurchaseInvoiceLine>
                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("ShipmentPurchaseInvoice", _locShipmentPurchaseInvoiceXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Lock),
                                                            new BinaryOperator("Status", Status.Posted))));

                        if (_locShipmentPurchaseInvoiceLines != null && _locShipmentPurchaseInvoiceLines.Count() > 0)
                        {
                            foreach (ShipmentPurchaseInvoiceLine _locShipmentPurchaseInvoiceLine in _locShipmentPurchaseInvoiceLines)
                            {
                                if (_locShipmentPurchaseInvoiceLine.Amount > 0)
                                {
                                    ShipmentPurchaseInvoiceMonitoring _saveDataShipmentPurchaseInvoiceMonitoring = new ShipmentPurchaseInvoiceMonitoring(_currSession)
                                    {
                                        ShipmentPurchaseInvoice = _locShipmentPurchaseInvoiceXPO,
                                        ShipmentPurchaseInvoiceLine = _locShipmentPurchaseInvoiceLine,
                                        Routing = _locShipmentPurchaseInvoiceLine.Routing,
                                        ShipmentType = _locShipmentPurchaseInvoiceLine.ShipmentType,
                                        TransactionTerm = _locShipmentPurchaseInvoiceLine.TransactionTerm,
                                        TransportType = _locShipmentPurchaseInvoiceLine.TransportType,
                                        ContainerType = _locShipmentPurchaseInvoiceLine.ContainerType,
                                        PriceType = _locShipmentPurchaseInvoiceLine.PriceType,
                                        DeliveryType = _locShipmentPurchaseInvoiceLine.DeliveryType,
                                        WorkOrder = _locShipmentPurchaseInvoiceLine.WorkOrder,
                                        Carrier = _locShipmentPurchaseInvoiceLine.Carrier,
                                        VehicleMode = _locShipmentPurchaseInvoiceLine.VehicleMode,
                                        Vehicle = _locShipmentPurchaseInvoiceLine.Vehicle,
                                        VehicleType = _locShipmentPurchaseInvoiceLine.VehicleType,
                                        ActualQuantity = _locShipmentPurchaseInvoiceLine.ActualQuantity,
                                        UOM = _locShipmentPurchaseInvoiceLine.UOM,
                                        CountryFrom = _locShipmentPurchaseInvoiceLine.CountryFrom,
                                        CityFrom = _locShipmentPurchaseInvoiceLine.CityFrom,
                                        AreaFrom = _locShipmentPurchaseInvoiceLine.AreaFrom,
                                        LocationFrom = _locShipmentPurchaseInvoiceLine.LocationFrom,
                                        TransportLocationFrom = _locShipmentPurchaseInvoiceLine.TransportLocationFrom,
                                        CountryTo = _locShipmentPurchaseInvoiceLine.CountryTo,
                                        CityTo = _locShipmentPurchaseInvoiceLine.CityTo,
                                        AreaTo = _locShipmentPurchaseInvoiceLine.AreaTo,
                                        LocationTo = _locShipmentPurchaseInvoiceLine.LocationTo,
                                        TransportLocationTo = _locShipmentPurchaseInvoiceLine.TransportLocationTo,
                                        MaxAmount = _locShipmentPurchaseInvoiceLine.Amount,
                                        Amount = _locShipmentPurchaseInvoiceLine.Amount,

                                    };
                                    _saveDataShipmentPurchaseInvoiceMonitoring.Save();
                                    _saveDataShipmentPurchaseInvoiceMonitoring.Session.CommitTransaction();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = ShipmentPurchaseInvoice ", ex.ToString());
            }
        }

        private void SetInvoiceAPJournal(Session _currSession, ShipmentPurchaseInvoice _locShipmentPurchaseInvoiceXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                double _locPayTUAmount = 0;

                if (_locShipmentPurchaseInvoiceXPO != null)
                {
                    #region CreateInvoiceAPJournal

                    XPCollection<ShipmentPurchaseInvoiceLine> _locShipmentPurchaseInvoiceLines = new XPCollection<ShipmentPurchaseInvoiceLine>
                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("ShipmentPurchaseInvoice", _locShipmentPurchaseInvoiceXPO),
                                                                                new BinaryOperator("Select", true),
                                                                                new GroupOperator(GroupOperatorType.Or,
                                                                                new BinaryOperator("Status", Status.Lock),
                                                                                new BinaryOperator("Status", Status.Posted))));

                    if (_locShipmentPurchaseInvoiceLines != null && _locShipmentPurchaseInvoiceLines.Count() > 0)
                    {
                        foreach (ShipmentPurchaseInvoiceLine _locShipmentPurchaseInvoiceLine in _locShipmentPurchaseInvoiceLines)
                        {
                            if (_locShipmentPurchaseInvoiceLine.ShipmentPurchaseInvoice != null)
                            {
                                #region CreateNormalInvoiceAPJournal

                                _locPayTUAmount = _locShipmentPurchaseInvoiceLine.Amount;

                                #region JournalMapBusinessPartnerAccountGroup
                                if (_locShipmentPurchaseInvoiceXPO.BuyFromBusinessPartner != null)
                                {
                                    if (_locShipmentPurchaseInvoiceXPO.BuyFromBusinessPartner.BusinessPartnerAccountGroup != null)
                                    {
                                        //Settingan Business Partner lansung pake 2 account debit dan credit
                                        XPCollection<JournalMap> _locJournalMapByBusinessPartners = new XPCollection<JournalMap>
                                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                     new BinaryOperator("BusinessPartnerAccountGroup", _locShipmentPurchaseInvoiceXPO.BuyFromBusinessPartner.BusinessPartnerAccountGroup)));

                                        if (_locJournalMapByBusinessPartners != null && _locJournalMapByBusinessPartners.Count() > 0)
                                        {
                                            foreach (JournalMap _locJournalMapByBusinessPartner in _locJournalMapByBusinessPartners)
                                            {
                                                XPCollection<JournalMapLine> _locJournalMapLineByBusinessPartners = new XPCollection<JournalMapLine>
                                                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                     new BinaryOperator("JournalMap", _locJournalMapByBusinessPartner)));

                                                if (_locJournalMapLineByBusinessPartners != null && _locJournalMapLineByBusinessPartners.Count() > 0)
                                                {
                                                    foreach (JournalMapLine _locJournalMapLineByBusinessPartner in _locJournalMapLineByBusinessPartners)
                                                    {
                                                        AccountMap _locAccountMapByBusinessPartner = _currSession.FindObject<AccountMap>
                                                                                                     (new GroupOperator(GroupOperatorType.And,
                                                                                                      new BinaryOperator("Code", _locJournalMapLineByBusinessPartner.AccountMap.Code),
                                                                                                      new BinaryOperator("PostingType", PostingType.Purchase),
                                                                                                      new BinaryOperator("OrderType", OrderType.Service),
                                                                                                      new BinaryOperator("PostingMethod", PostingMethod.InvoiceAP),
                                                                                                      new BinaryOperator("PostingMethodType", PostingMethodType.Normal)));

                                                        if (_locAccountMapByBusinessPartner != null)
                                                        {
                                                            XPCollection<AccountMapLine> _locAccountMapLineByBusinessPartners = new XPCollection<AccountMapLine>
                                                                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                                 new BinaryOperator("AccountMap", _locAccountMapByBusinessPartner),
                                                                                                                                 new BinaryOperator("Active", true)));

                                                            if (_locAccountMapLineByBusinessPartners != null && _locAccountMapLineByBusinessPartners.Count() > 0)
                                                            {
                                                                double _locTotalAmountDebit = 0;
                                                                double _locTotalAmountCredit = 0;
                                                                double _locTotalBalance = 0;

                                                                foreach (AccountMapLine _locAccountMapLine in _locAccountMapLineByBusinessPartners)
                                                                {
                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                    {
                                                                        _locTotalAmountDebit = _locPayTUAmount;
                                                                    }
                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                    {
                                                                        _locTotalAmountCredit = _locPayTUAmount;
                                                                    }

                                                                    GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                    {
                                                                        PostingDate = now,
                                                                        PostingType = PostingType.Purchase,
                                                                        OrderType = OrderType.Service,
                                                                        PostingMethod = PostingMethod.InvoiceAP,
                                                                        PostingMethodType = PostingMethodType.Normal,
                                                                        Account = _locAccountMapLine.Account,
                                                                        Debit = _locTotalAmountDebit,
                                                                        Credit = _locTotalAmountCredit,
                                                                        ShipmentPurchaseInvoice = _locShipmentPurchaseInvoiceXPO,
                                                                        Company = _locShipmentPurchaseInvoiceXPO.Company,
                                                                    };
                                                                    _saveGeneralJournal.Save();
                                                                    _saveGeneralJournal.Session.CommitTransaction();

                                                                    if (_locAccountMapLine.Account.Code != null)
                                                                    {
                                                                        ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                                 (new GroupOperator(GroupOperatorType.And,
                                                                                                  new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                                                                        if (_locCOA != null)
                                                                        {
                                                                            if (_locCOA.BalanceType == BalanceType.Change)
                                                                            {
                                                                                if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                                {
                                                                                    if (_locTotalAmountDebit > 0)
                                                                                    {
                                                                                        _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                                    }
                                                                                    if (_locTotalAmountCredit > 0)
                                                                                    {
                                                                                        _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                                    }
                                                                                }
                                                                                if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                                {
                                                                                    if (_locTotalAmountDebit > 0)
                                                                                    {
                                                                                        _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                                    }
                                                                                    if (_locTotalAmountCredit > 0)
                                                                                    {
                                                                                        _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                                    }
                                                                                }
                                                                            }

                                                                            _locCOA.Balance = _locTotalBalance;
                                                                            _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                            _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                            _locCOA.Save();
                                                                            _locCOA.Session.CommitTransaction();
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                #endregion JournalMapBusinessPartnerAccountGroup

                                #endregion CreateNormalInvoiceAPJournal   
                            }
                        }
                    }

                    #endregion CreateInvoiceARJournal
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = ShipmentPurchaseInvoice ", ex.ToString());
            }
        }

        private void SetNormalPay(Session _currSession, ShipmentPurchaseInvoice _locShipmentPurchaseInvoiceXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;

                if (_locShipmentPurchaseInvoiceXPO != null)
                {
                    _locShipmentPurchaseInvoiceXPO.Amount = 0;
                    _locShipmentPurchaseInvoiceXPO.Save();
                    _locShipmentPurchaseInvoiceXPO.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = ShipmentPurchaseInvoice ", ex.ToString());
            }
        }

        private void SetOutstandinAmountPosting(Session _currSession, ShipmentPurchaseInvoice _locShipmentPurchaseInvoiceXPO)
        {
            try
            {
                if (_locShipmentPurchaseInvoiceXPO != null)
                {
                    XPCollection<ShipmentPurchaseInvoiceLine> _locShipmentPurchaseInvoiceLines = new XPCollection<ShipmentPurchaseInvoiceLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("ShipmentPurchaseInvoice", _locShipmentPurchaseInvoiceXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Lock),
                                                            new BinaryOperator("Status", Status.Posted))));

                    if (_locShipmentPurchaseInvoiceLines != null && _locShipmentPurchaseInvoiceLines.Count > 0)
                    {
                        double _locOutstandingAmount = 0;

                        foreach (ShipmentPurchaseInvoiceLine _locShipmentPurchaseInvoiceLine in _locShipmentPurchaseInvoiceLines)
                        {
                            #region ProcessCount=0
                            if (_locShipmentPurchaseInvoiceLine.PostedCount == 0)
                            {
                                #region MaxQuantity
                                if (_locShipmentPurchaseInvoiceLine.MaxAmount > 0)
                                {
                                    if (_locShipmentPurchaseInvoiceLine.Amount > 0 && _locShipmentPurchaseInvoiceLine.Amount <= _locShipmentPurchaseInvoiceLine.MaxAmount)
                                    {
                                        _locOutstandingAmount = _locShipmentPurchaseInvoiceLine.MaxAmount - _locShipmentPurchaseInvoiceLine.Amount;
                                    }

                                }
                                #endregion MaxQuantity

                                #region NonMaxQuantity
                                else
                                {
                                    if (_locShipmentPurchaseInvoiceLine.Amount > 0)
                                    {
                                        _locOutstandingAmount = 0;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion ProcessCount=0

                            #region ProcessCount>0
                            if (_locShipmentPurchaseInvoiceLine.PostedCount > 0)
                            {
                                if (_locShipmentPurchaseInvoiceLine.OutstandingAmount > 0)
                                {
                                    _locOutstandingAmount = _locShipmentPurchaseInvoiceLine.OutstandingAmount - _locShipmentPurchaseInvoiceLine.Amount;
                                }
                            }
                            #endregion ProcessCount>0

                            _locShipmentPurchaseInvoiceLine.OutstandingAmount = _locOutstandingAmount;
                            _locShipmentPurchaseInvoiceLine.Save();
                            _locShipmentPurchaseInvoiceLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ShipmentPurchaseInvoice " + ex.ToString());
            }
        }

        private void SetPostedAmountPosting(Session _currSession, ShipmentPurchaseInvoice _locShipmentPurchaseInvoiceXPO)
        {
            try
            {
                if (_locShipmentPurchaseInvoiceXPO != null)
                {
                    XPCollection<ShipmentPurchaseInvoiceLine> _locShipmentPurchaseInvoiceLines = new XPCollection<ShipmentPurchaseInvoiceLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("ShipmentPurchaseInvoice", _locShipmentPurchaseInvoiceXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Lock),
                                                            new BinaryOperator("Status", Status.Posted))));

                    if (_locShipmentPurchaseInvoiceLines != null && _locShipmentPurchaseInvoiceLines.Count > 0)
                    {
                        double _locPostedAmount = 0;

                        foreach (ShipmentPurchaseInvoiceLine _locShipmentPurchaseInvoiceLine in _locShipmentPurchaseInvoiceLines)
                        {
                            #region ProcessCount=0
                            if (_locShipmentPurchaseInvoiceLine.PostedCount == 0)
                            {
                                #region MaxQuantity
                                if (_locShipmentPurchaseInvoiceLine.MaxAmount > 0)
                                {
                                    if (_locShipmentPurchaseInvoiceLine.Amount > 0 && _locShipmentPurchaseInvoiceLine.Amount <= _locShipmentPurchaseInvoiceLine.MaxAmount)
                                    {
                                        _locPostedAmount = _locShipmentPurchaseInvoiceLine.Amount;
                                    }
                                }
                                #endregion MaxQuantity

                                #region NonMaxQuantity
                                else
                                {
                                    if (_locShipmentPurchaseInvoiceLine.Amount > 0)
                                    {
                                        _locPostedAmount = _locShipmentPurchaseInvoiceLine.Amount;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion ProcessCount=0

                            #region ProcessCount>0
                            if (_locShipmentPurchaseInvoiceLine.PostedCount > 0)
                            {
                                if (_locShipmentPurchaseInvoiceLine.PostedAmount > 0)
                                {
                                    _locPostedAmount = _locShipmentPurchaseInvoiceLine.PostedAmount + _locShipmentPurchaseInvoiceLine.Amount;
                                }
                            }
                            #endregion ProcessCount>0

                            _locShipmentPurchaseInvoiceLine.PostedAmount = _locPostedAmount;
                            _locShipmentPurchaseInvoiceLine.Save();
                            _locShipmentPurchaseInvoiceLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ShipmentPurchaseInvoice " + ex.ToString());
            }
        }

        private void SetProcessCountPosting(Session _currSession, ShipmentPurchaseInvoice _locShipmentPurchaseInvoiceXPO)
        {
            try
            {
                if (_locShipmentPurchaseInvoiceXPO != null)
                {
                    XPCollection<ShipmentPurchaseInvoiceLine> _locShipmentPurchaseInvoiceLines = new XPCollection<ShipmentPurchaseInvoiceLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("ShipmentPurchaseInvoice", _locShipmentPurchaseInvoiceXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Lock),
                                                            new BinaryOperator("Status", Status.Posted))));

                    if (_locShipmentPurchaseInvoiceLines != null && _locShipmentPurchaseInvoiceLines.Count > 0)
                    {

                        foreach (ShipmentPurchaseInvoiceLine _locShipmentPurchaseInvoiceLine in _locShipmentPurchaseInvoiceLines)
                        {
                            _locShipmentPurchaseInvoiceLine.PostedCount = _locShipmentPurchaseInvoiceLine.PostedCount + 1;
                            _locShipmentPurchaseInvoiceLine.Save();
                            _locShipmentPurchaseInvoiceLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ShipmentPurchaseInvoice " + ex.ToString());
            }
        }

        private void SetStatusShipmentPurchaseInvoiceLine(Session _currSession, ShipmentPurchaseInvoice _locShipmentPurchaseInvoiceXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locShipmentPurchaseInvoiceXPO != null)
                {
                    XPCollection<ShipmentPurchaseInvoiceLine> _locShipmentPurchaseInvoiceLines = new XPCollection<ShipmentPurchaseInvoiceLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("ShipmentPurchaseInvoice", _locShipmentPurchaseInvoiceXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Lock),
                                                            new BinaryOperator("Status", Status.Posted))));

                    if (_locShipmentPurchaseInvoiceLines != null && _locShipmentPurchaseInvoiceLines.Count > 0)
                    {

                        foreach (ShipmentPurchaseInvoiceLine _locShipmentPurchaseInvoiceLine in _locShipmentPurchaseInvoiceLines)
                        {
                            if (_locShipmentPurchaseInvoiceLine.OutstandingAmount == 0)
                            {
                                _locShipmentPurchaseInvoiceLine.Status = Status.Close;
                                _locShipmentPurchaseInvoiceLine.ActivationPosting = true;
                                _locShipmentPurchaseInvoiceLine.StatusDate = now;
                            }
                            else
                            {
                                _locShipmentPurchaseInvoiceLine.Status = Status.Posted;
                                _locShipmentPurchaseInvoiceLine.StatusDate = now;
                            }
                            _locShipmentPurchaseInvoiceLine.Save();
                            _locShipmentPurchaseInvoiceLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ShipmentPurchaseInvoice " + ex.ToString());
            }
        }

        private void SetNormalPayLinePosting(Session _currSession, ShipmentPurchaseInvoice _locShipmentPurchaseInvoiceXPO)
        {
            try
            {
                if (_locShipmentPurchaseInvoiceXPO != null)
                {
                    XPCollection<ShipmentPurchaseInvoiceLine> _locShipmentPurchaseInvoiceLines = new XPCollection<ShipmentPurchaseInvoiceLine>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("ShipmentPurchaseInvoice", _locShipmentPurchaseInvoiceXPO),
                                                                                     new BinaryOperator("Select", true),
                                                                                     new GroupOperator(GroupOperatorType.Or,
                                                                                     new BinaryOperator("Status", Status.Lock),
                                                                                     new BinaryOperator("Status", Status.Posted),
                                                                                     new BinaryOperator("Status", Status.Close))));

                    if (_locShipmentPurchaseInvoiceLines != null && _locShipmentPurchaseInvoiceLines.Count > 0)
                    {
                        foreach (ShipmentPurchaseInvoiceLine _locShipmentPurchaseInvoiceLine in _locShipmentPurchaseInvoiceLines)
                        {
                            if (_locShipmentPurchaseInvoiceLine.Amount > 0)
                            {
                                _locShipmentPurchaseInvoiceLine.Select = false;
                                _locShipmentPurchaseInvoiceLine.Amount = 0;
                                _locShipmentPurchaseInvoiceLine.Save();
                                _locShipmentPurchaseInvoiceLine.Session.CommitTransaction();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ShipmentPurchaseInvoice " + ex.ToString());
            }
        }

        private void SetFinalShipmentPurchaseInvoicePosting(Session _currSession, ShipmentPurchaseInvoice _locShipmentPurchaseInvoiceXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locStatusCount = 0;
                int _locPurchInvLineCount = 0;

                if (_locShipmentPurchaseInvoiceXPO != null)
                {

                    XPCollection<ShipmentPurchaseInvoiceLine> _locShipmentPurchaseInvoiceLines = new XPCollection<ShipmentPurchaseInvoiceLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("ShipmentPurchaseInvoice", _locShipmentPurchaseInvoiceXPO)));

                    if (_locShipmentPurchaseInvoiceLines != null && _locShipmentPurchaseInvoiceLines.Count() > 0)
                    {
                        _locPurchInvLineCount = _locShipmentPurchaseInvoiceLines.Count();

                        foreach (ShipmentPurchaseInvoiceLine _locShipmentPurchaseInvoiceLine in _locShipmentPurchaseInvoiceLines)
                        {
                            if (_locShipmentPurchaseInvoiceLine.Status == Status.Close)
                            {
                                _locStatusCount = _locStatusCount + 1;
                            }
                        }
                    }

                    if (_locStatusCount == _locPurchInvLineCount)
                    {
                        _locShipmentPurchaseInvoiceXPO.ActivationPosting = true;
                        _locShipmentPurchaseInvoiceXPO.Status = Status.Close;
                        _locShipmentPurchaseInvoiceXPO.StatusDate = now;
                        _locShipmentPurchaseInvoiceXPO.Save();
                        _locShipmentPurchaseInvoiceXPO.Session.CommitTransaction();
                    }
                    else
                    {
                        _locShipmentPurchaseInvoiceXPO.Status = Status.Posted;
                        _locShipmentPurchaseInvoiceXPO.StatusDate = now;
                        _locShipmentPurchaseInvoiceXPO.Save();
                        _locShipmentPurchaseInvoiceXPO.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ShipmentPurchaseInvoice " + ex.ToString());
            }
        }

        #endregion Posting Method

        #region Email

        private string BackgroundBody(Session _currentSession, ShipmentPurchaseInvoice _locShipmentPurchaseInvoiceXPO, MailSetupDetail _locMailSetDetail)
        {
            string body = string.Empty;

            if (_locShipmentPurchaseInvoiceXPO != null)
            {

                ShipmentApprovalLine _locShipmentApprovalLineXPO = _currentSession.FindObject<ShipmentApprovalLine>
                                                   (new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("ShipmentPurchaseInvoice", _locShipmentPurchaseInvoiceXPO)));

                string Status = _locShipmentApprovalLineXPO.ApprovalStatus.ToString();

                using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Email/EmailTemplate.html")))
                {
                    body = reader.ReadToEnd();
                }
                body = body.Replace("{Code}", _locShipmentPurchaseInvoiceXPO.Code);

                #region Level
                if (_locShipmentPurchaseInvoiceXPO.ActiveApproved1 == true)
                {
                    string Active = "Level 1";
                    body = body.Replace("{Level}", Active);
                }
                else if (_locShipmentPurchaseInvoiceXPO.ActiveApproved2 == true)
                {
                    string Active = "Level 2";
                    body = body.Replace("{Level}", Active);
                }
                else if (_locShipmentPurchaseInvoiceXPO.ActiveApproved3 == true)
                {
                    string Active = "Level 3";
                    body = body.Replace("{Level}", Active);
                }
                else
                {
                    string Active = "Not Available";
                    body = body.Replace("{Level}", Active);
                }
                #endregion Level

                body = body.Replace("{Status}", Status);
                body = body.Replace("{Message}", _locMailSetDetail.MessageBody);
            }

            return body;
        }

        protected void SendEmail(Session _currentSession, ShipmentPurchaseInvoice _locShipmentPurchaseInvoiceXPO, Status _locStatus, ShipmentApprovalLine _locShipmentApprovalLineXPO2)
        {
            DateTime now = DateTime.Now;
            var User = SecuritySystem.CurrentUserName;
            GlobalFunction _globFunc = new GlobalFunction();
            UserAccess _numLineUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", User));
            string _locMessageBody = null;

            try
            {
                if (_locShipmentPurchaseInvoiceXPO != null)
                {
                    XPCollection<MailSetupDetail> _locMailSetDetails = new XPCollection<MailSetupDetail>
                                                                       (_currentSession, new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("ObjectList", ObjectList.ShipmentPurchaseInvoice),
                                                                        new BinaryOperator("UserAccess", _numLineUserAccess),
                                                                        new BinaryOperator("Active", true)));

                    if (_locMailSetDetails != null && _locMailSetDetails.Count > 0)
                    {
                        foreach (MailSetupDetail _locMailSetDetail in _locMailSetDetails)
                        {
                            if (_locMailSetDetail.MessageBody != null)
                            {
                                if (_locShipmentApprovalLineXPO2.EndApproval == true)
                                {
                                    _locMessageBody = this.BackgroundBody(_currentSession, _locShipmentPurchaseInvoiceXPO, _locMailSetDetail);
                                }
                                else
                                {
                                    _locMessageBody = this.BackgroundBody(_currentSession, _locShipmentPurchaseInvoiceXPO, _locMailSetDetail);
                                }
                            }

                            _globFunc.SetAndSendMail(_locMailSetDetail.SmtpHost, _locMailSetDetail.SmtpPort,
                            _locMailSetDetail.MailFrom, _locMailSetDetail.MailFromPassword, _locMailSetDetail.MailTo, _locMailSetDetail.MailSubject, _locMessageBody);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ShipmentPurchaseInvoice " + ex.ToString());
            }
        }

        #endregion Email

        #region Global Method

        private void SuccessMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 4000;
            options.Message = string.Format("{0}", _locMessage);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }





        #endregion Global Method

        
    }
}
