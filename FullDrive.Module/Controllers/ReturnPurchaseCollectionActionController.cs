﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using System.Collections;
using DevExpress.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class ReturnPurchaseCollectionActionController : ViewController
    {
        public ReturnPurchaseCollectionActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void ReturnPurchaseCollectionShowITIMAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
            var ListView = View as ListView;
            IObjectSpace os = Application.CreateObjectSpace();
            string listViewId = Application.FindListViewId(typeof(InventoryTransferInMonitoring));
            CollectionSourceBase cs = Application.CreateCollectionSource(os, typeof(InventoryTransferInMonitoring), listViewId);
            //Tinggal buat 
            ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
            string _stringITIM = null;
            if (_objectsToProcess != null)
            {
                foreach (Object obj in _objectsToProcess)
                {
                    ReturnPurchaseCollection _locReturnPurchaseCollection = (ReturnPurchaseCollection)_objectSpace.GetObject(obj);
                    if (_locReturnPurchaseCollection != null)
                    {
                        if (_locReturnPurchaseCollection.PurchaseOrder != null && _locReturnPurchaseCollection.InventoryTransferIn != null)
                        {
                            if (_stringITIM == null)
                            {
                                if (_locReturnPurchaseCollection.PurchaseOrder.Code != null && _locReturnPurchaseCollection.InventoryTransferIn.Code != null)
                                {
                                    _stringITIM = "( [PurchaseOrderMonitoring.PurchaseOrder.Code]=='" + _locReturnPurchaseCollection.PurchaseOrder.Code + "' AND [InventoryTransferIn.Code]=='" + _locReturnPurchaseCollection.InventoryTransferIn.Code + "')";
                                }
                            }
                            else
                            {
                                if (_locReturnPurchaseCollection.PurchaseOrder.Code != null && _locReturnPurchaseCollection.InventoryTransferIn.Code != null)
                                {
                                    _stringITIM = _stringITIM + " OR ( [PurchaseOrderMonitoring.PurchaseOrder.Code]=='" + _locReturnPurchaseCollection.PurchaseOrder.Code + "' AND [InventoryTransferIn.Code]=='" + _locReturnPurchaseCollection.InventoryTransferIn.Code + "')";
                                }
                            }
                        }
                    }
                }
            }
            if (_stringITIM != null)
            {
                cs.Criteria["ReturnPurchaseCollectionFilterITIM"] = CriteriaOperator.Parse(_stringITIM);
            }
            e.ShowViewParameters.CreatedView = Application.CreateListView(
                listViewId,
                cs,
                true
            );
            e.ShowViewParameters.Context = TemplateContext.View;
            e.ShowViewParameters.TargetWindow = TargetWindow.NewModalWindow;
            DialogController dc = Application.CreateController<DialogController>();
            dc.Accepting += new EventHandler<DialogControllerAcceptingEventArgs>(dc_Accepting);
            e.ShowViewParameters.Controllers.Add(dc);
        }

        void dc_Accepting(object sender, DialogControllerAcceptingEventArgs e)
        {
            try
            {
                PurchaseReturn _locPurchaseReturn = null;
                View popupView = ((Controller)sender).Frame.View;
                ArrayList _objectsToProcess = new ArrayList(View.SelectedObjects);
                if (_objectsToProcess != null)
                {
                    foreach (Object _obj in _objectsToProcess)
                    {
                        ReturnPurchaseCollection _locReturnPurchaseCollection = (ReturnPurchaseCollection)popupView.ObjectSpace.GetObject(_obj);
                        if (_locReturnPurchaseCollection != null)
                        {
                            if (_locReturnPurchaseCollection.PurchaseReturn != null)
                            {
                                _locPurchaseReturn = _locReturnPurchaseCollection.PurchaseReturn;
                            }
                        }
                    }
                }
                if (_locPurchaseReturn != null)
                {
                    ArrayList _objectsToProcess2 = new ArrayList(popupView.SelectedObjects);
                    if (_objectsToProcess2 != null)
                    {
                        foreach (Object _obj2 in _objectsToProcess2)
                        {
                            InventoryTransferInMonitoring _locInventoryTransferInMonitoring = (InventoryTransferInMonitoring)popupView.ObjectSpace.GetObject(_obj2);
                            if (_locInventoryTransferInMonitoring != null && _locInventoryTransferInMonitoring.Select == true && _locInventoryTransferInMonitoring.ReturnType != ReturnType.None)
                            {
                                _locInventoryTransferInMonitoring.PurchaseReturn = _locPurchaseReturn;
                            }
                        }
                    }
                }
                popupView.ObjectSpace.CommitChanges();
            }
            catch (ValidationException)
            {
            }
        }


    }
}
