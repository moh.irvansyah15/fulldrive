﻿namespace FullDrive.Module.Controllers
{
    partial class WorkOrderMaterialActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.WorkOrderMaterialProgressAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // WorkOrderMaterialProgressAction
            // 
            this.WorkOrderMaterialProgressAction.Caption = "Progress";
            this.WorkOrderMaterialProgressAction.ConfirmationMessage = null;
            this.WorkOrderMaterialProgressAction.Id = "WorkOrderMaterialProgressActionId";
            this.WorkOrderMaterialProgressAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.WorkOrderMaterial);
            this.WorkOrderMaterialProgressAction.ToolTip = null;
            this.WorkOrderMaterialProgressAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.WorkOrderMaterialProgressAction_Execute);
            // 
            // WorkOrderMaterialActionController
            // 
            this.Actions.Add(this.WorkOrderMaterialProgressAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction WorkOrderMaterialProgressAction;
    }
}
