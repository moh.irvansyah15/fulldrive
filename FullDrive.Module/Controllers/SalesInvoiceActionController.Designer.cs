﻿namespace FullDrive.Module.Controllers
{
    partial class SalesInvoiceActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.SalesInvoiceProgressAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.SalesInvoicePostingAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.SalesInvoiceListviewFilterSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.SalesInvoiceGetBillAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.SalesInvoiceGetITOAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.SalesInvoiceApprovalAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // SalesInvoiceProgressAction
            // 
            this.SalesInvoiceProgressAction.Caption = "Progress";
            this.SalesInvoiceProgressAction.ConfirmationMessage = null;
            this.SalesInvoiceProgressAction.Id = "SalesInvoiceProgressActionId";
            this.SalesInvoiceProgressAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesInvoice);
            this.SalesInvoiceProgressAction.ToolTip = null;
            this.SalesInvoiceProgressAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SalesInvoiceProgressAction_Execute);
            // 
            // SalesInvoicePostingAction
            // 
            this.SalesInvoicePostingAction.Caption = "Posting";
            this.SalesInvoicePostingAction.ConfirmationMessage = null;
            this.SalesInvoicePostingAction.Id = "SalesInvoicePostingActionId";
            this.SalesInvoicePostingAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesInvoice);
            this.SalesInvoicePostingAction.ToolTip = null;
            this.SalesInvoicePostingAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SalesInvoicePostingAction_Execute);
            // 
            // SalesInvoiceListviewFilterSelectionAction
            // 
            this.SalesInvoiceListviewFilterSelectionAction.Caption = "Filter";
            this.SalesInvoiceListviewFilterSelectionAction.ConfirmationMessage = null;
            this.SalesInvoiceListviewFilterSelectionAction.Id = "SalesInvoiceListviewFilterSelectionActionId";
            this.SalesInvoiceListviewFilterSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.SalesInvoiceListviewFilterSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesInvoice);
            this.SalesInvoiceListviewFilterSelectionAction.ToolTip = null;
            this.SalesInvoiceListviewFilterSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.SalesInvoiceListviewFilterSelectionAction_Execute);
            // 
            // SalesInvoiceGetBillAction
            // 
            this.SalesInvoiceGetBillAction.Caption = "Get Bill";
            this.SalesInvoiceGetBillAction.ConfirmationMessage = null;
            this.SalesInvoiceGetBillAction.Id = "SalesInvoiceGetBillActionId";
            this.SalesInvoiceGetBillAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesInvoice);
            this.SalesInvoiceGetBillAction.ToolTip = null;
            this.SalesInvoiceGetBillAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SalesInvoiceGetBillAction_Execute);
            // 
            // SalesInvoiceGetITOAction
            // 
            this.SalesInvoiceGetITOAction.Caption = "Get ITO";
            this.SalesInvoiceGetITOAction.ConfirmationMessage = null;
            this.SalesInvoiceGetITOAction.Id = "SalesInvoiceGetITOActionId";
            this.SalesInvoiceGetITOAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesInvoice);
            this.SalesInvoiceGetITOAction.ToolTip = null;
            this.SalesInvoiceGetITOAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SalesInvoiceGetITOAction_Execute);
            // 
            // SalesInvoiceApprovalAction
            // 
            this.SalesInvoiceApprovalAction.Caption = "Approval";
            this.SalesInvoiceApprovalAction.ConfirmationMessage = null;
            this.SalesInvoiceApprovalAction.Id = "SalesInvoiceApprovalActionId";
            this.SalesInvoiceApprovalAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.SalesInvoiceApprovalAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesInvoice);
            this.SalesInvoiceApprovalAction.ToolTip = null;
            this.SalesInvoiceApprovalAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.SalesInvoiceApprovalAction_Execute);
            // 
            // SalesInvoiceActionController
            // 
            this.Actions.Add(this.SalesInvoiceProgressAction);
            this.Actions.Add(this.SalesInvoicePostingAction);
            this.Actions.Add(this.SalesInvoiceListviewFilterSelectionAction);
            this.Actions.Add(this.SalesInvoiceGetBillAction);
            this.Actions.Add(this.SalesInvoiceGetITOAction);
            this.Actions.Add(this.SalesInvoiceApprovalAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction SalesInvoiceProgressAction;
        private DevExpress.ExpressApp.Actions.SimpleAction SalesInvoicePostingAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction SalesInvoiceListviewFilterSelectionAction;
        private DevExpress.ExpressApp.Actions.SimpleAction SalesInvoiceGetBillAction;
        private DevExpress.ExpressApp.Actions.SimpleAction SalesInvoiceGetITOAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction SalesInvoiceApprovalAction;
    }
}
