﻿#region Default

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

#endregion Default

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class ConsumptionActionController : ViewController
    {
        #region Default

        private ChoiceActionItem _selectionListviewFilter;
        public ConsumptionActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            ConsumptionFilterAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                ConsumptionFilterAction.Items.Add(_selectionListviewFilter);
            }
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        #endregion Default

        private void ConsumptionProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        Consumption _locConsumptionOS = (Consumption)_objectSpace.GetObject(obj);

                        if (_locConsumptionOS != null)
                        {
                            if (_locConsumptionOS.Code != null)
                            {
                                _currObjectId = _locConsumptionOS.Code;

                                Consumption _locConsumptionXPO = _currSession.FindObject<Consumption>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Code", _currObjectId)));

                                if (_locConsumptionXPO != null)
                                {
                                    if (_locConsumptionXPO.Status == Status.Open)
                                    {
                                        _locConsumptionXPO.Status = Status.Progress;
                                        _locConsumptionXPO.StatusDate = now;
                                        _locConsumptionXPO.Save();
                                        _locConsumptionXPO.Session.CommitTransaction();

                                        XPCollection<ConsumptionLine> _locConsumptionLines = new XPCollection<ConsumptionLine>
                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("Consumption", _locConsumptionXPO)));

                                        if (_locConsumptionLines != null && _locConsumptionLines.Count > 0)
                                        {
                                            foreach (ConsumptionLine _locConsumptionLine in _locConsumptionLines)
                                            {
                                                if (_locConsumptionLine.Status == Status.Open)
                                                {
                                                    _locConsumptionLine.Status = Status.Progress;
                                                    _locConsumptionLine.StatusDate = now;
                                                    _locConsumptionLine.Save();
                                                    _locConsumptionLine.Session.CommitTransaction();
                                                }
                                            }
                                        }
                                        SuccessMessageShow(_locConsumptionXPO.Code + " has been change successfully to Progress");
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data Consumption Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Consumption Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = Consumption " + ex.ToString());
            }
        }

        private void ConsumptionPostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        Consumption _locConsumptionOS = (Consumption)_objectSpace.GetObject(obj);

                        if (_locConsumptionOS != null)
                        {
                            if (_locConsumptionOS.Code != null)
                            {
                                _currObjectId = _locConsumptionOS.Code;

                                Consumption _locConsumptionXPO = _currSession.FindObject<Consumption>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Code", _currObjectId)));

                                if (_locConsumptionXPO != null)
                                {
                                    if (_locConsumptionXPO.Status == Status.Progress || _locConsumptionXPO.Status == Status.Posted)
                                    {
                                        SetPostingQty(_currSession, _locConsumptionXPO);
                                        SetRemainQty(_currSession, _locConsumptionXPO);
                                        SetOutputProduction(_currSession, _locConsumptionXPO);
                                        SetConsumptionJournal(_currSession, _locConsumptionXPO);
                                        SetProcessCountForPostingOutProd(_currSession, _locConsumptionXPO);
                                        SetStatusConsLine(_currSession, _locConsumptionXPO);
                                        SetNormalQty(_currSession, _locConsumptionXPO);
                                        SetStatusCons(_currSession, _locConsumptionXPO);
                                        SuccessMessageShow(_locConsumptionXPO.Code + " has been change successfully to Posted");
                                    }
                                    else
                                    {
                                        ErrorMessageShow("Data Consumption Not Available");
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data Consumption Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Consumption Not Available");
                            }
                        }
                        else
                        {
                            ErrorMessageShow("Data Consumption Not Available");
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = Consumption " + ex.ToString());
            }
        }

        private void ConsumptionFilterAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(Consumption)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = Consumption " + ex.ToString());
            }
        }

        //==== Code Only ====

        #region PostingforOutputProduction

        //Menentukan jumlah quantity yang di posting
        private void SetPostingQty(Session _currSession, Consumption _locConsumptionXPO)
        {
            try
            {
                if (_locConsumptionXPO != null)
                {
                    XPCollection<ConsumptionLine> _locConsumptionLines = new XPCollection<ConsumptionLine>(_currSession,
                                                                         new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("Consumption", _locConsumptionXPO),
                                                                         new BinaryOperator("Select", true)));

                    if (_locConsumptionLines != null && _locConsumptionLines.Count > 0)
                    {
                        double _locPDQty = 0;
                        double _locPQty = 0;
                        double _locInvLineTotal = 0;
                        ItemUnitOfMeasure _locItemUOM = null;

                        foreach (ConsumptionLine _locConsumptionLine in _locConsumptionLines)
                        {
                            #region ProcessCount=0
                            if (_locConsumptionLine.ProcessCount == 0)
                            {
                                #region MaxQuantity
                                if (_locConsumptionLine.MxDQty > 0 || _locConsumptionLine.MxQty > 0)
                                {
                                    if (_locConsumptionLine.DQty > 0 && _locConsumptionLine.DQty <= _locConsumptionLine.MxDQty)
                                    {
                                        _locPDQty = _locConsumptionLine.DQty;
                                    }

                                    if (_locConsumptionLine.Qty > 0 && _locConsumptionLine.Qty <= _locConsumptionLine.MxQty)
                                    {
                                        _locPQty = _locConsumptionLine.Qty;
                                    }

                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                  (new GroupOperator(GroupOperatorType.And,
                                                   new BinaryOperator("Item", _locConsumptionLine.Item),
                                                   new BinaryOperator("UOM", _locConsumptionLine.MxUOM),
                                                   new BinaryOperator("DefaultUOM", _locConsumptionLine.MxDUOM),
                                                   new BinaryOperator("Active", true)));

                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty + _locPDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }

                                }
                                #endregion MaxQuantity

                                #region NonMaxQuantity
                                else
                                {
                                    if (_locConsumptionLine.DQty > 0)
                                    {
                                        _locPDQty = _locConsumptionLine.DQty;
                                    }

                                    if (_locConsumptionLine.Qty > 0)
                                    {
                                        _locPQty = _locConsumptionLine.Qty;
                                    }

                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                  (new GroupOperator(GroupOperatorType.And,
                                                   new BinaryOperator("Item", _locConsumptionLine.Item),
                                                   new BinaryOperator("UOM", _locConsumptionLine.UOM),
                                                   new BinaryOperator("DefaultUOM", _locConsumptionLine.DUOM),
                                                   new BinaryOperator("Active", true)));

                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty + _locPDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion ProcessCount=0

                            #region ProcessCount>0
                            if (_locConsumptionLine.ProcessCount > 0)
                            {
                                if (_locConsumptionLine.PDQty > 0)
                                {
                                    _locPDQty = _locConsumptionLine.PDQty + _locConsumptionLine.DQty;
                                }

                                if (_locConsumptionLine.PQty > 0)
                                {
                                    _locPQty = _locConsumptionLine.PQty + _locConsumptionLine.Qty;
                                }

                                if (_locConsumptionLine.MxDQty > 0 || _locConsumptionLine.MxQty > 0)
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                  (new GroupOperator(GroupOperatorType.And,
                                                   new BinaryOperator("Item", _locConsumptionLine.Item),
                                                   new BinaryOperator("UOM", _locConsumptionLine.MxUOM),
                                                   new BinaryOperator("DefaultUOM", _locConsumptionLine.MxDUOM),
                                                   new BinaryOperator("Active", true)));
                                }
                                else
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                  (new GroupOperator(GroupOperatorType.And,
                                                   new BinaryOperator("Item", _locConsumptionLine.Item),
                                                   new BinaryOperator("UOM", _locConsumptionLine.UOM),
                                                   new BinaryOperator("DefaultUOM", _locConsumptionLine.DUOM),
                                                   new BinaryOperator("Active", true)));
                                }

                                if (_locItemUOM != null)
                                {
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locPQty + _locPDQty;
                                }

                            }
                            #endregion ProcessCount>0

                            _locConsumptionLine.PDQty = _locPDQty;
                            _locConsumptionLine.PDUOM = _locConsumptionLine.DUOM;
                            _locConsumptionLine.PQty = _locPQty;
                            _locConsumptionLine.PUOM = _locConsumptionLine.UOM;
                            _locConsumptionLine.PTQty = _locInvLineTotal;
                            _locConsumptionLine.Save();
                            _locConsumptionLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = WorkOrder " + ex.ToString());
            }
        }

        //Menentukan sisa dari transaksi
        private void SetRemainQty(Session _currSession, Consumption _locConsumptionXPO)
        {
            try
            {
                if (_locConsumptionXPO != null)
                {
                    XPCollection<ConsumptionLine> _locConsumptionLines = new XPCollection<ConsumptionLine>(_currSession,
                                                                         new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("Consumption", _locConsumptionXPO),
                                                                         new BinaryOperator("Select", true)));

                    if (_locConsumptionLines != null && _locConsumptionLines.Count > 0)
                    {
                        double _locRmDQty = 0;
                        double _locRmQty = 0;
                        double _locInvLineTotal = 0;
                        ItemUnitOfMeasure _locItemUOM = null;

                        foreach (ConsumptionLine _locConsumptionLine in _locConsumptionLines)
                        {
                            #region ProcessCount=0
                            if (_locConsumptionLine.ProcessCount == 0)
                            {
                                #region MaxQuantity
                                if (_locConsumptionLine.MxDQty > 0 || _locConsumptionLine.MxQty > 0)
                                {
                                    if (_locConsumptionLine.DQty > 0 && _locConsumptionLine.DQty <= _locConsumptionLine.MxDQty)
                                    {
                                        _locRmDQty = _locConsumptionLine.MxDQty - _locConsumptionLine.DQty;
                                    }

                                    if (_locConsumptionLine.Qty > 0 && _locConsumptionLine.Qty <= _locConsumptionLine.MxQty)
                                    {
                                        _locRmQty = _locConsumptionLine.MxQty - _locConsumptionLine.Qty;
                                    }

                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                  (new GroupOperator(GroupOperatorType.And,
                                                   new BinaryOperator("Item", _locConsumptionLine.Item),
                                                   new BinaryOperator("UOM", _locConsumptionLine.MxUOM),
                                                   new BinaryOperator("DefaultUOM", _locConsumptionLine.MxDUOM),
                                                   new BinaryOperator("Active", true)));

                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty + _locRmDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }

                                }
                                #endregion MaxQuantity

                                #region NonMaxQuantity
                                else
                                {
                                    if (_locConsumptionLine.DQty > 0)
                                    {
                                        _locRmDQty = _locConsumptionLine.DQty;
                                    }

                                    if (_locConsumptionLine.Qty > 0)
                                    {
                                        _locRmQty = _locConsumptionLine.Qty;
                                    }

                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                  (new GroupOperator(GroupOperatorType.And,
                                                   new BinaryOperator("Item", _locConsumptionLine.Item),
                                                   new BinaryOperator("UOM", _locConsumptionLine.UOM),
                                                   new BinaryOperator("DefaultUOM", _locConsumptionLine.DUOM),
                                                   new BinaryOperator("Active", true)));

                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty + _locRmDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion ProcessCount=0

                            #region ProcessCount>0
                            if (_locConsumptionLine.ProcessCount > 0)
                            {
                                if (_locConsumptionLine.RmDQty > 0)
                                {
                                    _locRmDQty = _locConsumptionLine.RmDQty - _locConsumptionLine.DQty;
                                }

                                if (_locConsumptionLine.RmQty > 0)
                                {
                                    _locRmQty = _locConsumptionLine.RmQty - _locConsumptionLine.Qty;
                                }

                                if (_locConsumptionLine.MxDQty > 0 || _locConsumptionLine.MxQty > 0)
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                  (new GroupOperator(GroupOperatorType.And,
                                                   new BinaryOperator("Item", _locConsumptionLine.Item),
                                                   new BinaryOperator("UOM", _locConsumptionLine.MxUOM),
                                                   new BinaryOperator("DefaultUOM", _locConsumptionLine.MxDUOM),
                                                   new BinaryOperator("Active", true)));
                                }
                                else
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                  (new GroupOperator(GroupOperatorType.And,
                                                   new BinaryOperator("Item", _locConsumptionLine.Item),
                                                   new BinaryOperator("UOM", _locConsumptionLine.UOM),
                                                   new BinaryOperator("DefaultUOM", _locConsumptionLine.DUOM),
                                                   new BinaryOperator("Active", true)));
                                }

                                if (_locItemUOM != null)
                                {
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locRmQty + _locRmDQty;
                                }

                            }
                            #endregion ProcessCount>0

                            _locConsumptionLine.RmDQty = _locRmDQty;
                            _locConsumptionLine.RmQty = _locRmQty;
                            _locConsumptionLine.RmTQty = _locInvLineTotal;
                            _locConsumptionLine.Save();
                            _locConsumptionLine.Session.CommitTransaction();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = Consumption " + ex.ToString());
            }
        }

        //Membuat outputproduction
        private void SetOutputProduction(Session _currSession, Consumption _locConsumptionXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _currSignCode = null;

                if (_locConsumptionXPO != null)
                {
                    if (_locConsumptionXPO.Status == Status.Progress)
                    {
                        _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.OutputProduction);

                        if (_currSignCode != null)
                        {
                            OutputProduction _saveDataOutPro = new OutputProduction(_currSession)
                            {
                                SignCode = _currSignCode,
                                Name = _locConsumptionXPO.Name,
                                Description = _locConsumptionXPO.Description,
                                StartDate = _locConsumptionXPO.StartDate,
                                EndDate = _locConsumptionXPO.EndDate,
                                StatusDate = _locConsumptionXPO.StatusDate,
                                Production = _locConsumptionXPO.Production,
                                MachineMapVersion = _locConsumptionXPO.MachineMapVersion,
                                MachineCost = _locConsumptionXPO.MachineCost,
                                Company = _locConsumptionXPO.Company,
                                WorkOrder = _locConsumptionXPO.WorkOrder,
                                Consumption = _locConsumptionXPO,
                            };
                            _saveDataOutPro.Save();
                            _saveDataOutPro.Session.CommitTransaction();
                        }
                    }
                }

                XPCollection<ConsumptionLine> _numLineConsLines = new XPCollection<ConsumptionLine>(_currSession,
                                                                    new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Consumption", _locConsumptionXPO),
                                                                    new BinaryOperator("Select", true)));

                if (_numLineConsLines != null && _numLineConsLines.Count > 0)
                {
                    OutputProduction _locOutProd2 = _currSession.FindObject<OutputProduction>
                                                    (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Consumption", _locConsumptionXPO)));

                    if (_locOutProd2 != null)
                    {
                        if (_numLineConsLines != null && _numLineConsLines.Count > 0)
                        {
                            foreach (ConsumptionLine _numLineConsLine in _numLineConsLines)
                            {
                                if (_numLineConsLine.Status == Status.Progress || _numLineConsLine.Status == Status.Posted)
                                {
                                    OutputProductionLine _saveDataOutProdLine = new OutputProductionLine(_currSession)
                                    {
                                        Name = _numLineConsLine.Name,
                                        Item = _numLineConsLine.Item,
                                        Location = _numLineConsLine.Location,
                                        BinLocation = _numLineConsLine.BinLocation,
                                        Description = _numLineConsLine.Description,
                                        MxDQty = _numLineConsLine.DQty,
                                        MxDUOM = _numLineConsLine.DUOM,
                                        MxQty = _numLineConsLine.Qty,
                                        MxUOM = _numLineConsLine.UOM,
                                        MxTQty = _numLineConsLine.TQty,
                                        DQty = _numLineConsLine.DQty,
                                        DUOM = _numLineConsLine.DUOM,
                                        Qty = _numLineConsLine.Qty,
                                        UOM = _numLineConsLine.UOM,
                                        TQty = _numLineConsLine.TQty,
                                        ADUOM = _numLineConsLine.DUOM,
                                        AUOM = _numLineConsLine.UOM,
                                        BillOfMaterial = _numLineConsLine.BillOfMaterial,
                                        BillOfMaterialVersion = _numLineConsLine.BillOfMaterialVersion,
                                        Company = _numLineConsLine.Company,
                                        OutputProduction = _locOutProd2,
                                    };
                                    _saveDataOutProdLine.Save();
                                    _saveDataOutProdLine.Session.CommitTransaction();
                                }
                            }
                        }
                    }
                    else
                    {
                        ErrorMessageShow("Data OutputProduction Not Available");
                    }
                }
                else
                {
                    ErrorMessageShow("Data ConsumptionLine Not Available");
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = Consumption ", ex.ToString());
            }
        }

        //Membuat consumption journal
        private void SetConsumptionJournal(Session _currSession, Consumption _locConsumptionXPO)
        {
            try
            {
                XPCollection<ConsumptionLine> _locConsLines = new XPCollection<ConsumptionLine>(_currSession,
                                                              new GroupOperator(GroupOperatorType.And,
                                                              new BinaryOperator("Consumption", _locConsumptionXPO),
                                                              new BinaryOperator("Select", true)));

                if (_locConsLines != null && _locConsLines.Count > 0)
                {
                    double _locConsLineTotal = 0;
                    double _locConsLineTotalActual = 0;
                    DateTime now = DateTime.Now;

                    foreach (ConsumptionLine _locConsLine in _locConsLines)
                    {
                        if (_locConsLine.Status == Status.Progress)
                        {
                            if (_locConsLine.DQty > 0 || _locConsLine.Qty > 0)
                            {
                                if (_locConsLine.Item != null && _locConsLine.UOM != null && _locConsLine.DUOM != null)
                                {
                                    ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Item", _locConsLine.Item),
                                                                     new BinaryOperator("UOM", _locConsLine.UOM),
                                                                     new BinaryOperator("DefaultUOM", _locConsLine.DUOM),
                                                                     new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        #region Consumption
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locConsLineTotal = _locConsLine.Qty * _locItemUOM.DefaultConversion + _locConsLine.DQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locConsLineTotal = _locConsLine.Qty / _locItemUOM.Conversion + _locConsLine.DQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locConsLineTotal = _locConsLine.Qty + _locConsLine.DQty;
                                        }
                                        #endregion Consumption

                                        #region Actual
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locConsLineTotalActual = _locConsLine.AQty * _locItemUOM.DefaultConversion + _locConsLine.ADQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locConsLineTotalActual = _locConsLine.AQty / _locItemUOM.Conversion + _locConsLine.ADQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locConsLineTotalActual = _locConsLine.AQty + _locConsLine.ADQty;
                                        }
                                        #endregion Actual

                                        ConsumptionJournal _locConsJournal = new ConsumptionJournal(_currSession)
                                        {
                                            Location = _locConsLine.Location,
                                            BinLocation = _locConsLine.BinLocation,
                                            Item = _locConsLine.Item,
                                            Company = _locConsLine.Company,
                                            QtyCons = _locConsLineTotal,
                                            QtyActual = _locConsLineTotalActual,
                                            QtyReturn = _locConsLineTotal - _locConsLineTotalActual,
                                            DUOM = _locConsLine.DUOM,
                                            JournalDate = now,
                                            Consumption = _locConsumptionXPO,
                                        };
                                        _locConsJournal.Save();
                                        _locConsJournal.Session.CommitTransaction();
                                    }
                                }
                                else
                                {
                                    _locConsLineTotal = _locConsLine.Qty + _locConsLine.DQty;
                                    _locConsLineTotalActual = _locConsLine.AQty + _locConsLine.ADQty;

                                    ConsumptionJournal _locConsJournal = new ConsumptionJournal(_currSession)
                                    {
                                        Location = _locConsLine.Location,
                                        BinLocation = _locConsLine.BinLocation,
                                        Item = _locConsLine.Item,
                                        Company = _locConsLine.Company,
                                        QtyCons = _locConsLineTotal,
                                        QtyActual = _locConsLineTotalActual,
                                        QtyReturn = _locConsLineTotal - _locConsLineTotalActual,
                                        DUOM = _locConsLine.DUOM,
                                        JournalDate = now,
                                        Consumption = _locConsumptionXPO,
                                    };
                                    _locConsJournal.Save();
                                    _locConsJournal.Session.CommitTransaction();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = Consumption ", ex.ToString());
            }
        }

        //Menentukan banyak proses posting
        private void SetProcessCountForPostingOutProd(Session _currSession, Consumption _locConsumptionXPO)
        {
            try
            {
                if (_locConsumptionXPO != null)
                {
                    XPCollection<ConsumptionLine> _locConsLines = new XPCollection<ConsumptionLine>(_currSession,
                                                                  new GroupOperator(GroupOperatorType.And,
                                                                  new BinaryOperator("Consumption", _locConsumptionXPO),
                                                                  new BinaryOperator("Select", true)));

                    if (_locConsLines != null && _locConsLines.Count > 0)
                    {
                        foreach (ConsumptionLine _locConsLine in _locConsLines)
                        {
                            if (_locConsLine.Status == Status.Progress || _locConsLine.Status == Status.Posted)
                            {
                                _locConsLine.ProcessCount = _locConsLine.ProcessCount + 1;
                                _locConsLine.Save();
                                _locConsLine.Session.CommitTransaction();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = Consumption " + ex.ToString());
            }
        }

        //Menentukan status pada consumptionline
        private void SetStatusConsLine(Session _currSession, Consumption _locConsumptionXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locConsumptionXPO != null)
                {
                    XPCollection<ConsumptionLine> _locConsLines = new XPCollection<ConsumptionLine>(_currSession,
                                                                  new GroupOperator(GroupOperatorType.And,
                                                                  new BinaryOperator("Consumption", _locConsumptionXPO),
                                                                  new BinaryOperator("Select", true)));

                    if (_locConsLines != null && _locConsLines.Count > 0)
                    {
                        foreach (ConsumptionLine _locConsLine in _locConsLines)
                        {
                            if (_locConsLine.Status == Status.Progress || _locConsLine.Status == Status.Posted)
                            {
                                if (_locConsLine.DQty > 0 || _locConsLine.Qty > 0)
                                {
                                    if (_locConsLine.RmDQty == 0 && _locConsLine.RmQty == 0 && _locConsLine.RmTQty == 0)
                                    {
                                        _locConsLine.Status = Status.Close;
                                        _locConsLine.ActivationPosting = true;
                                        _locConsLine.StatusDate = now;
                                    }
                                    else
                                    {
                                        _locConsLine.Status = Status.Posted;
                                        _locConsLine.StatusDate = now;
                                    }
                                    _locConsLine.Save();
                                    _locConsLine.Session.CommitTransaction();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = Consumption " + ex.ToString());
            }
        }

        //Menormalkan Quantity
        private void SetNormalQty(Session _currSession, Consumption _locConsumptionXPO)
        {
            try
            {
                if (_locConsumptionXPO != null)
                {
                    XPCollection<ConsumptionLine> _locConsLines = new XPCollection<ConsumptionLine>(_currSession,
                                                                  new GroupOperator(GroupOperatorType.And,
                                                                  new BinaryOperator("Consumption", _locConsumptionXPO),
                                                                  new BinaryOperator("Select", true)));

                    if (_locConsLines != null && _locConsLines.Count > 0)
                    {
                        foreach (ConsumptionLine _locConsLine in _locConsLines)
                        {
                            if (_locConsLine.Status == Status.Progress || _locConsLine.Status == Status.Posted || _locConsLine.Status == Status.Close)
                            {
                                if (_locConsLine.DQty > 0 || _locConsLine.Qty > 0)
                                {
                                    _locConsLine.Select = false;
                                    _locConsLine.DQty = 0;
                                    _locConsLine.Qty = 0;
                                    _locConsLine.Save();
                                    _locConsLine.Session.CommitTransaction();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = Consumption " + ex.ToString());
            }
        }

        //Menentukan status pada consumption
        private void SetStatusCons(Session _currSession, Consumption _locConsumptionXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locCountStatus = 0;
                bool _locActivationPosting = false;

                if (_locConsumptionXPO != null)
                {
                    XPCollection<ConsumptionLine> _locConsLines = new XPCollection<ConsumptionLine>
                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                 new BinaryOperator("Consumption", _locConsumptionXPO)));

                    if (_locConsLines != null && _locConsLines.Count() > 0)
                    {
                        foreach (ConsumptionLine _locConsLine in _locConsLines)
                        {
                            if (_locConsLine.Status != Status.Close)
                            {
                                _locCountStatus = _locCountStatus + 1;
                            }
                        }
                    }

                    if (_locCountStatus > 0)
                    {
                        if (_locConsLines.Count() == _locCountStatus)
                        {
                            _locActivationPosting = true;
                        }
                    }

                    _locConsumptionXPO.Status = Status.Posted;
                    _locActivationPosting = true;
                    _locConsumptionXPO.StatusDate = now;
                    _locConsumptionXPO.ActivationPosting = _locActivationPosting;
                    _locConsumptionXPO.Save();
                    _locConsumptionXPO.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = Consumption " + ex.ToString());
            }
        }

        #endregion PostingforOutputProduction

        #region Global Method

        private void SuccessMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 4000;
            options.Message = string.Format("{0}", _locMessage);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        //== Get ===

        private int GetStockType(StockType objectName)
        {
            int _result = 0;
            try
            {
                if (objectName == StockType.Good)
                {
                    _result = 1;
                }
                else if (objectName == StockType.Bad)
                {
                    _result = 2;
                }
                else
                {
                    _result = 0;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = Consumption " + ex.ToString());
            }
            return _result;
        }

        private int GetActive(bool objectName)
        {
            int _result = 0;
            try
            {
                if (objectName == true)
                {
                    _result = 1;
                }
                else
                {
                    _result = 0;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = Consumption " + ex.ToString());
            }
            return _result;
        }

        private double GetUnitPriceByPurchaseOrder(Session _currSession, PurchaseOrder _locPurchaseOrder, Item _locItem)
        {
            double _result = 0;
            try
            {
                if (_locPurchaseOrder != null)
                {
                    PurchaseOrderLine _locPurchaseOrderLine = _currSession.FindObject<PurchaseOrderLine>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseOrder", _locPurchaseOrder),
                                                            new BinaryOperator("Item", _locItem)));
                    if (_locPurchaseOrderLine != null)
                    {
                        _result = _locPurchaseOrderLine.UAmount;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = Consumption " + ex.ToString());
            }
            return _result;
        }

        #endregion Global Method

    }
}
