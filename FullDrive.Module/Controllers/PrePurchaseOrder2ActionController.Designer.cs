﻿namespace FullDrive.Module.Controllers
{
    partial class PrePurchaseOrder2ActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.PrePurchaseOrder2ProgressAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PrePurchaseOrder2PostingAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PrePurchaseOrder2ListviewFilterSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // PrePurchaseOrder2ProgressAction
            // 
            this.PrePurchaseOrder2ProgressAction.Caption = "Progress";
            this.PrePurchaseOrder2ProgressAction.ConfirmationMessage = null;
            this.PrePurchaseOrder2ProgressAction.Id = "PrePurchaseOrder2ProgressActionId";
            this.PrePurchaseOrder2ProgressAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PrePurchaseOrder2);
            this.PrePurchaseOrder2ProgressAction.ToolTip = null;
            this.PrePurchaseOrder2ProgressAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PrePurchaseOrder2ProgressAction_Execute);
            // 
            // PrePurchaseOrder2PostingAction
            // 
            this.PrePurchaseOrder2PostingAction.Caption = "Posting";
            this.PrePurchaseOrder2PostingAction.ConfirmationMessage = null;
            this.PrePurchaseOrder2PostingAction.Id = "PrePurchaseOrder2PostingActionId";
            this.PrePurchaseOrder2PostingAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PrePurchaseOrder2);
            this.PrePurchaseOrder2PostingAction.ToolTip = null;
            this.PrePurchaseOrder2PostingAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PrePurchaseOrder2PostingAction_Execute);
            // 
            // PrePurchaseOrder2ListviewFilterSelectionAction
            // 
            this.PrePurchaseOrder2ListviewFilterSelectionAction.Caption = "Filter";
            this.PrePurchaseOrder2ListviewFilterSelectionAction.ConfirmationMessage = null;
            this.PrePurchaseOrder2ListviewFilterSelectionAction.Id = "PrePurchaseOrder2ListviewFilterSelectionActionId";
            this.PrePurchaseOrder2ListviewFilterSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.PrePurchaseOrder2ListviewFilterSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PrePurchaseOrder2);
            this.PrePurchaseOrder2ListviewFilterSelectionAction.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.PrePurchaseOrder2ListviewFilterSelectionAction.ToolTip = null;
            this.PrePurchaseOrder2ListviewFilterSelectionAction.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.PrePurchaseOrder2ListviewFilterSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.PrePurchaseOrder2ListviewFilterSelectionAction_Execute);
            // 
            // PrePurchaseOrder2ActionController
            // 
            this.Actions.Add(this.PrePurchaseOrder2ProgressAction);
            this.Actions.Add(this.PrePurchaseOrder2PostingAction);
            this.Actions.Add(this.PrePurchaseOrder2ListviewFilterSelectionAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction PrePurchaseOrder2ProgressAction;
        private DevExpress.ExpressApp.Actions.SimpleAction PrePurchaseOrder2PostingAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction PrePurchaseOrder2ListviewFilterSelectionAction;
    }
}
