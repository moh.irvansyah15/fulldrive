﻿namespace FullDrive.Module.Controllers
{
    partial class ShipmentPurchaseMonitoringActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ShipmentPurchaseMonitoringSelectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.ShipmentPurchaseMonitoringUnselectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // ShipmentPurchaseMonitoringSelectAction
            // 
            this.ShipmentPurchaseMonitoringSelectAction.Caption = "Select";
            this.ShipmentPurchaseMonitoringSelectAction.ConfirmationMessage = null;
            this.ShipmentPurchaseMonitoringSelectAction.Id = "ShipmentPurchaseMonitoringSelectActionId";
            this.ShipmentPurchaseMonitoringSelectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.ShipmentPurchaseMonitoring);
            this.ShipmentPurchaseMonitoringSelectAction.ToolTip = null;
            this.ShipmentPurchaseMonitoringSelectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.ShipmentPurchaseMonitoringSelectAction_Execute);
            // 
            // ShipmentPurchaseMonitoringUnselectAction
            // 
            this.ShipmentPurchaseMonitoringUnselectAction.Caption = "Unselect";
            this.ShipmentPurchaseMonitoringUnselectAction.ConfirmationMessage = null;
            this.ShipmentPurchaseMonitoringUnselectAction.Id = "ShipmentPurchaseMonitoringUnselectActionId";
            this.ShipmentPurchaseMonitoringUnselectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.ShipmentPurchaseMonitoring);
            this.ShipmentPurchaseMonitoringUnselectAction.ToolTip = null;
            this.ShipmentPurchaseMonitoringUnselectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.ShipmentPurchaseMonitoringUnselectAction_Execute);
            // 
            // ShipmentPurchaseMonitoringActionController
            // 
            this.Actions.Add(this.ShipmentPurchaseMonitoringSelectAction);
            this.Actions.Add(this.ShipmentPurchaseMonitoringUnselectAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction ShipmentPurchaseMonitoringSelectAction;
        private DevExpress.ExpressApp.Actions.SimpleAction ShipmentPurchaseMonitoringUnselectAction;
    }
}
