﻿namespace FullDrive.Module.Controllers
{
    partial class ShipmentSalesMonitoringActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ShipmentSalesMonitoringSelectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.ShipmentSalesMonitoringUnselectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // ShipmentSalesMonitoringSelectAction
            // 
            this.ShipmentSalesMonitoringSelectAction.Caption = "Select";
            this.ShipmentSalesMonitoringSelectAction.ConfirmationMessage = null;
            this.ShipmentSalesMonitoringSelectAction.Id = "ShipmentSalesMonitoringSelectActionId";
            this.ShipmentSalesMonitoringSelectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.ShipmentSalesMonitoring);
            this.ShipmentSalesMonitoringSelectAction.ToolTip = null;
            this.ShipmentSalesMonitoringSelectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.ShipmentSalesMonitoringSelectAction_Execute);
            // 
            // ShipmentSalesMonitoringUnselectAction
            // 
            this.ShipmentSalesMonitoringUnselectAction.Caption = "Unselect";
            this.ShipmentSalesMonitoringUnselectAction.ConfirmationMessage = null;
            this.ShipmentSalesMonitoringUnselectAction.Id = "ShipmentSalesMonitoringUnselectActionId";
            this.ShipmentSalesMonitoringUnselectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.ShipmentSalesMonitoring);
            this.ShipmentSalesMonitoringUnselectAction.ToolTip = null;
            this.ShipmentSalesMonitoringUnselectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.ShipmentSalesMonitoringUnselectAction_Execute);
            // 
            // ShipmentSalesMonitoringActionController
            // 
            this.Actions.Add(this.ShipmentSalesMonitoringSelectAction);
            this.Actions.Add(this.ShipmentSalesMonitoringUnselectAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction ShipmentSalesMonitoringSelectAction;
        private DevExpress.ExpressApp.Actions.SimpleAction ShipmentSalesMonitoringUnselectAction;
    }
}
