﻿#region Default

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

#endregion Default

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;
using System.IO;
using System.Web;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class PurchaseOrderActionController : ViewController
    {
        #region Default

        private ChoiceActionItem _selectionListviewFilter;
        private ChoiceActionItem _selectionApprovalListviewFilter;
        private ChoiceActionItem _setApprovalLevel;

        public PurchaseOrderActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            #region FilterStatus
            PurchaseOrderListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                PurchaseOrderListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
            #endregion FilterStatus

            #region FilterApproval
            PurchaseOrderListviewFilterApprovalSelectionAction.Items.Clear();
            foreach (object _currApprovalFilter in Enum.GetValues(typeof(CustomProcess.ApprovalFilter)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.ApprovalFilter));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApprovalFilter), _currApprovalFilter);
                PurchaseOrderListviewFilterApprovalSelectionAction.Items.Add(_selectionListviewFilter);
            }
            #endregion FilterApproval
        }

        protected override void OnActivated()
        {
            TypeOfView = typeof(ObjectView);
            base.OnActivated();
            // Perform various tasks depending on the target View.
            #region Approval
            var _locUser = SecuritySystem.CurrentUserName;
            GlobalFunction _globalFunction = new GlobalFunction();
            Session _currentSession = null;
            if (this.ObjectSpace != null)
            {
                _currentSession = ((XPObjectSpace)this.ObjectSpace).Session;
            }

            if (_currentSession != null)
            {
                UserAccess _locUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", _locUser));

                if (_locUserAccess != null)
                {
                    PurchaseOrderApprovalAction.Items.Clear();

                    XPCollection<ApplicationSetupDetail> _locAppSetupDetails = new XPCollection<ApplicationSetupDetail>
                                                                                 (_currentSession, new GroupOperator(GroupOperatorType.And,
                                                                                  new BinaryOperator("UserAccess", _locUserAccess),
                                                                                  new BinaryOperator("ObjectList", CustomProcess.ObjectList.PurchaseOrder),
                                                                                  new BinaryOperator("FunctionList", CustomProcess.FunctionList.Approval),
                                                                                  new BinaryOperator("Active", true)));

                    if (_locAppSetupDetails != null && _locAppSetupDetails.Count > 0)
                    {

                        foreach (ApplicationSetupDetail _locAppSetupDetail in _locAppSetupDetails)
                        {
                            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.ApprovalLevel)))
                            {
                                if (_locAppSetupDetail.ApprovalLevel != CustomProcess.ApprovalLevel.None)
                                {
                                    EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.ApprovalLevel));
                                    _setApprovalLevel = new ChoiceActionItem(_ed.GetCaption(_locAppSetupDetail.ApprovalLevel), _locAppSetupDetail.ApprovalLevel);
                                }
                            }
                            PurchaseOrderApprovalAction.Items.Add(_setApprovalLevel);
                        }
                    }
                }
            }
            #endregion Approval
        }

        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }

        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        #endregion Default

        private void PurchaseOrderProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;


                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        PurchaseOrder _locPurchaseOrderOS = (PurchaseOrder)_objectSpace.GetObject(obj);

                        if (_locPurchaseOrderOS != null)
                        {
                            if (_locPurchaseOrderOS.Code != null)
                            {
                                _currObjectId = _locPurchaseOrderOS.Code;

                                PurchaseOrder _locPurchaseOrderXPO = _currSession.FindObject<PurchaseOrder>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locPurchaseOrderXPO != null)
                                {
                                    if (_locPurchaseOrderXPO.Status == Status.Open || _locPurchaseOrderXPO.Status == Status.Progress)
                                    {
                                        if (_locPurchaseOrderXPO.Status == Status.Open)
                                        {
                                            _locPurchaseOrderXPO.Status = Status.Progress;
                                            _locPurchaseOrderXPO.StatusDate = now;
                                            _locPurchaseOrderXPO.Save();
                                            _locPurchaseOrderXPO.Session.CommitTransaction();
                                        }

                                        XPCollection<PurchaseOrderLine> _locPurchaseOrderLines = new XPCollection<PurchaseOrderLine>
                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO),
                                                                new BinaryOperator("Status", Status.Open)));

                                        if (_locPurchaseOrderLines != null && _locPurchaseOrderLines.Count > 0)
                                        {
                                            foreach (PurchaseOrderLine _locPurchaseOrderLine in _locPurchaseOrderLines)
                                            {
                                                _locPurchaseOrderLine.Status = Status.Progress;
                                                _locPurchaseOrderLine.StatusDate = now;
                                                _locPurchaseOrderLine.Save();
                                                _locPurchaseOrderLine.Session.CommitTransaction();
                                            }
                                        }

                                        XPCollection<PurchaseRequisitionCollection> _locPurReqCollections = new XPCollection<PurchaseRequisitionCollection>
                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO),
                                                                new BinaryOperator("Status", Status.Open)));

                                        if (_locPurReqCollections != null && _locPurReqCollections.Count > 0)
                                        {
                                            foreach (PurchaseRequisitionCollection _locPurReqCollection in _locPurReqCollections)
                                            {
                                                _locPurReqCollection.Status = Status.Progress;
                                                _locPurReqCollection.StatusDate = now;
                                                _locPurReqCollection.Save();
                                                _locPurReqCollection.Session.CommitTransaction();
                                            }
                                        }
                                    }
                                    SuccessMessageShow("Purchase Order has been successfully updated to progress");
                                }
                                else
                                {
                                    ErrorMessageShow("Data Purchase Order Not Available");
                                }

                            }
                            else
                            {
                                ErrorMessageShow("Data Purchase Order Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseOrder " + ex.ToString());
            }
        }

        private void PurchaseOrderPostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        PurchaseOrder _locPurchaseOrderOS = (PurchaseOrder)_objectSpace.GetObject(obj);

                        if (_locPurchaseOrderOS != null)
                        {
                            if (_locPurchaseOrderOS.Code != null)
                            {
                                _currObjectId = _locPurchaseOrderOS.Code;

                                PurchaseOrder _locPurchaseOrderXPO = _currSession.FindObject<PurchaseOrder>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locPurchaseOrderXPO != null)
                                {
                                    if (_locPurchaseOrderXPO.Status == Status.Progress || _locPurchaseOrderXPO.Status == Status.Posted)
                                    {
                                        ApprovalLine _locApprovalLineXPO = _currSession.FindObject<ApprovalLine>
                                                                           (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("EndApproval", true),
                                                                            new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO)));

                                        if (_locApprovalLineXPO != null)
                                        {
                                            SetPurchaseOrderMonitoring(_currSession, _locPurchaseOrderXPO);
                                            SetRemainQty(_currSession, _locPurchaseOrderXPO);
                                            SetPostingQty(_currSession, _locPurchaseOrderXPO);
                                            SetProcessCount(_currSession, _locPurchaseOrderXPO);
                                            SetStatusPurchaseOrderLine(_currSession, _locPurchaseOrderXPO);
                                            SetNormalQuantity(_currSession, _locPurchaseOrderXPO);
                                            SetFinalStatusPurchaseOrder(_currSession, _locPurchaseOrderXPO);
                                            SetMaksPay(_currSession, _locPurchaseOrderXPO);
                                            SuccessMessageShow("Purchase Order has been successfully post");
                                        }
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data Purchase Order Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Purchase Order Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseOrder " + ex.ToString());
            }
        }

        private void PurchaseOrderGetPRAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        PurchaseOrder _locPurchaseOrderOS = (PurchaseOrder)_objectSpace.GetObject(obj);

                        if (_locPurchaseOrderOS != null)
                        {
                            if (_locPurchaseOrderOS.Code != null)
                            {
                                _currObjectId = _locPurchaseOrderOS.Code;

                                PurchaseOrder _locPurchaseOrderXPO = _currSession.FindObject<PurchaseOrder>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locPurchaseOrderXPO != null)
                                {
                                    if (_locPurchaseOrderXPO.Status == Status.Open || _locPurchaseOrderXPO.Status == Status.Progress)
                                    {
                                        XPCollection<PurchaseRequisitionCollection> _locPurchaseRequisitionCollections = new XPCollection<PurchaseRequisitionCollection>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO),
                                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("Status", Status.Open),
                                                                                        new BinaryOperator("Status", Status.Progress))));

                                        if (_locPurchaseRequisitionCollections != null && _locPurchaseRequisitionCollections.Count() > 0)
                                        {
                                            foreach (PurchaseRequisitionCollection _locPurchaseRequisitionCollection in _locPurchaseRequisitionCollections)
                                            {
                                                if (_locPurchaseRequisitionCollection.PurchaseRequisition != null)
                                                {
                                                    ApprovalLine _locApprovalLineXPO = _currSession.FindObject<ApprovalLine>
                                                                       (new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("EndApproval", true),
                                                                        new BinaryOperator("PurchaseRequisition", _locPurchaseRequisitionCollection.PurchaseRequisition)));
                                                    if (_locApprovalLineXPO != null)
                                                    {
                                                        GetPurchaseRequisitionMonitoring(_currSession, _locPurchaseRequisitionCollection.PurchaseRequisition, _locPurchaseOrderXPO);
                                                    }
                                                }
                                            }
                                            SuccessMessageShow("Purchase Requisition Has Been Successfully Getting into Purchase Order");
                                        }
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data Purchase Order Not Available");
                                }

                            }
                            else
                            {
                                ErrorMessageShow("Data Purchase Order Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseOrder " + ex.ToString());
            }
        }

        private void PurchaseOrderPurchaseReturnAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                string _currSignCode = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        PurchaseOrder _locPurchaseOrderOS = (PurchaseOrder)_objectSpace.GetObject(obj);

                        if (_locPurchaseOrderOS != null)
                        {
                            if (_locPurchaseOrderOS.Code != null)
                            {
                                _currObjectId = _locPurchaseOrderOS.Code;
                                PurchaseReturn _locPurchaseReturnByPO = null;

                                PurchaseOrder _locPurchaseOrderXPO = _currSession.FindObject<PurchaseOrder>
                                                                     (new GroupOperator(GroupOperatorType.And,
                                                                      new BinaryOperator("Code", _currObjectId)));
                                if (_locPurchaseOrderXPO != null)
                                {
                                    if (_locPurchaseOrderXPO.Status == Status.Progress || _locPurchaseOrderXPO.Status == Status.Posted)
                                    {
                                        _locPurchaseReturnByPO = _currSession.FindObject<PurchaseReturn>(new GroupOperator(GroupOperatorType.And,
                                                                 new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO)));

                                        if (_locPurchaseReturnByPO == null)
                                        {
                                            _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.PurchaseReturn);

                                            if (_currSignCode != null)
                                            {
                                                PurchaseReturn _saveDataPReturn = new PurchaseReturn(_currSession)
                                                {
                                                    BuyFromVendor = _locPurchaseOrderXPO.BuyFromVendor,
                                                    BuyFromContact = _locPurchaseOrderXPO.BuyFromContact,
                                                    BuyFromCountry = _locPurchaseOrderXPO.BuyFromCountry,
                                                    BuyFromCity = _locPurchaseOrderXPO.BuyFromCity,
                                                    BuyFromAddress = _locPurchaseOrderXPO.BuyFromAddress,
                                                    ProjectHeader = _locPurchaseOrderXPO.ProjectHeader,
                                                    SignCode = _locPurchaseOrderXPO.Code,
                                                    PurchaseOrder = _locPurchaseOrderXPO,
                                                };
                                                _saveDataPReturn.Save();
                                                _saveDataPReturn.Session.CommitTransaction();

                                                XPCollection<PurchaseOrderLine> _numLinePurchaseOrderLines = new XPCollection<PurchaseOrderLine>(_currSession,
                                                                                                             new GroupOperator(GroupOperatorType.And,
                                                                                                             new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO)));

                                                PurchaseReturn _locPurchaseReturn2 = _currSession.FindObject<PurchaseReturn>
                                                                                     (new GroupOperator(GroupOperatorType.And,
                                                                                      new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO)));
                                                if (_locPurchaseReturn2 != null)
                                                {
                                                    if (_numLinePurchaseOrderLines != null && _numLinePurchaseOrderLines.Count > 0)
                                                    {
                                                        foreach (PurchaseOrderLine _numLinePurchaseOrderLine in _numLinePurchaseOrderLines)
                                                        {
                                                            if (_numLinePurchaseOrderLine.Status == Status.Progress || _numLinePurchaseOrderLine.Status == Status.Posted)
                                                            {
                                                                PurchaseReturnLine _saveDataPurchaseReturnLine = new PurchaseReturnLine(_currSession)
                                                                {
                                                                    Item = _numLinePurchaseOrderLine.Item,
                                                                    MxDQty = _numLinePurchaseOrderLine.MxDQty,
                                                                    MxDUOM = _numLinePurchaseOrderLine.MxDUOM,
                                                                    MxTQty = _numLinePurchaseOrderLine.MxTQty,
                                                                    MxUOM = _numLinePurchaseOrderLine.MxUOM,
                                                                    DQty = _numLinePurchaseOrderLine.DQty,
                                                                    DUOM = _numLinePurchaseOrderLine.DUOM,
                                                                    Qty = _numLinePurchaseOrderLine.Qty,
                                                                    UOM = _numLinePurchaseOrderLine.UOM,
                                                                    TQty = _numLinePurchaseOrderLine.TQty,
                                                                    UAmount = _numLinePurchaseOrderLine.UAmount,
                                                                    TUAmount = _numLinePurchaseOrderLine.TUAmount,
                                                                    EstimatedDate = _numLinePurchaseOrderLine.EstimatedDate,
                                                                    StatusDate = _numLinePurchaseOrderLine.StatusDate,
                                                                    PurchaseReturn = _locPurchaseReturn2,
                                                                };
                                                                _saveDataPurchaseReturnLine.Save();
                                                                _saveDataPurchaseReturnLine.Session.CommitTransaction();
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        ErrorMessageShow("Data Purchase Return Not Available");
                                                    }
                                                }
                                                else
                                                {
                                                    ErrorMessageShow("Data Purchase Return Not Available");
                                                }
                                            }
                                        }
                                        SuccessMessageShow("Purchase Return has been successfully post");
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data Purchase Return Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Purchase Return Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseReturn " + ex.ToString());
            }
        }

        private void PurchaseOrderListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(PurchaseOrder)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseOrder " + ex.ToString());
            }
        }

        private void PurchaseOrderListviewFilterApprovalSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(PurchaseOrder)))
                {
                    if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.Approval)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("ActiveApproved3", false, BinaryOperatorType.Equal),
                                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("ActiveApproved1", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", true, BinaryOperatorType.Equal)));
                    }
                    else if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.EndApproval)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new BinaryOperator("ActiveApproved3", true, BinaryOperatorType.Equal);
                    }
                    else if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.AllData)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("ActiveApproved1", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved3", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved1", false, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", false, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved3", false, BinaryOperatorType.Equal));
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseOrder " + ex.ToString());
            }
        }

        private void PurchaseOrderApprovalAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                ApplicationSetupDetail _locAppSetDetail = null;
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                var User = SecuritySystem.CurrentUserName;
                Session _currentSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currentSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                UserAccess _locUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", User));

                foreach (Object obj in _objectsToProcess)
                {
                    PurchaseOrder _objInNewObjectSpace = (PurchaseOrder)_objectSpace.GetObject(obj);

                    if (_objInNewObjectSpace != null)
                    {
                        if (_objInNewObjectSpace.Code != null)
                        {
                            _currObjectId = _objInNewObjectSpace.Code;
                        }
                    }

                    if (_currObjectId != null)
                    {
                        PurchaseOrder _locPurchaseOrderXPO = _currentSession.FindObject<PurchaseOrder>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("Code", _currObjectId)));

                        if (_locPurchaseOrderXPO != null)
                        {
                            if (_locPurchaseOrderXPO.Status == Status.Progress)
                            {
                                ApprovalLine _locApprovalLine = _currentSession.FindObject<ApprovalLine>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO),
                                                                             new BinaryOperator("EndApproval", true)));
                                if (_locApprovalLine == null)
                                {
                                    #region Approval Level 1
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level1)
                                    {
                                        _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level1, ObjectList.PurchaseOrder);
                                        if (_locAppSetDetail != null)
                                        {
                                            //Buat bs input langsung ke approvalline
                                            ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level1)));
                                            if (_locApprovalLineXPO == null)
                                            {

                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level1,
                                                        EndApproval = true,
                                                        PurchaseOrder = _locPurchaseOrderXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locPurchaseOrderXPO.ActivationPosting = true;
                                                    _locPurchaseOrderXPO.ActiveApproved1 = false;
                                                    _locPurchaseOrderXPO.ActiveApproved2 = false;
                                                    _locPurchaseOrderXPO.ActiveApproved3 = true;
                                                    _locPurchaseOrderXPO.Save();
                                                    _locPurchaseOrderXPO.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level1,
                                                        PurchaseOrder = _locPurchaseOrderXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locPurchaseOrderXPO.ActiveApproved1 = true;
                                                    _locPurchaseOrderXPO.ActiveApproved2 = false;
                                                    _locPurchaseOrderXPO.ActiveApproved3 = false;
                                                    _locPurchaseOrderXPO.Save();
                                                    _locPurchaseOrderXPO.Session.CommitTransaction();
                                                }

                                                //Send Email
                                                ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO)));

                                                if (_locApprovalLineXPO2 != null)
                                                {
                                                    SendEmail(_currentSession, _locPurchaseOrderXPO, Status.Progress, _locApprovalLineXPO2);
                                                }

                                                SuccessMessageShow("Purchase Order has successfully Approve");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 1

                                    #region Approval Level 2
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level2)
                                    {
                                        _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level2, ObjectList.PurchaseOrder);

                                        if (_locAppSetDetail != null)
                                        {
                                            ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level2)));

                                            if (_locApprovalLineXPO == null)
                                            {
                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level2,
                                                        EndApproval = true,
                                                        PurchaseOrder = _locPurchaseOrderXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locPurchaseOrderXPO.ActivationPosting = true;
                                                    _locPurchaseOrderXPO.ActiveApproved1 = false;
                                                    _locPurchaseOrderXPO.ActiveApproved2 = false;
                                                    _locPurchaseOrderXPO.ActiveApproved3 = true;
                                                    _locPurchaseOrderXPO.Save();
                                                    _locPurchaseOrderXPO.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level2,
                                                        PurchaseOrder = _locPurchaseOrderXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    SetApprovalLine(_currentSession, _locPurchaseOrderXPO, ApprovalLevel.Level1);

                                                    _locPurchaseOrderXPO.ActiveApproved1 = false;
                                                    _locPurchaseOrderXPO.ActiveApproved2 = true;
                                                    _locPurchaseOrderXPO.ActiveApproved3 = false;
                                                    _locPurchaseOrderXPO.Save();
                                                    _locPurchaseOrderXPO.Session.CommitTransaction();
                                                }

                                                //Send Email
                                                ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO)));

                                                if (_locApprovalLineXPO2 != null)
                                                {
                                                    SendEmail(_currentSession, _locPurchaseOrderXPO, Status.Progress, _locApprovalLineXPO2);
                                                }

                                                SuccessMessageShow("Purchase Order has successfully Approve");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 2

                                    #region Approval Level 3
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level3)
                                    {
                                        _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level3, ObjectList.PurchaseOrder);

                                        if (_locAppSetDetail != null)
                                        {
                                            ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level3)));

                                            if (_locApprovalLineXPO == null)
                                            {
                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level3,
                                                        EndApproval = true,
                                                        PurchaseOrder = _locPurchaseOrderXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locPurchaseOrderXPO.ActivationPosting = true;
                                                    _locPurchaseOrderXPO.ActiveApproved1 = false;
                                                    _locPurchaseOrderXPO.ActiveApproved2 = false;
                                                    _locPurchaseOrderXPO.ActiveApproved3 = true;
                                                    _locPurchaseOrderXPO.Save();
                                                    _locPurchaseOrderXPO.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level3,
                                                        PurchaseOrder = _locPurchaseOrderXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    SetApprovalLine(_currentSession, _locPurchaseOrderXPO, ApprovalLevel.Level2);
                                                    SetApprovalLine(_currentSession, _locPurchaseOrderXPO, ApprovalLevel.Level1);

                                                    _locPurchaseOrderXPO.ActiveApproved1 = false;
                                                    _locPurchaseOrderXPO.ActiveApproved2 = false;
                                                    _locPurchaseOrderXPO.ActiveApproved3 = true;
                                                    _locPurchaseOrderXPO.Save();
                                                    _locPurchaseOrderXPO.Session.CommitTransaction();
                                                }

                                                //Send Email
                                                ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO)));

                                                if (_locApprovalLineXPO2 != null)
                                                {
                                                    SendEmail(_currentSession, _locPurchaseOrderXPO, Status.Progress, _locApprovalLineXPO2);
                                                }

                                                SuccessMessageShow("Purchase Order has successfully Approve");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 3
                                }

                            }
                            else
                            {
                                ErrorMessageShow("Purchase Order Status Not Available");
                            }
                        }
                        else
                        {
                            ErrorMessageShow("Purchase Order Not Available");
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseOrder " + ex.ToString());
            }
        }      

        #region GetPR

        private void GetPurchaseRequisitionMonitoring(Session _currSession, PurchaseRequisition _locPurchaseRequisitionXPO, PurchaseOrder _locPurchaseOrderXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _currSignCode = null;

                if (_locPurchaseRequisitionXPO != null && _locPurchaseOrderXPO != null)
                {
                    //Hanya memindahkan PurchaseRequisitionMonitoring ke PurchaseOrderLine
                    XPCollection<PurchaseRequisitionMonitoring> _locPurchaseRequisitionMonitorings = new XPCollection<PurchaseRequisitionMonitoring>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("PurchaseRequisition", _locPurchaseRequisitionXPO),
                                                                                        new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO),
                                                                                        new BinaryOperator("Select", true),
                                                                                        new BinaryOperator("PostedCount", 0)));

                    if (_locPurchaseRequisitionMonitorings != null && _locPurchaseRequisitionMonitorings.Count() > 0)
                    {
                        foreach (PurchaseRequisitionMonitoring _locPurchaseRequisitionMonitoring in _locPurchaseRequisitionMonitorings)
                        {
                            if (_locPurchaseRequisitionMonitoring.PurchaseRequisitionLine != null)
                            {
                                _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.PurchaseOrderLine);

                                if (_currSignCode != null)
                                {
                                    PurchaseOrderLine _saveDataPurchaseOrderLine = new PurchaseOrderLine(_currSession)
                                    {
                                        SignCode = _currSignCode,
                                        PurchaseType = _locPurchaseRequisitionMonitoring.PurchaseRequisitionLine.PurchaseType,
                                        Item = _locPurchaseRequisitionMonitoring.Item,
                                        Description = _locPurchaseRequisitionMonitoring.PurchaseRequisitionLine.Description,
                                        MxDQty = _locPurchaseRequisitionMonitoring.DQty,
                                        MxDUOM = _locPurchaseRequisitionMonitoring.DUOM,
                                        MxQty = _locPurchaseRequisitionMonitoring.Qty,
                                        MxUOM = _locPurchaseRequisitionMonitoring.UOM,
                                        MxTQty = _locPurchaseRequisitionMonitoring.TQty,
                                        DQty = _locPurchaseRequisitionMonitoring.DQty,
                                        DUOM = _locPurchaseRequisitionMonitoring.DUOM,
                                        Qty = _locPurchaseRequisitionMonitoring.Qty,
                                        UOM = _locPurchaseRequisitionMonitoring.UOM,
                                        TQty = _locPurchaseRequisitionMonitoring.TQty,
                                        PurchaseRequisitionMonitoring = _locPurchaseRequisitionMonitoring,
                                        PurchaseOrder = _locPurchaseOrderXPO,
                                    };
                                    _saveDataPurchaseOrderLine.Save();
                                    _saveDataPurchaseOrderLine.Session.CommitTransaction();

                                    //10-06-2020
                                    PurchaseOrderLine _locPurchaseOrderLine = _currSession.FindObject<PurchaseOrderLine>
                                                                (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("SignCode", _currSignCode)));
                                    if (_locPurchaseOrderLine != null)
                                    {
                                        _locPurchaseRequisitionMonitoring.PurchaseOrderLine = _locPurchaseOrderLine;
                                        _locPurchaseRequisitionMonitoring.Status = Status.Close;
                                        _locPurchaseRequisitionMonitoring.StatusDate = now;
                                        _locPurchaseRequisitionMonitoring.PostedCount = _locPurchaseRequisitionMonitoring.PostedCount + 1;
                                        _locPurchaseRequisitionMonitoring.Save();
                                        _locPurchaseRequisitionMonitoring.Session.CommitTransaction();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PurchaseOrder ", ex.ToString());
            }
        }

        #endregion GetPR

        #region PostingMethod

        private void SetPurchaseOrderMonitoring(Session _currSession, PurchaseOrder _locPurchaseOrderXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;

                if (_locPurchaseOrderXPO != null)
                {
                    if (_locPurchaseOrderXPO.Status == Status.Progress || _locPurchaseOrderXPO.Status == Status.Posted)
                    {
                        XPCollection<PurchaseOrderLine> _locPurchaseOrderLines = new XPCollection<PurchaseOrderLine>
                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Progress),
                                                            new BinaryOperator("Status", Status.Posted))));

                        if (_locPurchaseOrderLines != null && _locPurchaseOrderLines.Count() > 0)
                        {
                            foreach (PurchaseOrderLine _locPurchaseOrderLine in _locPurchaseOrderLines)
                            {
                                PurchaseOrderMonitoring _saveDataPurchaseOrderMonitoring = new PurchaseOrderMonitoring(_currSession)
                                {
                                    PurchaseOrder = _locPurchaseOrderXPO,
                                    PurchaseOrderLine = _locPurchaseOrderLine,
                                    PurchaseType = _locPurchaseOrderLine.PurchaseType,
                                    Item = _locPurchaseOrderLine.Item,
                                    DQty = _locPurchaseOrderLine.DQty,
                                    DUOM = _locPurchaseOrderLine.DUOM,
                                    Qty = _locPurchaseOrderLine.Qty,
                                    UOM = _locPurchaseOrderLine.UOM,
                                    TQty = _locPurchaseOrderLine.TQty,
                                    Currency = _locPurchaseOrderLine.Currency,
                                    UAmount = _locPurchaseOrderLine.UAmount,
                                    TUAmount = _locPurchaseOrderLine.TUAmount,
                                    MultiTax = _locPurchaseOrderLine.MultiTax,
                                    Tax = _locPurchaseOrderLine.Tax,
                                    TxValue = _locPurchaseOrderLine.TxValue,
                                    TxAmount = _locPurchaseOrderLine.TxAmount,
                                    MultiDiscount = _locPurchaseOrderLine.MultiDiscount,
                                    Discount = _locPurchaseOrderLine.Discount,
                                    Disc = _locPurchaseOrderLine.Disc,
                                    DiscAmount = _locPurchaseOrderLine.DiscAmount,
                                    TAmount = _locPurchaseOrderLine.TAmount,
                                };
                                _saveDataPurchaseOrderMonitoring.Save();
                                _saveDataPurchaseOrderMonitoring.Session.CommitTransaction();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PurchaseOrder ", ex.ToString());
            }
        }

        private void SetRemainQty(Session _currSession, PurchaseOrder _locPurchaseOrderXPO)
        {
            try
            {
                if (_locPurchaseOrderXPO != null)
                {
                    XPCollection<PurchaseOrderLine> _locPurchaseOrderLines = new XPCollection<PurchaseOrderLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locPurchaseOrderLines != null && _locPurchaseOrderLines.Count > 0)
                    {
                        double _locRmDQty = 0;
                        double _locRmQty = 0;
                        double _locInvLineTotal = 0;
                        ItemUnitOfMeasure _locItemUOM = null;

                        foreach (PurchaseOrderLine _locPurchaseOrderLine in _locPurchaseOrderLines)
                        {
                            #region ProcessCount=0
                            if (_locPurchaseOrderLine.PostedCount == 0)
                            {
                                #region MaxQuantity
                                if (_locPurchaseOrderLine.MxDQty > 0)
                                {
                                    if (_locPurchaseOrderLine.DQty > 0 && _locPurchaseOrderLine.DQty <= _locPurchaseOrderLine.MxDQty)
                                    {
                                        _locRmDQty = _locPurchaseOrderLine.MxDQty - _locPurchaseOrderLine.DQty;
                                    }

                                    if (_locPurchaseOrderLine.Qty > 0 && _locPurchaseOrderLine.Qty <= _locPurchaseOrderLine.MxQty)
                                    {
                                        _locRmQty = _locPurchaseOrderLine.MxQty - _locPurchaseOrderLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locPurchaseOrderLine.Item),
                                                                new BinaryOperator("UOM", _locPurchaseOrderLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locPurchaseOrderLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty + _locRmDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                #endregion MaxQuantity
                                #region NonMaxQuantity
                                else
                                {
                                    if (_locPurchaseOrderLine.DQty > 0)
                                    {
                                        _locRmDQty = 0;
                                    }

                                    if (_locPurchaseOrderLine.Qty > 0)
                                    {
                                        _locRmQty = 0;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locPurchaseOrderLine.Item),
                                                                new BinaryOperator("UOM", _locPurchaseOrderLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locPurchaseOrderLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty + _locRmDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion ProcessCount=0

                            #region ProcessCount>0
                            if (_locPurchaseOrderLine.PostedCount > 0)
                            {
                                if (_locPurchaseOrderLine.RmDQty > 0)
                                {
                                    _locRmDQty = _locPurchaseOrderLine.RmDQty - _locPurchaseOrderLine.DQty;
                                }

                                if (_locPurchaseOrderLine.RmQty > 0)
                                {
                                    _locRmQty = _locPurchaseOrderLine.RmQty - _locPurchaseOrderLine.Qty;
                                }

                                if (_locPurchaseOrderLine.MxDQty > 0 || _locPurchaseOrderLine.MxQty > 0)
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locPurchaseOrderLine.Item),
                                                                new BinaryOperator("UOM", _locPurchaseOrderLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locPurchaseOrderLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                }
                                else
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locPurchaseOrderLine.Item),
                                                                new BinaryOperator("UOM", _locPurchaseOrderLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locPurchaseOrderLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                }


                                if (_locItemUOM != null)
                                {
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locRmQty + _locRmDQty;
                                }

                            }
                            #endregion ProcessCount>0

                            _locPurchaseOrderLine.RmDQty = _locRmDQty;
                            _locPurchaseOrderLine.RmQty = _locRmQty;
                            _locPurchaseOrderLine.RmTQty = _locInvLineTotal;
                            _locPurchaseOrderLine.Save();
                            _locPurchaseOrderLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseOrder " + ex.ToString());
            }
        }

        private void SetPostingQty(Session _currSession, PurchaseOrder _locPurchaseOrderXPO)
        {
            try
            {
                if (_locPurchaseOrderXPO != null)
                {
                    XPCollection<PurchaseOrderLine> _locPurchaseOrderLines = new XPCollection<PurchaseOrderLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locPurchaseOrderLines != null && _locPurchaseOrderLines.Count > 0)
                    {
                        double _locPDQty = 0;
                        double _locPQty = 0;
                        double _locInvLineTotal = 0;
                        ItemUnitOfMeasure _locItemUOM = null;

                        foreach (PurchaseOrderLine _locPurchaseOrderLine in _locPurchaseOrderLines)
                        {
                            #region ProcessCount=0
                            if (_locPurchaseOrderLine.PostedCount == 0)
                            {
                                #region MaxQuantity
                                if (_locPurchaseOrderLine.MxDQty > 0)
                                {
                                    if (_locPurchaseOrderLine.DQty > 0 && _locPurchaseOrderLine.DQty <= _locPurchaseOrderLine.MxDQty)
                                    {
                                        _locPDQty = _locPurchaseOrderLine.DQty;
                                    }

                                    if (_locPurchaseOrderLine.Qty > 0 && _locPurchaseOrderLine.Qty <= _locPurchaseOrderLine.MxQty)
                                    {
                                        _locPQty = _locPurchaseOrderLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locPurchaseOrderLine.Item),
                                                                new BinaryOperator("UOM", _locPurchaseOrderLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locPurchaseOrderLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty + _locPDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }

                                }
                                #endregion MaxQuantity
                                #region NonMaxQuantity
                                else
                                {
                                    if (_locPurchaseOrderLine.DQty > 0)
                                    {
                                        _locPDQty = _locPurchaseOrderLine.DQty;
                                    }

                                    if (_locPurchaseOrderLine.Qty > 0)
                                    {
                                        _locPQty = _locPurchaseOrderLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locPurchaseOrderLine.Item),
                                                                new BinaryOperator("UOM", _locPurchaseOrderLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locPurchaseOrderLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty + _locPDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion ProcessCount=0

                            #region ProcessCount>0
                            if (_locPurchaseOrderLine.PostedCount > 0)
                            {
                                if (_locPurchaseOrderLine.PDQty > 0)
                                {
                                    _locPDQty = _locPurchaseOrderLine.PDQty + _locPurchaseOrderLine.DQty;
                                }

                                if (_locPurchaseOrderLine.PQty > 0)
                                {
                                    _locPQty = _locPurchaseOrderLine.PQty + _locPurchaseOrderLine.Qty;
                                }

                                if (_locPurchaseOrderLine.MxDQty > 0 || _locPurchaseOrderLine.MxQty > 0)
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                           (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Item", _locPurchaseOrderLine.Item),
                                                            new BinaryOperator("UOM", _locPurchaseOrderLine.MxUOM),
                                                            new BinaryOperator("DefaultUOM", _locPurchaseOrderLine.MxDUOM),
                                                            new BinaryOperator("Active", true)));
                                }
                                else
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                           (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Item", _locPurchaseOrderLine.Item),
                                                            new BinaryOperator("UOM", _locPurchaseOrderLine.UOM),
                                                            new BinaryOperator("DefaultUOM", _locPurchaseOrderLine.DUOM),
                                                            new BinaryOperator("Active", true)));
                                }

                                if (_locItemUOM != null)
                                {
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locPQty + _locPDQty;
                                }

                            }
                            #endregion ProcessCount>0

                            _locPurchaseOrderLine.PDQty = _locPDQty;
                            _locPurchaseOrderLine.PDUOM = _locPurchaseOrderLine.DUOM;
                            _locPurchaseOrderLine.PQty = _locPQty;
                            _locPurchaseOrderLine.PUOM = _locPurchaseOrderLine.UOM;
                            _locPurchaseOrderLine.PTQty = _locInvLineTotal;
                            _locPurchaseOrderLine.Save();
                            _locPurchaseOrderLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseOrder " + ex.ToString());
            }
        }

        private void SetProcessCount(Session _currSession, PurchaseOrder _locPurchaseOrderXPO)
        {
            try
            {
                if (_locPurchaseOrderXPO != null)
                {
                    XPCollection<PurchaseOrderLine> _locPurchaseOrderLines = new XPCollection<PurchaseOrderLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locPurchaseOrderLines != null && _locPurchaseOrderLines.Count > 0)
                    {

                        foreach (PurchaseOrderLine _locPurchaseOrderLine in _locPurchaseOrderLines)
                        {
                            if (_locPurchaseOrderLine.Status == Status.Progress || _locPurchaseOrderLine.Status == Status.Posted)
                            {
                                _locPurchaseOrderLine.PostedCount = _locPurchaseOrderLine.PostedCount + 1;
                                _locPurchaseOrderLine.Save();
                                _locPurchaseOrderLine.Session.CommitTransaction();
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrder " + ex.ToString());
            }
        }

        private void SetStatusPurchaseOrderLine(Session _currSession, PurchaseOrder _locPurchaseOrderXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locPurchaseOrderXPO != null)
                {
                    XPCollection<PurchaseOrderLine> _locPurchaseOrderLines = new XPCollection<PurchaseOrderLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locPurchaseOrderLines != null && _locPurchaseOrderLines.Count > 0)
                    {

                        foreach (PurchaseOrderLine _locPurchaseOrderLine in _locPurchaseOrderLines)
                        {
                            if (_locPurchaseOrderLine.Status == Status.Progress || _locPurchaseOrderLine.Status == Status.Posted)
                            {
                                if (_locPurchaseOrderLine.RmDQty == 0 && _locPurchaseOrderLine.RmQty == 0 && _locPurchaseOrderLine.RmTQty == 0)
                                {
                                    _locPurchaseOrderLine.Status = Status.Close;
                                    _locPurchaseOrderLine.ActivationQuantity = true;
                                    _locPurchaseOrderLine.ActivationPosting = true;
                                    _locPurchaseOrderLine.StatusDate = now;
                                }
                                else
                                {
                                    _locPurchaseOrderLine.Status = Status.Posted;
                                    _locPurchaseOrderLine.StatusDate = now;
                                }
                                _locPurchaseOrderLine.Save();
                                _locPurchaseOrderLine.Session.CommitTransaction();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseOrder " + ex.ToString());
            }
        }

        private void SetNormalQuantity(Session _currSession, PurchaseOrder _locPurchaseOrderXPO)
        {
            try
            {
                if (_locPurchaseOrderXPO != null)
                {
                    XPCollection<PurchaseOrderLine> _locPurchaseOrderLines = new XPCollection<PurchaseOrderLine>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO),
                                                                                     new BinaryOperator("Select", true)));

                    if (_locPurchaseOrderLines != null && _locPurchaseOrderLines.Count > 0)
                    {
                        foreach (PurchaseOrderLine _locPurchaseOrderLine in _locPurchaseOrderLines)
                        {
                            if (_locPurchaseOrderLine.Status == Status.Progress || _locPurchaseOrderLine.Status == Status.Posted || _locPurchaseOrderLine.Status == Status.Close)
                            {
                                if (_locPurchaseOrderLine.DQty > 0 || _locPurchaseOrderLine.Qty > 0)
                                {
                                    _locPurchaseOrderLine.Select = false;
                                    _locPurchaseOrderLine.DQty = 0;
                                    _locPurchaseOrderLine.Qty = 0;
                                    _locPurchaseOrderLine.Save();
                                    _locPurchaseOrderLine.Session.CommitTransaction();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseOrder " + ex.ToString());
            }
        }

        private void SetFinalStatusSalesOrder(Session _currSession, PurchaseOrder _locPurchaseOrderXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locStatusCount = 0;
                int _locPurchaseOrderLineCount = 0;

                if (_locPurchaseOrderXPO != null)
                {
                    XPCollection<PurchaseOrderLine> _locPurchaseOrderLines = new XPCollection<PurchaseOrderLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO)));

                    if (_locPurchaseOrderLines != null && _locPurchaseOrderLines.Count() > 0)
                    {
                        _locPurchaseOrderLineCount = _locPurchaseOrderLines.Count();

                        foreach (PurchaseOrderLine _locPurchaseOrderLine in _locPurchaseOrderLines)
                        {
                            if (_locPurchaseOrderLine.Status == Status.Close)
                            {
                                _locStatusCount = _locStatusCount + 1;
                            }
                        }
                    }

                    if (_locStatusCount == _locPurchaseOrderLineCount)
                    {
                        _locPurchaseOrderXPO.ActivationPosting = true;
                        _locPurchaseOrderXPO.Status = Status.Posted;
                        _locPurchaseOrderXPO.StatusDate = now;
                        _locPurchaseOrderXPO.Save();
                        _locPurchaseOrderXPO.Session.CommitTransaction();
                    }
                    else
                    {
                        _locPurchaseOrderXPO.Status = Status.Posted;
                        _locPurchaseOrderXPO.StatusDate = now;
                        _locPurchaseOrderXPO.Save();
                        _locPurchaseOrderXPO.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseOrder " + ex.ToString());
            }
        }

        private void SetMaksPay(Session _currSession, PurchaseOrder _locPurchaseOrderXPO)
        {
            try
            {
                double _locTxValue = 0;
                double _locTxAmount = 0;
                double _locDisc = 0;
                double _locDiscAmount = 0;
                double _locTAmount = 0;
                double _locGTAmount = 0;

                if (_locPurchaseOrderXPO != null)
                {
                    XPCollection<PurchaseOrderLine> _locPurchaseOrderLines = new XPCollection<PurchaseOrderLine>
                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO)));

                    if (_locPurchaseOrderLines != null && _locPurchaseOrderLines.Count() > 0)
                    {
                        foreach (PurchaseOrderLine _locPurchaseOrderLine in _locPurchaseOrderLines)
                        {
                            //Count Tax
                            if (_locPurchaseOrderLine.MultiTax == true)
                            {
                                _locTxAmount = GetSumTotalTaxAmount(_currSession, _locPurchaseOrderLine);
                            }
                            else
                            {
                                if (_locPurchaseOrderLine.TxValue > 0)
                                {
                                    _locTxValue = _locPurchaseOrderLine.TxValue;
                                    _locTxAmount = (_locTxValue / 100) * (_locPurchaseOrderLine.MxTUAmount);
                                }
                            }

                            //Count Discount
                            if (_locPurchaseOrderLine.MultiDiscount == true)
                            {
                                _locDiscAmount = GetSumTotalDiscAmount(_currSession, _locPurchaseOrderLine);
                            }
                            else
                            {
                                if (_locPurchaseOrderLine.Disc > 0)
                                {
                                    _locDisc = _locPurchaseOrderLine.Disc;
                                    _locDiscAmount = (_locDisc / 100) * (_locPurchaseOrderLine.MxTUAmount);
                                }
                            }

                            _locTAmount = (_locPurchaseOrderLine.MxTUAmount + _locTxAmount) - _locDiscAmount;

                            _locGTAmount = _locGTAmount + _locTAmount;
                        }

                        if (_locGTAmount > 0)
                        {
                            _locPurchaseOrderXPO.MaxPay = _locGTAmount;
                            _locPurchaseOrderXPO.Save();
                            _locPurchaseOrderXPO.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseOrder " + ex.ToString());
            }
        }

        private double GetSumTotalTaxAmount(Session _currSession, PurchaseOrderLine _purchaseOrderLineXPO)
        {
            double _return = 0;
            try
            {
                double _locTxAmount = 0;

                if (_purchaseOrderLineXPO != null)
                {
                    XPCollection<TaxLine> _locTaxLines = new XPCollection<TaxLine>(_currSession,
                                                     new BinaryOperator("PurchaseOrderLine", _purchaseOrderLineXPO));

                    if (_locTaxLines.Count() > 0)
                    {
                        foreach (TaxLine _locTaxLine in _locTaxLines)
                        {
                            if (_locTaxLine.TxValue > 0)
                            {
                                if (_locTaxLine.TaxNature == TaxNature.Increase)
                                {
                                    _locTxAmount = _locTxAmount + (_purchaseOrderLineXPO.MxTUAmount * _locTaxLine.TxValue / 100);
                                }
                                else if (_locTaxLine.TaxNature == TaxNature.Decrease)
                                {
                                    _locTxAmount = _locTxAmount - (_purchaseOrderLineXPO.MxTUAmount * _locTaxLine.TxValue / 100);
                                }
                            }
                        }
                        _return = _locTxAmount;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PurchaseOrder" + ex.ToString());
            }
            return _return;
        }

        private double GetSumTotalDiscAmount(Session _currSession, PurchaseOrderLine _purchaseOrderLineXPO)
        {
            double _result = 0;
            try
            {
                double _locDiscAmount = 0;

                if (_purchaseOrderLineXPO != null)
                {
                    XPCollection<DiscountLine> _locDiscountLines = new XPCollection<DiscountLine>(_currSession,
                                                               new BinaryOperator("PurchaseOrderLine", _purchaseOrderLineXPO));

                    if (_locDiscountLines.Count() > 0)
                    {
                        foreach (DiscountLine _locDiscountLine in _locDiscountLines)
                        {
                            if (_locDiscountLine.Disc > 0)
                            {
                                _locDiscAmount = _locDiscAmount + (_purchaseOrderLineXPO.MxTUAmount * _locDiscountLine.Disc / 100);
                            }
                        }
                        _result = _locDiscAmount;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PurchaseOrder" + ex.ToString());
            }
            return _result;
        }

        #endregion PostingMethod

        #region GetPROld

        private void SetRemainQuantityOld(Session _currSession, PurchaseRequisitionLine _locPurchaseRequisitionLine)
        {
            try
            {
                DateTime now = DateTime.Now;

                #region Remain Qty  with PostedCount != 0
                if (_locPurchaseRequisitionLine.PostedCount > 0)
                {
                    double _locRmDqty = 0;
                    double _locRmQty = 0;
                    double _locInvLineTotal = 0;
                    double _locRmTQty = 0;
                    bool _locActivationPosting = false;
                    Status _locStatus = Status.Posted;

                    if (_locPurchaseRequisitionLine.RmDQty > 0 || _locPurchaseRequisitionLine.RmQty > 0 && _locPurchaseRequisitionLine.MxTQty != _locPurchaseRequisitionLine.RmTQty)
                    {
                        _locRmDqty = _locPurchaseRequisitionLine.RmDQty - _locPurchaseRequisitionLine.DQty;
                        _locRmQty = _locPurchaseRequisitionLine.RmQty - _locPurchaseRequisitionLine.Qty;

                        if (_locPurchaseRequisitionLine.Item != null && _locPurchaseRequisitionLine.MxUOM != null && _locPurchaseRequisitionLine.MxDUOM != null)
                        {
                            ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                    (new GroupOperator(GroupOperatorType.And,
                                                     new BinaryOperator("Item", _locPurchaseRequisitionLine.Item),
                                                     new BinaryOperator("UOM", _locPurchaseRequisitionLine.MxUOM),
                                                     new BinaryOperator("DefaultUOM", _locPurchaseRequisitionLine.MxDUOM),
                                                     new BinaryOperator("Active", true)));
                            if (_locItemUOM != null)
                            {
                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDqty;
                                }
                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDqty;
                                }
                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty + _locRmDqty;
                                }
                            }
                        }
                        else
                        {
                            _locInvLineTotal = _locRmQty + _locRmDqty;
                        }

                        if (_locRmDqty > 0 || _locRmQty > 0)
                        {
                            _locRmTQty = _locPurchaseRequisitionLine.RmTQty - _locPurchaseRequisitionLine.TQty;
                        }
                        else
                        {
                            _locRmTQty = _locPurchaseRequisitionLine.RmTQty - _locPurchaseRequisitionLine.TQty;
                            _locActivationPosting = true;
                            _locStatus = Status.Close;
                        }


                        _locPurchaseRequisitionLine.ActivationPosting = _locActivationPosting;
                        _locPurchaseRequisitionLine.Select = false;
                        _locPurchaseRequisitionLine.RmDQty = _locRmDqty;
                        _locPurchaseRequisitionLine.RmQty = _locRmQty;
                        _locPurchaseRequisitionLine.RmTQty = _locRmTQty;
                        _locPurchaseRequisitionLine.PostedCount = _locPurchaseRequisitionLine.PostedCount + 1;
                        _locPurchaseRequisitionLine.DQty = 0;
                        _locPurchaseRequisitionLine.Qty = 0;
                        _locPurchaseRequisitionLine.TQty = 0;
                        _locPurchaseRequisitionLine.Status = _locStatus;
                        _locPurchaseRequisitionLine.StatusDate = now;
                        _locPurchaseRequisitionLine.Save();
                        _locPurchaseRequisitionLine.Session.CommitTransaction();
                    }
                }
                #endregion Remain Qty  with PostedCount != 0

                #region Remain Qty with PostedCount == 0
                else
                {
                    double _locRmDqty = 0;
                    double _locRmQty = 0;
                    double _locInvLineTotal = 0;
                    double _locRmTQty = 0;
                    _locRmDqty = _locPurchaseRequisitionLine.MxDQty - _locPurchaseRequisitionLine.DQty;
                    _locRmQty = _locPurchaseRequisitionLine.MxQty - _locPurchaseRequisitionLine.Qty;

                    #region Remain > 0
                    if (_locRmDqty > 0 || _locRmQty > 0)
                    {
                        if (_locPurchaseRequisitionLine.Item != null && _locPurchaseRequisitionLine.MxUOM != null && _locPurchaseRequisitionLine.MxDUOM != null)
                        {
                            ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                    (new GroupOperator(GroupOperatorType.And,
                                                     new BinaryOperator("Item", _locPurchaseRequisitionLine.Item),
                                                     new BinaryOperator("UOM", _locPurchaseRequisitionLine.MxUOM),
                                                     new BinaryOperator("DefaultUOM", _locPurchaseRequisitionLine.MxDUOM),
                                                     new BinaryOperator("Active", true)));
                            if (_locItemUOM != null)
                            {
                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDqty;
                                }
                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDqty;
                                }
                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty + _locRmDqty;
                                }
                            }
                        }
                        else
                        {
                            _locInvLineTotal = _locRmQty + _locRmDqty;
                        }

                        if (_locInvLineTotal > 0)
                        {
                            if (_locPurchaseRequisitionLine.RmTQty > 0)
                            {
                                _locRmTQty = _locPurchaseRequisitionLine.RmTQty - _locInvLineTotal;
                            }
                            else
                            {
                                _locRmTQty = _locInvLineTotal;
                            }

                        }

                        _locPurchaseRequisitionLine.RmDQty = _locRmDqty;
                        _locPurchaseRequisitionLine.RmQty = _locRmQty;
                        _locPurchaseRequisitionLine.RmTQty = _locRmTQty;
                        _locPurchaseRequisitionLine.PostedCount = _locPurchaseRequisitionLine.PostedCount + 1;
                        _locPurchaseRequisitionLine.Select = false;
                        _locPurchaseRequisitionLine.DQty = 0;
                        _locPurchaseRequisitionLine.Qty = 0;
                        _locPurchaseRequisitionLine.TQty = 0;
                        _locPurchaseRequisitionLine.Status = Status.Posted;
                        _locPurchaseRequisitionLine.StatusDate = now;
                        _locPurchaseRequisitionLine.Save();
                        _locPurchaseRequisitionLine.Session.CommitTransaction();
                    }
                    #endregion Remain > 0

                    #region Remain <= 0
                    if (_locRmDqty <= 0 && _locRmQty <= 0)
                    {
                        _locPurchaseRequisitionLine.ActivationPosting = true;
                        _locPurchaseRequisitionLine.RmDQty = 0;
                        _locPurchaseRequisitionLine.RmQty = 0;
                        _locPurchaseRequisitionLine.RmTQty = 0;
                        _locPurchaseRequisitionLine.PostedCount = _locPurchaseRequisitionLine.PostedCount + 1;
                        _locPurchaseRequisitionLine.Select = false;
                        _locPurchaseRequisitionLine.DQty = 0;
                        _locPurchaseRequisitionLine.Qty = 0;
                        _locPurchaseRequisitionLine.TQty = 0;
                        _locPurchaseRequisitionLine.Status = Status.Lock;
                        _locPurchaseRequisitionLine.StatusDate = now;
                        _locPurchaseRequisitionLine.Save();
                        _locPurchaseRequisitionLine.Session.CommitTransaction();
                    }
                    #endregion Remain < 0
                }
                #endregion Remain Qty with PostedCount == 0

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PrePurchaseOrder " + ex.ToString());
            }
        }

        private void SetFinalStatusPurchaseRequisitionOld(Session _currSession, PurchaseRequisition _locPurchaseRequisitionXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locCountStatus = 0;
                bool _locActivationPosting = false;

                if (_locPurchaseRequisitionXPO != null)
                {
                    XPCollection<PurchaseRequisitionLine> _locPurchaseRequisitionLines = new XPCollection<PurchaseRequisitionLine>
                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseRequisition", _locPurchaseRequisitionXPO)));
                    if (_locPurchaseRequisitionLines != null && _locPurchaseRequisitionLines.Count() > 0)
                    {
                        foreach (PurchaseRequisitionLine _locPurchaseRequisitionLine in _locPurchaseRequisitionLines)
                        {
                            if (_locPurchaseRequisitionLine.Status != Status.Close)
                            {
                                _locCountStatus = _locCountStatus + 1;
                            }
                        }
                    }

                    if (_locCountStatus > 0)
                    {
                        if (_locPurchaseRequisitionLines.Count() == _locCountStatus)
                        {
                            _locActivationPosting = true;
                        }
                    }

                    _locPurchaseRequisitionXPO.Status = Status.Posted;
                    _locPurchaseRequisitionXPO.StatusDate = now;
                    _locPurchaseRequisitionXPO.ActivationPosting = _locActivationPosting;
                    _locPurchaseRequisitionXPO.Save();
                    _locPurchaseRequisitionXPO.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseOrder " + ex.ToString());
            }
        }

        private void PurchaseOrderGetPRAction_ExecuteOld(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;


                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        PurchaseOrder _locPurchaseOrderOS = (PurchaseOrder)_objectSpace.GetObject(obj);

                        if (_locPurchaseOrderOS != null)
                        {
                            if (_locPurchaseOrderOS.Code != null)
                            {
                                _currObjectId = _locPurchaseOrderOS.Code;

                                PurchaseOrder _locPurchaseOrderXPO = _currSession.FindObject<PurchaseOrder>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locPurchaseOrderXPO != null)
                                {
                                    if (_locPurchaseOrderXPO.Status == Status.Open || _locPurchaseOrderXPO.Status == Status.Progress)
                                    {
                                        //if (_locPurchaseOrderXPO.Collection == true)
                                        //{
                                            XPCollection<PurchaseRequisitionCollection> _locPurchaseRequisitionCollections = new XPCollection<PurchaseRequisitionCollection>
                                                              (_currSession, new GroupOperator(GroupOperatorType.And,
                                                               new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO)));

                                            if (_locPurchaseRequisitionCollections != null && _locPurchaseRequisitionCollections.Count() > 0)
                                            {
                                                foreach (PurchaseRequisitionCollection _locPurchaseRequisitionCollection in _locPurchaseRequisitionCollections)
                                                {
                                                    if (_locPurchaseRequisitionCollection.PurchaseRequisition != null &&
                                                        (_locPurchaseRequisitionCollection.Status == Status.Open || _locPurchaseRequisitionCollection.Status == Status.Progress))
                                                    {
                                                        if (_locPurchaseRequisitionCollection.PurchaseRequisition.Code != null)
                                                        {
                                                            PurchaseRequisition _locPurchaseRequisition = _currSession.FindObject<PurchaseRequisition>
                                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                                    new BinaryOperator("Code", _locPurchaseRequisitionCollection.PurchaseRequisition.Code)));

                                                            if (_locPurchaseRequisition != null)
                                                            {
                                                                XPCollection<PurchaseRequisitionLine> _locPurchaseRequisitionLines = new XPCollection<PurchaseRequisitionLine>
                                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("PurchaseRequisition", _locPurchaseRequisition),
                                                                                                new BinaryOperator("Select", true)));

                                                                if (_locPurchaseRequisitionLines != null && _locPurchaseRequisitionLines.Count() > 0)
                                                                {
                                                                    foreach (PurchaseRequisitionLine _locPurchaseRequisitionLine in _locPurchaseRequisitionLines)
                                                                    {
                                                                        if (_locPurchaseRequisitionLine.PurchaseType == OrderType.Item
                                                                            && (_locPurchaseRequisitionLine.Status == Status.Progress || _locPurchaseRequisitionLine.Status == Status.Posted))
                                                                        {
                                                                            PurchaseOrderLine _locPurchaseOrderLine = _currSession.FindObject<PurchaseOrderLine>
                                                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                                                             new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO),
                                                                                                             new BinaryOperator("Item", _locPurchaseRequisitionLine.Item)));

                                                                            if (_locPurchaseOrderLine != null)
                                                                            {

                                                                                _locPurchaseOrderLine.MxDQty = _locPurchaseOrderLine.MxDQty + _locPurchaseRequisitionLine.DQty;
                                                                                _locPurchaseOrderLine.MxQty = _locPurchaseOrderLine.MxQty + _locPurchaseRequisitionLine.Qty;
                                                                                _locPurchaseOrderLine.MxTQty = _locPurchaseOrderLine.MxTQty + _locPurchaseRequisitionLine.TQty;
                                                                                _locPurchaseOrderLine.MxTUAmount = _locPurchaseOrderLine.MxTUAmount + (_locPurchaseOrderLine.MxUAmount * _locPurchaseRequisitionLine.TQty);
                                                                                _locPurchaseOrderLine.DQty = _locPurchaseOrderLine.DQty + _locPurchaseRequisitionLine.DQty;
                                                                                _locPurchaseOrderLine.Qty = _locPurchaseOrderLine.Qty + _locPurchaseRequisitionLine.Qty;
                                                                                _locPurchaseOrderLine.TQty = _locPurchaseOrderLine.TQty + _locPurchaseRequisitionLine.TQty;
                                                                                _locPurchaseOrderLine.TUAmount = _locPurchaseOrderLine.TUAmount + (_locPurchaseOrderLine.UAmount * _locPurchaseRequisitionLine.TQty);
                                                                                _locPurchaseOrderLine.Save();
                                                                                _locPurchaseOrderLine.Session.CommitTransaction();

                                                                                //SetRemainQuantity(_currSession, _locPurchaseRequisitionLine);
                                                                            }
                                                                            else
                                                                            {
                                                                                PurchaseOrderLine _saveDataPurchaseOrderLine = new PurchaseOrderLine(_currSession)
                                                                                {
                                                                                    PurchaseType = _locPurchaseRequisitionLine.PurchaseType,
                                                                                    Item = _locPurchaseRequisitionLine.Item,
                                                                                    Description = _locPurchaseRequisitionLine.Description,
                                                                                    MxDQty = _locPurchaseRequisitionLine.DQty,
                                                                                    MxDUOM = _locPurchaseRequisitionLine.DUOM,
                                                                                    MxQty = _locPurchaseRequisitionLine.Qty,
                                                                                    MxUOM = _locPurchaseRequisitionLine.UOM,
                                                                                    MxTQty = _locPurchaseRequisitionLine.TQty,
                                                                                    MxUAmount = _locPurchaseRequisitionLine.MxUAmount,
                                                                                    MxTUAmount = _locPurchaseRequisitionLine.TQty * _locPurchaseRequisitionLine.MxUAmount,
                                                                                    DQty = _locPurchaseRequisitionLine.DQty,
                                                                                    DUOM = _locPurchaseRequisitionLine.DUOM,
                                                                                    Qty = _locPurchaseRequisitionLine.Qty,
                                                                                    UOM = _locPurchaseRequisitionLine.UOM,
                                                                                    TQty = _locPurchaseRequisitionLine.TQty,
                                                                                    UAmount = _locPurchaseRequisitionLine.MxUAmount,
                                                                                    TUAmount = _locPurchaseRequisitionLine.TQty * _locPurchaseRequisitionLine.MxUAmount,
                                                                                    PurchaseOrder = _locPurchaseOrderXPO,
                                                                                };
                                                                                _saveDataPurchaseOrderLine.Save();
                                                                                _saveDataPurchaseOrderLine.Session.CommitTransaction();

                                                                                //SetRemainQuantity(_currSession, _locPurchaseRequisitionLine);
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                //SetFinalStatusPurchaseRequisition(_currSession, _locPurchaseRequisition);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        //}
                                    }
                                    SuccessMessageShow("Purchase Requisition Has Been Successfully Getting into Purchase Order");
                                }
                                else
                                {
                                    ErrorMessageShow("Data Purchase Order Not Available");
                                }

                            }
                            else
                            {
                                ErrorMessageShow("Data Purchase Order Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseOrder " + ex.ToString());
            }
        }

        #endregion GetPROld

        #region PostingMethodOld

        //Change SetInventoryTransferIn Into SetInventoryTransferOrder

        private void SetInventoryTransferOrder(Session _currSession, PurchaseOrder _locPurchaseOrderXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _locDocCode = null;
                ProjectHeader _locProjectHeader = null;

                if (_locPurchaseOrderXPO.ProjectHeader != null)
                {
                    _locProjectHeader = _locPurchaseOrderXPO.ProjectHeader;
                }

                DocumentType _locDocumentTypeXPO = _currSession.FindObject<DocumentType>
                                                (new GroupOperator(GroupOperatorType.And,
                                                 new BinaryOperator("TransferType", DirectionType.External),
                                                 new BinaryOperator("InventoryMovingType", InventoryMovingType.Receive),
                                                 new BinaryOperator("ObjectList", ObjectList.InventoryTransferOrder),
                                                 new BinaryOperator("DocumentRule", DocumentRule.Vendor),
                                                 new BinaryOperator("Active", true),
                                                 new BinaryOperator("Default", true)));

                if (_locDocumentTypeXPO != null)
                {
                    _locDocCode = _globFunc.GetDocumentNumberingUnlockOptimisticRecord(_currSession.DataLayer, _locDocumentTypeXPO);

                    if (_locDocCode != null)
                    {

                        InventoryTransferOrder _saveDataITOr = new InventoryTransferOrder(_currSession)
                        {
                            TransferType = _locDocumentTypeXPO.TransferType,
                            InventoryMovingType = _locDocumentTypeXPO.InventoryMovingType,
                            ObjectList = _locDocumentTypeXPO.ObjectList,
                            DocumentRule = _locDocumentTypeXPO.DocumentRule,
                            DocumentType = _locDocumentTypeXPO,
                            DocNo = _locDocCode,
                            BusinessPartner = _locPurchaseOrderXPO.BuyFromVendor,
                            BusinessPartnerCountry = _locPurchaseOrderXPO.BuyFromCountry,
                            BusinessPartnerCity = _locPurchaseOrderXPO.BuyFromCity,
                            BusinessPartnerAddress = _locPurchaseOrderXPO.BuyFromAddress,
                            EstimatedDate = _locPurchaseOrderXPO.EstimatedDate,
                            PurchaseOrder = _locPurchaseOrderXPO,
                            ProjectHeader = _locProjectHeader,
                            Company = _locPurchaseOrderXPO.Company,
                        };
                        _saveDataITOr.Save();
                        _saveDataITOr.Session.CommitTransaction();

                        InventoryTransferOrder _locInventoryTransferOrder = _currSession.FindObject<InventoryTransferOrder>
                                                (new GroupOperator(GroupOperatorType.And,
                                                 new BinaryOperator("DocNo", _locDocCode),
                                                 new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO)));

                        if (_locInventoryTransferOrder != null)
                        {

                            XPCollection<PurchaseOrderLine> _locPurchaseOrderLines = new XPCollection<PurchaseOrderLine>
                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                        new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO),
                                        new BinaryOperator("Status", Status.Progress)));

                            if (_locPurchaseOrderLines != null && _locPurchaseOrderLines.Count > 0)
                            {
                                foreach (PurchaseOrderLine _locPurchaseOrderLine in _locPurchaseOrderLines)
                                {
                                    #region SetMaxValue
                                    double _locMxDQty = _locPurchaseOrderLine.DQty;
                                    UnitOfMeasure _locMxDUOM = _locPurchaseOrderLine.DUOM;
                                    double _locMxQty = _locPurchaseOrderLine.MxQty;
                                    UnitOfMeasure _locMxUOM = _locPurchaseOrderLine.UOM;
                                    double _locMxTQty = _locPurchaseOrderLine.TQty;


                                    if (_locPurchaseOrderLine.MxDQty > 0)
                                    {
                                        _locMxDQty = _locPurchaseOrderLine.MxDQty;
                                    }

                                    if (_locPurchaseOrderLine.MxDUOM != null)
                                    {
                                        _locMxDUOM = _locPurchaseOrderLine.MxDUOM;
                                    }

                                    if (_locPurchaseOrderLine.MxQty > 0)
                                    {
                                        _locMxQty = _locPurchaseOrderLine.MxQty;
                                    }

                                    if (_locPurchaseOrderLine.MxUOM != null)
                                    {
                                        _locMxUOM = _locPurchaseOrderLine.MxUOM;
                                    }

                                    if (_locPurchaseOrderLine.MxTQty > 0)
                                    {
                                        _locMxTQty = _locPurchaseOrderLine.MxTQty;
                                    }
                                    #endregion SetMaxValue

                                    InventoryTransferOrderLine _saveDataInventoryTransferOrderLine = new InventoryTransferOrderLine(_currSession)
                                    {
                                        Select = true,
                                        Item = _locPurchaseOrderLine.Item,
                                        MxDQty = _locMxDQty,
                                        MxDUOM = _locMxDUOM,
                                        MxQty = _locMxQty,
                                        MxUOM = _locMxUOM,
                                        MxTQty = _locMxTQty,
                                        DQty = _locPurchaseOrderLine.DQty,
                                        DUOM = _locPurchaseOrderLine.DUOM,
                                        Qty = _locPurchaseOrderLine.Qty,
                                        UOM = _locPurchaseOrderLine.UOM,
                                        TQty = _locPurchaseOrderLine.TQty,
                                        EstimatedDate = _locPurchaseOrderLine.EstimatedDate,
                                        Company = _locInventoryTransferOrder.Company,
                                        InventoryTransferOrder = _locInventoryTransferOrder
                                    };
                                    _saveDataInventoryTransferOrderLine.Save();
                                    _saveDataInventoryTransferOrderLine.Session.CommitTransaction();

                                    //Change Status POL From Progress To Posted
                                    _locPurchaseOrderLine.Status = Status.Close;
                                    _locPurchaseOrderLine.StatusDate = now;
                                    _locPurchaseOrderLine.ActivationPosting = true;
                                    _locPurchaseOrderLine.Save();
                                    _locPurchaseOrderLine.Session.CommitTransaction();

                                }
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseOrder " + ex.ToString());
            }
        }

        private void SetInventoryTransferIn(Session _currSession, PurchaseOrder _locPurchaseOrderXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _locDocCode = null;
                double _locMaxPay = 0;
                ProjectHeader _locProjectHeader = null;

                if (_locPurchaseOrderXPO.ProjectHeader != null)
                {
                    _locProjectHeader = _locPurchaseOrderXPO.ProjectHeader;
                }

                //if(_locPurchaseOrderXPO.DownPayment == true)
                //{
                //    _locMaxPay = GetMaxPay(_currSession, _locPurchaseOrderXPO) - _locPurchaseOrderXPO.DP_Amount;
                //}else
                //{
                //    if(_locPurchaseOrderXPO.CBD == true)
                //    {
                //        _locMaxPay = 0;
                //    }
                //    else
                //    {
                //        _locMaxPay = GetMaxPay(_currSession, _locPurchaseOrderXPO);
                //    }

                //}

                DocumentType _locDocumentTypeXPO = _currSession.FindObject<DocumentType>
                                                (new GroupOperator(GroupOperatorType.And,
                                                 new BinaryOperator("TransferType", DirectionType.External),
                                                 new BinaryOperator("InventoryMovingType", InventoryMovingType.Receive),
                                                 new BinaryOperator("ObjectList", ObjectList.InventoryTransferIn),
                                                 new BinaryOperator("DocumentRule", DocumentRule.Vendor),
                                                 new BinaryOperator("Active", true),
                                                 new BinaryOperator("Default", true)));

                if (_locDocumentTypeXPO != null)
                {
                    _locDocCode = _globFunc.GetDocumentNumberingUnlockOptimisticRecord(_currSession.DataLayer, _locDocumentTypeXPO);

                    if (_locDocCode != null)
                    {

                        InventoryTransferIn _saveDataITIn = new InventoryTransferIn(_currSession)
                        {
                            TransferType = _locDocumentTypeXPO.TransferType,
                            InventoryMovingType = _locDocumentTypeXPO.InventoryMovingType,
                            ObjectList = _locDocumentTypeXPO.ObjectList,
                            DocumentRule = _locDocumentTypeXPO.DocumentRule,
                            DocumentType = _locDocumentTypeXPO,
                            DocNo = _locDocCode,
                            BusinessPartner = _locPurchaseOrderXPO.BuyFromVendor,
                            BusinessPartnerCountry = _locPurchaseOrderXPO.BuyFromCountry,
                            BusinessPartnerCity = _locPurchaseOrderXPO.BuyFromCity,
                            BusinessPartnerAddress = _locPurchaseOrderXPO.BuyFromAddress,
                            EstimatedDate = _locPurchaseOrderXPO.EstimatedDate,
                            PurchaseOrder = _locPurchaseOrderXPO,
                            ProjectHeader = _locProjectHeader,
                            MaxPay = _locMaxPay,
                            Company = _locPurchaseOrderXPO.Company,
                        };
                        _saveDataITIn.Save();
                        _saveDataITIn.Session.CommitTransaction();

                        InventoryTransferIn _locInventoryTransferIn = _currSession.FindObject<InventoryTransferIn>
                                                (new GroupOperator(GroupOperatorType.And,
                                                 new BinaryOperator("DocNo", _locDocCode),
                                                 new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO)));

                        if (_locInventoryTransferIn != null)
                        {

                            XPCollection<PurchaseOrderLine> _locPurchaseOrderLines = new XPCollection<PurchaseOrderLine>
                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                        new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO),
                                        new BinaryOperator("Status", Status.Progress)));

                            if (_locPurchaseOrderLines != null && _locPurchaseOrderLines.Count > 0)
                            {
                                foreach (PurchaseOrderLine _locPurchaseOrderLine in _locPurchaseOrderLines)
                                {
                                    #region SetMaxValueQuantity
                                    double _locMxDQty = _locPurchaseOrderLine.DQty;
                                    UnitOfMeasure _locMxDUOM = _locPurchaseOrderLine.DUOM;
                                    double _locMxQty = _locPurchaseOrderLine.MxQty;
                                    UnitOfMeasure _locMxUOM = _locPurchaseOrderLine.UOM;
                                    double _locMxTQty = _locPurchaseOrderLine.TQty;


                                    if (_locPurchaseOrderLine.MxDQty > 0)
                                    {
                                        _locMxDQty = _locPurchaseOrderLine.MxDQty;
                                    }

                                    if (_locPurchaseOrderLine.MxDUOM != null)
                                    {
                                        _locMxDUOM = _locPurchaseOrderLine.MxDUOM;
                                    }

                                    if (_locPurchaseOrderLine.MxQty > 0)
                                    {
                                        _locMxQty = _locPurchaseOrderLine.MxQty;
                                    }

                                    if (_locPurchaseOrderLine.MxUOM != null)
                                    {
                                        _locMxUOM = _locPurchaseOrderLine.MxUOM;
                                    }

                                    if (_locPurchaseOrderLine.MxTQty > 0)
                                    {
                                        _locMxTQty = _locPurchaseOrderLine.MxTQty;
                                    }
                                    #endregion SetMaxValueQuantity

                                    InventoryTransferInLine _saveDataInventoryTransferInLine = new InventoryTransferInLine(_currSession)
                                    {
                                        Select = true,
                                        Item = _locPurchaseOrderLine.Item,
                                        Description = _locPurchaseOrderLine.Description,
                                        MxDQty = _locMxDQty,
                                        MxDUOM = _locMxDUOM,
                                        MxQty = _locMxQty,
                                        MxUOM = _locMxUOM,
                                        MxTQty = _locMxTQty,
                                        DQty = _locPurchaseOrderLine.DQty,
                                        DUOM = _locPurchaseOrderLine.DUOM,
                                        Qty = _locPurchaseOrderLine.Qty,
                                        UOM = _locPurchaseOrderLine.UOM,
                                        TQty = _locPurchaseOrderLine.TQty,
                                        EstimatedDate = _locPurchaseOrderLine.EstimatedDate,
                                        Company = _locInventoryTransferIn.Company,
                                        InventoryTransferIn = _locInventoryTransferIn
                                    };
                                    _saveDataInventoryTransferInLine.Save();
                                    _saveDataInventoryTransferInLine.Session.CommitTransaction();

                                    //Change Status POL From Progress To Posted
                                    _locPurchaseOrderLine.Status = Status.Close;
                                    _locPurchaseOrderLine.StatusDate = now;
                                    _locPurchaseOrderLine.ActivationPosting = true;
                                    _locPurchaseOrderLine.Save();
                                    _locPurchaseOrderLine.Session.CommitTransaction();

                                }
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseOrder " + ex.ToString());
            }
        }

        private void SetPurchaseInvoice(Session _currSession, PurchaseOrder _locPurchaseOrderXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _currSignCode = null;
                ProjectHeader _locProjectHeader = null;

                if (_locPurchaseOrderXPO != null)
                {
                    if (_locPurchaseOrderXPO.ProjectHeader != null)
                    {
                        _locProjectHeader = _locPurchaseOrderXPO.ProjectHeader;
                    }
                    _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.PurchaseInvoice);

                    if (_currSignCode != null)
                    {
                        PurchaseInvoice _saveDataPurchaseInvoice = new PurchaseInvoice(_currSession)
                        {
                            SignCode = _currSignCode,
                            BuyFromVendor = _locPurchaseOrderXPO.BuyFromVendor,
                            BuyFromContact = _locPurchaseOrderXPO.BuyFromContact,
                            BuyFromCountry = _locPurchaseOrderXPO.BuyFromCountry,
                            BuyFromCity = _locPurchaseOrderXPO.BuyFromCity,
                            BuyFromAddress = _locPurchaseOrderXPO.BuyFromAddress,
                            PayToVendor = _locPurchaseOrderXPO.BuyFromVendor,
                            PayToContact = _locPurchaseOrderXPO.BuyFromContact,
                            PayToCountry = _locPurchaseOrderXPO.BuyFromCountry,
                            PayToCity = _locPurchaseOrderXPO.BuyFromCity,
                            PayToAddress = _locPurchaseOrderXPO.BuyFromAddress,
                            TaxNo = _locPurchaseOrderXPO.TaxNo,
                            TOP = _locPurchaseOrderXPO.TOP,
                            MaxPay = GetMaxPay(_currSession, _locPurchaseOrderXPO),
                            Pay = GetMaxPay(_currSession, _locPurchaseOrderXPO),
                            PurchaseOrder = _locPurchaseOrderXPO,
                            ProjectHeader = _locProjectHeader,
                            PaymentMethod = _locPurchaseOrderXPO.PaymentMethod,
                            Company = _locPurchaseOrderXPO.Company,
                        };
                        _saveDataPurchaseInvoice.Save();
                        _saveDataPurchaseInvoice.Session.CommitTransaction();

                        PurchaseInvoice _locPurchaseInvoice2 = _currSession.FindObject<PurchaseInvoice>(new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("SignCode", _currSignCode)));

                        if (_locPurchaseInvoice2 != null)
                        {
                            SetPurchaseInvoiceCollection(_currSession, _locPurchaseOrderXPO, _locPurchaseInvoice2);
                        }

                    }
                }


            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseOrder " + ex.ToString());
            }
        }

        private void SetPurchasePrePaymentInvoice(Session _currSession, PurchaseOrder _locPurchaseOrderXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _currSignCode = null;
                ProjectHeader _locProjectHeader = null;

                if (_locPurchaseOrderXPO != null)
                {
                    if (_locPurchaseOrderXPO.ProjectHeader != null)
                    {
                        _locProjectHeader = _locPurchaseOrderXPO.ProjectHeader;
                    }
                    _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.PurchasePrePaymentInvoice);

                    if (_currSignCode != null)
                    {
                        PurchasePrePaymentInvoice _saveDataPurchasePrePaymentInvoice = new PurchasePrePaymentInvoice(_currSession)
                        {
                            SignCode = _currSignCode,
                            BuyFromVendor = _locPurchaseOrderXPO.BuyFromVendor,
                            BuyFromContact = _locPurchaseOrderXPO.BuyFromContact,
                            BuyFromCountry = _locPurchaseOrderXPO.BuyFromCountry,
                            BuyFromCity = _locPurchaseOrderXPO.BuyFromCity,
                            BuyFromAddress = _locPurchaseOrderXPO.BuyFromAddress,
                            PayToVendor = _locPurchaseOrderXPO.BuyFromVendor,
                            PayToContact = _locPurchaseOrderXPO.BuyFromContact,
                            PayToCountry = _locPurchaseOrderXPO.BuyFromCountry,
                            PayToCity = _locPurchaseOrderXPO.BuyFromCity,
                            PayToAddress = _locPurchaseOrderXPO.BuyFromAddress,
                            TaxNo = _locPurchaseOrderXPO.TaxNo,
                            TOP = _locPurchaseOrderXPO.TOP,
                            PaymentMethod = _locPurchaseOrderXPO.PaymentMethod,
                            //MaxPay = _locPurchaseOrderXPO.DP_Amount,
                            //Pay = _locPurchaseOrderXPO.DP_Amount,
                            PurchaseOrder = _locPurchaseOrderXPO,
                            ProjectHeader = _locProjectHeader,
                            Company = _locPurchaseOrderXPO.Company,
                        };
                        _saveDataPurchasePrePaymentInvoice.Save();
                        _saveDataPurchasePrePaymentInvoice.Session.CommitTransaction();



                        PurchasePrePaymentInvoice _locPurchasePrePaymentInvoice2 = _currSession.FindObject<PurchasePrePaymentInvoice>(new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("SignCode", _currSignCode)));

                        if (_locPurchasePrePaymentInvoice2 != null)
                        {
                            SetPurchaseInvoiceCollectionPrePayment(_currSession, _locPurchaseOrderXPO, _locPurchasePrePaymentInvoice2);
                        }

                    }
                }


            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseOrder " + ex.ToString());
            }
        }

        private void SetPurchaseInvoiceCollection(Session _currSession, PurchaseOrder _locPurchaseOrderXPO, PurchaseInvoice _locPurchaseInvoiceXPO)
        {
            try
            {
                //SalesInvoiceCollection based On Sales Order
                GlobalFunction _globFunc = new GlobalFunction();
                string _currSignCode = null;
                ProjectHeader _locProjectHeader = null;

                if (_locPurchaseOrderXPO != null)
                {
                    if (_locPurchaseOrderXPO.ProjectHeader != null)
                    {
                        _locProjectHeader = _locPurchaseOrderXPO.ProjectHeader;
                    }
                }

                PurchaseInvoiceCollection _locPurchaseInvoiceCollection = _currSession.FindObject<PurchaseInvoiceCollection>(new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO)));
                if (_locPurchaseInvoiceCollection != null)
                {
                    PurchaseInvoiceCollectionLine _saveDataPurchaseInvoiceCollectionLine = new PurchaseInvoiceCollectionLine(_currSession)
                    {
                        PurchaseInvoice = _locPurchaseInvoiceXPO,
                        PurchaseInvoiceCollection = _locPurchaseInvoiceCollection,
                    };
                    _saveDataPurchaseInvoiceCollectionLine.Save();
                    _saveDataPurchaseInvoiceCollectionLine.Session.CommitTransaction();
                }
                if (_locPurchaseInvoiceCollection == null)
                {
                    _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.PurchaseInvoiceCollection);

                    if (_currSignCode != null)
                    {
                        PurchaseInvoiceCollection _saveDataPurchaseInvoiceCollection = new PurchaseInvoiceCollection(_currSession)
                        {
                            SignCode = _currSignCode,
                            BuyFromVendor = _locPurchaseOrderXPO.BuyFromVendor,
                            BuyFromContact = _locPurchaseOrderXPO.BuyFromContact,
                            BuyFromCountry = _locPurchaseOrderXPO.BuyFromCountry,
                            BuyFromCity = _locPurchaseOrderXPO.BuyFromCity,
                            BuyFromAddress = _locPurchaseOrderXPO.BuyFromAddress,
                            TaxNo = _locPurchaseOrderXPO.TaxNo,
                            TOP = _locPurchaseOrderXPO.TOP,
                            MaxPay = GetMaxPay(_currSession, _locPurchaseOrderXPO),
                            //DP_Amount = _locPurchaseOrderXPO.DP_Amount,
                            PurchaseOrder = _locPurchaseOrderXPO,
                            ProjectHeader = _locProjectHeader,
                            Company = _locPurchaseOrderXPO.Company,

                        };
                        _saveDataPurchaseInvoiceCollection.Save();
                        _saveDataPurchaseInvoiceCollection.Session.CommitTransaction();

                        PurchaseInvoiceCollection _locPurchaseInvoiceCollection2 = _currSession.FindObject<PurchaseInvoiceCollection>(new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("SignCode", _currSignCode)));

                        if (_locPurchaseInvoiceCollection2 != null)
                        {
                            PurchaseInvoiceCollectionLine _saveDataPurchaseInvoiceCollectionLine = new PurchaseInvoiceCollectionLine(_currSession)
                            {
                                PurchaseInvoice = _locPurchaseInvoiceXPO,
                                PurchaseInvoiceCollection = _locPurchaseInvoiceCollection2,
                            };
                            _saveDataPurchaseInvoiceCollectionLine.Save();
                            _saveDataPurchaseInvoiceCollectionLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PurchaseOrder ", ex.ToString());
            }
        }

        private void SetPurchaseInvoiceCollectionPrePayment(Session _currSession, PurchaseOrder _locPurchaseOrderXPO, PurchasePrePaymentInvoice _locPurchasePrePaymentInvoiceXPO)
        {
            try
            {
                //SalesInvoiceCollection based On Sales Order
                GlobalFunction _globFunc = new GlobalFunction();
                string _currSignCode = null;
                ProjectHeader _locProjectHeader = null;

                if (_locPurchaseOrderXPO != null)
                {
                    if (_locPurchaseOrderXPO.ProjectHeader != null)
                    {
                        _locProjectHeader = _locPurchaseOrderXPO.ProjectHeader;
                    }
                }

                PurchaseInvoiceCollection _locPurchaseInvoiceCollection = _currSession.FindObject<PurchaseInvoiceCollection>(new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO)));
                if (_locPurchaseInvoiceCollection != null)
                {
                    PurchaseInvoiceCollectionLine _saveDataPurchaseInvoiceCollectionLine = new PurchaseInvoiceCollectionLine(_currSession)
                    {
                        PurchasePrePaymentInvoice = _locPurchasePrePaymentInvoiceXPO,
                        PurchaseInvoiceCollection = _locPurchaseInvoiceCollection,
                    };
                    _saveDataPurchaseInvoiceCollectionLine.Save();
                    _saveDataPurchaseInvoiceCollectionLine.Session.CommitTransaction();
                }
                if (_locPurchaseInvoiceCollection == null)
                {
                    _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.PurchaseInvoiceCollection);

                    if (_currSignCode != null)
                    {
                        PurchaseInvoiceCollection _saveDataPurchaseInvoiceCollection = new PurchaseInvoiceCollection(_currSession)
                        {
                            SignCode = _currSignCode,
                            BuyFromVendor = _locPurchaseOrderXPO.BuyFromVendor,
                            BuyFromContact = _locPurchaseOrderXPO.BuyFromContact,
                            BuyFromCountry = _locPurchaseOrderXPO.BuyFromCountry,
                            BuyFromCity = _locPurchaseOrderXPO.BuyFromCity,
                            BuyFromAddress = _locPurchaseOrderXPO.BuyFromAddress,
                            TaxNo = _locPurchaseOrderXPO.TaxNo,
                            TOP = _locPurchaseOrderXPO.TOP,
                            MaxPay = GetMaxPay(_currSession, _locPurchaseOrderXPO),
                            //DP_Amount = _locPurchaseOrderXPO.DP_Amount,
                            PurchaseOrder = _locPurchaseOrderXPO,
                            ProjectHeader = _locProjectHeader,
                            Company = _locPurchaseOrderXPO.Company,

                        };
                        _saveDataPurchaseInvoiceCollection.Save();
                        _saveDataPurchaseInvoiceCollection.Session.CommitTransaction();

                        PurchaseInvoiceCollection _locPurchaseInvoiceCollection2 = _currSession.FindObject<PurchaseInvoiceCollection>(new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("SignCode", _currSignCode)));

                        if (_locPurchaseInvoiceCollection2 != null)
                        {
                            PurchaseInvoiceCollectionLine _saveDataPurchaseInvoiceCollectionLine = new PurchaseInvoiceCollectionLine(_currSession)
                            {
                                PurchasePrePaymentInvoice = _locPurchasePrePaymentInvoiceXPO,
                                PurchaseInvoiceCollection = _locPurchaseInvoiceCollection2,
                            };
                            _saveDataPurchaseInvoiceCollectionLine.Save();
                            _saveDataPurchaseInvoiceCollectionLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PurchaseOrder ", ex.ToString());
            }
        }

        private void SetFinalStatusPurchaseOrder(Session _currSession, PurchaseOrder _locPurchaseOrderXPO)
        {
            try
            {
                DateTime now = DateTime.Now;

                if (_locPurchaseOrderXPO != null)
                {
                    XPCollection<PurchaseOrderLine> _locPurchaseOrderLines = new XPCollection<PurchaseOrderLine>
                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO)));

                    if (_locPurchaseOrderLines != null && _locPurchaseOrderLines.Count() > 0)
                    {
                        foreach (PurchaseOrderLine _locPurchaseOrderLine in _locPurchaseOrderLines)
                        {
                            if (_locPurchaseOrderLine.Status == Status.Progress || _locPurchaseOrderLine.Status == Status.Posted)
                            {
                                _locPurchaseOrderLine.ActivationPosting = true;
                                _locPurchaseOrderLine.Status = Status.Close;
                                _locPurchaseOrderLine.StatusDate = now;
                                _locPurchaseOrderLine.Save();
                                _locPurchaseOrderLine.Session.CommitTransaction();
                            }
                        }
                    }

                    _locPurchaseOrderXPO.Status = Status.Posted;
                    _locPurchaseOrderXPO.StatusDate = now;
                    _locPurchaseOrderXPO.ActivationPosting = true;
                    _locPurchaseOrderXPO.Save();
                    _locPurchaseOrderXPO.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseOrder " + ex.ToString());
            }
        }

        private double GetMaxPay(Session _currSession, PurchaseOrder _locPurchaseOrderXPO)
        {
            double _result = 0;
            try
            {
                double _locTotAmount = 0;

                if (_locPurchaseOrderXPO != null)
                {
                    XPCollection<PurchaseOrderLine> _locPurchaseOrderLines = new XPCollection<PurchaseOrderLine>
                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO)));

                    if (_locPurchaseOrderLines != null && _locPurchaseOrderLines.Count() > 0)
                    {
                        foreach (PurchaseOrderLine _locPurchaseOrderLine in _locPurchaseOrderLines)
                        {
                            _locTotAmount = _locTotAmount + _locPurchaseOrderLine.TAmount;
                        }

                        _result = _locTotAmount;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseOrder " + ex.ToString());
            }
            return _result;
        }

        private bool GetStatusClosePurchaseOrderLine(Session _currSession, PurchaseOrder _locPurchaseOrderXPO)
        {
            bool _result = false;
            try
            {
                DateTime now = DateTime.Now;
                int _locCountClose = 0;

                if (_locPurchaseOrderXPO != null)
                {
                    XPCollection<PurchaseOrderLine> _locPurchaseOrderLines = new XPCollection<PurchaseOrderLine>
                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO)));

                    if (_locPurchaseOrderLines != null && _locPurchaseOrderLines.Count() > 0)
                    {
                        foreach (PurchaseOrderLine _locPurchaseOrderLine in _locPurchaseOrderLines)
                        {
                            if (_locPurchaseOrderLine.Status == Status.Close)
                            {
                                _locCountClose = _locCountClose + 1;
                            }
                        }

                        if (_locCountClose == _locPurchaseOrderLines.Count())
                        {
                            _result = true;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseOrder " + ex.ToString());
            }
            return _result;
        }

        #endregion PostingMethodOld

        #region Global Method

        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0} !", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method

        #region Approval Process

        private void SetApprovalLine(Session _currentSession, PurchaseOrder _locPurchaseOrderXPO, ApprovalLevel _locApprovalLevel)
        {
            try
            {
                DateTime now = DateTime.Now;

                ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO),
                                                                         new BinaryOperator("ApprovalLevel", _locApprovalLevel)));

                if (_locApprovalLineXPO == null)
                {
                    ApprovalLine _saveDataAL2 = new ApprovalLine(_currentSession)
                    {
                        ApprovalDate = now,
                        ApprovalStatus = Status.Approved,
                        ApprovalLevel = _locApprovalLevel,
                        PurchaseOrder = _locPurchaseOrderXPO,
                    };
                    _saveDataAL2.Save();
                    _saveDataAL2.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseOrder " + ex.ToString());
            }
        }

        #endregion Approval Process

        #region Email

        private string BackgroundBody(Session _currentSession, PurchaseOrder _locPurchaseOrderXPO, MailSetupDetail _locMailSetDetail)
        {
            string body = string.Empty;

            PurchaseOrder _locPurchaseOrders = _currentSession.FindObject<PurchaseOrder>
                                           (new GroupOperator(GroupOperatorType.And,
                                            new BinaryOperator("Code", _locPurchaseOrderXPO.Code)));

            ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                               (new GroupOperator(GroupOperatorType.And,
                                                new BinaryOperator("PurchaseOrder", _locPurchaseOrders)));

            string Status = _locApprovalLineXPO.ApprovalStatus.ToString();

            using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Email/EmailTemplate.html")))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{Code}", _locPurchaseOrderXPO.Code);

            #region Level
            if (_locPurchaseOrders.ActiveApproved1 == true)
            {
                string Active = "Level 1";
                body = body.Replace("{Level}", Active);
            }
            else if (_locPurchaseOrders.ActiveApproved2 == true)
            {
                string Active = "Level 2";
                body = body.Replace("{Level}", Active);
            }
            else if (_locPurchaseOrders.ActiveApproved3 == true)
            {
                string Active = "Level 3";
                body = body.Replace("{Level}", Active);
            }
            else
            {
                string Active = "Not Available";
                body = body.Replace("{Level}", Active);
            }
            #endregion Level

            body = body.Replace("{Status}", Status);
            body = body.Replace("{Message}", _locMailSetDetail.MessageBody);
            return body;
        }

        protected void SendEmail(Session _currentSession, PurchaseOrder _locPurchaseOrderXPO, Status _locStatus, ApprovalLine _locApprovalLineXPO2)
        {
            DateTime now = DateTime.Now;
            var User = SecuritySystem.CurrentUserName;
            GlobalFunction _globFunc = new GlobalFunction();
            UserAccess _numLineUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", User));
            string _locMessageBody = null;

            try
            {
                if (_locPurchaseOrderXPO != null)
                {
                    XPCollection<MailSetupDetail> _locMailSetDetails = new XPCollection<MailSetupDetail>
                                                                       (_currentSession, new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("ObjectList", ObjectList.PurchaseOrder),
                                                                        new BinaryOperator("UserAccess", _numLineUserAccess),
                                                                        new BinaryOperator("Active", true)));

                    if (_locMailSetDetails != null && _locMailSetDetails.Count > 0)
                    {
                        foreach (MailSetupDetail _locMailSetDetail in _locMailSetDetails)
                        {
                            if (_locMailSetDetail.MessageBody != null)
                            {
                                if (_locApprovalLineXPO2.EndApproval == true)
                                {
                                    _locMessageBody = this.BackgroundBody(_currentSession, _locPurchaseOrderXPO, _locMailSetDetail);
                                }
                                else
                                {
                                    _locMessageBody = this.BackgroundBody(_currentSession, _locPurchaseOrderXPO, _locMailSetDetail);
                                }
                            }

                            _globFunc.SetAndSendMail(_locMailSetDetail.SmtpHost, _locMailSetDetail.SmtpPort,
                            _locMailSetDetail.MailFrom, _locMailSetDetail.MailFromPassword, _locMailSetDetail.MailTo, _locMailSetDetail.MailSubject, _locMessageBody);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseOrder " + ex.ToString());
            }
        }

        #endregion Email


    }
}
