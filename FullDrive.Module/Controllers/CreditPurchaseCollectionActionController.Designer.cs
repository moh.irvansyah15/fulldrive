﻿namespace FullDrive.Module.Controllers
{
    partial class CreditPurchaseCollectionActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.CreditPurchaseCollectionShowPRMAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // CreditPurchaseCollectionShowPRMAction
            // 
            this.CreditPurchaseCollectionShowPRMAction.Caption = "Show PRM";
            this.CreditPurchaseCollectionShowPRMAction.ConfirmationMessage = null;
            this.CreditPurchaseCollectionShowPRMAction.Id = "CreditPurchaseCollectionShowPRMActionId";
            this.CreditPurchaseCollectionShowPRMAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.CreditPurchaseCollection);
            this.CreditPurchaseCollectionShowPRMAction.ToolTip = null;
            this.CreditPurchaseCollectionShowPRMAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.CreditPurchaseCollectionShowPRMAction_Execute);
            // 
            // CreditPurchaseCollectionActionController
            // 
            this.Actions.Add(this.CreditPurchaseCollectionShowPRMAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction CreditPurchaseCollectionShowPRMAction;
    }
}
