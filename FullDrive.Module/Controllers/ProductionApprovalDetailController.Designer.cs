﻿namespace FullDrive.Module.Controllers
{
    partial class ProductionApprovalDetailController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ProductionApprovalDetailAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // ProductionApprovalDetailAction
            // 
            this.ProductionApprovalDetailAction.Caption = "Approval";
            this.ProductionApprovalDetailAction.ConfirmationMessage = null;
            this.ProductionApprovalDetailAction.Id = "ProductionApprovalDetailActionId";
            this.ProductionApprovalDetailAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.ProductionApprovalDetailAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.Production);
            this.ProductionApprovalDetailAction.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.ProductionApprovalDetailAction.ToolTip = null;
            this.ProductionApprovalDetailAction.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.ProductionApprovalDetailAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.ProductionApprovalDetailAction_Execute);
            // 
            // ProductionApprovalDetailController
            // 
            this.Actions.Add(this.ProductionApprovalDetailAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SingleChoiceAction ProductionApprovalDetailAction;
    }
}
