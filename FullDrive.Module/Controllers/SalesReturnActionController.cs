﻿#region Default

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

#endregion Default

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;
using System.Web;
using System.IO;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class SalesReturnActionController : ViewController
    {
        #region Default

        private ChoiceActionItem _selectionListviewFilter;
        private ChoiceActionItem _setApprovalLevel;

        public SalesReturnActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            SalesReturnFilterAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                SalesReturnFilterAction.Items.Add(_selectionListviewFilter);
            }
        }
        protected override void OnActivated()
        {
            TypeOfView = typeof(ObjectView);
            base.OnActivated();
            // Perform various tasks depending on the target View.
            #region Approval
            var _locUser = SecuritySystem.CurrentUserName;
            GlobalFunction _globalFunction = new GlobalFunction();
            Session _currentSession = null;
            if (this.ObjectSpace != null)
            {
                _currentSession = ((XPObjectSpace)this.ObjectSpace).Session;
            }

            if (_currentSession != null)
            {
                UserAccess _locUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", _locUser));

                if (_locUserAccess != null)
                {
                    SalesReturnApprovalAction.Items.Clear();

                    XPCollection<ApplicationSetupDetail> _locAppSetupDetails = new XPCollection<ApplicationSetupDetail>
                                                                                 (_currentSession, new GroupOperator(GroupOperatorType.And,
                                                                                  new BinaryOperator("UserAccess", _locUserAccess),
                                                                                  new BinaryOperator("ObjectList", CustomProcess.ObjectList.SalesReturn),
                                                                                  new BinaryOperator("FunctionList", CustomProcess.FunctionList.Approval),
                                                                                  new BinaryOperator("Active", true)));

                    if (_locAppSetupDetails != null && _locAppSetupDetails.Count > 0)
                    {

                        foreach (ApplicationSetupDetail _locAppSetupDetail in _locAppSetupDetails)
                        {
                            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.ApprovalLevel)))
                            {
                                if (_locAppSetupDetail.ApprovalLevel != CustomProcess.ApprovalLevel.None)
                                {
                                    EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.ApprovalLevel));
                                    _setApprovalLevel = new ChoiceActionItem(_ed.GetCaption(_locAppSetupDetail.ApprovalLevel), _locAppSetupDetail.ApprovalLevel);
                                }
                            }
                            SalesReturnApprovalAction.Items.Add(_setApprovalLevel);
                        }
                    }
                }
            }
            #endregion Approval
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        #endregion Default

        private void SalesReturnProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        SalesReturn _locSalesReturnOS = (SalesReturn)_objectSpace.GetObject(obj);

                        if (_locSalesReturnOS != null)
                        {
                            if (_locSalesReturnOS.Code != null)
                            {
                                _currObjectId = _locSalesReturnOS.Code;

                                SalesReturn _locSalesReturnXPO = _currSession.FindObject<SalesReturn>
                                                                 (new GroupOperator(GroupOperatorType.And,
                                                                  new BinaryOperator("Code", _currObjectId)));

                                if (_locSalesReturnXPO != null)
                                {
                                    if (_locSalesReturnXPO.Status == Status.Open || _locSalesReturnXPO.Status == Status.Progress)
                                    {
                                        if (_locSalesReturnXPO.Status == Status.Open)
                                        {
                                            _locSalesReturnXPO.Status = Status.Progress;
                                            _locSalesReturnXPO.StatusDate = now;
                                            _locSalesReturnXPO.Save();
                                            _locSalesReturnXPO.Session.CommitTransaction();
                                        }

                                        XPCollection<SalesReturnLine> _locSalesReturnLines = new XPCollection<SalesReturnLine>
                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                             new BinaryOperator("SalesReturn", _locSalesReturnXPO),
                                                                                             new BinaryOperator("Status", Status.Open)));

                                        if (_locSalesReturnLines != null && _locSalesReturnLines.Count > 0)
                                        {
                                            foreach (SalesReturnLine _locSalesReturnLine in _locSalesReturnLines)
                                            {
                                                _locSalesReturnLine.Status = Status.Progress;
                                                _locSalesReturnLine.StatusDate = now;
                                                _locSalesReturnLine.Save();
                                                _locSalesReturnLine.Session.CommitTransaction();
                                            }
                                        }

                                        XPCollection<ReturnSalesCollection> _locReturnSalesCollections = new XPCollection<ReturnSalesCollection>(_currSession,
                                                                                                        new GroupOperator(GroupOperatorType.And,
                                                                                                        new BinaryOperator("SalesReturn", _locSalesReturnXPO),
                                                                                                        new BinaryOperator("Status", Status.Open)));
                                        if(_locReturnSalesCollections != null && _locReturnSalesCollections.Count() > 0)
                                        {
                                            foreach(ReturnSalesCollection _locReturnSalesCollection in _locReturnSalesCollections)
                                            {
                                                _locReturnSalesCollection.Status = Status.Progress;
                                                _locReturnSalesCollection.StatusDate = now;
                                                _locReturnSalesCollection.Save();
                                                _locReturnSalesCollection.Session.CommitTransaction();
                                            }
                                        }
                                    }
                                    SuccessMessageShow("Sales Return has been successfully updated to progress");
                                }
                                else
                                {
                                    ErrorMessageShow("Data Sales Return Not Available");
                                }

                            }
                            else
                            {
                                ErrorMessageShow("Data Sales Return Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesReturn " + ex.ToString());
            }
        }

        private void SalesReturnApprovalAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                ApplicationSetupDetail _locAppSetDetail = null;
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                var User = SecuritySystem.CurrentUserName;
                Session _currentSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currentSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                UserAccess _locUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", User));

                foreach (Object obj in _objectsToProcess)
                {
                    SalesReturn _objInNewObjectSpace = (SalesReturn)_objectSpace.GetObject(obj);

                    if (_objInNewObjectSpace != null)
                    {
                        if (_objInNewObjectSpace.Code != null)
                        {
                            _currObjectId = _objInNewObjectSpace.Code;
                        }
                    }

                    if (_currObjectId != null)
                    {
                        SalesReturn _locSalesReturnXPO = _currentSession.FindObject<SalesReturn>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("Code", _currObjectId)));

                        if (_locSalesReturnXPO != null)
                        {
                            if (_locSalesReturnXPO.Status == Status.Progress)
                            {
                                ApprovalLine _locApprovalLine = _currentSession.FindObject<ApprovalLine>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("SalesReturn", _locSalesReturnXPO),
                                                                             new BinaryOperator("EndApproval", true)));
                                if (_locApprovalLine == null)
                                {
                                    #region Approval Level 1
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level1)
                                    {
                                        _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level1, ObjectList.SalesReturn);
                                        if (_locAppSetDetail != null)
                                        {
                                            //Buat bs input langsung ke approvalline
                                            ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("SalesReturn", _locSalesReturnXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level1)));
                                            if (_locApprovalLineXPO == null)
                                            {

                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level1,
                                                        EndApproval = true,
                                                        SalesReturn = _locSalesReturnXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locSalesReturnXPO.ActivationPosting = true;
                                                    _locSalesReturnXPO.ActiveApproved1 = false;
                                                    _locSalesReturnXPO.ActiveApproved2 = false;
                                                    _locSalesReturnXPO.ActiveApproved3 = true;
                                                    _locSalesReturnXPO.Save();
                                                    _locSalesReturnXPO.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level1,
                                                        SalesReturn = _locSalesReturnXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locSalesReturnXPO.ActiveApproved1 = true;
                                                    _locSalesReturnXPO.ActiveApproved2 = false;
                                                    _locSalesReturnXPO.ActiveApproved3 = false;
                                                    _locSalesReturnXPO.Save();
                                                    _locSalesReturnXPO.Session.CommitTransaction();
                                                }

                                                //Send Email
                                                ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("SalesReturn", _locSalesReturnXPO)));

                                                if (_locApprovalLineXPO2 != null)
                                                {
                                                    SendEmail(_currentSession, _locSalesReturnXPO, Status.Progress, _locApprovalLineXPO2);
                                                }

                                                SuccessMessageShow("SalesReturn has successfully Approve");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 1

                                    #region Approval Level 2
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level2)
                                    {
                                        _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level2, ObjectList.SalesReturn);

                                        if (_locAppSetDetail != null)
                                        {
                                            ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("SalesReturn", _locSalesReturnXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level2)));

                                            if (_locApprovalLineXPO == null)
                                            {
                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level2,
                                                        EndApproval = true,
                                                        SalesReturn = _locSalesReturnXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locSalesReturnXPO.ActivationPosting = true;
                                                    _locSalesReturnXPO.ActiveApproved1 = false;
                                                    _locSalesReturnXPO.ActiveApproved2 = false;
                                                    _locSalesReturnXPO.ActiveApproved3 = true;
                                                    _locSalesReturnXPO.Save();
                                                    _locSalesReturnXPO.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level2,
                                                        SalesReturn = _locSalesReturnXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    SetApprovalLine(_currentSession, _locSalesReturnXPO, ApprovalLevel.Level1);

                                                    _locSalesReturnXPO.ActiveApproved1 = false;
                                                    _locSalesReturnXPO.ActiveApproved2 = true;
                                                    _locSalesReturnXPO.ActiveApproved3 = false;
                                                    _locSalesReturnXPO.Save();
                                                    _locSalesReturnXPO.Session.CommitTransaction();
                                                }

                                                //Send Email
                                                ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("SalesReturn", _locSalesReturnXPO)));

                                                if (_locApprovalLineXPO2 != null)
                                                {
                                                    SendEmail(_currentSession, _locSalesReturnXPO, Status.Progress, _locApprovalLineXPO2);
                                                }

                                                SuccessMessageShow("SalesReturn has successfully Approve");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 2

                                    #region Approval Level 3
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level3)
                                    {
                                        _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level3, ObjectList.SalesReturn);

                                        if (_locAppSetDetail != null)
                                        {
                                            ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("SalesReturn", _locSalesReturnXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level3)));

                                            if (_locApprovalLineXPO == null)
                                            {
                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level3,
                                                        EndApproval = true,
                                                        SalesReturn = _locSalesReturnXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locSalesReturnXPO.ActivationPosting = true;
                                                    _locSalesReturnXPO.ActiveApproved1 = false;
                                                    _locSalesReturnXPO.ActiveApproved2 = false;
                                                    _locSalesReturnXPO.ActiveApproved3 = true;
                                                    _locSalesReturnXPO.Save();
                                                    _locSalesReturnXPO.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level3,
                                                        SalesReturn = _locSalesReturnXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    SetApprovalLine(_currentSession, _locSalesReturnXPO, ApprovalLevel.Level2);
                                                    SetApprovalLine(_currentSession, _locSalesReturnXPO, ApprovalLevel.Level1);

                                                    _locSalesReturnXPO.ActiveApproved1 = false;
                                                    _locSalesReturnXPO.ActiveApproved2 = false;
                                                    _locSalesReturnXPO.ActiveApproved3 = true;
                                                    _locSalesReturnXPO.Save();
                                                    _locSalesReturnXPO.Session.CommitTransaction();
                                                }

                                                //Send Email
                                                ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("SalesReturn", _locSalesReturnXPO)));

                                                if (_locApprovalLineXPO2 != null)
                                                {
                                                    SendEmail(_currentSession, _locSalesReturnXPO, Status.Progress, _locApprovalLineXPO2);
                                                }

                                                SuccessMessageShow("SalesReturn has successfully Approve");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 3
                                }

                            }
                            else
                            {
                                ErrorMessageShow("SalesReturn Status Not Available");
                            }
                        }
                        else
                        {
                            ErrorMessageShow("SalesReturn Not Available");
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesReturn " + ex.ToString());
            }
        }

        private void SalesReturnPostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        SalesReturn _locSalesReturnOS = (SalesReturn)_objectSpace.GetObject(obj);

                        if (_locSalesReturnOS != null)
                        {
                            if (_locSalesReturnOS.Code != null)
                            {
                                _currObjectId = _locSalesReturnOS.Code;

                                SalesReturn _locSalesReturnXPO = _currSession.FindObject<SalesReturn>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locSalesReturnXPO != null)
                                {
                                    if (_locSalesReturnXPO.Status == Status.Progress || _locSalesReturnXPO.Status == Status.Posted)
                                    {
                                        SetSalesReturnMonitoring(_currSession, _locSalesReturnXPO);
                                        SetRemainQty(_currSession, _locSalesReturnXPO);
                                        SetPostingQty(_currSession, _locSalesReturnXPO);
                                        SetProcessCount(_currSession, _locSalesReturnXPO);
                                        SetStatusSalesReturnLine(_currSession, _locSalesReturnXPO);
                                        SetNormalQuantity(_currSession, _locSalesReturnXPO);
                                        SetFinalStatusSalesReturn(_currSession, _locSalesReturnXPO);
                                    }

                                    SuccessMessageShow("Sales Return has been successfully Posted");
                                }
                                else
                                {
                                    ErrorMessageShow("Data Sales Return Not Available");
                                }

                            }
                            else
                            {
                                ErrorMessageShow("Data Sales Return Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesReturn " + ex.ToString());
            }
        }

        private void SalesReturnFilterAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(SalesReturn)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesReturn " + ex.ToString());
            }
        }

        private void SalesReturnGetITOMAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;


                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        SalesReturn _locSalesReturnOS = (SalesReturn)_objectSpace.GetObject(obj);

                        if (_locSalesReturnOS != null)
                        {
                            if (_locSalesReturnOS.Code != null)
                            {
                                _currObjectId = _locSalesReturnOS.Code;

                                SalesReturn _locSalesReturnXPO = _currSession.FindObject<SalesReturn>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locSalesReturnXPO != null)
                                {
                                    if (_locSalesReturnXPO.Status == Status.Open || _locSalesReturnXPO.Status == Status.Progress)
                                    {
                                        XPCollection<ReturnSalesCollection> _locReturnSalesCollections = new XPCollection<ReturnSalesCollection>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("SalesReturn", _locSalesReturnXPO),
                                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("Status", Status.Open),
                                                                                        new BinaryOperator("Status", Status.Progress))));

                                        if (_locReturnSalesCollections != null && _locReturnSalesCollections.Count() > 0)
                                        {
                                            foreach (ReturnSalesCollection _locReturnSalesCollection in _locReturnSalesCollections)
                                            {
                                                if (_locReturnSalesCollection.SalesReturn != null && (_locReturnSalesCollection.InventoryTransferOut != null || _locReturnSalesCollection.SalesOrder != null))
                                                {
                                                    GetInventoryTransferOutMonitoring(_currSession, _locReturnSalesCollection.InventoryTransferOut, _locReturnSalesCollection.SalesOrder, _locSalesReturnXPO);
                                                }
                                            }
                                            SuccessMessageShow("InventoryTransferOut Has Been Successfully Getting into SalesReturn");
                                        }
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("InventoryTransferOut Order Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("InventoryTransferOut Order Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesReturn " + ex.ToString());
            }
        }

        //============================================ Code Only ==========================================

        #region GetITO

        private void GetInventoryTransferOutMonitoring(Session _currSession, InventoryTransferOut _locInventoryTransferOutXPO, SalesOrder _locSalesOrderXPO,SalesReturn _locSalesReturnXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _currSignCode = null;
                double _locTxValue = 0;
                double _locTxAmount = 0;
                double _locDisc = 0;
                double _locDiscAmount = 0;
                double _locTAmount = 0;
                XPCollection<InventoryTransferOutMonitoring> _locInventoryTransferOutMonitorings = null;

                if (_locSalesReturnXPO != null)
                {
                    //Harus mampu menghitung otomatis buat MultiTax dan MultiDiscount
                    if(_locInventoryTransferOutXPO != null && _locSalesOrderXPO != null)
                    {
                        _locInventoryTransferOutMonitorings = new XPCollection<InventoryTransferOutMonitoring>
                                                         (_currSession, new GroupOperator(GroupOperatorType.And,
                                                          new BinaryOperator("InventoryTransferOut", _locInventoryTransferOutXPO),
                                                          new BinaryOperator("SalesOrderMonitoring.SalesOrder", _locSalesOrderXPO),
                                                          new BinaryOperator("SalesReturn", _locSalesReturnXPO),
                                                          new BinaryOperator("Select", true),
                                                          new GroupOperator(GroupOperatorType.Or,
                                                          new BinaryOperator("ReturnType", ReturnType.ReplacedFull),
                                                          new BinaryOperator("ReturnType", ReturnType.DebitMemo))
                                                          ));

                    }else if(_locInventoryTransferOutXPO != null && _locSalesOrderXPO == null)
                    {
                        _locInventoryTransferOutMonitorings = new XPCollection<InventoryTransferOutMonitoring>
                                                         (_currSession, new GroupOperator(GroupOperatorType.And,
                                                          new BinaryOperator("InventoryTransferOut", _locInventoryTransferOutXPO),
                                                          new BinaryOperator("SalesReturn", _locSalesReturnXPO),
                                                          new BinaryOperator("Select", true),
                                                          new GroupOperator(GroupOperatorType.Or,
                                                          new BinaryOperator("ReturnType", ReturnType.ReplacedFull),
                                                          new BinaryOperator("ReturnType", ReturnType.DebitMemo))
                                                          ));
                    }else if(_locInventoryTransferOutXPO == null && _locSalesOrderXPO != null)
                    {
                        _locInventoryTransferOutMonitorings = new XPCollection<InventoryTransferOutMonitoring>
                                                         (_currSession, new GroupOperator(GroupOperatorType.And,
                                                          new BinaryOperator("SalesOrderMonitoring.SalesOrder", _locSalesOrderXPO),
                                                          new BinaryOperator("SalesReturn", _locSalesReturnXPO),
                                                          new BinaryOperator("Select", true),
                                                          new GroupOperator(GroupOperatorType.Or,
                                                          new BinaryOperator("ReturnType", ReturnType.ReplacedFull),
                                                          new BinaryOperator("ReturnType", ReturnType.DebitMemo))
                                                          ));
                    }
                    

                    if (_locInventoryTransferOutMonitorings != null && _locInventoryTransferOutMonitorings.Count() > 0)
                    {
                        foreach (InventoryTransferOutMonitoring _locInventoryTransferOutMonitoring in _locInventoryTransferOutMonitorings)
                        {
                            if (_locInventoryTransferOutMonitoring.InventoryTransferOutLine != null && _locInventoryTransferOutMonitoring.SalesOrderMonitoring != null
                                && _locInventoryTransferOutMonitoring.SalesOrderMonitoring.SalesOrderLine != null)
                            {
                                _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.SalesReturnLine);

                                if (_currSignCode != null)
                                {
                                    #region CountTax
                                    if (_locInventoryTransferOutMonitoring.SalesOrderMonitoring.MultiTax == true)
                                    {
                                        _locTxValue = GetSumTotalTaxValue(_currSession, _locInventoryTransferOutMonitoring.SalesOrderMonitoring.SalesOrderLine);
                                        _locTxAmount = GetSumTotalTaxAmount(_currSession, _locInventoryTransferOutMonitoring.SalesOrderMonitoring.SalesOrderLine, _locInventoryTransferOutMonitoring);
                                    }
                                    else
                                    {
                                        if (_locInventoryTransferOutMonitoring.SalesOrderMonitoring.TxValue > 0)
                                        {
                                            _locTxValue = _locInventoryTransferOutMonitoring.SalesOrderMonitoring.SalesOrderLine.TxValue;
                                            _locTxAmount = (_locTxValue / 100) * (_locInventoryTransferOutMonitoring.TQty * _locInventoryTransferOutMonitoring.SalesOrderMonitoring.UAmount);
                                        }
                                    }
                                    #endregion CountTax

                                    #region CountDiscount
                                    if (_locInventoryTransferOutMonitoring.SalesOrderMonitoring.MultiDiscount == true)
                                    {
                                        _locDisc = GetSumTotalDisc(_currSession, _locInventoryTransferOutMonitoring.SalesOrderMonitoring.SalesOrderLine);
                                        _locDiscAmount = GetSumTotalDiscAmount(_currSession, _locInventoryTransferOutMonitoring.SalesOrderMonitoring.SalesOrderLine, _locInventoryTransferOutMonitoring);
                                    }
                                    else
                                    {
                                        if (_locInventoryTransferOutMonitoring.SalesOrderMonitoring.Disc > 0)
                                        {
                                            _locDisc = _locInventoryTransferOutMonitoring.SalesOrderMonitoring.SalesOrderLine.Disc;
                                            _locDiscAmount = (_locDisc / 100) * (_locInventoryTransferOutMonitoring.TQty * _locInventoryTransferOutMonitoring.SalesOrderMonitoring.UAmount);
                                        }
                                    }
                                    #endregion CountDiscount

                                    _locTAmount = (_locInventoryTransferOutMonitoring.SalesOrderMonitoring.UAmount * _locInventoryTransferOutMonitoring.TQty) + _locTxAmount - _locDiscAmount;
                                    //Tempat untuk menghitung multitax dan multidiscount
                                    SalesReturnLine _saveDataSalesReturnLine = new SalesReturnLine(_currSession)
                                    {
                                        SignCode = _currSignCode,
                                        OrderType = _locInventoryTransferOutMonitoring.SalesOrderMonitoring.SalesOrderLine.SalesType,
                                        Item = _locInventoryTransferOutMonitoring.Item,
                                        Description = _locInventoryTransferOutMonitoring.InventoryTransferOutLine.Description,
                                        DQty = _locInventoryTransferOutMonitoring.DQty,
                                        DUOM = _locInventoryTransferOutMonitoring.DUOM,
                                        Qty = _locInventoryTransferOutMonitoring.Qty,
                                        UOM = _locInventoryTransferOutMonitoring.UOM,
                                        TQty = _locInventoryTransferOutMonitoring.TQty,
                                        Currency = _locInventoryTransferOutMonitoring.SalesOrderMonitoring.Currency,
                                        PriceGroup = _locInventoryTransferOutMonitoring.SalesOrderMonitoring.PriceGroup,
                                        Price = _locInventoryTransferOutMonitoring.SalesOrderMonitoring.Price,
                                        PriceLine = _locInventoryTransferOutMonitoring.SalesOrderMonitoring.PriceLine,
                                        UAmount = _locInventoryTransferOutMonitoring.SalesOrderMonitoring.UAmount,
                                        TUAmount = _locInventoryTransferOutMonitoring.TQty * _locInventoryTransferOutMonitoring.SalesOrderMonitoring.UAmount,
                                        MultiTax = _locInventoryTransferOutMonitoring.SalesOrderMonitoring.MultiTax,
                                        Tax = _locInventoryTransferOutMonitoring.SalesOrderMonitoring.Tax,
                                        TxValue = _locTxValue,
                                        TxAmount = _locTxAmount,
                                        MultiDiscount = _locInventoryTransferOutMonitoring.SalesOrderMonitoring.MultiDiscount,
                                        Discount = _locInventoryTransferOutMonitoring.SalesOrderMonitoring.Discount,
                                        Disc = _locDisc,
                                        DiscAmount = _locDiscAmount,
                                        TAmount = _locTAmount,
                                        InventoryTransferOutMonitoring = _locInventoryTransferOutMonitoring,
                                        SalesOrderMonitoring = _locInventoryTransferOutMonitoring.SalesOrderMonitoring,
                                        SalesReturn = _locSalesReturnXPO,
                                        ReturnType = _locInventoryTransferOutMonitoring.ReturnType,
                                    };
                                    _saveDataSalesReturnLine.Save();
                                    _saveDataSalesReturnLine.Session.CommitTransaction();

                                    SalesReturnLine _locSalReturnLine = _currSession.FindObject<SalesReturnLine>
                                                                (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("SignCode", _currSignCode)));
                                    if (_locSalReturnLine != null)
                                    {
                                        #region TaxLine
                                        if (_locInventoryTransferOutMonitoring.SalesOrderMonitoring.SalesOrderLine.MultiTax == true)
                                        {
                                            XPCollection<TaxLine> _locTaxLines = new XPCollection<TaxLine>(_currSession, new GroupOperator(
                                                                                GroupOperatorType.And,
                                                                                new BinaryOperator("SalesOrderLine", _locInventoryTransferOutMonitoring.SalesOrderMonitoring.SalesOrderLine)));
                                            if (_locTaxLines != null && _locTaxLines.Count() > 0)
                                            {
                                                foreach (TaxLine _locTaxLine in _locTaxLines)
                                                {
                                                    TaxLine _saveDataTaxLine = new TaxLine(_currSession)
                                                    {
                                                        Tax = _locTaxLine.Tax,
                                                        Name = _locTaxLine.Name,
                                                        TaxType = _locTaxLine.TaxType,
                                                        TaxGroup = _locTaxLine.TaxGroup,
                                                        TaxGroupType = _locTaxLine.TaxGroupType,
                                                        TaxAccountGroup = _locTaxLine.TaxAccountGroup,
                                                        TaxNature = _locTaxLine.TaxNature,
                                                        TaxMode = _locTaxLine.TaxMode,
                                                        TxValue = _locTaxLine.TxValue,
                                                        Description = _locTaxLine.Description,
                                                        UAmount = _locSalReturnLine.UAmount,
                                                        TUAmount = _locSalReturnLine.TUAmount,
                                                        TxAmount = _locSalReturnLine.TxAmount,
                                                        SalesReturnLine = _locSalReturnLine,
                                                    };
                                                    _saveDataTaxLine.Save();
                                                    _saveDataTaxLine.Session.CommitTransaction();
                                                }
                                            }
                                        }
                                        #endregion TaxLine
                                        #region DiscountLine
                                        if (_locInventoryTransferOutMonitoring.SalesOrderMonitoring.SalesOrderLine.MultiDiscount == true)
                                        {
                                            XPCollection<DiscountLine> _locDiscountLines = new XPCollection<DiscountLine>(_currSession,
                                                                                new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("SalesOrderLine", _locInventoryTransferOutMonitoring.SalesOrderMonitoring.SalesOrderLine)));
                                            if (_locDiscountLines != null && _locDiscountLines.Count() > 0)
                                            {
                                                foreach (DiscountLine _locDiscountLine in _locDiscountLines)
                                                {
                                                    DiscountLine _saveDataDiscountLine = new DiscountLine(_currSession)
                                                    {
                                                        Discount = _locDiscountLine.Discount,
                                                        Name = _locDiscountLine.Name,
                                                        DiscountMode = _locDiscountLine.DiscountMode,
                                                        Description = _locDiscountLine.Description,
                                                        UAmount = _locSalReturnLine.UAmount,
                                                        Disc = _locDiscountLine.Disc,
                                                        DiscAmount = _locSalReturnLine.DiscAmount,
                                                        TUAmount = _locSalReturnLine.TUAmount,
                                                        SalesReturnLine = _locSalReturnLine,
                                                    };
                                                    _saveDataDiscountLine.Save();
                                                    _saveDataDiscountLine.Session.CommitTransaction();
                                                }
                                            }
                                        }
                                        #endregion DiscountLine

                                        _locInventoryTransferOutMonitoring.SalesReturnLine = _locSalReturnLine;
                                        _locInventoryTransferOutMonitoring.ReturnStatus = Status.Close;
                                        _locInventoryTransferOutMonitoring.ReturnStatusDate = now;
                                        _locInventoryTransferOutMonitoring.Save();
                                        _locInventoryTransferOutMonitoring.Session.CommitTransaction();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = SalesReturn ", ex.ToString());
            }
        }

        private double GetSumTotalTaxValue(Session _currSession, SalesOrderLine _salesOrderLineXPO)
        {
            double _result = 0;
            try
            {
                double _locTxValue = 0;

                if (_salesOrderLineXPO != null)
                {
                    XPCollection<TaxLine> _locTaxLines = new XPCollection<TaxLine>(_currSession,
                                                     new BinaryOperator("SalesOrderLine", _salesOrderLineXPO));

                    if (_locTaxLines.Count() > 0)
                    {
                        foreach (TaxLine _locTaxLine in _locTaxLines)
                        {
                            if (_locTaxLine.TxValue > 0 && _locTaxLine.TxAmount > 0)
                            {
                                if (_locTaxLine.TaxNature == TaxNature.Increase)
                                {
                                    _locTxValue = _locTxValue + _locTaxLine.TxValue;

                                }
                                else if (_locTaxLine.TaxNature == TaxNature.Decrease)
                                {
                                    _locTxValue = _locTxValue - _locTaxLine.TxValue;
                                }
                            }
                        }
                        _result = _locTxValue;
                    }
                }


            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = SalesReturn " + ex.ToString());
            }
            return _result;
        }

        private double GetSumTotalTaxAmount(Session _currSession, SalesOrderLine _salesOrderLineXPO, InventoryTransferOutMonitoring _inventoryTransferOutMonitoringXPO)
        {
            double _return = 0;
            try
            {
                double _locTxAmount = 0;

                if (_salesOrderLineXPO != null)
                {
                    XPCollection<TaxLine> _locTaxLines = new XPCollection<TaxLine>(_currSession,
                                                     new BinaryOperator("SalesOrderLine", _salesOrderLineXPO));

                    if (_locTaxLines.Count() > 0)
                    {
                        foreach (TaxLine _locTaxLine in _locTaxLines)
                        {
                            if (_locTaxLine.TxValue > 0)
                            {
                                if (_locTaxLine.TaxNature == TaxNature.Increase)
                                {
                                    _locTxAmount = _locTxAmount + (_salesOrderLineXPO.UAmount * _inventoryTransferOutMonitoringXPO.TQty * _locTaxLine.TxValue / 100);
                                }
                                else if (_locTaxLine.TaxNature == TaxNature.Decrease)
                                {
                                    _locTxAmount = _locTxAmount - (_salesOrderLineXPO.UAmount * _inventoryTransferOutMonitoringXPO.TQty * _locTaxLine.TxValue / 100);
                                }
                            }
                        }
                        _return = _locTxAmount;
                    }
                }


            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = SalesReturn " + ex.ToString());
            }
            return _return;
        }

        private double GetSumTotalDisc(Session _currSession, SalesOrderLine _salesOrderLineXPO)
        {
            double _result = 0;
            try
            {
                double _locDisc = 0;


                if (_salesOrderLineXPO != null)
                {
                    XPCollection<DiscountLine> _locDiscountLines = new XPCollection<DiscountLine>(_currSession,
                                                               new BinaryOperator("SalesOrderLine", _salesOrderLineXPO));

                    if (_locDiscountLines.Count() > 0)
                    {
                        foreach (DiscountLine _locDiscountLine in _locDiscountLines)
                        {
                            if (_locDiscountLine.Disc > 0 && _locDiscountLine.DiscAmount > 0)
                            {
                                if (_locDiscountLine.Disc > 0 && _locDiscountLine.DiscAmount > 0)
                                {
                                    _locDisc = _locDisc + _locDiscountLine.Disc;
                                }
                            }
                        }
                        _result = _locDisc;
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = SalesReturn " + ex.ToString());
            }
            return _result;
        }

        private double GetSumTotalDiscAmount(Session _currSession, SalesOrderLine _salesOrderLineXPO, InventoryTransferOutMonitoring _inventoryTransferOutMonitoringXPO)
        {
            double _result = 0;
            try
            {
                double _locDiscAmount = 0;

                if (_salesOrderLineXPO != null)
                {
                    XPCollection<DiscountLine> _locDiscountLines = new XPCollection<DiscountLine>(_currSession,
                                                               new BinaryOperator("SalesOrderLine", _salesOrderLineXPO));

                    if (_locDiscountLines.Count() > 0)
                    {
                        foreach (DiscountLine _locDiscountLine in _locDiscountLines)
                        {
                            if (_locDiscountLine.Disc > 0)
                            {
                                _locDiscAmount = _locDiscAmount + (_salesOrderLineXPO.UAmount * _inventoryTransferOutMonitoringXPO.TQty * _locDiscountLine.Disc / 100);
                            }
                        }
                        _result = _locDiscAmount;
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = SalesReturn " + ex.ToString());
            }
            return _result;
        }

        #endregion GetITO

        #region Posting

        private void SetSalesReturnMonitoring(Session _currSession, SalesReturn _locSalesReturnXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locSalesReturnXPO != null)
                {
                    XPCollection<SalesReturnLine> _locSalesReturnLines = new XPCollection<SalesReturnLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("SalesReturn", _locSalesReturnXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Progress),
                                                            new BinaryOperator("Status", Status.Posted))));

                    if (_locSalesReturnLines != null && _locSalesReturnLines.Count() > 0)
                    {
                        foreach (SalesReturnLine _locSalesReturnLine in _locSalesReturnLines)
                        {
                            SalesReturnMonitoring _saveDataSalesReturnMonitoring = new SalesReturnMonitoring(_currSession)
                            {
                                SalesReturn = _locSalesReturnXPO,
                                SalesReturnLine = _locSalesReturnLine,
                                OrderType = _locSalesReturnLine.OrderType,
                                Item = _locSalesReturnLine.Item,
                                DQty = _locSalesReturnLine.DQty,
                                DUOM = _locSalesReturnLine.DUOM,
                                Qty = _locSalesReturnLine.Qty,
                                TQty = _locSalesReturnLine.TQty,
                                UOM = _locSalesReturnLine.UOM,
                                Currency = _locSalesReturnLine.Currency,
                                PriceGroup = _locSalesReturnLine.PriceGroup,
                                Price = _locSalesReturnLine.Price,
                                PriceLine = _locSalesReturnLine.PriceLine,
                                UAmount = _locSalesReturnLine.UAmount,
                                TUAmount = _locSalesReturnLine.TUAmount,
                                MultiTax = _locSalesReturnLine.MultiTax,
                                Tax = _locSalesReturnLine.Tax,
                                TxValue = _locSalesReturnLine.TxValue,
                                TxAmount = _locSalesReturnLine.TxAmount,
                                MultiDiscount = _locSalesReturnLine.MultiDiscount,
                                Discount = _locSalesReturnLine.Discount,
                                Disc = _locSalesReturnLine.Disc,
                                DiscAmount = _locSalesReturnLine.DiscAmount,
                                TAmount = _locSalesReturnLine.TAmount,
                                ReturnType = _locSalesReturnLine.ReturnType,
                            };
                            _saveDataSalesReturnMonitoring.Save();
                            _saveDataSalesReturnMonitoring.Session.CommitTransaction();
                            
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesReturn " + ex.ToString());
            }
        }

        private void SetRemainQty(Session _currSession, SalesReturn _locSalesReturnXPO)
        {
            try
            {
                if (_locSalesReturnXPO != null)
                {
                    XPCollection<SalesReturnLine> _locSalesReturnLines = new XPCollection<SalesReturnLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("SalesReturn", _locSalesReturnXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locSalesReturnLines != null && _locSalesReturnLines.Count > 0)
                    {
                        double _locRmDQty = 0;
                        double _locRmQty = 0;
                        double _locInvLineTotal = 0;
                        ItemUnitOfMeasure _locItemUOM = null;

                        foreach (SalesReturnLine _locSalesReturnLine in _locSalesReturnLines)
                        {
                            #region ProcessCount=0
                            if (_locSalesReturnLine.PostedCount == 0)
                            {
                                #region MaxQuantity
                                if (_locSalesReturnLine.MxDQty > 0)
                                {
                                    if (_locSalesReturnLine.DQty > 0 && _locSalesReturnLine.DQty <= _locSalesReturnLine.MxDQty)
                                    {
                                        _locRmDQty = _locSalesReturnLine.MxDQty - _locSalesReturnLine.DQty;
                                    }

                                    if (_locSalesReturnLine.Qty > 0 && _locSalesReturnLine.Qty <= _locSalesReturnLine.MxQty)
                                    {
                                        _locRmQty = _locSalesReturnLine.MxQty - _locSalesReturnLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locSalesReturnLine.Item),
                                                                new BinaryOperator("UOM", _locSalesReturnLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locSalesReturnLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty + _locRmDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }

                                }
                                #endregion MaxQuantity
                                #region NonMaxQuantity
                                else
                                {
                                    if (_locSalesReturnLine.DQty > 0)
                                    {
                                        _locRmDQty = 0;
                                    }

                                    if (_locSalesReturnLine.Qty > 0)
                                    {
                                        _locRmQty = 0;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locSalesReturnLine.Item),
                                                                new BinaryOperator("UOM", _locSalesReturnLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locSalesReturnLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty + _locRmDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion ProcessCount=0

                            #region ProcessCount>0
                            if (_locSalesReturnLine.PostedCount > 0)
                            {
                                if (_locSalesReturnLine.RmDQty > 0)
                                {
                                    _locRmDQty = _locSalesReturnLine.RmDQty - _locSalesReturnLine.DQty;
                                }

                                if (_locSalesReturnLine.RmQty > 0)
                                {
                                    _locRmQty = _locSalesReturnLine.RmQty - _locSalesReturnLine.Qty;
                                }

                                if (_locSalesReturnLine.MxDQty > 0 || _locSalesReturnLine.MxQty > 0)
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locSalesReturnLine.Item),
                                                                new BinaryOperator("UOM", _locSalesReturnLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locSalesReturnLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                }
                                else
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locSalesReturnLine.Item),
                                                                new BinaryOperator("UOM", _locSalesReturnLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locSalesReturnLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                }


                                if (_locItemUOM != null)
                                {
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locRmQty + _locRmDQty;
                                }

                            }
                            #endregion ProcessCount>0

                            _locSalesReturnLine.RmDQty = _locRmDQty;
                            _locSalesReturnLine.RmQty = _locRmQty;
                            _locSalesReturnLine.RmTQty = _locInvLineTotal;
                            _locSalesReturnLine.Save();
                            _locSalesReturnLine.Session.CommitTransaction();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrder " + ex.ToString());
            }
        }

        private void SetPostingQty(Session _currSession, SalesReturn _locSalesReturnXPO)
        {
            try
            {
                if (_locSalesReturnXPO != null)
                {
                    XPCollection<SalesReturnLine> _locSalesReturnLines = new XPCollection<SalesReturnLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("SalesReturn", _locSalesReturnXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locSalesReturnLines != null && _locSalesReturnLines.Count > 0)
                    {
                        double _locPDQty = 0;
                        double _locPQty = 0;
                        double _locInvLineTotal = 0;
                        ItemUnitOfMeasure _locItemUOM = null;

                        foreach (SalesReturnLine _locSalesReturnLine in _locSalesReturnLines)
                        {
                            #region ProcessCount=0
                            if (_locSalesReturnLine.PostedCount == 0)
                            {
                                #region MaxQuantity
                                if (_locSalesReturnLine.MxDQty > 0)
                                {
                                    if (_locSalesReturnLine.DQty > 0 && _locSalesReturnLine.DQty <= _locSalesReturnLine.MxDQty)
                                    {
                                        _locPDQty = _locSalesReturnLine.DQty;
                                    }

                                    if (_locSalesReturnLine.Qty > 0 && _locSalesReturnLine.Qty <= _locSalesReturnLine.MxQty)
                                    {
                                        _locPQty = _locSalesReturnLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locSalesReturnLine.Item),
                                                                new BinaryOperator("UOM", _locSalesReturnLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locSalesReturnLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty + _locPDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }

                                }
                                #endregion MaxQuantity
                                #region NonMaxQuantity
                                else
                                {
                                    if (_locSalesReturnLine.DQty > 0)
                                    {
                                        _locPDQty = _locSalesReturnLine.DQty;
                                    }

                                    if (_locSalesReturnLine.Qty > 0)
                                    {
                                        _locPQty = _locSalesReturnLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locSalesReturnLine.Item),
                                                                new BinaryOperator("UOM", _locSalesReturnLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locSalesReturnLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty + _locPDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion ProcessCount=0

                            #region ProcessCount>0
                            if (_locSalesReturnLine.PostedCount > 0)
                            {
                                if (_locSalesReturnLine.PDQty > 0)
                                {
                                    _locPDQty = _locSalesReturnLine.PDQty + _locSalesReturnLine.DQty;
                                }

                                if (_locSalesReturnLine.PQty > 0)
                                {
                                    _locPQty = _locSalesReturnLine.PQty + _locSalesReturnLine.Qty;
                                }

                                if (_locSalesReturnLine.MxDQty > 0 || _locSalesReturnLine.MxQty > 0)
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                           (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Item", _locSalesReturnLine.Item),
                                                            new BinaryOperator("UOM", _locSalesReturnLine.MxUOM),
                                                            new BinaryOperator("DefaultUOM", _locSalesReturnLine.MxDUOM),
                                                            new BinaryOperator("Active", true)));
                                }
                                else
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                           (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Item", _locSalesReturnLine.Item),
                                                            new BinaryOperator("UOM", _locSalesReturnLine.UOM),
                                                            new BinaryOperator("DefaultUOM", _locSalesReturnLine.DUOM),
                                                            new BinaryOperator("Active", true)));
                                }

                                if (_locItemUOM != null)
                                {
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locPQty + _locPDQty;
                                }

                            }
                            #endregion ProcessCount>0

                            _locSalesReturnLine.PDQty = _locPDQty;
                            _locSalesReturnLine.PDUOM = _locSalesReturnLine.DUOM;
                            _locSalesReturnLine.PQty = _locPQty;
                            _locSalesReturnLine.PUOM = _locSalesReturnLine.UOM;
                            _locSalesReturnLine.PTQty = _locInvLineTotal;
                            _locSalesReturnLine.Save();
                            _locSalesReturnLine.Session.CommitTransaction();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesReturn " + ex.ToString());
            }

        }

        private void SetProcessCount(Session _currSession, SalesReturn _locSalesReturnXPO)
        {
            try
            {
                if (_locSalesReturnXPO != null)
                {
                    XPCollection<SalesReturnLine> _locSalesReturnLines = new XPCollection<SalesReturnLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("SalesReturn", _locSalesReturnXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locSalesReturnLines != null && _locSalesReturnLines.Count > 0)
                    {

                        foreach (SalesReturnLine _locSalesReturnLine in _locSalesReturnLines)
                        {
                            if (_locSalesReturnLine.Status == Status.Progress || _locSalesReturnLine.Status == Status.Posted)
                            {
                                _locSalesReturnLine.PostedCount = _locSalesReturnLine.PostedCount + 1;
                                _locSalesReturnLine.Save();
                                _locSalesReturnLine.Session.CommitTransaction();
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesReturn " + ex.ToString());
            }
        }

        private void SetStatusSalesReturnLine(Session _currSession, SalesReturn _locSalesReturnXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locSalesReturnXPO != null)
                {
                    XPCollection<SalesReturnLine> _locSalesReturnLines = new XPCollection<SalesReturnLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("SalesReturn", _locSalesReturnXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locSalesReturnLines != null && _locSalesReturnLines.Count > 0)
                    {

                        foreach (SalesReturnLine _locSalesReturnLine in _locSalesReturnLines)
                        {
                            if (_locSalesReturnLine.Status == Status.Progress || _locSalesReturnLine.Status == Status.Posted)
                            {
                                if (_locSalesReturnLine.RmDQty == 0 && _locSalesReturnLine.RmQty == 0 && _locSalesReturnLine.RmTQty == 0)
                                {
                                    _locSalesReturnLine.Status = Status.Close;
                                    _locSalesReturnLine.ActivationPosting = true;
                                    _locSalesReturnLine.ActivationQuantity = true;
                                    _locSalesReturnLine.StatusDate = now;
                                }
                                else
                                {
                                    _locSalesReturnLine.Status = Status.Posted;
                                    _locSalesReturnLine.StatusDate = now;
                                }
                                _locSalesReturnLine.Save();
                                _locSalesReturnLine.Session.CommitTransaction();
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesReturn " + ex.ToString());
            }
        }

        private void SetNormalQuantity(Session _currSession, SalesReturn _locSalesReturnXPO)
        {
            try
            {
                if (_locSalesReturnXPO != null)
                {
                    XPCollection<SalesReturnLine> _locSalesReturnLines = new XPCollection<SalesReturnLine>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("SalesReturn", _locSalesReturnXPO),
                                                                                     new BinaryOperator("Select", true)));

                    if (_locSalesReturnLines != null && _locSalesReturnLines.Count > 0)
                    {
                        foreach (SalesReturnLine _locSalesReturnLine in _locSalesReturnLines)
                        {
                            if (_locSalesReturnLine.Status == Status.Progress || _locSalesReturnLine.Status == Status.Posted || _locSalesReturnLine.Status == Status.Close)
                            {
                                if (_locSalesReturnLine.DQty > 0 || _locSalesReturnLine.Qty > 0)
                                {
                                    _locSalesReturnLine.Select = false;
                                    _locSalesReturnLine.DQty = 0;
                                    _locSalesReturnLine.Qty = 0;
                                    _locSalesReturnLine.Save();
                                    _locSalesReturnLine.Session.CommitTransaction();
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesReturn " + ex.ToString());
            }
        }

        private void SetFinalStatusSalesReturn(Session _currSession, SalesReturn _locSalesReturnXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locStatusCount = 0;
                int _locSalesQuotationLineCount = 0;

                if (_locSalesReturnXPO != null)
                {

                    XPCollection<SalesReturnLine> _locSalesReturnLines = new XPCollection<SalesReturnLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("SalesReturn", _locSalesReturnXPO)));

                    if (_locSalesReturnLines != null && _locSalesReturnLines.Count() > 0)
                    {
                        _locSalesQuotationLineCount = _locSalesReturnLines.Count();

                        foreach (SalesReturnLine _locSalesReturnLine in _locSalesReturnLines)
                        {
                            if (_locSalesReturnLine.Status == Status.Close)
                            {
                                _locStatusCount = _locStatusCount + 1;
                            }
                        }
                    }

                    if (_locStatusCount == _locSalesQuotationLineCount)
                    {
                        _locSalesReturnXPO.ActivationPosting = true;
                        _locSalesReturnXPO.Status = Status.Close;
                        _locSalesReturnXPO.StatusDate = now;
                        _locSalesReturnXPO.Save();
                        _locSalesReturnXPO.Session.CommitTransaction();
                    }
                    else
                    {
                        _locSalesReturnXPO.Status = Status.Posted;
                        _locSalesReturnXPO.StatusDate = now;
                        _locSalesReturnXPO.Save();
                        _locSalesReturnXPO.Session.CommitTransaction();
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesReturn " + ex.ToString());
            }
        }

        #endregion Posting

        #region Approval Process

        private void SetApprovalLine(Session _currentSession, SalesReturn _locSalesReturnXPO, ApprovalLevel _locApprovalLevel)
        {
            try
            {
                DateTime now = DateTime.Now;

                ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("SalesReturn", _locSalesReturnXPO),
                                                                         new BinaryOperator("ApprovalLevel", _locApprovalLevel)));

                if (_locApprovalLineXPO == null)
                {
                    ApprovalLine _saveDataAL2 = new ApprovalLine(_currentSession)
                    {
                        ApprovalDate = now,
                        ApprovalStatus = Status.Approved,
                        ApprovalLevel = _locApprovalLevel,
                        SalesReturn = _locSalesReturnXPO,
                    };
                    _saveDataAL2.Save();
                    _saveDataAL2.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesReturn " + ex.ToString());
            }
        }

        #endregion Approval Process

        #region Email

        private string BackgroundBody(Session _currentSession, SalesReturn _locSalesReturnXPO, MailSetupDetail _locMailSetDetail)
        {
            string body = string.Empty;

            if(_locSalesReturnXPO != null)
            {
                ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                               (new GroupOperator(GroupOperatorType.And,
                                                new BinaryOperator("SalesReturn", _locSalesReturnXPO)));

                string Status = _locApprovalLineXPO.ApprovalStatus.ToString();

                using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Email/EmailTemplate.html")))
                {
                    body = reader.ReadToEnd();
                }
                body = body.Replace("{Code}", _locSalesReturnXPO.Code);

                #region Level
                if (_locSalesReturnXPO.ActiveApproved1 == true)
                {
                    string Active = "Level 1";
                    body = body.Replace("{Level}", Active);
                }
                else if (_locSalesReturnXPO.ActiveApproved2 == true)
                {
                    string Active = "Level 2";
                    body = body.Replace("{Level}", Active);
                }
                else if (_locSalesReturnXPO.ActiveApproved3 == true)
                {
                    string Active = "Level 3";
                    body = body.Replace("{Level}", Active);
                }
                else
                {
                    string Active = "Not Available";
                    body = body.Replace("{Level}", Active);
                }
                #endregion Level

                body = body.Replace("{Status}", Status);
                body = body.Replace("{Message}", _locMailSetDetail.MessageBody);
            }

            return body;
        }

        protected void SendEmail(Session _currentSession, SalesReturn _locSalesReturnXPO, Status _locStatus, ApprovalLine _locApprovalLineXPO2)
        {
            DateTime now = DateTime.Now;
            var User = SecuritySystem.CurrentUserName;
            GlobalFunction _globFunc = new GlobalFunction();
            UserAccess _numLineUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", User));
            string _locMessageBody = null;

            try
            {
                if (_locSalesReturnXPO != null)
                {
                    XPCollection<MailSetupDetail> _locMailSetDetails = new XPCollection<MailSetupDetail>
                                                                       (_currentSession, new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("ObjectList", ObjectList.SalesReturn),
                                                                        new BinaryOperator("UserAccess", _numLineUserAccess),
                                                                        new BinaryOperator("Active", true)));

                    if (_locMailSetDetails != null && _locMailSetDetails.Count > 0)
                    {
                        foreach (MailSetupDetail _locMailSetDetail in _locMailSetDetails)
                        {
                            if (_locMailSetDetail.MessageBody != null)
                            {
                                if (_locApprovalLineXPO2.EndApproval == true)
                                {
                                    _locMessageBody = this.BackgroundBody(_currentSession, _locSalesReturnXPO, _locMailSetDetail);
                                }
                                else
                                {
                                    _locMessageBody = this.BackgroundBody(_currentSession, _locSalesReturnXPO, _locMailSetDetail);
                                }
                            }

                            _globFunc.SetAndSendMail(_locMailSetDetail.SmtpHost, _locMailSetDetail.SmtpPort,
                            _locMailSetDetail.MailFrom, _locMailSetDetail.MailFromPassword, _locMailSetDetail.MailTo, _locMailSetDetail.MailSubject, _locMessageBody);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesReturn " + ex.ToString());
            }
        }

        #endregion Email

        #region Global Method

        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0} !", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }


        #endregion Global Method

        
    }
}
