﻿namespace FullDrive.Module.Controllers
{
    partial class PurchaseInvoiceLineActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.PurchaseInvoiceLineTaxCalculationAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PurchaseInvoiceLineDiscountCalculationAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PurchaseInvoiceLineListviewFilterSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // PurchaseInvoiceLineTaxCalculationAction
            // 
            this.PurchaseInvoiceLineTaxCalculationAction.Caption = "Tax Calculation";
            this.PurchaseInvoiceLineTaxCalculationAction.ConfirmationMessage = null;
            this.PurchaseInvoiceLineTaxCalculationAction.Id = "PurchaseInvoiceLineTaxCalculationActionId";
            this.PurchaseInvoiceLineTaxCalculationAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchaseInvoiceLine);
            this.PurchaseInvoiceLineTaxCalculationAction.ToolTip = null;
            this.PurchaseInvoiceLineTaxCalculationAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PurchaseInvoiceLineTaxCalculationAction_Execute);
            // 
            // PurchaseInvoiceLineDiscountCalculationAction
            // 
            this.PurchaseInvoiceLineDiscountCalculationAction.Caption = "Discount Calculation";
            this.PurchaseInvoiceLineDiscountCalculationAction.ConfirmationMessage = null;
            this.PurchaseInvoiceLineDiscountCalculationAction.Id = "PurchaseInvoiceLineDiscountCalculationActionId";
            this.PurchaseInvoiceLineDiscountCalculationAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchaseInvoiceLine);
            this.PurchaseInvoiceLineDiscountCalculationAction.ToolTip = null;
            this.PurchaseInvoiceLineDiscountCalculationAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PurchaseInvoiceLineDiscountCalculationAction_Execute);
            // 
            // PurchaseInvoiceLineListviewFilterSelectionAction
            // 
            this.PurchaseInvoiceLineListviewFilterSelectionAction.Caption = "Filter";
            this.PurchaseInvoiceLineListviewFilterSelectionAction.ConfirmationMessage = null;
            this.PurchaseInvoiceLineListviewFilterSelectionAction.Id = "PurchaseInvoiceLineListviewFilterSelectionActionId";
            this.PurchaseInvoiceLineListviewFilterSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.PurchaseInvoiceLineListviewFilterSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchaseInvoiceLine);
            this.PurchaseInvoiceLineListviewFilterSelectionAction.ToolTip = null;
            this.PurchaseInvoiceLineListviewFilterSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.PurchaseInvoiceLineListviewFilterSelectionAction_Execute);
            // 
            // PurchaseInvoiceLineActionController
            // 
            this.Actions.Add(this.PurchaseInvoiceLineTaxCalculationAction);
            this.Actions.Add(this.PurchaseInvoiceLineDiscountCalculationAction);
            this.Actions.Add(this.PurchaseInvoiceLineListviewFilterSelectionAction);
            this.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchaseInvoiceLine);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction PurchaseInvoiceLineTaxCalculationAction;
        private DevExpress.ExpressApp.Actions.SimpleAction PurchaseInvoiceLineDiscountCalculationAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction PurchaseInvoiceLineListviewFilterSelectionAction;
    }
}
