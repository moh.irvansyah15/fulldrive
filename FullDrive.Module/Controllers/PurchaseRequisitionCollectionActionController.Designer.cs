﻿namespace FullDrive.Module.Controllers
{
    partial class PurchaseRequisitionCollectionActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.PurchaseRequisitionCollectionShowPRMAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // PurchaseRequisitionCollectionShowPRMAction
            // 
            this.PurchaseRequisitionCollectionShowPRMAction.Caption = "Show PRM";
            this.PurchaseRequisitionCollectionShowPRMAction.ConfirmationMessage = null;
            this.PurchaseRequisitionCollectionShowPRMAction.Id = "PurchaseRequisitionCollectionShowPRMActionId";
            this.PurchaseRequisitionCollectionShowPRMAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchaseRequisitionCollection);
            this.PurchaseRequisitionCollectionShowPRMAction.ToolTip = null;
            this.PurchaseRequisitionCollectionShowPRMAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PurchaseRequisitionCollectionShowPRMAction_Execute);
            // 
            // PurchaseRequisitionCollectionActionController
            // 
            this.Actions.Add(this.PurchaseRequisitionCollectionShowPRMAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction PurchaseRequisitionCollectionShowPRMActionController;
        private DevExpress.ExpressApp.Actions.SimpleAction PurchaseRequisitionCollectionShowPRMAction;
    }
}
