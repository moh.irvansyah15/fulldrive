﻿#region Default

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

#endregion Default

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class ProductionActionController : ViewController
    {
        #region Default

        private ChoiceActionItem _selectionListviewFilter;
        public ProductionActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.

            #region FilterStatus
            ProductionFilterAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                ProductionFilterAction.Items.Add(_selectionListviewFilter);
            }
            #endregion FilterStatus

            #region FilterApproval
            ProductionListviewFilterApprovalSelectionAction.Items.Clear();
            foreach (object _currApprovalFilter in Enum.GetValues(typeof(CustomProcess.ApprovalFilter)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.ApprovalFilter));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApprovalFilter), _currApprovalFilter);
                ProductionListviewFilterApprovalSelectionAction.Items.Add(_selectionListviewFilter);
            }
            #endregion FilterApproval
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        #endregion Default

        private void ProductionProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        Production _locProductionOS = (Production)_objectSpace.GetObject(obj);

                        if (_locProductionOS != null)
                        {
                            if (_locProductionOS.Code != null)
                            {
                                _currObjectId = _locProductionOS.Code;

                                Production _locProductionXPO = _currSession.FindObject<Production>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Code", _currObjectId)));

                                if (_locProductionXPO != null)
                                {
                                    if (_locProductionXPO.Status == Status.Open)
                                    {
                                        _locProductionXPO.Status = Status.Progress;
                                        _locProductionXPO.StatusDate = now;
                                        _locProductionXPO.Save();
                                        _locProductionXPO.Session.CommitTransaction();

                                        XPCollection<ProductionLine> _locProductionLines = new XPCollection<ProductionLine>
                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("Production", _locProductionXPO)));

                                        XPCollection<ProductionMaterial> _locProductionMaterials = new XPCollection<ProductionMaterial>
                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                    new BinaryOperator("Production", _locProductionXPO)));

                                        #region ChangeStatus
                                        if (_locProductionLines != null && _locProductionLines.Count > 0)
                                        {
                                            foreach (ProductionLine _locProductionLine in _locProductionLines)
                                            {
                                                if (_locProductionLine.Status == Status.Open)
                                                {
                                                    _locProductionLine.Status = Status.Progress;
                                                    _locProductionLine.StatusDate = now;
                                                    _locProductionLine.Save();
                                                    _locProductionLine.Session.CommitTransaction();
                                                }
                                            }
                                        }
                                        else
                                        {
                                            ErrorMessageShow("Data ProductionLine Not Available");
                                        }

                                        if (_locProductionMaterials != null && _locProductionMaterials.Count > 0)
                                        {
                                            foreach (ProductionMaterial _locProductionMaterial in _locProductionMaterials)
                                            {
                                                if (_locProductionMaterial.Status == Status.Open)
                                                {
                                                    _locProductionMaterial.Status = Status.Progress;
                                                    _locProductionMaterial.StatusDate = now;
                                                    _locProductionMaterial.Save();
                                                    _locProductionMaterial.Session.CommitTransaction();
                                                }
                                            }
                                        }
                                        else
                                        {
                                            ErrorMessageShow("Data ProductionMaterial Not Available");
                                        }
                                        #endregion ChangeStatus

                                        SuccessMessageShow(_locProductionXPO.Code + " has been change successfully to Progress");
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data Production Not Available");
                                }
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = Production " + ex.ToString());
            }
        }

        private void ProductionPostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        Production _locProductionOS = (Production)_objectSpace.GetObject(obj);

                        if (_locProductionOS != null)
                        {
                            if (_locProductionOS.Code != null)
                            {
                                _currObjectId = _locProductionOS.Code;

                                Production _locProductionXPO = _currSession.FindObject<Production>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Code", _currObjectId)));

                                if (_locProductionXPO != null)
                                {
                                    if (_locProductionXPO.Status == Status.Progress || _locProductionXPO.Status == Status.Posted)
                                    {
                                        ApprovalLine _locApprovalLineXPO = _currSession.FindObject<ApprovalLine>
                                                                           (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("EndApproval", true),
                                                                            new BinaryOperator("Production", _locProductionXPO)));

                                        if (_locApprovalLineXPO != null)
                                        {
                                            SetPostingQty(_currSession, _locProductionXPO);
                                            SetRemainQty(_currSession, _locProductionXPO);
                                            SetConsumption(_currSession, _locProductionXPO);
                                            SetProcessCountForPostingCons(_currSession, _locProductionXPO);
                                            SetStatusProductionLine(_currSession, _locProductionXPO);
                                            SetStatusProductionMaterial(_currSession, _locProductionXPO);
                                            SetNormalQty(_currSession, _locProductionXPO);
                                            SetStatusProduction(_currSession, _locProductionXPO);
                                            SuccessMessageShow(_locProductionXPO.Code + " has been change successfully to Posted");
                                        }
                                        else
                                        {
                                            ErrorMessageShow("Data ApprovalLine Not Available");
                                        }
                                    }
                                    else
                                    {
                                        ErrorMessageShow("Data Production Not Available");
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data Production Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Production Not Available");
                            }
                        }
                        else
                        {
                            ErrorMessageShow("Data Production Not Available");
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = Production " + ex.ToString());
            }
        }

        private void ProductionFilterAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(Production)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = Production " + ex.ToString());
            }
        }

        private void ProductionListviewFilterApprovalSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(Production)))
                {
                    if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.Approval)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("ActiveApproved3", false, BinaryOperatorType.Equal),
                                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("ActiveApproved1", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", true, BinaryOperatorType.Equal)));
                    }
                    else if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.EndApproval)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new BinaryOperator("ActiveApproved3", true, BinaryOperatorType.Equal);
                    }
                    else if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.AllData)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("ActiveApproved1", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved3", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved1", false, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", false, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved3", false, BinaryOperatorType.Equal));
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = Production " + ex.ToString());
            }
        }

        //==== Code Only ====

        #region PostingforConsumption

        //Menentukan jumlah quantity yang di posting
        private void SetPostingQty(Session _currSession, Production _locProductionXPO)
        {
            try
            {
                if (_locProductionXPO != null)
                {
                    XPCollection<ProductionLine> _locProductionLines = new XPCollection<ProductionLine>(_currSession,
                                                                       new GroupOperator(GroupOperatorType.And,
                                                                       new BinaryOperator("Production", _locProductionXPO),
                                                                       new BinaryOperator("Select", true)));

                    if (_locProductionLines != null && _locProductionLines.Count > 0)
                    {
                        double _locPDQty = 0;
                        double _locPQty = 0;
                        double _locInvLineTotal = 0;
                        ItemUnitOfMeasure _locItemUOM = null;

                        foreach (ProductionLine _locProductionLine in _locProductionLines)
                        {
                            #region ProcessCount=0
                            if (_locProductionLine.ProcessCount == 0)
                            {
                                #region MaxQuantity
                                if (_locProductionLine.MxDQty > 0 || _locProductionLine.MxQty > 0)
                                {
                                    if (_locProductionLine.DQty > 0 && _locProductionLine.DQty <= _locProductionLine.MxDQty)
                                    {
                                        _locPDQty = _locProductionLine.DQty;
                                    }

                                    if (_locProductionLine.Qty > 0 && _locProductionLine.Qty <= _locProductionLine.MxQty)
                                    {
                                        _locPQty = _locProductionLine.Qty;
                                    }

                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                  (new GroupOperator(GroupOperatorType.And,
                                                   new BinaryOperator("Item", _locProductionLine.Item),
                                                   new BinaryOperator("UOM", _locProductionLine.MxUOM),
                                                   new BinaryOperator("DefaultUOM", _locProductionLine.MxDUOM),
                                                   new BinaryOperator("Active", true)));

                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty + _locPDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }

                                }
                                #endregion MaxQuantity

                                #region NonMaxQuantity
                                else
                                {
                                    if (_locProductionLine.DQty > 0)
                                    {
                                        _locPDQty = _locProductionLine.DQty;
                                    }

                                    if (_locProductionLine.Qty > 0)
                                    {
                                        _locPQty = _locProductionLine.Qty;
                                    }

                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                  (new GroupOperator(GroupOperatorType.And,
                                                   new BinaryOperator("Item", _locProductionLine.Item),
                                                   new BinaryOperator("UOM", _locProductionLine.UOM),
                                                   new BinaryOperator("DefaultUOM", _locProductionLine.DUOM),
                                                   new BinaryOperator("Active", true)));

                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty + _locPDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion ProcessCount=0

                            #region ProcessCount>0
                            if (_locProductionLine.ProcessCount > 0)
                            {
                                if (_locProductionLine.PDQty > 0)
                                {
                                    _locPDQty = _locProductionLine.PDQty + _locProductionLine.DQty;
                                }

                                if (_locProductionLine.PQty > 0)
                                {
                                    _locPQty = _locProductionLine.PQty + _locProductionLine.Qty;
                                }

                                if (_locProductionLine.MxDQty > 0 || _locProductionLine.MxQty > 0)
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                  (new GroupOperator(GroupOperatorType.And,
                                                   new BinaryOperator("Item", _locProductionLine.Item),
                                                   new BinaryOperator("UOM", _locProductionLine.MxUOM),
                                                   new BinaryOperator("DefaultUOM", _locProductionLine.MxDUOM),
                                                   new BinaryOperator("Active", true)));
                                }
                                else
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                  (new GroupOperator(GroupOperatorType.And,
                                                   new BinaryOperator("Item", _locProductionLine.Item),
                                                   new BinaryOperator("UOM", _locProductionLine.UOM),
                                                   new BinaryOperator("DefaultUOM", _locProductionLine.DUOM),
                                                   new BinaryOperator("Active", true)));
                                }

                                if (_locItemUOM != null)
                                {
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locPQty + _locPDQty;
                                }

                            }
                            #endregion ProcessCount>0

                            _locProductionLine.PDQty = _locPDQty;
                            _locProductionLine.PDUOM = _locProductionLine.DUOM;
                            _locProductionLine.PQty = _locPQty;
                            _locProductionLine.PUOM = _locProductionLine.UOM;
                            _locProductionLine.PTQty = _locInvLineTotal;
                            _locProductionLine.Save();
                            _locProductionLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = Production " + ex.ToString());
            }
        }

        //Menentukan sisa dari transaksi 
        private void SetRemainQty(Session _currSession, Production _locProductionXPO)
        {
            try
            {
                if (_locProductionXPO != null)
                {
                    XPCollection<ProductionLine> _locProductionLines = new XPCollection<ProductionLine>(_currSession,
                                                                       new GroupOperator(GroupOperatorType.And,
                                                                       new BinaryOperator("Production", _locProductionXPO),
                                                                       new BinaryOperator("Select", true)));

                    if (_locProductionLines != null && _locProductionLines.Count > 0)
                    {
                        double _locRmDQty = 0;
                        double _locRmQty = 0;
                        double _locInvLineTotal = 0;
                        ItemUnitOfMeasure _locItemUOM = null;

                        foreach (ProductionLine _locProductionLine in _locProductionLines)
                        {
                            #region ProcessCount=0
                            if (_locProductionLine.ProcessCount == 0)
                            {
                                #region MaxQuantity
                                if (_locProductionLine.MxDQty > 0 || _locProductionLine.MxQty > 0)
                                {
                                    if (_locProductionLine.DQty > 0 && _locProductionLine.DQty <= _locProductionLine.MxDQty)
                                    {
                                        _locRmDQty = _locProductionLine.MxDQty - _locProductionLine.DQty;
                                    }

                                    if (_locProductionLine.Qty > 0 && _locProductionLine.Qty <= _locProductionLine.MxQty)
                                    {
                                        _locRmQty = _locProductionLine.MxQty - _locProductionLine.Qty;
                                    }

                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                  (new GroupOperator(GroupOperatorType.And,
                                                   new BinaryOperator("Item", _locProductionLine.Item),
                                                   new BinaryOperator("UOM", _locProductionLine.MxUOM),
                                                   new BinaryOperator("DefaultUOM", _locProductionLine.MxDUOM),
                                                   new BinaryOperator("Active", true)));

                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty + _locRmDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }

                                }
                                #endregion MaxQuantity

                                #region NonMaxQuantity
                                else
                                {
                                    if (_locProductionLine.DQty > 0)
                                    {
                                        _locRmDQty = _locProductionLine.DQty;
                                    }

                                    if (_locProductionLine.Qty > 0)
                                    {
                                        _locRmQty = _locProductionLine.Qty;
                                    }

                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                  (new GroupOperator(GroupOperatorType.And,
                                                   new BinaryOperator("Item", _locProductionLine.Item),
                                                   new BinaryOperator("UOM", _locProductionLine.UOM),
                                                   new BinaryOperator("DefaultUOM", _locProductionLine.DUOM),
                                                   new BinaryOperator("Active", true)));

                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty + _locRmDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion ProcessCount=0

                            #region ProcessCount>0
                            if (_locProductionLine.ProcessCount > 0)
                            {
                                if (_locProductionLine.RmDQty > 0)
                                {
                                    _locRmDQty = _locProductionLine.RmDQty - _locProductionLine.DQty;
                                }

                                if (_locProductionLine.RmQty > 0)
                                {
                                    _locRmQty = _locProductionLine.RmQty - _locProductionLine.Qty;
                                }

                                if (_locProductionLine.MxDQty > 0 || _locProductionLine.MxQty > 0)
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                  (new GroupOperator(GroupOperatorType.And,
                                                   new BinaryOperator("Item", _locProductionLine.Item),
                                                   new BinaryOperator("UOM", _locProductionLine.MxUOM),
                                                   new BinaryOperator("DefaultUOM", _locProductionLine.MxDUOM),
                                                   new BinaryOperator("Active", true)));
                                }
                                else
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                  (new GroupOperator(GroupOperatorType.And,
                                                   new BinaryOperator("Item", _locProductionLine.Item),
                                                   new BinaryOperator("UOM", _locProductionLine.UOM),
                                                   new BinaryOperator("DefaultUOM", _locProductionLine.DUOM),
                                                   new BinaryOperator("Active", true)));
                                }

                                if (_locItemUOM != null)
                                {
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locRmQty + _locRmDQty;
                                }

                            }
                            #endregion ProcessCount>0

                            _locProductionLine.RmDQty = _locRmDQty;
                            _locProductionLine.RmQty = _locRmQty;
                            _locProductionLine.RmTQty = _locInvLineTotal;
                            _locProductionLine.Save();
                            _locProductionLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = Production " + ex.ToString());
            }
        }

        //Membuat Consumption
        private void SetConsumption(Session _currSession, Production _locProductionXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _currSignCode = null;

                if (_locProductionXPO != null)
                {
                    if (_locProductionXPO.Status == Status.Progress)
                    {
                        _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.Consumption);

                        if (_currSignCode != null)
                        {
                            Consumption _saveDataCons = new Consumption(_currSession)
                            {
                                SignCode = _currSignCode,
                                Name = _locProductionXPO.Name,
                                StartDate = _locProductionXPO.StartDate,
                                EndDate = _locProductionXPO.EndDate,
                                Description = _locProductionXPO.Description,
                                Company = _locProductionXPO.Company,
                                WorkOrder = _locProductionXPO.WorkOrder,
                                MachineMapVersion = _locProductionXPO.MachineMapVersion,
                                MachineCost = _locProductionXPO.MachineCost,
                                Production = _locProductionXPO,
                            };
                            _saveDataCons.Save();
                            _saveDataCons.Session.CommitTransaction();
                        }
                    }

                    XPCollection<ProductionLine> _numLineProdLines = new XPCollection<ProductionLine>(_currSession,
                                                                     new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Select", true),
                                                                     new BinaryOperator("Production", _locProductionXPO)));

                    if (_numLineProdLines != null && _numLineProdLines.Count > 0)
                    {
                        Consumption _locCons2 = _currSession.FindObject<Consumption>
                                                (new GroupOperator(GroupOperatorType.And,
                                                 new BinaryOperator("Production", _locProductionXPO)));

                        if (_locCons2 != null)
                        {
                            if (_numLineProdLines != null && _numLineProdLines.Count > 0)
                            {
                                foreach (ProductionLine _numLineProdLine in _numLineProdLines)
                                {
                                    if (_numLineProdLine.Status == Status.Progress || _numLineProdLine.Status == Status.Posted)
                                    {
                                        ConsumptionLine _saveDataConsLine = new ConsumptionLine(_currSession)
                                        {
                                            Name = _numLineProdLine.Name,
                                            Description = _numLineProdLine.Description,
                                            Company = _numLineProdLine.Company,
                                            Item = _numLineProdLine.Item,
                                            MxDQty = _numLineProdLine.DQty,
                                            MxDUOM = _numLineProdLine.DUOM,
                                            MxQty = _numLineProdLine.Qty,
                                            MxUOM = _numLineProdLine.UOM,
                                            MxTQty = _numLineProdLine.TQty,
                                            DQty = _numLineProdLine.DQty,
                                            DUOM = _numLineProdLine.DUOM,
                                            Qty = _numLineProdLine.Qty,
                                            UOM = _numLineProdLine.UOM,
                                            TQty = _numLineProdLine.TQty,
                                            ADUOM = _numLineProdLine.DUOM,
                                            AUOM = _numLineProdLine.UOM,
                                            BillOfMaterial = _numLineProdLine.BillOfMaterial,
                                            BillOfMaterialVersion = _numLineProdLine.BillOfMaterialVersion,
                                            Consumption = _locCons2,
                                        };
                                        _saveDataConsLine.Save();
                                        _saveDataConsLine.Session.CommitTransaction();
                                    }
                                }
                            }
                        }
                        else
                        {
                            ErrorMessageShow("Data Consumption Not Available");
                        }
                    }
                    else
                    {
                        ErrorMessageShow("Data ProductionLine Not Available");
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = Production ", ex.ToString());
            }
        }

        //Menentukan banyak proses posting Production
        private void SetProcessCountForPostingCons(Session _currSession, Production _locProductionXPO)
        {
            try
            {
                if (_locProductionXPO != null)
                {
                    XPCollection<ProductionLine> _locProductionLines = new XPCollection<ProductionLine>(_currSession,
                                                                       new GroupOperator(GroupOperatorType.And,
                                                                       new BinaryOperator("Production", _locProductionXPO),
                                                                       new BinaryOperator("Select", true)));

                    if (_locProductionLines != null && _locProductionLines.Count > 0)
                    {
                        foreach (ProductionLine _locProductionLine in _locProductionLines)
                        {
                            if (_locProductionLine.Status == Status.Progress || _locProductionLine.Status == Status.Posted)
                            {
                                _locProductionLine.ProcessCount = _locProductionLine.ProcessCount + 1;
                                _locProductionLine.Save();
                                _locProductionLine.Session.CommitTransaction();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = Production " + ex.ToString());
            }
        }

        //Menentukan status pada productionline
        private void SetStatusProductionLine(Session _currSession, Production _locProductionXPO)
        {
            try
            {
                DateTime now = DateTime.Now;

                if (_locProductionXPO != null)
                {
                    XPCollection<ProductionLine> _locProductionLines = new XPCollection<ProductionLine>(_currSession,
                                                                       new GroupOperator(GroupOperatorType.And,
                                                                       new BinaryOperator("Production", _locProductionXPO),
                                                                       new BinaryOperator("Select", true)));

                    if (_locProductionLines != null && _locProductionLines.Count > 0)
                    {

                        foreach (ProductionLine _locProductionLine in _locProductionLines)
                        {
                            if (_locProductionLine.Status == Status.Progress || _locProductionLine.Status == Status.Posted)
                            {
                                if (_locProductionLine.DQty > 0 || _locProductionLine.Qty > 0)
                                {
                                    if (_locProductionLine.RmDQty == 0 && _locProductionLine.RmQty == 0 && _locProductionLine.RmTQty == 0)
                                    {
                                        _locProductionLine.Status = Status.Close;
                                        _locProductionLine.ActivationPosting = true;
                                        _locProductionLine.StatusDate = now;
                                    }
                                    else
                                    {
                                        _locProductionLine.Status = Status.Posted;
                                        _locProductionLine.ActivationPosting = false;
                                        _locProductionLine.StatusDate = now;
                                    }
                                    _locProductionLine.Save();
                                    _locProductionLine.Session.CommitTransaction();
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = WorkRequisition " + ex.ToString());
            }
        }

        //Menentukan status pada productionmaterial
        private void SetStatusProductionMaterial(Session _currSession, Production _locProductionXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locCount = 0;

                if (_locProductionXPO != null)
                {
                    XPCollection<ProductionMaterial> _locProductionMaterials = new XPCollection<ProductionMaterial>(_currSession,
                                                                               new GroupOperator(GroupOperatorType.And,
                                                                               new BinaryOperator("Production", _locProductionXPO)));

                    if (_locProductionMaterials != null && _locProductionMaterials.Count() > 0)
                    {
                        foreach (ProductionMaterial _locProductionMaterial in _locProductionMaterials)
                        {
                            if (_locProductionMaterial.Status == Status.Close)
                            {
                                _locCount = _locCount + 1;
                            }
                        }

                        if (_locCount == _locProductionMaterials.Count())
                        {
                            _locProductionXPO.ActivationPosting = true;
                            _locProductionXPO.Status = Status.Close;
                            _locProductionXPO.StatusDate = now;
                            _locProductionXPO.Save();
                            _locProductionXPO.Session.CommitTransaction();
                        }
                        else
                        {
                            _locProductionXPO.Status = Status.Posted;
                            _locProductionXPO.StatusDate = now;
                            _locProductionXPO.Save();
                            _locProductionXPO.Session.CommitTransaction();
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = Production " + ex.ToString());
            }
        }

        //Menormalkan Quantity
        private void SetNormalQty(Session _currSession, Production _locProductionXPO)
        {
            try
            {
                if (_locProductionXPO != null)
                {
                    XPCollection<ProductionLine> _locProductionLines = new XPCollection<ProductionLine>(_currSession,
                                                                       new GroupOperator(GroupOperatorType.And,
                                                                       new BinaryOperator("Select", true),
                                                                       new BinaryOperator("Production", _locProductionXPO)));

                    if (_locProductionLines != null && _locProductionLines.Count > 0)
                    {
                        foreach (ProductionLine _locProductionLine in _locProductionLines)
                        {
                            if (_locProductionLine.Status == Status.Progress || _locProductionLine.Status == Status.Posted || _locProductionLine.Status == Status.Close)
                            {
                                if (_locProductionLine.DQty > 0 || _locProductionLine.Qty > 0)
                                {
                                    _locProductionLine.Select = false;
                                    _locProductionLine.DQty = 0;
                                    _locProductionLine.Qty = 0;
                                    _locProductionLine.Save();
                                    _locProductionLine.Session.CommitTransaction();
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = Production " + ex.ToString());
            }
        }

        //Menentukan status pada production
        private void SetStatusProduction(Session _currSession, Production _locProductionXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locCountStatus = 0;
                bool _locActivationPosting = false;

                if (_locProductionXPO != null)
                {
                    XPCollection<ProductionLine> _locProductionLines = new XPCollection<ProductionLine>(_currSession,
                                                                       new GroupOperator(GroupOperatorType.And,
                                                                       new BinaryOperator("Production", _locProductionXPO)));

                    if (_locProductionLines != null && _locProductionLines.Count() > 0)
                    {
                        foreach (ProductionLine _locProductionLine in _locProductionLines)
                        {
                            if (_locProductionLine.Status != Status.Close)
                            {
                                _locCountStatus = _locCountStatus + 1;
                            }
                        }
                    }

                    if (_locCountStatus > 0)
                    {
                        if (_locProductionLines.Count() == _locCountStatus)
                        {
                            _locActivationPosting = true;
                        }
                    }

                    _locProductionXPO.Status = Status.Posted;
                    _locActivationPosting = true;
                    _locProductionXPO.StatusDate = now;
                    _locProductionXPO.ActivationPosting = _locActivationPosting;
                    _locProductionXPO.Save();
                    _locProductionXPO.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = Production " + ex.ToString());
            }
        }

        #endregion PostingforConsumption

        #region Global Method

        private void SuccessMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 4000;
            options.Message = string.Format("{0}", _locMessage);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 4000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method
        
    }
}
