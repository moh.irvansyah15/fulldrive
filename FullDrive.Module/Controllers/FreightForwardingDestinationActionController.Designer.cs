﻿namespace FullDrive.Module.Controllers
{
    partial class FreightForwardingDestinationActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.FreightForwardingDestinationProgressAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.FreightForwardingDestinationApprovalAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.FreightForwardinDestinationGetSBMAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.FreightForwardinDestinationListviewFilterSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.FreightForwardingDestinationListviewFilterApprovalSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.FreightForwardingDestinationPostingPackageAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // FreightForwardingDestinationProgressAction
            // 
            this.FreightForwardingDestinationProgressAction.Caption = "Progress";
            this.FreightForwardingDestinationProgressAction.ConfirmationMessage = null;
            this.FreightForwardingDestinationProgressAction.Id = "FreightForwardingDestinationProgressActionId";
            this.FreightForwardingDestinationProgressAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.FreightForwardingDestination);
            this.FreightForwardingDestinationProgressAction.ToolTip = null;
            this.FreightForwardingDestinationProgressAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.FreightForwardingDestinationProgressAction_Execute);
            // 
            // FreightForwardingDestinationApprovalAction
            // 
            this.FreightForwardingDestinationApprovalAction.Caption = "Approval";
            this.FreightForwardingDestinationApprovalAction.ConfirmationMessage = null;
            this.FreightForwardingDestinationApprovalAction.Id = "FreightForwardingDestinationApprovalActionId";
            this.FreightForwardingDestinationApprovalAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.FreightForwardingDestinationApprovalAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.FreightForwardingDestination);
            this.FreightForwardingDestinationApprovalAction.ToolTip = null;
            this.FreightForwardingDestinationApprovalAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.FreightForwardingDestinationApprovalAction_Execute);
            // 
            // FreightForwardinDestinationGetSBMAction
            // 
            this.FreightForwardinDestinationGetSBMAction.Caption = "Get SBM";
            this.FreightForwardinDestinationGetSBMAction.ConfirmationMessage = null;
            this.FreightForwardinDestinationGetSBMAction.Id = "FreightForwardinDestinationGetSBMActionId";
            this.FreightForwardinDestinationGetSBMAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.FreightForwardingDestination);
            this.FreightForwardinDestinationGetSBMAction.ToolTip = null;
            this.FreightForwardinDestinationGetSBMAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.FreightForwardinDestinationGetSBMAction_Execute);
            // 
            // FreightForwardinDestinationListviewFilterSelectionAction
            // 
            this.FreightForwardinDestinationListviewFilterSelectionAction.Caption = "Filter";
            this.FreightForwardinDestinationListviewFilterSelectionAction.ConfirmationMessage = null;
            this.FreightForwardinDestinationListviewFilterSelectionAction.Id = "FreightForwardinDestinationListviewFilterSelectionActionId";
            this.FreightForwardinDestinationListviewFilterSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.FreightForwardinDestinationListviewFilterSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.FreightForwardingDestination);
            this.FreightForwardinDestinationListviewFilterSelectionAction.ToolTip = null;
            this.FreightForwardinDestinationListviewFilterSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.FreightForwardinDestinationListviewFilterSelectionAction_Execute);
            // 
            // FreightForwardingDestinationListviewFilterApprovalSelectionAction
            // 
            this.FreightForwardingDestinationListviewFilterApprovalSelectionAction.Caption = "Filter Approval";
            this.FreightForwardingDestinationListviewFilterApprovalSelectionAction.ConfirmationMessage = null;
            this.FreightForwardingDestinationListviewFilterApprovalSelectionAction.Id = "FreightForwardingDestinationListviewFilterApprovalSelectionActionId";
            this.FreightForwardingDestinationListviewFilterApprovalSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.FreightForwardingDestinationListviewFilterApprovalSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.FreightForwardingDestination);
            this.FreightForwardingDestinationListviewFilterApprovalSelectionAction.ToolTip = null;
            this.FreightForwardingDestinationListviewFilterApprovalSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.FreightForwardingDestinationListviewFilterApprovalSelectionAction_Execute);
            // 
            // FreightForwardingDestinationPostingPackageAction
            // 
            this.FreightForwardingDestinationPostingPackageAction.Caption = "Posting Package";
            this.FreightForwardingDestinationPostingPackageAction.ConfirmationMessage = null;
            this.FreightForwardingDestinationPostingPackageAction.Id = "FreightForwardingDestinationPostingPackageActionId";
            this.FreightForwardingDestinationPostingPackageAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.FreightForwardingDestination);
            this.FreightForwardingDestinationPostingPackageAction.ToolTip = null;
            this.FreightForwardingDestinationPostingPackageAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.FreightForwardingDestinationPostingPackageAction_Execute);
            // 
            // FreightForwardingDestinationActionController
            // 
            this.Actions.Add(this.FreightForwardingDestinationProgressAction);
            this.Actions.Add(this.FreightForwardingDestinationApprovalAction);
            this.Actions.Add(this.FreightForwardinDestinationGetSBMAction);
            this.Actions.Add(this.FreightForwardinDestinationListviewFilterSelectionAction);
            this.Actions.Add(this.FreightForwardingDestinationListviewFilterApprovalSelectionAction);
            this.Actions.Add(this.FreightForwardingDestinationPostingPackageAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction FreightForwardingDestinationProgressAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction FreightForwardingDestinationApprovalAction;
        private DevExpress.ExpressApp.Actions.SimpleAction FreightForwardinDestinationGetSBMAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction FreightForwardinDestinationListviewFilterSelectionAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction FreightForwardingDestinationListviewFilterApprovalSelectionAction;
        private DevExpress.ExpressApp.Actions.SimpleAction FreightForwardingDestinationPostingPackageAction;
    }
}
