﻿namespace FullDrive.Module.Controllers
{
    partial class DebitSalesCollectionActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.DebitSalesCollectionShowSRMAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // DebitSalesCollectionShowSRMAction
            // 
            this.DebitSalesCollectionShowSRMAction.Caption = "Show SRM";
            this.DebitSalesCollectionShowSRMAction.ConfirmationMessage = null;
            this.DebitSalesCollectionShowSRMAction.Id = "DebitSalesCollectionShowSRMActionId";
            this.DebitSalesCollectionShowSRMAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.DebitSalesCollection);
            this.DebitSalesCollectionShowSRMAction.ToolTip = null;
            this.DebitSalesCollectionShowSRMAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.DebitSalesCollectionShowSRMAction_Execute);
            // 
            // DebitSalesCollectionActionController
            // 
            this.Actions.Add(this.DebitSalesCollectionShowSRMAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction DebitSalesCollectionShowSRMAction;
    }
}
