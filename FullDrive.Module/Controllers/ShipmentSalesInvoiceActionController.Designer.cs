﻿namespace FullDrive.Module.Controllers
{
    partial class ShipmentSalesInvoiceActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ShipmentSalesInvoiceGetSSMAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.ShipmentSalesInvoiceProgressAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.ShipmentSalesInvoiceGetBillAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.ShipmentSalesInvoiceApprovalAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.ShipmentSalesInvoiceListviewFilterSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.ShipmentSalesInvoiceListviewFilterApprovalSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.ShipmentSalesInvoicePostingAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // ShipmentSalesInvoiceGetSSMAction
            // 
            this.ShipmentSalesInvoiceGetSSMAction.Caption = "Get SSM";
            this.ShipmentSalesInvoiceGetSSMAction.ConfirmationMessage = null;
            this.ShipmentSalesInvoiceGetSSMAction.Id = "ShipmentSalesInvoiceGetSSMActionId";
            this.ShipmentSalesInvoiceGetSSMAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.ShipmentSalesInvoice);
            this.ShipmentSalesInvoiceGetSSMAction.ToolTip = null;
            this.ShipmentSalesInvoiceGetSSMAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.ShipmentSalesInvoiceGetSSMAction_Execute);
            // 
            // ShipmentSalesInvoiceProgressAction
            // 
            this.ShipmentSalesInvoiceProgressAction.Caption = "Progress";
            this.ShipmentSalesInvoiceProgressAction.ConfirmationMessage = null;
            this.ShipmentSalesInvoiceProgressAction.Id = "ShipmentSalesInvoiceProgressActionId";
            this.ShipmentSalesInvoiceProgressAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.ShipmentSalesInvoice);
            this.ShipmentSalesInvoiceProgressAction.ToolTip = null;
            this.ShipmentSalesInvoiceProgressAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.ShipmentSalesInvoiceProgressAction_Execute);
            // 
            // ShipmentSalesInvoiceGetBillAction
            // 
            this.ShipmentSalesInvoiceGetBillAction.Caption = "Get Bill";
            this.ShipmentSalesInvoiceGetBillAction.ConfirmationMessage = null;
            this.ShipmentSalesInvoiceGetBillAction.Id = "ShipmentSalesInvoiceGetBillActionId";
            this.ShipmentSalesInvoiceGetBillAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.ShipmentSalesInvoice);
            this.ShipmentSalesInvoiceGetBillAction.ToolTip = null;
            this.ShipmentSalesInvoiceGetBillAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.ShipmentSalesInvoiceGetBillAction_Execute);
            // 
            // ShipmentSalesInvoiceApprovalAction
            // 
            this.ShipmentSalesInvoiceApprovalAction.Caption = "Approval";
            this.ShipmentSalesInvoiceApprovalAction.ConfirmationMessage = null;
            this.ShipmentSalesInvoiceApprovalAction.Id = "ShipmentSalesInvoiceApprovalActionId";
            this.ShipmentSalesInvoiceApprovalAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.ShipmentSalesInvoiceApprovalAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.ShipmentSalesInvoice);
            this.ShipmentSalesInvoiceApprovalAction.ToolTip = null;
            this.ShipmentSalesInvoiceApprovalAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.ShipmentSalesInvoiceApprovalAction_Execute);
            // 
            // ShipmentSalesInvoiceListviewFilterSelectionAction
            // 
            this.ShipmentSalesInvoiceListviewFilterSelectionAction.Caption = "Filter";
            this.ShipmentSalesInvoiceListviewFilterSelectionAction.ConfirmationMessage = null;
            this.ShipmentSalesInvoiceListviewFilterSelectionAction.Id = "ShipmentSalesInvoiceListviewFilterSelectionActionId";
            this.ShipmentSalesInvoiceListviewFilterSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.ShipmentSalesInvoiceListviewFilterSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.ShipmentSalesInvoice);
            this.ShipmentSalesInvoiceListviewFilterSelectionAction.ToolTip = null;
            this.ShipmentSalesInvoiceListviewFilterSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.ShipmentSalesInvoiceListviewFilterSelectionAction_Execute);
            // 
            // ShipmentSalesInvoiceListviewFilterApprovalSelectionAction
            // 
            this.ShipmentSalesInvoiceListviewFilterApprovalSelectionAction.Caption = "Filter Approval";
            this.ShipmentSalesInvoiceListviewFilterApprovalSelectionAction.ConfirmationMessage = null;
            this.ShipmentSalesInvoiceListviewFilterApprovalSelectionAction.Id = "ShipmentSalesInvoiceListviewFilterApprovalSelectionActionId";
            this.ShipmentSalesInvoiceListviewFilterApprovalSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.ShipmentSalesInvoiceListviewFilterApprovalSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.ShipmentSalesInvoice);
            this.ShipmentSalesInvoiceListviewFilterApprovalSelectionAction.ToolTip = null;
            this.ShipmentSalesInvoiceListviewFilterApprovalSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.ShipmentSalesInvoiceListviewFilterApprovalSelectionAction_Execute);
            // 
            // ShipmentSalesInvoicePostingAction
            // 
            this.ShipmentSalesInvoicePostingAction.Caption = "Posting";
            this.ShipmentSalesInvoicePostingAction.ConfirmationMessage = null;
            this.ShipmentSalesInvoicePostingAction.Id = "ShipmentSalesInvoicePostingActionId";
            this.ShipmentSalesInvoicePostingAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.ShipmentSalesInvoice);
            this.ShipmentSalesInvoicePostingAction.ToolTip = null;
            this.ShipmentSalesInvoicePostingAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.ShipmentSalesInvoicePostingAction_Execute);
            // 
            // ShipmentSalesInvoiceActionController
            // 
            this.Actions.Add(this.ShipmentSalesInvoiceGetSSMAction);
            this.Actions.Add(this.ShipmentSalesInvoiceProgressAction);
            this.Actions.Add(this.ShipmentSalesInvoiceGetBillAction);
            this.Actions.Add(this.ShipmentSalesInvoiceApprovalAction);
            this.Actions.Add(this.ShipmentSalesInvoiceListviewFilterSelectionAction);
            this.Actions.Add(this.ShipmentSalesInvoiceListviewFilterApprovalSelectionAction);
            this.Actions.Add(this.ShipmentSalesInvoicePostingAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction ShipmentSalesInvoiceGetSSMAction;
        private DevExpress.ExpressApp.Actions.SimpleAction ShipmentSalesInvoiceProgressAction;
        private DevExpress.ExpressApp.Actions.SimpleAction ShipmentSalesInvoiceGetBillAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction ShipmentSalesInvoiceApprovalAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction ShipmentSalesInvoiceListviewFilterSelectionAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction ShipmentSalesInvoiceListviewFilterApprovalSelectionAction;
        private DevExpress.ExpressApp.Actions.SimpleAction ShipmentSalesInvoicePostingAction;
    }
}
