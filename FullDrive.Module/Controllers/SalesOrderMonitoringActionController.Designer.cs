﻿namespace FullDrive.Module.Controllers
{
    partial class SalesOrderMonitoringActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.SalesOrderMonitoringSelectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.SalesOrderMonitoringUnselectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // SalesOrderMonitoringSelectAction
            // 
            this.SalesOrderMonitoringSelectAction.Caption = "Select";
            this.SalesOrderMonitoringSelectAction.ConfirmationMessage = null;
            this.SalesOrderMonitoringSelectAction.Id = "SalesOrderMonitoringSelectActionId";
            this.SalesOrderMonitoringSelectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesOrderMonitoring);
            this.SalesOrderMonitoringSelectAction.ToolTip = null;
            this.SalesOrderMonitoringSelectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SalesOrderMonitoringSelectAction_Execute);
            // 
            // SalesOrderMonitoringUnselectAction
            // 
            this.SalesOrderMonitoringUnselectAction.Caption = "Unselect";
            this.SalesOrderMonitoringUnselectAction.ConfirmationMessage = null;
            this.SalesOrderMonitoringUnselectAction.Id = "SalesOrderMonitoringUnselectActionId";
            this.SalesOrderMonitoringUnselectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesOrderMonitoring);
            this.SalesOrderMonitoringUnselectAction.ToolTip = null;
            this.SalesOrderMonitoringUnselectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SalesOrderMonitoringUnselectAction_Execute);
            // 
            // SalesOrderMonitoringActionController
            // 
            this.Actions.Add(this.SalesOrderMonitoringSelectAction);
            this.Actions.Add(this.SalesOrderMonitoringUnselectAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction SalesOrderMonitoringSelectAction;
        private DevExpress.ExpressApp.Actions.SimpleAction SalesOrderMonitoringUnselectAction;
    }
}
