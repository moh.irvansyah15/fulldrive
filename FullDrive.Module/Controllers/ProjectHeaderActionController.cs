﻿#region Default

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using FullDrive.Module.BusinessObjects;
using DevExpress.ExpressApp.Xpo;

#endregion Default

using FullDrive.Module.CustomProcess;
using System.Collections;
using DevExpress.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class ProjectHeaderActionController : ViewController
    {
        #region Default

        private ChoiceActionItem _selectionListviewFilter;
        public ProjectHeaderActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            ProjectHeaderListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {

                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                ProjectHeaderListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        #endregion Default

        private void ProjectHeaderProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;


                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        ProjectHeader _locProjectHeaderOS = (ProjectHeader)_objectSpace.GetObject(obj);

                        if (_locProjectHeaderOS != null)
                        {
                            if (_locProjectHeaderOS.Code != null)
                            {
                                _currObjectId = _locProjectHeaderOS.Code;

                                ProjectHeader _locProjectHeaderXPO = _currSession.FindObject<ProjectHeader>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locProjectHeaderXPO != null)
                                {
                                    if (_locProjectHeaderXPO.Status == Status.Open || _locProjectHeaderXPO.Status == Status.Progress || _locProjectHeaderXPO.Status == Status.Lock)
                                    {
                                        if (_locProjectHeaderXPO.Status == Status.Open)
                                        {
                                            _locProjectHeaderXPO.Status = Status.Progress;
                                            _locProjectHeaderXPO.StatusDate = now;
                                            _locProjectHeaderXPO.Save();
                                            _locProjectHeaderXPO.Session.CommitTransaction();
                                        }

                                        #region ProjectLine
                                        XPCollection<ProjectLine> _locProjectLines = new XPCollection<ProjectLine>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("ProjectHeader", _locProjectHeaderXPO)));

                                        if (_locProjectLines != null && _locProjectLines.Count > 0)
                                        {
                                            foreach (ProjectLine _locProjectLine in _locProjectLines)
                                            {
                                                if (_locProjectLine.Status == Status.Open || _locProjectLine.Status == Status.Progress || _locProjectLine.Status == Status.Lock)
                                                {
                                                    if (_locProjectLine.Status == Status.Open)
                                                    {
                                                        _locProjectLine.Status = Status.Progress;
                                                        _locProjectLine.StatusDate = now;
                                                        _locProjectLine.Save();
                                                        _locProjectLine.Session.CommitTransaction();
                                                    }


                                                    XPCollection<ProjectLineItem> _locProjectLineItems = new XPCollection<ProjectLineItem>(_currSession,
                                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                                     new BinaryOperator("ProjectLine", _locProjectLine),
                                                                                                     new BinaryOperator("ProjectHeader", _locProjectHeaderXPO)));

                                                    XPCollection<ProjectLineItem2> _locProjectLineItem2s = new XPCollection<ProjectLineItem2>(_currSession,
                                                                                                         new GroupOperator(GroupOperatorType.And,
                                                                                                         new BinaryOperator("ProjectLine", _locProjectLine),
                                                                                                         new BinaryOperator("ProjectHeader", _locProjectHeaderXPO)));

                                                    XPCollection<ProjectLineService> _locProjectLineServices = new XPCollection<ProjectLineService>(_currSession,
                                                                                                         new GroupOperator(GroupOperatorType.And,
                                                                                                         new BinaryOperator("ProjectLine", _locProjectLine),
                                                                                                         new BinaryOperator("ProjectHeader", _locProjectHeaderXPO)));

                                                    if (_locProjectLineItems != null && _locProjectLineItems.Count > 0)
                                                    {
                                                        foreach (ProjectLineItem _locProjectLineItem in _locProjectLineItems)
                                                        {
                                                            if (_locProjectLineItem.Status == Status.Open)
                                                            {
                                                                _locProjectLineItem.Status = Status.Progress;
                                                                _locProjectLineItem.StatusDate = now;
                                                                _locProjectLineItem.Save();
                                                                _locProjectLineItem.Session.CommitTransaction();
                                                            }
                                                        }
                                                    }

                                                    if (_locProjectLineItem2s != null && _locProjectLineItem2s.Count > 0)
                                                    {
                                                        foreach (ProjectLineItem2 _locProjectLineItem2 in _locProjectLineItem2s)
                                                        {
                                                            if (_locProjectLineItem2.Status == Status.Open)
                                                            {
                                                                _locProjectLineItem2.Status = Status.Progress;
                                                                _locProjectLineItem2.StatusDate = now;
                                                                _locProjectLineItem2.Save();
                                                                _locProjectLineItem2.Session.CommitTransaction();
                                                            }
                                                        }
                                                    }

                                                    if (_locProjectLineServices != null && _locProjectLineServices.Count > 0)
                                                    {
                                                        foreach (ProjectLineService _locProjectLineService in _locProjectLineServices)
                                                        {
                                                            if (_locProjectLineService.Status == Status.Open)
                                                            {
                                                                _locProjectLineService.Status = Status.Progress;
                                                                _locProjectLineService.StatusDate = now;
                                                                _locProjectLineService.Save();
                                                                _locProjectLineService.Session.CommitTransaction();
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        #endregion ProjectLine

                                        #region ProjectLine2
                                        XPCollection<ProjectLine2> _locProjectLine2s = new XPCollection<ProjectLine2>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("ProjectHeader", _locProjectHeaderXPO)));

                                        if (_locProjectLines != null && _locProjectLines.Count > 0)
                                        {
                                            foreach (ProjectLine2 _locProjectLine2 in _locProjectLine2s)
                                            {
                                                if (_locProjectLine2.Status == Status.Open || _locProjectLine2.Status == Status.Progress || _locProjectLine2.Status == Status.Lock)
                                                {
                                                    if (_locProjectLine2.Status == Status.Open)
                                                    {
                                                        _locProjectLine2.Status = Status.Progress;
                                                        _locProjectLine2.StatusDate = now;
                                                        _locProjectLine2.Save();
                                                        _locProjectLine2.Session.CommitTransaction();
                                                    }


                                                    XPCollection<ProjectLine2Item> _locProjectLine2Items = new XPCollection<ProjectLine2Item>(_currSession,
                                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                                     new BinaryOperator("ProjectLine2", _locProjectLine2),
                                                                                                     new BinaryOperator("ProjectHeader", _locProjectHeaderXPO)));

                                                    XPCollection<ProjectLine2Item2> _locProjectLine2Item2s = new XPCollection<ProjectLine2Item2>(_currSession,
                                                                                                         new GroupOperator(GroupOperatorType.And,
                                                                                                         new BinaryOperator("ProjectLine2", _locProjectLine2),
                                                                                                         new BinaryOperator("ProjectHeader", _locProjectHeaderXPO)));

                                                    XPCollection<ProjectLine2Service> _locProjectLine2Services = new XPCollection<ProjectLine2Service>(_currSession,
                                                                                                         new GroupOperator(GroupOperatorType.And,
                                                                                                         new BinaryOperator("ProjectLine2", _locProjectLine2),
                                                                                                         new BinaryOperator("ProjectHeader", _locProjectHeaderXPO)));

                                                    if (_locProjectLine2Items != null && _locProjectLine2Items.Count > 0)
                                                    {
                                                        foreach (ProjectLine2Item _locProjectLine2Item in _locProjectLine2Items)
                                                        {
                                                            if (_locProjectLine2Item.Status == Status.Open)
                                                            {
                                                                _locProjectLine2Item.Status = Status.Progress;
                                                                _locProjectLine2Item.StatusDate = now;
                                                                _locProjectLine2Item.Save();
                                                                _locProjectLine2Item.Session.CommitTransaction();
                                                            }
                                                        }
                                                    }

                                                    if (_locProjectLine2Item2s != null && _locProjectLine2Item2s.Count > 0)
                                                    {
                                                        foreach (ProjectLine2Item2 _locProjectLine2Item2 in _locProjectLine2Item2s)
                                                        {
                                                            if (_locProjectLine2Item2.Status == Status.Open)
                                                            {
                                                                _locProjectLine2Item2.Status = Status.Progress;
                                                                _locProjectLine2Item2.StatusDate = now;
                                                                _locProjectLine2Item2.Save();
                                                                _locProjectLine2Item2.Session.CommitTransaction();
                                                            }
                                                        }
                                                    }

                                                    if (_locProjectLine2Services != null && _locProjectLine2Services.Count > 0)
                                                    {
                                                        foreach (ProjectLine2Service _locProjectLine2Service in _locProjectLine2Services)
                                                        {
                                                            if (_locProjectLine2Service.Status == Status.Open)
                                                            {
                                                                _locProjectLine2Service.Status = Status.Progress;
                                                                _locProjectLine2Service.StatusDate = now;
                                                                _locProjectLine2Service.Save();
                                                                _locProjectLine2Service.Session.CommitTransaction();
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        #endregion ProjectLine2
                                    }

                                    SuccessMessageShow("Project Header has been successfully updated to progress");
                                }
                                else
                                {
                                    ErrorMessageShow("Project Header Data Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Project Header Data Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ProjectHeader " + ex.ToString());
            }
        }

        //private void ProjectHeaderCompareAmountAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        //{
        //    try
        //    {
        //        GlobalFunction _globFunc = new GlobalFunction();
        //        IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
        //        ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
        //        DateTime now = DateTime.Now;
        //        Session _currSession = null;
        //        string _currObjectId = null;

        //        if (this.ObjectSpace != null)
        //        {
        //            _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
        //        }

        //        if (_objectsToProcess != null)
        //        {
        //            foreach (Object obj in _objectsToProcess)
        //            {
        //                ProjectHeader _locProjectHeaderOS = (ProjectHeader)_objectSpace.GetObject(obj);

        //                if (_locProjectHeaderOS != null)
        //                {
        //                    if (_locProjectHeaderOS.Code != null)
        //                    {
        //                        _currObjectId = _locProjectHeaderOS.Code;

        //                        ProjectHeader _locProjectHeaderXPO = _currSession.FindObject<ProjectHeader>
        //                                                            (new GroupOperator(GroupOperatorType.And,
        //                                                             new BinaryOperator("Code", _currObjectId)));

        //                        #region ProjectHeaderXPO
        //                        if (_locProjectHeaderXPO != null)
        //                        {
        //                            //Total Amount in ProjectLine
        //                            SetApproximateAmountFromProjectLine(_currSession, _locProjectHeaderXPO);
        //                            //Total Amount in ProjectLine2
        //                            SetApproximateAmountFromProjectLine2(_currSession, _locProjectHeaderXPO);
        //                        }
        //                        #endregion ProjectHeaderXPO
        //                        SuccessMessageShow("Project Header Not Available");
        //                    }
        //                }
        //                ErrorMessageShow("Project Header Not Available");
        //            }
        //        }

        //        if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
        //        {
        //            _objectSpace.CommitChanges();
        //            _objectSpace.Refresh();
        //        }
        //        if (View is ListView)
        //        {
        //            _objectSpace.CommitChanges();
        //            View.ObjectSpace.Refresh();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Tracing.Tracer.LogError(" Controller = ProjectHeaderAction " + ex.ToString());
        //    }
        //}

        private void ProjectHeaderLockAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;


                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        ProjectHeader _locProjectHeaderOS = (ProjectHeader)_objectSpace.GetObject(obj);

                        if (_locProjectHeaderOS != null)
                        {
                            if (_locProjectHeaderOS.Code != null)
                            {
                                _currObjectId = _locProjectHeaderOS.Code;

                                ProjectHeader _locProjectHeaderXPO = _currSession.FindObject<ProjectHeader>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locProjectHeaderXPO != null)
                                {
                                    if (_locProjectHeaderXPO.Status == Status.Progress || _locProjectHeaderXPO.Status == Status.Lock)
                                    {
                                        if (_locProjectHeaderXPO.Status == Status.Progress)
                                        {
                                            _locProjectHeaderXPO.Status = Status.Lock;
                                            _locProjectHeaderXPO.StatusDate = now;
                                            _locProjectHeaderXPO.ActivationPosting = true;
                                            _locProjectHeaderXPO.Save();
                                            _locProjectHeaderXPO.Session.CommitTransaction();
                                        }

                                        #region ProjectLine
                                        XPCollection<ProjectLine> _locProjectLines = new XPCollection<ProjectLine>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("ProjectHeader", _locProjectHeaderXPO)));

                                        if (_locProjectLines != null && _locProjectLines.Count > 0)
                                        {
                                            foreach (ProjectLine _locProjectLine in _locProjectLines)
                                            {
                                                if (_locProjectLine.Status == Status.Progress || _locProjectLine.Status == Status.Lock)
                                                {
                                                    if (_locProjectLine.Status == Status.Progress)
                                                    {
                                                        _locProjectLine.Status = Status.Lock;
                                                        _locProjectLine.StatusDate = now;
                                                        _locProjectLine.ActivationPosting = true;
                                                        _locProjectLine.Save();
                                                        _locProjectLine.Session.CommitTransaction();
                                                    }


                                                    XPCollection<ProjectLineItem> _locProjectLineItems = new XPCollection<ProjectLineItem>(_currSession,
                                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                                     new BinaryOperator("ProjectLine", _locProjectLine),
                                                                                                     new BinaryOperator("ProjectHeader", _locProjectHeaderXPO)));

                                                    XPCollection<ProjectLineItem2> _locProjectLineItem2s = new XPCollection<ProjectLineItem2>(_currSession,
                                                                                                         new GroupOperator(GroupOperatorType.And,
                                                                                                         new BinaryOperator("ProjectLine", _locProjectLine),
                                                                                                         new BinaryOperator("ProjectHeader", _locProjectHeaderXPO)));

                                                    XPCollection<ProjectLineService> _locProjectLineServices = new XPCollection<ProjectLineService>(_currSession,
                                                                                                         new GroupOperator(GroupOperatorType.And,
                                                                                                         new BinaryOperator("ProjectLine", _locProjectLine),
                                                                                                         new BinaryOperator("ProjectHeader", _locProjectHeaderXPO)));

                                                    if (_locProjectLineItems != null && _locProjectLineItems.Count > 0)
                                                    {
                                                        foreach (ProjectLineItem _locProjectLineItem in _locProjectLineItems)
                                                        {
                                                            if (_locProjectLineItem.Status == Status.Progress)
                                                            {
                                                                _locProjectLineItem.Status = Status.Lock;
                                                                _locProjectLineItem.StatusDate = now;
                                                                _locProjectLineItem.ActivationPosting = true;
                                                                _locProjectLineItem.Save();
                                                                _locProjectLineItem.Session.CommitTransaction();
                                                            }
                                                        }
                                                    }

                                                    if (_locProjectLineItem2s != null && _locProjectLineItem2s.Count > 0)
                                                    {
                                                        foreach (ProjectLineItem2 _locProjectLineItem2 in _locProjectLineItem2s)
                                                        {
                                                            if (_locProjectLineItem2.Status == Status.Progress)
                                                            {
                                                                _locProjectLineItem2.Status = Status.Lock;
                                                                _locProjectLineItem2.StatusDate = now;
                                                                _locProjectLineItem2.ActivationPosting = true;
                                                                _locProjectLineItem2.Save();
                                                                _locProjectLineItem2.Session.CommitTransaction();
                                                            }
                                                        }
                                                    }

                                                    if (_locProjectLineServices != null && _locProjectLineServices.Count > 0)
                                                    {
                                                        foreach (ProjectLineService _locProjectLineService in _locProjectLineServices)
                                                        {
                                                            if (_locProjectLineService.Status == Status.Progress)
                                                            {
                                                                _locProjectLineService.Status = Status.Lock;
                                                                _locProjectLineService.StatusDate = now;
                                                                _locProjectLineService.ActivationPosting = true;
                                                                _locProjectLineService.Save();
                                                                _locProjectLineService.Session.CommitTransaction();
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        #endregion ProjectLine

                                        #region ProjectLine2
                                        XPCollection<ProjectLine2> _locProjectLine2s = new XPCollection<ProjectLine2>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("ProjectHeader", _locProjectHeaderXPO)));

                                        if (_locProjectLine2s != null && _locProjectLine2s.Count > 0)
                                        {
                                            foreach (ProjectLine2 _locProjectLine2 in _locProjectLine2s)
                                            {
                                                if (_locProjectLine2.Status == Status.Progress || _locProjectLine2.Status == Status.Lock)
                                                {
                                                    if (_locProjectLine2.Status == Status.Progress)
                                                    {
                                                        _locProjectLine2.Status = Status.Lock;
                                                        _locProjectLine2.StatusDate = now;
                                                        _locProjectLine2.ActivationPosting = true;
                                                        _locProjectLine2.Save();
                                                        _locProjectLine2.Session.CommitTransaction();
                                                    }


                                                    XPCollection<ProjectLine2Item> _locProjectLine2Items = new XPCollection<ProjectLine2Item>(_currSession,
                                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                                     new BinaryOperator("ProjectLine2", _locProjectLine2),
                                                                                                     new BinaryOperator("ProjectHeader", _locProjectHeaderXPO)));

                                                    XPCollection<ProjectLine2Item2> _locProjectLine2Item2s = new XPCollection<ProjectLine2Item2>(_currSession,
                                                                                                         new GroupOperator(GroupOperatorType.And,
                                                                                                         new BinaryOperator("ProjectLine2", _locProjectLine2),
                                                                                                         new BinaryOperator("ProjectHeader", _locProjectHeaderXPO)));

                                                    XPCollection<ProjectLine2Service> _locProjectLine2Services = new XPCollection<ProjectLine2Service>(_currSession,
                                                                                                         new GroupOperator(GroupOperatorType.And,
                                                                                                         new BinaryOperator("ProjectLine2", _locProjectLine2),
                                                                                                         new BinaryOperator("ProjectHeader", _locProjectHeaderXPO)));

                                                    if (_locProjectLine2Items != null && _locProjectLine2Items.Count > 0)
                                                    {
                                                        foreach (ProjectLine2Item _locProjectLine2Item in _locProjectLine2Items)
                                                        {
                                                            if (_locProjectLine2Item.Status == Status.Progress)
                                                            {
                                                                _locProjectLine2Item.Status = Status.Lock;
                                                                _locProjectLine2Item.StatusDate = now;
                                                                _locProjectLine2Item.ActivationPosting = true;
                                                                _locProjectLine2Item.Save();
                                                                _locProjectLine2Item.Session.CommitTransaction();
                                                            }
                                                        }
                                                    }

                                                    if (_locProjectLine2Item2s != null && _locProjectLine2Item2s.Count > 0)
                                                    {
                                                        foreach (ProjectLine2Item2 _locProjectLine2Item2 in _locProjectLine2Item2s)
                                                        {
                                                            if (_locProjectLine2Item2.Status == Status.Progress)
                                                            {
                                                                _locProjectLine2Item2.Status = Status.Lock;
                                                                _locProjectLine2Item2.StatusDate = now;
                                                                _locProjectLine2Item2.ActivationPosting = true;
                                                                _locProjectLine2Item2.Save();
                                                                _locProjectLine2Item2.Session.CommitTransaction();
                                                            }
                                                        }
                                                    }

                                                    if (_locProjectLine2Services != null && _locProjectLine2Services.Count > 0)
                                                    {
                                                        foreach (ProjectLine2Service _locProjectLine2Service in _locProjectLine2Services)
                                                        {
                                                            if (_locProjectLine2Service.Status == Status.Progress)
                                                            {
                                                                _locProjectLine2Service.Status = Status.Lock;
                                                                _locProjectLine2Service.StatusDate = now;
                                                                _locProjectLine2Service.ActivationPosting = true;
                                                                _locProjectLine2Service.Save();
                                                                _locProjectLine2Service.Session.CommitTransaction();
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        #endregion ProjectLine2
                                    }

                                    SuccessMessageShow("Project Header has been successfully updated to Lock");
                                }
                                else
                                {
                                    ErrorMessageShow("Project Header Data Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Project Header Data Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ProjectHeader " + ex.ToString());
            }
        }

        private void ProjectHeaderPrePOAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        ProjectHeader _locProjectHeaderOS = (ProjectHeader)_objectSpace.GetObject(obj);

                        if (_locProjectHeaderOS != null)
                        {
                            if (_locProjectHeaderOS.Code != null)
                            {
                                _currObjectId = _locProjectHeaderOS.Code;

                                ProjectHeader _locProjectHeaderXPO = _currSession.FindObject<ProjectHeader>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                #region ProjectHeaderXPO
                                if (_locProjectHeaderXPO != null)
                                {
                                    SetPrePurchaseOrderPosting(_currSession, _locProjectHeaderXPO);
                                    SetPrePurchaseOrder2Posting(_currSession, _locProjectHeaderXPO);
                                    SuccessMessageShow("Pre Purchase Order was succesfully created");
                                }
                                #endregion ProjectHeaderXPO
                                ErrorMessageShow("Project Header Not Available");
                            }
                        }
                        ErrorMessageShow("Project Header Not Available");
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Controller = ProjectHeaderAction " + ex.ToString());
            }
        }

        private void ProjectHeaderPreSOAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        ProjectHeader _locProjectHeaderOS = (ProjectHeader)_objectSpace.GetObject(obj);

                        if (_locProjectHeaderOS != null)
                        {
                            if (_locProjectHeaderOS.Code != null)
                            {
                                _currObjectId = _locProjectHeaderOS.Code;

                                ProjectHeader _locProjectHeaderXPO = _currSession.FindObject<ProjectHeader>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                #region ProjectHeaderXPO
                                if (_locProjectHeaderXPO != null)
                                {
                                    SetPreSalesOrderPosting(_currSession, _locProjectHeaderXPO);
                                    SetPreSalesOrder2Posting(_currSession, _locProjectHeaderXPO);
                                    SuccessMessageShow("Pre Sales Order was succesfully created");
                                }
                                #endregion ProjectHeaderXPO

                                ErrorMessageShow("Project Header Not Available");
                            }
                        }
                        ErrorMessageShow("Project Header Not Available");
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Controller = ProjectHeaderAction " + ex.ToString());
            }
        }

        private void ProjectHeaderListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(ProjectHeader)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ProjectHeader " + ex.ToString());
            }
        }

        #region PrePOPostingMethod

        private void SetPrePurchaseOrderPosting(Session _currSession, ProjectHeader _locProjectHeaderXPO)
        {
            try
            {
                XPQuery<ProjectLineItem2> _projectLineItem2sQuery = new XPQuery<ProjectLineItem2>(_currSession);

                if (_locProjectHeaderXPO.Status == Status.Lock)
                {
                    var _projectLineItem2s = from pli2 in _projectLineItem2sQuery
                                             where (pli2.ProjectHeader == _locProjectHeaderXPO)
                                             where (pli2.Status == Status.Lock)
                                             group pli2 by pli2.Item into g
                                             select new { Item = g.Key };

                    if (_projectLineItem2s != null && _projectLineItem2s.Count() > 0)
                    {
                        foreach (var _projectLineItem2 in _projectLineItem2s)
                        {
                            XPCollection<ProjectLineItem2> _locProjectLineItem2s = new XPCollection<ProjectLineItem2>(_currSession,
                                                    new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("ProjectHeader", _locProjectHeaderXPO),
                                                    new BinaryOperator("Item", _projectLineItem2.Item),
                                                    new BinaryOperator("Company", _locProjectHeaderXPO.Company),
                                                    new BinaryOperator("Status", Status.Lock)));

                            double _locDefaultQty = 0;
                            UnitOfMeasure _locDefaultUOM = null;
                            double _locQty = 0;
                            UnitOfMeasure _locUOM = null;
                            double _locTotalQty = 0;
                            double _locUnitPrice = 0;
                            double _locTotalUnitPrice = 0;

                            if (_locProjectLineItem2s != null && _locProjectLineItem2s.Count > 0)
                            {
                                foreach (ProjectLineItem2 _locProjectLineItem2 in _locProjectLineItem2s)
                                {
                                    _locDefaultQty = _locDefaultQty + _locProjectLineItem2.DQty;
                                    _locDefaultUOM = _locProjectLineItem2.DUOM;
                                    _locQty = _locQty + _locProjectLineItem2.Qty;
                                    _locUOM = _locProjectLineItem2.UOM;
                                    _locTotalQty = _locTotalQty + _locProjectLineItem2.TQty;
                                    _locUnitPrice = _locProjectLineItem2.UnitAmount;
                                    _locTotalUnitPrice = _locTotalUnitPrice + _locProjectLineItem2.TotalUnitAmount;
                                }

                                PrePurchaseOrder _locPrePurchaseOrder = _currSession.FindObject<PrePurchaseOrder>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("ProjectHeader", _locProjectHeaderXPO),
                                                                     new BinaryOperator("Company", _locProjectHeaderXPO.Company),
                                                                     new BinaryOperator("Item", _projectLineItem2.Item)));

                                if (_locPrePurchaseOrder != null)
                                {
                                    _locPrePurchaseOrder.Item = _projectLineItem2.Item;
                                    _locPrePurchaseOrder.MxDQty = _locDefaultQty;
                                    _locPrePurchaseOrder.MxDUOM = _locDefaultUOM;
                                    _locPrePurchaseOrder.MxQty = _locQty;
                                    _locPrePurchaseOrder.MxTQty = _locTotalQty;
                                    _locPrePurchaseOrder.MxUAmount = _locUnitPrice;
                                    _locPrePurchaseOrder.MxTUAmount = _locTotalUnitPrice;
                                    _locPrePurchaseOrder.DQty = _locDefaultQty;
                                    _locPrePurchaseOrder.Qty = _locQty;
                                    _locPrePurchaseOrder.TotalQty = _locTotalQty;
                                    _locPrePurchaseOrder.Save();
                                    _locPrePurchaseOrder.Session.CommitTransaction();
                                }
                                else
                                {
                                    PrePurchaseOrder _saveData = new PrePurchaseOrder(_currSession)
                                    {
                                        Item = _projectLineItem2.Item,
                                        MxDQty = _locDefaultQty,
                                        MxDUOM = _locDefaultUOM,
                                        MxQty = _locQty,
                                        MxTQty = _locTotalQty,
                                        MxUAmount = _locUnitPrice,
                                        MxTUAmount = _locTotalUnitPrice,
                                        DQty = _locDefaultQty,
                                        Qty = _locQty,
                                        TotalQty = _locTotalQty,
                                        ProjectHeader = _locProjectHeaderXPO,
                                        Company = _locProjectHeaderXPO.Company,
                                    };
                                    _saveData.Save();
                                    _saveData.Session.CommitTransaction();
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ProjectHeader " + ex.ToString());
            }
        }

        private void SetPrePurchaseOrder2Posting(Session _currSession, ProjectHeader _locProjectHeaderXPO)
        {
            try
            {
                XPQuery<ProjectLineItem2> _projectLineItem2sQuery = new XPQuery<ProjectLineItem2>(_currSession);

                if (_locProjectHeaderXPO.Status == Status.Lock)
                {
                    XPCollection<ProjectLineService> _locProjectLineServices = new XPCollection<ProjectLineService>(_currSession,
                                                    new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("ProjectHeader", _locProjectHeaderXPO),
                                                    new BinaryOperator("Company", _locProjectHeaderXPO.Company),
                                                    new BinaryOperator("Status", Status.Lock)));

                    if (_locProjectLineServices != null && _locProjectLineServices.Count() > 0)
                    {


                        foreach (ProjectLineService _locProjectLineService in _locProjectLineServices)
                        {
                            PrePurchaseOrder2 _locPrePurchaseOrder2 = _currSession.FindObject<PrePurchaseOrder2>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("ProjectHeader", _locProjectHeaderXPO),
                                                            new BinaryOperator("Company", _locProjectHeaderXPO.Company),
                                                            new BinaryOperator("Item", _locProjectLineService.Item)));
                            if (_locPrePurchaseOrder2 != null)
                            {
                                _locPrePurchaseOrder2.Name = _locProjectLineService.Name;
                                _locPrePurchaseOrder2.Item = _locProjectLineService.Item;
                                _locPrePurchaseOrder2.MaxQty = _locProjectLineService.Qty;
                                _locPrePurchaseOrder2.MaxUOM = _locProjectLineService.UOM;
                                _locPrePurchaseOrder2.MaxUnitAmount = _locProjectLineService.UnitAmount;
                                _locPrePurchaseOrder2.MaxTotalUnitAmount = _locProjectLineService.TotalUnitAmount;
                                _locPrePurchaseOrder2.Qty = _locProjectLineService.Qty;
                                _locPrePurchaseOrder2.UOM = _locProjectLineService.UOM;
                                _locPrePurchaseOrder2.UnitAmount = _locProjectLineService.UnitAmount;
                                _locPrePurchaseOrder2.TotalUnitAmount = _locProjectLineService.TotalUnitAmount;
                                _locPrePurchaseOrder2.Company = _locProjectHeaderXPO.Company;
                                _locPrePurchaseOrder2.ProjectHeader = _locProjectHeaderXPO;
                                _locPrePurchaseOrder2.Company = _locProjectHeaderXPO.Company;
                                _locPrePurchaseOrder2.Save();
                                _locPrePurchaseOrder2.Session.CommitTransaction();
                            }
                            else
                            {
                                PrePurchaseOrder2 _saveData = new PrePurchaseOrder2(_currSession)
                                {
                                    Name = _locProjectLineService.Name,
                                    Item = _locProjectLineService.Item,
                                    MaxQty = _locProjectLineService.Qty,
                                    MaxUOM = _locProjectLineService.UOM,
                                    MaxUnitAmount = _locProjectLineService.UnitAmount,
                                    MaxTotalUnitAmount = _locProjectLineService.TotalUnitAmount,
                                    Qty = _locProjectLineService.Qty,
                                    UOM = _locProjectLineService.UOM,
                                    UnitAmount = _locProjectLineService.UnitAmount,
                                    TotalUnitAmount = _locProjectLineService.TotalUnitAmount,
                                    Company = _locProjectHeaderXPO.Company,
                                    ProjectHeader = _locProjectHeaderXPO,
                                };
                                _saveData.Save();
                                _saveData.Session.CommitTransaction();
                            }

                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ProjectHeader " + ex.ToString());
            }
        }

        #endregion PrePOPostingMethod

        #region PreSOPostingMethod

        private void SetPreSalesOrderPosting(Session _currSession, ProjectHeader _locProjectHeaderXPO)
        {
            try
            {
                XPQuery<ProjectLineItem2> _projectLineItem2sQuery = new XPQuery<ProjectLineItem2>(_currSession);

                if (_locProjectHeaderXPO.Status == Status.Lock)
                {
                    var _projectLineItem2s = from pli2 in _projectLineItem2sQuery
                                             where (pli2.ProjectHeader == _locProjectHeaderXPO)
                                             where (pli2.Status == Status.Lock)
                                             group pli2 by pli2.Item into g
                                             select new { Item = g.Key };

                    if (_projectLineItem2s != null && _projectLineItem2s.Count() > 0)
                    {
                        foreach (var _projectLineItem2 in _projectLineItem2s)
                        {
                            XPCollection<ProjectLineItem2> _locProjectLineItem2s = new XPCollection<ProjectLineItem2>(_currSession,
                                                                                   new GroupOperator(GroupOperatorType.And,
                                                                                   new BinaryOperator("ProjectHeader", _locProjectHeaderXPO),
                                                                                   new BinaryOperator("Item", _projectLineItem2.Item),
                                                                                   new BinaryOperator("Status", Status.Lock)));

                            double _locDefaultQty = 0;
                            UnitOfMeasure _locDefaultUOM = null;
                            double _locQty = 0;
                            UnitOfMeasure _locUOM = null;
                            double _locTotalQty = 0;
                            double _locUnitPrice = 0;
                            double _locTotalUnitPrice = 0;

                            if (_locProjectLineItem2s != null && _locProjectLineItem2s.Count > 0)
                            {
                                foreach (ProjectLineItem2 _locProjectLineItem2 in _locProjectLineItem2s)
                                {
                                    _locDefaultQty = _locDefaultQty + _locProjectLineItem2.DQty;
                                    _locDefaultUOM = _locProjectLineItem2.DUOM;
                                    _locQty = _locQty + _locProjectLineItem2.Qty;
                                    _locUOM = _locProjectLineItem2.UOM;
                                    _locTotalQty = _locTotalQty + _locProjectLineItem2.TQty;
                                    _locUnitPrice = _locProjectLineItem2.UnitAmount;
                                    _locTotalUnitPrice = _locTotalUnitPrice + _locProjectLineItem2.TotalUnitAmount;
                                }

                                PreSalesOrder _locPreSalesOrder = _currSession.FindObject<PreSalesOrder>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("ProjectHeader", _locProjectHeaderXPO),
                                                                     new BinaryOperator("Item", _projectLineItem2.Item)));

                                if (_locPreSalesOrder != null)
                                {
                                    _locPreSalesOrder.Item = _projectLineItem2.Item;
                                    _locPreSalesOrder.MxDQty = _locDefaultQty;
                                    _locPreSalesOrder.MxDUOM = _locDefaultUOM;
                                    _locPreSalesOrder.MxUOM = _locUOM;
                                    _locPreSalesOrder.MxQty = _locQty;
                                    _locPreSalesOrder.MxTQty = _locTotalQty;
                                    _locPreSalesOrder.MxUAmount = _locUnitPrice;
                                    _locPreSalesOrder.MxTUAmount = _locTotalUnitPrice;
                                    _locPreSalesOrder.DQty = _locDefaultQty;
                                    _locPreSalesOrder.Qty = _locQty;
                                    _locPreSalesOrder.TotalQty = _locTotalQty;
                                    _locPreSalesOrder.Save();
                                    _locPreSalesOrder.Session.CommitTransaction();
                                }
                                else
                                {
                                    PreSalesOrder _saveData = new PreSalesOrder(_currSession)
                                    {
                                        Item = _projectLineItem2.Item,
                                        MxDQty = _locDefaultQty,
                                        MxDUOM = _locDefaultUOM,
                                        MxUOM = _locUOM,
                                        MxQty = _locQty,
                                        MxTQty = _locTotalQty,
                                        MxUAmount = _locUnitPrice,
                                        MxTUAmount = _locTotalUnitPrice,
                                        DQty = _locDefaultQty,
                                        Qty = _locQty,
                                        TotalQty = _locTotalQty,
                                        ProjectHeader = _locProjectHeaderXPO,
                                        Company = _locProjectHeaderXPO.Company,
                                    };
                                    _saveData.Save();
                                    _saveData.Session.CommitTransaction();
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ProjectHeader " + ex.ToString());
            }
        }

        private void SetPreSalesOrder2Posting(Session _currSession, ProjectHeader _locProjectHeaderXPO)
        {
            try
            {
                XPQuery<ProjectLineItem2> _projectLineItem2sQuery = new XPQuery<ProjectLineItem2>(_currSession);

                if (_locProjectHeaderXPO.Status == Status.Lock)
                {
                    XPCollection<ProjectLineService> _locProjectLineServices = new XPCollection<ProjectLineService>(_currSession,
                                                                               new GroupOperator(GroupOperatorType.And,
                                                                               new BinaryOperator("ProjectHeader", _locProjectHeaderXPO),
                                                                               new BinaryOperator("Status", Status.Lock)));

                    if (_locProjectLineServices != null && _locProjectLineServices.Count() > 0)
                    {
                        foreach (ProjectLineService _locProjectLineService in _locProjectLineServices)
                        {
                            PreSalesOrder2 _saveData = new PreSalesOrder2(_currSession)
                            {
                                Item = _locProjectLineService.Item,
                                Name = _locProjectLineService.Name,
                                MaxQty = _locProjectLineService.Qty,
                                MaxUOM = _locProjectLineService.UOM,
                                MaxUnitAmount = _locProjectLineService.UnitAmount,
                                MaxTotalUnitAmount = _locProjectLineService.TotalUnitAmount,
                                Qty = _locProjectLineService.Qty,
                                UOM = _locProjectLineService.UOM,
                                UnitAmount = _locProjectLineService.UnitAmount,
                                TotalUnitAmount = _locProjectLineService.TotalUnitAmount,
                                ProjectHeader = _locProjectHeaderXPO,
                                Company = _locProjectHeaderXPO.Company,
                            };
                            _saveData.Save();
                            _saveData.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ProjectHeader " + ex.ToString());
            }
        }

        private void SetRemainQuantitySO(Session _currSession, PrePurchaseOrder _locPrePurchaseOrder)
        {
            try
            {
                #region Remain Qty  with PostedCount != 0
                if (_locPrePurchaseOrder.PostedCount > 0)
                {
                    double _locRmDqty = 0;
                    double _locRmQty = 0;
                    double _locInvLineTotal = 0;
                    double _locRmTQty = 0;
                    bool _locActivationPosting = false;

                    if (_locPrePurchaseOrder.RmDQty > 0 || _locPrePurchaseOrder.RmQty > 0 && _locPrePurchaseOrder.MxTQty != _locPrePurchaseOrder.RmTQty)
                    {
                        _locRmDqty = _locPrePurchaseOrder.RmDQty - _locPrePurchaseOrder.DQty;
                        _locRmQty = _locPrePurchaseOrder.RmQty - _locPrePurchaseOrder.Qty;

                        if (_locPrePurchaseOrder.Item != null && _locPrePurchaseOrder.MxUOM != null && _locPrePurchaseOrder.MxDUOM != null)
                        {
                            ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                    (new GroupOperator(GroupOperatorType.And,
                                                     new BinaryOperator("Item", _locPrePurchaseOrder.Item),
                                                     new BinaryOperator("UOM", _locPrePurchaseOrder.MxUOM),
                                                     new BinaryOperator("DefaultUOM", _locPrePurchaseOrder.MxDUOM),
                                                     new BinaryOperator("Active", true)));
                            if (_locItemUOM != null)
                            {
                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDqty;
                                }
                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDqty;
                                }
                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty + _locRmDqty;
                                }
                            }
                        }
                        else
                        {
                            _locInvLineTotal = _locRmQty + _locRmDqty;
                        }

                        if (_locRmDqty > 0 || _locRmQty > 0)
                        {
                            _locRmTQty = _locPrePurchaseOrder.RmTQty - _locPrePurchaseOrder.TotalQty;
                        }
                        else
                        {
                            _locRmTQty = _locPrePurchaseOrder.RmTQty - _locPrePurchaseOrder.TotalQty;
                            _locActivationPosting = true;
                        }

                        _locPrePurchaseOrder.ActivationPosting = _locActivationPosting;
                        _locPrePurchaseOrder.Select = false;
                        _locPrePurchaseOrder.RmDQty = _locRmDqty;
                        _locPrePurchaseOrder.RmQty = _locRmQty;
                        _locPrePurchaseOrder.RmTQty = _locRmTQty;
                        _locPrePurchaseOrder.PostedCount = _locPrePurchaseOrder.PostedCount + 1;
                        _locPrePurchaseOrder.DQty = 0;
                        _locPrePurchaseOrder.Qty = 0;
                        _locPrePurchaseOrder.TotalQty = 0;
                        _locPrePurchaseOrder.Save();
                        _locPrePurchaseOrder.Session.CommitTransaction();
                    }
                }
                #endregion Remain Qty  with PostedCount != 0

                #region Remain Qty with PostedCount == 0
                else
                {
                    double _locRmDqty = 0;
                    double _locRmQty = 0;
                    double _locInvLineTotal = 0;
                    double _locRmTQty = 0;
                    _locRmDqty = _locPrePurchaseOrder.MxDQty - _locPrePurchaseOrder.DQty;
                    _locRmQty = _locPrePurchaseOrder.MxQty - _locPrePurchaseOrder.Qty;

                    #region Remain > 0
                    if (_locRmDqty > 0 || _locRmQty > 0)
                    {
                        if (_locPrePurchaseOrder.Item != null && _locPrePurchaseOrder.MxUOM != null && _locPrePurchaseOrder.MxDUOM != null)
                        {
                            ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                    (new GroupOperator(GroupOperatorType.And,
                                                     new BinaryOperator("Item", _locPrePurchaseOrder.Item),
                                                     new BinaryOperator("UOM", _locPrePurchaseOrder.MxUOM),
                                                     new BinaryOperator("DefaultUOM", _locPrePurchaseOrder.MxDUOM),
                                                     new BinaryOperator("Active", true)));
                            if (_locItemUOM != null)
                            {
                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDqty;
                                }
                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDqty;
                                }
                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty + _locRmDqty;
                                }
                            }
                        }
                        else
                        {
                            _locInvLineTotal = _locRmQty + _locRmDqty;
                        }

                        if (_locInvLineTotal > 0)
                        {
                            if (_locPrePurchaseOrder.RmTQty > 0)
                            {
                                _locRmTQty = _locPrePurchaseOrder.RmTQty - _locInvLineTotal;
                            }
                            else
                            {
                                _locRmTQty = _locInvLineTotal;
                            }

                        }

                        _locPrePurchaseOrder.RmDQty = _locRmDqty;
                        _locPrePurchaseOrder.RmQty = _locRmQty;
                        _locPrePurchaseOrder.RmTQty = _locRmTQty;
                        _locPrePurchaseOrder.PostedCount = _locPrePurchaseOrder.PostedCount + 1;
                        _locPrePurchaseOrder.Select = false;
                        _locPrePurchaseOrder.DQty = 0;
                        _locPrePurchaseOrder.Qty = 0;
                        _locPrePurchaseOrder.TotalQty = 0;
                        _locPrePurchaseOrder.Save();
                        _locPrePurchaseOrder.Session.CommitTransaction();
                    }
                    #endregion Remain > 0

                    #region Remain <= 0
                    if (_locRmDqty <= 0 && _locRmQty <= 0)
                    {
                        _locPrePurchaseOrder.ActivationPosting = true;
                        _locPrePurchaseOrder.RmDQty = 0;
                        _locPrePurchaseOrder.RmQty = 0;
                        _locPrePurchaseOrder.RmTQty = 0;
                        _locPrePurchaseOrder.PostedCount = _locPrePurchaseOrder.PostedCount + 1;
                        _locPrePurchaseOrder.Select = false;
                        _locPrePurchaseOrder.DQty = 0;
                        _locPrePurchaseOrder.Qty = 0;
                        _locPrePurchaseOrder.TotalQty = 0;
                        _locPrePurchaseOrder.Save();
                        _locPrePurchaseOrder.Session.CommitTransaction();
                    }
                    #endregion Remain < 0
                }
                #endregion Remain Qty with PostedCount == 0

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PrePurchaseOrder " + ex.ToString());
            }
        }

        private double GetTotalQuantitySO(Session _currSession, PrePurchaseOrder _locPrePurchaseOrder)
        {
            double _result = 0;
            try
            {

                if (_locPrePurchaseOrder.Item != null && _locPrePurchaseOrder.MxUOM != null && _locPrePurchaseOrder.MxDUOM != null)
                {
                    ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                            (new GroupOperator(GroupOperatorType.And,
                                             new BinaryOperator("Item", _locPrePurchaseOrder.Item),
                                             new BinaryOperator("UOM", _locPrePurchaseOrder.MxUOM),
                                             new BinaryOperator("DefaultUOM", _locPrePurchaseOrder.MxDUOM),
                                             new BinaryOperator("Active", true)));
                    if (_locItemUOM != null)
                    {
                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                        {
                            _result = _locPrePurchaseOrder.Qty * _locItemUOM.DefaultConversion + _locPrePurchaseOrder.DQty;
                        }
                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                        {
                            _result = _locPrePurchaseOrder.Qty / _locItemUOM.Conversion + _locPrePurchaseOrder.DQty;
                        }
                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                        {
                            _result = _locPrePurchaseOrder.Qty + _locPrePurchaseOrder.DQty;
                        }
                    }
                }
                else
                {
                    _result = _locPrePurchaseOrder.Qty + _locPrePurchaseOrder.DQty;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PrePurchaseOrder " + ex.ToString());
            }
            return _result;
        }

        #endregion PreSOPostingMethod

        #region CompareAmountMethod

        //private void SetApproximateAmountFromProjectLine(Session _currSession, ProjectHeader _locProjectHeaderXPO)
        //{
        //    try
        //    {
        //        double _totPriceProjectLineItem2 = 0;
        //        double _totPriceProjectLineService = 0;

        //        if (_locProjectHeaderXPO.Status == Status.Progress || _locProjectHeaderXPO.Status == Status.Lock)
        //        {
        //            XPCollection<ProjectLine> _locProjectLines = new XPCollection<ProjectLine>
        //                                                (_currSession, new GroupOperator(GroupOperatorType.And,
        //                                                new BinaryOperator("ProjectHeader", _locProjectHeaderXPO)));

        //            if (_locProjectLines != null && _locProjectLines.Count() > 0)
        //            {
        //                foreach (ProjectLine _locProjectLine in _locProjectLines)
        //                {
        //                    if (_locProjectLine.Status == Status.Progress || _locProjectLine.Status == Status.Lock)
        //                    {
        //                        XPCollection<ProjectLineItem2> _locProjectLineItem2s = new XPCollection<ProjectLineItem2>
        //                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
        //                                                            new BinaryOperator("ProjectHeader", _locProjectHeaderXPO),
        //                                                            new BinaryOperator("ProjectLine", _locProjectLine)));

        //                        if (_locProjectLineItem2s != null && _locProjectLineItem2s.Count() > 0)
        //                        {
        //                            foreach (ProjectLineItem2 _locProjectLineItem2 in _locProjectLineItem2s)
        //                            {
        //                                if (_locProjectLineItem2.Status == Status.Progress || _locProjectLineItem2.Status == Status.Lock)
        //                                {
        //                                    _totPriceProjectLineItem2 = _totPriceProjectLineItem2 + _locProjectLineItem2.TotalUnitAmount;
        //                                }
        //                            }
        //                        }

        //                        XPCollection<ProjectLineService> _locProjectLineServices = new XPCollection<ProjectLineService>
        //                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
        //                                                            new BinaryOperator("ProjectHeader", _locProjectHeaderXPO),
        //                                                            new BinaryOperator("ProjectLine", _locProjectLine)));

        //                        if (_locProjectLineServices != null && _locProjectLineServices.Count() > 0)
        //                        {
        //                            foreach (ProjectLineService _locProjectLineService in _locProjectLineServices)
        //                            {
        //                                if (_locProjectLineService.Status == Status.Progress || _locProjectLineService.Status == Status.Lock)
        //                                {
        //                                    _totPriceProjectLineService = _totPriceProjectLineService + _locProjectLineService.TotalUnitAmount;
        //                                }
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //            _locProjectHeaderXPO.ApproximateAmount = _totPriceProjectLineItem2 + _totPriceProjectLineService;
        //            _locProjectHeaderXPO.Save();
        //            _locProjectHeaderXPO.Session.CommitTransaction();
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        Tracing.Tracer.LogError(" BusinessObject = ProjectHeader " + ex.ToString());
        //    }
        //}

        //private void SetApproximateAmountFromProjectLine2(Session _currSession, ProjectHeader _locProjectHeaderXPO)
        //{
        //    try
        //    {
        //        double _totPriceProjectLine2Item2 = 0;
        //        double _totPriceProjectLine2Service = 0;

        //        if (_locProjectHeaderXPO.Status == Status.Progress || _locProjectHeaderXPO.Status == Status.Lock)
        //        {
        //            XPCollection<ProjectLine2> _locProjectLine2s = new XPCollection<ProjectLine2>
        //                                                (_currSession, new GroupOperator(GroupOperatorType.And,
        //                                                new BinaryOperator("ProjectHeader", _locProjectHeaderXPO)));

        //            if (_locProjectLine2s != null && _locProjectLine2s.Count() > 0)
        //            {
        //                foreach (ProjectLine2 _locProjectLine2 in _locProjectLine2s)
        //                {
        //                    if (_locProjectLine2.Status == Status.Progress || _locProjectLine2.Status == Status.Lock)
        //                    {
        //                        XPCollection<ProjectLine2Item2> _locProjectLine2Item2s = new XPCollection<ProjectLine2Item2>
        //                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
        //                                                            new BinaryOperator("ProjectHeader", _locProjectHeaderXPO),
        //                                                            new BinaryOperator("ProjectLine2", _locProjectLine2)));

        //                        if (_locProjectLine2Item2s != null && _locProjectLine2Item2s.Count() > 0)
        //                        {
        //                            foreach (ProjectLine2Item2 _locProjectLine2Item2 in _locProjectLine2Item2s)
        //                            {
        //                                if (_locProjectLine2Item2.Status == Status.Progress || _locProjectLine2Item2.Status == Status.Lock)
        //                                {
        //                                    _totPriceProjectLine2Item2 = _totPriceProjectLine2Item2 + _locProjectLine2Item2.TotalUnitAmount;
        //                                }
        //                            }
        //                        }

        //                        XPCollection<ProjectLine2Service> _locProjectLine2Services = new XPCollection<ProjectLine2Service>
        //                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
        //                                                            new BinaryOperator("ProjectHeader", _locProjectHeaderXPO),
        //                                                            new BinaryOperator("ProjectLine", _locProjectLine2)));

        //                        if (_locProjectLine2Services != null && _locProjectLine2Services.Count() > 0)
        //                        {
        //                            foreach (ProjectLine2Service _locProjectLine2Service in _locProjectLine2Services)
        //                            {
        //                                if (_locProjectLine2Service.Status == Status.Progress || _locProjectLine2Service.Status == Status.Lock)
        //                                {
        //                                    _totPriceProjectLine2Service = _totPriceProjectLine2Service + _locProjectLine2Service.TotalUnitAmount;
        //                                }
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //            _locProjectHeaderXPO.OfferAmount = _totPriceProjectLine2Item2 + _totPriceProjectLine2Service;
        //            _locProjectHeaderXPO.Save();
        //            _locProjectHeaderXPO.Session.CommitTransaction();
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        Tracing.Tracer.LogError(" BusinessObject = ProjectHeader " + ex.ToString());
        //    }
        //}

        #endregion CompareAmountMethod

        #region Global Method

        private void SetRemainQuantity(Session _currSession, PrePurchaseOrder _locPrePurchaseOrder)
        {
            try
            {

                #region Remain Qty  with PostedCount != 0
                if (_locPrePurchaseOrder.PostedCount > 0)
                {
                    double _locRmDqty = 0;
                    double _locRmQty = 0;
                    double _locInvLineTotal = 0;
                    double _locRmTQty = 0;
                    bool _locActivationPosting = false;

                    if (_locPrePurchaseOrder.RmDQty > 0 || _locPrePurchaseOrder.RmQty > 0 && _locPrePurchaseOrder.MxTQty != _locPrePurchaseOrder.RmTQty)
                    {
                        _locRmDqty = _locPrePurchaseOrder.RmDQty - _locPrePurchaseOrder.DQty;
                        _locRmQty = _locPrePurchaseOrder.RmQty - _locPrePurchaseOrder.Qty;

                        if (_locPrePurchaseOrder.Item != null && _locPrePurchaseOrder.MxUOM != null && _locPrePurchaseOrder.MxDUOM != null)
                        {
                            ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                    (new GroupOperator(GroupOperatorType.And,
                                                     new BinaryOperator("Item", _locPrePurchaseOrder.Item),
                                                     new BinaryOperator("UOM", _locPrePurchaseOrder.MxUOM),
                                                     new BinaryOperator("DefaultUOM", _locPrePurchaseOrder.MxDUOM),
                                                     new BinaryOperator("Active", true)));
                            if (_locItemUOM != null)
                            {
                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDqty;
                                }
                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDqty;
                                }
                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty + _locRmDqty;
                                }
                            }
                        }
                        else
                        {
                            _locInvLineTotal = _locRmQty + _locRmDqty;
                        }

                        if (_locRmDqty > 0 || _locRmQty > 0)
                        {
                            _locRmTQty = _locPrePurchaseOrder.RmTQty - _locPrePurchaseOrder.TotalQty;
                        }
                        else
                        {
                            _locRmTQty = _locPrePurchaseOrder.RmTQty - _locPrePurchaseOrder.TotalQty;
                            _locActivationPosting = true;
                        }


                        _locPrePurchaseOrder.ActivationPosting = _locActivationPosting;
                        _locPrePurchaseOrder.Select = false;
                        _locPrePurchaseOrder.RmDQty = _locRmDqty;
                        _locPrePurchaseOrder.RmQty = _locRmQty;
                        _locPrePurchaseOrder.RmTQty = _locRmTQty;
                        _locPrePurchaseOrder.PostedCount = _locPrePurchaseOrder.PostedCount + 1;
                        _locPrePurchaseOrder.DQty = 0;
                        _locPrePurchaseOrder.Qty = 0;
                        _locPrePurchaseOrder.TotalQty = 0;
                        _locPrePurchaseOrder.Save();
                        _locPrePurchaseOrder.Session.CommitTransaction();
                    }
                }
                #endregion Remain Qty  with PostedCount != 0

                #region Remain Qty with PostedCount == 0
                else
                {
                    double _locRmDqty = 0;
                    double _locRmQty = 0;
                    double _locInvLineTotal = 0;
                    double _locRmTQty = 0;
                    _locRmDqty = _locPrePurchaseOrder.MxDQty - _locPrePurchaseOrder.DQty;
                    _locRmQty = _locPrePurchaseOrder.MxQty - _locPrePurchaseOrder.Qty;

                    #region Remain > 0
                    if (_locRmDqty > 0 || _locRmQty > 0)
                    {
                        if (_locPrePurchaseOrder.Item != null && _locPrePurchaseOrder.MxUOM != null && _locPrePurchaseOrder.MxDUOM != null)
                        {
                            ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                    (new GroupOperator(GroupOperatorType.And,
                                                     new BinaryOperator("Item", _locPrePurchaseOrder.Item),
                                                     new BinaryOperator("UOM", _locPrePurchaseOrder.MxUOM),
                                                     new BinaryOperator("DefaultUOM", _locPrePurchaseOrder.MxDUOM),
                                                     new BinaryOperator("Active", true)));
                            if (_locItemUOM != null)
                            {
                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDqty;
                                }
                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDqty;
                                }
                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty + _locRmDqty;
                                }
                            }
                        }
                        else
                        {
                            _locInvLineTotal = _locRmQty + _locRmDqty;
                        }

                        if (_locInvLineTotal > 0)
                        {
                            if (_locPrePurchaseOrder.RmTQty > 0)
                            {
                                _locRmTQty = _locPrePurchaseOrder.RmTQty - _locInvLineTotal;
                            }
                            else
                            {
                                _locRmTQty = _locInvLineTotal;
                            }

                        }

                        _locPrePurchaseOrder.RmDQty = _locRmDqty;
                        _locPrePurchaseOrder.RmQty = _locRmQty;
                        _locPrePurchaseOrder.RmTQty = _locRmTQty;
                        _locPrePurchaseOrder.PostedCount = _locPrePurchaseOrder.PostedCount + 1;
                        _locPrePurchaseOrder.Select = false;
                        _locPrePurchaseOrder.DQty = 0;
                        _locPrePurchaseOrder.Qty = 0;
                        _locPrePurchaseOrder.TotalQty = 0;
                        _locPrePurchaseOrder.Save();
                        _locPrePurchaseOrder.Session.CommitTransaction();
                    }
                    #endregion Remain > 0

                    #region Remain <= 0
                    if (_locRmDqty <= 0 && _locRmQty <= 0)
                    {
                        _locPrePurchaseOrder.ActivationPosting = true;
                        _locPrePurchaseOrder.RmDQty = 0;
                        _locPrePurchaseOrder.RmQty = 0;
                        _locPrePurchaseOrder.RmTQty = 0;
                        _locPrePurchaseOrder.PostedCount = _locPrePurchaseOrder.PostedCount + 1;
                        _locPrePurchaseOrder.Select = false;
                        _locPrePurchaseOrder.DQty = 0;
                        _locPrePurchaseOrder.Qty = 0;
                        _locPrePurchaseOrder.TotalQty = 0;
                        _locPrePurchaseOrder.Save();
                        _locPrePurchaseOrder.Session.CommitTransaction();
                    }
                    #endregion Remain < 0
                }
                #endregion Remain Qty with PostedCount == 0

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PrePurchaseOrder " + ex.ToString());
            }
        }

        private double GetTotalQuantity(Session _currSession, PrePurchaseOrder _locPrePurchaseOrder)
        {
            double _result = 0;
            try
            {

                if (_locPrePurchaseOrder.Item != null && _locPrePurchaseOrder.MxUOM != null && _locPrePurchaseOrder.MxDUOM != null)
                {
                    ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                            (new GroupOperator(GroupOperatorType.And,
                                             new BinaryOperator("Item", _locPrePurchaseOrder.Item),
                                             new BinaryOperator("UOM", _locPrePurchaseOrder.MxUOM),
                                             new BinaryOperator("DefaultUOM", _locPrePurchaseOrder.MxDUOM),
                                             new BinaryOperator("Active", true)));
                    if (_locItemUOM != null)
                    {
                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                        {
                            _result = _locPrePurchaseOrder.Qty * _locItemUOM.DefaultConversion + _locPrePurchaseOrder.DQty;
                        }
                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                        {
                            _result = _locPrePurchaseOrder.Qty / _locItemUOM.Conversion + _locPrePurchaseOrder.DQty;
                        }
                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                        {
                            _result = _locPrePurchaseOrder.Qty + _locPrePurchaseOrder.DQty;
                        }
                    }
                }
                else
                {
                    _result = _locPrePurchaseOrder.Qty + _locPrePurchaseOrder.DQty;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PrePurchaseOrder " + ex.ToString());
            }
            return _result;
        }

        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0} !", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method

    }
}
