﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class DebitMemoLineActionController : ViewController
    {
        public DebitMemoLineActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void DebitMemoLineTaxCalculationAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        DebitMemoLine _locDebitMemoLineOS = (DebitMemoLine)_objectSpace.GetObject(obj);

                        if (_locDebitMemoLineOS != null)
                        {
                            if (_locDebitMemoLineOS.TQty != 0 && _locDebitMemoLineOS.UAmount != 0)
                            {
                                if (_locDebitMemoLineOS.Code != null)
                                {
                                    _currObjectId = _locDebitMemoLineOS.Code;

                                    DebitMemoLine _locDebitMemoLineXPO = _currSession.FindObject<DebitMemoLine>
                                                                           (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Code", _currObjectId)));

                                    if (_locDebitMemoLineXPO != null)
                                    {
                                        GetSumTotalTax(_currSession, _locDebitMemoLineXPO);
                                    }
                                }
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = DebitMemoLine" + ex.ToString());
            }
        }

        private void DebitMemoLineDiscCalculationAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        DebitMemoLine _locDebitMemoLineOS = (DebitMemoLine)_objectSpace.GetObject(obj);

                        if (_locDebitMemoLineOS != null)
                        {
                            if (_locDebitMemoLineOS.TQty != 0 && _locDebitMemoLineOS.UAmount != 0)
                            {
                                if (_locDebitMemoLineOS.Code != null)
                                {
                                    _currObjectId = _locDebitMemoLineOS.Code;

                                    DebitMemoLine _locDebitMemoLineXPO = _currSession.FindObject<DebitMemoLine>
                                                                           (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Code", _currObjectId)));

                                    if (_locDebitMemoLineXPO != null)
                                    {
                                        GetSumTotalDisc(_currSession, _locDebitMemoLineXPO);
                                    }
                                }
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = DebitMemoLine" + ex.ToString());
            }
        }

        //=============================================== Code Only ================================================

        #region Get

        private void GetSumTotalTax(Session _currSession, DebitMemoLine _debitMemoLineXPO)
        {
            try
            {
                double _locTxAmount = 0;
                double _locTxValue = 0;

                if (_debitMemoLineXPO != null)
                {
                    XPCollection<TaxLine> _locTaxLines = new XPCollection<TaxLine>(_currSession,
                                                     new BinaryOperator("DebitMemoLine", _debitMemoLineXPO));

                    if (_locTaxLines.Count() > 0)
                    {
                        foreach (TaxLine _locTaxLine in _locTaxLines)
                        {
                            if (_locTaxLine.TxValue > 0 && _locTaxLine.TxAmount > 0)
                            {
                                if (_locTaxLine.TaxNature == TaxNature.Increase)
                                {
                                    _locTxAmount = _locTxAmount + _locTaxLine.TxAmount;
                                    _locTxValue = _locTxValue + _locTaxLine.TxValue;

                                }
                                else if (_locTaxLine.TaxNature == TaxNature.Decrease)
                                {
                                    _locTxAmount = _locTxAmount - _locTaxLine.TxAmount;
                                    _locTxValue = _locTxValue - _locTaxLine.TxValue;
                                }
                            }
                        }
                        _debitMemoLineXPO.TxValue = _locTxValue;
                        _debitMemoLineXPO.TxAmount = _locTxAmount;
                        _debitMemoLineXPO.Save();
                        _debitMemoLineXPO.Session.CommitTransaction();
                    }
                }


            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = DebitMemoLine" + ex.ToString());
            }
        }

        private void GetSumTotalDisc(Session _currSession, DebitMemoLine _debitMemoLineXPO)
        {
            try
            {
                double _locDisc = 0;
                double _locDiscAmount = 0;

                if (_debitMemoLineXPO != null)
                {
                    XPCollection<DiscountLine> _locDiscountLines = new XPCollection<DiscountLine>(_currSession,
                                                               new BinaryOperator("DebitMemoLine", _debitMemoLineXPO));

                    if (_locDiscountLines.Count() > 0)
                    {
                        foreach (DiscountLine _locDiscountLine in _locDiscountLines)
                        {
                            if (_locDiscountLine.Disc > 0 && _locDiscountLine.DiscAmount > 0)
                            {
                                if (_locDiscountLine.Disc > 0 && _locDiscountLine.DiscAmount > 0)
                                {
                                    _locDisc = _locDisc + _locDiscountLine.Disc;
                                    _locDiscAmount = _locDiscAmount + _locDiscountLine.DiscAmount;
                                }
                            }
                        }
                        _debitMemoLineXPO.Disc = _locDisc;
                        _debitMemoLineXPO.DiscAmount = _locDiscAmount;
                        _debitMemoLineXPO.Save();
                        _debitMemoLineXPO.Session.CommitTransaction();
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = SalesReturnLine" + ex.ToString());
            }
        }

        #endregion Get

        #region Global Method

        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0}", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method
    }
}
