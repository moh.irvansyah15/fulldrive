﻿namespace FullDrive.Module.Controllers
{
    partial class ShipmentBookingMonitoringActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ShipmentBookingMonitoringSelectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.ShipmentBookingMonitoringUnselectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // ShipmentBookingMonitoringSelectAction
            // 
            this.ShipmentBookingMonitoringSelectAction.Caption = "Select";
            this.ShipmentBookingMonitoringSelectAction.ConfirmationMessage = null;
            this.ShipmentBookingMonitoringSelectAction.Id = "ShipmentBookingMonitoringSelectActionId";
            this.ShipmentBookingMonitoringSelectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.ShipmentBookingMonitoring);
            this.ShipmentBookingMonitoringSelectAction.ToolTip = null;
            this.ShipmentBookingMonitoringSelectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.ShipmentBookingMonitoringSelectAction_Execute);
            // 
            // ShipmentBookingMonitoringUnselectAction
            // 
            this.ShipmentBookingMonitoringUnselectAction.Caption = "Unselect";
            this.ShipmentBookingMonitoringUnselectAction.ConfirmationMessage = null;
            this.ShipmentBookingMonitoringUnselectAction.Id = "ShipmentBookingMonitoringUnselectActionId";
            this.ShipmentBookingMonitoringUnselectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.ShipmentBookingMonitoring);
            this.ShipmentBookingMonitoringUnselectAction.ToolTip = null;
            this.ShipmentBookingMonitoringUnselectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.ShipmentBookingMonitoringUnselectAction_Execute);
            // 
            // ShipmentBookingMonitoringActionController
            // 
            this.Actions.Add(this.ShipmentBookingMonitoringSelectAction);
            this.Actions.Add(this.ShipmentBookingMonitoringUnselectAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction ShipmentBookingMonitoringSelectAction;
        private DevExpress.ExpressApp.Actions.SimpleAction ShipmentBookingMonitoringUnselectAction;
    }
}
