﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class InventoryTransferOutMonitoringActionController : ViewController
    {
        private ChoiceActionItem _selectionReturnType;

        public InventoryTransferOutMonitoringActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            #region FilterApproval
            InventoryTransferOutMonitoringReturnTypeAction.Items.Clear();
            foreach (object _currReturnType in Enum.GetValues(typeof(CustomProcess.ReturnType)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.ReturnType));
                _selectionReturnType = new ChoiceActionItem(_ed.GetCaption(_currReturnType), _currReturnType);
                InventoryTransferOutMonitoringReturnTypeAction.Items.Add(_selectionReturnType);
            }
            #endregion FilterApproval
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void InventoryTransferOutMonitoringSelectAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        InventoryTransferOutMonitoring _locInventoryTransferOutMonitoringOS = (InventoryTransferOutMonitoring)_objectSpace.GetObject(obj);

                        if (_locInventoryTransferOutMonitoringOS != null)
                        {
                            if (_locInventoryTransferOutMonitoringOS.Code != null)
                            {
                                _currObjectId = _locInventoryTransferOutMonitoringOS.Code;

                                XPCollection<InventoryTransferOutMonitoring> _locInventoryTransferOutMonitorings = new XPCollection<InventoryTransferOutMonitoring>
                                                                                                     (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                      new BinaryOperator("Code", _currObjectId),
                                                                                                      new GroupOperator(GroupOperatorType.Or,
                                                                                                      new BinaryOperator("Status", Status.Progress),
                                                                                                      new BinaryOperator("Status", Status.Open)
                                                                                                      )));

                                if (_locInventoryTransferOutMonitorings != null && _locInventoryTransferOutMonitorings.Count > 0)
                                {
                                    foreach (InventoryTransferOutMonitoring _locInventoryTransferOutMonitoring in _locInventoryTransferOutMonitorings)
                                    {
                                        _locInventoryTransferOutMonitoring.Select = true;
                                        _locInventoryTransferOutMonitoring.Save();
                                        _locInventoryTransferOutMonitoring.Session.CommitTransaction();
                                    }

                                    SuccessMessageShow("InventoryTransferOutMonitoring has been Select");
                                }
                                else
                                {
                                    ErrorMessageShow("Data InventoryTransferOutMonitoring Not Available");
                                }
                            }
                        }
                    }
                    if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                    {
                        _objectSpace.CommitChanges();
                        _objectSpace.Refresh();
                    }
                    if (View is ListView)
                    {
                        _objectSpace.CommitChanges();
                        View.ObjectSpace.Refresh();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = InventoryTransferOutMonitoring" + ex.ToString());
            }
        }

        private void InventoryTransferOutMonitoringUnselectAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        InventoryTransferOutMonitoring _locInventoryTransferOutMonitoringOS = (InventoryTransferOutMonitoring)_objectSpace.GetObject(obj);

                        if (_locInventoryTransferOutMonitoringOS != null)
                        {
                            if (_locInventoryTransferOutMonitoringOS.Code != null)
                            {
                                _currObjectId = _locInventoryTransferOutMonitoringOS.Code;

                                XPCollection<InventoryTransferOutMonitoring> _locInventoryTransferOutMonitorings = new XPCollection<InventoryTransferOutMonitoring>
                                                                                                     (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                      new BinaryOperator("Code", _currObjectId),
                                                                                                      new GroupOperator(GroupOperatorType.Or,
                                                                                                      new BinaryOperator("Status", Status.Progress),
                                                                                                      new BinaryOperator("Status", Status.Open)
                                                                                                      )));

                                if (_locInventoryTransferOutMonitorings != null && _locInventoryTransferOutMonitorings.Count > 0)
                                {
                                    foreach (InventoryTransferOutMonitoring _locInventoryTransferOutMonitoring in _locInventoryTransferOutMonitorings)
                                    {
                                        _locInventoryTransferOutMonitoring.Select = false;
                                        _locInventoryTransferOutMonitoring.Save();
                                        _locInventoryTransferOutMonitoring.Session.CommitTransaction();
                                    }

                                    SuccessMessageShow("InventoryTransferOutMonitoring has been Unselect");
                                }
                                else
                                {
                                    ErrorMessageShow("Data InventoryTransferOutMonitoring Not Available");
                                }
                            }
                        }
                    }
                    if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                    {
                        _objectSpace.CommitChanges();
                        _objectSpace.Refresh();
                    }
                    if (View is ListView)
                    {
                        _objectSpace.CommitChanges();
                        View.ObjectSpace.Refresh();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = InventoryTransferOutMonitoring" + ex.ToString());
            }
        }

        private void InventoryTransferOutMonitoringReturnTypeAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                var User = SecuritySystem.CurrentUserName;
                Session _currentSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currentSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                foreach (Object obj in _objectsToProcess)
                {
                    InventoryTransferOutMonitoring _objInNewObjectSpace = (InventoryTransferOutMonitoring)_objectSpace.GetObject(obj);

                    if (_objInNewObjectSpace != null)
                    {
                        if (_objInNewObjectSpace.Code != null)
                        {
                            _currObjectId = _objInNewObjectSpace.Code;
                        }
                    }

                    if (_currObjectId != null)
                    {
                        InventoryTransferOutMonitoring _locInventoryTransferOutMonitoringXPO = _currentSession.FindObject<InventoryTransferOutMonitoring>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("Code", _currObjectId),
                                                                         new BinaryOperator("Select", true),
                                                                         new BinaryOperator("Status", Status.Open, BinaryOperatorType.NotEqual)));

                        if (_locInventoryTransferOutMonitoringXPO != null)
                        {
                            #region ReplacedFull
                            if ((ReturnType)e.SelectedChoiceActionItem.Data == ReturnType.ReplacedFull)
                            {
                                _locInventoryTransferOutMonitoringXPO.ReturnType = ReturnType.ReplacedFull;
                                _locInventoryTransferOutMonitoringXPO.Save();
                                _locInventoryTransferOutMonitoringXPO.Session.CommitTransaction();
                            }
                            #endregion ReplacedFull

                            #region DebitMemo
                            if ((ReturnType)e.SelectedChoiceActionItem.Data == ReturnType.DebitMemo)
                            {
                                _locInventoryTransferOutMonitoringXPO.ReturnType = ReturnType.DebitMemo;
                                _locInventoryTransferOutMonitoringXPO.Save();
                                _locInventoryTransferOutMonitoringXPO.Session.CommitTransaction();
                            }
                            #endregion DebitMemo

                            #region None
                            if ((ReturnType)e.SelectedChoiceActionItem.Data == ReturnType.None)
                            {
                                _locInventoryTransferOutMonitoringXPO.ReturnType = ReturnType.None;
                                _locInventoryTransferOutMonitoringXPO.Save();
                                _locInventoryTransferOutMonitoringXPO.Session.CommitTransaction();
                            }
                            #endregion None

                        }
                        else
                        {
                            ErrorMessageShow("InventoryTransferOutMonitoring Not Available");
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrder " + ex.ToString());
            }
        }

        #region Global Method

        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0}", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method

        
    }
}
