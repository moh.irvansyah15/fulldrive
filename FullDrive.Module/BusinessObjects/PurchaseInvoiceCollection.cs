﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Editors;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("PurchaseInvoiceCollectionRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class PurchaseInvoiceCollection : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private bool _activationPosting;
        private string _code;
        private string _name;
        private BusinessPartner _buyFromVendor;
        private string _buyFromContact;
        private Country _buyFromCountry;
        private City _buyFromCity;
        private string _buyFromAddress;
        private string _taxNo;
        private TermOfPayment _top;
        private double _maxPay;
        private bool _dp_posting;
        private double _dp_amount;
        private double _pay;
        private double _plan;
        private double _outstanding;
        private double _cm_amount;
        private int _postedCount;
        private Status _status;
        private DateTime _statusDate;
        private PurchaseOrder _purchaseOrder;
        private ProjectHeader _projectHeader;
        private Company _company;
        private string _signCode;  
        private GlobalFunction _globFunc;   

        public PurchaseInvoiceCollection(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                DateTime now = DateTime.Now;
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.PurchaseInvoiceCollection);
                Status = Status.Open;
                StatusDate = now;
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [Appearance("PurchaseInvoiceCollectionCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("PurchaseInvoiceCollectionNameClose", Enabled = false)]
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        #region BuyFromVendor

        [DataSourceCriteria("Active = true")]
        [Appearance("PurchaseInvoiceCollectionBuyFromVendorClose", Enabled = false)]
        public BusinessPartner BuyFromVendor
        {
            get { return _buyFromVendor; }
            set {
                SetPropertyValue("BuyFromVendor", ref _buyFromVendor, value);
                if (!IsLoading)
                {
                    if (this._buyFromVendor != null)
                    {
                        this.BuyFromContact = this._buyFromVendor.Contact;
                        this.BuyFromCountry = this._buyFromVendor.Country;
                        this.BuyFromCity = this._buyFromVendor.City;
                        this.TaxNo = this._buyFromVendor.TaxNo;
                        this.TOP = this._buyFromVendor.TOP;
                        this.BuyFromAddress = this._buyFromVendor.Address;
                    }
                }
            }
        }

        [Appearance("PurchaseInvoiceCollectionBuyFromContactClose", Enabled = false)]
        public string BuyFromContact
        {
            get { return _buyFromContact; }
            set { SetPropertyValue("BuyFromContact", ref _buyFromContact, value); }
        }

        [DataSourceCriteria("Active = true")]
        [Appearance("PurchaseInvoiceCollectionBuyFromCountryClose", Enabled = false)]
        public Country BuyFromCountry
        {
            get { return _buyFromCountry; }
            set { SetPropertyValue("BuyFromCountry", ref _buyFromCountry, value); }
        }

        [DataSourceCriteria("Active = true")]
        [Appearance("PurchaseInvoiceCollectionBuyFromCityClose", Enabled = false)]
        public City BuyFromCity
        {
            get { return _buyFromCity; }
            set { SetPropertyValue("BuyFromCity", ref _buyFromCity, value); }
        }

        [Appearance("PurchaseInvoiceCollectionBuyFromAddressClose", Enabled = false)]
        [Size(512)]
        public string BuyFromAddress
        {
            get { return _buyFromAddress; }
            set { SetPropertyValue("BuyFromAddress", ref _buyFromAddress, value); }
        }

        #endregion BuyFromVendor

        [Appearance("PurchaseInvoiceCollectionTaxNoClose", Enabled = false)]
        public string TaxNo
        {
            get { return _taxNo; }
            set { SetPropertyValue("TaxNo", ref _taxNo, value); }
        }

        [Appearance("PurchaseInvoiceCollectionTermOfPaymentClose", Enabled = false)]
        public TermOfPayment TOP
        {
            get { return _top; }
            set { SetPropertyValue("TOP", ref _top, value); }
        }

        #region Amount

        [Appearance("PurchaseInvoiceCollectionMaxPayClose", Enabled = false)]
        public double MaxPay
        {
            get { return _maxPay; }
            set { SetPropertyValue("MaxPay", ref _maxPay, value); }
        }

        [ImmediatePostData()]
        [Appearance("PurchaseInvoiceCollectionPayClose", Enabled = false)]
        public double Pay
        {
            get { return _pay; }
            set
            {
                SetPropertyValue("Pay", ref _pay, value);
                if (!IsLoading)
                {
                    if (this.PostedCount > 0)
                    {
                        if (this.Outstanding > 0)
                        {
                            if (this._pay > 0 && this._pay > this.Outstanding)
                            {
                                this._pay = this.Outstanding;
                            }
                        }
                    }

                    if (this.PostedCount == 0)
                    {
                        if (this._pay > 0 && this._pay > this.MaxPay)
                        {
                            this._pay = this.MaxPay;
                        }
                    }
                }
            }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        public bool DP_Posting
        {
            get { return _dp_posting; }
            set { SetPropertyValue("DP_Posting", ref _dp_posting, value); }
        }

        [ImmediatePostData()]
        [Appearance("PurchaseInvoiceCollectionDP_AmountClose", Enabled = false)]
        public double DP_Amount
        {
            get { return _dp_amount; }
            set { SetPropertyValue("DP_Amount", ref _dp_amount, value); }
        }

        [Appearance("PurchaseInvoiceCollectionPlanClose", Enabled = false)]
        public double Plan
        {
            get { return _plan; }
            set { SetPropertyValue("Plan", ref _plan, value); }
        }

        [Appearance("PurchaseInvoiceCollectionOutstandingClose", Enabled = false)]
        public double Outstanding
        {
            get { return _outstanding; }
            set { SetPropertyValue("Outstanding", ref _outstanding, value); }
        }

        //[Browsable(false)]
        [ImmediatePostData()]
        [Appearance("PurchaseInvoiceCollectionCM_AmountClose", Enabled = false)]
        public double CM_Amount
        {
            get { return _cm_amount; }
            set { SetPropertyValue("CM_Amount", ref _cm_amount, value); }
        }

        #endregion Amount

        [Appearance("PurchaseInvoiceCollectionSignCodeClose", Enabled = false)]
        public string SignCode
        {
            get { return _signCode; }
            set { SetPropertyValue("SignCode", ref _signCode, value); }
        }

        [Appearance("PurchaseInvoiceCollectionPostedCountClose", Enabled = false)]
        public int PostedCount
        {
            get { return _postedCount; }
            set { SetPropertyValue("PostedCount", ref _postedCount, value); }
        }

        [Appearance("PurchaseInvoiceCollectionStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("PurchaseInvoiceCollectionStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("PurchaseInvoiceCollectionPurchaseOrderClose", Enabled = false)]
        public PurchaseOrder PurchaseOrder
        {
            get { return _purchaseOrder; }
            set { SetPropertyValue("PurchaseOrder", ref _purchaseOrder, value); }
        }

        [Appearance("PurchaseInvoiceCollectionProjectHeaderClose", Enabled = false)]
        public ProjectHeader ProjectHeader
        {
            get { return _projectHeader; }
            set { SetPropertyValue("ProjectHeader", ref _projectHeader, value); }
        }

        [Browsable(false)]
        [DataSourceCriteria("Active = true")]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Association("PurchaseInvoiceCollection-PurchaseInvoiceCollectionLines")]
        public XPCollection<PurchaseInvoiceCollectionLine> PurchaseInvoiceCollectionLines
        {
            get { return GetCollection<PurchaseInvoiceCollectionLine>("PurchaseInvoiceCollectionLines"); }
        }

        [Association("PurchaseInvoiceCollection-PaymentOutPlans")]
        public XPCollection<PaymentOutPlan> PaymentOutPlans
        {
            get { return GetCollection<PaymentOutPlan>("PaymentOutPlans"); }
        }

        //[Association("PurchaseInvoiceCollection-CreditMemoCollections")]
        //public XPCollection<CreditMemoCollection> CreditMemoCollections
        //{
        //    get { return GetCollection<CreditMemoCollection>("CreditMemoCollections"); }
        //}

        #endregion Field
    }
}