﻿        #region Default
using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using DevExpress.ExpressApp.Xpo;
using DevExpress.ExpressApp.ConditionalAppearance;
using System.Collections;
using FullDrive.Module.CustomProcess;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    [NavigationItem("Payroll")]
    [RuleCombinationOfPropertiesIsUnique("SalaryComponentRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class SalaryComponent : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private string _code;
        private string _name;
        private PPH21Coloumn _pph21Mapping;
        private SalaryType _salaryType;
        private GlobalFunction _globFunc;
        public SalaryComponent(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this._code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.SalaryComponent);
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }
        #endregion

        #region Field
        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [ImmediatePostData()]
        public string Name
        {
            get {
                    if (SalaryType != null)
                {
                    if (PPH21Mapping != null)
                    {
                        UpdateName();
                    }
                }
                return _name;
               }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        [ImmediatePostData()]
        public SalaryType SalaryType
        {
            get { return _salaryType; }
            set {
                    SetPropertyValue("SalaryType", ref _salaryType, value);
                    UpdateName();
                }
        }

        [ImmediatePostData()]
        public PPH21Coloumn PPH21Mapping
        {
            get { return _pph21Mapping; }
            set {
                    SetPropertyValue("PPH21Mapping", ref _pph21Mapping, value);
                    UpdateName();
                }
        }
        #endregion

        //private string _PersistentProperty;
        //[XafDisplayName("My display name"), ToolTip("My hint message")]
        //[ModelDefault("EditMask", "(000)-00"), Index(0), VisibleInListView(false)]
        //[Persistent("DatabaseColumnName"), RuleRequiredField(DefaultContexts.Save)]
        //public string PersistentProperty {
        //    get { return _PersistentProperty; }
        //    set { SetPropertyValue("PersistentProperty", ref _PersistentProperty, value); }
        //}

        //[Action(Caption = "My UI Action", ConfirmationMessage = "Are you sure?", ImageName = "Attention", AutoCommit = true)]
        //public void ActionMethod() {
        //    // Trigger a custom business logic for the current record in the UI (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112619.aspx).
        //    this.PersistentProperty = "Paid";
        //}

        #region UpdateName
        private void UpdateName()
        {
            try
            {
                Name = string.Format("Salary Component - {0} [ {1} ] ", SalaryType, PPH21Mapping);
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = SalaryComponent" + ex.ToString());
            }
        }
        #endregion UpdateName

        //
    }
}