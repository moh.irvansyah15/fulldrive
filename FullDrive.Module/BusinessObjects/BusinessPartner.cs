﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using FullDrive.Module.CustomProcess;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    [NavigationItem("Business Partner")]
    [RuleCombinationOfPropertiesIsUnique("BusinessPartnerRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class BusinessPartner : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private string _code;
        private string _name;
        private Country _country;
        private City _city;
        private string _address;
        private string _pic;
        private Employee _internalPIC;
        private string _contact;
        private string _email;
        private string _taxNo;
        private TermOfPayment _top;
        private IndustrialType _industrialType;
        private BusinessPartnerGroup _businessPartnerGroup;
        private BusinessPartnerAccountGroup _businessPartnerAccountGroup;
        private BusinessPartnerType _businessPartnerType;
        private string _remark;
        private bool _active;
        private GlobalFunction _globFunc;

        public BusinessPartner(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if(!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.BusinessPartner);
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        [DataSourceCriteria("Active = true")]
        public Country Country
        {
            get { return _country; }
            set { SetPropertyValue("Country", ref _country, value); }
        }

        [DataSourceCriteria("Active = true")]
        public City City
        {
            get { return _city; }
            set { SetPropertyValue("City", ref _city, value); }
        }

        [Size(512)]
        public string Address
        {
            get { return _address; }
            set { SetPropertyValue("Address", ref _address, value); }
        }

        public string PIC
        {
            get { return _pic; }
            set { SetPropertyValue("PIC", ref _pic, value); }
        }

        public Employee InternalPIC
        {
            get { return _internalPIC; }
            set { SetPropertyValue("InternalPIC", ref _internalPIC, value); }
        }

        public string Contact
        {
            get { return _contact; }
            set { SetPropertyValue("Contact", ref _contact, value); }
        }

        public string Email
        {
            get { return _email; }
            set { SetPropertyValue("Email", ref _email, value); }
        }

        public string TaxNo
        {
            get { return _taxNo; }
            set { SetPropertyValue("TaxNo", ref _taxNo, value); }
        }

        [Association("IndustrialType-BusinessPartners")]
        [DataSourceCriteria("Active = true")]
        public IndustrialType IndustrialType
        {
            get { return _industrialType; }
            set { SetPropertyValue("IndustrialType", ref _industrialType, value); }
        }

        [Association("TOP-BusinesPartners")]
        [DataSourceCriteria("Active = true")]
        public TermOfPayment TOP
        {
            get { return _top; }
            set { SetPropertyValue("TOP", ref _top, value); }
        }

        [Association("BusinessPartnerGroup-BusinessPartners")]
        public BusinessPartnerGroup BusinessPartnerGroup
        {
            get { return _businessPartnerGroup; }
            set { SetPropertyValue("BusinessPartnerGroup", ref _businessPartnerGroup, value); }
        }

        [Association("BusinessPartnerAccountGroup-BusinessPartners")]
        public BusinessPartnerAccountGroup BusinessPartnerAccountGroup
        {
            get { return _businessPartnerAccountGroup; }
            set { SetPropertyValue("BusinessPartnerAccountGroup", ref _businessPartnerAccountGroup, value); }
        }

        public BusinessPartnerType BusinessPartnerType
        {
            get { return _businessPartnerType; }
            set { SetPropertyValue("BusinessPartnerType", ref _businessPartnerType, value); }
        }

        [Size(512)]
        public string Remark
        {
            get { return _remark; }
            set { SetPropertyValue("Remark", ref _remark, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue("Active", ref _active, value); }
        }

        [Association("BusinessPartner-BankAccounts")]
        public XPCollection<BankAccount> BankAccounts
        {
            get { return GetCollection<BankAccount>("BankAccounts"); }
        }

        [Association("BusinessPartner-Addresses")]
        public XPCollection<Address> Addresses
        {
            get { return GetCollection<Address>("Addresses"); }
        }
        //private string _PersistentProperty;
        //[XafDisplayName("My display name"), ToolTip("My hint message")]
        //[ModelDefault("EditMask", "(000)-00"), Index(0), VisibleInListView(false)]
        //[Persistent("DatabaseColumnName"), RuleRequiredField(DefaultContexts.Save)]
        //public string PersistentProperty {
        //    get { return _PersistentProperty; }
        //    set { SetPropertyValue("PersistentProperty", ref _PersistentProperty, value); }
        //}

        //[Action(Caption = "My UI Action", ConfirmationMessage = "Are you sure?", ImageName = "Attention", AutoCommit = true)]
        //public void ActionMethod() {
        //    // Trigger a custom business logic for the current record in the UI (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112619.aspx).
        //    this.PersistentProperty = "Paid";
        //}
    }
}