﻿#region Default

using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

#endregion Default

using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    #region Default

    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("TaxLineRuleUnique", DefaultContexts.Save, "Code")]

    #endregion Default

    // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
    public class TaxLine : FullDriveSysBaseObject
    {
        #region Default

        private int _no;
        private string _code;
        private string _name;
        private TaxType _taxType;
        private TaxGroup _taxGroup;
        private TaxGroupType _taxGroupType;
        private TaxAccountGroup _taxAccountGroup;
        private double _txValue;
        private Tax _tax;
        private double _uAmount;
        private double _tUAmount;
        private double _txAmount;
        private TaxNature _taxNature;
        private TaxMode _taxMode;
        private string _description;
        private PurchaseOrderLine _purchaseOrderLine;
        private PurchaseInvoiceLine _purchaseInvoiceLine;
        private PurchaseReturnLine _purchaseReturnLine;
        private CreditMemoLine _creditMemoLine;
        private SalesOrderLine _salesOrderLine;
        private SalesQuotationLine _salesQuotationLine;
        private SalesInvoiceLine _salesInvoiceLine;
        private SalesReturnLine _salesReturnLine;
        private DebitMemoLine _debitMemoLine;
        private bool _active;
        private bool _default;
        private GlobalFunction _globFunc;

        public TaxLine(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.TaxLine);
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        //No
        protected override void OnSaving()
        {
            base.OnSaving();
            if (!IsLoading && IsSaving)
            {
                UpdateNo();
            }
        }
        protected override void OnDeleting()
        {
            base.OnDeleting();
            if (!IsLoading)
            {
                RecoveryDeleteNo();
            }
        }

        #endregion Default

        #region Field

        [Browsable(false)]
        [Appearance("TaxLineNoEnabled", Enabled = false)]
        public int No
        {
            get { return _no; }
            set { SetPropertyValue("No", ref _no, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("TaxLineCodeEnabled", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        [Association("Tax-TaxLines")]
        public Tax Tax
        {
            get { return _tax; }
            set
            {
                SetPropertyValue("Tax", ref _tax, value);
                if (!IsLoading)
                {
                    if (this._tax != null)
                    {
                        this.Name = GetName();
                        this.TaxType = this._tax.TaxType;
                        this.TaxGroup = this._tax.TaxGroup;
                        this.TaxGroupType = this._tax.TaxGroupType;
                        this.TaxNature = this._tax.TaxNature;
                        this.TaxMode = this._tax.TaxMode;
                        this.TaxAccountGroup = this._tax.TaxAccountGroup;
                        this.TxValue = this._tax.Value;
                        this.Active = this._tax.Active;
                        this.Default = this._tax.Default;
                    }
                    SetTaxAmount();
                }
            }
        }

        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        [Appearance("TaxLineTaxTypeEnabled", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public TaxType TaxType
        {
            get { return _taxType; }
            set { SetPropertyValue("TaxType", ref _taxType, value); }
        }

        [Appearance("TaxLineTaxGroupEnabled", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public TaxGroup TaxGroup
        {
            get { return _taxGroup; }
            set { SetPropertyValue("TaxGroup", ref _taxGroup, value); }
        }

        public TaxGroupType TaxGroupType
        {
            get { return _taxGroupType; }
            set { SetPropertyValue("TaxGroupType", ref _taxGroupType, value); }
        }

        [Appearance("TaxLineTaxAccountGroupEnabled", Enabled = false)]
        [Association("TaxAccountGroup-TaxLines")]
        [DataSourceCriteria("Active = true")]
        public TaxAccountGroup TaxAccountGroup
        {
            get { return _taxAccountGroup; }
            set { SetPropertyValue("TaxAccountGroup", ref _taxAccountGroup, value); }
        }

        [Appearance("TaxLineTaxNatureEnabled", Enabled = false)]
        public TaxNature TaxNature
        {
            get { return _taxNature; }
            set { SetPropertyValue("TaxNature", ref _taxNature, value); }
        }

        [Appearance("TaxLineTaxModeEnabled", Enabled = false)]
        public TaxMode TaxMode
        {
            get { return _taxMode; }
            set { SetPropertyValue("TaxMode", ref _taxMode, value); }
        }

        [Browsable(false)]
        [Appearance("TaxLineActiveEnabled", Enabled = false)]
        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue("Active", ref _active, value); }
        }

        [Browsable(false)]
        [Appearance("TaxLineDefaultEnabled", Enabled = false)]
        public bool Default
        {
            get { return _default; }
            set { SetPropertyValue("Default", ref _default, value); }
        }

        [Appearance("TaxLineValueEnabled", Enabled = false)]
        public double TxValue
        {
            get { return _txValue; }
            set { SetPropertyValue("TxValue", ref _txValue, value); }
        }

        [ImmediatePostData()]
        public double TTxValue
        {
            get
            {
                if (this._txValue > 0)
                {
                    return GetSumTotalTxValue();
                }
                else
                {
                    return 0;
                }
            }
        }

        [Size(512)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        [Browsable(false)]
        [Appearance("TaxLineUPriceEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double UAmount
        {
            get { return _uAmount; }
            set
            {
                SetPropertyValue("UAmount", ref _uAmount, value);
                if (!IsLoading)
                {
                    _globFunc = new GlobalFunction();
                    if (this._uAmount > 0)
                    {
                        if (_globFunc.GetRoundingList(Session, ObjectList.TaxLine, FieldName.UAmount) == true)
                        {
                            this._uAmount = _globFunc.GetRoundUp(Session, this._uAmount, ObjectList.TaxLine, FieldName.UAmount);
                        }
                    }
                }
            }
        }

        [Appearance("TaxLineLineTUAmountEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double TUAmount
        {
            get { return _tUAmount; }
            set
            {
                SetPropertyValue("TUAmount", ref _tUAmount, value);
                if (!IsLoading)
                {
                    _globFunc = new GlobalFunction();
                    if (this._tUAmount > 0)
                    {
                        if (_globFunc.GetRoundingList(Session, ObjectList.TaxLine, FieldName.TUAmount) == true)
                        {
                            this._tUAmount = _globFunc.GetRoundUp(Session, this._tUAmount, ObjectList.TaxLine, FieldName.TUAmount);
                        }
                    }

                    SetTaxAmount();
                }
            }
        }

        [Appearance("TaxLineTxAmountEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double TxAmount
        {
            get { return _txAmount; }
            set
            {
                SetPropertyValue("TxAmount", ref _txAmount, value);
                _globFunc = new GlobalFunction();
                if (this._txAmount > 0)
                {
                    if (_globFunc.GetRoundingList(Session, ObjectList.TaxLine, FieldName.TxAmount) == true)
                    {
                        this._txAmount = _globFunc.GetRoundUp(Session, this._txAmount, ObjectList.TaxLine, FieldName.TxAmount);
                    }
                }
            }
        }

        [Appearance("TaxLineTTxAmountEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double TTxAmount
        {
            get
            {
                if (this._txValue > 0 && this._tUAmount > 0)
                {
                    return GetSumTotalTaxAmount();
                }
                else
                {
                    return 0;
                }
            }
        }

        [ImmediatePostData()]
        [Appearance("TaxLinePurchaseOrderLineEnabled", Enabled = false)]
        [Association("PurchaseOrderLine-TaxLines")]
        public PurchaseOrderLine PurchaseOrderLine
        {
            get { return _purchaseOrderLine; }
            set
            {
                SetPropertyValue("PurchaseOrderLine", ref _purchaseOrderLine, value);
                if (!IsLoading)
                {
                    if (this._purchaseOrderLine != null)
                    {
                        this.UAmount = this._purchaseOrderLine.UAmount;
                        this.TUAmount = this._purchaseOrderLine.TUAmount;
                    }
                }
            }
        }

        [ImmediatePostData()]
        [Appearance("TaxLinePurchaseInvoiceLineEnabled", Enabled = false)]
        [Association("PurchaseInvoiceLine-TaxLines")]
        public PurchaseInvoiceLine PurchaseInvoiceLine
        {
            get { return _purchaseInvoiceLine; }
            set
            {
                SetPropertyValue("PurchaseInvoiceLine", ref _purchaseInvoiceLine, value);
                if (!IsLoading)
                {
                    if (this._purchaseOrderLine != null)
                    {
                        this.UAmount = this.PurchaseInvoiceLine.UAmount;
                        this.TUAmount = this.PurchaseInvoiceLine.TUAmount;
                    }
                }
            }
        }

        [ImmediatePostData()]
        [Appearance("TaxLinePurchasereturnLineEnabled", Enabled = false)]
        [Association("PurchaseReturnLine-TaxLines")]
        public PurchaseReturnLine PurchaseReturnLine
        {
            get { return _purchaseReturnLine; }
            set
            {
                SetPropertyValue("PurchaseReturnLine", ref _purchaseReturnLine, value);
                if (!IsLoading)
                {
                    if (this._purchaseReturnLine != null)
                    {
                        this.UAmount = this.PurchaseReturnLine.UAmount;
                        this.TUAmount = this.PurchaseReturnLine.TUAmount;
                    }
                }
            }
        }

        [ImmediatePostData()]
        [Appearance("TaxLineCreditMemoLineEnabled", Enabled = false)]
        [Association("CreditMemoLine-TaxLines")]
        public CreditMemoLine CreditMemoLine
        {
            get { return _creditMemoLine; }
            set
            {
                SetPropertyValue("CreditMemoLine", ref _creditMemoLine, value);
                if (!IsLoading)
                {
                    if (this._creditMemoLine != null)
                    {
                        this.UAmount = this._creditMemoLine.UAmount;
                        this.TUAmount = this._creditMemoLine.TUAmount;
                    }
                }
            }
        }

        [ImmediatePostData()]
        [Appearance("TaxLineSalesOrderLineEnabled", Enabled = false)]
        [Association("SalesOrderLine-TaxLines")]
        public SalesOrderLine SalesOrderLine
        {
            get { return _salesOrderLine; }
            set
            {
                SetPropertyValue("SalesOrderLine", ref _salesOrderLine, value);
                if (!IsLoading)
                {
                    if (this._salesOrderLine != null)
                    {
                        this.UAmount = this._salesOrderLine.UAmount;
                        this.TUAmount = this._salesOrderLine.TUAmount;
                    }
                }
            }
        }

        [ImmediatePostData()]
        [Appearance("TaxLineSalesQuotationLineEnabled", Enabled = false)]
        [Association("SalesQuotationLine-TaxLines")]
        public SalesQuotationLine SalesQuotationLine
        {
            get { return _salesQuotationLine; }
            set
            {
                SetPropertyValue("SalesQuotationLine", ref _salesQuotationLine, value);
                if (!IsLoading)
                {
                    if (this._salesQuotationLine != null)
                    {
                        this.UAmount = this._salesQuotationLine.UAmount;
                        this.TUAmount = this._salesQuotationLine.TUAmount;
                    }
                }
            }
        }

        [ImmediatePostData()]
        [Appearance("TaxLineSalesInvoiceLineEnabled", Enabled = false)]
        [Association("SalesInvoiceLine-TaxLines")]
        public SalesInvoiceLine SalesInvoiceLine
        {
            get { return _salesInvoiceLine; }
            set {
                SetPropertyValue("SalesInvoiceLine", ref _salesInvoiceLine, value);
                if (!IsLoading)
                {
                    if (this._salesInvoiceLine != null)
                    {
                        this.UAmount = this._salesInvoiceLine.UAmount;
                        this.TUAmount = this._salesInvoiceLine.TUAmount;
                    }
                }
            }
        }

        [ImmediatePostData()]
        [Appearance("TaxLineSalesReturnLineEnabled", Enabled = false)]
        [Association("SalesReturnLine-TaxLines")]
        public SalesReturnLine SalesReturnLine
        {
            get { return _salesReturnLine; }
            set {
                SetPropertyValue("SalesReturnLine", ref _salesReturnLine, value);
                if (!IsLoading)
                {
                    if (this._salesReturnLine != null)
                    {
                        this.UAmount = this._salesReturnLine.UAmount;
                        this.TUAmount = this._salesReturnLine.TUAmount;
                    }
                }
            }
        }

        [ImmediatePostData()]
        [Appearance("TaxLineDebitMemoLineEnabled", Enabled = false)]
        [Association("DebitMemoLine-TaxLines")]
        public DebitMemoLine DebitMemoLine
        {
            get { return _debitMemoLine; }  
            set {
                SetPropertyValue("DebitMemoLine", ref _debitMemoLine, value);
                if (!IsLoading)
                {
                    if (this._debitMemoLine != null)
                    {
                        this.UAmount = this._debitMemoLine.UAmount;
                        this.TUAmount = this._debitMemoLine.TUAmount;
                    }
                }
            }
        }
        #endregion Field

        //===== Code Only =====

        #region No

        public void UpdateNo()
        {
            try
            {
                if (!IsLoading && Session.IsNewObject(this))
                {
                    if (this.Tax != null)
                    {
                        object _makRecord = Session.Evaluate<TaxLine>(CriteriaOperator.Parse("Max(No)"), CriteriaOperator.Parse("Tax=?", this.Tax));
                        this.No = Convert.ToInt32(_makRecord) + 1;
                        this.Save();
                        RecoveryUpdateNo();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TaxLine " + ex.ToString());
            }
        }

        public void RecoveryUpdateNo()
        {
            try
            {
                if (this.Tax != null)
                {
                    Tax _numHeader = Session.FindObject<Tax>
                                                (new BinaryOperator("Code", this.Tax.Code));

                    XPCollection<TaxLine> _numLines = new XPCollection<TaxLine>
                                                (Session, new BinaryOperator("Tax", _numHeader),
                                                new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i1 = 0;
                        foreach (TaxLine _numLine in _numLines)
                        {
                            i1 += 1;
                            _numLine.No = i1;
                            _numLine.Save();
                        }
                        i1 = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TaxLine " + ex.ToString());
            }
        }

        public void RecoveryDeleteNo()
        {
            try
            {
                if (this.Tax != null)
                {
                    Tax _numHeader = Session.FindObject<Tax>
                                                (new BinaryOperator("Code", this.Tax.Code));

                    XPCollection<TaxLine> _numLines = new XPCollection<TaxLine>
                                                (Session, new GroupOperator(GroupOperatorType.And,
                                                 new BinaryOperator("This", this, BinaryOperatorType.NotEqual),
                                                 new BinaryOperator("Tax", _numHeader)),
                                                 new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i = 0;
                        foreach (TaxLine _numLine in _numLines)
                        {
                            i += 1;
                            _numLine.No = i;
                            _numLine.Save();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TaxLine " + ex.ToString());
            }
        }

        #endregion No

        #region Get

        public string GetName()
        {
            string _result = "";
            try
            {
                if (this._tax != null)
                {
                    string _result1 = null;
                    if (this._tax.Name != null)
                    {
                        _result1 = this._tax.Name;
                    }
                    _result = _result1 + " ";
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TaxLine " + ex.ToString());
            }
            return _result;
        }

        private double GetSumTotalTxValue()
        {
            double _result = 0;
            try
            {
                double _locTotalTxValue = 0;
                double _locTotalTxValue2 = 0;
                double _locTotalTxValue3 = 0;
                double _locTotalTxValue4 = 0;
                double _locTotalTxValue5 = 0;
                double _locTotalTxValue6 = 0;
                double _locTotalTxValue7 = 0;
                if (!IsLoading)
                {
                    XPCollection<TaxLine> _locTaxLines = new XPCollection<TaxLine>(Session,
                                                         new BinaryOperator("PurchaseOrderLine", this.PurchaseOrderLine));

                    XPCollection<TaxLine> _locTaxLine2s = new XPCollection<TaxLine>(Session,
                                                         new BinaryOperator("PurchaseInvoiceLine", this.PurchaseInvoiceLine));

                    XPCollection<TaxLine> _locTaxLine3s = new XPCollection<TaxLine>(Session,
                                                         new BinaryOperator("SalesOrderLine", this.SalesOrderLine));

                    XPCollection<TaxLine> _locTaxLine4s = new XPCollection<TaxLine>(Session,
                                                         new BinaryOperator("SalesQuotationLine", this.SalesQuotationLine));

                    XPCollection<TaxLine> _locTaxLine5s = new XPCollection<TaxLine>(Session,
                                                         new BinaryOperator("SalesInvoiceLine", this.SalesInvoiceLine));

                    XPCollection<TaxLine> _locTaxLine6s = new XPCollection<TaxLine>(Session,
                                                         new BinaryOperator("SalesReturnLine", this.SalesReturnLine));

                    XPCollection<TaxLine> _locTaxLine7s = new XPCollection<TaxLine>(Session,
                                                         new BinaryOperator("DebitMemoLine", this.DebitMemoLine));

                    if (_locTaxLines != null && _locTaxLines.Count() > 0)
                    {
                        foreach (TaxLine _locTaxLine in _locTaxLines)
                        {
                            if (_locTaxLine.TaxNature == TaxNature.Increase)
                            {
                                _locTotalTxValue = _locTotalTxValue + _locTaxLine.TxValue;
                            }
                            else if (_locTaxLine.TaxNature == FullDrive.Module.CustomProcess.TaxNature.Decrease)
                            {
                                _locTotalTxValue = _locTotalTxValue - _locTaxLine.TxValue;
                            }

                        }
                        _result = _locTotalTxValue;
                    }
                    else if (_locTaxLine2s != null && _locTaxLine2s.Count() > 0)
                    {
                        foreach (TaxLine _locTaxLine2 in _locTaxLine2s)
                        {
                            if (_locTaxLine2.TaxNature == TaxNature.Increase)
                            {
                                _locTotalTxValue2 = _locTotalTxValue2 + _locTaxLine2.TxValue;
                            }
                            else if (_locTaxLine2.TaxNature == TaxNature.Decrease)
                            {
                                _locTotalTxValue2 = _locTotalTxValue2 - _locTaxLine2.TxValue;
                            }

                        }
                        _result = _locTotalTxValue2;
                    }else if(_locTaxLine3s != null && _locTaxLine3s.Count() > 0)
                    {
                        foreach(TaxLine _locTaxLine3 in _locTaxLine3s)
                        {
                            if (_locTaxLine3.TaxNature == TaxNature.Increase)
                            {
                                _locTotalTxValue3 = _locTotalTxValue3 + _locTaxLine3.TxValue;
                            }
                            else if (_locTaxLine3.TaxNature == TaxNature.Decrease)
                            {
                                _locTotalTxValue3 = _locTotalTxValue3 - _locTaxLine3.TxValue;
                            }
                        }
                        _result = _locTotalTxValue3;
                    }
                    else if(_locTaxLine4s != null && _locTaxLine4s.Count() > 0)
                    {
                        foreach (TaxLine _locTaxLine4 in _locTaxLine4s)
                        {
                            if (_locTaxLine4.TaxNature == TaxNature.Increase)
                            {
                                _locTotalTxValue4 = _locTotalTxValue4 + _locTaxLine4.TxValue;
                            }
                            else if (_locTaxLine4.TaxNature == TaxNature.Decrease)
                            {
                                _locTotalTxValue4 = _locTotalTxValue4 - _locTaxLine4.TxValue;
                            }
                        }
                        _result = _locTotalTxValue4;
                    }
                    else if(_locTaxLine5s != null && _locTaxLine5s.Count() > 0)
                    {
                        foreach (TaxLine _locTaxLine5 in _locTaxLine5s)
                        {
                            if (_locTaxLine5.TaxNature == TaxNature.Increase)
                            {
                                _locTotalTxValue5 = _locTotalTxValue5 + _locTaxLine5.TxValue;
                            }
                            else if (_locTaxLine5.TaxNature == TaxNature.Decrease)
                            {
                                _locTotalTxValue5 = _locTotalTxValue5 - _locTaxLine5.TxValue;
                            }
                        }
                        _result = _locTotalTxValue5;
                    }
                    else if(_locTaxLine6s != null && _locTaxLine6s.Count() > 0)
                    {
                        foreach (TaxLine _locTaxLine6 in _locTaxLine6s)
                        {
                            if (_locTaxLine6.TaxNature == TaxNature.Increase)
                            {
                                _locTotalTxValue6 = _locTotalTxValue6 + _locTaxLine6.TxValue;
                            }
                            else if (_locTaxLine6.TaxNature == TaxNature.Decrease)
                            {
                                _locTotalTxValue6 = _locTotalTxValue6 - _locTaxLine6.TxValue;
                            }
                        }
                        _result = _locTotalTxValue6;
                    }
                    else if(_locTaxLine7s != null && _locTaxLine7s.Count() > 0)
                    {
                        foreach (TaxLine _locTaxLine7 in _locTaxLine7s)
                        {
                            if (_locTaxLine7.TaxNature == TaxNature.Increase)
                            {
                                _locTotalTxValue7 = _locTotalTxValue7 + _locTaxLine7.TxValue;
                            }
                            else if (_locTaxLine7.TaxNature == TaxNature.Decrease)
                            {
                                _locTotalTxValue7 = _locTotalTxValue7 - _locTaxLine7.TxValue;
                            }
                        }
                        _result = _locTotalTxValue7;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = TaxLine" + ex.ToString());
            }
            return _result;
        }

        private double GetSumTotalTaxAmount()
        {
            double _result = 0;
            try
            {
                double _locTotalTxAmount = 0;
                double _locTotalTxAmount2 = 0;
                double _locTotalTxAmount3 = 0;
                double _locTotalTxAmount4 = 0;
                double _locTotalTxAmount5 = 0;
                double _locTotalTxAmount6 = 0;
                double _locTotalTxAmount7 = 0;
                if (!IsLoading)
                {
                    XPCollection<TaxLine> _locTaxLines = new XPCollection<TaxLine>(Session,
                                                         new BinaryOperator("PurchaseOrderLine", this.PurchaseOrderLine));

                    XPCollection<TaxLine> _locTaxLines2s = new XPCollection<TaxLine>(Session,
                                                         new BinaryOperator("PurchaseInvoiceLine", this.PurchaseInvoiceLine));

                    XPCollection<TaxLine> _locTaxLines3s = new XPCollection<TaxLine>(Session,
                                                         new BinaryOperator("SalesOrderLine", this.SalesOrderLine));

                    XPCollection<TaxLine> _locTaxLines4s = new XPCollection<TaxLine>(Session,
                                                         new BinaryOperator("SalesQuotationLine", this.SalesQuotationLine));

                    XPCollection<TaxLine> _locTaxLines5s = new XPCollection<TaxLine>(Session,
                                                         new BinaryOperator("SalesInvoiceLine", this.SalesInvoiceLine));

                    XPCollection<TaxLine> _locTaxLines6s = new XPCollection<TaxLine>(Session,
                                                         new BinaryOperator("SalesReturnLine", this.SalesReturnLine));

                    XPCollection<TaxLine> _locTaxLines7s = new XPCollection<TaxLine>(Session,
                                                         new BinaryOperator("DebitMemoLine", this.DebitMemoLine));

                    if (_locTaxLines != null && _locTaxLines.Count() > 0)
                    {
                        foreach (TaxLine _locTaxLine in _locTaxLines)
                        {
                            _locTotalTxAmount = _locTotalTxAmount + _locTaxLine.TxAmount;
                        }
                        _result = _locTotalTxAmount;
                    }
                    else if (_locTaxLines2s != null && _locTaxLines3s.Count() > 0)
                    {
                        foreach (TaxLine _locTaxLines2 in _locTaxLines2s)
                        {
                            _locTotalTxAmount2 = _locTotalTxAmount2 + _locTaxLines2.TxAmount;
                        }
                        _result = _locTotalTxAmount2;
                    }
                    else if (_locTaxLines3s != null && _locTaxLines3s.Count() > 0)
                    {
                        foreach (TaxLine _locTaxLines3 in _locTaxLines3s)
                        {
                            _locTotalTxAmount3 = _locTotalTxAmount3 + _locTaxLines3.TxAmount;
                        }
                        _result = _locTotalTxAmount3;
                    }
                    else if (_locTaxLines4s != null && _locTaxLines4s.Count() > 0)
                    {
                        foreach (TaxLine _locTaxLines4 in _locTaxLines4s)
                        {
                            _locTotalTxAmount4 = _locTotalTxAmount4 + _locTaxLines4.TxAmount;
                        }
                        _result = _locTotalTxAmount4;
                    }
                    else if (_locTaxLines5s != null && _locTaxLines5s.Count() > 0)
                    {
                        foreach (TaxLine _locTaxLines5 in _locTaxLines5s)
                        {
                            _locTotalTxAmount5 = _locTotalTxAmount5 + _locTaxLines5.TxAmount;
                        }
                        _result = _locTotalTxAmount5;
                    }
                    else if (_locTaxLines6s != null && _locTaxLines6s.Count() > 0)
                    {
                        foreach (TaxLine _locTaxLines6 in _locTaxLines6s)
                        {
                            _locTotalTxAmount6 = _locTotalTxAmount6 + _locTaxLines6.TxAmount;
                        }
                        _result = _locTotalTxAmount6;
                    }
                    else if (_locTaxLines7s != null && _locTaxLines7s.Count() > 0)
                    {
                        foreach (TaxLine _locTaxLines7 in _locTaxLines7s)
                        {
                            _locTotalTxAmount7 = _locTotalTxAmount7 + _locTaxLines7.TxAmount;
                        }
                        _result = _locTotalTxAmount7;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = TaxLine" + ex.ToString());
            }
            return _result;
        }

        #endregion Get

        private void SetTaxAmount()
        {
            try
            {
                if (_uAmount >= 0 && Tax.Value >= 0)
                {
                    if (_globFunc.GetRoundingList(Session, ObjectList.TaxLine, FieldName.TxAmount) == true)
                    {
                        this.TxAmount = _globFunc.GetRoundUp(Session, (this.TUAmount * this.TxValue / 100), ObjectList.TaxLine, FieldName.TxAmount);
                    }
                    else
                    {
                        this.TxAmount = this.TUAmount * this.TxValue / 100;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TaxLine " + ex.ToString());
            }
        }

    }
}