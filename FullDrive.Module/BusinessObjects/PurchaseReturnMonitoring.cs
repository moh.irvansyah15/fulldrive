﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.SystemModule;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Editors;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Purchase")]
    [RuleCombinationOfPropertiesIsUnique("PurchaseReturnMonitoringRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class PurchaseReturnMonitoring : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private string _code;
        private bool _select;
        private PurchaseReturn _purchaseReturn;
        private PurchaseReturnLine _purchaseReturnLine;
        private InventoryTransferOut _inventoryTransferOut;
        private InventoryTransferOutLine _inventoryTransferOutLine;
        private CreditMemo _creditMemo;
        private CreditMemoLine _creditMemoLine;
        private Item _item;
        private OrderType _orderType;
        private XPCollection<Item> _availableItem;
        #region InisialisasiDefaultQty
        private double _dQty;
        private UnitOfMeasure _dUom;
        private double _qty;
        private UnitOfMeasure _uom;
        private double _tQty;
        #endregion InisialisasiDefaultQty
        #region InisialisasiAmount
        private Currency _currency;
        private double _uAmount;
        private double _tUAmount;
        private bool _multiTax;
        private Tax _tax;
        private double _txValue;
        private double _txAmount;
        private bool _multiDiscount;
        private Discount _discount;
        private double _disc;
        private double _discAmount;
        private double _tAmount;
        private double _pay;
        #endregion InisialisasiAmount
        private Status _inventoryStatus;
        private DateTime _inventoryStatusDate;
        private Status _memoStatus;
        private DateTime _memoStatusDate;
        private int _postedCount;
        private string _signCode;
        private ReturnType _returnType;
        private InventoryTransferInMonitoring _inventoryTransferInMonitoring;
        private PurchaseOrderMonitoring _purchaseOrderMonitoring;
        private GlobalFunction _globFunc;

        public PurchaseReturnMonitoring(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            _globFunc = new GlobalFunction();
            DateTime now = DateTime.Now;
            this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.PurchaseReturnMonitoring);
            this.Select = true;
            this.InventoryStatus = Status.Open;
            this.InventoryStatusDate = now;
            this.MemoStatus = Status.Open;
            this.MemoStatusDate = now;
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("PurchaseReturnMonitoringCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        public bool Select
        {
            get { return _select; }
            set { SetPropertyValue("Select", ref _select, value); }
        }

        [Appearance("PurchaseReturnMonitoringPurchaseReturnClose", Enabled = false)]
        public PurchaseReturn PurchaseReturn
        {
            get { return _purchaseReturn; }
            set { SetPropertyValue("PurchaseReturn", ref _purchaseReturn, value); }
        }

        [Appearance("PurchaseReturnMonitoringPurchaseReturnLineClose", Enabled = false)]
        public PurchaseReturnLine PurchaseReturnLine
        {
            get { return _purchaseReturnLine; }
            set { SetPropertyValue("PurchaseReturnLine", ref _purchaseReturnLine, value); }
        }

        [Appearance("PurchaseReturnMonitoringInventoryTransferOutClose", Enabled = false)]
        public InventoryTransferOut InventoryTransferOut
        {
            get { return _inventoryTransferOut; }
            set { SetPropertyValue("InventoryTransferOut", ref _inventoryTransferOut, value); }
        }

        [Appearance("PurchaseReturnMonitoringInventoryTransferOutLineClose", Enabled = false)]
        public InventoryTransferOutLine InventoryTransferOutLine
        {
            get { return _inventoryTransferOutLine; }
            set { SetPropertyValue("InventoryTransferOutLine", ref _inventoryTransferOutLine, value); }
        }

        [Appearance("PurchaseReturnMonitoringCreditMemoClose", Enabled = false)]
        public CreditMemo CreditMemo
        {
            get { return _creditMemo; }
            set { SetPropertyValue("CreditMemo", ref _creditMemo, value); }
        }

        [Appearance("PurchaseReturnMonitoringCreditMemoLineClose", Enabled = false)]
        public CreditMemoLine CreditMemoLine
        {
            get { return _creditMemoLine; }
            set { SetPropertyValue("CreditMemoLine", ref _creditMemoLine, value); }
        }

        [ImmediatePostData()]
        [Appearance("PurchaseReturnMonitoringOrderTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public OrderType OrderType
        {
            get { return _orderType; }
            set { SetPropertyValue("OrderType", ref _orderType, value); }
        }

        [Browsable(false)]
        public XPCollection<Item> AvailableItem
        {
            get
            {
                if (OrderType == OrderType.Item)
                {
                    _availableItem = new XPCollection<Item>(Session,
                                    new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("OrderType", OrderType.Item)));

                }
                else if (OrderType == OrderType.Service)
                {
                    _availableItem = new XPCollection<Item>(Session,
                                    new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("OrderType", OrderType.Service)));
                }
                else
                {
                    _availableItem = new XPCollection<Item>(Session);
                }

                return _availableItem;

            }
        }

        [DataSourceProperty("AvailableItem", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("PurchaseReturnMonitoringItemClose", Enabled = false)]
        public Item Item
        {
            get { return _item; }
            set { SetPropertyValue("Item", ref _item, value); }
        }

        #region DefaultQty
        [Appearance("PurchaseReturnMonitoringDQtyClose", Enabled = false)]
        [ImmediatePostData()]
        public double DQty
        {
            get { return _dQty; }
            set { SetPropertyValue("DQty", ref _dQty, value); }
        }

        [Appearance("PurchaseReturnMonitoringDUOMClose", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure DUOM
        {
            get { return _dUom; }
            set { SetPropertyValue("DUOM", ref _dUom, value); }
        }

        [Appearance("PurchaseReturnMonitoringQtyClose", Enabled = false)]
        [ImmediatePostData()]
        public double Qty
        {
            get { return _qty; }
            set { SetPropertyValue("Qty", ref _qty, value); }
        }

        [Appearance("PurchaseReturnMonitoringUOMClose", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure UOM
        {
            get { return _uom; }
            set { SetPropertyValue("UOM", ref _uom, value); }
        }

        [Appearance("PurchaseReturnMonitoringTotalQtyEnabled", Enabled = false)]
        public double TQty
        {
            get { return _tQty; }
            set { SetPropertyValue("TotalQty", ref _tQty, value); }
        }
        #endregion DefaultQty

        #region Amount

        [Appearance("PurchaseReturnMonitoringCurrencyClose", Enabled = false)]
        public Currency Currency
        {
            get { return _currency; }
            set { SetPropertyValue("Currency", ref _currency, value); }
        }

        [Appearance("PurchaseReturnMonitoringUAmountClose", Enabled = false)]
        [ImmediatePostData()]
        public double UAmount
        {
            get { return _uAmount; }
            set { SetPropertyValue("UAmount", ref _uAmount, value); }
        }

        [Appearance("PurchaseReturnMonitoringTUAmountEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double TUAmount
        {
            get { return _tUAmount; }
            set { SetPropertyValue("TUAmount", ref _tUAmount, value); }
        }

        [Appearance("PurchaseReturnMonitoringMultiTaxClose", Enabled = false)]
        [ImmediatePostData()]
        public bool MultiTax
        {
            get { return _multiTax; }
            set { SetPropertyValue("MultiTax", ref _multiTax, value); }
        }

        [Appearance("PurchaseReturnMonitoringTaxClose", Enabled = false)]
        [Appearance("PurchaseReturnMonitoringTaxEnabled", Criteria = "MultiTax = true", Enabled = false)]
        [ImmediatePostData]
        [DataSourceCriteria("Active = true")]
        public Tax Tax
        {
            get { return _tax; }
            set { SetPropertyValue("Tax", ref _tax, value); }
        }

        [Appearance("PurchaseReturnMonitoringTxValueClose", Enabled = false)]
        [ImmediatePostData()]
        public double TxValue
        {
            get { return _txValue; }
            set { SetPropertyValue("TxValue", ref _txValue, value); }
        }

        [Appearance("PurchaseReturnMonitoringTxAmountEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double TxAmount
        {
            get { return _txAmount; }
            set { SetPropertyValue("TxAmount", ref _txAmount, value); }
        }

        [Appearance("PurchaseReturnMonitoringMultiDiscountClose", Enabled = false)]
        [ImmediatePostData()]
        public bool MultiDiscount
        {
            get { return _multiDiscount; }
            set { SetPropertyValue("MultiDiscount", ref _multiDiscount, value); }
        }

        [Appearance("PurchaseReturnMonitoringDiscountClose", Enabled = false)]
        [Appearance("PurchaseReturnMonitoringDiscountEnabled", Criteria = "MultiDiscount = true", Enabled = false)]
        [ImmediatePostData()]
        public Discount Discount
        {
            get { return _discount; }
            set { SetPropertyValue("Discount", ref _discount, value); }
        }

        [Appearance("PurchaseReturnMonitoringDiscClose", Enabled = false)]
        [Appearance("PurchaseReturnMonitoringDiscEnabled", Criteria = "MultiDiscount = true", Enabled = false)]
        [ImmediatePostData()]
        public double Disc
        {
            get { return _disc; }
            set { SetPropertyValue("Disc", ref _disc, value); }
        }

        [Appearance("PurchaseReturnMonitoringDiscAmountEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double DiscAmount
        {
            get { return _discAmount; }
            set { SetPropertyValue("DiscAmount", ref _discAmount, value); }
        }

        [Appearance("PurchaseReturnMonitoringTAmountEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double TAmount
        {
            get { return _tAmount; }
            set { SetPropertyValue("TAmount", ref _tAmount, value); }
        }

        [Appearance("PurchaseReturnMonitoringPayEnabled", Enabled = false)]
        public double Pay
        {
            get { return _pay; }
            set { SetPropertyValue("Pay", ref _pay, value); }
        }
        #endregion Amount

        [Appearance("PurchaseReturnMonitoringInventoryStatusEnabled", Enabled = false)]
        public Status InventoryStatus
        {
            get { return _inventoryStatus; }
            set { SetPropertyValue("InventoryStatus", ref _inventoryStatus, value); }
        }

        [Appearance("PurchaseReturnMonitoringInventoryStatusDateEnabled", Enabled = false)]
        public DateTime InventoryStatusDate
        {
            get { return _inventoryStatusDate; }
            set { SetPropertyValue("InventoryStatusDate", ref _inventoryStatusDate, value); }
        }

        [Appearance("PurchaseReturnMonitoringMemoStatusEnabled", Enabled = false)]
        public Status MemoStatus
        {
            get { return _memoStatus; }
            set { SetPropertyValue("MemoStatus", ref _memoStatus, value); }
        }

        [Appearance("PurchaseReturnMonitoringMemoStatusDateEnabled", Enabled = false)]
        public DateTime MemoStatusDate
        {
            get { return _memoStatusDate; }
            set { SetPropertyValue("MemoStatusDate", ref _memoStatusDate, value); }
        }

        [Appearance("PurchaseReturnMonitoringPostedCountEnabled", Enabled = false)]
        public int PostedCount
        {
            get { return _postedCount; }
            set { SetPropertyValue("PostedCount", ref _postedCount, value); }
        }

        [Appearance("PurchaseReturnMonitoringSignCodeClose", Enabled = false)]
        public string SignCode
        {
            get { return _signCode; }
            set { SetPropertyValue("SignCode", ref _signCode, value); }
        }

        //[Appearance("PurchaseReturnMonitoringReturnTypeClose", Enabled = false)]
        public ReturnType ReturnType
        {
            get { return _returnType; }
            set { SetPropertyValue("ReturnType", ref _returnType, value); }
        }

        [Browsable(false)]
        public InventoryTransferInMonitoring InventoryTransferInMonitoring
        {
            get { return _inventoryTransferInMonitoring; }
            set { SetPropertyValue("InventoryTransferInMonitoring", ref _inventoryTransferInMonitoring, value); }
        }

        [Browsable(false)]
        public PurchaseOrderMonitoring PurchaseOrderMonitoring
        {
            get { return _purchaseOrderMonitoring; }
            set { SetPropertyValue("PurchaseOrderMonitoring", ref _purchaseOrderMonitoring, value); }
        }

        #endregion Field

        //private string _PersistentProperty;
        //[XafDisplayName("My display name"), ToolTip("My hint message")]
        //[ModelDefault("EditMask", "(000)-00"), Index(0), VisibleInListView(false)]
        //[Persistent("DatabaseColumnName"), RuleRequiredField(DefaultContexts.Save)]
        //public string PersistentProperty {
        //    get { return _PersistentProperty; }
        //    set { SetPropertyValue("PersistentProperty", ref _PersistentProperty, value); }
        //}

        //[Action(Caption = "My UI Action", ConfirmationMessage = "Are you sure?", ImageName = "Attention", AutoCommit = true)]
        //public void ActionMethod() {
        //    // Trigger a custom business logic for the current record in the UI (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112619.aspx).
        //    this.PersistentProperty = "Paid";
        //}
    }
}