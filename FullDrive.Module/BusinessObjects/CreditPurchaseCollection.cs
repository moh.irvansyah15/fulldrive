﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("CreditPurchaseCollectionRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class CreditPurchaseCollection : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private bool _activationPosting;
        private string _code;
        private XPCollection<PurchaseReturn> _availablePurchaseReturn;
        private PurchaseReturn _purchaseReturn;
        private CreditMemo _creditMemo;
        private Status _status;
        private DateTime _statusDate;
        private int _postedCount;
        private GlobalFunction _globFunc;

        public CreditPurchaseCollection(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.CreditPurchaseCollection);
                DateTime now = DateTime.Now;
                this.Status = Status.Open;
                this.StatusDate = now;
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("CreditPurchaseCollectionCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Browsable(false)]
        public XPCollection<PurchaseReturn> AvailablePurchaseReturn
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;

                if (!IsLoading)
                {
                    if (this.CreditMemo != null)
                    {
                        if (this.CreditMemo.BuyFromVendor != null)
                        {
                            XPQuery<PurchaseReturnMonitoring> _purchaseReturnMonitoringsQuery = new XPQuery<PurchaseReturnMonitoring>(Session);

                            var _purchaseReturnMonitorings = from prm in _purchaseReturnMonitoringsQuery
                                                          where (prm.MemoStatus != Status.Open && prm.ReturnType == ReturnType.CreditMemo
                                                                   && prm.PurchaseReturn.BuyFromVendor == this.CreditMemo.BuyFromVendor
                                                                   && prm.CreditMemo == null)
                                                          group prm by prm.PurchaseReturn into g
                                                          select new { PurchaseReturn = g.Key };

                            if (_purchaseReturnMonitorings != null && _purchaseReturnMonitorings.Count() > 0)
                            {
                                List<string> _stringPRM = new List<string>();

                                foreach (var _inventoryTransferInMonitoring in _purchaseReturnMonitorings)
                                {
                                    if (_inventoryTransferInMonitoring.PurchaseReturn != null)
                                    {
                                        if (_inventoryTransferInMonitoring.PurchaseReturn.Code != null)
                                        {
                                            _stringPRM.Add(_inventoryTransferInMonitoring.PurchaseReturn.Code);
                                        }
                                    }
                                }

                                IEnumerable<string> _stringArrayPRMDistinct = _stringPRM.Distinct();
                                string[] _stringArrayPRMList = _stringArrayPRMDistinct.ToArray();
                                if (_stringArrayPRMList.Length == 1)
                                {
                                    for (int i = 0; i < _stringArrayPRMList.Length; i++)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _stringArrayPRMList[i] + "'";
                                        }
                                    }
                                }
                                else if (_stringArrayPRMList.Length > 1)
                                {
                                    for (int i = 0; i < _stringArrayPRMList.Length; i++)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _stringArrayPRMList[i] + "'";
                                        }
                                        else
                                        {
                                            _endString = _endString + " OR [Code]=='" + _stringArrayPRMList[i] + "'";
                                        }
                                    }
                                }
                                _fullString = _beginString + _endString;

                                if (_fullString != null)
                                {
                                    _availablePurchaseReturn = new XPCollection<PurchaseReturn>(Session, CriteriaOperator.Parse(_fullString));
                                }
                            }
                        }
                    }
                }
                return _availablePurchaseReturn;
            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailablePurchaseReturn", DataSourcePropertyIsNullMode.SelectNothing)]
        public PurchaseReturn PurchaseReturn
        {
            get { return _purchaseReturn; }
            set { SetPropertyValue("PurchaseReturn", ref _purchaseReturn, value); }
        }

        [ImmediatePostData()]
        [Appearance("CreditPurchaseCollectionDebitMemoClose", Enabled = false)]
        [Association("CreditMemo-CreditPurchaseCollections")]
        public CreditMemo CreditMemo
        {
            get { return _creditMemo; }
            set { SetPropertyValue("CreditMemo", ref _creditMemo, value); }
        }

        [Appearance("CreditPurchaseCollectionStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("CreditPurchaseCollectionStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("CreditPurchaseCollectionPostedCountEnabled", Enabled = false)]
        public int PostedCount
        {
            get { return _postedCount; }
            set { SetPropertyValue("PostedCount", ref _postedCount, value); }
        }

        #endregion Field

        //private string _PersistentProperty;
        //[XafDisplayName("My display name"), ToolTip("My hint message")]
        //[ModelDefault("EditMask", "(000)-00"), Index(0), VisibleInListView(false)]
        //[Persistent("DatabaseColumnName"), RuleRequiredField(DefaultContexts.Save)]
        //public string PersistentProperty {
        //    get { return _PersistentProperty; }
        //    set { SetPropertyValue("PersistentProperty", ref _PersistentProperty, value); }
        //}

        //[Action(Caption = "My UI Action", ConfirmationMessage = "Are you sure?", ImageName = "Attention", AutoCommit = true)]
        //public void ActionMethod() {
        //    // Trigger a custom business logic for the current record in the UI (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112619.aspx).
        //    this.PersistentProperty = "Paid";
        //}
    }
}