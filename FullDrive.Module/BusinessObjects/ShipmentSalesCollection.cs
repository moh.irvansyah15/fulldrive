﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("ShipmentSalesCollectionRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class ShipmentSalesCollection : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).

        private bool _activationPosting;
        private bool _select;
        private string _code;
        private ShipmentType _shipmentType;
        private BusinessPartner _shipper;
        private BusinessPartner _consignee;
        private XPCollection<ShipmentSalesMonitoring> _availableShipmentSalesMonitoring;
        private ShipmentSalesMonitoring _shipmentSalesMonitoring;
        private ShipmentSalesInvoice _shipmentSalesInvoice;
        private Status _status;
        private DateTime _statusDate;
        private int _postedCount;
        private GlobalFunction _globFunc;

        public ShipmentSalesCollection(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            DateTime now = DateTime.Now;
            _globFunc = new GlobalFunction();
            this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.ShipmentSalesCollection);
            this.Status = Status.Open;
            this.StatusDate = now;
            this.Select = true;
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [Appearance("ShipmentSalesCollectionSelectClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public bool Select
        {
            get { return _select; }
            set { SetPropertyValue("Select", ref _select, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("ShipmentSalesCollectionCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [ImmediatePostData()]
        public ShipmentType ShipmentType
        {
            get { return _shipmentType; }
            set { SetPropertyValue("ShipmentType", ref _shipmentType, value); }
        }

        [ImmediatePostData()]
        public BusinessPartner Shipper
        {
            get { return _shipper; }
            set { SetPropertyValue("Shipper", ref _shipper, value); }
        }

        [ImmediatePostData()]
        public BusinessPartner Consignee
        {
            get { return _consignee; }
            set { SetPropertyValue("Consignee", ref _consignee, value); }
        }

        [Browsable(false)]
        public XPCollection<ShipmentSalesMonitoring> AvailableShipmentSalesMonitoring
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;
                
                if (!IsLoading)
                {
                    if (this.ShipmentSalesInvoice != null)
                    {
                        if (this.ShipmentSalesInvoice.ShipmentType != ShipmentType.None )
                        {
                            XPQuery<ShipmentSalesMonitoring> _shipmentSalesMonitoringsQuery = new XPQuery<ShipmentSalesMonitoring>(Session);

                            #region ShipperAndConsignee
                            if(this.Shipper != null && this.Consignee != null)
                            {
                                var _shipmentSalesMonitorings = from spm in _shipmentSalesMonitoringsQuery
                                                                    where (spm.Status != Status.Close
                                                                    && spm.ShipmentType == this.ShipmentSalesInvoice.ShipmentType
                                                                    && spm.Shipper == this.Shipper
                                                                    && spm.Consignee == this.Consignee
                                                                    && spm.ShipmentSalesInvoice == null)
                                                                    group spm by spm.Code into g
                                                                    select new { Code = g.Key };

                                if (_shipmentSalesMonitorings != null && _shipmentSalesMonitorings.Count() > 0)
                                {
                                    List<string> _stringSSM = new List<string>();

                                    foreach (var _shipmentSalesMonitoring in _shipmentSalesMonitorings)
                                    {
                                        if (_shipmentSalesMonitoring != null)
                                        {
                                            if (_shipmentSalesMonitoring.Code != null)
                                            {
                                                _stringSSM.Add(_shipmentSalesMonitoring.Code);
                                            }
                                        }
                                    }

                                    IEnumerable<string> _stringArraySSMDistinct = _stringSSM.Distinct();
                                    string[] _stringArraySSMList = _stringArraySSMDistinct.ToArray();
                                    if (_stringArraySSMList.Length == 1)
                                    {
                                        for (int i = 0; i < _stringArraySSMList.Length; i++)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _stringArraySSMList[i] + "'";
                                            }
                                        }
                                    }
                                    else if (_stringArraySSMList.Length > 1)
                                    {
                                        for (int i = 0; i < _stringArraySSMList.Length; i++)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _stringArraySSMList[i] + "'";
                                            }
                                            else
                                            {
                                                _endString = _endString + " OR [Code]=='" + _stringArraySSMList[i] + "'";
                                            }
                                        }
                                    }
                                    _fullString = _beginString + _endString;
                                }
                            }
                            #endregion ShipperAndConsignee

                            #region Shipper
                            if (this.Shipper != null && this.Consignee == null)
                            {
                                var _shipmentSalesMonitorings = from spm in _shipmentSalesMonitoringsQuery
                                                                where (spm.Status != Status.Close
                                                                && spm.ShipmentType == this.ShipmentSalesInvoice.ShipmentType
                                                                && spm.Shipper == this.Shipper
                                                                && spm.ShipmentSalesInvoice == null)
                                                                group spm by spm.Code into g
                                                                select new { Code = g.Key };

                                if (_shipmentSalesMonitorings != null && _shipmentSalesMonitorings.Count() > 0)
                                {
                                    List<string> _stringSSM = new List<string>();

                                    foreach (var _shipmentSalesMonitoring in _shipmentSalesMonitorings)
                                    {
                                        if (_shipmentSalesMonitoring != null)
                                        {
                                            if (_shipmentSalesMonitoring.Code != null)
                                            {
                                                _stringSSM.Add(_shipmentSalesMonitoring.Code);
                                            }
                                        }
                                    }

                                    IEnumerable<string> _stringArraySSMDistinct = _stringSSM.Distinct();
                                    string[] _stringArraySSMList = _stringArraySSMDistinct.ToArray();
                                    if (_stringArraySSMList.Length == 1)
                                    {
                                        for (int i = 0; i < _stringArraySSMList.Length; i++)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _stringArraySSMList[i] + "'";
                                            }
                                        }
                                    }
                                    else if (_stringArraySSMList.Length > 1)
                                    {
                                        for (int i = 0; i < _stringArraySSMList.Length; i++)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _stringArraySSMList[i] + "'";
                                            }
                                            else
                                            {
                                                _endString = _endString + " OR [Code]=='" + _stringArraySSMList[i] + "'";
                                            }
                                        }
                                    }
                                    _fullString = _beginString + _endString;
                                }
                            }
                            #endregion Shipper

                            #region Consignee
                            if (this.Shipper == null && this.Consignee != null)
                            {
                                var _shipmentSalesMonitorings = from spm in _shipmentSalesMonitoringsQuery
                                                                where (spm.Status != Status.Close
                                                                && spm.ShipmentType == this.ShipmentSalesInvoice.ShipmentType
                                                                && spm.Shipper == this.Shipper
                                                                && spm.Consignee == this.Consignee
                                                                && spm.ShipmentSalesInvoice == null)
                                                                group spm by spm.Code into g
                                                                select new { Code = g.Key };

                                if (_shipmentSalesMonitorings != null && _shipmentSalesMonitorings.Count() > 0)
                                {
                                    List<string> _stringSSM = new List<string>();

                                    foreach (var _shipmentSalesMonitoring in _shipmentSalesMonitorings)
                                    {
                                        if (_shipmentSalesMonitoring != null)
                                        {
                                            if (_shipmentSalesMonitoring.Code != null)
                                            {
                                                _stringSSM.Add(_shipmentSalesMonitoring.Code);
                                            }
                                        }
                                    }

                                    IEnumerable<string> _stringArraySSMDistinct = _stringSSM.Distinct();
                                    string[] _stringArraySSMList = _stringArraySSMDistinct.ToArray();
                                    if (_stringArraySSMList.Length == 1)
                                    {
                                        for (int i = 0; i < _stringArraySSMList.Length; i++)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _stringArraySSMList[i] + "'";
                                            }
                                        }
                                    }
                                    else if (_stringArraySSMList.Length > 1)
                                    {
                                        for (int i = 0; i < _stringArraySSMList.Length; i++)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _stringArraySSMList[i] + "'";
                                            }
                                            else
                                            {
                                                _endString = _endString + " OR [Code]=='" + _stringArraySSMList[i] + "'";
                                            }
                                        }
                                    }
                                    _fullString = _beginString + _endString;
                                }
                            }
                            #endregion Consignee

                            #region NullShipperAndConsignee
                            if (this.Shipper == null && this.Consignee == null)
                            {
                                var _shipmentSalesMonitorings = from spm in _shipmentSalesMonitoringsQuery
                                                                where (spm.Status != Status.Close
                                                                && spm.ShipmentType == this.ShipmentSalesInvoice.ShipmentType
                                                                && spm.ShipmentSalesInvoice == null)
                                                                group spm by spm.Code into g
                                                                select new { Code = g.Key };

                                if (_shipmentSalesMonitorings != null && _shipmentSalesMonitorings.Count() > 0)
                                {
                                    List<string> _stringSSM = new List<string>();

                                    foreach (var _shipmentSalesMonitoring in _shipmentSalesMonitorings)
                                    {
                                        if (_shipmentSalesMonitoring != null)
                                        {
                                            if (_shipmentSalesMonitoring.Code != null)
                                            {
                                                _stringSSM.Add(_shipmentSalesMonitoring.Code);
                                            }
                                        }
                                    }

                                    IEnumerable<string> _stringArraySSMDistinct = _stringSSM.Distinct();
                                    string[] _stringArraySSMList = _stringArraySSMDistinct.ToArray();
                                    if (_stringArraySSMList.Length == 1)
                                    {
                                        for (int i = 0; i < _stringArraySSMList.Length; i++)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _stringArraySSMList[i] + "'";
                                            }
                                        }
                                    }
                                    else if (_stringArraySSMList.Length > 1)
                                    {
                                        for (int i = 0; i < _stringArraySSMList.Length; i++)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _stringArraySSMList[i] + "'";
                                            }
                                            else
                                            {
                                                _endString = _endString + " OR [Code]=='" + _stringArraySSMList[i] + "'";
                                            }
                                        }
                                    }
                                    _fullString = _beginString + _endString;
                                }
                            }
                            #endregion NullShipperAndConsignee

                            if (_fullString != null)
                            {
                                _availableShipmentSalesMonitoring = new XPCollection<ShipmentSalesMonitoring>(Session, CriteriaOperator.Parse(_fullString));
                            }
                               
                        }
                    }
                }

                return _availableShipmentSalesMonitoring;
            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableShipmentSalesMonitoring", DataSourcePropertyIsNullMode.SelectNothing)]
        public ShipmentSalesMonitoring ShipmentSalesMonitoring
        {
            get { return _shipmentSalesMonitoring; }
            set { SetPropertyValue("ShipmentSalesMonitoring", ref _shipmentSalesMonitoring, value); }
        }

        [Appearance("ShipmentSalesCollectionStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("ShipmentSalesCollectionStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [ImmediatePostData()]
        [Appearance("ShipmentSalesCollectionShipmentSalesInvoiceClose", Enabled = false)]
        [Association("ShipmentSalesInvoice-ShipmentSalesCollections")]
        public ShipmentSalesInvoice ShipmentSalesInvoice
        {
            get { return _shipmentSalesInvoice; }
            set {
                SetPropertyValue("ShipmentSalesInvoice", ref _shipmentSalesInvoice, value);
                if(!IsLoading)
                {
                    if(this._shipmentSalesInvoice != null)
                    {
                        if(this._shipmentSalesInvoice.Shipper != null)
                        {
                            this.Shipper = this._shipmentSalesInvoice.Shipper;
                        }

                        if(this._shipmentSalesInvoice.Consignee != null)
                        {
                            this.Consignee = this._shipmentSalesInvoice.Consignee;
                        }
                    }
                }
            }
        }

        #endregion Field
    }
}