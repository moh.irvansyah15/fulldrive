﻿#region Default

using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

#endregion Default

using DevExpress.ExpressApp.ConditionalAppearance;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using System.IO;

namespace FullDrive.Module.BusinessObjects
{
    #region Default

    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("CashAdvanceLineRuleUnique", DefaultContexts.Save, "Code")]

    #endregion Default

    // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
    public class CashAdvanceLine : FullDriveSysBaseObject
    {
        #region Default

        
        private bool _activationPosting;
        private string _code;
        private bool _select;
        private string _name;
        private CashAdvance _cashAdvance;
        private Employee _employee;
        private DateTime _requestDate;
        private DateTime _requiredDate;
        private CashAdvanceType _cashAdvanceType;
        private double _amount;
        private double _amountRealized;
        private string _description;
        private Status _status;
        private DateTime _statusDate;
        private int _processCount;
        private string _currentUser;
        private Company _company;
        private Division _division;
        private Department _department;
        private Section _section;
        private GlobalFunction _globFunc;

        public CashAdvanceLine(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).

            _currentUser = SecuritySystem.CurrentUserName;
            this._globFunc = new GlobalFunction();
            this._code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.CashAdvanceLine);
            this._status = Status.Open;
            this.Select = true;
            this._requestDate = DateTime.Now;
            this._statusDate = DateTime.Now;
            this._requiredDate = DateTime.Now;
        }

        #endregion Default

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [ImmediatePostData()]
        [Appearance("CashAdvanceCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("CashAdvanceLineSelectClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public bool Select
        {
            get { return _select; }
            set { SetPropertyValue("Select", ref _select, value); }
        }

        [ImmediatePostData()]
        [Appearance("CashAdvanceLineNameClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Name
        {
            get
            {
                if (Code != null)
                {
                    if (Employee != null)
                    {
                        UpdateName();
                    }
                }
                return _name;
            }
            set
            {
                SetPropertyValue("Name", ref _name, value);
                UpdateName();
            }
        }

        [ImmediatePostData()]
        [Appearance("CashAdvanceLineEmployeeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Employee Employee
        {
            get
            {
                if (CashAdvance != null)
                {
                    this._employee = this._cashAdvance.Employee;
                }
                return _employee;
            }
            set
            {
                SetPropertyValue("Employee", ref _employee, value);
                UpdateName();
            }
        }

        [Appearance("CashAdvanceLineRequestDateClose", Enabled = false)]
        public DateTime RequestDate
        {
            get { return _requestDate; }
            set { SetPropertyValue("RequestDate", ref _requestDate, value); }
        }

        [Appearance("CashAdvanceLineRequiredDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime RequiredDate
        {
            get { return _requiredDate; }
            set { SetPropertyValue("RequiredDate", ref _requiredDate, value); }
        }

        [Appearance("CashAdvanceLineCashAdvanceTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public CashAdvanceType CashAdvanceType
        {
            get { return _cashAdvanceType; }
            set { SetPropertyValue("CashAdvaceType", ref _cashAdvanceType, value); }
        }

        [ImmediatePostData()]
        [Appearance("CashAdvanceLineAmountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double Amount
        {
            get { return _amount; }
            set {
                SetPropertyValue("Amount", ref _amount, value);
                if(!IsLoading)
                {
                    if(this._amount > 0)
                    {
                        this.AmountRealized = this._amount;
                    }
                }
            }
        }

        [ImmediatePostData]
        [Appearance("CashAdvanceLineAmountRealizedClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double AmountRealized
        {
            get { return _amountRealized; }
            set {
                SetPropertyValue("AmountRealized", ref _amountRealized, value);
                if(!IsLoading)
                {
                    if(this._amountRealized > 0)
                    {
                        if(this._amountRealized > this.Amount )
                        {
                            this._amountRealized = this.Amount;
                        }
                    }
                }
            }
        }

        [Size(512)]
        [Appearance("CashAdvanceLineDescriptionClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        //[Appearance("CashAdvanceLineStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("CashAdvanceLineStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("CashAdvanceLineProcessCountEnabled", Enabled = false)]
        public int ProcessCount
        {
            get { return _processCount; }
            set { SetPropertyValue("ProcessCount", ref _processCount, value); }
        }

        [Browsable(false)]
        [Appearance("CashAdvanceLineCompanyEnabled", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Browsable(false)]
        [Appearance("CashAdvanceLineDivisionEnabled", Enabled = false)]
        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        [Browsable(false)]
        [Appearance("CashAdvanceLineDepartmentEnabled", Enabled = false)]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        [Browsable(false)]
        [Appearance("CashAdvanceLineSectionEnabled", Enabled = false)]
        public Section Section
        {
            get { return _section; }
            set { SetPropertyValue("Section", ref _section, value); }
        }

        [Association("CashAdvance-CashAdvanceLines")]
        [Appearance("CashAdvanceLineCashAdvanceClose", Enabled = false)]
        public CashAdvance CashAdvance
        {
            get { return _cashAdvance; }
            set {
                SetPropertyValue("CashAdvance", ref _cashAdvance, value);
                if(!IsLoading)
                {
                    if(this._cashAdvance != null)
                    {
                        if(this._cashAdvance.Company != null)
                        {
                            this.Company = this._cashAdvance.Company;
                        }
                        if(this._cashAdvance.Division != null)
                        {
                            this.Division = this._cashAdvance.Division;
                        }
                        if(this._cashAdvance.Department != null)
                        {
                            this.Department = this._cashAdvance.Department;
                        }
                        if(this._cashAdvance.Section != null)
                        {
                            this.Section = this._cashAdvance.Section;
                        }
                        this.Amount = this._cashAdvance.AmountRealized;
                    }
                }
            }
        }

        #endregion Field

        //==== Code Only =====

        #region UpdateName

        private void UpdateName()
        {
            try
            {
                this._name = string.Format("Cash Advance Line {0} [ {1} ]", Code, Employee.Name);
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = CashAdvanceLine" + ex.ToString());
            }
        }

        #endregion UpdateName

    }
}