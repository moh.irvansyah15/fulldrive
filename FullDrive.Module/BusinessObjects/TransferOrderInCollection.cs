﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Inventory")]
    [RuleCombinationOfPropertiesIsUnique("TransferOrderInCollectionRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class TransferOrderInCollection : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private bool _activationPosting;
        private string _code;
        private XPCollection<TransferOrder> _availableTransferOrder;
        private TransferOrder _transferOrder;
        private TransferIn _transferIn;
        private Status _status;
        private DateTime _statusDate;
        private GlobalFunction _globFunc;

        public TransferOrderInCollection(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.TransferOrderInCollection);
                DateTime now = DateTime.Now;
                this.Status = Status.Open;
                this.StatusDate = now;
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("TransferOrderInCollectionCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Browsable(false)]
        public XPCollection<TransferOrder> AvailableTransferOrder
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;

                if (!IsLoading)
                {
                    if (this.TransferIn != null)
                    {
                        XPQuery<TransferOrderMonitoring> _transferOrderMonitoringsQuery = new XPQuery<TransferOrderMonitoring>(Session);

                        var _transferOrderMonitorings = from tom in _transferOrderMonitoringsQuery
                                                        where (tom.Status == Status.Open && tom.PostedCount == 0
                                                        && tom.InventoryMovingType == InventoryMovingType.TransferIn
                                                        && tom.TransferIn == null)
                                                        group tom by tom.TransferOrder into g
                                                        select new { TransferOrder = g.Key };

                        if (_transferOrderMonitorings != null && _transferOrderMonitorings.Count() > 0)
                        {
                            List<string> _stringTOM = new List<string>();

                            foreach (var _transferOrderMonitoring in _transferOrderMonitorings)
                            {
                                if (_transferOrderMonitoring != null)
                                {
                                    _stringTOM.Add(_transferOrderMonitoring.TransferOrder.Code);
                                }
                            }

                            IEnumerable<string> _stringArrayTOMDistinct = _stringTOM.Distinct();
                            string[] _stringArrayTOMList = _stringArrayTOMDistinct.ToArray();
                            if (_stringArrayTOMList.Length == 1)
                            {
                                for (int i = 0; i < _stringArrayTOMList.Length; i++)
                                {
                                    if (i == 0)
                                    {
                                        _beginString = "[Code]=='" + _stringArrayTOMList[i] + "'";
                                    }
                                }
                            }
                            else if (_stringArrayTOMList.Length > 1)
                            {
                                for (int i = 0; i < _stringArrayTOMList.Length; i++)
                                {
                                    if (i == 0)
                                    {
                                        _beginString = "[Code]=='" + _stringArrayTOMList[i] + "'";
                                    }
                                    else
                                    {
                                        _endString = _endString + " OR [Code]=='" + _stringArrayTOMList[i] + "'";
                                    }
                                }
                            }
                            _fullString = _beginString + _endString;

                            if (_fullString != null)
                            {
                                _availableTransferOrder = new XPCollection<TransferOrder>(Session, CriteriaOperator.Parse(_fullString));
                            }
                        }
                    }
                }
                return _availableTransferOrder;
            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableTransferOrder", DataSourcePropertyIsNullMode.SelectNothing)]
        public TransferOrder TransferOrder
        {
            get { return _transferOrder; }
            set { SetPropertyValue("TransferOrder", ref _transferOrder, value); }
        }

        [ImmediatePostData()]
        [Appearance("TransferOrderInCollectionTransferInClose", Enabled = false)]
        [Association("TransferIn-TransferOrderInCollections")]
        public TransferIn TransferIn
        {
            get { return _transferIn; }
            set { SetPropertyValue("TransferIn", ref _transferIn, value); }
        }

        [Appearance("TransferOrderCollectionStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("TransferOrderCollectionStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        #endregion Field

        //private string _PersistentProperty;
        //[XafDisplayName("My display name"), ToolTip("My hint message")]
        //[ModelDefault("EditMask", "(000)-00"), Index(0), VisibleInListView(false)]
        //[Persistent("DatabaseColumnName"), RuleRequiredField(DefaultContexts.Save)]
        //public string PersistentProperty {
        //    get { return _PersistentProperty; }
        //    set { SetPropertyValue("PersistentProperty", ref _PersistentProperty, value); }
        //}

        //[Action(Caption = "My UI Action", ConfirmationMessage = "Are you sure?", ImageName = "Attention", AutoCommit = true)]
        //public void ActionMethod() {
        //    // Trigger a custom business logic for the current record in the UI (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112619.aspx).
        //    this.PersistentProperty = "Paid";
        //}
    }
}