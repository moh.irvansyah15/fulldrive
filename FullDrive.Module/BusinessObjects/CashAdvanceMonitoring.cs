﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using DevExpress.ExpressApp.ConditionalAppearance;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using System.IO;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("CashAdvanceMonitoringRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class CashAdvanceMonitoring : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private string _code;
        private bool _select;
        private CashAdvance _cashAdvance;
        private CashAdvanceLine _cashAdvanceLine;
        private PaymentRealization _paymentRealization;
        private PaymentRealizationLine _paymentRealizationLine;
        private Employee _employee;
        private Company _company;
        private Division _division;
        private Department _department;
        private Section _section;
        private DateTime _requestDate;
        private DateTime _requiredDate;
        private CashAdvanceType _cashAdvanceType;
        private double _amount;
        private double _amountRealized;
        private string _description;
        private Status _status;
        private DateTime _statusDate;
        private int _postedCount;
        private string _signCode;
        private GlobalFunction _globFunc;

        public CashAdvanceMonitoring(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            _globFunc = new GlobalFunction();
            DateTime now = DateTime.Now;
            this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.CashAdvanceMonitoring);
            this.Select = true;
            this.Status = Status.Open;
            this.StatusDate = now;
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [Appearance("CashAdvanceMonitoringCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        public bool Select
        {
            get { return _select; }
            set { SetPropertyValue("Select", ref _select, value); }
        }

        [Appearance("CashAdvanceMonitoringCashAdvanceClose", Enabled = false)]
        public CashAdvance CashAdvance
        {
            get { return _cashAdvance; }
            set { SetPropertyValue("CashAdvance", ref _cashAdvance, value); }
        }

        [Appearance("CashAdvanceMonitoringCashAdvanceLineClose", Enabled = false)]
        public CashAdvanceLine CashAdvanceLine
        {
            get { return _cashAdvanceLine; }
            set { SetPropertyValue("CashAdvanceLine", ref _cashAdvanceLine, value); }
        }

        [Appearance("CashAdvanceMonitoringPaymentRealizationClose", Enabled = false)]
        public PaymentRealization PaymentRealization
        {
            get { return _paymentRealization; }
            set { SetPropertyValue("PaymentRealization", ref _paymentRealization, value); }
        }

        [Appearance("CashAdvanceMonitoringPaymentRealizationLineClose", Enabled = false)]
        public PaymentRealizationLine PaymentRealizationLine
        {
            get { return _paymentRealizationLine; }
            set { SetPropertyValue("PaymentRealizationLine", ref _paymentRealizationLine, value); }
        }

        [Appearance("CashAdvanceMonitoringEmployeeClose", Enabled = false)]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue("Employee", ref _employee, value); }
        }

        [Appearance("CashAdvanceMonitoringCompanyClose", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Appearance("CashAdvanceMonitoringDivisionClose", Enabled = false)]
        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        [Appearance("CashAdvanceMonitoringDepartmentClose", Enabled = false)]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        [Appearance("CashAdvanceMonitoringSectionClose", Enabled = false)]
        public Section Section
        {
            get { return _section; }
            set { SetPropertyValue("Section", ref _section, value); }
        }

        [Appearance("CashAdvanceMonitoringRequestDateClose", Enabled = false)]
        public DateTime RequestDate
        {
            get { return _requestDate; }
            set { SetPropertyValue("RequestDate", ref _requestDate, value); }
        }

        [Appearance("CashAdvanceMonitoringRequiredDateClose", Enabled = false)]
        public DateTime RequiredDate
        {
            get { return _requiredDate; }
            set { SetPropertyValue("RequiredDate", ref _requiredDate, value); }
        }

        [Appearance("CashAdvanceMonitoringCashAdvanceTypeClose", Enabled = false)]
        public CashAdvanceType CashAdvanceType
        {
            get { return _cashAdvanceType; }
            set { SetPropertyValue("CashAdvanceType", ref _cashAdvanceType, value); }
        }

        [Appearance("CashAdvanceMonitoringAmountClose", Enabled = false)]
        public double Amount
        {
            get { return _amount; }
            set { SetPropertyValue("Amount", ref _amount, value); }
        }

        [Appearance("CashAdvanceMonitoringAmountRealizedClose", Enabled = false)]
        public double AmountRealized
        {
            get { return _amountRealized; }
            set { SetPropertyValue("AmountRealized", ref _amountRealized, value); }
        }

        [Size(512)]
        [Appearance("CashAdvanceMonitoringDescriptionClose", Enabled = false)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        [Appearance("CashAdvanceMonitoringStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("CashAdvanceMonitoringStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("CashAdvanceMonitoringPostedCountEnabled", Enabled = false)]
        public int PostedCount
        {
            get { return _postedCount; }
            set { SetPropertyValue("PostedCount", ref _postedCount, value); }
        }

        [Appearance("CashAdvanceMonitoringSignCodeEnabled", Enabled = false)]
        public string SignCode
        {
            get { return _signCode; }
            set { SetPropertyValue("SignCode", ref _signCode, value); }
        }

        #endregion Field
    }
}