﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.SystemModule;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Project Transaction")]
    [RuleCombinationOfPropertiesIsUnique("ProjectLine2ServiceRuleUnique", DefaultContexts.Save, "Code")]
    [ListViewFilter("AllDataProjectLineService2", "", "All Data", "All Data In Project Line Service2", 1, true)]
    [ListViewFilter("OpenProjectLineService2", "Status = 'Open'", "Open", "Open Data Status In Project Line Service2", 2, true)]
    [ListViewFilter("ProgressProjectLineService2", "Status = 'Progress'", "Progress", "Progress Data Status In Project Line Service2", 3, true)]
    [ListViewFilter("LockProjectLineService2", "Status = 'Lock'", "Lock", "Lock Data Status In Project Line Service2", 4, true)]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class ProjectLine2Service : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private bool _activationPosting;
        private int _no;
        private string _code;
        private string _name;
        private OrderType _orderType;
        private XPCollection<Item> _availableItem;
        private Item _item;
        private double _qty;
        private UnitOfMeasure _uom;
        private double _unitAmount;
        private double _totalUnitAmount;
        private Status _status;
        private DateTime _statusDate;
        private Company _company;
        private ProjectLine2 _projectLine2;
        private ProjectLine _projectLine;
        private ProjectHeader _projectHeader;
        private ProjectLineService _projectLineService;
        private GlobalFunction _globFunc;

        public ProjectLine2Service(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.ProjectLine2Service);
                DateTime now = DateTime.Now;
                this.Status = Status.Open;
                this.StatusDate = now;
                this.OrderType = OrderType.Service;
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        protected override void OnSaving()
        {
            base.OnSaving();
            if (!IsLoading && IsSaving)
            {
                UpdateNo();
            }
        }

        protected override void OnDeleting()
        {
            base.OnDeleting();
            RecoveryDeleteNo();
        }

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [Appearance("ProjectLine2ServiceNoClose", Enabled = false)]
        public int No
        {
            get { return _no; }
            set { SetPropertyValue("No", ref _no, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("ProjectLine2ServiceCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("ProjectLine2ServiceNameLock", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        [Browsable(false)]
        public OrderType OrderType
        {
            get { return _orderType; }
            set { SetPropertyValue("OrderType", ref _orderType, value); }
        }

        [Browsable(false)]
        public XPCollection<Item> AvailableItem
        {
            get
            {
                if (OrderType == OrderType.Service)
                {
                    _availableItem = new XPCollection<Item>(Session,
                                    new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("OrderType", OrderType.Service)));

                }
                return _availableItem;

            }
        }

        [DataSourceProperty("AvailableItem", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        [Appearance("ProjectLine2ServiceItemLock", Criteria = "ActivationPosting = true", Enabled = false)]
        public Item Item
        {
            get { return _item; }
            set { SetPropertyValue("Item", ref _item, value); }
        }

        [ImmediatePostData()]
        [Appearance("ProjectLine2ServiceQtyLock", Criteria = "ActivationPosting = true", Enabled = false)]
        public double Qty
        {
            get { return _qty; }
            set
            {
                SetPropertyValue("Qty", ref _qty, value);
                if (!IsLoading)
                {
                    SetTotalAmount();
                }
            }
        }

        [DataSourceCriteria("Active = true")]
        [ImmediatePostData()]
        [Appearance("ProjectLine2ServiceUOMLock", Criteria = "ActivationPosting = true", Enabled = false)]
        public UnitOfMeasure UOM
        {
            get { return _uom; }
            set
            {
                SetPropertyValue("UOM", ref _uom, value);
                if (!IsLoading)
                {
                    SetTotalAmount();
                }
            }
        }

        [ImmediatePostData()]
        [Appearance("ProjectLine2ServiceUnitAmountLock", Criteria = "ActivationPosting = true", Enabled = false)]
        public double UnitAmount
        {
            get { return _unitAmount; }
            set
            {
                SetPropertyValue("UnitAmount", ref _unitAmount, value);
                if (!IsLoading)
                {
                    _globFunc = new GlobalFunction();

                    if (this._unitAmount > 0)
                    {
                        if (_globFunc.GetRoundingList(Session, ObjectList.ProjectLine2Service, FieldName.UnitAmount) == true)
                        {
                            this._unitAmount = _globFunc.GetRoundUp(Session, this._unitAmount, ObjectList.ProjectLine2Service, FieldName.UnitAmount);
                        }
                    }

                    SetTotalAmount();
                }
            }
        }

        [Appearance("ProjectLine2ServiceTotalUnitAmountClose", Enabled = false)]
        public double TotalUnitAmount
        {
            get { return _totalUnitAmount; }
            set { SetPropertyValue("TotalUnitAmount", ref _totalUnitAmount, value); }
        }

        [Appearance("ProjectLine2ServiceStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("ProjectLine2ServiceStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("ProjectLine2ServiceCompanyLock", Criteria = "ActivationPosting = true", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("ProjectLine2ServiceProjectLine2Close", Enabled = false)]
        [Association("ProjectLine2-ProjectLine2Services")]
        public ProjectLine2 ProjectLine2
        {
            get { return _projectLine2; }
            set
            {
                SetPropertyValue("ProjectLine2", ref _projectLine2, value);
                if (!IsLoading)
                {
                    if (this._projectLine2 != null)
                    {
                        if (this._projectLine2.ProjectHeader != null)
                        {
                            this.ProjectHeader = this._projectLine2.ProjectHeader;
                        }
                    }
                }
            }
        }

        [Browsable(false)]
        public ProjectLine ProjectLine
        {
            get { return _projectLine; }
            set { SetPropertyValue("ProjectLine", ref _projectLine, value); }
        }

        [Browsable(false)]
        public ProjectHeader ProjectHeader
        {
            get { return _projectHeader; }
            set { SetPropertyValue("ProjectHeader", ref _projectHeader, value); }
        }

        [Browsable(false)]
        public ProjectLineService ProjectLineService
        {
            get { return _projectLineService; }
            set { SetPropertyValue("ProjectLineService", ref _projectLineService, value); }
        }

        //private string _PersistentProperty;
        //[XafDisplayName("My display name"), ToolTip("My hint message")]
        //[ModelDefault("EditMask", "(000)-00"), Index(0), VisibleInListView(false)]
        //[Persistent("DatabaseColumnName"), RuleRequiredField(DefaultContexts.Save)]
        //public string PersistentProperty {
        //    get { return _PersistentProperty; }
        //    set { SetPropertyValue("PersistentProperty", ref _PersistentProperty, value); }
        //}

        //[Action(Caption = "My UI Action", ConfirmationMessage = "Are you sure?", ImageName = "Attention", AutoCommit = true)]
        //public void ActionMethod() {
        //    // Trigger a custom business logic for the current record in the UI (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112619.aspx).
        //    this.PersistentProperty = "Paid";
        //}

        //================================================== Code Only ==================================================

        #region No

        public void UpdateNo()
        {
            try
            {
                if (!IsLoading && Session.IsNewObject(this))
                {
                    if (this.ProjectLine2 != null)
                    {
                        object _makRecord = Session.Evaluate<ProjectLine2Service>(CriteriaOperator.Parse("Max(No)"), CriteriaOperator.Parse("ProjectLine2=?", this.ProjectLine2));
                        this.No = Convert.ToInt32(_makRecord) + 1;
                        this.Save();
                        RecoveryUpdateNo();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ProjectLine2Service " + ex.ToString());
            }
        }

        public void RecoveryUpdateNo()
        {
            try
            {
                if (this.ProjectLine2 != null)
                {
                    ProjectLine2 _numHeader = Session.FindObject<ProjectLine2>
                                                (new BinaryOperator("Code", this.ProjectLine2.Code));

                    XPCollection<ProjectLine2Service> _numLines = new XPCollection<ProjectLine2Service>
                                                (Session, new BinaryOperator("ProjectLine2", _numHeader),
                                                new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i1 = 0;
                        foreach (ProjectLine2Service _numLine in _numLines)
                        {
                            i1 += 1;
                            _numLine.No = i1;
                            _numLine.Save();
                        }
                        i1 = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ProjectLine2Service " + ex.ToString());
            }
        }

        public void RecoveryDeleteNo()
        {
            try
            {
                if (this.ProjectLine2 != null)
                {
                    ProjectLine2 _numHeader = Session.FindObject<ProjectLine2>
                                                (new BinaryOperator("Code", this.ProjectLine2.Code));

                    XPCollection<ProjectLine2Service> _numLines = new XPCollection<ProjectLine2Service>
                                                (Session, new GroupOperator(GroupOperatorType.And,
                                                 new BinaryOperator("This", this, BinaryOperatorType.NotEqual),
                                                 new BinaryOperator("ProjectLine2", _numHeader)),
                                                 new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i = 0;
                        foreach (ProjectLine2Service _numLine in _numLines)
                        {
                            i += 1;
                            _numLine.No = i;
                            _numLine.Save();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ProjectLine2Service " + ex.ToString());
            }
        }

        #endregion No

        #region AmountMethod

        private void SetTotalAmount()
        {
            try
            {
                _globFunc = new GlobalFunction();

                if (_qty >= 0 && _unitAmount >= 0)
                {
                    if (_globFunc.GetRoundingList(Session, ObjectList.ProjectLine2Service, FieldName.TotalUnitAmount) == true)
                    {
                        this.TotalUnitAmount = _globFunc.GetRoundUp(Session, (this.Qty * this.UnitAmount), ObjectList.ProjectLine2Service, FieldName.TotalUnitAmount);
                    }
                    else
                    {
                        this.TotalUnitAmount = this.Qty * this.UnitAmount;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ProjectLine2Service " + ex.ToString());
            }
        }

        #endregion AmountMethod
    }
}