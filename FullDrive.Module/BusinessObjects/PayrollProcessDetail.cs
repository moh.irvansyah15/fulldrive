﻿        #region Default
using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using DevExpress.ExpressApp.Xpo;
using DevExpress.ExpressApp.ConditionalAppearance;
using System.Collections;
using FullDrive.Module.CustomProcess;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Payroll")]
    [RuleCombinationOfPropertiesIsUnique("PayrollProcessDetailRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class PayrollProcessDetail : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private string _code;
        private PayrollProcess _payrollProcess;
        private PayrollProcessDetail _payrollProcessDetail2;
        private string _detail;
        private SalaryComponent _component;
        private string _componentFormula;
        private double _componentValue;
        private string _componentValueText;
        private double _componentValueLCY;
        private bool _setAsConstant;
        private Currency _currency;
        private CurrencyRate _currencyRate;
        private string _periodYearPPD;
        private string _perioMonthPPD;
        private GlobalFunction _globFunc;

        public PayrollProcessDetail(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.PayrollProcessDetail);
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }
        #endregion

        #region OnChanged
        protected override void OnChanged(string propertyName, object oldValue, object newValue)
        {
            base.OnChanged(propertyName, oldValue, newValue);
            if (!IsLoading)
            {
                try
                {
                    switch (propertyName)
                    {
                        case "Component":
                            {
                                if ((newValue as SalaryComponent).SalaryType == SalaryType.BasicSalary)
                                    UpdateBaseSalary();
                                break;
                            }

                        case "ComponentFormula":
                            {
                                Calculate();
                                break;
                            }
                    }
                }
                catch (Exception ex)
                {
                    Tracing.Tracer.LogError("BusinessObject = PayrollProcessDetail"+ ex.Message.ToString());
                }
            }
        }

        #endregion

        #region OnSaving
        protected override void OnSaving()
        {
            base.OnSaving();
            UpdateBaseSalary();
            Calculate();
        }

        #endregion

        #region Field
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        public string Detail
        {
            get { return _detail; }
            set { SetPropertyValue("Detail", ref _detail, value); }
        }

        public SalaryComponent Component
        {
            get { return _component; }
            set { SetPropertyValue("Component", ref _component, value); }
        }

        public double ComponentValue
        {
            get { return _componentValue; }
            set { SetPropertyValue("ComponentValue", ref _componentValue, value); }
        }

        public string ComponentValueText
        {
            get { return _componentValueText; }
            set { SetPropertyValue("ComponentValueText", ref _componentValueText, value); }
        }

        public double ComponentValueLCY
        {
            get { return _componentValueLCY; }
            set { SetPropertyValue("ComponentValueLCY", ref _componentValueLCY, value); }
            // ComponentValue * CurrencyRate
        }

        public string ComponentFormula
        {
            get { return _componentFormula; }
            set { SetPropertyValue("ComponentFormula", ref _componentFormula, value); }
        }

        private bool SetAsConstant
        {
            get { return _setAsConstant; }
            set { SetPropertyValue("SetAsConstant", ref _setAsConstant, value); }
        }

        private Currency Currency
        {
            get { return _currency; }
            set { SetPropertyValue("Currency", ref _currency, value); }
        }

        private CurrencyRate CurrencyRate
        {
            get { return _currencyRate; }
            set { SetPropertyValue("CurrencyRate", ref _currencyRate, value); }
            
        }

        public string PeriodYearPPD
        { 
            get
            {
                string val1;
                if (_payrollProcess != null)
                {
                    if (_payrollProcess.Period != null)
                    {
                        if (_payrollProcess.Period.PeriodYear > 0)
                        {
                            val1 = _payrollProcess.Period.PeriodYear.ToString();
                            return val1;
                        }
                    }
                }
                return null;
        }
        //get { return _periodYearPPD; }
        //set { SetPropertyValue("PeriodYearPPD", ref _periodYearPPD, value); }
    }

    public string PeriodMonthPPD
        {
            get
            {
                string val2;
                if (_payrollProcess != null)
                {
                    if (_payrollProcess.Period != null)
                    {
                        if (_payrollProcess.Period.PeriodMonth > 0)
                        {
                            val2 = _payrollProcess.Period.PeriodMonth.ToString();
                            return val2;
                        }
                    }
                }
                return null;
            }
            //get { return _perioMonthPPD; }
            //set { SetPropertyValue("PeriodMonthPPD", ref _perioMonthPPD, value); }
        }

        [Association("PayrollProcess-PayrollProcessDetails")]
        public PayrollProcess PayrollProcess
        {
            get { return _payrollProcess; }
            set { SetPropertyValue("PayrollProcess", ref _payrollProcess, value); }
        }

        [Association("PayrollProcessDetail-PayrollProcessDetails")]
        public XPCollection<PayrollProcessDetail> PayrollProcessDetails
        {
            get { return GetCollection<PayrollProcessDetail>("PayrollProcessDetails"); }
        }

        [Association("PayrollProcessDetail-PayrollProcessDetails")]
        public PayrollProcessDetail PayrollProcessDetails2
        {
            get { return _payrollProcessDetail2; }
            set { SetPropertyValue("PayrollProcessDetails2", ref _payrollProcessDetail2, value); }

        }
        #endregion

        #region GetSalaryBasic
        public double  GetSalaryBasic()
        {
            double Result = 0;
            if (this.PayrollProcess != null)
                Result = this.PayrollProcess.SalaryBasic;
            else
                Result = this.PayrollProcessDetails2.GetSalaryBasic();
            return Result;
        }
        #endregion

        #region UpdateBaseSalary
        public void UpdateBaseSalary()
        {
            if (!IsLoading)
            {
                try
                {
                    if (this.Component.SalaryType == SalaryType.BasicSalary)
                        ComponentValue = this.GetSalaryBasic();
                }
                // Calculate()
                catch (Exception ex)
                {
                    Tracing.Tracer.LogError("BusinessObject = PayrollProcessDetail" + ex.ToString());
                }
            }
        }
        #endregion

        #region Calculate
        [Action(ToolTip = "Process", Caption = "Process")]
        public void Calculate()
        {
            try
            {
                // Debug.Print(String.Format("{0} {1} {2}", "PayrollProcessDetail", Component.Name, ComponentValue))
                UpdateBaseSalary();
                if (ComponentFormula != "" & this.PayrollProcessDetails.Count > 0)
                {
                    //var MyEval = new EvalFormula(PayrollProcessDetails);
                    //ComponentValue = MyEval.Parse(ComponentFormula);
                }
            }
            // Debug.Print(String.Format("{0} {1} {2}", "PayrollProcessDetail", Component.Type, ComponentValue))
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PayrollProcessDetail" + ex.ToString());
            }
        }
        #endregion 

        //
    }
}