﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.SystemModule;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Editors;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("AccountReclassJournalMonitoringRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class AccountReclassJournalMonitoring : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).

        private string _code;
        private AccountReclassJournal _accountReclassJournal;
        private AccountReclassJournalLine _accountReclassJournalLine;
        private ChartOfAccount _account;
        private double _totAmountDebit;
        private double _totAmountCredit;
        private Status _status;
        private DateTime _statusDate;
        private GlobalFunction _globFunc;

        public AccountReclassJournalMonitoring(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            _globFunc = new GlobalFunction();
            DateTime now = DateTime.Now;
            this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.AccountReclassJournalMonitoring);
            this.Status = Status.Open;
            this.StatusDate = now;
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("AccountReclassJournalMonitoringCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("AccountReclassJournalMonitoringAccountReclassJournalClose", Enabled = false)]
        public AccountReclassJournal AccountReclassJournal
        {
            get { return _accountReclassJournal; }
            set { SetPropertyValue("AccountReclassJournal", ref _accountReclassJournal, value); }
        }

        [Appearance("AccountReclassJournalMonitoringAccountReclassJournalLineClose", Enabled = false)]
        public AccountReclassJournalLine AccountReclassJournalLine
        {
            get { return _accountReclassJournalLine; }
            set { SetPropertyValue("AccountReclassJournalLine", ref _accountReclassJournalLine, value); }
        }

        
        [Appearance("AccountReclassJournalMonitoringAccountClose", Enabled = false)]
        public ChartOfAccount Account
        {
            get { return _account; }
            set { SetPropertyValue("Account", ref _account, value); }
        }

        [Appearance("AccountReclassJournalMonitoringTotAmountDebitClose", Enabled = false)]
        public double TotAmountDebit
        {
            get { return _totAmountDebit; }
            set { SetPropertyValue("TotAmountDebit", ref _totAmountDebit, value); }
        }

        [Appearance("AccountReclassJournalMonitoringTotAmountCreditClose", Enabled = false)]
        public double TotAmountCredit
        {
            get { return _totAmountCredit; }
            set { SetPropertyValue("TotAmountCredit", ref _totAmountCredit, value); }
        }

        [Appearance("AccountReclassJournalMonitoringStatusEnabled", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("AccountReclassJournalMonitoringStatusDateEnabled", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        #endregion Field
    }
}