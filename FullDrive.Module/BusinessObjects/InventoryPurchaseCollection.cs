﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Inventory")]
    [RuleCombinationOfPropertiesIsUnique("InventoryPurchaseCollectionRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class InventoryPurchaseCollection : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private bool _activationPosting;
        private string _code;
        private XPCollection<PurchaseOrder> _availablePurchaseOrder;
        private PurchaseOrder _purchaseOrder;
        private XPCollection<InventoryTransferOrder> _availableInventoryTransferOrder;
        private InventoryTransferOrder _inventoryTransferOrder;
        private XPCollection<PurchaseReturn> _availablePurchaseReturn;
        private PurchaseReturn _purchaseReturn;
        private InventoryTransferIn _inventoryTransferIn;
        private InventoryTransferOut _inventoryTransferOut;
        private Status _status;
        private DateTime _statusDate;
        private int _postedCount;
        private GlobalFunction _globFunc;

        public InventoryPurchaseCollection(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.InventoryPurchaseCollection);
                DateTime now = DateTime.Now;
                this.Status = Status.Open;
                this.StatusDate = now;
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("InventoryPurchaseCollectionCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Browsable(false)]
        public XPCollection<PurchaseOrder> AvailablePurchaseOrder
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;

                if (!IsLoading)
                {
                    if (this.InventoryTransferIn != null)
                    {
                        if (this.InventoryTransferIn.BusinessPartner != null)
                        {
                            XPQuery<PurchaseOrderMonitoring> _purchaseOrderMonitoringsQuery = new XPQuery<PurchaseOrderMonitoring>(Session);

                            var _purchaseOrderMonitorings = from pom in _purchaseOrderMonitoringsQuery
                                                            where (pom.Status == Status.Open && pom.PostedCount == 0
                                                            && pom.PurchaseOrder.BuyFromVendor == this.InventoryTransferIn.BusinessPartner
                                                            && pom.InventoryTransferIn == null)
                                                            group pom by pom.PurchaseOrder into g
                                                            select new { PurchaseOrder = g.Key };

                            if (_purchaseOrderMonitorings != null && _purchaseOrderMonitorings.Count() > 0)
                            {
                                List<string> _stringPOM = new List<string>();

                                foreach (var _purchaseOrderMonitoring in _purchaseOrderMonitorings)
                                {
                                    if (_purchaseOrderMonitoring != null)
                                    {
                                        _stringPOM.Add(_purchaseOrderMonitoring.PurchaseOrder.Code);
                                    }
                                }

                                IEnumerable<string> _stringArrayPOMDistinct = _stringPOM.Distinct();
                                string[] _stringArrayPOMList = _stringArrayPOMDistinct.ToArray();
                                if (_stringArrayPOMList.Length == 1)
                                {
                                    for (int i = 0; i < _stringArrayPOMList.Length; i++)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _stringArrayPOMList[i] + "'";
                                        }
                                    }
                                }
                                else if (_stringArrayPOMList.Length > 1)
                                {
                                    for (int i = 0; i < _stringArrayPOMList.Length; i++)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _stringArrayPOMList[i] + "'";
                                        }
                                        else
                                        {
                                            _endString = _endString + " OR [Code]=='" + _stringArrayPOMList[i] + "'";
                                        }
                                    }
                                }
                                _fullString = _beginString + _endString;

                                if (_fullString != null)
                                {
                                    _availablePurchaseOrder = new XPCollection<PurchaseOrder>(Session, CriteriaOperator.Parse(_fullString));
                                }
                            }
                        }
                    }
                }

                return _availablePurchaseOrder;
            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailablePurchaseOrder", DataSourcePropertyIsNullMode.SelectNothing)]
        public PurchaseOrder PurchaseOrder
        {
            get { return _purchaseOrder; }
            set { SetPropertyValue("PurchaseOrder", ref _purchaseOrder, value); }
        }

        public XPCollection<InventoryTransferOrder> AvailableInventoryTransferOrder
        {
            get
            {
                if (!IsLoading)
                {
                    if (this.InventoryTransferIn != null)
                    {
                        if (InventoryTransferIn.BusinessPartner != null)
                        {
                            XPCollection<InventoryTransferOrder> _locCollectionITOs = new XPCollection<InventoryTransferOrder>
                                                                            (Session, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("ActiveApproved3", true),
                                                                            new BinaryOperator("BusinessPartner", InventoryTransferIn.BusinessPartner),
                                                                            new GroupOperator(GroupOperatorType.Or,
                                                                            new BinaryOperator("Status", Status.Progress),
                                                                            new BinaryOperator("Status", Status.Posted)))
                                                                            );

                            if (_locCollectionITOs != null && _locCollectionITOs.Count() > 0)
                            {
                                _availableInventoryTransferOrder = _locCollectionITOs;
                            }
                        }
                    }
                }
                return _availableInventoryTransferOrder;
            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableInventoryTransferOrder", DataSourcePropertyIsNullMode.SelectNothing)]
        public InventoryTransferOrder InventoryTransferOrder
        {
            get { return _inventoryTransferOrder; }
            set { SetPropertyValue("InventoryTransferOrder", ref _inventoryTransferOrder, value); }
        }

        [Browsable(false)]
        public XPCollection<PurchaseReturn> AvailablePurchaseReturn
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;

                if (!IsLoading)
                {
                    if (this.InventoryTransferOut != null)
                    {
                        if (this.InventoryTransferOut.BusinessPartner != null)
                        {
                            XPQuery<PurchaseReturnMonitoring> _purchaseReturnMonitoringsQuery = new XPQuery<PurchaseReturnMonitoring>(Session);

                            var _purchaseReturnMonitorings = from prm in _purchaseReturnMonitoringsQuery
                                                          where (prm.InventoryStatus != Status.Close
                                                          && prm.PurchaseReturn.BuyFromVendor == this.InventoryTransferOut.BusinessPartner
                                                          && prm.InventoryTransferOut == null
                                                          && (prm.ReturnType == ReturnType.ReplacedFull || prm.ReturnType == ReturnType.CreditMemo))
                                                          group prm by prm.PurchaseReturn into g
                                                          select new { PurchaseReturn = g.Key };

                            if (_purchaseReturnMonitorings != null && _purchaseReturnMonitorings.Count() > 0)
                            {
                                List<string> _stringPRM = new List<string>();

                                foreach (var _purchaseReturnMonitoring in _purchaseReturnMonitorings)
                                {
                                    if (_purchaseReturnMonitoring != null)
                                    {
                                        if (_purchaseReturnMonitoring.PurchaseReturn.Code != null)
                                        {
                                            _stringPRM.Add(_purchaseReturnMonitoring.PurchaseReturn.Code);
                                        }
                                    }
                                }

                                IEnumerable<string> _stringArrayPRMDistinct = _stringPRM.Distinct();
                                string[] _stringArrayPRMList = _stringArrayPRMDistinct.ToArray();
                                if (_stringArrayPRMList.Length == 1)
                                {
                                    for (int i = 0; i < _stringArrayPRMList.Length; i++)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _stringArrayPRMList[i] + "'";
                                        }
                                    }
                                }
                                else if (_stringArrayPRMList.Length > 1)
                                {
                                    for (int i = 0; i < _stringArrayPRMList.Length; i++)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _stringArrayPRMList[i] + "'";
                                        }
                                        else
                                        {
                                            _endString = _endString + " OR [Code]=='" + _stringArrayPRMList[i] + "'";
                                        }
                                    }
                                }
                                _fullString = _beginString + _endString;

                                if (_fullString != null)
                                {
                                    _availablePurchaseReturn = new XPCollection<PurchaseReturn>(Session, CriteriaOperator.Parse(_fullString));
                                }
                            }
                        }
                    }
                }

                return _availablePurchaseReturn;
            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailablePurchaseReturn", DataSourcePropertyIsNullMode.SelectNothing)]
        public PurchaseReturn PurchaseReturn
        {
            get { return _purchaseReturn; }
            set { SetPropertyValue("PurchaseReturn", ref _purchaseReturn, value); }
        }

        [ImmediatePostData()]
        [Appearance("InventoryPurchaseCollectionInventoryTransferInClose", Enabled = false)]
        [Association("InventoryTransferIn-InventoryPurchaseCollections")]
        public InventoryTransferIn InventoryTransferIn
        {
            get { return _inventoryTransferIn; }
            set { SetPropertyValue("InventoryTransferIn", ref _inventoryTransferIn, value); }
        }

        [Appearance("InventoryPurchaseCollectionInventoryTransferOutClose", Enabled = false)]
        [Association("InventoryTransferOut-InventoryPurchaseCollections")]
        public InventoryTransferOut InventoryTransferOut
        {
            get { return _inventoryTransferOut; }
            set { SetPropertyValue("InventoryTransferOut", ref _inventoryTransferOut, value); }
        }

        [Appearance("InventoryPurchaseCollectionStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("InventoryPurchaseCollectionStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("InventoryPurchaseCollectionPostedCountEnabled", Enabled = false)]
        public int PostedCount
        {
            get { return _postedCount; }
            set { SetPropertyValue("PostedCount", ref _postedCount, value); }
        }

        //private string _PersistentProperty;
        //[XafDisplayName("My display name"), ToolTip("My hint message")]
        //[ModelDefault("EditMask", "(000)-00"), Index(0), VisibleInListView(false)]
        //[Persistent("DatabaseColumnName"), RuleRequiredField(DefaultContexts.Save)]
        //public string PersistentProperty {
        //    get { return _PersistentProperty; }
        //    set { SetPropertyValue("PersistentProperty", ref _PersistentProperty, value); }
        //}

        //[Action(Caption = "My UI Action", ConfirmationMessage = "Are you sure?", ImageName = "Attention", AutoCommit = true)]
        //public void ActionMethod() {
        //    // Trigger a custom business logic for the current record in the UI (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112619.aspx).
        //    this.PersistentProperty = "Paid";
        //}
    }
}