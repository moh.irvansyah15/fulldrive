﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("ReceivableTransactionRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class ReceivableTransaction : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private bool _activationPosting;
        private string _code;
        #region Sales
        private BusinessPartner _salesToCustomer;
        private string _salesToContact;
        private Country _salesToCountry;
        private City _salesToCity;
        private string _salesToAddress;
        #endregion Sales
        #region Bill
        private BusinessPartner _billToCustomer;
        private string _billToContact;
        private Country _billToCountry;
        private City _billToCity;
        private string _billToAddress;
        #endregion Bill
        private string _description;
        private Currency _currency;
        private PostingType _salesType;
        private PostingMethod _postingMethod;
        private PriceGroup _priceGroup;
        private PaymentMethodType _paymentMethodType;
        private PaymentMethod _paymentMethod;
        private PaymentType _paymentType;
        private DateTime _estimatedDate;
        private DateTime _actualDate;
        private Status _status;
        private DateTime _statusDate;
        private string _signCode;
        private string _userAccess;
        private Company _companyDefault;
        private GlobalFunction _globFunc;

        public ReceivableTransaction(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                DateTime now = DateTime.Now;
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.ReceivableTransaction);
                this.Status = Status.Open;
                this.StatusDate = now;
                this.SalesType = PostingType.Sales;
                this.PostingMethod = PostingMethod.Bill;

                _userAccess = SecuritySystem.CurrentUserName;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                if (Session.IsNewObject(this))
                {
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null)
                            {
                                CompanyDefault = _locUserAccess.Employee.Company;
                            }
                        }
                    }
                }

                this.Currency = _globFunc.GetDefaultCurrency(this.Session);
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("ReceivableTransactionCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        #region SalesToCostumer

        [ImmediatePostData]
        [DataSourceCriteria("Active = true")]
        [Appearance("ReceivableTransactionSalesToCustomerClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BusinessPartner SalesToCustomer
        {
            get { return _salesToCustomer; }
            set
            {
                SetPropertyValue("SalesToCustomer", ref _salesToCustomer, value);
                if (!IsLoading)
                {
                    if (_salesToCustomer != null)
                    {
                        this.SalesToContact = _salesToCustomer.Contact;
                        this.SalesToCountry = _salesToCustomer.Country;
                        this.SalesToCity = _salesToCustomer.City;
                        this.SalesToAddress = this.SalesToAddress;
                        this.BillToCustomer = _salesToCustomer;
                    }
                }
            }
        }

        [Appearance("ReceivableTransactionSalesToContactClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string SalesToContact
        {
            get { return _salesToContact; }
            set { SetPropertyValue("SalesToContact", ref _salesToContact, value); }
        }

        [DataSourceCriteria("Active = true")]
        [Appearance("ReceivableTransactionSalesCountryClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Country SalesToCountry
        {
            get { return _salesToCountry; }
            set { SetPropertyValue("SalesToCountry", ref _salesToCountry, value); }
        }

        [DataSourceCriteria("Active = true")]
        [Appearance("ReceivableTransactionSalesToCityClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public City SalesToCity
        {
            get { return _salesToCity; }
            set { SetPropertyValue("SalesToCity", ref _salesToCity, value); }
        }

        [Appearance("ReceivableTransactionSalesToAddressClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Size(512)]
        public string SalesToAddress
        {
            get { return _salesToAddress; }
            set { SetPropertyValue("SalesToAddress", ref _salesToAddress, value); }
        }

        #endregion SalesToCostumer

        #region BillToCostumer

        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        [Appearance("ReceivableTransactionBillToCostumerClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BusinessPartner BillToCustomer
        {
            get { return _billToCustomer; }
            set
            {
                SetPropertyValue("BillToCustomer", ref _billToCustomer, value);
                if (!IsLoading)
                {
                    if (this._billToCustomer != null)
                    {
                        this.BillToContact = this._billToCustomer.Contact;
                        this.BillToAddress = this._billToCustomer.Address;
                        this.BillToCountry = this._billToCustomer.Country;
                        this.BillToCity = this._billToCustomer.City;
                    }
                }
            }
        }

        [Appearance("ReceivableTransactionBillToContactClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string BillToContact
        {
            get { return _billToContact; }
            set { SetPropertyValue("BillToContact", ref _billToContact, value); }
        }

        [DataSourceCriteria("Active = true")]
        [Appearance("ReceivableTransactionBillCountryClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Country BillToCountry
        {
            get { return _billToCountry; }
            set { SetPropertyValue("BillToCountry", ref _billToCountry, value); }
        }

        [DataSourceCriteria("Active = true")]
        [Appearance("ReceivableTransactionBillToCityClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public City BillToCity
        {
            get { return _billToCity; }
            set { SetPropertyValue("BillToCity", ref _billToCity, value); }
        }

        [Appearance("ReceivableTransactionBillToAddressClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Size(512)]
        public string BillToAddress
        {
            get { return _billToAddress; }
            set { SetPropertyValue("BillToAddress", ref _billToAddress, value); }
        }

        #endregion BillToCostumer

        [Size(512)]
        [Appearance("ReceivableTransactionDescriptionClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        [Appearance("ReceivableTransactionCurrencyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Currency Currency
        {
            get { return _currency; }
            set { SetPropertyValue("Currency", ref _currency, value); }
        }

        [Appearance("ReceivableTransactionPostingTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PostingType SalesType
        {
            get { return _salesType; }
            set { SetPropertyValue("PostingType", ref _salesType, value); }
        }

        [Appearance("ReceivableTransactionPostingMethodClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PostingMethod PostingMethod
        {
            get { return _postingMethod; }
            set { SetPropertyValue("PostingMethod", ref _postingMethod, value); }
        }

        [ImmediatePostData()]
        [Appearance("ReceivableTransactionPriceGroupClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PriceGroup PriceGroup
        {
            get { return _priceGroup; }
            set { SetPropertyValue("PriceGroup", ref _priceGroup, value); }
        }

        [Appearance("ReceivableTransactionPaymentMethodTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PaymentMethodType PaymentMethodType
        {
            get { return _paymentMethodType; }
            set { SetPropertyValue("PaymentMethodType", ref _paymentMethodType, value); }
        }

        [Appearance("ReceivableTransactionPaymentMethodClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PaymentMethod PaymentMethod
        {
            get { return _paymentMethod; }
            set { SetPropertyValue("PaymentMethod", ref _paymentMethod, value); }
        }

        [Appearance("ReceivableTransactionPaymentTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PaymentType PaymentType
        {
            get { return _paymentType; }
            set { SetPropertyValue("PaymentType", ref _paymentType, value); }
        }

        [Appearance("ReceivableTransactionEstimatedDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime EstimatedDate
        {
            get { return _estimatedDate; }
            set { SetPropertyValue("EstimatedDate", ref _estimatedDate, value); }
        }

        [Appearance("ReceivableTransactionActualDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime ActualDate
        {
            get { return _actualDate; }
            set { SetPropertyValue("ActualDate", ref _actualDate, value); }
        }

        //[Appearance("ReceivableTransactionStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("ReceivableTransactionStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("ReceivableTransactionSignCodeClose", Enabled = false)]
        public string SignCode
        {
            get { return _signCode; }
            set { SetPropertyValue("SignCode", ref _signCode, value); }
        }

        [Appearance("ReceivableTransactionCompanyDefaultClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Company CompanyDefault
        {
            get { return _companyDefault; }
            set { SetPropertyValue("CompanyDefault", ref _companyDefault, value); }
        }

        [Association("ReceivableTransaction-ReceivableTransactionLines")]
        public XPCollection<ReceivableTransactionLine> ReceivableTransactionLines
        {
            get { return GetCollection<ReceivableTransactionLine>("ReceivableTransactionLines"); }
        }

        [Association("ReceivableTransaction-PaymentInPlans")]
        public XPCollection<PaymentInPlan> PaymentInPlans
        {
            get { return GetCollection<PaymentInPlan>("PaymentInPlans"); }
        }

        [Association("ReceivableTransaction-ReceivableSalesCollections")]
        public XPCollection<ReceivableSalesCollection> ReceivableSalesCollections
        {
            get { return GetCollection<ReceivableSalesCollection>("ReceivableSalesCollections"); }
        }

    }
}