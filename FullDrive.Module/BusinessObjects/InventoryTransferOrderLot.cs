﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.SystemModule;
using FullDrive.Module.CustomProcess;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Inventory")]
    [RuleCombinationOfPropertiesIsUnique("InventoryTransferOrderLotRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class InventoryTransferOrderLot : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private bool _activationPosting;
        private int _no;
        private string _code;
        private bool _select;
        private string _name;
        private Item _item;
        private string _description;
        #region InisialisasiLocation
        private XPCollection<Location> _availableLocationFrom;
        private Location _locationFrom;
        private XPCollection<BinLocation> _availableBinLocationFrom;
        private BinLocation _binLocationFrom;
        private StockType _stockTypeFrom;
        private XPCollection<Location> _availableLocationTo;
        private XPCollection<BinLocation> _availableBinLocationTo;
        private Location _locationTo;
        private BinLocation _binLocationTo;
        private StockType _stockTypeTo;
        #endregion InisialisasiLocation
        private XPCollection<BeginingInventoryLine> _availableBeginingInventoryLine;
        private BeginingInventoryLine _begInvLine;
        private string _lotNumber;
        private XPCollection<UnitOfMeasure> _availableUnitOfMeasure;
        #region InisialisasiNewDefaultQty
        private UnitOfMeasure _unitPack;
        private double _dQty;
        private UnitOfMeasure _dUom;
        private double _qty;
        private UnitOfMeasure _uom;
        private double _tQty;
        #endregion InisialisasiNewDefaultQty
        private Status _status;
        private DateTime _statusDate;
        private InventoryTransferOrderLine _inventoryTransferOrderLine;
        private Company _company;
        private string _userAccess;
        private GlobalFunction _globFunc;

        public InventoryTransferOrderLot(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            _globFunc = new GlobalFunction();
            DateTime now = DateTime.Now;
            this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.InventoryTransferOrderLot);
            this.Status = Status.Open;
            this.StatusDate = now;
            this.StockTypeFrom = StockType.Good;
            this.Select = true;
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        protected override void OnSaving()
        {
            base.OnSaving();
            if (!IsLoading && IsSaving)
            {
                UpdateNo();
            }
        }

        protected override void OnDeleting()
        {
            base.OnDeleting();
            if (!IsLoading)
            {
                RecoveryDeleteNo();
            }
        }

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [Appearance("InventoryTransferOrderLotNoEnabled", Enabled = false)]
        public int No
        {
            get { return _no; }
            set { SetPropertyValue("No", ref _no, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("InventoryTransferOrderLotCodeEnabled", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("InventoryTransferOrderLotSelectClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public bool Select
        {
            get { return _select; }
            set { SetPropertyValue("Select", ref _select, value); }
        }

        [Appearance("InventoryTransferOrderLotNameClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        [ImmediatePostData()]
        [Appearance("InventoryTransferOrderLotItemClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Item Item
        {
            get { return _item; }
            set
            {
                SetPropertyValue("Item", ref _item, value);
                if (!IsLoading)
                {
                    if (this._item != null)
                    {
                        this.DUOM = this._item.BasedUOM;
                    }
                }
            }
        }

        [Appearance("InventoryTransferOrderLotDescriptionClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Size(512)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        #region LocationFrom

        [Browsable(false)]
        public XPCollection<Location> AvailableLocationFrom
        {
            get
            {
                _userAccess = SecuritySystem.CurrentUserName;
                string _beginString = null;
                string _endString = null;
                string _fullString = null;
                string _locLocationCode = null;
                DirectionType _locTransferType = DirectionType.None;
                InventoryMovingType _locInventoryMovingType = InventoryMovingType.None;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));

                if (_locUserAccess != null)
                {
                    if (this.InventoryTransferOrderLine != null)
                    {
                        if (InventoryTransferOrderLine.InventoryTransferOrder != null)
                        {
                            if (InventoryTransferOrderLine.InventoryTransferOrder.TransferType != DirectionType.None)
                            {
                                _locTransferType = InventoryTransferOrderLine.InventoryTransferOrder.TransferType;
                            }

                            if (InventoryTransferOrderLine.InventoryTransferOrder.InventoryMovingType != InventoryMovingType.None)
                            {
                                _locInventoryMovingType = InventoryTransferOrderLine.InventoryTransferOrder.InventoryMovingType;
                            }
                        }
                        List<string> _stringLocation = new List<string>();

                        XPCollection<WarehouseSetupDetail> _locWhsSetupDetails = new XPCollection<WarehouseSetupDetail>
                                                                                  (Session, new GroupOperator(GroupOperatorType.And,
                                                                                   new BinaryOperator("UserAccess", _locUserAccess),
                                                                                   new BinaryOperator("ObjectList", ObjectList.InventoryTransferOrder),
                                                                                   new BinaryOperator("Owner", true),
                                                                                   new BinaryOperator("TransferType", _locTransferType),
                                                                                   new BinaryOperator("InventoryMovingType", _locInventoryMovingType),
                                                                                   new BinaryOperator("LocationType", LocationType.Main),
                                                                                   new BinaryOperator("Active", true)));

                        if (_locWhsSetupDetails != null && _locWhsSetupDetails.Count() > 0)
                        {
                            foreach (WarehouseSetupDetail _locWhsSetupDetail in _locWhsSetupDetails)
                            {
                                if (_locWhsSetupDetail.Location != null)
                                {
                                    if (_locWhsSetupDetail.Location.Code != null)
                                    {
                                        _locLocationCode = _locWhsSetupDetail.Location.Code;
                                        _stringLocation.Add(_locLocationCode);
                                    }
                                }

                            }
                        }

                        IEnumerable<string> _stringArrayLocationFromDistinct = _stringLocation.Distinct();
                        string[] _stringArrayLocationFromList = _stringArrayLocationFromDistinct.ToArray();
                        if (_stringArrayLocationFromList.Length == 1)
                        {
                            for (int i = 0; i < _stringArrayLocationFromList.Length; i++)
                            {
                                Location _locLocationFrom = Session.FindObject<Location>(new BinaryOperator("Code", _stringArrayLocationFromList[i]));
                                if (_locLocationFrom != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString = "[Code]=='" + _locLocationFrom.Code + "'";
                                    }
                                }
                            }
                        }
                        else if (_stringArrayLocationFromList.Length > 1)
                        {
                            for (int i = 0; i < _stringArrayLocationFromList.Length; i++)
                            {
                                Location _locLocationFrom = Session.FindObject<Location>(new BinaryOperator("Code", _stringArrayLocationFromList[i]));
                                if (_locLocationFrom != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString = "[Code]=='" + _locLocationFrom.Code + "'";
                                    }
                                    else
                                    {
                                        _endString = " OR [Code]=='" + _locLocationFrom.Code + "'";
                                    }
                                }
                            }
                        }
                        _fullString = _beginString + _endString;

                        if (_fullString != null)
                        {
                            _availableLocationFrom = new XPCollection<Location>(Session, CriteriaOperator.Parse(_fullString));
                        }

                    }
                }
                else
                {
                    _availableLocationFrom = new XPCollection<Location>(Session);
                }

                return _availableLocationFrom;

            }
        }

        [Appearance("InventoryTransferOrderLotLocationFromClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceProperty("AvailableLocationFrom", DataSourcePropertyIsNullMode.SelectAll)]
        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public Location LocationFrom
        {
            get { return _locationFrom; }
            set { SetPropertyValue("LocationFrom", ref _locationFrom, value); }
        }

        [Browsable(false)]
        public XPCollection<BinLocation> AvailableBinLocationFrom
        {
            get
            {
                _userAccess = SecuritySystem.CurrentUserName;
                string _beginString = null;
                string _endString = null;
                string _fullString = null;
                string _locBinLocationCode = null;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                if (_locUserAccess != null)
                {
                    if (this.LocationFrom != null)
                    {
                        if (this.InventoryTransferOrderLine != null)
                        {
                            List<string> _stringBinLocation = new List<string>();

                            XPCollection<WarehouseSetupDetail> _locWhsSetupDetails = new XPCollection<WarehouseSetupDetail>
                                                                                      (Session, new GroupOperator(GroupOperatorType.And,
                                                                                       new BinaryOperator("UserAccess", _locUserAccess),
                                                                                       new BinaryOperator("Location", this.LocationFrom),
                                                                                       new BinaryOperator("Owner", true),
                                                                                       new BinaryOperator("Active", true)));

                            if (_locWhsSetupDetails != null && _locWhsSetupDetails.Count() > 0)
                            {
                                foreach (WarehouseSetupDetail _locWhsSetupDetail in _locWhsSetupDetails)
                                {
                                    if (_locWhsSetupDetail.BinLocation != null)
                                    {
                                        if (_locWhsSetupDetail.BinLocation.Code != null)
                                        {
                                            _locBinLocationCode = _locWhsSetupDetail.BinLocation.Code;
                                            _stringBinLocation.Add(_locBinLocationCode);
                                        }
                                    }

                                }
                            }

                            IEnumerable<string> _stringArrayBinLocationFromDistinct = _stringBinLocation.Distinct();
                            string[] _stringArrayBinLocationFromList = _stringArrayBinLocationFromDistinct.ToArray();
                            if (_stringArrayBinLocationFromList.Length == 1)
                            {
                                for (int i = 0; i < _stringArrayBinLocationFromList.Length; i++)
                                {
                                    BinLocation _locBinLocationFrom = Session.FindObject<BinLocation>(new BinaryOperator("Code", _stringArrayBinLocationFromList[i]));
                                    if (_locBinLocationFrom != null)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _locBinLocationFrom.Code + "'";
                                        }
                                    }
                                }
                            }
                            else if (_stringArrayBinLocationFromList.Length > 1)
                            {
                                for (int i = 0; i < _stringArrayBinLocationFromList.Length; i++)
                                {
                                    BinLocation _locBinLocationFrom = Session.FindObject<BinLocation>(new BinaryOperator("Code", _stringArrayBinLocationFromList[i]));
                                    if (_locBinLocationFrom != null)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _locBinLocationFrom.Code + "'";
                                        }
                                        else
                                        {
                                            _endString = " OR [Code]=='" + _locBinLocationFrom.Code + "'";
                                        }
                                    }
                                }
                            }
                            _fullString = _beginString + _endString;

                            if (_fullString != null)
                            {
                                _availableBinLocationFrom = new XPCollection<BinLocation>(Session, CriteriaOperator.Parse(_fullString));
                            }
                        }
                    }
                }
                else
                {
                    _availableBinLocationFrom = new XPCollection<BinLocation>(Session);
                }

                return _availableBinLocationFrom;

            }
        }

        [Appearance("InventoryTransferOrderLotBinLocationFromClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceProperty("AvailableBinLocationFrom", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        public BinLocation BinLocationFrom
        {
            get { return _binLocationFrom; }
            set { SetPropertyValue("BinLocationFrom", ref _binLocationFrom, value); }
        }

        [Appearance("InventoryTransferOrderLotStockTypeFromClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public StockType StockTypeFrom
        {
            get { return _stockTypeFrom; }
            set { SetPropertyValue("StockTypeFrom", ref _stockTypeFrom, value); }
        }

        #endregion LocationFrom

        #region LocationTo

        [Browsable(false)]
        public XPCollection<Location> AvailableLocationTo
        {
            get
            {
                _userAccess = SecuritySystem.CurrentUserName;
                string _beginString = null;
                string _endString = null;
                string _fullString = null;
                string _locLocationCode = null;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                List<string> _stringLocation = new List<string>();
                if (_locUserAccess != null)
                {
                    if (this.InventoryTransferOrderLine != null)
                    {
                        if (this.InventoryTransferOrderLine.InventoryTransferOrder != null)
                        {
                            #region TransferInLine
                            if (this.InventoryTransferOrderLine.InventoryTransferOrder.InventoryMovingType != InventoryMovingType.None)
                            {
                                XPCollection<WarehouseSetupDetail> _locWhsSetupDetails = new XPCollection<WarehouseSetupDetail>
                                                                                      (Session, new GroupOperator(GroupOperatorType.And,
                                                                                       new BinaryOperator("UserAccess", _locUserAccess),
                                                                                       new BinaryOperator("Owner", true),
                                                                                       new BinaryOperator("Active", true)));

                                if (_locWhsSetupDetails != null && _locWhsSetupDetails.Count() > 0)
                                {
                                    foreach (WarehouseSetupDetail _locWhsSetupDetail in _locWhsSetupDetails)
                                    {
                                        if (_locWhsSetupDetail.Location != null)
                                        {
                                            if (_locWhsSetupDetail.Location.Code != null)
                                            {
                                                _locLocationCode = _locWhsSetupDetail.Location.Code;
                                                _stringLocation.Add(_locLocationCode);
                                            }
                                        }
                                    }
                                }

                                IEnumerable<string> _stringArrayLocationToDistinct = _stringLocation.Distinct();
                                string[] _stringArrayLocationToList = _stringArrayLocationToDistinct.ToArray();
                                if (_stringArrayLocationToList.Length == 1)
                                {
                                    for (int i = 0; i < _stringArrayLocationToList.Length; i++)
                                    {
                                        Location _locLocationTo = Session.FindObject<Location>(new BinaryOperator("Code", _stringArrayLocationToList[i]));
                                        if (_locLocationTo != null)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _locLocationTo.Code + "'";
                                            }
                                        }
                                    }
                                }
                                else if (_stringArrayLocationToList.Length > 1)
                                {
                                    for (int i = 0; i < _stringArrayLocationToList.Length; i++)
                                    {
                                        Location _locLocationTo = Session.FindObject<Location>(new BinaryOperator("Code", _stringArrayLocationToList[i]));
                                        if (_locLocationTo != null)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _locLocationTo.Code + "'";
                                            }
                                            else
                                            {
                                                _endString = " OR [Code]=='" + _locLocationTo.Code + "'";
                                            }
                                        }
                                    }
                                }
                                _fullString = _beginString + _endString;

                                if (_fullString != null)
                                {
                                    _availableLocationTo = new XPCollection<Location>(Session, CriteriaOperator.Parse(_fullString));
                                }
                            }
                            #endregion TransferInLine
                        }
                    }
                }
                else
                {
                    _availableLocationTo = new XPCollection<Location>(Session);
                }

                return _availableLocationTo;

            }
        }

        [Appearance("InventoryTransferOrderLotLocationToClose", Enabled = false)]
        [DataSourceProperty("AvailableLocationTo", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        [ImmediatePostData]
        public Location LocationTo
        {
            get { return _locationTo; }
            set { SetPropertyValue("LocationTo", ref _locationTo, value); }
        }

        [Browsable(false)]
        public XPCollection<BinLocation> AvailableBinLocationTo
        {
            get
            {
                _userAccess = SecuritySystem.CurrentUserName;
                string _beginString = null;
                string _endString = null;
                string _fullString = null;
                string _locBinLocationCode = null;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                List<string> _stringBinLocation = new List<string>();
                if (_locUserAccess != null)
                {
                    if (this.LocationTo != null)
                    {
                        if (this.InventoryTransferOrderLine != null)
                        {
                            if (this.InventoryTransferOrderLine.InventoryTransferOrder != null)
                            {
                                #region TransferIn
                                if (this.InventoryTransferOrderLine.InventoryTransferOrder.InventoryMovingType == InventoryMovingType.TransferIn)
                                {
                                    XPCollection<WarehouseSetupDetail> _locWhsSetupDetails = new XPCollection<WarehouseSetupDetail>
                                                                                      (Session, new GroupOperator(GroupOperatorType.And,
                                                                                       new BinaryOperator("UserAccess", _locUserAccess),
                                                                                       new BinaryOperator("Location", this.LocationTo),
                                                                                       new BinaryOperator("Owner", true),
                                                                                       new BinaryOperator("Active", true)));

                                    if (_locWhsSetupDetails != null && _locWhsSetupDetails.Count() > 0)
                                    {
                                        foreach (WarehouseSetupDetail _locWhsSetupDetail in _locWhsSetupDetails)
                                        {
                                            if (_locWhsSetupDetail.BinLocation != null)
                                            {
                                                if (_locWhsSetupDetail.BinLocation.Code != null)
                                                {
                                                    _locBinLocationCode = _locWhsSetupDetail.BinLocation.Code;
                                                    _stringBinLocation.Add(_locBinLocationCode);
                                                }
                                            }
                                        }
                                    }

                                    IEnumerable<string> _stringArrayBinLocationToDistinct = _stringBinLocation.Distinct();
                                    string[] _stringArrayBinLocationToList = _stringArrayBinLocationToDistinct.ToArray();
                                    if (_stringArrayBinLocationToList.Length == 1)
                                    {
                                        for (int i = 0; i < _stringArrayBinLocationToList.Length; i++)
                                        {
                                            BinLocation _locBinLocationTo = Session.FindObject<BinLocation>(new BinaryOperator("Code", _stringArrayBinLocationToList[i]));
                                            if (_locBinLocationTo != null)
                                            {
                                                if (i == 0)
                                                {
                                                    _beginString = "[Code]=='" + _locBinLocationTo.Code + "'";
                                                }
                                            }
                                        }
                                    }
                                    else if (_stringArrayBinLocationToList.Length > 1)
                                    {
                                        for (int i = 0; i < _stringArrayBinLocationToList.Length; i++)
                                        {
                                            BinLocation _locBinLocationTo = Session.FindObject<BinLocation>(new BinaryOperator("Code", _stringArrayBinLocationToList[i]));
                                            if (_locBinLocationTo != null)
                                            {
                                                if (i == 0)
                                                {
                                                    _beginString = "[Code]=='" + _locBinLocationTo.Code + "'";
                                                }
                                                else
                                                {
                                                    _endString = " OR [Code]=='" + _locBinLocationTo.Code + "'";
                                                }
                                            }
                                        }
                                    }
                                    _fullString = _beginString + _endString;

                                    if (_fullString != null)
                                    {
                                        _availableBinLocationTo = new XPCollection<BinLocation>(Session, CriteriaOperator.Parse(_fullString));
                                    }
                                }
                                #endregion TransferIn
                            }

                        }
                    }
                }
                else
                {
                    _availableBinLocationTo = new XPCollection<BinLocation>(Session);
                }

                return _availableBinLocationTo;

            }
        }

        [Appearance("InventoryTransferOrderLotBinLocationToClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceProperty("AvailableBinLocationTo", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        public BinLocation BinLocationTo
        {
            get { return _binLocationTo; }
            set { SetPropertyValue("BinLocationTo", ref _binLocationTo, value); }
        }

        [Appearance("InventoryTransferOrderLotStockTypeToClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public StockType StockTypeTo
        {
            get { return _stockTypeTo; }
            set { SetPropertyValue("StockTypeTo", ref _stockTypeTo, value); }
        }

        #endregion LocationTo

        [Browsable(false)]
        public XPCollection<BeginingInventoryLine> AvailableBeginingInventoryLine
        {
            get
            {
                string _locItem = null;
                string _locLocation = null;
                string _locBinLocation = null;
                string _fullString = null;
                if (this.InventoryTransferOrderLine != null)
                {
                    if (Item == null)
                    {
                        _availableBeginingInventoryLine = new XPCollection<BeginingInventoryLine>(Session);
                    }
                    else
                    {
                        //If For Item
                        if (this.Item != null) { _locItem = "[Item.Code]=='" + this.Item.Code + "'"; }
                        else { _locItem = ""; }

                        //If For Location
                        if (this.LocationFrom != null && this.Item != null)
                        { _locLocation = " AND [Location.Code]=='" + this.LocationFrom.Code + "'"; }
                        else if (this.LocationFrom != null && this.Item == null)
                        { _locLocation = " [Location.Code]=='" + this.LocationFrom.Code + "'"; }
                        else { _locLocation = ""; }

                        //If for BinLocation
                        if (this.BinLocationFrom != null && (this.Item != null || this.LocationFrom != null))
                        { _locBinLocation = " AND [BinLocation.Code]=='" + this.BinLocationFrom.Code + "'"; }
                        else if (this.BinLocationFrom != null && this.Item != null && this.LocationFrom != null)
                        { _locBinLocation = "[BinLocation.Code] == '" + this.BinLocationFrom.Code + "'"; }
                        else { _locBinLocation = ""; }

                        if (_locItem != null || _locLocation != null || _locBinLocation != null)
                        {
                            _fullString = _locItem + _locLocation + _locBinLocation;
                        }

                        if (_fullString != null)
                        {
                            _availableBeginingInventoryLine = new XPCollection<BeginingInventoryLine>(Session, CriteriaOperator.Parse(_fullString));
                        }
                    }
                }

                return _availableBeginingInventoryLine;
            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableBeginingInventoryLine", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("InventoryTransferOrderLotBegInvLineClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BeginingInventoryLine BegInvLine
        {
            get { return _begInvLine; }
            set
            {
                SetPropertyValue("BegInvLine", ref _begInvLine, value);
                if (!IsLoading)
                {
                    if (this._begInvLine != null)
                    {
                        if (this._begInvLine.LotNumber != null)
                        {
                            this.LotNumber = this._begInvLine.LotNumber;
                        }
                        if (this._begInvLine.UnitPack != null)
                        {
                            this.UnitPack = this._begInvLine.UnitPack;
                        }
                    }
                }
            }
        }

        [Appearance("InventoryTransferOrderLotNewLotNumberClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string LotNumber
        {
            get { return _lotNumber; }
            set { SetPropertyValue("LotNumber", ref _lotNumber, value); }
        }

        [Persistent]
        public double QtyAvailable
        {
            get
            {
                double _locQtyAvailable = 0;
                if (this.BegInvLine != null)
                {
                    if (this.BegInvLine.QtyAvailable > 0)
                    {
                        _locQtyAvailable = this.BegInvLine.QtyAvailable;
                    }
                    return _locQtyAvailable;
                }
                return 0;
            }
        }

        [Persistent]
        public string UOM_Available
        {
            get
            {
                string _locName = null;
                if (this.BegInvLine != null)
                {
                    if (this.BegInvLine.FullName != null)
                    {
                        _locName = this.BegInvLine.FullName;
                    }
                    return String.Format("{0}", _locName);
                }
                return null;
            }
        }

        [Browsable(false)]
        public XPCollection<UnitOfMeasure> AvailableUnitOfMeasure
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;
                if (Item == null)
                {
                    _availableUnitOfMeasure = new XPCollection<UnitOfMeasure>(Session);
                }
                else
                {
                    List<string> _stringUOM = new List<string>();

                    XPCollection<ItemUnitOfMeasure> _locItemUnitOfMeasures = new XPCollection<ItemUnitOfMeasure>
                                                                              (Session, new GroupOperator(GroupOperatorType.And,
                                                                               new BinaryOperator("Item", this.Item),
                                                                               new BinaryOperator("Active", true)));

                    if (_locItemUnitOfMeasures != null && _locItemUnitOfMeasures.Count() > 0)
                    {
                        foreach (ItemUnitOfMeasure _locItemUnitOfMeasure in _locItemUnitOfMeasures)
                        {
                            _stringUOM.Add(_locItemUnitOfMeasure.UOM.Code);
                        }
                    }

                    IEnumerable<string> _stringArrayUOMDistinct = _stringUOM.Distinct();
                    string[] _stringArrayUOMList = _stringArrayUOMDistinct.ToArray();
                    if (_stringArrayUOMList.Length == 1)
                    {
                        for (int i = 0; i < _stringArrayUOMList.Length; i++)
                        {
                            UnitOfMeasure _locUOM = Session.FindObject<UnitOfMeasure>(new BinaryOperator("Code", _stringArrayUOMList[i]));
                            if (_locUOM != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locUOM.Code + "'";
                                }
                            }
                        }
                    }
                    else if (_stringArrayUOMList.Length > 1)
                    {
                        for (int i = 0; i < _stringArrayUOMList.Length; i++)
                        {
                            UnitOfMeasure _locUOM = Session.FindObject<UnitOfMeasure>(new BinaryOperator("Code", _stringArrayUOMList[i]));
                            if (_locUOM != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locUOM.Code + "'";
                                }
                                else
                                {
                                    _endString = " OR [Code]=='" + _locUOM.Code + "'";
                                }
                            }
                        }
                    }
                    _fullString = _beginString + _endString;

                    if (_fullString != null)
                    {
                        _availableUnitOfMeasure = new XPCollection<UnitOfMeasure>(Session, CriteriaOperator.Parse(_fullString));
                    }

                }
                return _availableUnitOfMeasure;

            }
        }

        #region NewDefaultQty

        [Appearance("InventoryTransferOrderLotUnitPackClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public UnitOfMeasure UnitPack
        {
            get { return _unitPack; }
            set { SetPropertyValue("UnitPack", ref _unitPack, value); }
        }

        [Appearance("InventoryTransferOrderLotDQtyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double DQty
        {
            get { return _dQty; }
            set
            {
                SetPropertyValue("DQty", ref _dQty, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                }
            }
        }

        [Appearance("InventoryTransferOrderLotDUOMClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure DUOM
        {
            get { return _dUom; }
            set
            {
                SetPropertyValue("DUOM", ref _dUom, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                }
            }
        }

        [Appearance("InventoryTransferOrderLotQtyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double Qty
        {
            get { return _qty; }
            set
            {
                SetPropertyValue("Qty", ref _qty, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                }
            }
        }

        [Appearance("InventoryTransferOrderLotUOMClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceProperty("AvailableUnitOfMeasure", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure UOM
        {
            get { return _uom; }
            set
            {
                SetPropertyValue("UOM", ref _uom, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                }
            }
        }

        [Appearance("InventoryTransferOrderLotTQtyEnabled", Enabled = false)]
        public double TQty
        {
            get { return _tQty; }
            set { SetPropertyValue("TQty", ref _tQty, value); }
        }

        #endregion NewDefaultQty

        [Appearance("InventoryTransferOrderLotStatusEnabled", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("InventoryTransferOrderLotStatusDateEnabled", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [ImmediatePostData]
        [Appearance("InventoryTransferOrderLotInventoryTransferOrderLineEnabled", Enabled = false)]
        [Association("InventoryTransferOrderLine-InventoryTransferOrderLots")]
        public InventoryTransferOrderLine InventoryTransferOrderLine
        {
            get { return _inventoryTransferOrderLine; }
            set
            {
                SetPropertyValue("InventoryTransferOrderLine", ref _inventoryTransferOrderLine, value);
                if (!IsLoading)
                {
                    if (this._inventoryTransferOrderLine != null)
                    {
                        this.DQty = this._inventoryTransferOrderLine.DQty;
                        if (this._inventoryTransferOrderLine.UOM != null)
                        {
                            this.DUOM = this._inventoryTransferOrderLine.UOM;
                        }
                        this.Qty = this._inventoryTransferOrderLine.Qty;
                        if (this._inventoryTransferOrderLine.UOM != null)
                        {
                            this.UOM = this._inventoryTransferOrderLine.UOM;
                        }
                        if (this._inventoryTransferOrderLine.LocationFrom != null)
                        {
                            this.LocationFrom = this._inventoryTransferOrderLine.LocationFrom;
                        }
                        if (this._inventoryTransferOrderLine.BinLocationFrom != null)
                        {
                            this.BinLocationFrom = this._inventoryTransferOrderLine.BinLocationFrom;
                        }
                        if (this._inventoryTransferOrderLine.LocationTo != null)
                        {
                            this.LocationTo = this._inventoryTransferOrderLine.LocationTo;
                        }
                        if (this._inventoryTransferOrderLine.BinLocationTo != null)
                        {
                            this.BinLocationTo = this._inventoryTransferOrderLine.BinLocationTo;
                        }
                        if (this._inventoryTransferOrderLine.Item != null)
                        {
                            this.Item = this._inventoryTransferOrderLine.Item;
                        }
                    }
                }
            }
        }

        [Browsable(false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        #endregion Field

        //==================================================== Code Only ======================================================

        #region No

        public void UpdateNo()
        {
            try
            {
                if (!IsLoading && Session.IsNewObject(this))
                {
                    if (this.InventoryTransferOrderLine != null)
                    {
                        object _makRecord = Session.Evaluate<InventoryTransferOrderLot>(CriteriaOperator.Parse("Max(No)"), CriteriaOperator.Parse("InventoryTransferOrderLine=?", this.InventoryTransferOrderLine));
                        this.No = Convert.ToInt32(_makRecord) + 1;
                        this.Save();
                        RecoveryUpdateNo();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferOrderLot " + ex.ToString());
            }
        }

        public void RecoveryUpdateNo()
        {
            try
            {
                if (this.InventoryTransferOrderLine != null)
                {
                    InventoryTransferOrderLine _numHeader = Session.FindObject<InventoryTransferOrderLine>
                                                (new BinaryOperator("Code", this.InventoryTransferOrderLine.Code));

                    XPCollection<InventoryTransferOrderLot> _numLines = new XPCollection<InventoryTransferOrderLot>
                                                (Session, new BinaryOperator("InventoryTransferOrderLine", _numHeader),
                                                new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i1 = 0;
                        foreach (InventoryTransferOrderLot _numLine in _numLines)
                        {
                            i1 += 1;
                            _numLine.No = i1;
                            _numLine.Save();
                        }
                        i1 = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferOrderLot " + ex.ToString());
            }
        }

        public void RecoveryDeleteNo()
        {
            try
            {
                if (this.InventoryTransferOrderLine != null)
                {
                    InventoryTransferOrderLine _numHeader = Session.FindObject<InventoryTransferOrderLine>
                                                (new BinaryOperator("Code", this.InventoryTransferOrderLine.Code));

                    XPCollection<InventoryTransferOrderLot> _numLines = new XPCollection<InventoryTransferOrderLot>
                                                (Session, new GroupOperator(GroupOperatorType.And,
                                                 new BinaryOperator("This", this, BinaryOperatorType.NotEqual),
                                                 new BinaryOperator("InventoryTransferOrderLine", _numHeader)),
                                                 new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i = 0;
                        foreach (InventoryTransferOrderLot _numLine in _numLines)
                        {
                            i += 1;
                            _numLine.No = i;
                            _numLine.Save();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferOrderLot " + ex.ToString());
            }
        }

        #endregion No

        #region Set

        private void SetTotalQty()
        {
            try
            {
                double _locInvLineTotal = 0;
                DateTime now = DateTime.Now;
                if (this.InventoryTransferOrderLine != null)
                {
                    ItemUnitOfMeasure _locItemUOM = Session.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", this.Item),
                                                         new BinaryOperator("UOM", this.UOM),
                                                         new BinaryOperator("DefaultUOM", this.DUOM),
                                                         new BinaryOperator("Active", true)));
                    if (_locItemUOM != null)
                    {
                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                        {
                            _locInvLineTotal = this.Qty * _locItemUOM.DefaultConversion + this.DQty;
                        }
                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                        {
                            _locInvLineTotal = this.Qty / _locItemUOM.Conversion + this.DQty;
                        }
                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                        {
                            _locInvLineTotal = this.Qty + this.DQty;
                        }

                        this.TQty = _locInvLineTotal;
                    }
                    else
                    {
                        _locInvLineTotal = this.Qty + this.DQty;
                        this.TQty = _locInvLineTotal;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferOrderLot " + ex.ToString());
            }
        }

        #endregion Set
    }
}