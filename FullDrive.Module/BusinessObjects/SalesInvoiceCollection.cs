﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Editors;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("SalesInvoiceCollectionRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class SalesInvoiceCollection : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private bool _activationPosting;
        private string _code;
        private string _name;
        private BusinessPartner _salesToCostumer;
        private string _salesToContact;
        private Country _salesToCountry;
        private City _salesToCity;
        private string _salesToAddress;
        private DateTime _orderDate;
        private DateTime _documentDate;
        private string _costumerOrderNo;
        private string _costumerShipmentNo;
        private string _costumerInvoiceNo;
        private string _taxNo;
        private TermOfPayment _top;
        private bool _splitInvoice;
        private double _maxBill;
        private bool _dp_posting;
        private double _dp_amount;
        private double _bill;
        private double _plan;
        private double _outstanding;
        private double _dm_amount;
        private PriceGroup _priceGroup;
        private string _remarks;
        private int _postedCount;
        private Status _status;
        private DateTime _statusDate;
        private SalesOrder _salesOrder;
        private ProjectHeader _projectHeader;
        private Company _company;
        private string _signCode;
        private GlobalFunction _globFunc;

        public SalesInvoiceCollection(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                DateTime now = DateTime.Now;
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.SalesInvoiceCollection);
                Status = Status.Open;
                StatusDate = now;
                
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [Appearance("SalesInvoiceCollectionCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("SalesInvoiceCollectionNameClose", Enabled = false)]
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        #region SalesToCustomer

        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        [Appearance("SalesInvoiceCollectionSalesToCostumerClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BusinessPartner SalesToCostumer
        {
            get { return _salesToCostumer; }
            set
            {
                SetPropertyValue("SalesToCostumer", ref _salesToCostumer, value);
                if (!IsLoading)
                {
                    if (this._salesToCostumer != null)
                    {
                        this.SalesToContact = this._salesToCostumer.Contact;
                        this.SalesToCountry = this._salesToCostumer.Country;
                        this.SalesToCity = this._salesToCostumer.City;
                        this.TaxNo = this._salesToCostumer.TaxNo;
                        this.TOP = this._salesToCostumer.TOP;
                        this.SalesToAddress = this._salesToCostumer.Address;
                    }
                }
            }
        }

        [Appearance("SalesInvoiceCollectionSalesToContactClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string SalesToContact
        {
            get { return _salesToContact; }
            set { SetPropertyValue("SalesToContact", ref _salesToContact, value); }
        }

        [DataSourceCriteria("Active = true")]
        [Appearance("SalesInvoiceCollectionSalesToCountryClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Country SalesToCountry
        {
            get { return _salesToCountry; }
            set { SetPropertyValue("SalesToCountry", ref _salesToCountry, value); }
        }

        [DataSourceCriteria("Active = true")]
        [Appearance("SalesInvoiceCollectionSalesToCityClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public City SalesToCity
        {
            get { return _salesToCity; }
            set { SetPropertyValue("SalesToCity", ref _salesToCity, value); }
        }

        [Size(512)]
        [Appearance("SalesInvoiceCollectionSalesToAddressClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string SalesToAddress
        {
            get { return _salesToAddress; }
            set { SetPropertyValue("SalesToAddress", ref _salesToAddress, value); }
        }

        #endregion SalesToCustomer

        [Appearance("SalesInvoiceCollectionOrderDateClose", Enabled = false)]
        public DateTime OrderDate
        {
            get { return _orderDate; }
            set { SetPropertyValue("OrderDate", ref _orderDate, value); }
        }

        [Appearance("SalesInvoiceCollectionDocumentDateClose", Enabled = false)]
        public DateTime DocumentDate
        {
            get { return _documentDate; }
            set { SetPropertyValue("DocumentDate", ref _documentDate, value); }
        }

        [Appearance("SalesInvoiceCollectionCostumerOrderNoClose", Enabled = false)]
        public string CostumerOrderNo
        {
            get { return _costumerOrderNo; }
            set { SetPropertyValue("CostumerOrderNo", ref _costumerOrderNo, value); }
        }

        [Appearance("SalesInvoiceCollectionCostumerShipmentNoClose", Enabled = false)]
        public string CostumerShipmentNo
        {
            get { return _costumerShipmentNo; }
            set { SetPropertyValue("CostumerShipmentNo", ref _costumerShipmentNo, value); }
        }

        [Appearance("SalesInvoiceCollectionCostumerInvoiceNoClose", Enabled = false)]
        public string CostumerInvoiceNo
        {
            get { return _costumerInvoiceNo; }
            set { SetPropertyValue("CostumerInvoiceNo", ref _costumerInvoiceNo, value); }
        }

        [Appearance("SalesInvoiceCollectionTaxNoClose", Enabled = false)]
        public string TaxNo
        {
            get { return _taxNo; }
            set { SetPropertyValue("TaxNo", ref _taxNo, value); }
        }

        [DataSourceCriteria("Active = true")]
        [Appearance("SalesInvoiceCollectionTOPClose", Enabled = false)]
        public TermOfPayment TOP
        {
            get { return _top; }
            set { SetPropertyValue("TOP", ref _top, value); }
        }

        [Appearance("SalesInvoiceCollectionSplitInvoiceClose", Enabled = false)]
        public bool SplitInvoice
        {
            get { return _splitInvoice; }
            set { SetPropertyValue("SplitInvoice", ref _splitInvoice, value); }
        }

        #region Amount

        [Appearance("SalesInvoiceCollectionMaxBillClose", Enabled = false)]
        public double MaxBill
        {
            get { return _maxBill; }
            set { SetPropertyValue("MaxBill", ref _maxBill, value); }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        public bool DP_Posting
        {
            get { return _dp_posting; }
            set { SetPropertyValue("DP_Posting", ref _dp_posting, value); }
        }

        [Appearance("SalesInvoiceCollectionDP_Amount", Enabled = false)]
        public double DP_Amount
        {
            get { return _dp_amount; }
            set { SetPropertyValue("DP_Amount", ref _dp_amount, value); }
        }

        [Appearance("SalesInvoiceCollectionPlanClose", Enabled = false)]
        public double Plan
        {
            get { return _plan; }
            set { SetPropertyValue("Plan", ref _plan, value); }
        }

        [Appearance("SalesInvoiceCollectionOutstandingClose", Enabled = false)]
        public double Outstanding
        {
            get { return _outstanding; }
            set { SetPropertyValue("Outstanding", ref _outstanding, value); }
        }

        [ImmediatePostData()]
        [Appearance("SalesInvoiceCollectionDM_AmountClose", Enabled = false)]
        public double DM_Amount
        {
            get { return _dm_amount; }
            set { SetPropertyValue("DM_Amount", ref _dm_amount, value); }
        }

        #endregion Amount

        [ImmediatePostData()]
        [Appearance("SalesInvoiceCollectionPriceGroupClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PriceGroup PriceGroup
        {
            get { return _priceGroup; }
            set { SetPropertyValue("PriceGroup", ref _priceGroup, value); }
        }

        [Appearance("SalesInvoiceCollectionSignCodeClose", Enabled = false)]
        public string SignCode
        {
            get { return _signCode; }
            set { SetPropertyValue("SignCode", ref _signCode, value); }
        }

        [Appearance("SalesInvoiceCollectionPostedCountClose", Enabled = false)]
        public int PostedCount
        {
            get { return _postedCount; }
            set { SetPropertyValue("PostedCount", ref _postedCount, value); }
        }

        [Appearance("SalesInvoiceCollectionStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("SalesInvoiceCollectionStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("SalesInvoiceCollectionSalesOrderClose", Enabled = false)]
        public SalesOrder SalesOrder
        {
            get { return _salesOrder; }
            set { SetPropertyValue("SalesOrder", ref _salesOrder, value); }
        }

        [Appearance("SalesInvoiceCollectionProjectHeaderClose", Enabled = false)]
        public ProjectHeader ProjectHeader
        {
            get { return _projectHeader; }
            set { SetPropertyValue("ProjectHeader", ref _projectHeader, value); }
        }

        [Browsable(false)]
        [DataSourceCriteria("Active = true")]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Association("SalesInvoiceCollection-SalesInvoiceCollectionLines")]
        public XPCollection<SalesInvoiceCollectionLine> SalesInvoiceCollectionLines
        {
            get { return GetCollection<SalesInvoiceCollectionLine>("SalesInvoiceCollectionLines"); }
        }

        [Association("SalesInvoiceCollection-PaymentInPlans")]
        public XPCollection<PaymentInPlan> PaymentInPlans
        {
            get { return GetCollection<PaymentInPlan>("PaymentInPlans"); }
        }

        #endregion Field
    }
}