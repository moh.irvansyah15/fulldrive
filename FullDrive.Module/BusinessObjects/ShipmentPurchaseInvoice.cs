﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using DevExpress.ExpressApp.Editors;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;


namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("ShipmentPurchaseInvoiceRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class ShipmentPurchaseInvoice : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).

        private bool _activationPosting;
        private bool _activeApproved1;
        private bool _activeApproved2;
        private bool _activeApproved3;
        private string _code;
        private ShipmentType _shipmentType;
        #region BuyFrom
        private BusinessPartner _buyFromBusinessPartner;
        private string _buyFromContact;
        private Country _buyFromCountry;
        private City _buyFromCity;
        private string _buyFromAddress;
        #endregion BuyFrom
        #region PayTo
        private BusinessPartner _payToBusinessPartner;
        private string _payToContact;
        private Country _payToCountry;
        private City _payToCity;
        private string _payToAddress;
        #endregion PayTo
        private PaymentMethod _paymentMethod;
        private PaymentMethodType _paymentMethodType;
        private PaymentType _paymentType;
        private TermOfPayment _top;
        private Currency _currency;
        #region Bank
        private XPCollection<BankAccount> _availableBankAccountCompany;
        private BankAccount _bankAccountCompany;
        private string _companyAccountNo;
        private string _companyAccountName;
        private XPCollection<BankAccount> _availableBankAccounts;
        private BankAccount _bankAccount;
        private string _accountNo;
        private string _accountName;
        #endregion Bank
        private double _maxAmount;
        private double _amount;
        private int _postedCount;
        private Status _status;
        private DateTime _statusDate;
        private Company _company;
        private string _userAccess;
        private GlobalFunction _globFunc;

        public ShipmentPurchaseInvoice(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.ShipmentPurchaseInvoice);
            DateTime now = DateTime.Now;
            this.Status = CustomProcess.Status.Open;
            this.StatusDate = now;

            _userAccess = SecuritySystem.CurrentUserName;
            UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
            if (Session.IsNewObject(this))
            {
                if (_locUserAccess != null)
                {
                    if (_locUserAccess.Employee != null)
                    {
                        if (_locUserAccess.Employee.Company != null)
                        {
                            Company = _locUserAccess.Employee.Company;
                        }
                    }
                }
            }
            this.Currency = _globFunc.GetDefaultCurrency(this.Session);
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field



        [ImmediatePostData()]
        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        #region ColorApproval

        [Browsable(false)]
        [Appearance("ShipmentPurchaseInvoiceActiveApproved1Close", Enabled = false)]
        public bool ActiveApproved1
        {
            get { return _activeApproved1; }
            set { SetPropertyValue("ActiveApproved1", ref _activeApproved1, value); }
        }

        [Browsable(false)]
        [Appearance("ShipmentPurchaseInvoiceActiveApproved2Close", Enabled = false)]
        public bool ActiveApproved2
        {
            get { return _activeApproved2; }
            set { SetPropertyValue("ActiveApproved2", ref _activeApproved2, value); }
        }

        [Browsable(false)]
        [Appearance("ShipmentPurchaseInvoiceActiveApproved3Close", Enabled = false)]
        public bool ActiveApproved3
        {
            get { return _activeApproved3; }
            set { SetPropertyValue("ActiveApproved3", ref _activeApproved3, value); }
        }

        #endregion ColorApproval

        [Appearance("ShipmentPurchaseInvoiceRedColor", Criteria = "ActiveApproved1 = true", BackColor = "#fd79a8")]
        [Appearance("ShipmentPurchaseInvoiceYellowColor", Criteria = "ActiveApproved2 = true", BackColor = "#ffeaa7")]
        [Appearance("ShipmentPurchaseInvoiceGreenOColor", Criteria = "ActiveApproved3 = true", BackColor = "#81ecec")]
        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("ShipmentPurchaseInvoiceCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("ShipmentPurchaseInvoiceShipmentTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public ShipmentType ShipmentType
        {
            get { return _shipmentType; }
            set { SetPropertyValue("ShipmentType", ref _shipmentType, value); }
        }

        #region BuyFromBusinessPartner

        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        [Appearance("ShipmentPurchaseInvoiceBuyFromBusinessPartnerClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BusinessPartner BuyFromBusinessPartner
        {
            get { return _buyFromBusinessPartner; }
            set
            {
                SetPropertyValue("BuyFromBusinessPartner", ref _buyFromBusinessPartner, value);
                if (!IsLoading)
                {
                    if (_buyFromBusinessPartner != null)
                    {
                        if(this._buyFromBusinessPartner.Contact != null)
                        {
                            this.BuyFromContact = _buyFromBusinessPartner.Contact;
                        }
                        if(this._buyFromBusinessPartner.Country != null)
                        {
                            this.BuyFromCountry = _buyFromBusinessPartner.Country;
                        }
                        if(this._buyFromBusinessPartner.City != null)
                        {
                            this.BuyFromCity = _buyFromBusinessPartner.City;
                        }
                        if(this._buyFromBusinessPartner.Address != null)
                        {
                            this.BuyFromAddress = _buyFromBusinessPartner.Address;
                        }
                        
                        this.PayToBusinessPartner = _buyFromBusinessPartner;
                    }
                }
            }
        }

        [Appearance("ShipmentPurchaseInvoiceBuyFromContactClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string BuyFromContact
        {
            get { return _buyFromContact; }
            set { SetPropertyValue("BuyFromContact", ref _buyFromContact, value); }
        }

        [DataSourceCriteria("Active = true")]
        [Appearance("ShipmentPurchaseInvoiceBuyFromCountryClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Country BuyFromCountry
        {
            get { return _buyFromCountry; }
            set { SetPropertyValue("BuyFromCountry", ref _buyFromCountry, value); }
        }

        [DataSourceCriteria("Active = true")]
        [Appearance("ShipmentPurchaseInvoiceBuyFromCityClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public City BuyFromCity
        {
            get { return _buyFromCity; }
            set { SetPropertyValue("BuyFromCity", ref _buyFromCity, value); }
        }

        [Appearance("ShipmentPurchaseInvoiceBuyFromAddressClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Size(512)]
        public string BuyFromAddress
        {
            get { return _buyFromAddress; }
            set { SetPropertyValue("BuyFromAddress", ref _buyFromAddress, value); }
        }

        #endregion BuyFromBusinessPartner

        #region PayToBusinessPartner

        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        [Appearance("ShipmentPurchaseInvoicePayToVendorClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BusinessPartner PayToBusinessPartner
        {
            get { return _payToBusinessPartner; }
            set
            {
                SetPropertyValue("PayToCarrier", ref _payToBusinessPartner , value);
                if (!IsLoading)
                {
                    if (this._payToBusinessPartner != null)
                    {
                        if(this._payToBusinessPartner.Contact != null)
                        {
                            this.PayToContact = this._payToBusinessPartner.Contact;
                        }
                        if(this._payToBusinessPartner.Address != null)
                        {
                            this.PayToAddress = this._payToBusinessPartner.Address;
                        }
                        if(this._payToBusinessPartner.Country != null)
                        {
                            this.PayToCountry = this._payToBusinessPartner.Country;
                        }
                        if(this._payToBusinessPartner.City != null)
                        {
                            this.PayToCity = this._payToBusinessPartner.City;
                        }
                    }
                }
            }
        }

        [Appearance("ShipmentPurchaseInvoicePayToContactClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string PayToContact
        {
            get { return _payToContact; }
            set { SetPropertyValue("PayToContact", ref _payToContact, value); }
        }

        [DataSourceCriteria("Active = true")]
        [Appearance("ShipmentPurchaseInvoicePayToCountryClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Country PayToCountry
        {
            get { return _payToCountry; }
            set { SetPropertyValue("PayToCountry", ref _payToCountry, value); }
        }

        [DataSourceCriteria("Active = true")]
        [Appearance("ShipmentPurchaseInvoicePayToCityClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public City PayToCity
        {
            get { return _payToCity; }
            set { SetPropertyValue("PayToCity", ref _payToCity, value); }
        }

        [Appearance("ShipmentPurchaseInvoicePayToAddressClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Size(512)]
        public string PayToAddress
        {
            get { return _payToAddress; }
            set { SetPropertyValue("PayToAddress", ref _payToAddress, value); }
        }

        #endregion PayToBusinessPartner

        [Appearance("ShipmentPurchaseInvoicePaymentMethodClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PaymentMethod PaymentMethod
        {
            get { return _paymentMethod; }
            set { SetPropertyValue("PaymentMethod", ref _paymentMethod, value); }
        }

        [Appearance("ShipmentPurchaseInvoicePaymentMethodTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PaymentMethodType PaymentMethodType
        {
            get { return _paymentMethodType; }
            set { SetPropertyValue("PaymentMethodType", ref _paymentMethodType, value); }
        }

        [Appearance("ShipmentPurchaseInvoicePaymentTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PaymentType PaymentType
        {
            get { return _paymentType; }
            set { SetPropertyValue("PaymentType", ref _paymentType, value); }
        }

        [Appearance("ShipmentPurchaseInvoiceTermOfPaymentClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public TermOfPayment TOP
        {
            get { return _top; }
            set { SetPropertyValue("TOP", ref _top, value); }
        }

        [Appearance("ShipmentPurchaseInvoiceCurrencyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Currency Currency
        {
            get { return _currency; }
            set { SetPropertyValue("Currency", ref _currency, value); }
        }

        #region Bank

        [Browsable(false)]
        public XPCollection<BankAccount> AvailableBankAccountCompany
        {
            get
            {
                if (this.Company == null)
                {
                    _availableBankAccountCompany = new XPCollection<BankAccount>(Session);
                }
                else
                {
                    _availableBankAccountCompany = new XPCollection<BankAccount>(Session, new GroupOperator
                                                (GroupOperatorType.And,
                                                new BinaryOperator("Company", this.Company)));
                }
                return _availableBankAccountCompany;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableBankAccountCompany", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("ShipmentPurchaseInvoiceBankAccountCompanyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BankAccount BankAccountCompany
        {
            get { return _bankAccountCompany; }
            set
            {
                SetPropertyValue("BankAccountCompany", ref _bankAccountCompany, value);
                if (!IsLoading)
                {
                    if (this._bankAccountCompany != null)
                    {
                        this.CompanyAccountNo = this._bankAccountCompany.AccountNo;
                        this.CompanyAccountName = this._bankAccountCompany.AccountName;
                    }
                }
            }
        }

        [Appearance("ShipmentPurchaseInvoiceCompanyAccountNoClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string CompanyAccountNo
        {
            get { return _companyAccountNo; }
            set { SetPropertyValue("CompanyAccountNo", ref _companyAccountNo, value); }
        }

        [Appearance("ShipmentPurchaseInvoiceCompanyAccountNameClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string CompanyAccountName
        {
            get { return _companyAccountName; }
            set { SetPropertyValue("CompanyAccountName", ref _companyAccountName, value); }
        }

        [Browsable(false)]
        public XPCollection<BankAccount> AvailableBankAccounts
        {
            get
            {
                if (this.PayToBusinessPartner == null)
                {
                    _availableBankAccounts = new XPCollection<BankAccount>(Session);
                }
                else
                {
                    _availableBankAccounts = new XPCollection<BankAccount>(Session, new GroupOperator
                                                (GroupOperatorType.And,
                                                new BinaryOperator("BusinessPartner", this.PayToBusinessPartner)));
                }
                return _availableBankAccounts;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableBankAccounts", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("ShipmentPurchaseInvoiceBankAccountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BankAccount BankAccount
        {
            get { return _bankAccount; }
            set
            {
                SetPropertyValue("BankAccount", ref _bankAccount, value);
                if (!IsLoading)
                {
                    if (this._bankAccount != null)
                    {
                        this.AccountNo = this._bankAccount.AccountNo;
                        this.AccountName = this._bankAccount.AccountName;
                    }
                }
            }
        }

        [Appearance("ShipmentPurchaseInvoiceAccountNoClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string AccountNo
        {
            get { return _accountNo; }
            set { SetPropertyValue("AccountNo", ref _accountNo, value); }
        }

        [Appearance("ShipmentPurchaseInvoiceAccountNameClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string AccountName
        {
            get { return _accountName; }
            set { SetPropertyValue("AccountName", ref _accountName, value); }
        }

        #endregion Bank

        [Appearance("ShipmentPurchaseInvoiceMaxAmountClose", Enabled = false)]
        public double MaxAmount
        {
            get { return _maxAmount; }
            set { SetPropertyValue("MaxAmount", ref _maxAmount, value); }
        }

        [ImmediatePostData()]
        [Appearance("ShipmentPurchaseInvoiceAmountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double Amount
        {
            get { return _amount; }
            set { SetPropertyValue("Amount", ref _amount, value); }
        }

        [Appearance("ShipmentPurchaseInvoicePostedCountClose", Enabled = false)]
        public int PostedCount
        {
            get { return _postedCount; }
            set { SetPropertyValue("PostedCount", ref _postedCount, value); }
        }

        [Appearance("ShipmentPurchaseInvoiceStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("ShipmentPurchaseInvoiceStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Browsable(false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Association("ShipmentPurchaseInvoice-ShipmentPurchaseInvoiceLines")]
        public XPCollection<ShipmentPurchaseInvoiceLine> ShipmentPurchaseInvoiceLines
        {
            get { return GetCollection<ShipmentPurchaseInvoiceLine>("ShipmentPurchaseInvoiceLines"); }
        }

        [Association("ShipmentPurchaseInvoice-ShipmentApprovalLines")]
        public XPCollection<ShipmentApprovalLine> ShipmentApprovalLines
        {
            get { return GetCollection<ShipmentApprovalLine>("ShipmentApprovalLines"); }
        }

        [Association("ShipmentPurchaseInvoice-ShipmentPurchaseCollections")]
        public XPCollection<ShipmentPurchaseCollection> ShipmentPurchaseCollections
        {
            get { return GetCollection<ShipmentPurchaseCollection>("ShipmentPurchaseCollections"); }
        }

        #endregion Field
        
    }
}