﻿#region Default

using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

#endregion Default

using DevExpress.ExpressApp.Editors;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    #region Default

    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Production")]
    [RuleCombinationOfPropertiesIsUnique("ConsumptionJournalRuleUnique", DefaultContexts.Save, "Code")]

    #endregion Default

    // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
    public class ConsumptionJournal : BaseObject
    {
        #region Default

        private string _code;
        private Location _location;
        private BinLocation _binLocation;
        private Item _item;
        private Company _company;
        private double _qtyCons;
        private double _qtyActual;
        private double _qtyReturn;
        private UnitOfMeasure _dUOM;
        private Consumption _consumption;
        private DateTime _journalDate;
        private GlobalFunction _globFunc;

        public ConsumptionJournal(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).

            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.ConsumptionJournal);
            }
        }

        #endregion Default

        #region Field

        [Appearance("ConsumptionJournalCodeEnabled", Enabled = false)]
        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [DataSourceCriteria("Active = true")]
        [Appearance("ConsumptionJournalLocationEnabled", Enabled = false)]
        public Location Location
        {
            get { return _location; }
            set { SetPropertyValue("Location", ref _location, value); }
        }

        [DataSourceCriteria("Active = true")]
        [Appearance("ConsumptionJournalBinLocationEnabled", Enabled = false)]
        public BinLocation BinLocation
        {
            get { return _binLocation; }
            set { SetPropertyValue("binLocation", ref _binLocation, value); }
        }

        [DataSourceCriteria("Active = true")]
        [Appearance("ConsumptionJournalItemEnabled", Enabled = false)]
        public Item Item
        {
            get { return _item; }
            set { SetPropertyValue("Item", ref _item, value); }
        }

        [Appearance("ConsumptionJournalQtyConsEnabled", Enabled = false)]
        public double QtyCons
        {
            get { return _qtyCons; }
            set { SetPropertyValue("QtyCons", ref _qtyCons, value); }
        }

        [Appearance("ConsumptionJournalQtyActualEnabled", Enabled = false)]
        public double QtyActual
        {
            get { return _qtyActual; }
            set { SetPropertyValue("QtyActual", ref _qtyActual, value); }
        }

        [Appearance("ConsumptionJournalQtyReturnEnabled", Enabled = false)]
        public double QtyReturn
        {
            get { return _qtyReturn; }
            set { SetPropertyValue("QtyReturn", ref _qtyReturn, value); }
        }

        [DataSourceCriteria("Active = true")]
        [Appearance("ConsumptionJournalDUOMEnabled", Enabled = false)]
        public UnitOfMeasure DUOM
        {
            get { return _dUOM; }
            set { SetPropertyValue("DUOM", ref _dUOM, value); }
        }

        [Appearance("ConsumptionJournalJournalDateEnabled", Enabled = false)]
        public DateTime JournalDate
        {
            get { return _journalDate; }
            set { SetPropertyValue("JournalDate", ref _journalDate, value); }
        }

        [DataSourceCriteria("Active = true")]
        [Appearance("ConsumptionJournalCompanyEnabled", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Appearance("ConsumptionJournalConsumptionEnabled", Enabled = false)]
        public Consumption Consumption
        {
            get { return _consumption; }
            set { SetPropertyValue("Consumption", ref _consumption, value); }
        }

        #endregion Field

    }
}