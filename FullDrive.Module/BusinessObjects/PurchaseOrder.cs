﻿#region Default

using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

#endregion Default

using DevExpress.ExpressApp.Editors;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    #region Addon
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Purchase")]
    [RuleCombinationOfPropertiesIsUnique("PurchaseOrderRuleUnique", DefaultContexts.Save, "Code")]

    ////filter button custom
    //[ListViewFilter("AllDataPO", "", "All Data", "All Data In Purchase Order", 1, true)]
    //[ListViewFilter("OpenPO", "Status = 'Open'", "Open", "Open Data Status In Purchase Order", 2, true)]
    //[ListViewFilter("ProgressPO", "Status = 'Progress'", "Progress", "Progress Data Status In Purchase Order", 3, true)]
    //[ListViewFilter("ClosePo", "Status = 'Close'", "Close", "Close Data Status In Purchase Order", 4, true)]
    #endregion Addon

    // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
    public class PurchaseOrder : FullDriveSysBaseObject
    { 
        #region Default

        private bool _activationPosting;
        private string _code;
        private ObjectList _objectList;
        XPCollection<NumberingLine> _availableNumberingLine;
        private NumberingLine _numbering;
        private BusinessPartner _buyFromVendor;
        private string _buyFromContact;
        private Country _buyFromCountry;
        private City _buyFromCity;
        private string _buyFromAddress;
        private DateTime _orderDate;
        private DateTime _documentDate;
        private string _vendorOrderNo;
        private string _vendorShipmentNo;
        private string _vendorInvoiceNo;
        private TermOfPayment _top;
        private PaymentMethod _paymentMethod;
        private string _taxNo;
        private Currency _currency;
        private DirectionType _transferType;
        private string _remarkTOP;
        private double _dp_percentage;
        private Status _status;
        private DateTime _statusDate;
        private DateTime _estimatedDate;
        private string _remarks;
        private ProjectHeader _projectHeader;
        private PurchaseRequisition _purchaseRequisition;
        private MaterialRequisition _materialRequisition;
        private string _signCode;
        private Company _company;
        private double _maxPay;
        private string _userAccess;
        private GlobalFunction _globFunc;
        //clm
        private bool _activeApproved1;
        private bool _activeApproved2;
        private bool _activeApproved3;
        //Monitoring
        private string _approvalDate1;
        private string _approvalDate2;
        private string _fullNamePR;
        private string _fullnameRequiredDatePR;
        private string _fullnameApprovalDatePR;
        private string _fullNameUserPR;
        private string _fullNameDeptPR;
        private string _fullNameNoBPB;
        private string _fullNametglBPB;
        

        public PurchaseOrder(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.PurchaseOrder);
                DateTime now = DateTime.Now;
                this.Status = CustomProcess.Status.Open;
                this.StatusDate = now;
                this.ObjectList = CustomProcess.ObjectList.PurchaseOrder;

                #region UserAccess
                _userAccess = SecuritySystem.CurrentUserName;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                if (Session.IsNewObject(this))
                {
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null)
                            {
                                Company = _locUserAccess.Employee.Company;
                            }
                        }
                    }
                }
                #endregion UserAccess

                this.Currency = _globFunc.GetDefaultCurrency(this.Session);

            }
        }

        #endregion Default

        #region Field
        [Browsable(false)]
        [ImmediatePostData()]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [ImmediatePostData()]
        [Appearance("PurchaseOrderCodeClose", Enabled = false)]
        [Appearance("PurchaseOrderRedColor", Criteria = "ActiveApproved1 = true", BackColor = "#fd79a8")]
        [Appearance("PurchaseOrderYellowColor", Criteria = "ActiveApproved2 = true", BackColor = "#ffeaa7")]
        [Appearance("PurchaseOrderGreenOColor", Criteria = "ActiveApproved3 = true", BackColor = "#81ecec")]
        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("PurchaseOrderCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Browsable(false)]
        public ObjectList ObjectList
        {
            get { return _objectList; }
            set { SetPropertyValue("ObjectList", ref _objectList, value); }
        }

        [Browsable(false)]
        public XPCollection<NumberingLine> AvailableNumberingLine
        {
            get
            {
                if (ObjectList == ObjectList.None)
                {
                    _availableNumberingLine = new XPCollection<NumberingLine>(Session);
                }
                else
                {
                    _availableNumberingLine = new XPCollection<NumberingLine>(Session,
                                              new GroupOperator(GroupOperatorType.And,
                                              new BinaryOperator("ObjectList", this.ObjectList),
                                              new BinaryOperator("Selection", true),
                                              new BinaryOperator("Active", true)));
                }
                return _availableNumberingLine;

            }
        }

        [NonPersistent()]
        [ImmediatePostData()]
        [DataSourceProperty("AvailableNumberingLine", DataSourcePropertyIsNullMode.SelectAll)]
        public NumberingLine Numbering
        {
            get { return _numbering; }
            set
            {
                NumberingLine _oldNumbering = _numbering;
                SetPropertyValue("Numbering", ref _numbering, value);
                if (!IsLoading)
                {
                    if (_numbering != null)
                    {
                        if (this.Session != null)
                        {
                            if (this.Session.DataLayer != null)
                            {
                                this.Code = LocGetNumberingSelectionUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.PurchaseOrder, _numbering);
                            }
                        }
                    }
                }
            }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        [Appearance("PurchaseOrderBuyFromVendorClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BusinessPartner BuyFromVendor
        {
            get { return _buyFromVendor; }
            set
            {
                SetPropertyValue("BuyFromVendor", ref _buyFromVendor, value);
                if (!IsLoading)
                {
                    if (this._buyFromVendor != null)
                    {
                        this.BuyFromContact = this._buyFromVendor.Contact;
                        this.BuyFromCountry = this._buyFromVendor.Country;
                        this.BuyFromCity = this._buyFromVendor.City;
                        this.TaxNo = this._buyFromVendor.TaxNo;
                        this.TOP = this._buyFromVendor.TOP;
                        this.BuyFromAddress = this._buyFromVendor.Address;
                    }
                }
            }
        }

        [Appearance("PurchaseOrderBuyFromContactClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string BuyFromContact
        {
            get { return _buyFromContact; }
            set { SetPropertyValue("BuyFromContact", ref _buyFromContact, value); }
        }

        [DataSourceCriteria("Active = true")]
        [Appearance("PurchaseOrderBuyFromCountryClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Country BuyFromCountry
        {
            get { return _buyFromCountry; }
            set { SetPropertyValue("BuyFromCountry", ref _buyFromCountry, value); }
        }

        [DataSourceCriteria("Active = true")]
        [Appearance("PurchaseOrderBuyFromCityClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public City BuyFromCity
        {
            get { return _buyFromCity; }
            set { SetPropertyValue("BuyFromCity", ref _buyFromCity, value); }
        }

        [Size(512)]
        [Appearance("PurchaseOrderBuyFromAddressClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string BuyFromAddress
        {
            get { return _buyFromAddress; }
            set { SetPropertyValue("BuyFromAddress", ref _buyFromAddress, value); }
        }

        [Appearance("PurchaseOrderOrderDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime OrderDate
        {
            get { return _orderDate; }
            set { SetPropertyValue("OrderDate", ref _orderDate, value); }
        }

        [Appearance("PurchaseOrderDocumentDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime DocumentDate
        {
            get { return _documentDate; }
            set { SetPropertyValue("DocumentDate", ref _documentDate, value); }
        }

        [Appearance("PurchaseOrderVendorOrderNoClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string VendorOrderNo
        {
            get { return _vendorOrderNo; }
            set { SetPropertyValue("VendorOrderNo", ref _vendorOrderNo, value); }
        }

        [Appearance("PurchaseOrderVendorShipmentNoClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string VendorShipmentNo
        {
            get { return _vendorShipmentNo; }
            set { SetPropertyValue("VendorShipmentNo", ref _vendorShipmentNo, value); }
        }

        [Appearance("PurchaseOrderVendorInvoiceNoClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string VendorInvoiceNo
        {
            get { return _vendorInvoiceNo; }
            set { SetPropertyValue("VendorInvoiceNo", ref _vendorInvoiceNo, value); }
        }

        [DataSourceCriteria("Active = true")]
        [Appearance("PurchaseOrderTOPClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public TermOfPayment TOP
        {
            get { return _top; }
            set { SetPropertyValue("TOP", ref _top, value); }
        }

        [DataSourceCriteria("Active = true")]
        [ImmediatePostData()]
        [Appearance("PurchaseOrderPaymentMethodClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PaymentMethod PaymentMethod
        {
            get { return _paymentMethod; }
            set {
                SetPropertyValue("PaymentMethod", ref _paymentMethod, value);       
            }
        }

        [Appearance("PurchaseOrderTaxNoClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string TaxNo
        {
            get { return _taxNo; }
            set { SetPropertyValue("TaxNo", ref _taxNo, value); }
        }

        [Appearance("PurchaseOrderCurrencyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Currency Currency
        {
            get { return _currency; }
            set { SetPropertyValue("Currency", ref _currency, value); }
        }

        [Appearance("PurchaseOrderTransferTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DirectionType TransferType
        {
            get { return _transferType; }
            set { SetPropertyValue("TransferType", ref _transferType, value); }
        }

        [Appearance("PurchaseOrderRemarkTOPClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string RemarkTOP
        {
            get { return _remarkTOP; }
            set { SetPropertyValue("RemarkTOP", ref _remarkTOP, value); }
        }

        //menghitung total di line terus di kalikan 
        [ImmediatePostData()]
        [Appearance("PurchaseOrderDP_PercentageClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double DP_Percentage
        {
            get { return _dp_percentage; }
            set { SetPropertyValue("DP_Percentage", ref _dp_percentage, value); }
        }

        [Appearance("PurchaseOrderStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("PurchaseOrderStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("PurchaseOrderEstimatedDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime EstimatedDate
        {
            get { return _estimatedDate; }
            set { SetPropertyValue("EstimatedDate", ref _estimatedDate, value); }
        }

        [Appearance("PurchaseOrderRemarksClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Size(512)]
        public string Remarks
        {
            get { return _remarks; }
            set { SetPropertyValue("Remarks", ref _remarks, value); }
        }

        [Appearance("PurchaseOrderPurchaseProjectHeaderClose", Enabled = false)]
        [Association("ProjectHeader-PurchaseOrders")]
        public ProjectHeader ProjectHeader
        {
            get { return _projectHeader; }
            set { SetPropertyValue("ProjectHeader", ref _projectHeader, value); }
        }

        [Appearance("PurchaseOrderPurchaseRequisitionClose", Enabled = false)]
        public PurchaseRequisition PurchaseRequisition
        {
            get { return _purchaseRequisition; }
            set { SetPropertyValue("PurchaseRequisition", ref _purchaseRequisition, value); }
        }

        [Appearance("PurchaseOrderMaterialRequisitionClose", Enabled = false)]
        public MaterialRequisition MaterialRequisition
        {
            get { return _materialRequisition; }
            set { SetPropertyValue("MaterialRequisition", ref _materialRequisition, value); }
        }

        [Appearance("PurchaseOrderSignCodeClose", Enabled = false)]
        public string SignCode
        {
            get { return _signCode; }
            set { SetPropertyValue("SignCode", ref _signCode, value); }
        }

        [Browsable(false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        //[Browsable(false)]
        public double MaxPay
        {
            get { return _maxPay; }
            set { SetPropertyValue("MaxPay", ref _maxPay, value); }
        }

        [Association("PurchaseOrder-PurchaseOrderLines")]
        public XPCollection<PurchaseOrderLine> PurchaseOrderLines
        {
            get { return GetCollection<PurchaseOrderLine>("PurchaseOrderLines"); }
        }

        [Association("PurchaseOrder-ApprovalLines")]
        public XPCollection<ApprovalLine> ApprovalLines
        {
            get { return GetCollection<ApprovalLine>("ApprovalLines"); }
        }

        [Association("PurchaseOrder-PurchaseRequisitionCollections")]
        public XPCollection<PurchaseRequisitionCollection> PurchaseRequisitionCollections
        {
            get { return GetCollection<PurchaseRequisitionCollection>("PurchaseRequisitionCollections"); }
        }

        [Association("PurchaseOrder-EDocuments")]
        public XPCollection<EDocument> EDocuments
        {
            get { return GetCollection<EDocument>("EDocuments"); }
        }

        #region ApprovedAndFullName

        [ImmediatePostData()]
        [Appearance("PurchaseOrderApprovalDate1Enabled", Enabled = false, Visibility = ViewItemVisibility.Hide)]
        public string ApprovalDate1
        {
            get
            {
                string _result = null;
                try
                {
                    DateTime _date;
                    if (!IsLoading)
                    {
                        XPCollection<ApprovalLine> _locApprovalLines = new XPCollection<ApprovalLine>(Session,
                                                                           new BinaryOperator("PurchaseOrder", this));

                        if (_locApprovalLines.Count() > 0)
                        {
                            foreach (ApprovalLine _locApprovalLine in _locApprovalLines)
                            {
                                if (_locApprovalLine.ApprovalLevel == ApprovalLevel.Level1)
                                {
                                    _date = _locApprovalLine.ApprovalDate;
                                    _result = _date.ToString("dd/MM/yyyy HH:mm:ss");
                                }
                            }
                            return _result;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Tracing.Tracer.LogError("BusinessObject = PurchaseOrder" + ex.ToString());
                }
                return _result;
            }
        }
        
        [ImmediatePostData()]
        [Appearance("PurchaseOrderApprovalDate2Enabled", Enabled = false, Visibility = ViewItemVisibility.Hide)]
        public string ApprovalDate2
        {
            get
            {
                string _result = null;
                try
                {
                    DateTime _date;
                    if (!IsLoading)
                    {
                        XPCollection<ApprovalLine> _locApprovalLines = new XPCollection<ApprovalLine>(Session,
                                                                           new BinaryOperator("PurchaseOrder", this));

                        if (_locApprovalLines.Count() > 0)
                        {
                            foreach (ApprovalLine _locApprovalLine in _locApprovalLines)
                            {
                                if (_locApprovalLine.ApprovalLevel == ApprovalLevel.Level2)
                                {
                                    _date = _locApprovalLine.ApprovalDate;
                                    _result = _date.ToString("dd/MM/yyyy HH:mm:ss");
                                }
                            }
                            return _result;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Tracing.Tracer.LogError("BusinessObject = PurchaseOrder" + ex.ToString());
                }
                return _result;
            }
        }
        
        [Appearance("PurchaseOrderFullNamePREnabled", Enabled = false, Visibility = ViewItemVisibility.Hide)]
        public string FullNamePR
        {
            get
            {
                string _locName = "";
                string _locName1 = "";
                bool _filterString;
                if (this != null)
                {
                    XPCollection<PurchaseRequisitionCollection> _locPurchaseRequisitionCollections = new XPCollection<PurchaseRequisitionCollection>
                                                                                     (this.Session, new GroupOperator(GroupOperatorType.And,
                                                                                      new BinaryOperator("PurchaseOrder", this)));

                    if (_locPurchaseRequisitionCollections != null && _locPurchaseRequisitionCollections.Count() > 0)
                    {
                        foreach (PurchaseRequisitionCollection _locPurchaseRequisitionCollection in _locPurchaseRequisitionCollections)
                        {
                            if (_locPurchaseRequisitionCollection != null)
                            {
                                if (_locPurchaseRequisitionCollection.PurchaseRequisition != null)
                                {
                                    if (_locPurchaseRequisitionCollection.PurchaseRequisition.Code != null)
                                    {
                                        _locName1 = _locPurchaseRequisitionCollection.PurchaseRequisition.Code;
                                        if (_locName.Contains(_locName1) == false)
                                        {
                                            _locName = _locName + _locName1 + ',';
                                        }
                                        else if (_locName.Contains(_locName1) == true)
                                        {
                                            _locName = _locName;
                                        }
                                        
                                    }
                                }
                            } 
                        }
                        return String.Format("{0}", _locName);
                    }
                    else if (this.PurchaseRequisition != null)
                    {
                        {
                            _locName = _locName + this.PurchaseRequisition.Code + ',';
                        };
                        return String.Format("{0}", _locName);
                    }
                    else
                    {
                        return null;
                    }
                }
                return null;
            }
        }
        
        [Appearance("PurchaseOrderFullNameRequiredDatePREnabled", Enabled = false, Visibility = ViewItemVisibility.Hide)]
        public string FullNameRequiredDatePR
        {
            get
            {
                string _locName = "";
                string _locName1 = "";
                if (this != null)
                {
                    XPCollection<PurchaseRequisitionCollection> _locPurchaseRequisitionCollections = new XPCollection<PurchaseRequisitionCollection>
                                                                                     (this.Session, new GroupOperator(GroupOperatorType.And,
                                                                                      new BinaryOperator("PurchaseOrder", this)));

                    if (_locPurchaseRequisitionCollections != null && _locPurchaseRequisitionCollections.Count() > 0)
                    {
                        foreach (PurchaseRequisitionCollection _locPurchaseRequisitionCollection in _locPurchaseRequisitionCollections)
                        {
                            if (_locPurchaseRequisitionCollection != null)
                            {
                                if (_locPurchaseRequisitionCollection.PurchaseRequisition != null)
                                {
                                    if (_locPurchaseRequisitionCollection.PurchaseRequisition.RequiredDate!= null)
                                    {
                                        _locName1 = _locPurchaseRequisitionCollection.PurchaseRequisition.RequiredDate.ToString("dd/MM/yyyy HH:mm:ss");
                                        if (_locName.Contains(_locName1) == false)
                                        {
                                            _locName = _locName + _locName1 + ',';
                                        }
                                        else if (_locName.Contains(_locName1) == true)
                                        {
                                            _locName = _locName;
                                        }

                                    }
                                }
                            }
                        }

                        return String.Format("{0}", _locName);
                    }
                    else if (this.PurchaseRequisition != null)
                    {
                        {
                            _locName = _locName + this.PurchaseRequisition.RequiredDate.ToString("dd/MM/yyyy HH:mm:ss") + ',';
                        };
                        return String.Format("{0}", _locName);
                    }

                    else
                    {
                        return null;
                    }
                }
                return null;
            }
        }
        
        [Appearance("PurchaseRequisitionApprovalDate2PREnabled", Enabled = false, Visibility = ViewItemVisibility.Hide)]
        public string FullNameApprovalDate2PR
        {
            get
            {
                string _result = null;
                try
                {
                    DateTime _date;
                    if (!IsLoading)
                    {
                        XPCollection<ApprovalLine> _locApprovalLines = new XPCollection<ApprovalLine>(Session,
                                                                           new BinaryOperator("PurchaseRequisition", this));
                        if (_locApprovalLines.Count() > 0)
                        {
                            foreach (ApprovalLine _locApprovalLine in _locApprovalLines)
                            {
                                if (_locApprovalLine.ApprovalLevel == ApprovalLevel.Level2)
                                {
                                    _date = _locApprovalLine.ApprovalDate;
                                    _result = _date.ToString("dd/MM/yyyy HH:mm:ss");
                                }
                            }
                            return _result;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Tracing.Tracer.LogError("BusinessObject = PurchaseRequisition" + ex.ToString());
                }
                return null;
            }
        }

        [Appearance("PurchaseOrderFullNameUserPREnabled", Enabled = false, Visibility = ViewItemVisibility.Hide)]
        public string FullNameUserPR
        {
            get
            {
                string _locName = "";
                string _locName1 = "";
                bool _filterString;
                if (this != null)
                {
                    XPCollection<PurchaseRequisitionCollection> _locPurchaseRequisitionCollections = new XPCollection<PurchaseRequisitionCollection>
                                                                                     (this.Session, new GroupOperator(GroupOperatorType.And,
                                                                                      new BinaryOperator("PurchaseOrder", this)));

                    if (_locPurchaseRequisitionCollections != null && _locPurchaseRequisitionCollections.Count() > 0)
                    {
                        foreach (PurchaseRequisitionCollection _locPurchaseRequisitionCollection in _locPurchaseRequisitionCollections)
                        {
                            if (_locPurchaseRequisitionCollection != null)
                            {
                                if (_locPurchaseRequisitionCollection.PurchaseRequisition != null)
                                {
                                    if (_locPurchaseRequisitionCollection.PurchaseRequisition.CreateBy != null)
                                    {
                                        _locName1 = _locPurchaseRequisitionCollection.PurchaseRequisition.CreateBy;
                                        if (_locName.Contains(_locName1) == false)
                                        {
                                            _locName = _locName + _locName1 + ',';
                                        }
                                        else if (_locName.Contains(_locName1) == true)
                                        {
                                            _locName = _locName;
                                        }

                                    }
                                }
                            }
                        }
                        return String.Format("{0}", _locName);
                    }
                    else if (this.PurchaseRequisition != null)
                    {
                        {
                            _locName = _locName + this.PurchaseRequisition.CreateBy + ',';
                        };
                        return String.Format("{0}", _locName);
                    }
                    else
                    {
                        return null;
                    }
                }
                return null;
            }
        }

        [Appearance("PurchaseOrderFullNameDeptPREnabled", Enabled = false, Visibility = ViewItemVisibility.Hide)]
        public string FullNameDeptPR
        {
            get
            {
                string _locName = "";
                string _locName1 = "";
                bool _filterString;
                if (this != null)
                {
                    XPCollection<PurchaseRequisitionCollection> _locPurchaseRequisitionCollections = new XPCollection<PurchaseRequisitionCollection>
                                                                                     (this.Session, new GroupOperator(GroupOperatorType.And,
                                                                                      new BinaryOperator("PurchaseOrder", this)));

                    if (_locPurchaseRequisitionCollections != null && _locPurchaseRequisitionCollections.Count() > 0)
                    {
                        foreach (PurchaseRequisitionCollection _locPurchaseRequisitionCollection in _locPurchaseRequisitionCollections)
                        {
                            if (_locPurchaseRequisitionCollection != null)
                            {
                                if (_locPurchaseRequisitionCollection.PurchaseRequisition != null)
                                {
                                    if (_locPurchaseRequisitionCollection.PurchaseRequisition.Department != null)
                                    {
                                        if (_locPurchaseRequisitionCollection.PurchaseRequisition.Department.Name != null)
                                        {
                                            _locName1 = _locPurchaseRequisitionCollection.PurchaseRequisition.Department.Name;
                                            if (_locName.Contains(_locName1) == false)
                                            {
                                                _locName = _locName + _locName1 + ',';
                                            }
                                            else if (_locName.Contains(_locName1) == true)
                                            {
                                                _locName = _locName;
                                            }
                                        }
                                        else
                                        {
                                            _locName = _locName;
                                        }
                                    }
                                }
                            }
                        }
                        return String.Format("{0}", _locName);
                    }
                    else if (this.PurchaseRequisition != null)
                    {
                        {
                            if (this.PurchaseRequisition.Department != null)
                            {
                                if (this.PurchaseRequisition.Department.Name != null)
                                {
                                    _locName = _locName + this.PurchaseRequisition.Department.Name + ',';
                                }
                            }
                        };
                        return String.Format("{0}", _locName);
                    }
                    else
                    {
                        return null;
                    }
                }
                return null;
            }
        }

        [Appearance("PurchaseOrderFullNameNoBPBEnabled", Enabled = false, Visibility = ViewItemVisibility.Hide)]
        public string FullNameNoBPB
        {
            get
            {
                string _locName = null;

                if (this != null)
                {
                    XPCollection<InventoryTransferIn> _locInventoryTranferIns = new XPCollection<InventoryTransferIn>
                                                                                (this.Session, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("PurchaseOrder", this)));

                    if (_locInventoryTranferIns != null && _locInventoryTranferIns.Count() > 0)
                    {
                        foreach (InventoryTransferIn _locInventoryTransferIn in _locInventoryTranferIns)
                        {
                            _locName = _locName + _locInventoryTransferIn.Code + ',';
                        }

                        return String.Format("{0}", _locName);
                    }
                    else
                    {
                        return null;
                    }
                }
                return null;
            }
        }
        
        [Appearance("PurchaseOrderFullNameTglBPBEnabled", Enabled = false, Visibility = ViewItemVisibility.Hide)]
        public string FullNameTglBPB
        {
            get
            {
                string _locName = null;

                if (this != null)
                {
                    XPCollection<InventoryTransferIn> _locInventoryTranferIns = new XPCollection<InventoryTransferIn>
                                                                                (this.Session, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("PurchaseOrder", this)));

                    if (_locInventoryTranferIns != null && _locInventoryTranferIns.Count() > 0)
                    {
                        foreach (InventoryTransferIn _locInventoryTransferIn in _locInventoryTranferIns)
                        {
                            _locName = _locName + _locInventoryTransferIn.EstimatedDate + ',';
                        }

                        return String.Format("{0}", _locName);
                    }
                    else
                    {
                        return null;
                    }
                }
                return null;
            }
        }

        #endregion ApprovedAndFullName

        //Wahab 27-02-2020

        //[Association("PurchaseOrder-InventoryTransferOrders")]
        //public XPCollection<InventoryTransferOrder> InventoryTransferOrders
        //{
        //    get { return GetCollection<InventoryTransferOrder>("InventoryTransferOrders"); }
        //}

        //[Association("PurchaseOrder-InventoryTransferIns")]
        //public XPCollection<InventoryTransferIn> InventoryTransferIns
        //{
        //    get { return GetCollection<InventoryTransferIn>("InventoryTransferIns"); }
        //}

        //[Association("PurchaseOrder-InventoryTransferOuts")]
        //public XPCollection<InventoryTransferOut> InventoryTransferOuts
        //{
        //    get { return GetCollection<InventoryTransferOut>("InventoryTransferOuts"); }
        //}

        //[Association("PurchaseOrder-PurchaseInvoices")]
        //public XPCollection<PurchaseInvoice> PurchaseInvoices
        //{
        //    get { return GetCollection<PurchaseInvoice>("PurchaseInvoices"); }
        //}

        //[Association("PurchaseOrder-PaymentOutPlans")]
        //public XPCollection<PaymentOutPlan> PaymentOutPlans
        //{
        //    get { return GetCollection<PaymentOutPlan>("PaymentOutPlans"); }
        //}

        //[Association("PurchaseOrder-PurchasePrePaymentInvoices")]
        //public XPCollection<PurchasePrePaymentInvoice> PurchasePrePaymentInvoices
        //{
        //    get { return GetCollection<PurchasePrePaymentInvoice>("PurchasePrePaymentInvoices"); }
        //}

        #region CLM

        [Persistent("GrandTotalPOL")]
        public double GrandTotalPOL
        {
            get
            {
                double _result = 0;
                int _totalLine = 0;
                if (!IsLoading)
                {
                    _totalLine = Convert.ToInt32(Session.Evaluate<PurchaseOrder>(CriteriaOperator.Parse("PurchaseOrderLines.Count"), CriteriaOperator.Parse("Oid=?", Oid)));
                    if (_totalLine >= 0)
                    {
                        _result = UpdateGrandTotalPOL(true);
                    }
                }
                return _result;
            }
        }

        [Browsable(false)]
        [Appearance("PurchaseOrderActiveApproved1Close", Enabled = false)]
        public bool ActiveApproved1
        {
            get { return _activeApproved1; }
            set { SetPropertyValue("ActiveApproved1", ref _activeApproved1, value); }
        }

        [Browsable(false)]
        [Appearance("PurchaseOrderActiveApproved2Close", Enabled = false)]
        public bool ActiveApproved2
        {
            get { return _activeApproved2; }
            set { SetPropertyValue("ActiveApproved2", ref _activeApproved2, value); }
        }

        [Browsable(false)]
        [Appearance("PurchaseOrderActiveApproved3Close", Enabled = false)]
        public bool ActiveApproved3
        {
            get { return _activeApproved3; }
            set { SetPropertyValue("ActiveApproved3", ref _activeApproved3, value); }
        }

        #endregion CLM

        #endregion Field

        //===== Code Only =====

        #region CLM

        public double UpdateGrandTotalPOL(bool forceChangeEvents)
        {
            double _result = 0;
            try
            {
                XPCollection<PurchaseOrderLine> _locPurchaseOrderLines = new XPCollection<PurchaseOrderLine>(Session,
                                                                         new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("PurchaseOrder", this)));

                if (_locPurchaseOrderLines != null && _locPurchaseOrderLines.Count > 0)
                {
                    if (_locPurchaseOrderLines.Count > 0)
                    {
                        foreach (PurchaseOrderLine _locPurchaseOrderLine in _locPurchaseOrderLines)
                        {
                            if (forceChangeEvents)
                            {
                                if (_locPurchaseOrderLine.TAmountPOL >= 0)
                                {
                                    _result = _locPurchaseOrderLine.TAmountPOL;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PurchaseOrder", ex.ToString());
            }
            return _result;
        }

        #endregion CLm

        #region LocalNumbering

        public string LocGetNumberingSelectionUnlockOptimisticRecord(IDataLayer _currIDataLayer, ObjectList _currObject, NumberingLine _currNumberingLine)
        {
            string _result = null;
            Session _generatorSession = null;
            try
            {
                if (_currIDataLayer != null)
                {
                    _generatorSession = new Session(_currIDataLayer);
                }

                if (_generatorSession != null)
                {
                    ApplicationSetup _appSetup = _generatorSession.FindObject<ApplicationSetup>(new GroupOperator(GroupOperatorType.And,
                                            new BinaryOperator("Active", true), new BinaryOperator("DefaultSystem", true)));
                    if (_appSetup != null)
                    {
                        NumberingHeader _numberingHeader = _generatorSession.FindObject<NumberingHeader>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                             new BinaryOperator("NumberingType", NumberingType.Objects),
                                                             new BinaryOperator("ApplicationSetup", _appSetup, BinaryOperatorType.Equal),
                                                             new BinaryOperator("Active", true)));
                        if (_numberingHeader != null)
                        {
                            NumberingLine _numberingLine = _generatorSession.FindObject<NumberingLine>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Oid", _currNumberingLine.Oid),
                                                         new BinaryOperator("ObjectList", _currObject),
                                                         new BinaryOperator("Selection", true),
                                                         new BinaryOperator("Active", true),
                                                         new BinaryOperator("NumberingHeader", _numberingHeader)));
                            if (_numberingLine != null)
                            {
                                _numberingLine.LastValue = _numberingLine.LastValue + _numberingLine.IncrementValue;
                                _numberingLine.Save();
                                _result = _numberingLine.FormatedValue;
                                _numberingLine.Session.CommitTransaction();
                            }

                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }
        #endregion LocalNumbering

        #region ApprovalDate
        public string GetApprovalDate1()
        {
            string _result = null;
            try
            {
                DateTime _date;
                if (!IsLoading)
                {
                    XPCollection<ApprovalLine> _locApprovalLines = new XPCollection<ApprovalLine>(Session,
                                                                       new BinaryOperator("PurchaseOrder", this));

                    if (_locApprovalLines.Count() > 0)
                    {
                        foreach (ApprovalLine _locApprovalLine in _locApprovalLines)
                        {
                            if (_locApprovalLine.ApprovalLevel == ApprovalLevel.Level1)
                            {
                                _date = _locApprovalLine.ApprovalDate;
                                _result = _date.ToString("dd/MM/yyyy HH:mm:ss");
                            }
                        }
                        return _result;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PurchaseOrder" + ex.ToString());
            }
            return _result;
        }

        public string GetApprovalDate2()
        {
            string _result = null;
            try
            {
                DateTime _date;
                if (!IsLoading)
                {
                    XPCollection<ApprovalLine> _locApprovalLines = new XPCollection<ApprovalLine>(Session,
                                                                       new BinaryOperator("PurchaseOrder", this));

                    if (_locApprovalLines.Count() > 0)
                    {
                        foreach (ApprovalLine _locApprovalLine in _locApprovalLines)
                        {
                            if (_locApprovalLine.ApprovalLevel == ApprovalLevel.Level2)
                            {
                                _date = _locApprovalLine.ApprovalDate;
                                _result = _date.ToString("dd/MM/yyyy HH:mm:ss");
                            }
                        }
                        return _result;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PurchaseOrder" + ex.ToString());
            }
            return _result;
        }
        #endregion ApprovalDate

    }
}