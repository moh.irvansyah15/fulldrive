﻿#region Default

using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

#endregion Default

using DevExpress.ExpressApp.Editors;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    #region Default

    [DefaultClassOptions]
    [NavigationItem("Production")]
    [DefaultProperty("Code")]
    [RuleCombinationOfPropertiesIsUnique("ConsumptionRuleUnique", DefaultContexts.Save, "Code")]

    #endregion Default

    // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
    public class Consumption : FullDriveSysBaseObject
    {
        #region Default

        private bool _activationPosting;
        private string _code;
        private string _name;
        private string _description;
        private DateTime _startDate;
        private DateTime _endDate;
        private Status _status;
        private DateTime _statusDate;
        private WorkOrder _workOrder;
        private Production _production;
        private MachineMapVersion _machineMapVersion;
        private MachineCost _machineCost;
        private string _signCode;
        private Company _company;
        private string _userAccess;
        private GlobalFunction _globalFunc;

        public Consumption(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).

            if (!IsLoading)
            {
                _globalFunc = new GlobalFunction();
                this.Code = _globalFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.Consumption);
                DateTime Now = DateTime.Now;
                this.Status = CustomProcess.Status.Open;
                this.StatusDate = Now;
            }

            _userAccess = SecuritySystem.CurrentUserName;
            UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
            if (Session.IsNewObject(this))
            {
                if (_locUserAccess != null)
                {
                    if (_locUserAccess.Employee != null)
                    {
                        if (_locUserAccess.Employee.Company != null)
                        {
                            Company = _locUserAccess.Employee.Company;
                        }
                    }
                }
            }
        }

        #endregion Default

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("ConsumptionCodeEnabled", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("ConsumptionNameClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        [Size(512)]
        [Appearance("ConsumptionDescriptionClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        [Appearance("ConsumptionStartDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime StartDate
        {
            get { return _startDate; }
            set { SetPropertyValue("StartDate", ref _startDate, value); }
        }

        [Appearance("ConsumptionEndDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime EndDate
        {
            get { return _endDate; }
            set { SetPropertyValue("EndDate", ref _endDate, value); }
        }

        [Appearance("ConsumptionStatusEnabled", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("ConsumptionStatusDateEnabled", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        //01-03-2020
        //[Association("WorkOrder-Consumptions")]
        [Appearance("ConsumptionWorkOrderEnabled", Enabled = false)]
        public WorkOrder WorkOrder
        {
            get { return _workOrder; }
            set { SetPropertyValue("WorkOrder", ref _workOrder, value); }
        }

        [Appearance("ConsumptionProductionEnabled", Enabled = false)]
        [Association("Production-Consumptions")]
        public Production Production
        {
            get { return _production; }
            set { SetPropertyValue("Production", ref _production, value); }
        }

        [Appearance("ConsumptionMachineMapVersionEnabled", Enabled = false)]
        public MachineMapVersion MachineMapVersion
        {
            get { return _machineMapVersion; }
            set { SetPropertyValue("MachineMapVersion", ref _machineMapVersion, value); }
        }

        [Appearance("ConsumptionMachineCostEnabled", Enabled = false)]
        public MachineCost MachineCost
        {
            get { return _machineCost; }
            set { SetPropertyValue("MachineCost", ref _machineCost, value); }
        }

        [Appearance("ConsumptionSignCodeClose", Enabled = false)]
        public string SignCode
        {
            get { return _signCode; }
            set { SetPropertyValue("SignCode", ref _signCode, value); }
        }

        //[Browsable(false)]
        [Appearance("ConsumptionCompanyClose", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Association("Consumption-ConsumptionLines")]
        public XPCollection<ConsumptionLine> ConsumptionLines
        {
            get { return GetCollection<ConsumptionLine>("ConsumptionLines"); }
        }

        #endregion Field

    }
}