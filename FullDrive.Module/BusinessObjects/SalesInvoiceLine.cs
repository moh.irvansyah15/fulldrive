﻿#region Default

using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

#endregion Default

using DevExpress.ExpressApp.Editors;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    #region Default

    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("SalesInvoiceLineRuleUnique", DefaultContexts.Save, "Code")]
    //[DeferredDeletion]
    #endregion Default

    // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
    public class SalesInvoiceLine : FullDriveSysBaseObject
    {
        #region Default

        private bool _activationPosting;
        private int _no;
        private bool _select;
        private string _code;
        private OrderType _salesType;
        private XPCollection<Item> _availableItem;
        private Item _item;
        private string _name;
        private string _description;
        private XPCollection<UnitOfMeasure> _availableUnitOfMeasure;
        #region InisialisasiMaxQty
        private double _mxDQTY;
        private UnitOfMeasure _mxDUom;
        private double _mxQty;
        private UnitOfMeasure _mxUom;
        private double _mxTQty;
        private double _mxUAmount;
        private double _mxTUAmount;
        #endregion InisialisasiMaxQty
        #region inisialisasiDefaultQty
        private double _dQty;
        private UnitOfMeasure _dUom;
        private double _qty;
        private UnitOfMeasure _uom;
        private double _tQty;
        #endregion inisialisasiDefaultQty
        private Currency _currency;
        private PriceGroup _priceGroup;
        private XPCollection<Price> _availablePrice;
        private Price _price;
        private XPCollection<PriceLine> _availablePriceLine;
        private PriceLine _priceLine;
        private double _uAmount;
        private double _tUAmount;
        private bool _multiTax;
        private Tax _tax;
        private double _txValue;
        private double _txAmount;
        private bool _multiDiscount;
        private Discount _discount;
        private double _disc;
        private double _discAmount;
        private double _tAmount;
        private double _bill;
        #region InisialisasiRemainQty
        private double _rmDQty;
        private double _rmQty;
        private double _rmTQty;
        #endregion InisialisasiRemainQty
        #region InitialPostingQuantityTo
        private double _pDQty;
        private UnitOfMeasure _pDUom;
        private double _pQty;
        private UnitOfMeasure _pUom;
        private double _pTQty;
        private double _PTotalSil;
        private double _PTUAmount;
        private double _PTAmountSIL;
        private double _PTSil;
        private double _PDisc;
        private double _PTax;
        #endregion InitialPostingQuantityTo
        private TermOfPayment _top;
        private Company _company;
        private Status _status;
        private DateTime _statusDate;
        private int _processCount;
        private string _signCode;
        private SalesInvoice _salesInvoice;
        private InventoryTransferOutMonitoring _inventoryTransferOutMonitoring;
        private SalesOrderMonitoring _salesOrderMonitoring;
        private GlobalFunction _globFunc;
        //clm
        private double _totalDM;
        private bool _hideSumTotalItem;

        public SalesInvoiceLine(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.SalesInvoiceLine);
                this.Select = true;
                DateTime now = DateTime.Now;
                this.Status = CustomProcess.Status.Open;
                this.StatusDate = now;
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        protected override void OnSaving()
        {
            base.OnSaving();
            if (!IsLoading && IsSaving)
            {
                UpdateNo();
            }
        }

        protected override void OnDeleting()
        {
            base.OnDeleting();
            if (!IsLoading)
            {
                RecoveryDeleteNo();
            }
        }

        #endregion Default

        #region Field

        //[Browsable(false)]
        [ImmediatePostData()]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [Appearance("SalesInvoiceLineNoClose", Enabled = false)]
        public int No
        {
            get { return _no; }
            set { SetPropertyValue("No", ref _no, value); }
        }

        [Appearance("SalesInvoiceLineSelectClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public bool Select
        {
            get { return _select; }
            set { SetPropertyValue("Select", ref _select, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("SalesInvoiceLineCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [ImmediatePostData()]
        [Appearance("SalesInvoiceLineSalesTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public OrderType SalesType
        {
            get { return _salesType; }
            set { SetPropertyValue("SalesType", ref _salesType, value); }
        }

        [Browsable(false)]
        public XPCollection<Item> AvailableItem
        {
            get
            {
                if (SalesType == OrderType.Item)
                {
                    _availableItem = new XPCollection<Item>(Session,
                                    new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("OrderType", OrderType.Item)));

                }
                else if (SalesType == OrderType.Service)
                {
                    _availableItem = new XPCollection<Item>(Session,
                                    new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("OrderType", OrderType.Service)));
                }
                else
                {
                    _availableItem = new XPCollection<Item>(Session);
                }

                return _availableItem;

            }
        }

        [DataSourceProperty("AvailableItem", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("SalesInvoiceLineItemClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Item Item
        {
            get { return _item; }
            set { SetPropertyValue("Item", ref _item, value); }
        }

        [Appearance("SalesInvoiceLineNameClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        [Appearance("SalesInvoiceLineDescriptionClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Size(512)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        [Browsable(false)]
        public XPCollection<UnitOfMeasure> AvailableUnitOfMeasure
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;
                if (Item == null)
                {
                    _availableUnitOfMeasure = new XPCollection<UnitOfMeasure>(Session);
                }
                else
                {
                    List<string> _stringUOM = new List<string>();

                    XPCollection<ItemUnitOfMeasure> _locItemUnitOfMeasures = new XPCollection<ItemUnitOfMeasure>
                                                                              (Session, new GroupOperator(GroupOperatorType.And,
                                                                               new BinaryOperator("Item", this.Item),
                                                                               new BinaryOperator("Active", true)));

                    if (_locItemUnitOfMeasures != null && _locItemUnitOfMeasures.Count() > 0)
                    {
                        foreach (ItemUnitOfMeasure _locItemUnitOfMeasure in _locItemUnitOfMeasures)
                        {
                            _stringUOM.Add(_locItemUnitOfMeasure.UOM.Code);
                        }
                    }

                    IEnumerable<string> _stringArrayUOMDistinct = _stringUOM.Distinct();
                    string[] _stringArrayUOMList = _stringArrayUOMDistinct.ToArray();
                    if (_stringArrayUOMList.Length == 1)
                    {
                        for (int i = 0; i < _stringArrayUOMList.Length; i++)
                        {
                            UnitOfMeasure _locUOM = Session.FindObject<UnitOfMeasure>(new BinaryOperator("Code", _stringArrayUOMList[i]));
                            if (_locUOM != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locUOM.Code + "'";
                                }
                            }
                        }
                    }
                    else if (_stringArrayUOMList.Length > 1)
                    {
                        for (int i = 0; i < _stringArrayUOMList.Length; i++)
                        {
                            UnitOfMeasure _locUOM = Session.FindObject<UnitOfMeasure>(new BinaryOperator("Code", _stringArrayUOMList[i]));
                            if (_locUOM != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locUOM.Code + "'";
                                }
                                else
                                {
                                    _endString = _endString + " OR [Code]=='" + _locUOM.Code + "'";
                                }
                            }
                        }
                    }
                    _fullString = _beginString + _endString;

                    if (_fullString != null)
                    {
                        _availableUnitOfMeasure = new XPCollection<UnitOfMeasure>(Session, CriteriaOperator.Parse(_fullString));
                    }

                }
                return _availableUnitOfMeasure;

            }
        }

        #region MaxDefaultQty

        [Appearance("SalesInvoiceLineMxDQtyClose", Enabled = false)]
        [ImmediatePostData()]
        public double MxDQty
        {
            get { return _mxDQTY; }
            set
            {
                SetPropertyValue("MxDQty", ref _mxDQTY, value);
                if (!IsLoading)
                {
                    SetMaxTotalQty();
                }
            }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        [Appearance("SalesInvoiceLineMxDUOMClose", Enabled = false)]
        public UnitOfMeasure MxDUOM
        {
            get { return _mxDUom; }
            set
            {
                SetPropertyValue("MxDUOM", ref _mxDUom, value);
                if (!IsLoading)
                {
                    SetMaxTotalQty();
                }
            }
        }

        [ImmediatePostData()]
        [Appearance("SalesInvoiceLineMxQtyClose", Enabled = false)]
        public double MxQty
        {
            get { return _mxQty; }
            set
            {
                SetPropertyValue("MxQty", ref _mxQty, value);
                if (!IsLoading)
                {
                    SetMaxTotalQty();
                }
            }
        }

        [DataSourceProperty("AvailableUnitOfMeasure", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        [Appearance("SalesInvoiceLineMxUOMClose", Enabled = false)]
        public UnitOfMeasure MxUOM
        {
            get { return _mxUom; }
            set
            {
                SetPropertyValue("MxUOM", ref _mxUom, value);
                if (!IsLoading)
                {
                    SetMaxTotalQty();
                }
            }
        }

        [Appearance("SalesInvoiceLineMxTQtyClose", Enabled = false)]
        public double MxTQty
        {
            get { return _mxTQty; }
            set { SetPropertyValue("MxTQty", ref _mxTQty, value); }
        }

        [Appearance("SalesInvoiceLineMxUAmountClose", Enabled = false)]
        public double MxUAmount
        {
            get { return _mxUAmount; }
            set { SetPropertyValue("MxUAmount", ref _mxUAmount, value); }
        }

        [Appearance("SalesInvoiceLineMxTUAmountClose", Enabled = false)]
        public double MxTUAmount
        {
            get { return _mxTUAmount; }
            set { SetPropertyValue("MxTUAmount", ref _mxTUAmount, value); }
        }

        #endregion MaxDefaultQty

        #region DefaultQty

        [Appearance("SalesInvoiceLineDQtyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double DQty
        {
            get { return _dQty; }
            set
            {
                SetPropertyValue("DQty", ref _dQty, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                    if (this.MxDQty > 0)
                    {
                        if (this.ProcessCount > 0)
                        {
                            if (this.RmDQty == 0)
                            {
                                if (this._dQty > this.MxDQty || this._dQty <= this.MxDQty)
                                {
                                    this._dQty = 0;
                                    SetTotalQty();
                                }
                            }

                            if (this.RmDQty > 0)
                            {
                                if (this._dQty > this.RmDQty)
                                {
                                    this._dQty = this.RmDQty;
                                    SetTotalQty();
                                }
                            }
                        }

                        if (this.ProcessCount == 0)
                        {
                            if (this.DQty > this.MxDQty)
                            {
                                this.DQty = this.MxDQty;
                                SetTotalQty();
                            }
                        }

                        SetTotalUnitAmount();
                        SetTaxAmount();
                        SetDiscAmount();
                        SetTotalAmount();
                    }
                }
            }
        }

        [Appearance("SalesInvoiceLineDUOMClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure DUOM
        {
            get { return _dUom; }
            set
            {
                SetPropertyValue("DUOM", ref _dUom, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                    SetTotalUnitAmount();
                    SetTaxAmount();
                    SetDiscAmount();
                    SetTotalAmount();
                }
            }
        }

        [Appearance("SalesInvoiceLineQtyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double Qty
        {
            get { return _qty; }
            set
            {
                SetPropertyValue("Qty", ref _qty, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                    
                    if (this.MxQty > 0)
                    {
                        if (this.ProcessCount > 0)
                        {
                            if (this.RmQty == 0)
                            {
                                if (this._qty > this.MxQty || this._qty <= this.MxQty)
                                {
                                    this._qty = 0;
                                    SetTotalQty();
                                }
                            }

                            if (this.RmQty > 0)
                            {
                                if (this._qty > this.RmQty)
                                {
                                    this._qty = this.RmQty;
                                    SetTotalQty();
                                }
                            }
                        }

                        if (this.ProcessCount == 0)
                        {
                            if (this._qty > this.MxQty)
                            {
                                this._qty = this.MxQty;
                                SetTotalQty();
                            }
                        }
                    }
                    SetTotalUnitAmount();
                    SetTaxAmount();
                    SetDiscAmount();
                    SetTotalAmount();
                }
            }
        }

        [Appearance("SalesInvoiceLineUOMClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceProperty("AvailableUnitOfMeasure", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure UOM
        {
            get { return _uom; }
            set
            {
                SetPropertyValue("UOM", ref _uom, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                    SetTotalUnitAmount();
                    SetTaxAmount();
                    SetDiscAmount();
                    SetTotalAmount();
                }
            }
        }

        [Appearance("SalesInvoiceLineTotalQtyEnabled", Enabled = false)]
        public double TQty
        {
            get { return _tQty; }
            set { SetPropertyValue("TotalQty", ref _tQty, value); }
        }

        #endregion DefaultQty

        #region Amount

        [Appearance("SalesInvoiceLineCurrencyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Currency Currency
        {
            get { return _currency; }
            set { SetPropertyValue("Currency", ref _currency, value); }
        }

        [ImmediatePostData()]
        [Appearance("SalesInvoiceLinePriceGroupClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PriceGroup PriceGroup
        {
            get { return _priceGroup; }
            set { SetPropertyValue("PriceGroup", ref _priceGroup, value); }
        }

        [Browsable(false)]
        public XPCollection<Price> AvailablePrice
        {
            get
            {
                if (this.Item != null && this.PriceGroup != null)
                {
                    _availablePrice = new XPCollection<Price>(Session,
                                    new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("Item", this.Item),
                                    new BinaryOperator("PriceGroup", this.PriceGroup)));

                }

                else
                {
                    _availablePrice = new XPCollection<Price>(Session);
                }

                return _availablePrice;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailablePrice", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("SalesInvoiceLinePriceClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Price Price
        {
            get { return _price; }
            set { SetPropertyValue("Price", ref _price, value); }
        }

        [Browsable(false)]
        public XPCollection<PriceLine> AvailablePriceLine
        {
            get
            {
                if (this.Price != null)
                {
                    _availablePriceLine = new XPCollection<PriceLine>(Session,
                                          new GroupOperator(GroupOperatorType.And,
                                          new BinaryOperator("Price", this.Price)));

                }

                else
                {
                    _availablePriceLine = new XPCollection<PriceLine>(Session);
                }

                return _availablePriceLine;

            }
        }

        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        [DataSourceProperty("AvailablePriceLine", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("SalesInvoiceLinePriceLineClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PriceLine PriceLine
        {
            get { return _priceLine; }
            set
            {
                SetPropertyValue("PriceLine", ref _priceLine, value);
                if (!IsLoading)
                {
                    if (this._priceLine != null)
                    {
                        this.UAmount = this._priceLine.Amount2;
                    }
                }
            }
        }

        [Appearance("SalesInvoiceLineUnitAmountEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double UAmount
        {
            get { return _uAmount; }
            set
            {
                SetPropertyValue("UAmount", ref _uAmount, value);
                if (!IsLoading)
                {
                    _globFunc = new GlobalFunction();
                    if (this._uAmount > 0)
                    {
                        if (_globFunc.GetRoundingList(Session, ObjectList.SalesInvoiceLine, FieldName.UAmount) == true)
                        {
                            this._uAmount = _globFunc.GetRoundUp(Session, this._uAmount, ObjectList.SalesInvoiceLine, FieldName.UAmount);
                        }
                    }
                    SetTotalUnitAmount();
                    SetTaxAmount();
                    SetDiscAmount();
                    SetTotalAmount();
                }
            }
        }

        [Appearance("SalesInvoiceLineTotalUnitAmountEnabled", Enabled = false)]
        public double TUAmount
        {
            get { return _tUAmount; }
            set { SetPropertyValue("TUAmount", ref _tUAmount, value); }
        }

        [Appearance("SalesInvoiceLineMultiTaxClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public bool MultiTax
        {
            get { return _multiTax; }
            set
            {
                SetPropertyValue("ActivationPosting", ref _multiTax, value);
                if (!IsLoading)
                {
                    SetNormalTax();
                }
            }
        }

        [Appearance("SalesInvoiceLineTaxClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Appearance("SalesInvoiceLineTaxEnabled", Criteria = "MultiTax = true", Enabled = false)]
        [ImmediatePostData]
        [DataSourceCriteria("Active = true")]
        public Tax Tax
        {
            get { return _tax; }
            set
            {
                SetPropertyValue("Tax", ref _tax, value);
                if (!IsLoading)
                {
                    if (this._tax != null)
                    {
                        this.TxValue = this._tax.Value;
                        SetTaxAmount();
                        SetTotalAmount();
                    }
                    else
                    {
                        SetNormalTax();
                    }
                }
            }
        }

        [Appearance("SalesInvoiceLineTxValueClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Appearance("SalesInvoiceLineTxValueEnabled", Criteria = "MultiTax = true", Enabled = false)]
        [ImmediatePostData()]
        public double TxValue
        {
            get { return _txValue; }
            set
            {
                SetPropertyValue("TxValue", ref _txValue, value);
                if (!IsLoading)
                {
                    SetTaxAmount();
                    SetTotalAmount();
                }
            }
        }

        [Appearance("SalesInvoiceLineTaxAmountEnabled", Enabled = false)]
        public double TxAmount
        {
            get { return _txAmount; }
            set {
                SetPropertyValue("TxAmount", ref _txAmount, value);
                if (!IsLoading)
                {
                    _globFunc = new GlobalFunction();
                    if (this._txAmount > 0)
                    {
                        if (_globFunc.GetRoundingList(Session, ObjectList.SalesInvoiceLine, FieldName.TxAmount) == true)
                        {
                            this._txAmount = _globFunc.GetRoundUp(Session, this._txAmount, ObjectList.SalesInvoiceLine, FieldName.TxAmount);
                        }
                    }

                    SetTotalAmount();
                }
            }
        }

        [Appearance("SalesInvoiceLineMultiDiscountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public bool MultiDiscount
        {
            get { return _multiDiscount; }
            set
            {
                SetPropertyValue("MultiDiscount", ref _multiDiscount, value);
                if (!IsLoading)
                {
                    SetNormalDiscount();
                }
            }
        }

        [Appearance("SalesInvoiceLineDiscountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Appearance("SalesInvoiceLineDiscountEnabled", Criteria = "MultiTax = true", Enabled = false)]
        [ImmediatePostData()]
        public Discount Discount
        {
            get { return _discount; }
            set {
                SetPropertyValue("Discount", ref _discount, value);
                if (!IsLoading)
                {
                    if (this._discount != null)
                    {
                        this.Disc = this._discount.Value;
                    }
                    else
                    {
                        SetNormalDiscount();
                    }
                }
            }
        }

        [Appearance("SalesInvoiceLineDiscClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Appearance("SalesInvoiceLineDiscEnabled", Criteria = "MultiTax = true", Enabled = false)]
        [ImmediatePostData()]
        public double Disc
        {
            get { return _disc; }
            set
            {
                SetPropertyValue("Disc", ref _disc, value);
                if (!IsLoading)
                {
                    SetDiscAmount();
                    SetTotalAmount();
                }
            }
        }

        [Appearance("SalesInvoiceLineDiscAmountEnabled", Enabled = false)]
        public double DiscAmount
        {
            get { return _discAmount; }
            set {
                SetPropertyValue("DiscAmount", ref _discAmount, value);
                if (!IsLoading)
                {
                    _globFunc = new GlobalFunction();
                    if (this._discAmount > 0)
                    {
                        if (_globFunc.GetRoundingList(Session, ObjectList.SalesInvoiceLine, FieldName.DiscAmount) == true)
                        {
                            this._discAmount = _globFunc.GetRoundUp(Session, this._discAmount, ObjectList.SalesInvoiceLine, FieldName.DiscAmount);
                        }
                    }
                }
            }
        }

        [Appearance("SalesInvoiceLineTotalAmountEnabled", Enabled = false)]
        public double TAmount
        {
            get { return _tAmount; }
            set {
                SetPropertyValue("TAmount", ref _tAmount, value);
                if (!IsLoading)
                {
                    _globFunc = new GlobalFunction();
                    if (this._tAmount > 0)
                    {
                        if (_globFunc.GetRoundingList(Session, ObjectList.SalesInvoiceLine, FieldName.TAmount) == true)
                        {
                            this._tAmount = _globFunc.GetRoundUp(Session, this._tAmount, ObjectList.SalesInvoiceLine, FieldName.TAmount);
                        }
                    }
                }
            }
        }

        [Appearance("SalesInvoiceLineBillClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double Bill
        {
            get { return _bill; }
            set { SetPropertyValue("Bill", ref _bill, value); }
        }

        #endregion Amount

        #region RemainQty

        [Appearance("SalesInvoiceLineRmDQtyClose", Enabled = false)]
        public double RmDQty
        {
            get { return _rmDQty; }
            set {
                SetPropertyValue("RmDQty", ref _rmDQty, value);
                if (!IsLoading)
                {
                    SetRemainTotalQty();
                }
            }
        }

        [Appearance("SalesInvoiceLineRmQtyClose", Enabled = false)]
        public double RmQty
        {
            get { return _rmQty; }
            set {
                SetPropertyValue("RmQty", ref _rmQty, value);
                if (!IsLoading)
                {
                    SetRemainTotalQty();
                }
            }
        }

        [Appearance("SalesInvoiceLineRmTQtyClose", Enabled = false)]
        public double RmTQty
        {
            get { return _rmTQty; }
            set { SetPropertyValue("RmTQty", ref _rmTQty, value); }
        }

        #endregion RemainQty

        #region PostingQty

        [Appearance("SalesInvoiceLinePDQtyClose", Enabled = false)]
        public double PDQty
        {
            get { return _pDQty; }
            set
            {
                SetPropertyValue("PDQty", ref _pDQty, value);
                if (!IsLoading)
                {
                    SetTotalPQty();
                }
            }
        }

        [Appearance("SalesInvoiceLinePDUOMClose", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure PDUOM
        {
            get { return _pDUom; }
            set
            {
                SetPropertyValue("PDUOM", ref _pDUom, value);
                if (!IsLoading)
                {
                    SetTotalPQty();
                }
            }
        }

        [Appearance("SalesInvoiceLinePQtyToClose", Enabled = false)]
        public double PQty
        {
            get { return _pQty; }
            set
            {
                SetPropertyValue("PQty", ref _pQty, value);
                if (!IsLoading)
                {
                    SetTotalPQty();
                }
            }
        }

        [Appearance("SalesInvoiceLinePUOMClose", Enabled = false)]
        [DataSourceProperty("AvailableUnitOfMeasure", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure PUOM
        {
            get { return _pUom; }
            set
            {
                SetPropertyValue("PUOM", ref _pUom, value);
                if (!IsLoading)
                {
                    SetTotalPQty();
                }
            }
        }

        [Appearance("SalesInvoiceLinePTQtyEnabled", Enabled = false)]
        public double PTQty
        {
            get { return _pTQty; }
            set { SetPropertyValue("PTQty", ref _pTQty, value); }
        }

        #endregion PostingQty

        public TermOfPayment TOP
        {
            get { return _top; }
            set { SetPropertyValue("TOP", ref _top, value); }
        }

        [Browsable(false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        //[Appearance("SalesInvoiceLineStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("SalesInvoiceLineStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("SalesInvoiceLineProcessCountEnabled", Enabled = false)]
        public int ProcessCount
        {
            get { return _processCount; }
            set { SetPropertyValue("ProcessCount", ref _processCount, value); }
        }

        [Appearance("SalesInvoiceLineSignCodeClose", Enabled = false)]
        public string SignCode
        {
            get { return _signCode; }
            set { SetPropertyValue("SignCode", ref _signCode, value); }
        }

        [Association("SalesInvoice-SalesInvoiceLines")]
        public SalesInvoice SalesInvoice
        {
            get { return _salesInvoice; }
            set
            {
                SetPropertyValue("SalesInvoice", ref _salesInvoice, value);
                if (!IsLoading)
                {
                    if (this._salesInvoice != null)
                    {
                        this.Company = this._salesInvoice.Company;
                        if (this._salesInvoice.Currency != null)
                        {
                            this.Currency = this._salesInvoice.Currency;
                        }
                    }
                    
                }
            }
        }

        [Browsable(false)]
        public InventoryTransferOutMonitoring InventoryTransferOutMonitoring
        {
            get { return _inventoryTransferOutMonitoring; }
            set { SetPropertyValue("InventoryTransferOutMonitoring", ref _inventoryTransferOutMonitoring, value); }
        }

        [Browsable(false)]
        public SalesOrderMonitoring SalesOrderMonitoring
        {
            get { return _salesOrderMonitoring; }
            set { SetPropertyValue("SalesOrderMonitoring", ref _salesOrderMonitoring, value); }
        }

        #region Clm

        [Appearance("SalesInvoiceLineTotalDMEnabled", Enabled = false)]
        public double TotalDm
        {
            get { return _totalDM; }
            set { SetPropertyValue("TotalDM", ref _totalDM, value); }
        }

        //grand
        [Browsable(false)]
        public bool HideSumTotalItem
        {
            get { return _hideSumTotalItem; }
            set { SetPropertyValue("HideSumTotalItem", ref _hideSumTotalItem, value); }
        }

        [Appearance("SalesInvoiceLineTotalSILEnabled", Criteria = "HideSumTotalItem = false", Visibility = ViewItemVisibility.Hide)]
        [Persistent("TotalSIL")]
        public double TotalSIL
        {
            get
            {
                double _result = 0;
                if (!IsLoading)
                {
                    if (GetTotalSIL() >= 0)
                    {
                        _result = UpdateTotalSIL(true);
                    }
                }
                return _result;
            }
        }

        //[Browsable(false)]
        [Appearance("SalesInvoiceLineTAmountSILEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double TAmountSIL
        {
            get
            {
                if (this._tAmount >= 0)
                {
                    return GetTotalAmountSIL();
                }
                else
                {
                    return 0;
                }
            }
        }

        //for report
        [Appearance("PurchaseOrderLineReportTSILEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double ReportTSIl
        {
            get
            {
                if (this._tAmount >= 0)
                {
                    return ReportTotalSIL();
                }
                else
                {
                    return 0;
                }
            }
        }

        //[Browsable(false)]
        [Appearance("SalesInvoiceLineReportDiscEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double ReportDisc
        {
            get
            {
                if (this._disc >= 0)
                {
                    return ReportDiscSIL();
                }
                else
                {
                    return 0;
                }
            }
        }

        //[Browsable(false)]
        [Appearance("SalesInvoiceLineReportTaxEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double ReportTax
        {
            get
            {
                if (this._txValue >= 0)
                {
                    return ReportTaxSIL();
                }
                else
                {
                    return 0;
                }
            }
        }

        [Browsable(false)]
        [Appearance("SalesInvoiceLinePTotalPILEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double PTotalPIL
        {
            get { return _PTotalSil; }
            set { SetPropertyValue("PTotalPIL", ref _PTotalSil, value); }
        }

        [Browsable(false)]
        [Appearance("SalesInvoiceLinePTUAmountEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double PTUAmount
        {
            get { return _PTUAmount; }
            set { SetPropertyValue("PTUAmount", ref _PTUAmount, value); }
        }

        [Browsable(false)]
        [Appearance("SalesInvoiceLinePTAmountPILEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double PTAmountPIL
        {
            get { return _PTAmountSIL; }
            set { SetPropertyValue("PTAmountPIL", ref _PTAmountSIL, value); }
        }

        [Browsable(false)]
        [Appearance("SalesInvoiceLinePTPILEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double PTPIL
        {
            get { return _PTSil; }
            set { SetPropertyValue("PTPIL", ref _PTSil, value); }
        }

        [Browsable(false)]
        [Appearance("SalesInvoiceLinePDiscEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double PDisc
        {
            get { return _PDisc; }
            set { SetPropertyValue("PDisc", ref _PDisc, value); }
        }

        [Browsable(false)]
        [Appearance("SalesInvoiceLinePTaxEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double PTax
        {
            get { return _PTax; }
            set { SetPropertyValue("PTax", ref _PTax, value); }
        }

        #endregion Clm

        [Association("SalesInvoiceLine-TaxLines")]
        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        [Appearance("SalesInvoiceLineTaxLineClose", Criteria = "MultiTax = false", Visibility = ViewItemVisibility.Hide, Context = "DetailView")]
        [Appearance("SalesInvoiceLineTaxLineEnabled", Criteria = "MultiTax = true", Visibility = ViewItemVisibility.Show, Context = "DetailView")]
        public XPCollection<TaxLine> TaxLines
        {
            get { return GetCollection<TaxLine>("TaxLines"); }
        }

        [Association("SalesInvoiceLine-DiscountLines")]
        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        [Appearance("SalesInvoiceLineDiscountLineClose", Criteria = "MultiDiscount = false", Visibility = ViewItemVisibility.Hide, Context = "DetailView")]
        [Appearance("SalesInvoiceLineDiscountLineEnabled", Criteria = "MultiDiscount = true", Visibility = ViewItemVisibility.Show, Context = "DetailView")]
        public XPCollection<DiscountLine> DiscountLines
        {
            get { return GetCollection<DiscountLine>("DiscountLines"); }
        }

        #endregion Field

        //===== Code Only =====

        #region No

        public void UpdateNo()
        {
            try
            {
                if (!IsLoading && Session.IsNewObject(this))
                {
                    if (this.SalesInvoice != null)
                    {
                        object _makRecord = Session.Evaluate<SalesInvoiceLine>(CriteriaOperator.Parse("Max(No)"), CriteriaOperator.Parse("SalesInvoice=?", this.SalesInvoice));
                        this.No = Convert.ToInt32(_makRecord) + 1;
                        this.Save();
                        RecoveryUpdateNo();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesInvoiceLine " + ex.ToString());
            }
        }

        public void RecoveryUpdateNo()
        {
            try
            {
                if (this.SalesInvoice != null)
                {
                    SalesInvoice _numHeader = Session.FindObject<SalesInvoice>
                                                (new BinaryOperator("Code", this.SalesInvoice.Code));

                    XPCollection<SalesInvoiceLine> _numLines = new XPCollection<SalesInvoiceLine>
                                                (Session, new BinaryOperator("SalesInvoice", _numHeader),
                                                new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i1 = 0;
                        foreach (SalesInvoiceLine _numLine in _numLines)
                        {
                            i1 += 1;
                            _numLine.No = i1;
                            _numLine.Save();
                        }
                        i1 = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesInvoiceLine " + ex.ToString());
            }
        }

        public void RecoveryDeleteNo()
        {
            try
            {
                if (this.SalesInvoice != null)
                {
                    SalesInvoice _numHeader = Session.FindObject<SalesInvoice>
                                                (new BinaryOperator("Code", this.SalesInvoice.Code));

                    XPCollection<SalesInvoiceLine> _numLines = new XPCollection<SalesInvoiceLine>
                                                (Session, new GroupOperator(GroupOperatorType.And,
                                                 new BinaryOperator("This", this, BinaryOperatorType.NotEqual),
                                                 new BinaryOperator("PurchaseInvoice", _numHeader)),
                                                 new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i = 0;
                        foreach (SalesInvoiceLine _numLine in _numLines)
                        {
                            i += 1;
                            _numLine.No = i;
                            _numLine.Save();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesInvoiceLine " + ex.ToString());
            }
        }

        #endregion No

        #region Set

        private void SetTaxAmount()
        {
            try
            {
                _globFunc = new GlobalFunction();

                if (_uAmount >= 0 && Tax.Value >= 0)
                {
                    if (_globFunc.GetRoundingList(Session, ObjectList.SalesInvoiceLine, FieldName.TxAmount) == true)
                    {
                        this.TxAmount = _globFunc.GetRoundUp(Session, (this.TUAmount * this.TxValue / 100), ObjectList.SalesInvoiceLine, FieldName.TxAmount);
                    }
                    else
                    {
                        this.TxAmount = this.TUAmount * this.TxValue / 100;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrderLine " + ex.ToString());
            }
        }

        private void SetDiscAmount()
        {
            try
            {
                _globFunc = new GlobalFunction();

                if (_uAmount >= 0 && _discAmount >= 0)
                {
                    if (_globFunc.GetRoundingList(Session, ObjectList.SalesInvoiceLine, FieldName.DiscAmount) == true)
                    {
                        this.DiscAmount = _globFunc.GetRoundUp(Session, (this.TUAmount * this.Disc / 100), ObjectList.SalesInvoiceLine, FieldName.DiscAmount);
                    }
                    else
                    {
                        this.DiscAmount = this.TUAmount * this.Disc / 100;
                    }

                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrderLine " + ex.ToString());
            }
        }

        private void SetTotalAmount()
        {
            try
            {
                _globFunc = new GlobalFunction();

                if (_uAmount >= 0 && _txAmount >= 0 && _discAmount >= 0)
                {
                    if (!IsLoading)
                    {
                        if (this.Tax != null)
                        {
                            if (this.Tax.TaxNature == TaxNature.Increase)
                            {
                                if (_globFunc.GetRoundingList(Session, ObjectList.SalesInvoiceLine, FieldName.TAmount) == true)
                                {
                                    this.TAmount = _globFunc.GetRoundUp(Session, (this.TUAmount + this.TxAmount - this.DiscAmount), ObjectList.SalesInvoiceLine, FieldName.TAmount);
                                }
                                else
                                {
                                    this.TAmount = this.TUAmount + this.TxAmount - this.DiscAmount;
                                }
                            }
                            else if (this.Tax.TaxNature == TaxNature.Decrease)
                            {
                                if (_globFunc.GetRoundingList(Session, ObjectList.SalesInvoiceLine, FieldName.TAmount) == true)
                                {
                                    this.TAmount = _globFunc.GetRoundUp(Session, (this.TUAmount - this.TxAmount - this.DiscAmount), ObjectList.SalesInvoiceLine, FieldName.TAmount);
                                }
                                else
                                {
                                    this.TAmount = this.TUAmount - this.TxAmount - this.DiscAmount;
                                }
                            }
                            else if (this.Tax.TaxNature == TaxNature.None)
                            {
                                if (_globFunc.GetRoundingList(Session, ObjectList.SalesInvoiceLine, FieldName.TAmount) == true)
                                {
                                    this.TAmount = _globFunc.GetRoundUp(Session, (this.TUAmount - this.DiscAmount), ObjectList.SalesInvoiceLine, FieldName.TAmount);
                                }
                                else
                                {
                                    this.TAmount = this.TUAmount - this.DiscAmount;
                                }
                            }
                            else
                            {
                                if (_globFunc.GetRoundingList(Session, ObjectList.SalesInvoiceLine, FieldName.TAmount) == true)
                                {
                                    this.TAmount = _globFunc.GetRoundUp(Session, (this.TUAmount + this.TxAmount - this.DiscAmount), ObjectList.SalesInvoiceLine, FieldName.TAmount);
                                }
                                else
                                {
                                    this.TAmount = this.TUAmount + this.TxAmount - this.DiscAmount;
                                }
                            }
                        }
                        //clm
                        else
                        {
                            this.TAmount = this.TUAmount + this.TxAmount - this.DiscAmount;
                        }
                    }
                }
                //clm
                else
                {
                    this.TAmount = this.TUAmount + this.TxAmount - this.DiscAmount;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesInvoiceLine " + ex.ToString());
            }
        }

        private void SetMaxTotalQty()
        {
            try
            {
                double _locInvLineTotal = 0;
                DateTime now = DateTime.Now;
                if (this.SalesInvoice != null)
                {
                    if (this.Item != null && this.MxUOM != null && this.MxDUOM != null)
                    {
                        ItemUnitOfMeasure _locItemUOM = Session.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", this.Item),
                                                         new BinaryOperator("UOM", this.MxUOM),
                                                         new BinaryOperator("DefaultUOM", this.MxDUOM),
                                                         new BinaryOperator("Active", true)));
                        if (_locItemUOM != null)
                        {
                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.MxQty * _locItemUOM.DefaultConversion + this.MxDQty;
                            }
                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.MxQty / _locItemUOM.Conversion + this.MxDQty;
                            }
                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.MxQty + this.MxDQty;
                            }

                            this.MxTQty = _locInvLineTotal;
                        }
                    }
                    else
                    {
                        _locInvLineTotal = this.MxQty + this.MxDQty;
                        this.MxTQty = _locInvLineTotal;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesInvoiceLine " + ex.ToString());
            }
        }

        private void SetTotalUnitAmount()
        {
            try
            {
                _globFunc = new GlobalFunction();

                if (_tQty >= 0 & _uAmount >= 0)
                {
                    if (_globFunc.GetRoundingList(Session, ObjectList.SalesOrderLine, FieldName.TUAmount) == true)
                    {
                        this.TUAmount = _globFunc.GetRoundUp(Session, (this.TQty * this.UAmount), ObjectList.SalesInvoiceLine, FieldName.TUAmount);
                    }
                    else
                    {
                        this.TUAmount = this.TQty * this.UAmount;
                    }

                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrderLine " + ex.ToString());
            }
        }

        private void SetNormalTax()
        {
            try
            {
                if (_tax != null && _txValue > 0 && _txAmount > 0)
                {
                    this.Tax = this.Tax;
                    this.TxValue = this.TxValue;
                    this.TxAmount = this.TxAmount;
                }
                else
                {
                    this.TxValue = 0;
                    this.TxAmount = 0;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesInvoiceLine " + ex.ToString());
            }
        }

        private void SetNormalDiscount()
        {
            try
            {
                if (_discount != null)
                {
                    this.Disc = this.Disc;
                    this.DiscAmount = this.DiscAmount;
                }
                else
                {
                    this.Disc = 0;
                    this.DiscAmount = 0;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesInvoiceLine " + ex.ToString());
            }
        }

        private void SetTotalQty()
        {
            try
            {
                double _locInvLineTotal = 0;
                DateTime now = DateTime.Now;
                if (this.SalesInvoice != null)
                {
                    ItemUnitOfMeasure _locItemUOM = Session.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", this.Item),
                                                         new BinaryOperator("UOM", this.UOM),
                                                         new BinaryOperator("DefaultUOM", this.DUOM),
                                                         new BinaryOperator("Active", true)));
                    if (_locItemUOM != null)
                    {
                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                        {
                            _locInvLineTotal = this.Qty * _locItemUOM.DefaultConversion + this.DQty;
                        }
                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                        {
                            _locInvLineTotal = this.Qty / _locItemUOM.Conversion + this.DQty;
                        }
                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                        {
                            _locInvLineTotal = this.Qty + this.DQty;
                        }

                        this.TQty = _locInvLineTotal;
                    }
                    else
                    {
                        _locInvLineTotal = this.Qty + this.DQty;
                        this.TQty = _locInvLineTotal;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesInvoiceLine " + ex.ToString());
            }
        }

        private void SetRemainTotalQty()
        {
            try
            {
                double _locInvLineTotal = 0;
                DateTime now = DateTime.Now;
                if (this.SalesInvoice != null)
                {
                    ItemUnitOfMeasure _locItemUOM = Session.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", this.Item),
                                                         new BinaryOperator("UOM", this.UOM),
                                                         new BinaryOperator("DefaultUOM", this.DUOM),
                                                         new BinaryOperator("Active", true)));
                    if (_locItemUOM != null)
                    {
                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                        {
                            _locInvLineTotal = this.RmQty * _locItemUOM.DefaultConversion + this.RmDQty;
                        }
                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                        {
                            _locInvLineTotal = this.RmQty / _locItemUOM.Conversion + this.RmDQty;
                        }
                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                        {
                            _locInvLineTotal = this.RmQty + this.RmDQty;
                        }

                        this.RmTQty = _locInvLineTotal;
                    }
                    else
                    {
                        _locInvLineTotal = this.RmQty + this.RmDQty;
                        this.RmTQty = _locInvLineTotal;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesInvoiceLine " + ex.ToString());
            }
        }

        private void SetTotalPQty()
        {
            try
            {
                double _locInvLineTotal = 0;
                DateTime now = DateTime.Now;
                if (this.SalesInvoice != null)
                {
                    if (this.Item != null && this.UOM != null && this.DUOM != null)
                    {
                        ItemUnitOfMeasure _locItemUOM = Session.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", this.Item),
                                                         new BinaryOperator("UOM", this.UOM),
                                                         new BinaryOperator("DefaultUOM", this.DUOM),
                                                         new BinaryOperator("Active", true)));
                        if (_locItemUOM != null)
                        {
                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.PQty * _locItemUOM.DefaultConversion + this.PDQty;
                            }
                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.PQty / _locItemUOM.Conversion + this.PDQty;
                            }
                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.PQty + this.PDQty;
                            }

                            this.PTQty = _locInvLineTotal;
                        }
                    }
                    else
                    {
                        _locInvLineTotal = this.PQty + this.PDQty;
                        this.PTQty = _locInvLineTotal;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesInvoiceLine " + ex.ToString());
            }
        }

        #endregion Set

        #region Clm

        public int GetTotalSIL()
        {
            int _result = 0;
            try
            {
                XPCollection<SalesInvoiceLine> _locSalesinvoiceLines = new XPCollection<SalesInvoiceLine>(Session, new GroupOperator(GroupOperatorType.And,
                                                                       new BinaryOperator("SalesInvoiceLine", this)));

                if (_locSalesinvoiceLines != null && _locSalesinvoiceLines.Count > 0)
                {
                    _result = _locSalesinvoiceLines.Count();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = SalesInvoiceLine", ex.ToString());
            }
            return _result;
        }

        private double GetTotalAmountSIL()
        {
            double _result = 0;
            try
            {
                double _locTotalAmount = 0;
                if (!IsLoading)
                {
                    XPCollection<SalesInvoiceLine> _locSalesInvoiceLines = new XPCollection<SalesInvoiceLine>(Session,
                                                                             new BinaryOperator("SalesInvoice", this.SalesInvoice));

                    if (_locSalesInvoiceLines.Count() >= 0)
                    {
                        foreach (SalesInvoiceLine _locSalesInvoiceLine in _locSalesInvoiceLines)
                        {
                            _locTotalAmount = _locTotalAmount + _locSalesInvoiceLine.TAmount;
                        }
                        _result = _locTotalAmount;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = SalesInvoiceLine" + ex.ToString());
            }

            return _result;
        }

        public double UpdateTotalSIL(bool forceChangeEvents)
        {
            double _result = 0;
            try
            {
                double _locTotalAmount = 0;
                if (!IsLoading)
                {
                    XPCollection<SalesInvoiceLine> _locSalesInvoiceLines = new XPCollection<SalesInvoiceLine>(Session, new GroupOperator(GroupOperatorType.And,
                                                                           new BinaryOperator("SalesInvoiceLine", this)));

                    if (_locSalesInvoiceLines != null && _locSalesInvoiceLines.Count() > 0)
                    {
                        foreach (SalesInvoiceLine _locSalesInvoiceLine in _locSalesInvoiceLines)
                        {
                            _locTotalAmount = _locTotalAmount + _locSalesInvoiceLine.TAmountSIL;
                        }
                        _result = _locTotalAmount;
                    }

                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesInvoiceLine " + ex.ToString());
            }
            return _result;
        }

        //For Report
        private double ReportTotalSIL()
        {
            double _result = 0;
            try
            {
                double _locTotalSil = 0;
                if (!IsLoading)
                {
                    XPCollection<SalesInvoiceLine> _locSalesInvoiceLines = new XPCollection<SalesInvoiceLine>(Session,
                                                                           new BinaryOperator("SalesInvoice", this.SalesInvoice));

                    if (_locSalesInvoiceLines.Count() >= 0)
                    {
                        foreach (SalesInvoiceLine _locSalesInvoiceLine in _locSalesInvoiceLines)
                        {
                            _locTotalSil = _locTotalSil + _locSalesInvoiceLine.TUAmount;
                        }
                        _result = _locTotalSil;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = SalesInvoiceLine" + ex.ToString());
            }

            return _result;
        }

        private double ReportDiscSIL()
        {
            double _result = 0;
            try
            {
                double _locTotalDiscSil = 0;
                if (!IsLoading)
                {
                    XPCollection<SalesInvoiceLine> _locSalesInvoiceLines = new XPCollection<SalesInvoiceLine>(Session,
                                                                             new BinaryOperator("SalesInvoice", this.SalesInvoice));

                    if (_locSalesInvoiceLines.Count() >= 0)
                    {
                        foreach (SalesInvoiceLine _locSalesInvoiceLine in _locSalesInvoiceLines)
                        {
                            _locTotalDiscSil = _locTotalDiscSil + _locSalesInvoiceLine.DiscAmount;
                        }
                        _result = _locTotalDiscSil;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = SalesInvoiceLine" + ex.ToString());
            }

            return _result;
        }

        private double ReportTaxSIL()
        {
            double _result = 0;
            try
            {
                double _locTotalTaxSil = 0;
                if (!IsLoading)
                {
                    XPCollection<SalesInvoiceLine> _locSalesInvoiceLines = new XPCollection<SalesInvoiceLine>(Session,
                                                                           new BinaryOperator("SalesInvoice", this.SalesInvoice));

                    if (_locSalesInvoiceLines.Count() >= 0)
                    {
                        foreach (SalesInvoiceLine _locSalesInvoiceLine in _locSalesInvoiceLines)
                        {
                            _locTotalTaxSil = _locTotalTaxSil + _locSalesInvoiceLine.TxAmount;
                        }
                        _result = _locTotalTaxSil;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = SalesInvoiceLine" + ex.ToString());
            }

            return _result;
        }

        #endregion Clm

    }
}