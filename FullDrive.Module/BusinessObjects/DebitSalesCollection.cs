﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("DebitSalesCollectionRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class DebitSalesCollection : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).

        private bool _activationPosting;
        private string _code;
        private XPCollection<SalesReturn> _availableSalesReturn;
        private SalesReturn _salesReturn;
        private DebitMemo _debitMemo;
        private Status _status;
        private DateTime _statusDate;
        private int _postedCount;
        private GlobalFunction _globFunc;

        public DebitSalesCollection(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.DebitSalesCollection);
                DateTime now = DateTime.Now;
                this.Status = Status.Open;
                this.StatusDate = now;
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("DebitSalesCollectionCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Browsable(false)]
        public XPCollection<SalesReturn> AvailableSalesReturn
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;

                if (!IsLoading)
                {

                    if (this.DebitMemo != null)
                    {
                        if (this.DebitMemo.SalesToCustomer != null)
                        {
                            XPQuery<SalesReturnMonitoring> _salesReturnMonitoringsQuery = new XPQuery<SalesReturnMonitoring>(Session);

                            var _salesReturnMonitorings = from srm in _salesReturnMonitoringsQuery
                                                          where (srm.MemoStatus != Status.Open && srm.ReturnType == ReturnType.DebitMemo
                                                                   && srm.SalesReturn.SalesToCustomer == this.DebitMemo.SalesToCustomer
                                                                   && srm.DebitMemo == null)
                                                                   group srm by srm.SalesReturn into g
                                                                   select new { SalesReturn = g.Key };

                            if (_salesReturnMonitorings != null && _salesReturnMonitorings.Count() > 0)
                            {
                                List<string> _stringSRM = new List<string>();

                                foreach (var _inventoryTransferOutMonitoring in _salesReturnMonitorings)
                                {
                                    if (_inventoryTransferOutMonitoring.SalesReturn != null)
                                    {
                                        if (_inventoryTransferOutMonitoring.SalesReturn.Code != null)
                                        {
                                            _stringSRM.Add(_inventoryTransferOutMonitoring.SalesReturn.Code);
                                        }
                                    }
                                }

                                IEnumerable<string> _stringArraySRMDistinct = _stringSRM.Distinct();
                                string[] _stringArraySRMList = _stringArraySRMDistinct.ToArray();
                                if (_stringArraySRMList.Length == 1)
                                {
                                    for (int i = 0; i < _stringArraySRMList.Length; i++)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _stringArraySRMList[i] + "'";
                                        }
                                    }
                                }
                                else if (_stringArraySRMList.Length > 1)
                                {
                                    for (int i = 0; i < _stringArraySRMList.Length; i++)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _stringArraySRMList[i] + "'";
                                        }
                                        else
                                        {
                                            _endString = _endString + " OR [Code]=='" + _stringArraySRMList[i] + "'";
                                        }
                                    }
                                }
                                _fullString = _beginString + _endString;

                                if (_fullString != null)
                                {
                                    _availableSalesReturn = new XPCollection<SalesReturn>(Session, CriteriaOperator.Parse(_fullString));
                                }
                            }
                        }
                    }
                }
                return _availableSalesReturn;
            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableSalesReturn", DataSourcePropertyIsNullMode.SelectNothing)]
        public SalesReturn SalesReturn
        {
            get { return _salesReturn; }
            set { SetPropertyValue("SalesReturn", ref _salesReturn, value); }
        }

        [ImmediatePostData()]
        [Appearance("DebitSalesCollectionDebitMemoClose", Enabled = false)]
        [Association("DebitMemo-DebitSalesCollections")]
        public DebitMemo DebitMemo
        {
            get { return _debitMemo; }
            set { SetPropertyValue("DebitMemo", ref _debitMemo, value); }
        }

        [Appearance("DebitSalesCollectionStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("DebitSalesCollectionStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("DebitSalesCollectionPostedCountEnabled", Enabled = false)]
        public int PostedCount
        {
            get { return _postedCount; }
            set { SetPropertyValue("PostedCount", ref _postedCount, value); }
        }

        #endregion Field

    }
}