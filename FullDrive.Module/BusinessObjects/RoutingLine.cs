﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using DevExpress.ExpressApp.Editors;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Shipment")]
    [RuleCombinationOfPropertiesIsUnique("RoutingLineRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class RoutingLine : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).

        private bool _activationPosting;
        private int _no;
        private bool _select;
        private string _code;
        private ShipmentType _shipmentType;
        private TransactionTerm _transactionTerm;
        private string _workOrder;
        private TransportType _transportType;
        private XPCollection<ContainerType> _availableContainerType;
        private ContainerType _containerType;
        private PriceType _priceType;
        private DeliveryType _deliveryType;
        private BusinessPartner _carrier;
        private VehicleMode _vehicleMode;
        private XPCollection<Vehicle> _availableVehicle;
        private Vehicle _vehicle;
        private VehicleType _vehicleType;
        private double _actualQuantity;
        private UnitOfMeasure _uom;
        private string _containerNo;
        #region VariableFrom
        private Country _countryFrom;
        private City _cityFrom;
        private XPCollection<Area> _availableAreaFrom;
        private Area _areaFrom;
        private XPCollection<Location> _availableLocationFrom;
        private Location _locationFrom;
        private XPCollection<TransportLocation> _availableTransportLocationFrom;
        private TransportLocation _transportLocationFrom;
        #endregion VariableFrom
        #region VariableTo
        private Country _countryTo;
        private City _cityTo;
        private XPCollection<Area> _availableAreaTo;
        private Area _areaTo;
        private XPCollection<Location> _availableLocationTo;
        private Location _locationTo;
        private XPCollection<TransportLocation> _availableTransportLocationTo;
        private TransportLocation _transportLocationTo;
        #endregion VariableTo
        private DateTime _etd;
        private DateTime _atd;
        private DateTime _eta;
        private DateTime _ata;
        private string _remark;
        private Status _status;
        private DateTime _statusDate;
        private Routing _routing;
        private GlobalFunction _globFunc;

        public RoutingLine(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                DateTime now = DateTime.Now;
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.RoutingLine);
                this.Status = Status.Open;
                this.StatusDate = now;
                this.Select = true;
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [Browsable(false)]
        [ImmediatePostData()]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [Appearance("RoutingNoEnabled", Enabled = false)]
        public int No
        {
            get { return _no; }
            set { SetPropertyValue("No", ref _no, value); }
        }

        [Appearance("RoutingLineSelectClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public bool Select
        {
            get { return _select; }
            set { SetPropertyValue("Select", ref _select, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("RoutingLineCodeEnabled", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("RoutingLineShipmentTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public ShipmentType ShipmentType
        {
            get { return _shipmentType; }
            set { SetPropertyValue("ShipmentType", ref _shipmentType, value); }
        }

        [Appearance("RoutingLineTransactionTermClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public TransactionTerm TransactionTerm
        {
            get { return _transactionTerm; }
            set { SetPropertyValue("TransactionTerm", ref _transactionTerm, value); }
        }

        [Appearance("RoutingLineTransportTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        public TransportType TransportType
        {
            get { return _transportType; }
            set { SetPropertyValue("TransportType", ref _transportType, value); }
        }

        [Browsable(false)]
        public XPCollection<ContainerType> AvailableContainerType
        {
            get
            {
                if (_transportType == null)
                {
                    _availableContainerType = new XPCollection<ContainerType>(Session);
                }
                else
                {
                    _availableContainerType = new XPCollection<ContainerType>(Session,
                                                new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("TransportType", _transportType, BinaryOperatorType.Equal),
                                                    new BinaryOperator("Active", true)));
                }
                return _availableContainerType;
            }
        }

        [Appearance("RoutingLineContainerTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceProperty("AvailableContainerType", DataSourcePropertyIsNullMode.SelectAll)]
        public ContainerType ContainerType
        {
            get { return _containerType; }
            set { SetPropertyValue("ContainerType", ref _containerType, value); }
        }

        [Appearance("RoutingLinePriceTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PriceType PriceType
        {
            get { return _priceType; }
            set { SetPropertyValue("PriceType", ref _priceType, value); }
        }

        [Appearance("RoutingLineDeliveryTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DeliveryType DeliveryType
        {
            get { return _deliveryType; }
            set { SetPropertyValue("DeliveryType", ref _deliveryType, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("RoutingLineWorkOrderClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string WorkOrder
        {
            get { return _workOrder; }
            set { SetPropertyValue("WorkOrder", ref _workOrder, value); }
        }

        [ImmediatePostData()]
        [Appearance("RoutingLineCarrierClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BusinessPartner Carrier
        {
            get { return _carrier; }
            set { SetPropertyValue("Carrier", ref _carrier, value); }
        }

        [ImmediatePostData()]
        [Appearance("RoutingLineVehicleModeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public VehicleMode VehicleMode
        {
            get { return _vehicleMode; }
            set { SetPropertyValue("VehicleMode", ref _vehicleMode, value); }
        }

        [Browsable(false)]
        public XPCollection<Vehicle> AvailableVehicle
        {
            get
            {
                if (this.Carrier == null && this.VehicleMode == VehicleMode.None)
                {
                    _availableVehicle = new XPCollection<Vehicle>(Session);
                }
                else
                {
                    if(this.VehicleMode != VehicleMode.None)
                    {
                        if(this.Carrier != null)
                        {
                            _availableVehicle = new XPCollection<Vehicle>(Session,
                                                new GroupOperator(GroupOperatorType.And,
                                                new BinaryOperator("VehicleMode", this.VehicleMode),
                                                new BinaryOperator("BusinessPartner", this.Carrier),
                                                new BinaryOperator("Active", true)));
                        }else
                        {
                            _availableVehicle = new XPCollection<Vehicle>(Session,
                                                new GroupOperator(GroupOperatorType.And,
                                                new BinaryOperator("VehicleMode", this.VehicleMode),
                                                new BinaryOperator("Active", true)));
                        }
                    }
                    
                }
                return _availableVehicle;
            }
        }

        [ImmediatePostData()]
        [Appearance("RoutingLineVehicleClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceProperty("AvailableVehicle", DataSourcePropertyIsNullMode.SelectAll)]
        public Vehicle Vehicle
        {
            get { return _vehicle; }
            set {
                SetPropertyValue("Vehicle", ref _vehicle, value);
                if(this._vehicle != null)
                {
                    if(this._vehicle.UOM != null)
                    {
                        this.UOM = this._vehicle.UOM;
                    }
                    if(this._vehicle.Capacity > 0)
                    {
                        this.ActualQuantity = this._vehicle.Capacity;
                    }
                    if(this._vehicle.VehicleType != null)
                    {
                        this._vehicleType = this._vehicle.VehicleType;
                    }
                }
            }
        }

        [Appearance("RoutingLineVehicleTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public VehicleType VehicleType
        {
            get { return _vehicleType; }
            set { SetPropertyValue("VehicleType", ref _vehicleType, value); }
        }

        [Appearance("RoutingLineActualQuantityClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double ActualQuantity
        {
            get { return _actualQuantity; }
            set { SetPropertyValue("ActualQuantity", ref _actualQuantity, value); }
        }

        [Appearance("RoutingLineUOMClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure UOM
        {
            get { return _uom; }
            set { SetPropertyValue("UOM", ref _uom, value); }
        }

        [Appearance("RoutingLineContainerNoClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string ContainerNo
        {
            get { return _containerNo; }
            set { SetPropertyValue("ContainerNo", ref _containerNo, value); }
        }

        #region From

        [ImmediatePostData()]
        [Appearance("RoutingLineCountryFromClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Country CountryFrom
        {
            get { return _countryFrom; }
            set { SetPropertyValue("CountryFrom", ref _countryFrom, value); }
        }

        [ImmediatePostData()]
        [Appearance("RoutingLineCityFromClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public City CityFrom
        {
            get { return _cityFrom; }
            set { SetPropertyValue("CityFrom", ref _cityFrom, value); }
        }

        [Browsable(false)]
        public XPCollection<Area> AvailableAreaFrom
        {
            get
            {
                if (this.CountryFrom != null && this.CityFrom != null)
                {
                    _availableAreaFrom = new XPCollection<Area>(Session,
                                    new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("Country", this.CountryFrom),
                                    new BinaryOperator("City", this.CityFrom)));

                }
                else if (this.CountryFrom != null && this.CityFrom == null)
                {
                    _availableAreaFrom = new XPCollection<Area>(Session,
                                    new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("Country", this.CountryFrom)));
                }
                else if (this.CountryFrom == null && this.CityFrom != null)
                {
                    _availableAreaFrom = new XPCollection<Area>(Session,
                                    new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("City", this.CityFrom)));
                }
                else
                {
                    _availableAreaFrom = new XPCollection<Area>(Session);
                }

                return _availableAreaFrom;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableAreaFrom", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("RoutingLineAreaFromClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public Area AreaFrom
        {
            get { return _areaFrom; }
            set { SetPropertyValue("AreaFrom", ref _areaFrom, value); }
        }

        [Browsable(false)]
        public XPCollection<Location> AvailableLocationFrom
        {
            get
            {
                if (this.AreaFrom != null)
                {
                    _availableLocationFrom = new XPCollection<Location>(Session,
                                          new GroupOperator(GroupOperatorType.And,
                                          new BinaryOperator("Area", this.AreaFrom),
                                          new BinaryOperator("Active", true)));

                }
                else
                {
                    _availableLocationFrom = new XPCollection<Location>(Session);
                }

                return _availableLocationFrom;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableLocationFrom", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("RoutingLineLocationFromClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public Location LocationFrom
        {
            get { return _locationFrom; }
            set { SetPropertyValue("LocationFrom", ref _locationFrom, value); }
        }

        [Browsable(false)]
        public XPCollection<TransportLocation> AvailableTransportLocationFrom
        {
            get
            {
                if (this.CountryFrom != null)
                {
                    _availableTransportLocationFrom = new XPCollection<TransportLocation>(Session,
                                          new GroupOperator(GroupOperatorType.And,
                                          new BinaryOperator("Country", this.CountryFrom),
                                          new BinaryOperator("Active", true)));

                }
                else
                {
                    _availableTransportLocationFrom = new XPCollection<TransportLocation>(Session);
                }

                return _availableTransportLocationFrom;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableTransportLocationFrom", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("RoutingLineTransportLocationFromClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public TransportLocation TransportLocationFrom
        {
            get { return _transportLocationFrom; }
            set { SetPropertyValue("TransportLocationFrom", ref _transportLocationFrom, value); }
        }

        #endregion From

        #region To
        [ImmediatePostData()]
        [Appearance("RoutingLineCountryToClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Country CountryTo
        {
            get { return _countryTo; }
            set { SetPropertyValue("CountryTo", ref _countryTo, value); }
        }

        [Appearance("RoutingLineCityToClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public City CityTo
        {
            get { return _cityTo; }
            set { SetPropertyValue("CityTo", ref _cityTo, value); }
        }

        [Browsable(false)]
        public XPCollection<Area> AvailableAreaTo
        {
            get
            {
                if (this.CountryTo != null && this.CityTo != null)
                {
                    _availableAreaTo = new XPCollection<Area>(Session,
                                    new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("Country", this.CountryTo),
                                    new BinaryOperator("City", this.CityTo)));

                }
                else if (this.CountryTo != null && this.CityTo == null)
                {
                    _availableAreaTo = new XPCollection<Area>(Session,
                                    new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("Country", this.CountryTo)));
                }
                else if (this.CountryTo == null && this.CityTo != null)
                {
                    _availableAreaTo = new XPCollection<Area>(Session,
                                    new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("City", this.CityTo)));
                }
                else
                {
                    _availableAreaTo = new XPCollection<Area>(Session);
                }

                return _availableAreaTo;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableAreaTo", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("RoutingLineAreaToClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public Area AreaTo
        {
            get { return _areaTo; }
            set { SetPropertyValue("AreaTo", ref _areaTo, value); }
        }

        [Browsable(false)]
        public XPCollection<Location> AvailableLocationTo
        {
            get
            {
                if (this.AreaTo != null)
                {
                    _availableLocationTo = new XPCollection<Location>(Session,
                                          new GroupOperator(GroupOperatorType.And,
                                          new BinaryOperator("Area", this.AreaTo),
                                          new BinaryOperator("Active", true)));

                }
                else
                {
                    _availableLocationTo = new XPCollection<Location>(Session);
                }

                return _availableLocationTo;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableLocationTo", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("RoutingLineLocationToClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public Location LocationTo
        {
            get { return _locationTo; }
            set { SetPropertyValue("LocationTo", ref _locationTo, value); }
        }

        [Browsable(false)]
        public XPCollection<TransportLocation> AvailableTransportLocationTo
        {
            get
            {
                if (this.CountryTo != null)
                {
                    _availableTransportLocationTo = new XPCollection<TransportLocation>(Session,
                                          new GroupOperator(GroupOperatorType.And,
                                          new BinaryOperator("Country", this.CountryTo),
                                          new BinaryOperator("Active", true)));

                }
                else
                {
                    _availableTransportLocationTo = new XPCollection<TransportLocation>(Session);
                }

                return _availableTransportLocationTo;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableTransportLocationTo", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("RoutingLineTransportLocationToClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public TransportLocation TransportLocationTo
        {
            get { return _transportLocationTo; }
            set { SetPropertyValue("TransportLocationTo", ref _transportLocationTo, value); }
        }

        #endregion To

        #region Time

        [Appearance("RoutingLineETDClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yy H:mm:ss}")]
        [ModelDefault("EditMask", "dd/MM/yy H:mm:ss")]
        [EditorAlias(FullDriveCenterEditorAliases.CustomDateTimeEditor)]
        public DateTime ETD
        {
            get { return _etd; }
            set { SetPropertyValue("ETD", ref _etd, value); }
        }

        [Appearance("RoutingLineATDClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yy H:mm:ss}")]
        [ModelDefault("EditMask", "dd/MM/yy H:mm:ss")]
        [EditorAlias(FullDriveCenterEditorAliases.CustomDateTimeEditor)]
        public DateTime ATD
        {
            get { return _atd; }
            set { SetPropertyValue("ATD", ref _atd, value); }
        }

        [Appearance("RoutingLineETAClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yy H:mm:ss}")]
        [ModelDefault("EditMask", "dd/MM/yy H:mm:ss")]
        [EditorAlias(FullDriveCenterEditorAliases.CustomDateTimeEditor)]
        public DateTime ETA
        {
            get { return _eta; }
            set { SetPropertyValue("ETA", ref _eta, value); }
        }

        [Appearance("RoutingLineATAClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yy H:mm:ss}")]
        [ModelDefault("EditMask", "dd/MM/yy H:mm:ss")]
        [EditorAlias(FullDriveCenterEditorAliases.CustomDateTimeEditor)]
        public DateTime ATA
        {
            get { return _ata; }
            set { SetPropertyValue("ATA", ref _ata, value); }
        }

        #endregion Time

        [Appearance("RoutingLineRemarkClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Size(512)]
        public string Remark
        {
            get { return _remark; }
            set { SetPropertyValue("Remark", ref _remark, value); }
        }

        [Appearance("RoutingLineStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("RoutingLineStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [ImmediatePostData()]
        [Appearance("RoutingLineRoutingClose", Enabled = false)]
        [Association("Routing-RoutingLines")]
        public Routing Routing
        {
            get { return _routing; }
            set {
                SetPropertyValue("Routing", ref _routing, value);
                if (!IsLoading)
                {
                    if (this._routing != null)
                    {

                        if (this._routing.ShipmentType != ShipmentType.None)
                        {
                            this.ShipmentType = this._routing.ShipmentType;
                        }
                        if (this._routing.TransportType != null)
                        {
                            this.TransportType = this._routing.TransportType;
                        }
                        if (this._routing.CountryFrom != null)
                        {
                            this.CountryFrom = this._routing.CountryFrom;
                        }
                        if (this._routing.CityFrom != null)
                        {
                            this.CityFrom = this._routing.CityFrom;
                        }
                        if (this._routing.CountryTo != null)
                        {
                            this.CountryTo = this._routing.CountryTo;
                        }
                        if (this._routing.CityTo != null)
                        {
                            this.CityTo = this._routing.CityTo;
                        }

                    }
                }
            }
        }

        #endregion Field

        //=========================================================== Code Only =====================================================

        #region No

        public void UpdateNo()
        {
            try
            {
                if (!IsLoading && Session.IsNewObject(this))
                {
                    if (this.Routing != null)
                    {
                        object _makRecord = Session.Evaluate<RoutingLine>(CriteriaOperator.Parse("Max(No)"), CriteriaOperator.Parse("Routing=?", this.Routing));
                        this.No = Convert.ToInt32(_makRecord) + 1;
                        this.Save();
                        RecoveryUpdateNo();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = RoutingLine " + ex.ToString());
            }
        }

        public void RecoveryUpdateNo()
        {
            try
            {
                if (this.Routing != null)
                {
                    Routing _numHeader = Session.FindObject<Routing>
                                            (new BinaryOperator("Code", this.Routing.Code));

                    XPCollection<RoutingLine> _numLines = new XPCollection<RoutingLine>
                                                             (Session, new BinaryOperator("Routing", _numHeader),
                                                              new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i1 = 0;
                        foreach (RoutingLine _numLine in _numLines)
                        {
                            i1 += 1;
                            _numLine.No = i1;
                            _numLine.Save();
                        }
                        i1 = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = RoutingLine " + ex.ToString());
            }
        }

        public void RecoveryDeleteNo()
        {
            try
            {
                if (this.Routing != null)
                {
                    FreightForwardingOrigin _numHeader = Session.FindObject<FreightForwardingOrigin>
                                            (new BinaryOperator("Code", this.Routing.Code));

                    XPCollection<RoutingLine> _numLines = new XPCollection<RoutingLine>
                                                             (Session, new GroupOperator(GroupOperatorType.And,
                                                              new BinaryOperator("This", this, BinaryOperatorType.NotEqual),
                                                              new BinaryOperator("Routing", _numHeader)),
                                                              new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i = 0;
                        foreach (RoutingLine _numLine in _numLines)
                        {
                            i += 1;
                            _numLine.No = i;
                            _numLine.Save();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = RoutingLine " + ex.ToString());
            }
        }

        #endregion No

        [Action(Caption = "GetRouting", ConfirmationMessage = "Are you sure?", AutoCommit = false)]
        public void GetDataRouting()
        {
            try
            {
                if(this.Routing != null)
                {
              
                    this.TransportType = this.Routing.TransportType;
                    this.CountryFrom = this.Routing.CountryFrom;
                    this.CityFrom = this.Routing.CityFrom;
                    this.AreaFrom = this.Routing.AreaFrom;
                    this.TransportLocationFrom = this.Routing.TransportLocationFrom;
                    this.CountryTo = this.Routing.CountryTo;
                    this.CityTo = this.Routing.CityTo;
                    this.AreaTo = this.Routing.AreaTo;
                    this.TransportLocationTo = this.Routing.TransportLocationTo;
                    this.ETD = this.Routing.ETD;
                    this.ETA = this.Routing.ETA;
                    
                }
                
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = RoutingLine " + ex.ToString());
            }
        }

        [Action(Caption = "GetPreviousData", ConfirmationMessage = "Are you sure?", AutoCommit = false)]
        public void GetPreviousData()
        {
            try
            {
                RoutingLine _dataLine = null;
                int _locNo = 0;
                if (this.Routing != null )
                {
                    if(this.WorkOrder != null)
                    {
                        object _makRecord = Session.Evaluate<RoutingLine>(CriteriaOperator.Parse("Max(No)"), CriteriaOperator.Parse("Routing=? and WorkOrder=?", this.Routing, this.WorkOrder));
                        _locNo = Convert.ToInt32(_makRecord);

                        if(_locNo > 0)
                        {
                            _dataLine = Session.FindObject<RoutingLine>
                                (new GroupOperator(GroupOperatorType.And,
                                new BinaryOperator("Routing", this.Routing),
                                new BinaryOperator("WorkOrder", WorkOrder),
                                new BinaryOperator("No", _locNo)));
                        }
                        
                    }else
                    {
                        object _makRecord = Session.Evaluate<RoutingLine>(CriteriaOperator.Parse("Max(No)"), CriteriaOperator.Parse("Routing=? ", this.Routing));
                        _locNo = Convert.ToInt32(_makRecord);

                        if(_locNo > 0)
                        {
                            _dataLine = Session.FindObject<RoutingLine>
                                (new GroupOperator(GroupOperatorType.And,
                                new BinaryOperator("Routing", this.Routing),
                                new BinaryOperator("No", _locNo)));
                        }
                        
                    }
                    
                    if (_dataLine != null)
                    {
                        this.WorkOrder = _dataLine.WorkOrder;
                        this.PriceType = _dataLine.PriceType;
                        this.ContainerType = _dataLine.ContainerType;
                        this.DeliveryType = _dataLine.DeliveryType;
                        this.TransportType = _dataLine.TransportType;
                        this.Carrier = _dataLine.Carrier;
                        this.Vehicle = _dataLine.Vehicle;
                        this.CountryFrom = _dataLine.CountryTo;
                        this.CityFrom = _dataLine.CityTo;
                        this.AreaFrom = _dataLine.AreaTo;
                        this.TransportLocationFrom = _dataLine.TransportLocationTo;
                        this.ActualQuantity = _dataLine.ActualQuantity;
                        this.UOM = _dataLine.UOM;
                        this.ETD = this.Routing.ETD;
                        this.ETA = this.Routing.ETA;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = RoutingLine " + ex.ToString());
            }

        }
    }
}