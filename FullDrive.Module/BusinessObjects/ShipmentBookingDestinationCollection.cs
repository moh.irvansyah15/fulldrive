﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Shipment")]
    [RuleCombinationOfPropertiesIsUnique("ShipmentBookingDestinationCollectionRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class ShipmentBookingDestinationCollection : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).

        private bool _activationPosting;
        private string _code;
        private XPCollection<ShipmentBooking> _availableShipmentBooking;
        private ShipmentBooking _shipmentBooking;
        private FreightForwardingDestination _freightForwardingDestination;
        private Status _status;
        private DateTime _statusDate;
        private int _postedCount;
        private GlobalFunction _globFunc;

        public ShipmentBookingDestinationCollection(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.ShipmentBookingDestinationCollection);
                DateTime now = DateTime.Now;
                this.Status = Status.Open;
                this.StatusDate = now;
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("ShipmentBookingDestinationCollectionCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Browsable(false)]
        public XPCollection<ShipmentBooking> AvailableShipmentBooking
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;

                if (!IsLoading)
                {
                    if (this.FreightForwardingDestination != null)
                    {
                        if (this.FreightForwardingDestination.ShipmentType == ShipmentType.Import || this.FreightForwardingDestination.ShipmentType == ShipmentType.ExportAndImport )
                        {
                            if (this.FreightForwardingDestination.Shipper != null && this.FreightForwardingDestination.Consignee != null)
                            {
                                XPQuery<ShipmentBookingMonitoring> _salesShipmentBookingMonitoringsQuery = new XPQuery<ShipmentBookingMonitoring>(Session);

                                var _shipmentBookingMonitorings = from sbm in _salesShipmentBookingMonitoringsQuery
                                                                  where ((sbm.Status == Status.Open || sbm.Status == Status.Posted)
                                                                  && sbm.ShipmentType == this.FreightForwardingDestination.ShipmentType
                                                                  && sbm.ShipmentBooking.Shipper == this.FreightForwardingDestination.Shipper
                                                                  && sbm.ShipmentBooking.Consignee == this.FreightForwardingDestination.Consignee
                                                                  && sbm.FreightForwardingDestination == null)
                                                                  group sbm by sbm.ShipmentBooking into g
                                                                  select new { ShipmentBooking = g.Key };

                                if (_shipmentBookingMonitorings != null && _shipmentBookingMonitorings.Count() > 0)
                                {
                                    List<string> _stringSBM = new List<string>();

                                    foreach (var _shipmentBookingMonitoring in _shipmentBookingMonitorings)
                                    {
                                        if (_shipmentBookingMonitoring != null)
                                        {
                                            if (_shipmentBookingMonitoring.ShipmentBooking.Code != null)
                                            {
                                                _stringSBM.Add(_shipmentBookingMonitoring.ShipmentBooking.Code);
                                            }
                                        }
                                    }

                                    IEnumerable<string> _stringArraySBMDistinct = _stringSBM.Distinct();
                                    string[] _stringArraySBMList = _stringArraySBMDistinct.ToArray();
                                    if (_stringArraySBMList.Length == 1)
                                    {
                                        for (int i = 0; i < _stringArraySBMList.Length; i++)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _stringArraySBMList[i] + "'";
                                            }
                                        }
                                    }
                                    else if (_stringArraySBMList.Length > 1)
                                    {
                                        for (int i = 0; i < _stringArraySBMList.Length; i++)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _stringArraySBMList[i] + "'";
                                            }
                                            else
                                            {
                                                _endString = _endString + " OR [Code]=='" + _stringArraySBMList[i] + "'";
                                            }
                                        }
                                    }
                                    _fullString = _beginString + _endString;

                                    if (_fullString != null)
                                    {
                                        _availableShipmentBooking = new XPCollection<ShipmentBooking>(Session, CriteriaOperator.Parse(_fullString));
                                    }
                                }
                            }
                        }
                    }
                }

                return _availableShipmentBooking;
            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableShipmentBooking", DataSourcePropertyIsNullMode.SelectNothing)]
        public ShipmentBooking ShipmentBooking
        {
            get { return _shipmentBooking; }
            set { SetPropertyValue("ShipmentBooking", ref _shipmentBooking, value); }
        }

        [ImmediatePostData()]
        [Appearance("ShipmentBookingDestinationCollectionFreightForwardingDestinationClose", Enabled = false)]
        [Association("FreightForwardingDestination-ShipmentBookingDestinationCollections")]
        public FreightForwardingDestination FreightForwardingDestination
        {
            get { return _freightForwardingDestination; }
            set { SetPropertyValue("FreightForwardingDestination", ref _freightForwardingDestination, value); }
        }

        [Appearance("ShipmentBookingDestinationCollectionStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("ShipmentBookingDestinationCollectionStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("ShipmentBookingDestinationCollectionPostedCountEnabled", Enabled = false)]
        public int PostedCount
        {
            get { return _postedCount; }
            set { SetPropertyValue("PostedCount", ref _postedCount, value); }
        }
        #endregion Field
    }
}