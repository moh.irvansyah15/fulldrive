﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using DevExpress.ExpressApp.Editors;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Master")]
    [RuleCombinationOfPropertiesIsUnique("ShipmentApprovalLineRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class ShipmentApprovalLine : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).

        private string _code;
        private ApprovalLevel _approvalLevel;
        private Status _approvalStatus;
        private DateTime _approvalDate;
        private bool _endApproval;
        private Employee _approvedBy;
        private string _userAccess;
        private ShipmentBooking _shipmentBooking;
        private FreightForwardingOrigin _freightForwardingOrigin;
        private FreightForwardingDestination _freightForwardingDestination;
        private Routing _routing;
        private ShipmentPurchaseInvoice _shipmentPurchaseInvoice;
        private ShipmentSalesInvoice _shipmentSalesInvoice;
        private GlobalFunction _globFunc;

        public ShipmentApprovalLine(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.ShipmentApprovalLine);
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).

            #region UserAccess

            _userAccess = SecuritySystem.CurrentUserName;
            UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));

            if (Session.IsNewObject(this))
            {
                if (_locUserAccess != null)
                {
                    if (_locUserAccess.Employee != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            ApprovedBy = _locUserAccess.Employee;
                        }
                    }
                }
            }

            #endregion UserAccess
        }

        #region Field

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("ShipmentApprovalLineCodeEnabled", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("ShipmentApprovalLineApprovalLevelEnabled", Enabled = false)]
        public ApprovalLevel ApprovalLevel
        {
            get { return _approvalLevel; }
            set { SetPropertyValue("ApprovalLevel", ref _approvalLevel, value); }
        }

        [Appearance("ShipmentApprovalLineApprovalStatusEnabled", Enabled = false)]
        public Status ApprovalStatus
        {
            get { return _approvalStatus; }
            set { SetPropertyValue("ApprovalStatus", ref _approvalStatus, value); }
        }

        [Appearance("ShipmentApprovalLineApprovalDateEnabled", Enabled = false)]
        public DateTime ApprovalDate
        {
            get { return _approvalDate; }
            set { SetPropertyValue("ApprovalDate", ref _approvalDate, value); }
        }

        [Appearance("ShipmentApprovalLineEndApprovalEnabled", Enabled = false)]
        public bool EndApproval
        {
            get { return _endApproval; }
            set { SetPropertyValue("EndApproval", ref _endApproval, value); }
        }

        [Appearance("ShipmentApprovalLineApprovedByEnabled", Enabled = false)]
        public Employee ApprovedBy
        {
            get { return _approvedBy; }
            set { SetPropertyValue("ApprovedBy", ref _approvedBy, value); }
        }

        [Appearance("ShipmentApprovalLineShipmentBookingEnabled", Enabled = false)]
        [Association("ShipmentBooking-ShipmentApprovalLines")]
        public ShipmentBooking ShipmentBooking
        {
            get { return _shipmentBooking; }
            set { SetPropertyValue("ShipmentBooking", ref _shipmentBooking, value); }
        }

        [Association("FreightForwardingOrigin-ShipmentApprovalLines")]
        public FreightForwardingOrigin FreightForwardingOrigin
        {
            get { return _freightForwardingOrigin; }
            set { SetPropertyValue("FreightForwardingOrigin", ref _freightForwardingOrigin, value); }
        }

        [Association("FreightForwardingDestination-ShipmentApprovalLines")]
        public FreightForwardingDestination FreightForwardingDestination
        {
            get { return _freightForwardingDestination; }
            set { SetPropertyValue("FreightForwardingDestination", ref _freightForwardingDestination, value); }
        }

        [Association("Routing-ShipmentApprovalLines")]
        public Routing Routing
        {
            get { return _routing; }
            set { SetPropertyValue("Routing", ref _routing, value); }
        }

        [Association("ShipmentPurchaseInvoice-ShipmentApprovalLines")]
        public ShipmentPurchaseInvoice ShipmentPurchaseInvoice
        {
            get { return _shipmentPurchaseInvoice; }
            set { SetPropertyValue("ShipmentPurchaseInvoice", ref _shipmentPurchaseInvoice, value); }
        }

        [Association("ShipmentSalesInvoice-ShipmentApprovalLines")]
        public ShipmentSalesInvoice ShipmentSalesInvoice
        {
            get { return _shipmentSalesInvoice; }
            set { SetPropertyValue("ShipmentSalesInvoice", ref _shipmentSalesInvoice, value); }
        }

        #endregion Field
    }
}