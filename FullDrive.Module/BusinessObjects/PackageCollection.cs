﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Shipment")]
    [RuleCombinationOfPropertiesIsUnique("PackageCollectionRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class PackageCollection : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).

        private bool _activationPosting;
        private string _code;
        private ShipmentType _shipmentType;
        #region VariableFrom
        private Country _countryFrom;
        private City _cityFrom;
        private XPCollection<Area> _availableAreaFrom;
        private Area _areaFrom;
        private XPCollection<Location> _availableLocationFrom;
        private Location _locationFrom;
        private XPCollection<TransportLocation> _availableTransportLocationFrom;
        private TransportLocation _transportLocationFrom;
        private XPCollection<BusinessPartner> _availableShipper;
        private BusinessPartner _shipper;
        #endregion VariableFrom
        #region VariableTo
        private Country _countryTo;
        private City _cityTo;
        private XPCollection<Area> _availableAreaTo;
        private Area _areaTo;
        private XPCollection<Location> _availableLocationTo;
        private Location _locationTo;
        private XPCollection<TransportLocation> _availableTransportLocationTo;
        private TransportLocation _transportLocationTo;
        private XPCollection<BusinessPartner> _availableConsignee;
        private BusinessPartner _consignee;
        #endregion VariableTo
        private XPCollection<FreightForwardingOrigin> _availableFreightForwardingOrigin;
        private FreightForwardingOrigin _freightForwardingOrigin;
        private XPCollection<FreightForwardingDestination> _availableFreightForwardingDestination;
        private FreightForwardingDestination _freightForwardingDestination;
        private Routing _routing;
        private Status _status;
        private DateTime _statusDate;
        private int _postedCount;
        private GlobalFunction _globFunc;


        public PackageCollection(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.PackageCollection);
                DateTime now = DateTime.Now;
                this.Status = Status.Open;
                this.StatusDate = now;
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("PackageCollectionCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [ImmediatePostData()]
        public ShipmentType ShipmentType
        {
            get { return _shipmentType; }
            set { SetPropertyValue("ShipmentType", ref _shipmentType, value); }
        }

        #region From
        [ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("PackageCollectionCountryFromClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Country CountryFrom
        {
            get { return _countryFrom; }
            set { SetPropertyValue("CountryFrom", ref _countryFrom, value); }
        }

        [Appearance("PackageCollectionCityFromClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public City CityFrom
        {
            get { return _cityFrom; }
            set { SetPropertyValue("CityFrom", ref _cityFrom, value); }
        }

        [Browsable(false)]
        public XPCollection<Area> AvailableAreaFrom
        {
            get
            {
                if (this.CountryFrom != null && this.CityFrom != null)
                {
                    _availableAreaFrom = new XPCollection<Area>(Session,
                                    new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("Country", this.CountryFrom),
                                    new BinaryOperator("City", this.CityFrom)));

                }
                else if (this.CountryFrom != null && this.CityFrom == null)
                {
                    _availableAreaFrom = new XPCollection<Area>(Session,
                                    new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("Country", this.CountryFrom)));
                }
                else if (this.CountryFrom == null && this.CityFrom != null)
                {
                    _availableAreaFrom = new XPCollection<Area>(Session,
                                    new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("City", this.CityFrom)));
                }
                else
                {
                    _availableAreaFrom = new XPCollection<Area>(Session);
                }

                return _availableAreaFrom;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableAreaFrom", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("PackageCollectionAreaFromClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public Area AreaFrom
        {
            get { return _areaFrom; }
            set { SetPropertyValue("AreaFrom", ref _areaFrom, value); }
        }

        [Browsable(false)]
        public XPCollection<Location> AvailableLocationFrom
        {
            get
            {
                if (this.AreaFrom != null)
                {
                    _availableLocationFrom = new XPCollection<Location>(Session,
                                          new GroupOperator(GroupOperatorType.And,
                                          new BinaryOperator("Area", this.AreaFrom),
                                          new BinaryOperator("Active", true)));

                }
                else
                {
                    _availableLocationFrom = new XPCollection<Location>(Session);
                }

                return _availableLocationFrom;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableLocationFrom", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("PackageCollectionLocationFromClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public Location LocationFrom
        {
            get { return _locationFrom; }
            set { SetPropertyValue("LocationFrom", ref _locationFrom, value); }
        }

        [Browsable(false)]
        public XPCollection<TransportLocation> AvailableTransportLocationFrom
        {
            get
            {
                if (this.CountryFrom != null)
                {
                    _availableTransportLocationFrom = new XPCollection<TransportLocation>(Session,
                                          new GroupOperator(GroupOperatorType.And,
                                          new BinaryOperator("Country", this.CountryFrom),
                                          new BinaryOperator("Active", true)));

                }
                else
                {
                    _availableTransportLocationFrom = new XPCollection<TransportLocation>(Session);
                }

                return _availableTransportLocationFrom;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableTransportLocationFrom", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("PackageCollectionTransportLocationFromClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public TransportLocation TransportLocationFrom
        {
            get { return _transportLocationFrom; }
            set { SetPropertyValue("TransportLocationFrom", ref _transportLocationFrom, value); }
        }

        [Browsable(false)]
        public XPCollection<BusinessPartner> AvailableShipper
        {
            get
            {
                if (this.CountryFrom != null)
                {
                    if (this.CityFrom != null)
                    {
                        _availableShipper = new XPCollection<BusinessPartner>
                                            (Session, new GroupOperator(GroupOperatorType.And,
                                            new BinaryOperator("Country", this.CountryFrom),
                                            new BinaryOperator("City", this.CityFrom)));
                    }
                    else
                    {
                        _availableShipper = new XPCollection<BusinessPartner>
                                            (Session, new GroupOperator(GroupOperatorType.And,
                                            new BinaryOperator("Country", this.CountryFrom)));
                    }
                }
                else
                {
                    _availableShipper = new XPCollection<BusinessPartner>(Session);
                }


                return _availableShipper;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableShipper", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("PackageCollectionShipperClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BusinessPartner Shipper
        {
            get { return _shipper; }
            set { SetPropertyValue("Shipper", ref _shipper, value); }
        }

        #endregion From

        #region To
        [Appearance("PackageCollectionCountryToClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Country CountryTo
        {
            get { return _countryTo; }
            set { SetPropertyValue("CountryTo", ref _countryTo, value); }
        }

        [Appearance("PackageCollectionCityToClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public City CityTo
        {
            get { return _cityTo; }
            set { SetPropertyValue("CityTo", ref _cityTo, value); }
        }

        [Browsable(false)]
        public XPCollection<Area> AvailableAreaTo
        {
            get
            {
                if (this.CountryTo != null && this.CityTo != null)
                {
                    _availableAreaTo = new XPCollection<Area>(Session,
                                    new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("Country", this.CountryTo),
                                    new BinaryOperator("City", this.CityTo)));

                }
                else if (this.CountryTo != null && this.CityTo == null)
                {
                    _availableAreaTo = new XPCollection<Area>(Session,
                                    new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("Country", this.CountryTo)));
                }
                else if (this.CountryTo == null && this.CityTo != null)
                {
                    _availableAreaTo = new XPCollection<Area>(Session,
                                    new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("City", this.CityTo)));
                }
                else
                {
                    _availableAreaTo = new XPCollection<Area>(Session);
                }

                return _availableAreaTo;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableAreaTo", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("PackageCollectionAreaToClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public Area AreaTo
        {
            get { return _areaTo; }
            set { SetPropertyValue("AreaTo", ref _areaTo, value); }
        }

        [Browsable(false)]
        public XPCollection<Location> AvailableLocationTo
        {
            get
            {
                if (this.AreaTo != null)
                {
                    _availableLocationTo = new XPCollection<Location>(Session,
                                          new GroupOperator(GroupOperatorType.And,
                                          new BinaryOperator("Area", this.AreaTo),
                                          new BinaryOperator("Active", true)));

                }
                else
                {
                    _availableLocationTo = new XPCollection<Location>(Session);
                }

                return _availableLocationTo;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableLocationTo", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("PackageCollectionLocationToClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public Location LocationTo
        {
            get { return _locationTo; }
            set { SetPropertyValue("LocationTo", ref _locationTo, value); }
        }

        [Browsable(false)]
        public XPCollection<TransportLocation> AvailableTransportLocationTo
        {
            get
            {
                if (this.CountryTo != null)
                {
                    _availableTransportLocationTo = new XPCollection<TransportLocation>(Session,
                                          new GroupOperator(GroupOperatorType.And,
                                          new BinaryOperator("Country", this.CountryTo),
                                          new BinaryOperator("Active", true)));

                }
                else
                {
                    _availableTransportLocationTo = new XPCollection<TransportLocation>(Session);
                }

                return _availableTransportLocationTo;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableTransportLocationTo", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("PackageCollectionTransportLocationToClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public TransportLocation TransportLocationTo
        {
            get { return _transportLocationTo; }
            set { SetPropertyValue("TransportLocationTo", ref _transportLocationTo, value); }
        }

        [Browsable(false)]
        public XPCollection<BusinessPartner> AvailableConsignee
        {
            get
            {
                if (this.CountryFrom != null)
                {
                    if (this.CityFrom != null)
                    {
                        _availableConsignee = new XPCollection<BusinessPartner>
                                            (Session, new GroupOperator(GroupOperatorType.And,
                                            new BinaryOperator("Country", this.CountryTo),
                                            new BinaryOperator("City", this.CityTo)));
                    }
                    else
                    {
                        _availableConsignee = new XPCollection<BusinessPartner>
                                            (Session, new GroupOperator(GroupOperatorType.And,
                                            new BinaryOperator("Country", this.CountryTo)));
                    }
                }
                else
                {
                    _availableConsignee = new XPCollection<BusinessPartner>(Session);
                }


                return _availableConsignee;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableConsignee", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("PackageCollectionConsigneeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BusinessPartner Consignee
        {
            get { return _consignee; }
            set { SetPropertyValue("Consignee", ref _consignee, value); }
        }

        #endregion To

        [Browsable(false)]
        public XPCollection<FreightForwardingOrigin> AvailableFreightForwardingOrigin
        {
            get
            {
                ShipmentType _locShipType = ShipmentType.None;
                string _beginString = null;
                string _endString = null;
                string _fullString = null;
                string _fullString1 = null;
                string _locShipmentType = null;
                string _locCountryFrom = null;
                string _locCityFrom = null;
                string _locAreaFrom = null;
                string _locLocationFrom = null;
                string _locTransportLocationFrom = null;
                string _locShipper = null;
                string _locCountryTo = null;
                string _locCityTo = null;
                string _locAreaTo = null;
                string _locLocationTo = null;
                string _locTransportLocationTo = null;
                string _locConsignee = null;

                if (!IsLoading)
                {
                    if (this.Routing != null)
                    {
                        if (this.ShipmentType == ShipmentType.Export || this.ShipmentType == ShipmentType.ExportAndImport
                            || this.ShipmentType == ShipmentType.DomesticDelivery)
                        {
                            if(this.ShipmentType == ShipmentType.Export || this.ShipmentType == ShipmentType.ExportAndImport)
                            {
                                _locShipType = ShipmentType.Export;
                            }
                            if(this.ShipmentType == ShipmentType.DomesticDelivery)
                            {
                                _locShipType = ShipmentType.DomesticDelivery;
                            }

                            #region If From
                            if(this.ShipmentType != ShipmentType.None) { _locShipmentType = " AND [ShipmentType]=='" + GetShipmentType(_locShipType).ToString() + "'"; }
                            if (this.CountryFrom != null) { if (this.CountryFrom.Code != null) { _locCountryFrom = " [CountryFrom.Code]=='" + this.CountryFrom.Code + "'"; } } else { _locCountryFrom = ""; }
                            if (this.CityFrom != null) { if (this.CityFrom.Code != null) { _locCityFrom = " AND [CityFrom.Code]=='" + this.CityFrom.Code + "'"; } } else { _locCityFrom = ""; }
                            if (this.AreaFrom != null) { if (this.AreaFrom.Code != null) { _locAreaFrom = " AND [AreaFrom.Code]=='" + this.AreaFrom.Code + "'"; } } else { _locAreaFrom = ""; }
                            if (this.LocationFrom != null) { if (this.LocationFrom.Code != null) { _locLocationFrom = " AND [LocationFrom.Code]=='" + this.LocationFrom.Code  + "'"; } } else { _locLocationFrom = ""; }
                            if (this.TransportLocationFrom != null) { if (this.TransportLocationFrom.Code != null) { _locTransportLocationFrom = " AND [TransportLocationFrom.Code]=='" + this.TransportLocationFrom.Code + "'"; } } else { _locTransportLocationFrom = ""; }
                            if (this.Shipper != null) { if (this.Shipper.Code != null) { _locShipper = " AND [Shipper.Code]=='" + this.Shipper.Code + "'"; } } else { _locShipper = ""; }
                            #endregion If From

                            #region If To
                            if (this.CountryTo != null) { if (this.CountryTo.Code != null) { _locCountryTo = " AND [CountryTo.Code]=='" + this.CountryTo.Code + "'"; } } else { _locCountryTo = ""; }
                            if (this.CityTo != null) { if (this.CityTo.Code != null) { _locCityTo = " AND [CityTo.Code]=='" + this.CityTo.Code + "'"; } } else { _locCityTo = ""; }
                            if (this.AreaTo != null) { if (this.AreaTo.Code != null) { _locAreaTo = " AND [AreaTo.Code]=='" + this.AreaTo.Code + "'"; } } else { _locAreaTo = ""; }
                            if (this.LocationTo != null) { if (this.LocationTo.Code != null) { _locLocationTo = " AND [LocationTo.Code]=='" + this.LocationTo.Code + "'"; } } else { _locLocationTo = ""; }
                            if (this.TransportLocationTo != null) { if (this.TransportLocationTo.Code != null) { _locTransportLocationTo = " AND [TransportLocationTo.Code]=='" + this.TransportLocationTo.Code + "'"; } } else { _locTransportLocationTo = ""; }
                            if (this.Consignee != null) { if (this.Consignee.Code != null) { _locConsignee = " AND [Consignee.Code]=='" + this.Consignee.Code + "'"; } } else { _locConsignee = ""; }
                            #endregion If To

                            _fullString1 = _locCountryFrom + _locShipmentType + _locCityFrom + _locAreaFrom + _locLocationFrom + _locTransportLocationFrom + _locShipper + _locCountryTo + _locCityTo + _locAreaTo + _locLocationTo + _locTransportLocationTo + _locConsignee;

                            XPCollection<OriginPackageMonitoring> _dataLines = new XPCollection<OriginPackageMonitoring>(Session, CriteriaOperator.Parse(_fullString1));

                            

                            if (_dataLines != null && _dataLines.Count() > 0)
                            {
                                List<string> _stringOPM = new List<string>();

                                foreach (OriginPackageMonitoring _dataLine in _dataLines)
                                {
                                    if (_dataLine.FreightForwardingOrigin != null)
                                    {
                                        if (_dataLine.FreightForwardingOrigin.Code != null)
                                        {
                                            _stringOPM.Add(_dataLine.FreightForwardingOrigin.Code);
                                        }
                                    }
                                }

                                IEnumerable<string> _stringArrayOPMDistinct = _stringOPM.Distinct();
                                string[] _stringArrayOPMList = _stringArrayOPMDistinct.ToArray();
                                if (_stringArrayOPMList.Length == 1)
                                {
                                    for (int i = 0; i < _stringArrayOPMList.Length; i++)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _stringArrayOPMList[i] + "'";
                                        }
                                    }
                                }
                                else if (_stringArrayOPMList.Length > 1)
                                {
                                    for (int i = 0; i < _stringArrayOPMList.Length; i++)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _stringArrayOPMList[i] + "'";
                                        }
                                        else
                                        {
                                            _endString = _endString + " OR [Code]=='" + _stringArrayOPMList[i] + "'";
                                        }
                                    }
                                }
                                _fullString = _beginString + _endString;

                                if (_fullString != null)
                                {
                                    _availableFreightForwardingOrigin = new XPCollection<FreightForwardingOrigin>(Session, CriteriaOperator.Parse(_fullString));
                                }
                            }
                        }
                        
                    }
                }

                return _availableFreightForwardingOrigin;
            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableFreightForwardingOrigin", DataSourcePropertyIsNullMode.SelectNothing)]
        public FreightForwardingOrigin FreightForwardingOrigin
        {
            get { return _freightForwardingOrigin; }
            set { SetPropertyValue("FreightForwardingOrigin", ref _freightForwardingOrigin, value); }
        }

        public XPCollection<FreightForwardingDestination> AvailableFreightForwardingDestination
        {
            get
            {
                ShipmentType _locShipType = ShipmentType.None;
                string _beginString = null;
                string _endString = null;
                string _fullString = null;
                string _fullString1 = null;
                string _locShipmentType = null;
                string _locCountryFrom = null;
                string _locCityFrom = null;
                string _locAreaFrom = null;
                string _locLocationFrom = null;
                string _locTransportLocationFrom = null;
                string _locShipper = null;
                string _locCountryTo = null;
                string _locCityTo = null;
                string _locAreaTo = null;
                string _locLocationTo = null;
                string _locTransportLocationTo = null;
                string _locConsignee = null;

                if (!IsLoading)
                {
                    if (this.Routing != null)
                    {
                        if (this.ShipmentType == ShipmentType.Import || this.ShipmentType == ShipmentType.ExportAndImport)
                        {
                            _locShipType = ShipmentType.Import;
                            #region If From
                            if (this.ShipmentType != ShipmentType.None) { _locShipmentType = " AND [ShipmentType]=='" + GetShipmentType(_locShipType).ToString() + "'"; }
                            if (this.CountryFrom != null) { if (this.CountryFrom.Code != null) { _locCountryFrom = " [CountryFrom.Code]=='" + this.CountryFrom.Code + "'"; } } else { _locCountryFrom = ""; }
                            if (this.CityFrom != null) { if (this.CityFrom.Code != null) { _locCityFrom = " AND [CityFrom.Code]=='" + this.CityFrom.Code + "'"; } } else { _locCityFrom = ""; }
                            if (this.AreaFrom != null) { if (this.AreaFrom.Code != null) { _locAreaFrom = " AND [AreaFrom.Code]=='" + this.AreaFrom.Code + "'"; } } else { _locAreaFrom = ""; }
                            if (this.LocationFrom != null) { if (this.LocationFrom.Code != null) { _locLocationFrom = " AND [LocationFrom.Code]=='" + this.LocationFrom.Code + "'"; } } else { _locLocationFrom = ""; }
                            if (this.TransportLocationFrom != null) { if (this.TransportLocationFrom.Code != null) { _locTransportLocationFrom = " AND [TransportLocationFrom.Code]=='" + this.TransportLocationFrom.Code + "'"; } } else { _locTransportLocationFrom = ""; }
                            if (this.Shipper != null) { if (this.Shipper.Code != null) { _locShipper = " AND [Shipper.Code]=='" + this.Shipper.Code + "'"; } } else { _locShipper = ""; }
                            #endregion If From

                            #region If To
                            if (this.CountryTo != null) { if (this.CountryTo.Code != null) { _locCountryTo = " AND [CountryTo.Code]=='" + this.CountryTo.Code + "'"; } } else { _locCountryTo = ""; }
                            if (this.CityTo != null) { if (this.CityTo.Code != null) { _locCityTo = " AND [CityTo.Code]=='" + this.CityTo.Code + "'"; } } else { _locCityTo = ""; }
                            if (this.AreaTo != null) { if (this.AreaTo.Code != null) { _locAreaTo = " AND [AreaTo.Code]=='" + this.AreaTo.Code + "'"; } } else { _locAreaTo = ""; }
                            if (this.LocationTo != null) { if (this.LocationTo.Code != null) { _locLocationTo = " AND [LocationTo.Code]=='" + this.LocationTo.Code + "'"; } } else { _locLocationTo = ""; }
                            if (this.TransportLocationTo != null) { if (this.TransportLocationTo.Code != null) { _locTransportLocationTo = " AND [TransportLocationTo.Code]=='" + this.TransportLocationTo.Code + "'"; } } else { _locTransportLocationTo = ""; }
                            if (this.Consignee != null) { if (this.Consignee.Code != null) { _locConsignee = " AND [Consignee.Code]=='" + this.Consignee.Code + "'"; } } else { _locConsignee = ""; }
                            #endregion If To

                            _fullString1 = _locCountryFrom + _locShipmentType + _locCityFrom + _locAreaFrom + _locLocationFrom + _locTransportLocationFrom + _locShipper + _locCountryTo + _locCityTo + _locAreaTo + _locLocationTo + _locTransportLocationTo + _locConsignee;

                            XPCollection<DestinationPackageMonitoring> _dataLines = new XPCollection<DestinationPackageMonitoring>(Session, CriteriaOperator.Parse(_fullString1));



                            if (_dataLines != null && _dataLines.Count() > 0)
                            {
                                List<string> _stringDPM = new List<string>();

                                foreach (DestinationPackageMonitoring _dataLine in _dataLines)
                                {
                                    if (_dataLine.FreightForwardingDestination != null)
                                    {
                                        if (_dataLine.FreightForwardingDestination.Code != null)
                                        {
                                            _stringDPM.Add(_dataLine.FreightForwardingDestination.Code);
                                        }
                                    }
                                }

                                IEnumerable<string> _stringArrayDPMDistinct = _stringDPM.Distinct();
                                string[] _stringArrayDPMList = _stringArrayDPMDistinct.ToArray();
                                if (_stringArrayDPMList.Length == 1)
                                {
                                    for (int i = 0; i < _stringArrayDPMList.Length; i++)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _stringArrayDPMList[i] + "'";
                                        }
                                    }
                                }
                                else if (_stringArrayDPMList.Length > 1)
                                {
                                    for (int i = 0; i < _stringArrayDPMList.Length; i++)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _stringArrayDPMList[i] + "'";
                                        }
                                        else
                                        {
                                            _endString = _endString + " OR [Code]=='" + _stringArrayDPMList[i] + "'";
                                        }
                                    }
                                }
                                _fullString = _beginString + _endString;

                                if (_fullString != null)
                                {
                                    _availableFreightForwardingDestination = new XPCollection<FreightForwardingDestination>(Session, CriteriaOperator.Parse(_fullString));
                                }
                            }
                        }

                    }
                }

                return _availableFreightForwardingDestination;
            }
        }

        public FreightForwardingDestination FreightForwardingDestination
        {
            get { return _freightForwardingDestination; }
            set { SetPropertyValue("FreightForwardingDestination", ref _freightForwardingDestination, value); }
        }

        [ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("PackageCollectionRoutingClose", Enabled = false)]
        [Association("Routing-PackageCollections")]
        public Routing Routing
        {
            get { return _routing; }
            set {
                SetPropertyValue("Routing", ref _routing, value);
                if(this._routing != null)
                {
                    this.ShipmentType = this._routing.ShipmentType;
                    if(this._routing.CountryFrom != null)
                    {
                        this.CountryFrom = this._routing.CountryFrom;
                    }
                    if(this._routing.CountryTo != null)
                    {
                        this.CountryTo = this._routing.CountryTo;
                    }
                    if (this._routing.CityFrom != null)
                    {
                        this.CityFrom = this._routing.CityFrom;
                    }
                    if (this._routing.CityTo != null)
                    {
                        this.CityTo = this._routing.CityTo;
                    }
                    
                }
            }
        }

        [Appearance("PackageCollectionStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("PackageCollectionStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("PackageCollectionPostedCountEnabled", Enabled = false)]
        public int PostedCount
        {
            get { return _postedCount; }
            set { SetPropertyValue("PostedCount", ref _postedCount, value); }
        }
        #endregion Field

        //====================================================== Code Only ==================================================================

        public int GetShipmentType(ShipmentType objectName)
        {
            int _result = 0;
            try
            {
                if (objectName == ShipmentType.Import)
                {
                    _result = 1;
                }
                else if (objectName == ShipmentType.Export)
                {
                    _result = 2;
                }
                else if (objectName == ShipmentType.ExportAndImport)
                {
                    _result = 3;
                }
                else if (objectName == ShipmentType.DomesticDelivery)
                {
                    _result = 4;
                }
                else
                {
                    _result = 0;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = Routing " + ex.ToString());
            }
            return _result;
        }

    }
}