﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Cost Of Goods Sold")]
    [RuleCombinationOfPropertiesIsUnique("OverheadCostElectricRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class OverheadCostElectric : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private int _no;
        private string _code;
        private double _kwh;        
        private double _electricCost;
        private double _mpm;
        private double _totalElectricCost;
        private OverheadCost _overheadCost;
        private GlobalFunction _globFunc;

        public OverheadCostElectric(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if(!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(Session.DataLayer, ObjectList.OverheadCostElectric);
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        protected override void OnSaving()
        {
            base.OnSaving();
            if (!IsLoading && IsSaving)
            {
                UpdateNo();
            }
        }

        protected override void OnDeleting()
        {
            base.OnDeleting();
            RecoveryDeleteNo();
        }

        #region Field

        [Appearance("OverheadCostElectricNoEnabled", Enabled = false)]
        public int No
        {
            get { return _no; }
            set { SetPropertyValue("No", ref _no, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }       

        [ImmediatePostData()]
        public double KWH
        {
            get { return _kwh; }
            set
            {
                SetPropertyValue("KWH", ref _kwh, value);
                if (!IsLoading)
                {
                    SetTotalElectricCost();
                }
            }
        }

        [ImmediatePostData()]
        public double ElectricCost
        {
            get { return _electricCost; }
            set
            {
                SetPropertyValue("ElectricCost", ref _electricCost, value);
                if (!IsLoading)
                {
                    SetTotalElectricCost();
                }
            }
        }

        [ImmediatePostData()]
        public double MPM
        {
            get { return _mpm; }
            set
            {
                SetPropertyValue("MPM", ref _mpm, value);
                if (!IsLoading)
                {
                    if(this._mpm > 0)
                    {
                        SetTotalElectricCost();
                    }
                    
                }
            }
        }

        [Appearance("OverheadCostElectricTotalElectricCostEnabled", Enabled = false)]
        public double TotalElectricCost
        {
            get { return _totalElectricCost; }
            set { SetPropertyValue("TotalElectricCost", ref _totalElectricCost, value); }
        }

        [Appearance("OverheadCostElectricSumCostElectricEnabled", Enabled = false)]
        public Double SumCostElectric
        {
            get
            {
                if (this.TotalElectricCost > 0)
                {
                    return GetTotalElectricCost();
                }
                else
                {
                    return 0;
                }
            }
        }

        [Association("OverheadCost-OverheadCostElectrics")]
        [Appearance("OverheadCostElectricOverheadCostEnabled", Enabled = false)]
        public OverheadCost OverheadCost
        {
            get { return _overheadCost; }
            set
            {
                SetPropertyValue("OverheadCost", ref _overheadCost, value);
                if(!IsLoading)
                {
                    this.KWH = _overheadCost.KWH;
                    this.MPM = _overheadCost.MPM;
                }
            }
        }

        #endregion Field

        //private string _PersistentProperty;
        //[XafDisplayName("My display name"), ToolTip("My hint message")]
        //[ModelDefault("EditMask", "(000)-00"), Index(0), VisibleInListView(false)]
        //[Persistent("DatabaseColumnName"), RuleRequiredField(DefaultContexts.Save)]
        //public string PersistentProperty {
        //    get { return _PersistentProperty; }
        //    set { SetPropertyValue("PersistentProperty", ref _PersistentProperty, value); }
        //}

        //[Action(Caption = "My UI Action", ConfirmationMessage = "Are you sure?", ImageName = "Attention", AutoCommit = true)]
        //public void ActionMethod() {
        //    // Trigger a custom business logic for the current record in the UI (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112619.aspx).
        //    this.PersistentProperty = "Paid";
        //}

        //================================================== Code Only ==================================================

        #region Numbering

        public void UpdateNo()
        {
            try
            {
                if (!IsLoading)
                {
                    if (!(Session is NestedUnitOfWork) && Session.IsNewObject(this))
                    {
                        if (this.OverheadCost != null)
                        {
                            object _makRecord = Session.Evaluate<OverheadCostElectric>(CriteriaOperator.Parse("Max(No)"), CriteriaOperator.Parse("OverheadCost=?", this.OverheadCost));
                            this.No = Convert.ToInt32(_makRecord) + 1;
                            this.Save();
                            RecoveryUpdateNo();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = OverheadCostElectric " + ex.ToString());
            }
        }

        public void RecoveryUpdateNo()
        {
            try
            {
                if (this.OverheadCost != null)
                {
                    OverheadCost _numHeader = Session.FindObject<OverheadCost>
                                                (new BinaryOperator("Code", this.OverheadCost.Code));

                    XPCollection<OverheadCostElectric> _numLines = new XPCollection<OverheadCostElectric>
                                                (Session, new BinaryOperator("OverheadCost", _numHeader),
                                                new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i1 = 0;
                        foreach (OverheadCostElectric _numLine in _numLines)
                        {
                            i1 += 1;
                            _numLine.No = i1;
                            _numLine.Save();
                        }
                        i1 = 1;
                        //Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = OverheadCostElectric " + ex.ToString());
            }
        }

        public void RecoveryDeleteNo()
        {
            try
            {
                if (this.OverheadCost != null)
                {
                    OverheadCost _numHeader = Session.FindObject<OverheadCost>
                                                (new BinaryOperator("Code", this.OverheadCost.Code));

                    XPCollection<OverheadCostElectric> _numLines = new XPCollection<OverheadCostElectric>
                                                (Session, new GroupOperator(GroupOperatorType.And,
                                                 new BinaryOperator("This", this, BinaryOperatorType.NotEqual),
                                                 new BinaryOperator("OverheadCost", _numHeader)),
                                                 new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i = 0;
                        foreach (OverheadCostElectric _numLine in _numLines)
                        {
                            i += 1;
                            _numLine.No = i;
                            _numLine.Save();
                        }
                        //Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = OverheadCostElectric " + ex.ToString());
            }
        }

        #endregion Numbering

        private void SetTotalElectricCost()
        {
            try
            {
                this._totalElectricCost = (this._kwh * this._electricCost) / this._mpm;
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = OverheadCostElectric " + ex.ToString());
            }
        }

        private double GetTotalElectricCost()
        {
            double _result = 0;
            try
            {
                double _locTotalCostElectric = 0;
                if (!IsLoading)
                {
                    if (this.OverheadCost != null)
                    {
                        XPCollection<OverheadCostElectric> _locOverheadCostElectrics = new XPCollection<OverheadCostElectric>
                                                    (Session, new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("OverheadCost", this.OverheadCost)));
                        if (_locOverheadCostElectrics.Count() > 0)
                        {
                            foreach (OverheadCostElectric _locOverheadCostElectric in _locOverheadCostElectrics)
                            {
                                _locTotalCostElectric = _locTotalCostElectric + _locOverheadCostElectric.TotalElectricCost;
                            }
                        }
                        _result = _locTotalCostElectric;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = OverheadCostElectric " + ex.ToString());
            }
            return _result;
        }

    }
}