﻿#region Default

using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

#endregion Default

using DevExpress.ExpressApp.Editors;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    #region Default

    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Sales")]
    [RuleCombinationOfPropertiesIsUnique("SalesReturnRuleUnique", DefaultContexts.Save, "Code")]

    #endregion Default

    // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
    public class SalesReturn : FullDriveSysBaseObject
    {
        #region Default

        private bool _activationPosting;
        private string _code;
        private string _name;
        private BusinessPartner _salesToCustomer;
        private string _salesToContact;
        private Country _salesToCountry;
        private City _salesToCity;
        private string _salesToAddress;
        private string _customerDebitMemo;
        private string _description;
        private string _signCode;
        private InventoryTransferIn _inventoryTransferIn;
        private SalesOrder _salesOrder;
        private ProjectHeader _projectHeader;
        private Status _status;
        private DateTime _statusDate;
        private DateTime _estimatedDate;
        private Company _company;
        private string _localUserAccess;
        private GlobalFunction _globFunc;

        private bool _activeApproved1;
        private bool _activeApproved2;
        private bool _activeApproved3;

        public SalesReturn(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.SalesReturn);
                this.Status = Status.Open;
                this.StatusDate = now;

                #region UserAccess
                _localUserAccess = SecuritySystem.CurrentUserName;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _localUserAccess));
                if (Session.IsNewObject(this))
                {
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null)
                            {
                                Company = _locUserAccess.Employee.Company;
                            }
                            
                        }
                    }
                }
                #endregion UserAccess
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #endregion Default

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [Appearance("SalesOrderRedColor", Criteria = "ActiveApproved1 = true", BackColor = "#fd79a8")]
        [Appearance("SalesOrderYellowColor", Criteria = "ActiveApproved2 = true", BackColor = "#ffeaa7")]
        [Appearance("SalesOrderGreenOColor", Criteria = "ActiveApproved3 = true", BackColor = "#81ecec")]
        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("SalesReturnCodeEnabled", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("SalesReturnNameClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        [ImmediatePostData()]
        [Appearance("SalesReturnSalesToCustomerClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BusinessPartner SalesToCustomer
        {
            get { return _salesToCustomer; }
            set {
                SetPropertyValue("SalesToCustomer", ref _salesToCustomer, value);
                if (!IsLoading)
                {
                    if (this._salesToCustomer != null)
                    {
                        this.SalesToContact = this._salesToCustomer.Contact;
                        this.SalesToCountry = this._salesToCustomer.Country;
                        this.SalesToCity = this._salesToCustomer.City;
                        this.SalesToAddress = this._salesToCustomer.Address;
                        
                    }
                }
            }
        }

        [Appearance("SalesReturnSalesToContactClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string SalesToContact
        {
            get { return _salesToContact; }
            set { SetPropertyValue("SalesToContact", ref _salesToContact, value); }
        }

        [Appearance("SalesReturnSalesToCountryClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Country SalesToCountry
        {
            get { return _salesToCountry; }
            set { SetPropertyValue("SalesToCountry", ref _salesToCountry, value); }
        }

        [Appearance("SalesReturnSalesToCityClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public City SalesToCity
        {
            get { return _salesToCity; }
            set { SetPropertyValue("SalesToCity", ref _salesToCity, value); }
        }

        [Appearance("SalesReturnSalesToAddressClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Size(512)]
        public string SalesToAddress
        {
            get { return _salesToAddress; }
            set { SetPropertyValue("SalesToAddress", ref _salesToAddress, value); }
        }

        [Appearance("SalesReturnCustomerDebitMemoClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string CustomerDebitMemo
        {
            get { return _customerDebitMemo; }
            set { SetPropertyValue("CustomerDebitMemo", ref _customerDebitMemo, value); }
        }

        [Appearance("SalesReturnDescriptionClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Size(512)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        [Appearance("SalesReturnSignCodeEnabled", Enabled = false)]
        public string SignCode
        {
            get { return _signCode; }
            set { SetPropertyValue("SignCode", ref _signCode, value); }
        }

        [Appearance("SalesReturnInventoryTransferInClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public InventoryTransferIn InventoryTransferIn
        {
            get { return _inventoryTransferIn; }
            set { SetPropertyValue("InventoryTransferIn", ref _inventoryTransferIn, value); }
        }

        [Appearance("SalesReturnSalesOrderClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public SalesOrder SalesOrder
        {
            get { return _salesOrder; }
            set { SetPropertyValue("SalesOrder", ref _salesOrder, value); }
        }

        [Association("ProjectHeader-SalesReturns")]
        public ProjectHeader ProjectHeader
        {
            get { return _projectHeader; }
            set { SetPropertyValue("ProjectHeader", ref _projectHeader, value); }
        }

        //[Appearance("SalesReturnStatusEnabled", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("SalesReturnStatusDateEnabled", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("SalesReturnEstimatedDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime EstimatedDate
        {
            get { return _estimatedDate; }
            set { SetPropertyValue("EstimatedDate", ref _estimatedDate, value); }
        }

        [Browsable(false)]
        [DataSourceCriteria("Active = true")]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Association("SalesReturn-SalesReturnLines")]
        public XPCollection<SalesReturnLine> SalesReturnLines
        {
            get { return GetCollection<SalesReturnLine>("SalesReturnLines"); }
        }

        [Association("SalesReturn-ApprovalLines")]
        public XPCollection<ApprovalLine> ApprovalLines
        {
            get { return GetCollection<ApprovalLine>("ApprovalLines"); }
        }

        [Association("SalesReturn-ReturnSalesCollections")]
        public XPCollection<ReturnSalesCollection> ReturnSalesCollections
        {
            get { return GetCollection<ReturnSalesCollection>("ReturnSalesCollections"); }
        }

        //CLM
        [Browsable(false)]
        [Appearance("SalesReturnActiveApproved1Close", Enabled = false)]
        public bool ActiveApproved1
        {
            get { return _activeApproved1; }
            set { SetPropertyValue("ActiveApproved1", ref _activeApproved1, value); }
        }

        [Browsable(false)]
        [Appearance("SalesReturnActiveApproved2Close", Enabled = false)]
        public bool ActiveApproved2
        {
            get { return _activeApproved2; }
            set { SetPropertyValue("ActiveApproved2", ref _activeApproved2, value); }
        }

        [Browsable(false)]
        [Appearance("SalesReturnActiveApproved3Close", Enabled = false)]
        public bool ActiveApproved3
        {
            get { return _activeApproved3; }
            set { SetPropertyValue("ActiveApproved3", ref _activeApproved3, value); }
        }


        #endregion Field

    }
}