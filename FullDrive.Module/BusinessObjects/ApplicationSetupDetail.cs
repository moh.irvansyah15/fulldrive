﻿#region Default

using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

#endregion Default

using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    #region Default

    [DefaultClassOptions]
    [DefaultProperty("Name")]
    [NavigationItem("Setup")]

    #endregion Default

    // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
    public class ApplicationSetupDetail : FullDriveSysBaseObject
    {
        #region Default

        private string _code;
        private string _name;
        private UserAccess _userAccess;
        private bool _businessPartnerActive;
        private BusinessPartner _businessPartner;
        private ObjectList _objectList;
        private FunctionList _functionList;
        private ApprovalLevel _approvalLevel;
        private bool _endApproval;
        private bool _qcPassed;
        private bool _splitGoodsReceive;
        private bool _allSplitGoodsReceive;
        private bool _active;
        private ApplicationSetup _applicationSetup;       
        private GlobalFunction _globFunc;

        public ApplicationSetupDetail(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if(!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.ApplicationSetupDetail);
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #endregion Default

        #region Field

        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        [Association("UserAccess-ApplicationSetupDetails")]
        public UserAccess UserAccess
        {
            get { return _userAccess; }
            set { SetPropertyValue("UserAccess", ref _userAccess, value); }
        }

        public ObjectList ObjectList
        {
            get { return _objectList; }
            set { SetPropertyValue("ObjectList", ref _objectList, value); }
        }

        public FunctionList FunctionList
        {
            get { return _functionList; }
            set { SetPropertyValue("FunctionList", ref _functionList, value); }
        }

        public ApprovalLevel ApprovalLevel
        {
            get { return _approvalLevel; }
            set { SetPropertyValue("ApprovalLevel", ref _approvalLevel, value); }
        }

        public bool EndApproval
        {
            get { return _endApproval; }
            set { SetPropertyValue("EndApproval", ref _endApproval, value); }
        }

        public bool QcPassed
        {
            get { return _qcPassed; }
            set { SetPropertyValue("QcPassed", ref _qcPassed, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue("Active", ref _active, value); }
        }

        [Appearance("ApplicationSetupDetailApplicationSetupEnabled", Enabled = false)]
        [RuleRequiredField(DefaultContexts.Save)]
        [Association("ApplicationSetup-ApplicationSetupDetails")]
        public ApplicationSetup ApplicationSetup
        {
            get { return _applicationSetup; }
            set { SetPropertyValue("ApplicationSetup", ref _applicationSetup, value); }
        }

        #endregion Field

    }
}