﻿#region Default

using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

#endregion Default

using DevExpress.ExpressApp.Editors;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    #region Default

    [DefaultClassOptions]
    [NavigationItem("Production")]
    [DefaultProperty("Code")]
    [RuleCombinationOfPropertiesIsUnique("OutputProductionRuleUnique", DefaultContexts.Save, "Code")]

    #endregion Default

    // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
    public class OutputProduction : FullDriveSysBaseObject
    {
        #region Default

        private bool _activationPosting;
        private string _code;
        private string _name;
        private string _description;
        private DateTime _startDate;
        private DateTime _endDate;
        private DateTime _estimatedDate;
        private Status _status;
        private DateTime _statusDate;
        private Consumption _consumption;
        private MachineMapVersion _machineMapVersion;
        private MachineCost _machineCost;
        private Production _production;
        private WorkOrder _workOrder;
        private string _signCode;
        private Company _company;
        private string _userAccess;
        private GlobalFunction _globalFunc;

        public OutputProduction(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).

            if (!IsLoading)
            {
                _globalFunc = new GlobalFunction();
                this.Code = _globalFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.OutputProduction);
                DateTime Now = DateTime.Now;
                this.Status = CustomProcess.Status.Open;
                this.StatusDate = Now;
            }

            _userAccess = SecuritySystem.CurrentUserName;
            UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
            if (Session.IsNewObject(this))
            {
                if (_locUserAccess != null)
                {
                    if (_locUserAccess.Employee != null)
                    {
                        if (_locUserAccess.Employee.Company != null)
                        {
                            Company = _locUserAccess.Employee.Company;
                        }
                    }
                }
            }
        }

        #endregion Default

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("OutputProductionCodeEnabled", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("OutputProductionNameClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        [Size(512)]
        [Appearance("OutputProductionDescriptionClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        [Appearance("OutputProductionStartDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime StartDate
        {
            get { return _startDate; }
            set { SetPropertyValue("StartDate", ref _startDate, value); }
        }

        [Appearance("OutputProductionEndDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime EndDate
        {
            get { return _endDate; }
            set { SetPropertyValue("EndDate", ref _endDate, value); }
        }

        [Appearance("OutputProductionEstimatedDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime EstimatedDate
        {
            get { return _estimatedDate; }
            set { SetPropertyValue("EstimatedDate", ref _estimatedDate, value); }
        }

        [Appearance("OutputProductionStatusEnabled", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("OutputProductionStatusDateEnabled", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("OutputProductionConsumptionEnabled", Enabled = false)]
        public Consumption Consumption
        {
            get { return _consumption; }
            set { SetPropertyValue("Consumption", ref _consumption, value); }
        }

        [Appearance("OutputProductionMachineMapVersionEnabled", Enabled = false)]
        public MachineMapVersion MachineMapVersion
        {
            get { return _machineMapVersion; }
            set { SetPropertyValue("MachineMapVersion", ref _machineMapVersion, value); }
        }

        [Appearance("OutputProductionMachineCostEnabled", Enabled = false)]
        public MachineCost MachineCost
        {
            get { return _machineCost; }
            set { SetPropertyValue("MachineCost", ref _machineCost, value); }
        }

        [Appearance("OutputProductionProductionEnabled", Enabled = false)]
        [Association("Production-OutputProductions")]
        public Production Production
        {
            get { return _production; }
            set { SetPropertyValue("Production", ref _production, value); }
        }

        //Wahab 01-03-2020
        //[Association("WorkOrder-OutputProductions")]
        [Appearance("OutputProductionWorkOrderEnabled", Enabled = false)]       
        public WorkOrder WorkOrder
        {
            get { return _workOrder; }
            set { SetPropertyValue("WorkOrder", ref _workOrder, value); }
        }

        //[Browsable(false)]
        [Appearance("OutputProductionCompanyEnabled", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Appearance("OutputProductionSignCodeClose", Enabled = false)]
        public string SignCode
        {
            get { return _signCode; }
            set { SetPropertyValue("SignCode", ref _signCode, value); }
        }

        [Association("OutputProduction-OutputProductionLines")]
        public XPCollection<OutputProductionLine> OutputProductionLines
        {
            get { return GetCollection<OutputProductionLine>("OutputProductionLines"); }
        }

        #endregion Field

    }
}