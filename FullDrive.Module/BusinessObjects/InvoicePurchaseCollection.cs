﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("InvoicePurchaseCollectionRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class InvoicePurchaseCollection : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private bool _activationPosting;
        private string _code;
        private XPCollection<PurchaseOrder> _availablePurchaseOrder;
        private PurchaseOrder _purchaseOrder;
        private XPCollection<InventoryTransferIn> _availableInventoryTransferIn;
        private InventoryTransferIn _inventoryTransferIn;
        private PurchaseInvoice _purchaseInvoice;
        private Status _status;
        private DateTime _statusDate;
        private int _postedCount;
        private GlobalFunction _globFunc;

        public InvoicePurchaseCollection(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.InvoicePurchaseCollection);
                DateTime now = DateTime.Now;
                this.Status = Status.Open;
                this.StatusDate = now;
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("InvoicePurchaseCollectionCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Browsable(false)]
        public XPCollection<PurchaseOrder> AvailablePurchaseOrder
        {
            get
            {
                if (!IsLoading)
                {
                    if (this.PurchaseInvoice != null)
                    {
                        if (this.PurchaseInvoice.BuyFromVendor != null)
                        {
                            XPCollection<PurchaseOrder> _locCollectionPOs = new XPCollection<PurchaseOrder>
                                                                            (Session, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("ActiveApproved3", true),
                                                                            new BinaryOperator("BuyFromVendor", PurchaseInvoice.BuyFromVendor),
                                                                            new GroupOperator(GroupOperatorType.Or,
                                                                            new BinaryOperator("Status", Status.Progress),
                                                                            new BinaryOperator("Status", Status.Posted)))
                                                                            );

                            if (_locCollectionPOs != null && _locCollectionPOs.Count() > 0)
                            {
                                _availablePurchaseOrder = _locCollectionPOs;
                            }
                        }
                    }
                }
                return _availablePurchaseOrder;
            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailablePurchaseOrder", DataSourcePropertyIsNullMode.SelectNothing)]
        public PurchaseOrder PurchaseOrder
        {
            get { return _purchaseOrder; }
            set { SetPropertyValue("PurchaseOrder", ref _purchaseOrder, value); }
        }

        public XPCollection<InventoryTransferIn> AvailableInventoryTransferIn
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;

                if (!IsLoading)
                {
                    if (this.PurchaseInvoice != null)
                    {
                        if (this.PurchaseInvoice.BuyFromVendor != null)
                        {
                            XPQuery<InventoryTransferInMonitoring> _inventoryTransferInMonitoringsQuery = new XPQuery<InventoryTransferInMonitoring>(Session);

                            var _inventoryTransferInMonitorings = from itim in _inventoryTransferInMonitoringsQuery
                                                                   where (itim.Status == Status.Open && itim.PostedCount == 0
                                                                   && itim.InventoryTransferIn.BusinessPartner == this.PurchaseInvoice.BuyFromVendor
                                                                   && itim.PurchaseInvoice == null)
                                                                   group itim by itim.InventoryTransferIn into g
                                                                   select new { InventoryTransferIn = g.Key };

                            if (_inventoryTransferInMonitorings != null && _inventoryTransferInMonitorings.Count() > 0)
                            {
                                List<string> _stringITIM = new List<string>();

                                foreach (var _inventoryTransferInMonitoring in _inventoryTransferInMonitorings)
                                {
                                    if (_inventoryTransferInMonitoring.InventoryTransferIn != null)
                                    {
                                        if (_inventoryTransferInMonitoring.InventoryTransferIn.Code != null)
                                        {
                                            _stringITIM.Add(_inventoryTransferInMonitoring.InventoryTransferIn.Code);
                                        }
                                    }
                                }

                                IEnumerable<string> _stringArrayITIDistinct = _stringITIM.Distinct();
                                string[] _stringArrayITIMList = _stringArrayITIDistinct.ToArray();
                                if (_stringArrayITIMList.Length == 1)
                                {
                                    for (int i = 0; i < _stringArrayITIMList.Length; i++)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _stringArrayITIMList[i] + "'";
                                        }
                                    }
                                }
                                else if (_stringArrayITIMList.Length > 1)
                                {
                                    for (int i = 0; i < _stringArrayITIMList.Length; i++)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _stringArrayITIMList[i] + "'";
                                        }
                                        else
                                        {
                                            _endString = _endString + " OR [Code]=='" + _stringArrayITIMList[i] + "'";
                                        }
                                    }
                                }
                                _fullString = _beginString + _endString;

                                if (_fullString != null)
                                {
                                    _availableInventoryTransferIn = new XPCollection<InventoryTransferIn>(Session, CriteriaOperator.Parse(_fullString));
                                }
                            }
                        }
                    }
                }
                return _availableInventoryTransferIn;
            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableInventoryTransferIn", DataSourcePropertyIsNullMode.SelectNothing)]
        public InventoryTransferIn InventoryTransferIn
        {
            get { return _inventoryTransferIn; }
            set { SetPropertyValue("InventoryTransferIn", ref _inventoryTransferIn, value); }
        }

        [ImmediatePostData()]
        [Appearance("InvoicePurchaseCollectionPurchaseInvoiceClose", Enabled = false)]
        [Association("PurchaseInvoice-InvoicePurchaseCollections")]
        public PurchaseInvoice PurchaseInvoice
        {
            get { return _purchaseInvoice; }
            set { SetPropertyValue("PurchaseInvoice", ref _purchaseInvoice, value); }
        }

        [Appearance("InvoicePurchaseCollectionStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("InvoicePurchaseCollectionStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("InvoicePurchaseCollectionPostedCountEnabled", Enabled = false)]
        public int PostedCount
        {
            get { return _postedCount; }
            set { SetPropertyValue("PostedCount", ref _postedCount, value); }
        }

        //private string _PersistentProperty;
        //[XafDisplayName("My display name"), ToolTip("My hint message")]
        //[ModelDefault("EditMask", "(000)-00"), Index(0), VisibleInListView(false)]
        //[Persistent("DatabaseColumnName"), RuleRequiredField(DefaultContexts.Save)]
        //public string PersistentProperty {
        //    get { return _PersistentProperty; }
        //    set { SetPropertyValue("PersistentProperty", ref _PersistentProperty, value); }
        //}

        //[Action(Caption = "My UI Action", ConfirmationMessage = "Are you sure?", ImageName = "Attention", AutoCommit = true)]
        //public void ActionMethod() {
        //    // Trigger a custom business logic for the current record in the UI (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112619.aspx).
        //    this.PersistentProperty = "Paid";
        //}
    }
}