﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.SystemModule;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Editors;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Sales")]
    [RuleCombinationOfPropertiesIsUnique("SalesReturnMonitoringRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class SalesReturnMonitoring : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private string _code;
        private bool _select;
        private SalesReturn _salesReturn;
        private SalesReturnLine _salesReturnLine;
        private InventoryTransferIn _inventoryTransferIn;
        private InventoryTransferInLine _inventoryTransferInLine;
        private DebitMemo _debitMemo;
        private DebitMemoLine _debitMemoLine;
        private Item _item;
        private OrderType _orderType;
        private XPCollection<Item> _availableItem;
        #region InisialisasiDefaultQty
        private double _dQty;
        private UnitOfMeasure _dUom;
        private double _qty;
        private UnitOfMeasure _uom;
        private double _tQty;
        #endregion InisialisasiDefaultQty
        #region InisialisasiAmount
        private Currency _currency;
        private PriceGroup _priceGroup;
        private XPCollection<Price> _availablePrice;
        private Price _price;
        private XPCollection<PriceLine> _availablePriceLine;
        private PriceLine _priceLine;
        private double _uAmount;
        private double _tUAmount;
        private bool _multiTax;
        private Tax _tax;
        private double _txValue;
        private double _txAmount;
        private bool _multiDiscount;
        private Discount _discount;
        private double _disc;
        private double _discAmount;
        private double _tAmount;
        private double _bill;
        #endregion InisialisasiAmount
        private Status _inventoryStatus;
        private DateTime _inventoryStatusDate;
        private Status _memoStatus;
        private DateTime _memoStatusDate;
        private int _postedCount;
        private string _signCode;
        private ReturnType _returnType;
        private InventoryTransferOutMonitoring _inventoryTransferOutMonitoring;
        private SalesOrderMonitoring _salesOrderMonitoring;
        private GlobalFunction _globFunc;

        public SalesReturnMonitoring(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            _globFunc = new GlobalFunction();
            DateTime now = DateTime.Now;
            this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.SalesReturnMonitoring);
            this.Select = true;
            this.InventoryStatus = Status.Open;
            this.InventoryStatusDate = now;
            this.MemoStatus = Status.Open;
            this.MemoStatusDate = now;
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("SalesReturnMonitoringCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        public bool Select
        {
            get { return _select; }
            set { SetPropertyValue("Select", ref _select, value); }
        }

        [Appearance("SalesReturnMonitoringSalesReturnClose", Enabled = false)]
        public SalesReturn SalesReturn
        {
            get { return _salesReturn; }
            set { SetPropertyValue("SalesReturn", ref _salesReturn, value); }
        }

        [Appearance("SalesReturnMonitoringSalesReturnLineClose", Enabled = false)]
        public SalesReturnLine SalesReturnLine
        {
            get { return _salesReturnLine; }
            set { SetPropertyValue("SalesReturnLine", ref _salesReturnLine, value); }
        }

        [Appearance("SalesReturnMonitoringInventoryTransferInClose", Enabled = false)]
        public InventoryTransferIn InventoryTransferIn
        {
            get { return _inventoryTransferIn; }
            set { SetPropertyValue("InventoryTransferIn", ref _inventoryTransferIn, value); }
        }

        [Appearance("SalesReturnMonitoringInventoryTransferInLineClose", Enabled = false)]
        public InventoryTransferInLine InventoryTransferInLine
        {
            get { return _inventoryTransferInLine; }
            set { SetPropertyValue("InventoryTransferInLine", ref _inventoryTransferInLine, value); }
        }

        [Appearance("SalesReturnMonitoringDebitMemoClose", Enabled = false)]
        public DebitMemo DebitMemo
        {
            get { return _debitMemo; }
            set { SetPropertyValue("DebitMemo", ref _debitMemo, value); }
        }

        [Appearance("SalesReturnMonitoringDebitMemoLineClose", Enabled = false)]
        public DebitMemoLine DebitMemoLine
        {
            get { return _debitMemoLine; }
            set { SetPropertyValue("DebitMemoLine", ref _debitMemoLine, value); }
        }

        [ImmediatePostData()]
        [Appearance("SalesReturnMonitoringOrderTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public OrderType OrderType
        {
            get { return _orderType; }
            set { SetPropertyValue("OrderType", ref _orderType, value); }
        }

        [Browsable(false)]
        public XPCollection<Item> AvailableItem
        {
            get
            {
                if (OrderType == OrderType.Item)
                {
                    _availableItem = new XPCollection<Item>(Session,
                                    new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("OrderType", OrderType.Item)));

                }
                else if (OrderType == OrderType.Service)
                {
                    _availableItem = new XPCollection<Item>(Session,
                                    new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("OrderType", OrderType.Service)));
                }
                else
                {
                    _availableItem = new XPCollection<Item>(Session);
                }

                return _availableItem;

            }
        }

        [DataSourceProperty("AvailableItem", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("SalesReturnMonitoringItemClose", Enabled = false)]
        public Item Item
        {
            get { return _item; }
            set { SetPropertyValue("Item", ref _item, value); }
        }

        #region DefaultQty
        [Appearance("SalesReturnMonitoringDQtyClose", Enabled = false)]
        [ImmediatePostData()]
        public double DQty
        {
            get { return _dQty; }
            set { SetPropertyValue("DQty", ref _dQty, value); }
        }

        [Appearance("SalesReturnMonitoringDUOMClose", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure DUOM
        {
            get { return _dUom; }
            set { SetPropertyValue("DUOM", ref _dUom, value); }
        }

        [Appearance("SalesReturnMonitoringQtyClose", Enabled = false)]
        [ImmediatePostData()]
        public double Qty
        {
            get { return _qty; }
            set { SetPropertyValue("Qty", ref _qty, value); }
        }

        [Appearance("SalesReturnMonitoringUOMClose", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure UOM
        {
            get { return _uom; }
            set { SetPropertyValue("UOM", ref _uom, value); }
        }

        [Appearance("SalesReturnMonitoringTotalQtyEnabled", Enabled = false)]
        public double TQty
        {
            get { return _tQty; }
            set { SetPropertyValue("TotalQty", ref _tQty, value); }
        }
        #endregion DefaultQty

        #region Amount

        [Appearance("SalesReturnMonitoringCurrencyClose", Enabled = false)]
        public Currency Currency
        {
            get { return _currency; }
            set { SetPropertyValue("Currency", ref _currency, value); }
        }

        [ImmediatePostData()]
        [Appearance("SalesReturnMonitoringPriceGroupClose", Enabled = false)]
        public PriceGroup PriceGroup
        {
            get { return _priceGroup; }
            set { SetPropertyValue("PriceGroup", ref _priceGroup, value); }
        }

        [Browsable(false)]
        public XPCollection<Price> AvailablePrice
        {
            get
            {
                if (this.Item != null && this.PriceGroup != null)
                {
                    _availablePrice = new XPCollection<Price>(Session,
                                    new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("Item", this.Item),
                                    new BinaryOperator("PriceGroup", this.PriceGroup)));

                }

                else
                {
                    _availablePrice = new XPCollection<Price>(Session);
                }

                return _availablePrice;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailablePrice", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("SalesReturnMonitoringPriceClose", Enabled = false)]
        public Price Price
        {
            get { return _price; }
            set { SetPropertyValue("Price", ref _price, value); }
        }

        [Browsable(false)]
        public XPCollection<PriceLine> AvailablePriceLine
        {
            get
            {
                if (this.Price != null)
                {
                    _availablePriceLine = new XPCollection<PriceLine>(Session,
                                          new GroupOperator(GroupOperatorType.And,
                                          new BinaryOperator("Price", this.Price)));

                }

                else
                {
                    _availablePriceLine = new XPCollection<PriceLine>(Session);
                }

                return _availablePriceLine;

            }
        }

        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        [DataSourceProperty("AvailablePriceLine", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("SalesReturnMonitoringPriceLineClose", Enabled = false)]
        public PriceLine PriceLine
        {
            get { return _priceLine; }
            set
            {
                SetPropertyValue("PriceLine", ref _priceLine, value);
                if (!IsLoading)
                {
                    if (this._priceLine != null)
                    {
                        this.UAmount = this._priceLine.Amount2;
                    }
                }
            }
        }

        [Appearance("SalesReturnMonitoringUAmountClose", Enabled = false)]
        [ImmediatePostData()]
        public double UAmount
        {
            get { return _uAmount; }
            set { SetPropertyValue("UAmount", ref _uAmount, value); }
        }

        [Appearance("SalesReturnMonitoringTUAmountEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double TUAmount
        {
            get { return _tUAmount; }
            set { SetPropertyValue("TUAmount", ref _tUAmount, value); }
        }

        [Appearance("SalesReturnMonitoringMultiTaxClose", Enabled = false)]
        [ImmediatePostData()]
        public bool MultiTax
        {
            get { return _multiTax; }
            set { SetPropertyValue("ActivationPosting", ref _multiTax, value); }
        }

        [Appearance("SalesReturnMonitoringTaxClose", Enabled = false)]
        [Appearance("SalesReturnMonitoringTaxEnabled", Criteria = "MultiTax = true", Enabled = false)]
        [ImmediatePostData]
        [DataSourceCriteria("Active = true")]
        public Tax Tax
        {
            get { return _tax; }
            set { SetPropertyValue("Tax", ref _tax, value); }
        }

        [Appearance("SalesReturnMonitoringTxValueClose", Enabled = false)]
        [ImmediatePostData()]
        public double TxValue
        {
            get { return _txValue; }
            set { SetPropertyValue("TxValue", ref _txValue, value); }
        }

        [Appearance("SalesReturnMonitoringTxAmountEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double TxAmount
        {
            get { return _txAmount; }
            set { SetPropertyValue("TxAmount", ref _txAmount, value); }
        }

        [Appearance("SalesReturnMonitoringMultiDiscountClose", Enabled = false)]
        [ImmediatePostData()]
        public bool MultiDiscount
        {
            get { return _multiDiscount; }
            set { SetPropertyValue("MultiDiscount", ref _multiDiscount, value); }
        }

        [Appearance("SalesReturnMonitoringDiscountClose", Enabled = false)]
        [Appearance("SalesReturnMonitoringDiscountEnabled", Criteria = "MultiDiscount = true", Enabled = false)]
        [ImmediatePostData()]
        public Discount Discount
        {
            get { return _discount; }
            set { SetPropertyValue("Discount", ref _discount, value); }
        }

        [Appearance("SalesReturnMonitoringDiscClose", Enabled = false)]
        [Appearance("SalesReturnMonitoringDiscEnabled", Criteria = "MultiDiscount = true", Enabled = false)]
        [ImmediatePostData()]
        public double Disc
        {
            get { return _disc; }
            set { SetPropertyValue("Disc", ref _disc, value); }
        }

        [Appearance("SalesReturnMonitoringDiscAmountEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double DiscAmount
        {
            get { return _discAmount; }
            set { SetPropertyValue("DiscAmount", ref _discAmount, value); }
        }

        [Appearance("SalesReturnMonitoringTAmountEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double TAmount
        {
            get { return _tAmount; }
            set { SetPropertyValue("TAmount", ref _tAmount, value); }
        }

        [Appearance("SalesReturnMonitoringBillEnabled", Enabled = false)]
        public double Bill
        {
            get { return _bill; }
            set { SetPropertyValue("Bill", ref _bill, value); }
        }
        #endregion Amount

        [Appearance("SalesReturnMonitoringInventoryStatusEnabled", Enabled = false)]
        public Status InventoryStatus
        {
            get { return _inventoryStatus; }
            set { SetPropertyValue("InventoryStatus", ref _inventoryStatus, value); }
        }

        [Appearance("SalesReturnMonitoringInventoryStatusDateEnabled", Enabled = false)]
        public DateTime InventoryStatusDate
        {
            get { return _inventoryStatusDate; }
            set { SetPropertyValue("InventoryStatusDate", ref _inventoryStatusDate, value); }
        }

        [Appearance("SalesReturnMonitoringMemoStatusEnabled", Enabled = false)]
        public Status MemoStatus
        {
            get { return _memoStatus; }
            set { SetPropertyValue("MemoStatus", ref _memoStatus, value); }
        }

        [Appearance("SalesReturnMonitoringMemoStatusDateEnabled", Enabled = false)]
        public DateTime MemoStatusDate
        {
            get { return _memoStatusDate; }
            set { SetPropertyValue("MemoStatusDate", ref _memoStatusDate, value); }
        }

        [Appearance("SalesReturnMonitoringPostedCountEnabled", Enabled = false)]
        public int PostedCount
        {
            get { return _postedCount; }
            set { SetPropertyValue("PostedCount", ref _postedCount, value); }
        }

        [Appearance("SalesReturnMonitoringSignCodeClose", Enabled = false)]
        public string SignCode
        {
            get { return _signCode; }
            set { SetPropertyValue("SignCode", ref _signCode, value); }
        }

        //[Appearance("SalesReturnMonitoringReturnTypeClose", Enabled = false)]
        public ReturnType ReturnType
        {
            get { return _returnType; }
            set { SetPropertyValue("ReturnType", ref _returnType, value); }
        }

        [Browsable(false)]
        public InventoryTransferOutMonitoring InventoryTransferOutMonitoring
        {
            get { return _inventoryTransferOutMonitoring; }
            set { SetPropertyValue("InventoryTransferOutMonitoring", ref _inventoryTransferOutMonitoring, value); }
        }

        [Browsable(false)]
        public SalesOrderMonitoring SalesOrderMonitoring
        {
            get { return _salesOrderMonitoring; }
            set { SetPropertyValue("SalesOrderMonitoring", ref _salesOrderMonitoring, value); }
        }

        #endregion Field
    }
}