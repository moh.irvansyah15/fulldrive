﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using DevExpress.ExpressApp.Editors;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Shipment")]
    [RuleCombinationOfPropertiesIsUnique("FreightForwardingDestinationLineRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class FreightForwardingDestinationLine : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).

        private bool _activationPosting;
        private bool _select;
        private int _no;
        private string _code;
        private ShipmentType _shipmentType;
        private TransportType _transportType;
        private ServiceModa _serviceModa;
        private DeliveryType _deliveryType;
        private TransactionTerm _transactionTerm;
        private ItemGroup _itemGroup;
        private XPCollection<Item> _availableItem;
        private Item _item;
        private string _name;
        private string _description;
        private XPCollection<UnitOfMeasure> _availableUnitOfMeasure;
        #region InisialisasiMaxQty
        private double _mxDQTY;
        private UnitOfMeasure _mxDUom;
        private double _mxQty;
        private UnitOfMeasure _mxUom;
        private double _mxTQty;
        private double _mxUAmount;
        private double _mxTUAmount;
        #endregion InisialisasiMaxQty
        #region InisialisasiDefaultQty
        private double _dQty;
        private UnitOfMeasure _dUom;
        private double _qty;
        private UnitOfMeasure _uom;
        private double _tQty;
        #endregion InisialisasiDefaultQty
        #region InisialisasiRemainQty
        private double _rmDQty;
        private double _rmQty;
        private double _rmTQty;
        #endregion InisialisasiRemainQty
        #region InitialPostingQuantityTo
        private double _pDQty;
        private UnitOfMeasure _pDUom;
        private double _pQty;
        private UnitOfMeasure _pUom;
        private double _pTQty;
        #endregion InitialPostingQuantityTo
        #region InisialisasiAmount
        private Currency _currency;
        private PriceGroup _priceGroup;
        private PriceType _priceType;
        private XPCollection<Price> _availablePrice;
        private Price _price;
        private XPCollection<PriceLine> _availablePriceLine;
        private PriceLine _priceLine;
        private double _uAmount;
        private double _tUAmount;
        private bool _multiTax;
        private Tax _tax;
        private double _txValue;
        private double _txAmount;
        private bool _multiDiscount;
        private Discount _discount;
        private double _disc;
        private double _discAmount;
        private double _tAmount;
        #endregion InisialisasiAmount
        #region Location
        private Country _countryFrom;
        private City _cityFrom;
        private XPCollection<Area> _availableAreaFrom;
        private Area _areaFrom;
        private XPCollection<Location> _availableLocationFrom;
        private Location _locationFrom;
        private XPCollection<TransportLocation> _availableTransportLocationFrom;
        private TransportLocation _transportLocationFrom;
        private Country _countryTo;
        private City _cityTo;
        private XPCollection<Area> _availableAreaTo;
        private Area _areaTo;
        private XPCollection<Location> _availableLocationTo;
        private Location _locationTo;
        private XPCollection<TransportLocation> _availableTransportLocationTo;
        private TransportLocation _transportLocationTo;
        #endregion Location
        private Status _status;
        private DateTime _statusDate;
        private int _postedCount;
        private string _signCode;
        private ShipmentBookingMonitoring _shipmentBookingMonitoring;
        private FreightForwardingDestination _freightForwardingDestination;
        private GlobalFunction _globFunc;

        public FreightForwardingDestinationLine(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            DateTime now = DateTime.Now;
            _globFunc = new GlobalFunction();
            this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.FreightForwardingDestinationLine);
            this.Status = Status.Open;
            this.StatusDate = now;
            this.Select = true;
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        protected override void OnSaving()
        {
            base.OnSaving();
            if (!IsLoading && IsSaving)
            {
                UpdateNo();
            }
        }
        protected override void OnDeleting()
        {
            base.OnDeleting();
            if (!IsLoading)
            {
                RecoveryDeleteNo();
            }
        }

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [Appearance("FreightForwardingDestinationLineNoEnabled", Enabled = false)]
        public int No
        {
            get { return _no; }
            set { SetPropertyValue("No", ref _no, value); }
        }

        [Appearance("FreightForwardingDestinationLineSelectClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public bool Select
        {
            get { return _select; }
            set { SetPropertyValue("Select", ref _select, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("FreightForwardingDestinationLineCodeEnabled", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("FreightForwardingDestinationLineShipmentTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public ShipmentType ShipmentType
        {
            get { return _shipmentType; }
            set { SetPropertyValue("ShipmentType", ref _shipmentType, value); }
        }

        [Appearance("FreightForwardingDestinationLineServiceModaClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public ServiceModa ServiceModa
        {
            get { return _serviceModa; }
            set { SetPropertyValue("ServiceModa", ref _serviceModa, value); }
        }

        [Appearance("FreightForwardingDestinationLineTransportTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public TransportType TransportType
        {
            get { return _transportType; }
            set { SetPropertyValue("TransportType", ref _transportType, value); }
        }

        [Appearance("FreightForwardingDestinationLineDeliveryTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DeliveryType DeliveryType
        {
            get { return _deliveryType; }
            set { SetPropertyValue("DeliveryType", ref _deliveryType, value); }
        }

        [Appearance("FreightForwardingDestinationLineTransactionTermClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public TransactionTerm TransactionTerm
        {
            get { return _transactionTerm; }
            set { SetPropertyValue("TransactionTerm", ref _transactionTerm, value); }
        }

        [ImmediatePostData()]
        [Appearance("FreightForwardingDestinationLineItemGroupClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public ItemGroup ItemGroup
        {
            get { return _itemGroup; }
            set { SetPropertyValue("ItemGroup", ref _itemGroup, value); }
        }

        [Browsable(false)]
        public XPCollection<Item> AvailableItem
        {
            get
            {
                if (this.ItemGroup != null)
                {
                    _availableItem = new XPCollection<Item>(Session,
                                    new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("ItemGroup", this.ItemGroup)));

                }
                else
                {
                    _availableItem = new XPCollection<Item>(Session);
                }

                return _availableItem;
            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableItem", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("FreightForwardingDestinationLineItemClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Item Item
        {
            get { return _item; }
            set
            {
                SetPropertyValue("Item", ref _item, value);
                if (!IsLoading)
                {
                    if (this._item != null)
                    {
                        //this.DUOM = this._item.BasedUOM;
                        this.Description = GetDescriptions();
                    }
                }
            }
        }

        [Appearance("FreightForwardingDestinationLineNameClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        [Appearance("FreightForwardingDestinationLineDescriptionClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Size(512)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        [Browsable(false)]
        public XPCollection<UnitOfMeasure> AvailableUnitOfMeasure
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;
                if (Item == null)
                {
                    _availableUnitOfMeasure = new XPCollection<UnitOfMeasure>(Session);
                }
                else
                {
                    List<string> _stringUOM = new List<string>();

                    XPCollection<ItemUnitOfMeasure> _locItemUnitOfMeasures = new XPCollection<ItemUnitOfMeasure>
                                                                              (Session, new GroupOperator(GroupOperatorType.And,
                                                                               new BinaryOperator("Item", this.Item),
                                                                               new BinaryOperator("Active", true)));

                    if (_locItemUnitOfMeasures != null && _locItemUnitOfMeasures.Count() > 0)
                    {
                        foreach (ItemUnitOfMeasure _locItemUnitOfMeasure in _locItemUnitOfMeasures)
                        {
                            _stringUOM.Add(_locItemUnitOfMeasure.UOM.Code);
                        }
                    }

                    IEnumerable<string> _stringArrayUOMDistinct = _stringUOM.Distinct();
                    string[] _stringArrayUOMList = _stringArrayUOMDistinct.ToArray();
                    if (_stringArrayUOMList.Length == 1)
                    {
                        for (int i = 0; i < _stringArrayUOMList.Length; i++)
                        {
                            UnitOfMeasure _locUOM = Session.FindObject<UnitOfMeasure>(new BinaryOperator("Code", _stringArrayUOMList[i]));
                            if (_locUOM != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locUOM.Code + "'";
                                }
                            }
                        }
                    }
                    else if (_stringArrayUOMList.Length > 1)
                    {
                        for (int i = 0; i < _stringArrayUOMList.Length; i++)
                        {
                            UnitOfMeasure _locUOM = Session.FindObject<UnitOfMeasure>(new BinaryOperator("Code", _stringArrayUOMList[i]));
                            if (_locUOM != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locUOM.Code + "'";
                                }
                                else
                                {
                                    _endString = _endString + " OR [Code]=='" + _locUOM.Code + "'";
                                }
                            }
                        }
                    }
                    _fullString = _beginString + _endString;

                    if (_fullString != null)
                    {
                        _availableUnitOfMeasure = new XPCollection<UnitOfMeasure>(Session, CriteriaOperator.Parse(_fullString));
                    }

                }
                return _availableUnitOfMeasure;

            }
        }

        #region MaxDefaultQty
        [Appearance("FreightForwardingDestinationLineMxDQtyClose", Enabled = false)]
        [ImmediatePostData()]
        public double MxDQty
        {
            get { return _mxDQTY; }
            set
            {
                SetPropertyValue("MxDQty", ref _mxDQTY, value);
                if (!IsLoading)
                {
                    SetMaxTotalQty();
                }
            }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        [Appearance("FreightForwardingDestinationLineMxDUOMClose", Enabled = false)]
        public UnitOfMeasure MxDUOM
        {
            get { return _mxDUom; }
            set
            {
                SetPropertyValue("MxDUOM", ref _mxDUom, value);
                if (!IsLoading)
                {
                    SetMaxTotalQty();
                }
            }
        }

        [ImmediatePostData()]
        [Appearance("FreightForwardingDestinationLineMxQtyClose", Enabled = false)]
        public double MxQty
        {
            get { return _mxQty; }
            set
            {
                SetPropertyValue("MxQty", ref _mxQty, value);
                if (!IsLoading)
                {
                    SetMaxTotalQty();
                }
            }
        }

        [DataSourceProperty("AvailableUnitOfMeasure", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        [Appearance("FreightForwardingDestinationLineMxUOMClose", Enabled = false)]
        public UnitOfMeasure MxUOM
        {
            get { return _mxUom; }
            set
            {
                SetPropertyValue("MxUOM", ref _mxUom, value);
                if (!IsLoading)
                {
                    SetMaxTotalQty();
                }
            }
        }

        [Appearance("FreightForwardingDestinationLineMxTQtyClose", Enabled = false)]
        public double MxTQty
        {
            get { return _mxTQty; }
            set { SetPropertyValue("MxTQty", ref _mxTQty, value); }
        }

        [Appearance("FreightForwardingDestinationLineMxUAmountClose", Enabled = false)]
        public double MxUAmount
        {
            get { return _mxUAmount; }
            set { SetPropertyValue("MxUAmount", ref _mxUAmount, value); }
        }

        [Appearance("FreightForwardingDestinationLineMxTUAmountClose", Enabled = false)]
        public double MxTUAmount
        {
            get { return _mxTUAmount; }
            set { SetPropertyValue("MxTUAmount", ref _mxTUAmount, value); }
        }
        #endregion MaxDefaultQty

        #region DefaultQty
        [Appearance("FreightForwardingDestinationLineDQtyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double DQty
        {
            get { return _dQty; }
            set
            {
                SetPropertyValue("DQty", ref _dQty, value);
                if (!IsLoading)
                {
                    SetMxDQty();
                    SetTotalQty();
                    SetTotalUnitAmount();
                    SetTaxAmount();
                    SetDiscAmount();
                    SetTotalAmount();
                }
            }
        }

        [Appearance("FreightForwardingDestinationLineDUOMClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure DUOM
        {
            get { return _dUom; }
            set
            {
                SetPropertyValue("DUOM", ref _dUom, value);
                if (!IsLoading)
                {
                    SetMxDUOM();
                    SetTotalQty();
                    SetTotalUnitAmount();
                    SetTaxAmount();
                    SetDiscAmount();
                    SetTotalAmount();
                }
            }
        }

        [Appearance("FreightForwardingDestinationLineQtyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double Qty
        {
            get { return _qty; }
            set
            {
                SetPropertyValue("Qty", ref _qty, value);
                if (!IsLoading)
                {
                    SetMxQty();
                    SetTotalQty();
                    SetTotalUnitAmount();
                    SetTaxAmount();
                    SetDiscAmount();
                    SetTotalAmount();
                }
            }
        }

        [Appearance("FreightForwardingDestinationLineUOMClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceProperty("AvailableUnitOfMeasure", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure UOM
        {
            get { return _uom; }
            set
            {
                SetPropertyValue("UOM", ref _uom, value);
                if (!IsLoading)
                {
                    SetMxUOM();
                    SetTotalQty();
                    SetTotalUnitAmount();
                    SetTaxAmount();
                    SetDiscAmount();
                    SetTotalAmount();
                }
            }
        }

        [Appearance("FreightForwardingDestinationLineTotalQtyEnabled", Enabled = false)]
        public double TQty
        {
            get { return _tQty; }
            set { SetPropertyValue("TotalQty", ref _tQty, value); }
        }
        #endregion DefaultQty

        #region RemainQty

        [Appearance("FreightForwardingDestinationLineRmDQtyClose", Enabled = false)]
        public double RmDQty
        {
            get { return _rmDQty; }
            set { SetPropertyValue("RmDQty", ref _rmDQty, value); }
        }

        [Appearance("FreightForwardingDestinationLineRmQtyClose", Enabled = false)]
        public double RmQty
        {
            get { return _rmQty; }
            set { SetPropertyValue("RmQty", ref _rmQty, value); }
        }

        [Appearance("FreightForwardingDestinationLineRmTQtyClose", Enabled = false)]
        public double RmTQty
        {
            get { return _rmTQty; }
            set { SetPropertyValue("RmTQty", ref _rmTQty, value); }
        }

        #endregion RemainQty

        #region PostingQty

        [Appearance("FreightForwardingDestinationLinePDQtyClose", Enabled = false)]
        public double PDQty
        {
            get { return _pDQty; }
            set
            {
                SetPropertyValue("PDQty", ref _pDQty, value);
                if (!IsLoading)
                {
                    SetTotalPQty();
                }
            }
        }

        [Appearance("FreightForwardingDestinationLinePDUOMClose", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure PDUOM
        {
            get { return _pDUom; }
            set
            {
                SetPropertyValue("PDUOM", ref _pDUom, value);
                if (!IsLoading)
                {
                    SetTotalPQty();
                }
            }
        }

        [Appearance("FreightForwardingDestinationLinePQtyToClose", Enabled = false)]
        public double PQty
        {
            get { return _pQty; }
            set
            {
                SetPropertyValue("PQty", ref _pQty, value);
                if (!IsLoading)
                {
                    SetTotalPQty();
                }
            }
        }

        [Appearance("FreightForwardingDestinationLinePUOMClose", Enabled = false)]
        [DataSourceProperty("AvailableUnitOfMeasure", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure PUOM
        {
            get { return _pUom; }
            set
            {
                SetPropertyValue("PUOM", ref _pUom, value);
                if (!IsLoading)
                {
                    SetTotalPQty();
                }
            }
        }

        [Appearance("FreightForwardingDestinationLinePTQtyEnabled", Enabled = false)]
        public double PTQty
        {
            get { return _pTQty; }
            set { SetPropertyValue("PTQty", ref _pTQty, value); }
        }

        #endregion PostingQty

        #region Amount
        [Appearance("FreightForwardingDestinationLineCurrencyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Currency Currency
        {
            get { return _currency; }
            set { SetPropertyValue("Currency", ref _currency, value); }
        }

        [ImmediatePostData()]
        [Appearance("FreightForwardingDestinationLinePriceGroupClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PriceGroup PriceGroup
        {
            get { return _priceGroup; }
            set { SetPropertyValue("PriceGroup", ref _priceGroup, value); }
        }

        [ImmediatePostData()]
        [Appearance("FreightForwardingDestinationLinePriceTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PriceType PriceType
        {
            get { return _priceType; }
            set { SetPropertyValue("PriceType", ref _priceType, value); }
        }

        [Browsable(false)]
        public XPCollection<Price> AvailablePrice
        {
            get
            {
                if (this.Item != null && this.PriceGroup != null)
                {
                    _availablePrice = new XPCollection<Price>(Session,
                                    new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("Item", this.Item),
                                    new BinaryOperator("PriceGroup", this.PriceGroup),
                                    new BinaryOperator("PriceType", this.PriceType)));

                }
                else if (this.Item != null && this.PriceGroup == null)
                {
                    _availablePrice = new XPCollection<Price>(Session,
                                    new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("Item", this.Item),
                                    new BinaryOperator("PriceType", this.PriceType)));
                }
                else if (this.Item == null && this.PriceGroup != null)
                {
                    _availablePrice = new XPCollection<Price>(Session,
                                    new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("PriceGroup", this.PriceGroup),
                                    new BinaryOperator("PriceType", this.PriceType)));
                }
                else
                {
                    _availablePrice = new XPCollection<Price>(Session);
                }

                return _availablePrice;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailablePrice", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("FreightForwardingDestinationLinePriceClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Price Price
        {
            get { return _price; }
            set { SetPropertyValue("Price", ref _price, value); }
        }

        [Browsable(false)]
        public XPCollection<PriceLine> AvailablePriceLine
        {
            get
            {
                if (this.Price != null)
                {
                    _availablePriceLine = new XPCollection<PriceLine>(Session,
                                          new GroupOperator(GroupOperatorType.And,
                                          new BinaryOperator("Price", this.Price),
                                          new BinaryOperator("Active", true)));

                }
                else
                {
                    _availablePriceLine = new XPCollection<PriceLine>(Session);
                }

                return _availablePriceLine;

            }
        }

        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        [DataSourceProperty("AvailablePriceLine", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("FreightForwardingDestinationLinePriceLineClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PriceLine PriceLine
        {
            get { return _priceLine; }
            set
            {
                SetPropertyValue("PriceLine", ref _priceLine, value);
                if (!IsLoading)
                {
                    if (this._priceLine != null)
                    {
                        this.UAmount = this._priceLine.Amount2;
                    }
                }
            }
        }

        [Appearance("FreightForwardingDestinationLineUAmountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double UAmount
        {
            get { return _uAmount; }
            set
            {
                SetPropertyValue("UAmount", ref _uAmount, value);
                if (!IsLoading)
                {
                    _globFunc = new GlobalFunction();
                    if (this._uAmount > 0)
                    {
                        if (_globFunc.GetRoundingList(Session, ObjectList.ShipmentBookingLine, FieldName.UAmount) == true)
                        {
                            this._uAmount = _globFunc.GetRoundUp(Session, this._uAmount, ObjectList.ShipmentBookingLine, FieldName.UAmount);
                        }
                    }
                    SetTotalUnitAmount();
                    SetTaxAmount();
                    SetDiscAmount();
                    SetTotalAmount();
                }
            }
        }

        [Appearance("FreightForwardingDestinationLineTUAmountEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double TUAmount
        {
            get { return _tUAmount; }
            set { SetPropertyValue("TUAmount", ref _tUAmount, value); }
        }

        [Appearance("FreightForwardingDestinationLineMultiTaxClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public bool MultiTax
        {
            get { return _multiTax; }
            set
            {
                SetPropertyValue("MultiTax", ref _multiTax, value);
                if (!IsLoading)
                {
                    SetNormalTax();
                }
            }
        }

        [Appearance("FreightForwardingDestinationLineTaxClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Appearance("FreightForwardingDestinationLineTaxEnabled", Criteria = "MultiTax = true", Enabled = false)]
        [ImmediatePostData]
        [DataSourceCriteria("Active = true")]
        public Tax Tax
        {
            get { return _tax; }
            set
            {
                SetPropertyValue("Tax", ref _tax, value);
                if (!IsLoading)
                {
                    if (this._tax != null)
                    {
                        this.TxValue = this._tax.Value;
                        SetTaxAmount();
                        SetTotalAmount();
                    }
                    else
                    {
                        SetNormalTax();
                    }
                }
            }
        }

        [Appearance("FreightForwardingDestinationLineTxValueClose", Enabled = false)]
        [ImmediatePostData()]
        public double TxValue
        {
            get { return _txValue; }
            set
            {
                SetPropertyValue("TxValue", ref _txValue, value);
                if (!IsLoading)
                {
                    SetTaxAmount();
                    SetTotalAmount();
                }
            }
        }

        [Appearance("FreightForwardingDestinationLineTxAmountEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double TxAmount
        {
            get { return _txAmount; }
            set
            {
                SetPropertyValue("TxAmount", ref _txAmount, value);
                if (!IsLoading)
                {
                    _globFunc = new GlobalFunction();
                    if (this._txAmount > 0)
                    {
                        if (_globFunc.GetRoundingList(Session, ObjectList.SalesOrderLine, FieldName.TxAmount) == true)
                        {
                            this._txAmount = _globFunc.GetRoundUp(Session, this._txAmount, ObjectList.SalesOrderLine, FieldName.TxAmount);
                        }
                    }

                    SetTotalAmount();
                }
            }
        }

        [Appearance("FreightForwardingDestinationLineMultiDiscountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public bool MultiDiscount
        {
            get { return _multiDiscount; }
            set
            {
                SetPropertyValue("MultiDiscount", ref _multiDiscount, value);
                if (!IsLoading)
                {
                    SetNormalDiscount();
                }
            }
        }

        [Appearance("FreightForwardingDestinationLineDiscountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Appearance("FreightForwardingDestinationLineDiscountEnabled", Criteria = "MultiDiscount = true", Enabled = false)]
        [ImmediatePostData()]
        public Discount Discount
        {
            get { return _discount; }
            set
            {
                SetPropertyValue("Discount", ref _discount, value);
                if (!IsLoading)
                {
                    if (this._discount != null)
                    {
                        this.Disc = this._discount.Value;
                    }
                    else
                    {
                        SetNormalDiscount();
                    }
                }
            }
        }

        [Appearance("FreightForwardingDestinationLineDiscClose", Enabled = false)]
        [Appearance("FreightForwardingDestinationLineDiscEnabled", Criteria = "MultiDiscount = true", Enabled = false)]
        [ImmediatePostData()]
        public double Disc
        {
            get { return _disc; }
            set
            {
                SetPropertyValue("Disc", ref _disc, value);
                if (!IsLoading)
                {
                    SetDiscAmount();
                    SetTotalAmount();
                }
            }
        }

        [Appearance("FreightForwardingDestinationLineDiscAmountEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double DiscAmount
        {
            get { return _discAmount; }
            set
            {
                SetPropertyValue("DiscAmount", ref _discAmount, value);
                if (!IsLoading)
                {
                    _globFunc = new GlobalFunction();
                    if (this._discAmount > 0)
                    {
                        if (_globFunc.GetRoundingList(Session, ObjectList.SalesOrderLine, FieldName.DiscAmount) == true)
                        {
                            this._discAmount = _globFunc.GetRoundUp(Session, this._discAmount, ObjectList.SalesOrderLine, FieldName.DiscAmount);
                        }
                    }
                }
            }
        }

        [Appearance("FreightForwardingDestinationLineTPriceEnabled", Criteria = "ActivationQuantity = true", Enabled = false)]
        [ImmediatePostData()]
        public double TAmount
        {
            get { return _tAmount; }
            set
            {
                SetPropertyValue("TAmount", ref _tAmount, value);
                if (!IsLoading)
                {
                    _globFunc = new GlobalFunction();
                    if (this._tAmount > 0)
                    {
                        if (_globFunc.GetRoundingList(Session, ObjectList.SalesOrderLine, FieldName.TAmount) == true)
                        {
                            this._tAmount = _globFunc.GetRoundUp(Session, this._tAmount, ObjectList.SalesOrderLine, FieldName.TAmount);
                        }
                    }
                }
            }
        }
        #endregion Amount

        #region Location
        [Appearance("FreightForwardingDestinationLineCountryFromClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public Country CountryFrom
        {
            get { return _countryFrom; }
            set { SetPropertyValue("CountryFrom", ref _countryFrom, value); }
        }

        [Appearance("FreightForwardingDestinationLineCityFromClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public City CityFrom
        {
            get { return _cityFrom; }
            set { SetPropertyValue("CityFrom", ref _cityFrom, value); }
        }

        [Browsable(false)]
        public XPCollection<Area> AvailableAreaFrom
        {
            get
            {
                if (this.CountryFrom != null && this.CityFrom != null)
                {
                    _availableAreaFrom = new XPCollection<Area>(Session,
                                    new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("Country", this.CountryFrom),
                                    new BinaryOperator("City", this.CityFrom)));

                }
                else if (this.CountryFrom != null && this.CityFrom == null)
                {
                    _availableAreaFrom = new XPCollection<Area>(Session,
                                    new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("Country", this.CountryFrom)));
                }
                else if (this.CountryFrom == null && this.CityFrom != null)
                {
                    _availableAreaFrom = new XPCollection<Area>(Session,
                                    new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("City", this.CityFrom)));
                }
                else
                {
                    _availableAreaFrom = new XPCollection<Area>(Session);
                }

                return _availableAreaFrom;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableAreaFrom", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("FreightForwardingDestinationLineAreaFromClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public Area AreaFrom
        {
            get { return _areaFrom; }
            set { SetPropertyValue("AreaFrom", ref _areaFrom, value); }
        }

        [Browsable(false)]
        public XPCollection<Location> AvailableLocationFrom
        {
            get
            {
                if (this.AreaFrom != null)
                {
                    _availableLocationFrom = new XPCollection<Location>(Session,
                                          new GroupOperator(GroupOperatorType.And,
                                          new BinaryOperator("Area", this.AreaFrom),
                                          new BinaryOperator("Active", true)));

                }
                else
                {
                    _availableLocationFrom = new XPCollection<Location>(Session);
                }

                return _availableLocationFrom;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableLocationFrom", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("FreightForwardingDestinationLineLocationFromClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public Location LocationFrom
        {
            get { return _locationFrom; }
            set { SetPropertyValue("LocationFrom", ref _locationFrom, value); }
        }

        [Browsable(false)]
        public XPCollection<TransportLocation> AvailableTransportLocationFrom
        {
            get
            {
                if (this.CountryFrom != null)
                {
                    _availableTransportLocationFrom = new XPCollection<TransportLocation>(Session,
                                          new GroupOperator(GroupOperatorType.And,
                                          new BinaryOperator("Country", this.CountryFrom),
                                          new BinaryOperator("Active", true)));

                }
                else
                {
                    _availableTransportLocationFrom = new XPCollection<TransportLocation>(Session);
                }

                return _availableTransportLocationFrom;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableTransportLocationFrom", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("FreightForwardingDestinationLineTransportLocationFromClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public TransportLocation TransportLocationFrom
        {
            get { return _transportLocationFrom; }
            set { SetPropertyValue("TransportLocationFrom", ref _transportLocationFrom, value); }
        }

        [Appearance("FreightForwardingDestinationLineCountryToClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public Country CountryTo
        {
            get { return _countryTo; }
            set { SetPropertyValue("CountryTo", ref _countryTo, value); }
        }

        [Appearance("FreightForwardingDestinationLineCityToClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public City CityTo
        {
            get { return _cityTo; }
            set { SetPropertyValue("CityTo", ref _cityTo, value); }
        }

        [Browsable(false)]
        public XPCollection<Area> AvailableAreaTo
        {
            get
            {
                if (this.CountryTo != null && this.CityTo != null)
                {
                    _availableAreaTo = new XPCollection<Area>(Session,
                                    new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("Country", this.CountryTo),
                                    new BinaryOperator("City", this.CityTo)));

                }
                else if (this.CountryTo != null && this.CityTo == null)
                {
                    _availableAreaTo = new XPCollection<Area>(Session,
                                    new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("Country", this.CountryTo)));
                }
                else if (this.CountryTo == null && this.CityTo != null)
                {
                    _availableAreaTo = new XPCollection<Area>(Session,
                                    new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("City", this.CityTo)));
                }
                else
                {
                    _availableAreaTo = new XPCollection<Area>(Session);
                }

                return _availableAreaTo;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableAreaTo", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("FreightForwardingDestinationLineAreaToClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public Area AreaTo
        {
            get { return _areaTo; }
            set { SetPropertyValue("AreaTo", ref _areaTo, value); }
        }

        [Browsable(false)]
        public XPCollection<Location> AvailableLocationTo
        {
            get
            {
                if (this.AreaTo != null)
                {
                    _availableLocationTo = new XPCollection<Location>(Session,
                                          new GroupOperator(GroupOperatorType.And,
                                          new BinaryOperator("Area", this.AreaTo),
                                          new BinaryOperator("Active", true)));

                }
                else
                {
                    _availableLocationTo = new XPCollection<Location>(Session);
                }

                return _availableLocationTo;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableLocationTo", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("FreightForwardingDestinationLineLocationToClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public Location LocationTo
        {
            get { return _locationTo; }
            set { SetPropertyValue("LocationTo", ref _locationTo, value); }
        }

        [Browsable(false)]
        public XPCollection<TransportLocation> AvailableTransportLocationTo
        {
            get
            {
                if (this.CountryTo != null)
                {
                    _availableTransportLocationTo = new XPCollection<TransportLocation>(Session,
                                          new GroupOperator(GroupOperatorType.And,
                                          new BinaryOperator("Country", this.CountryTo),
                                          new BinaryOperator("Active", true)));

                }
                else
                {
                    _availableTransportLocationTo = new XPCollection<TransportLocation>(Session);
                }

                return _availableTransportLocationTo;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableTransportLocationTo", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("FreightForwardingDestinationLineTransportLocationToClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public TransportLocation TransportLocationTo
        {
            get { return _transportLocationTo; }
            set { SetPropertyValue("TransportLocationTo", ref _transportLocationTo, value); }
        }

        #endregion Location

        [Appearance("FreightForwardingDestinationLineStatusEnabled", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("FreightForwardingDestinationLineStatusDateEnabled", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("FreightForwardingDestinationLinePostedCountEnabled", Enabled = false)]
        public int PostedCount
        {
            get { return _postedCount; }
            set { SetPropertyValue("PostedCount", ref _postedCount, value); }
        }

        [Appearance("FreightForwardingDestinationLineSignCodeEnabled", Enabled = false)]
        public string SignCode
        {
            get { return _signCode; }
            set { SetPropertyValue("SignCode", ref _signCode, value); }
        }

        [Appearance("FreightForwardingDestinationLineShipmentBookingMonitoringEnabled", Enabled = false)]
        public ShipmentBookingMonitoring ShipmentBookingMonitoring
        {
            get { return _shipmentBookingMonitoring; }
            set { SetPropertyValue("ShipmentBookingMonitoring", ref _shipmentBookingMonitoring, value); }
        }

        [Association("FreightForwardingDestination-FreightForwardingDestinationLines")]
        [Appearance("FreightForwardingDestinationLineFreightForwardingDestinationEnabled", Enabled = false)]
        public FreightForwardingDestination FreightForwardingDestination
        {
            get { return _freightForwardingDestination; }
            set
            {
                SetPropertyValue("FreightForwardingDestination", ref _freightForwardingDestination, value);
                if (!IsLoading)
                {
                    if (this._freightForwardingDestination != null)
                    {
                        if (this._freightForwardingDestination.TransportType != null)
                        {
                            this.TransportType = this._freightForwardingDestination.TransportType;
                        }
                        if (this._freightForwardingDestination.ShipmentType != ShipmentType.None)
                        {
                            this.ShipmentType = this._freightForwardingDestination.ShipmentType;
                        }
                        if (this._freightForwardingDestination.CountryFrom != null)
                        {
                            this.CountryFrom = this._freightForwardingDestination.CountryFrom;
                        }
                        if (this._freightForwardingDestination.CityFrom != null)
                        {
                            this.CityFrom = this._freightForwardingDestination.CityFrom;
                        }
                        if (this._freightForwardingDestination.CountryTo != null)
                        {
                            this.CountryTo = this._freightForwardingDestination.CountryTo;
                        }
                        if (this._freightForwardingDestination.CityTo != null)
                        {
                            this.CityTo = this._freightForwardingDestination.CityTo;
                        }

                    }
                }
            }
        }

        #endregion Field

        //=========================================================== Code Only =====================================================

        #region CodeOnly

        #region No

        public void UpdateNo()
        {
            try
            {
                if (!IsLoading && Session.IsNewObject(this))
                {
                    if (this.FreightForwardingDestination != null)
                    {
                        object _makRecord = Session.Evaluate<FreightForwardingDestinationLine>(CriteriaOperator.Parse("Max(No)"), CriteriaOperator.Parse("FreightForwardingDestination=?", this.FreightForwardingDestination));
                        this.No = Convert.ToInt32(_makRecord) + 1;
                        this.Save();
                        RecoveryUpdateNo();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FreightForwardingDestinationLine " + ex.ToString());
            }
        }

        public void RecoveryUpdateNo()
        {
            try
            {
                if (this.FreightForwardingDestination != null)
                {
                    FreightForwardingDestination _numHeader = Session.FindObject<FreightForwardingDestination>
                                            (new BinaryOperator("Code", this.FreightForwardingDestination.Code));

                    XPCollection<FreightForwardingDestinationLine> _numLines = new XPCollection<FreightForwardingDestinationLine>
                                                             (Session, new BinaryOperator("FreightForwardingDestination", _numHeader),
                                                              new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i1 = 0;
                        foreach (FreightForwardingDestinationLine _numLine in _numLines)
                        {
                            i1 += 1;
                            _numLine.No = i1;
                            _numLine.Save();
                        }
                        i1 = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FreightForwardingDestinationLine " + ex.ToString());
            }
        }

        public void RecoveryDeleteNo()
        {
            try
            {
                if (this.FreightForwardingDestination != null)
                {
                    FreightForwardingDestination _numHeader = Session.FindObject<FreightForwardingDestination>
                                            (new BinaryOperator("Code", this.FreightForwardingDestination.Code));

                    XPCollection<FreightForwardingDestinationLine> _numLines = new XPCollection<FreightForwardingDestinationLine>
                                                             (Session, new GroupOperator(GroupOperatorType.And,
                                                              new BinaryOperator("This", this, BinaryOperatorType.NotEqual),
                                                              new BinaryOperator("FreightForwardingDestination", _numHeader)),
                                                              new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i = 0;
                        foreach (FreightForwardingDestinationLine _numLine in _numLines)
                        {
                            i += 1;
                            _numLine.No = i;
                            _numLine.Save();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FreightForwardingDestinationLine " + ex.ToString());
            }
        }

        #endregion No

        public string GetDescriptions()
        {
            string _result = "";
            try
            {
                if (this._item != null)
                {
                    string _result1 = null;
                    string _result2 = null;
                    if (this._item.Name != null)
                    {
                        _result1 = this._item.Name;
                    }
                    if (this._item.Description != null)
                    {
                        _result2 = this._item.Description;
                    }
                    _result = _result1 + " " + _result2;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FreightForwardingDestinationLine " + ex.ToString());
            }

            return _result;
        }

        #region Set

        private void SetMaxTotalQty()
        {
            try
            {
                double _locInvLineTotal = 0;
                DateTime now = DateTime.Now;
                if (this.FreightForwardingDestination != null)
                {
                    if (this.Item != null && this.MxUOM != null && this.MxDUOM != null)
                    {
                        ItemUnitOfMeasure _locItemUOM = Session.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", this.Item),
                                                         new BinaryOperator("UOM", this.MxUOM),
                                                         new BinaryOperator("DefaultUOM", this.MxDUOM),
                                                         new BinaryOperator("Active", true)));
                        if (_locItemUOM != null)
                        {
                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.MxQty * _locItemUOM.DefaultConversion + this.MxDQty;
                            }
                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.MxQty / _locItemUOM.Conversion + this.MxDQty;
                            }
                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.MxQty + this.MxDQty;
                            }

                            this.MxTQty = _locInvLineTotal;
                        }
                    }
                    else
                    {
                        _locInvLineTotal = this.MxQty + this.MxDQty;
                        this.MxTQty = _locInvLineTotal;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FreightForwardingDestinationLine " + ex.ToString());
            }
        }

        private void SetTotalQty()
        {
            try
            {
                double _locInvLineTotal = 0;
                DateTime now = DateTime.Now;
                if (this.FreightForwardingDestination != null)
                {
                    if (this.Item != null && this.UOM != null && this.DUOM != null)
                    {
                        ItemUnitOfMeasure _locItemUOM = Session.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", this.Item),
                                                         new BinaryOperator("UOM", this.UOM),
                                                         new BinaryOperator("DefaultUOM", this.DUOM),
                                                         new BinaryOperator("Active", true)));
                        if (_locItemUOM != null)
                        {
                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.Qty * _locItemUOM.DefaultConversion + this.DQty;
                            }
                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.Qty / _locItemUOM.Conversion + this.DQty;
                            }
                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.Qty + this.DQty;
                            }

                            this.TQty = _locInvLineTotal;
                        }
                    }
                    else
                    {
                        _locInvLineTotal = this.Qty + this.DQty;
                        this.TQty = _locInvLineTotal;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FreightForwardingDestinationLine " + ex.ToString());
            }
        }

        private void SetTotalUnitAmount()
        {
            try
            {
                _globFunc = new GlobalFunction();

                if (_tQty >= 0 & _uAmount >= 0)
                {
                    if (_globFunc.GetRoundingList(Session, ObjectList.FreightForwardingDestinationLine, FieldName.TUAmount) == true)
                    {
                        this.TUAmount = _globFunc.GetRoundUp(Session, (this.TQty * this.UAmount), ObjectList.FreightForwardingDestinationLine, FieldName.TUAmount);
                    }
                    else
                    {
                        this.TUAmount = this.TQty * this.UAmount;
                    }

                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FreightForwardingDestinationLine " + ex.ToString());
            }
        }

        private void SetTaxAmount()
        {
            try
            {
                _globFunc = new GlobalFunction();

                if (_uAmount >= 0 && Tax.Value >= 0)
                {
                    if (_globFunc.GetRoundingList(Session, ObjectList.FreightForwardingDestinationLine, FieldName.TxAmount) == true)
                    {
                        this.TxAmount = _globFunc.GetRoundUp(Session, (this.TUAmount * this.TxValue / 100), ObjectList.FreightForwardingDestinationLine, FieldName.TxAmount);
                    }
                    else
                    {
                        this.TxAmount = this.TUAmount * this.TxValue / 100;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FreightForwardingDestinationLine " + ex.ToString());
            }
        }

        private void SetDiscAmount()
        {
            try
            {
                _globFunc = new GlobalFunction();

                if (_uAmount >= 0 && _discAmount >= 0)
                {
                    if (_globFunc.GetRoundingList(Session, ObjectList.FreightForwardingDestinationLine, FieldName.DiscAmount) == true)
                    {
                        this.DiscAmount = _globFunc.GetRoundUp(Session, (this.TUAmount * this.Disc / 100), ObjectList.FreightForwardingDestinationLine, FieldName.DiscAmount);
                    }
                    else
                    {
                        this.DiscAmount = this.TUAmount * this.Disc / 100;
                    }

                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FreightForwardingDestinationLine " + ex.ToString());
            }
        }

        private void SetTotalAmount()
        {
            try
            {
                _globFunc = new GlobalFunction();

                if (_uAmount >= 0 && _txAmount >= 0 && _discAmount >= 0)
                {
                    if (!IsLoading)
                    {
                        if (this.Tax != null)
                        {
                            if (this.Tax.TaxNature == TaxNature.Increase)
                            {
                                if (_globFunc.GetRoundingList(Session, ObjectList.FreightForwardingDestinationLine, FieldName.TAmount) == true)
                                {
                                    this.TAmount = _globFunc.GetRoundUp(Session, (this.TUAmount + this.TxAmount - this.DiscAmount), ObjectList.FreightForwardingDestinationLine, FieldName.TAmount);
                                }
                                else
                                {
                                    this.TAmount = this.TUAmount + this.TxAmount - this.DiscAmount;
                                }
                            }
                            else if (this.Tax.TaxNature == TaxNature.Decrease)
                            {
                                if (_globFunc.GetRoundingList(Session, ObjectList.FreightForwardingDestinationLine, FieldName.TAmount) == true)
                                {
                                    this.TAmount = _globFunc.GetRoundUp(Session, (this.TUAmount - this.TxAmount - this.DiscAmount), ObjectList.FreightForwardingDestinationLine, FieldName.TAmount);
                                }
                                else
                                {
                                    this.TAmount = this.TUAmount - this.TxAmount - this.DiscAmount;
                                }
                            }
                            else if (this.Tax.TaxNature == TaxNature.None)
                            {
                                if (_globFunc.GetRoundingList(Session, ObjectList.FreightForwardingDestinationLine, FieldName.TAmount) == true)
                                {
                                    this.TAmount = _globFunc.GetRoundUp(Session, (this.TUAmount - this.DiscAmount), ObjectList.FreightForwardingDestinationLine, FieldName.TAmount);
                                }
                                else
                                {
                                    this.TAmount = this.TUAmount - this.DiscAmount;
                                }
                            }
                            else
                            {
                                if (_globFunc.GetRoundingList(Session, ObjectList.FreightForwardingDestinationLine, FieldName.TAmount) == true)
                                {
                                    this.TAmount = _globFunc.GetRoundUp(Session, (this.TUAmount + this.TxAmount - this.DiscAmount), ObjectList.FreightForwardingDestinationLine, FieldName.TAmount);
                                }
                                else
                                {
                                    this.TAmount = this.TUAmount + this.TxAmount - this.DiscAmount;
                                }
                            }
                        }
                        //clm
                        else
                        {
                            this.TAmount = this.TUAmount + this.TxAmount - this.DiscAmount;
                        }
                    }
                }
                //clm
                else
                {
                    this.TAmount = this.TUAmount + this.TxAmount - this.DiscAmount;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FreightForwardingDestinationLine " + ex.ToString());
            }
        }

        private void SetTotalPQty()
        {
            try
            {
                double _locInvLineTotal = 0;
                DateTime now = DateTime.Now;
                if (this.FreightForwardingDestination != null)
                {
                    if (this.Item != null && this.UOM != null && this.DUOM != null)
                    {
                        ItemUnitOfMeasure _locItemUOM = Session.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", this.Item),
                                                         new BinaryOperator("UOM", this.UOM),
                                                         new BinaryOperator("DefaultUOM", this.DUOM),
                                                         new BinaryOperator("Active", true)));
                        if (_locItemUOM != null)
                        {
                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.PQty * _locItemUOM.DefaultConversion + this.PDQty;
                            }
                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.PQty / _locItemUOM.Conversion + this.PDQty;
                            }
                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.PQty + this.PDQty;
                            }

                            this.PTQty = _locInvLineTotal;
                        }
                    }
                    else
                    {
                        _locInvLineTotal = this.PQty + this.PDQty;
                        this.PTQty = _locInvLineTotal;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FreightForwardingDestinationLine " + ex.ToString());
            }
        }

        private void SetNormalTax()
        {
            try
            {
                if (_tax != null && _txValue > 0 && _txAmount > 0)
                {
                    this.Tax = this.Tax;
                    this.TxValue = this.TxValue;
                    this.TxAmount = this.TxAmount;
                }
                else
                {
                    this.TxValue = 0;
                    this.TxAmount = 0;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FreightForwardingDestinationLine " + ex.ToString());
            }
        }

        private void SetNormalDiscount()
        {
            try
            {
                if (_discount != null)
                {
                    this.Disc = this.Disc;
                    this.DiscAmount = this.DiscAmount;
                }
                else
                {
                    this.Disc = 0;
                    this.DiscAmount = 0;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FreightForwardingDestinationLine " + ex.ToString());
            }
        }

        #region SetMaxInQty

        private void SetMxDQty()
        {
            try
            {
                if (this.FreightForwardingDestination != null)
                {

                    XPCollection<ShipmentApprovalLine> _locShipmentApprovalLineXPOs = new XPCollection<ShipmentApprovalLine>
                                                                   (Session, new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("FreightForwardingDestination", this.FreightForwardingDestination)));

                    if (_locShipmentApprovalLineXPOs != null && _locShipmentApprovalLineXPOs.Count() > 0)
                    {
                        if (this.Status == Status.Posted || this.PostedCount > 0)
                        {
                            if (this.RmDQty > 0)
                            {
                                if (this._dQty > this.RmDQty)
                                {
                                    this._dQty = this.RmDQty;
                                }
                            }
                        }
                        else if (this.Status == Status.Close || this.PostedCount > 0)
                        {
                            this._dQty = 0;
                        }
                    }
                    else
                    {
                        if ((this.Status == Status.Open || this.Status == Status.Progress) && this.PostedCount <= 0)
                        {
                            if (this._dQty > 0)
                            {
                                this.MxDQty = this._dQty;
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FreightForwardingDestinationLine " + ex.ToString());
            }
        }

        private void SetMxDUOM()
        {
            try
            {
                if (this.FreightForwardingDestination != null)
                {
                    ShipmentApprovalLine _locShipmentApprovalLineXPO = Session.FindObject<ShipmentApprovalLine>
                                                                   (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("FreightForwardingDestination", this.FreightForwardingDestination)));
                    if (_locShipmentApprovalLineXPO == null)
                    {
                        if ((this.Status == Status.Open || this.Status == Status.Progress)
                            && this.PostedCount <= 0)
                        {
                            this.MxDUOM = this.DUOM;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FreightForwardingDestinationLine " + ex.ToString());
            }
        }

        private void SetMxQty()
        {
            try
            {
                if (this.FreightForwardingDestination != null)
                {
                    XPCollection<ShipmentApprovalLine> _locShipmentApprovalLineXPOs = new XPCollection<ShipmentApprovalLine>
                                                                   (Session, new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("FreightForwardingDestination", this.FreightForwardingDestination)));
                    if (_locShipmentApprovalLineXPOs != null && _locShipmentApprovalLineXPOs.Count() > 0)
                    {
                        if (this.Status == Status.Posted || this.PostedCount > 0)
                        {
                            if (this.RmQty > 0)
                            {
                                if (this._qty > this.RmQty)
                                {
                                    this._qty = this.RmQty;
                                }
                            }
                        }
                        else if (this.Status == Status.Close || this.PostedCount > 0)
                        {
                            this._qty = 0;
                        }
                    }
                    else
                    {
                        if ((this.Status == Status.Open || this.Status == Status.Progress) && this.PostedCount <= 0)
                        {
                            if (this._qty > 0)
                            {
                                this.MxQty = this._qty;
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FreightForwardingDestinationLine " + ex.ToString());
            }
        }

        private void SetMxUOM()
        {
            try
            {
                if (this.FreightForwardingDestination != null)
                {
                    ShipmentApprovalLine _locShipmentApprovalLineXPO = Session.FindObject<ShipmentApprovalLine>
                                                                   (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("FreightForwardingDestination", this.FreightForwardingDestination)));
                    if (_locShipmentApprovalLineXPO == null)
                    {
                        if ((this.Status == Status.Open || this.Status == Status.Progress)
                            && this.PostedCount <= 0)
                        {
                            this.MxUOM = this.UOM;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FreightForwardingDestinationLine " + ex.ToString());
            }
        }

        #endregion SetMaxInQty

        #endregion Set

        #endregion CodeOnly

    }
}