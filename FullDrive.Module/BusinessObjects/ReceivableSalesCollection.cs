﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;


namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("ReceivableSalesCollectionRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class ReceivableSalesCollection : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private string _code;
        private XPCollection<SalesInvoice> _availableSalesInvoice;
        private SalesInvoice _salesInvoice;
        private XPCollection<SalesPrePaymentInvoice> _availableSalesPrePaymentInvoice;
        private SalesPrePaymentInvoice _salesPrePaymentInvoice;
        private ReceivableTransaction _receivableTransaction;
        private Status _status;
        private DateTime _statusDate;
        private int _postedCount;
        private GlobalFunction _globFunc;

        public ReceivableSalesCollection(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.ReceivableSalesCollection);
                this.Status = Status.Open;
                this.StatusDate = now;
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("ReceivableSalesCollectionCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Browsable(false)]
        public XPCollection<SalesInvoice> AvailableSalesInvoice
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;

                if (!IsLoading)
                {
                    if (this.ReceivableTransaction != null )
                    {
                        if (this.ReceivableTransaction.SalesToCustomer != null && this.ReceivableTransaction.PriceGroup != null && this.ReceivableTransaction.BillToCustomer != null)
                        {
                            XPQuery<SalesInvoiceMonitoring> _salesInvoiceMonitoringsQuery = new XPQuery<SalesInvoiceMonitoring>(Session);

                            var _salesInvoiceMonitorings = from sim in _salesInvoiceMonitoringsQuery
                                                                   where (sim.Status != Status.Close 
                                                                   && sim.PriceGroup == this.ReceivableTransaction.PriceGroup
                                                                   && sim.SalesInvoice.SalesToCustomer == this.ReceivableTransaction.SalesToCustomer
                                                                   && sim.SalesInvoice.BillToCustomer == this.ReceivableTransaction.BillToCustomer
                                                                   && sim.ReceivableTransaction == null
                                                                   )
                                                                   group sim by sim.SalesInvoice into g
                                                                   select new { SalesInvoice = g.Key };

                            if (_salesInvoiceMonitorings != null && _salesInvoiceMonitorings.Count() > 0)
                            {
                                List<string> _stringSIM = new List<string>();

                                foreach (var _salesInvoiceMonitoring in _salesInvoiceMonitorings)
                                {
                                    if (_salesInvoiceMonitoring != null)
                                    {
                                        _stringSIM.Add(_salesInvoiceMonitoring.SalesInvoice.Code);
                                    }
                                }

                                IEnumerable<string> _stringArraySIMDistinct = _stringSIM.Distinct();
                                string[] _stringArraySIMList = _stringArraySIMDistinct.ToArray();
                                if (_stringArraySIMList.Length == 1)
                                {
                                    for (int i = 0; i < _stringArraySIMList.Length; i++)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _stringArraySIMList[i] + "'";
                                        }
                                    }
                                }
                                else if (_stringArraySIMList.Length > 1)
                                {
                                    for (int i = 0; i < _stringArraySIMList.Length; i++)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _stringArraySIMList[i] + "'";
                                        }
                                        else
                                        {
                                            _endString = _endString + " OR [Code]=='" + _stringArraySIMList[i] + "'";
                                        }
                                    }
                                }
                                _fullString = _beginString + _endString;

                                if (_fullString != null)
                                {
                                    _availableSalesInvoice = new XPCollection<SalesInvoice>(Session, CriteriaOperator.Parse(_fullString));
                                }
                            }
                        }
                    }
                }
                return _availableSalesInvoice;
            }
        }

        [DataSourceProperty("AvailableSalesInvoice", DataSourcePropertyIsNullMode.SelectNothing)]
        public SalesInvoice SalesInvoice
        {
            get { return _salesInvoice; }
            set { SetPropertyValue("SalesInvoice", ref _salesInvoice, value); }
        }

        [Browsable(false)]
        public XPCollection<SalesPrePaymentInvoice> AvailableSalesPrePaymentInvoice
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;

                if (!IsLoading)
                {
                    if (this.ReceivableTransaction != null )
                    {
                        if (this.ReceivableTransaction.SalesToCustomer != null && this.ReceivableTransaction.PriceGroup != null && this.ReceivableTransaction.BillToCustomer != null)
                        {
                            XPQuery<SalesPrePaymentInvoice> _salesPrePaymentInvoiceQuery = new XPQuery<SalesPrePaymentInvoice>(Session);

                            var _salesPrePaymentInvoices = from sim in _salesPrePaymentInvoiceQuery
                                                           where (sim.Status != Status.Close
                                                           && sim.PriceGroup == this.ReceivableTransaction.PriceGroup
                                                           && sim.SalesToCustomer == this.ReceivableTransaction.SalesToCustomer
                                                           && sim.BillToCustomer == this.ReceivableTransaction.BillToCustomer
                                                           && sim.ReceivableTransaction == null
                                                           )
                                                           group sim by sim.Code into g
                                                           select new { Code = g.Key };

                            if (_salesPrePaymentInvoices != null && _salesPrePaymentInvoices.Count() > 0)
                            {
                                List<string> _stringSPI = new List<string>();

                                foreach (var _salesPrePaymentInvoice in _salesPrePaymentInvoices)
                                {
                                    if (_salesPrePaymentInvoice != null)
                                    {
                                        _stringSPI.Add(_salesPrePaymentInvoice.Code);
                                    }
                                }

                                IEnumerable<string> _stringArraySPIDistinct = _stringSPI.Distinct();
                                string[] _stringArraySPIList = _stringArraySPIDistinct.ToArray();
                                if (_stringArraySPIList.Length == 1)
                                {
                                    for (int i = 0; i < _stringArraySPIList.Length; i++)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _stringArraySPIList[i] + "'";
                                        }
                                    }
                                }
                                else if (_stringArraySPIList.Length > 1)
                                {
                                    for (int i = 0; i < _stringArraySPIList.Length; i++)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _stringArraySPIList[i] + "'";
                                        }
                                        else
                                        {
                                            _endString = _endString + " OR [Code]=='" + _stringArraySPIList[i] + "'";
                                        }
                                    }
                                }
                                _fullString = _beginString + _endString;

                                if (_fullString != null)
                                {
                                    _availableSalesPrePaymentInvoice = new XPCollection<SalesPrePaymentInvoice>(Session, CriteriaOperator.Parse(_fullString));
                                }
                            }
                        }
                    }
                }
                return _availableSalesPrePaymentInvoice;
            }
        }

        [DataSourceProperty("AvailableSalesPrePaymentInvoice", DataSourcePropertyIsNullMode.SelectNothing)]
        public SalesPrePaymentInvoice SalesPrePaymentInvoice
        {
            get { return _salesPrePaymentInvoice; }
            set { SetPropertyValue("SalesPrePaymentInvoice", ref _salesPrePaymentInvoice, value); }
        }

        [ImmediatePostData()]
        [Appearance("ReceivableSalesCollectionReceivableTransactionClose", Enabled = false)]
        [Association("ReceivableTransaction-ReceivableSalesCollections")]
        public ReceivableTransaction ReceivableTransaction
        {
            get { return _receivableTransaction; }
            set { SetPropertyValue("ReceivableTransaction", ref _receivableTransaction, value); }
        }

        [Appearance("ReceivableSalesCollectionStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("ReceivableSalesCollectionStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("ReceivableSalesCollectionPostedCountEnabled", Enabled = false)]
        public int PostedCount
        {
            get { return _postedCount; }
            set { SetPropertyValue("PostedCount", ref _postedCount, value); }
        }
        #endregion Field
    }
}