﻿#region Default

using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

#endregion Default

using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    #region Default

    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("DebitMemoRuleUnique", DefaultContexts.Save, "Code")]

    #endregion Default

    // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
    public class DebitMemo : FullDriveSysBaseObject
    {
        #region Default

        private bool _activationPosting;
        private string _code;
        private BusinessPartner _salesToCustomer;
        private string _salesToContact;
        private Country _salesToCountry;
        private City _salesToCity;
        private string _salesToAddress;
        private TermOfPayment _top;
        private Status _status;
        private DateTime _statusDate;
        private string _signCode;
        private SalesReturn _salesReturn;
        private SalesOrder _salesOrder;
        private InventoryTransferIn _inventoryTransferIn;
        private Company _company;
        private GlobalFunction _globFunc;

        private bool _activeApproved1;
        private bool _activeApproved2;
        private bool _activeApproved3;

        public DebitMemo(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.DebitMemo);
                this.Status = Status.Open;
                this.StatusDate = now;
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #endregion Default

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("DebitMemoCodeEnabled", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("DebitMemoSalesToCustomerClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BusinessPartner SalesToCustomer
        {
            get { return _salesToCustomer; }
            set {
                SetPropertyValue("SalesToCustomer", ref _salesToCustomer, value);
                if (!IsLoading)
                {
                    if (this._salesToCustomer != null)
                    {
                        this.SalesToContact = this._salesToCustomer.Contact;
                        this.SalesToCountry = this._salesToCustomer.Country;
                        this.SalesToCity = this._salesToCustomer.City;
                        this.SalesToAddress = this._salesToCustomer.Address;

                    }
                }
            }
        }

        [Appearance("DebitMemoSalesToContactClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string SalesToContact
        {
            get { return _salesToContact; }
            set { SetPropertyValue("SalesToContact", ref _salesToContact, value); }
        }

        [Appearance("DebitMemoSalesToCountryClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Country SalesToCountry
        {
            get { return _salesToCountry; }
            set { SetPropertyValue("SalesToCountry", ref _salesToCountry, value); }
        }

        [Appearance("DebitMemoSalesToCityClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public City SalesToCity
        {
            get { return _salesToCity; }
            set { SetPropertyValue("SalesToCity", ref _salesToCity, value); }
        }

        [Size(512)]
        [Appearance("DebitMemoSalesToAddressClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string SalesToAddress
        {
            get { return _salesToAddress; }
            set { SetPropertyValue("SalesToAddress", ref _salesToAddress, value); }
        }

        [Appearance("DebitMemoTOPClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public TermOfPayment TOP
        {
            get { return _top; }
            set { SetPropertyValue("TOP", ref _top, value); }
        }

        [Appearance("DebitMemoStatusEnabled", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("DebitMemoStatusDateEnabled", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("DebitMemoSignCodeEnabled", Enabled = false)]
        public string SignCode
        {
            get { return _signCode; }
            set { SetPropertyValue("SignCode", ref _signCode, value); }
        }

        [Appearance("DebitMemoSalesReturnClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public SalesReturn SalesReturn
        {
            get { return _salesReturn; }
            set
            {
                SetPropertyValue("SalesReturn", ref _salesReturn, value);
                if (!IsLoading)
                {
                    if (this._salesReturn != null)
                    {
                        if (this._salesReturn.SalesOrder != null)
                        {
                            this.SalesOrder = _salesReturn.SalesOrder;
                        }
                    }
                }
            }
        }

        [Appearance("DebitMemoSalesOrderClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public SalesOrder SalesOrder
        {
            get { return _salesOrder; }
            set { SetPropertyValue("SalesOrder", ref _salesOrder, value); }
        }

        [Appearance("DebitMemoInventoryTransferClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public InventoryTransferIn InventoryTransferIn
        {
            get { return _inventoryTransferIn; }
            set { SetPropertyValue("InventoryTransferIn", ref _inventoryTransferIn, value); }
        }

        [Browsable(false)]
        [DataSourceCriteria("Active = true")]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Association("DebitMemo-DebitMemoLines")]
        public XPCollection<DebitMemoLine> DebitMemoLines
        {
            get { return GetCollection<DebitMemoLine>("DebitMemoLines"); }
        }

        [Association("DebitMemo-ApprovalLines")]
        public XPCollection<ApprovalLine> ApprovalLines
        {
            get { return GetCollection<ApprovalLine>("ApprovalLines"); }
        }

        [Association("DebitMemo-DebitSalesCollections")]
        public XPCollection<DebitSalesCollection> DebitSalesCollections
        {
            get { return GetCollection<DebitSalesCollection>("DebitSalesCollections"); }
        }

        #region CLM

        //CLM
        [Browsable(false)]
        [Appearance("SalesReturnActiveApproved1Close", Enabled = false)]
        public bool ActiveApproved1
        {
            get { return _activeApproved1; }
            set { SetPropertyValue("ActiveApproved1", ref _activeApproved1, value); }
        }

        [Browsable(false)]
        [Appearance("SalesReturnActiveApproved2Close", Enabled = false)]
        public bool ActiveApproved2
        {
            get { return _activeApproved2; }
            set { SetPropertyValue("ActiveApproved2", ref _activeApproved2, value); }
        }

        [Browsable(false)]
        [Appearance("SalesReturnActiveApproved3Close", Enabled = false)]
        public bool ActiveApproved3
        {
            get { return _activeApproved3; }
            set { SetPropertyValue("ActiveApproved3", ref _activeApproved3, value); }
        }

        #endregion CLM

        #endregion Field

        

       

    }
}