﻿        #region Default
using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using DevExpress.ExpressApp.Xpo;
using DevExpress.ExpressApp.ConditionalAppearance;
using System.Collections;
using FullDrive.Module.CustomProcess;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    [NavigationItem("Tax")]
    [RuleCombinationOfPropertiesIsUnique("TaxRateNPWPRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class TaxRateNPWP : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private string _code;
        private string _name;
        private double _rate;
        private double _sequenceNo;
        private double _startRange;
        private double _endRange;
        private double _taxValue;
        private GlobalFunction _globFunc;
        private TaxSetup _taxSetup;
        public TaxRateNPWP(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                this._globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.TaxRateNPWP);
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }
        #endregion

        //protected override void OnSaving()
        //{
        //    base.OnSaving();
        //    if (!IsLoading && IsSaving)
        //    {
        //        _sequenceNo += 1;
        //    }
        //}

        #region Field
        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        public double SequenceNo
        {
            get { return _sequenceNo; }
            set { SetPropertyValue("SequenceNo", ref _sequenceNo, value); }
        }

        public double StartRange
        {
            get { return _startRange; }
            set { SetPropertyValue("StartRange", ref _startRange, value); }
        }

        public double EndRange
        {
            get { return _endRange; }
            set { SetPropertyValue("EndRange", ref _endRange, value); }
        }

        [ImmediatePostData()]
        public double Rate
        {
            get { return _rate; }
            set { SetPropertyValue("Rate", ref _rate, value); }
        }

        [ImmediatePostData()]
        public double TaxValue
        {
            get
            {
                if (!IsLoading)
                {
                    if (Rate > 0)
                    {
                        return _taxValue = Rate / 100 * (StartRange - EndRange);
                    }
                }
                return 0;
            }
            set { SetPropertyValue("TaxValue", ref _taxValue, value); }
            // Rate / 100 * (EndRange-StartRange)
        }

        [Association("TaxSetup-TaxRateNPWPs")]
        public TaxSetup TaxSetup
        {
            get { return _taxSetup; }
            set { SetPropertyValue("TaxSetup", ref _taxSetup, value); }
        }
        #endregion

        //private string _PersistentProperty;
        //[XafDisplayName("My display name"), ToolTip("My hint message")]
        //[ModelDefault("EditMask", "(000)-00"), Index(0), VisibleInListView(false)]
        //[Persistent("DatabaseColumnName"), RuleRequiredField(DefaultContexts.Save)]
        //public string PersistentProperty {
        //    get { return _PersistentProperty; }
        //    set { SetPropertyValue("PersistentProperty", ref _PersistentProperty, value); }
        //}

        //[Action(Caption = "My UI Action", ConfirmationMessage = "Are you sure?", ImageName = "Attention", AutoCommit = true)]
        //public void ActionMethod() {
        //    // Trigger a custom business logic for the current record in the UI (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112619.aspx).
        //    this.PersistentProperty = "Paid";
        //}
    }
}