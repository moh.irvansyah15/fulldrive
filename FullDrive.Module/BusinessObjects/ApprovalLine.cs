﻿#region Default

using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

#endregion Default

using DevExpress.ExpressApp.Editors;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    #region Default

    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Master")]
    [RuleCombinationOfPropertiesIsUnique("ApprovalLineRuleUnique", DefaultContexts.Save, "Code")]

    #endregion Default

    // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
    public class ApprovalLine : FullDriveSysBaseObject
    {
        #region Default

        private string _code;
        private ApprovalLevel _approvalLevel;
        private Status _approvalStatus;
        private DateTime _approvalDate;
        private bool _endApproval;
        private Employee _approvedBy;
        private string _userAccess;
        private PurchaseRequisition _purchaseRequisition;
        private PurchaseOrder _purchaseOrder;
        private Production _production;
        private SalesQuotation _salesQuotation;
        private SalesOrder _salesOrder;
        private TransferOrder _transferOrder;
        private FinishProduction _finishProduction;
        private ItemConsumption _itemConsumption;
        private InventoryTransferOrder _inventoryTransferOrder;
        private WorkRequisition _workRequisition;
        private WorkOrder _workOrder;
        private PaymentRealization _paymentRealization;
        private MaterialRequisition _materialRequisition;
        private CashAdvance _cashAdvance;
        private PurchaseInvoice _purchaseInvoice;
        private PurchasePrePaymentInvoice _purchasePrePaymentInvoice;
        private PurchaseReturn _purchaseReturn;
        private CreditMemo _creditMemo;
        private SalesInvoice _salesInvoice;
        private SalesPrePaymentInvoice _salesPrePaymentInvoice;
        private SalesReturn _salesReturn;
        private DebitMemo _debitMemo;
        private GlobalFunction _globFunc;

        public ApprovalLine(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.ApprovalLine);
            }

            #region UserAccess

            _userAccess = SecuritySystem.CurrentUserName;
            UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));

            if (Session.IsNewObject(this))
            {
                if (_locUserAccess != null)
                {
                    if (_locUserAccess.Employee != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            ApprovedBy = _locUserAccess.Employee;
                        }
                    }
                }
            }

            #endregion UserAccess
        }

        #endregion Default

        #region Field

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("ApprovalLineCodeEnabled", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("ApprovalLineApprovalLevelEnabled", Enabled = false)]
        public ApprovalLevel ApprovalLevel
        {
            get { return _approvalLevel; }
            set { SetPropertyValue("ApprovalLevel", ref _approvalLevel, value); }
        }

        [Appearance("ApprovalLineApprovalStatusEnabled", Enabled = false)]
        public Status ApprovalStatus
        {
            get { return _approvalStatus; }
            set { SetPropertyValue("ApprovalStatus", ref _approvalStatus, value); }
        }

        [Appearance("ApprovalLineApprovalDateEnabled", Enabled = false)]
        public DateTime ApprovalDate
        {
            get { return _approvalDate; }
            set { SetPropertyValue("ApprovalDate", ref _approvalDate, value); }
        }

        [Appearance("ApprovalLineEndApprovalEnabled", Enabled = false)]
        public bool EndApproval
        {
            get { return _endApproval; }
            set { SetPropertyValue("EndApproval", ref _endApproval, value); }
        }

        [Appearance("ApprovalLineApprovedByEnabled", Enabled = false)]
        public Employee ApprovedBy
        {
            get { return _approvedBy; }
            set { SetPropertyValue("ApprovedBy", ref _approvedBy, value); }
        }

        [Appearance("ApprovalLinePurchaseRequisitionEnabled", Enabled = false)]
        [Association("PurchaseRequisition-ApprovalLines")]
        public PurchaseRequisition PurchaseRequisition
        {
            get { return _purchaseRequisition; }
            set { SetPropertyValue("PurchaseRequisition", ref _purchaseRequisition, value); }
        }

        [Appearance("ApprovalLinePurchaseOrderEnabled", Enabled = false)]
        [Association("PurchaseOrder-ApprovalLines")]
        public PurchaseOrder PurchaseOrder
        {
            get { return _purchaseOrder; }
            set { SetPropertyValue("PurchaseOrder", ref _purchaseOrder, value); }
        }

        [Appearance("ApprovalLineProductionEnabled", Enabled = false)]
        [Association("Production-ApprovalLines")]
        public Production Production
        {
            get { return _production; }
            set { SetPropertyValue("Production", ref _production, value); }
        }

        [Appearance("ApprovalLineSalesQuotationEnabled", Enabled = false)]
        [Association("SalesQuotation-ApprovalLines")]
        public SalesQuotation SalesQuotation
        {
            get { return _salesQuotation; }
            set { SetPropertyValue("SalesQuotation", ref _salesQuotation, value); }
        }

        [Appearance("ApprovalLineSalesOrderEnabled", Enabled = false)]
        [Association("SalesOrder-ApprovalLines")]
        public SalesOrder SalesOrder
        {
            get { return _salesOrder; }
            set { SetPropertyValue("SalesOrder", ref _salesOrder, value); }
        }

        [Appearance("ApprovalLineTransferOrderEnabled", Enabled = false)]
        [Association("TransferOrder-ApprovalLines")]
        public TransferOrder TransferOrder
        {
            get { return _transferOrder; }
            set { SetPropertyValue("TransferOrder", ref _transferOrder, value); }
        }

        [Appearance("ApprovalLineFinishProductionEnabled", Enabled = false)]
        [Association("FinishProduction-ApprovalLines")]
        public FinishProduction FinishProduction
        {
            get { return _finishProduction; }
            set { SetPropertyValue("FinishProduction", ref _finishProduction, value); }
        }

        [Appearance("ApprovalLineItemConsumptionEnabled", Enabled = false)]
        [Association("ItemConsumption-ApprovalLines")]
        public ItemConsumption ItemConsumption
        {
            get { return _itemConsumption; }
            set { SetPropertyValue("ItemConsumption", ref _itemConsumption, value); }
        }

        [Appearance("ApprovalLineInventoryTransferOrderEnabled", Enabled = false)]
        [Association("InventoryTransferOrder-ApprovalLines")]
        public InventoryTransferOrder InventoryTransferOrder
        {
            get { return _inventoryTransferOrder; }
            set { SetPropertyValue("InventoryTransferOrder", ref _inventoryTransferOrder, value); }
        }

        [Appearance("ApprovalLineWorkRequisitionEnabled", Enabled = false)]
        [Association("WorkRequisition-ApprovalLines")]
        public WorkRequisition WorkRequisition
        {
            get { return _workRequisition; }
            set { SetPropertyValue("WorkRequisition", ref _workRequisition, value); }
        }

        [Appearance("ApprovalLineWorkOrderEnabled", Enabled = false)]
        [Association("WorkOrder-ApprovalLines")]
        public WorkOrder WorkOrder
        {
            get { return _workOrder; }
            set { SetPropertyValue("WorkOrder", ref _workOrder, value); }
        }

        [Appearance("ApprovalLinePaymentRealizationEnabled", Enabled = false)]
        [Association("PaymentRealization-ApprovalLines")]
        public PaymentRealization PaymentRealization
        {
            get { return _paymentRealization; }
            set { SetPropertyValue("PaymentRealization", ref _paymentRealization, value); }
        }

        [Appearance("ApprovalLineMaterialRequisitionEnabled", Enabled = false)]
        [Association("MaterialRequisition-ApprovalLines")]
        public MaterialRequisition MaterialRequisition
        {
            get { return _materialRequisition; }
            set { SetPropertyValue("MaterialRequisition", ref _materialRequisition, value); }
        }

        [Appearance("ApprovalLinesCashAdvanceEnabled", Enabled = false)]
        [Association("CashAdvance-ApprovalLines")]
        public CashAdvance CashAdvance
        {
            get { return _cashAdvance; }
            set { SetPropertyValue("CashAdvance", ref _cashAdvance, value); }
        }

        [Appearance("ApprovalLinesPurchaseInvoiceEnabled", Enabled = false)]
        [Association("PurchaseInvoice-ApprovalLines")]
        public PurchaseInvoice PurchaseInvoice
        {
            get { return _purchaseInvoice; }
            set { SetPropertyValue("PurchaseInvoice", ref _purchaseInvoice, value); }
        }

        [Appearance("ApprovalLinesPurchasePrePaymentInvoiceEnabled", Enabled = false)]
        [Association("PurchasePrePaymentInvoice-ApprovalLines")]
        public PurchasePrePaymentInvoice PurchasePrePaymentInvoice
        {
            get { return _purchasePrePaymentInvoice; }
            set { SetPropertyValue("PurchasePrePaymentInvoice", ref _purchasePrePaymentInvoice, value); }
        }

        [Appearance("ApprovalLinesPurchaseReturnEnabled", Enabled = false)]
        [Association("PurchaseReturn-ApprovalLines")]
        public PurchaseReturn PurchaseReturn
        {
            get { return _purchaseReturn; }
            set { SetPropertyValue("PurchaseReturn", ref _purchaseReturn, value); }
        }

        [Appearance("ApprovalLinesCreditMemoEnabled", Enabled = false)]
        [Association("CreditMemo-ApprovalLines")]
        public CreditMemo CreditMemo
        {
            get { return _creditMemo; }
            set { SetPropertyValue("CreditMemo", ref _creditMemo, value); }
        }

        [Appearance("ApprovalLinesSalesInvoiceEnabled", Enabled = false)]
        [Association("SalesInvoice-ApprovalLines")]
        public SalesInvoice SalesInvoice
        {
            get { return _salesInvoice; }
            set { SetPropertyValue("SalesInvoice", ref _salesInvoice, value); }
        }

        [Appearance("ApprovalLinesSalesPrePaymentInvoiceEnabled", Enabled = false)]
        [Association("SalesPrePaymentInvoice-ApprovalLines")]
        public SalesPrePaymentInvoice SalesPrePaymentInvoice
        {
            get { return _salesPrePaymentInvoice; }
            set { SetPropertyValue("SalesPrePaymentInvoice", ref _salesPrePaymentInvoice, value); }
        }

        [Appearance("ApprovalLinesSalesReturnEnabled", Enabled = false)]
        [Association("SalesReturn-ApprovalLines")]
        public SalesReturn SalesReturn
        {
            get { return _salesReturn; }
            set { SetPropertyValue("SalesReturn", ref _salesReturn, value); }
        }

        [Appearance("ApprovalLinesDebitMemoEnabled", Enabled = false)]
        [Association("DebitMemo-ApprovalLines")]
        public DebitMemo DebitMemo
        {
            get { return _debitMemo; }
            set { SetPropertyValue("DebitMemo", ref _debitMemo, value); }
        }
        #endregion Field
    }
}