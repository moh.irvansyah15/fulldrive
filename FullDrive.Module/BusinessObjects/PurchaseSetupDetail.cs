﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Setup")]
    [RuleCombinationOfPropertiesIsUnique("PurchaseSetupDetailRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class PurchaseSetupDetail : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private string _code;
        private string _name;
        private UserAccess _userAccess;
        private ObjectList _objectList;
        private bool _businessPartnerActive;
        private BusinessPartner _businessPartner;
        private bool _splitInvoice;
        private bool _allSplitInvoice;
        private bool _activationQuantity;
        private bool _active;
        private ApplicationSetup _applicationSetup;
        private GlobalFunction _globFunc;

        public PurchaseSetupDetail(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.PurchaseSetupDetail);
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        protected override void OnSaving()
        {
            base.OnSaving();
            if (this.AllSplitInvoice == true)
            {
                CheckAllSplitInvoiceSystem();
            }
            if (this.SplitInvoice == true)
            {
                CheckSplitInvoiceSystem();
            }
        }

        #region Field

        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        public UserAccess UserAccess
        {
            get { return _userAccess; }
            set { SetPropertyValue("UserAccess", ref _userAccess, value); }
        }

        public ObjectList ObjectList
        {
            get { return _objectList; }
            set { SetPropertyValue("ObjectList", ref _objectList, value); }
        }

        [ImmediatePostData()]
        [Appearance("PurchaseSetupDetailBusinessPartnerActiveClose", Criteria = "AllSplitInvoice = true", Enabled = false)]
        public bool BusinessPartnerActive
        {
            get { return _businessPartnerActive; }
            set { SetPropertyValue("BusinessPartnerActive", ref _businessPartnerActive, value); }
        }

        [Appearance("PurchaseSetupDetailBusinessPartnerClose", Criteria = "BusinessPartnerActive = false", Enabled = false)]
        public BusinessPartner BusinessPartner
        {
            get { return _businessPartner; }
            set { SetPropertyValue("BusinessPartner", ref _businessPartner, value); }
        }
        
        [Appearance("PurchaseSetupDetailSplitInvoiceClose", Criteria = "AllSplitInvoice = true", Enabled = false)]
        public bool SplitInvoice
        {
            get { return _splitInvoice; }
            set { SetPropertyValue("SplitInvoice", ref _splitInvoice, value); }
        }

        [ImmediatePostData()]
        public bool AllSplitInvoice
        {
            get { return _allSplitInvoice; }
            set
            {
                SetPropertyValue("AllSplitInvoice", ref _allSplitInvoice, value);
                if (!IsLoading)
                {
                    if (this._allSplitInvoice == true)
                    {
                        this.SplitInvoice = false;
                        this.BusinessPartnerActive = false;
                        this.BusinessPartner = null;
                    }
                }
            }
        }

        
        public bool ActivationQuantity
        {
            get { return _activationQuantity; }
            set { SetPropertyValue("ActivationQuantity", ref _activationQuantity, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue("Active", ref _active, value); }
        }

        [Appearance("ApplicationSetupApplicationSetupDetailEnabled", Enabled = false)]
        [RuleRequiredField(DefaultContexts.Save)]
        [Association("ApplicationSetup-PurchaseSetupDetails")]
        public ApplicationSetup ApplicationSetup
        {
            get { return _applicationSetup; }
            set { SetPropertyValue("ApplicationSetup", ref _applicationSetup, value); }
        }

        #endregion Field

        //=============================================== Code In Here ===============================================

        private void CheckAllSplitInvoiceSystem()
        {
            try
            {
                XPCollection<PurchaseSetupDetail> _appSetups = new XPCollection<PurchaseSetupDetail>(Session, new GroupOperator(GroupOperatorType.And,
                                                               new BinaryOperator("ApplicationSetup", this.ApplicationSetup),
                                                               new BinaryOperator("This", this, BinaryOperatorType.NotEqual)));
                if (_appSetups == null)
                {
                    return;
                }
                else
                {
                    foreach (PurchaseSetupDetail _appSetup in _appSetups)
                    {
                        _appSetup.AllSplitInvoice = false;
                        _appSetup.Active = false;
                        _appSetup.BusinessPartnerActive = false;
                        _appSetup.BusinessPartner = null;
                        _appSetup.SplitInvoice = false;
                        _appSetup.Save();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business Object = PurchaseSetupDetail", ex.ToString());
            }
        }

        private void CheckSplitInvoiceSystem()
        {
            XPCollection<PurchaseSetupDetail> _appSetups = new XPCollection<PurchaseSetupDetail>(Session, new GroupOperator(GroupOperatorType.And,
                                                               new BinaryOperator("ApplicationSetup", this.ApplicationSetup),
                                                               new BinaryOperator("AllSplitGoodsReceive", true)));
            if (_appSetups == null)
            {
                return;
            }
            else
            {
                this.Active = false;
                this.BusinessPartnerActive = false;
                this.BusinessPartner = null;
                this.SplitInvoice = false;
                this.Save();
            }
        }

    }
}