﻿#region Default

using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

#endregion Default

using DevExpress.ExpressApp.ConditionalAppearance;
using FullDrive.Module.CustomProcess;

namespace FullDrive.Module.BusinessObjects
{
    #region Default

    [DefaultClassOptions]
    [DefaultProperty("Name")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("JournalMapRuleUnique", DefaultContexts.Save, "Code")]

    #endregion Default

    // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
    public class JournalMap : FullDriveSysBaseObject
    {
        #region Default

        private string _code;
        private string _name;
        private ItemAccountGroup _itemAccountGroup;
        private BusinessPartnerAccountGroup _businessPartnerAccountGroup;
        private TaxAccountGroup _taxAccountGroup;
        private DiscountAccountGroup _discountAccountGroup;
        private CompanyAccountGroup _companyAccountGroup;
        private BankAccountGroup _bankAccountGroup;
        private OrganizationAccountGroup _organizationAccountGroup;
        private AdvanceAccountGroup _advanceAccountGroup;
        private bool _default;
        private bool _active;
        private GlobalFunction _globFunc;

        public JournalMap(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.JournalMap);
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #endregion Default

        #region Field

        [Appearance("JournalMapCodeClose", Enabled = false)]
        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        [Association("ItemAccountGroup-JournalMaps")]
        public ItemAccountGroup ItemAccountGroup
        {
            get { return _itemAccountGroup; }
            set { SetPropertyValue("ItemAccountGroup", ref _itemAccountGroup, value); }
        }

        [Association("BusinessPartnerAccountGroup-JournalMaps")]
        public BusinessPartnerAccountGroup BusinessPartnerAccountGroup
        {
            get { return _businessPartnerAccountGroup; }
            set { SetPropertyValue("BusinessPartnerAccountGroup", ref _businessPartnerAccountGroup, value); }
        }

        [Association("TaxAccountGroup-JournalMaps")]
        public TaxAccountGroup TaxAccountGroup
        {
            get { return _taxAccountGroup; }
            set { SetPropertyValue("TaxAccountGroup", ref _taxAccountGroup, value); }
        }

        [Association("DiscountAccountGroup-JournalMaps")]
        public DiscountAccountGroup DiscountAccountGroup
        {
            get { return _discountAccountGroup; }
            set { SetPropertyValue("DiscountAccountGroup", ref _discountAccountGroup, value); }
        }

        [Association("CompanyAccountGroup-JournalMaps")]
        public CompanyAccountGroup CompanyAccountGroup
        {
            get { return _companyAccountGroup; }
            set { SetPropertyValue("CompanyAccountGroup", ref _companyAccountGroup, value); }
        }

        [Association("BankAccountGroup-JournalMaps")]
        public BankAccountGroup BankAccountGroup
        {
            get { return _bankAccountGroup; }
            set { SetPropertyValue("BankAccountGroup", ref _bankAccountGroup, value); }
        }

        [Association("OrganizationAccountGroup-JournalMaps")]
        public OrganizationAccountGroup OrganizationAccountGroup
        {
            get { return _organizationAccountGroup; }
            set { SetPropertyValue("OrganizationAccountGroup", ref _organizationAccountGroup, value); }
        }

        [Association("AdvanceAccountGroup-JournalMaps")]
        public AdvanceAccountGroup AdvanceAccountGroup
        {
            get { return _advanceAccountGroup; }
            set { SetPropertyValue("AdvanceAccountGroup", ref _advanceAccountGroup, value); }
        }

        public bool Default
        {
            get { return _default; }
            set { SetPropertyValue("Default", ref _default, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue("Active", ref _active, value); }
        }

        [Association("JournalMap-JournalMapLines")]
        public XPCollection<JournalMapLine> JournalMapLines
        {
            get { return GetCollection<JournalMapLine>("JournalMapLines"); }
        }

        #endregion Field

    }
}