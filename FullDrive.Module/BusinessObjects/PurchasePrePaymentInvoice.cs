﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.ExpressApp.Editors;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("PurchasePrePaymentInvoiceRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class PurchasePrePaymentInvoice : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private bool _activationPosting;
        private string _code;
        private bool _select;
        #region Purchase
        private BusinessPartner _buyFromVendor;
        private string _buyFromContact;
        private Country _buyFromCountry;
        private City _buyFromCity;
        private string _buyFromAddress;
        #endregion Purchase
        #region Payment
        private BusinessPartner _payToVendor;
        private string _payToContact;
        private Country _payToCountry;
        private City _payToCity;
        private string _payToAddress;
        #endregion Payment
        private PaymentMethod _paymentMethod;
        private PaymentMethodType _paymentMethodType;
        private PaymentType _paymentType;
        private TermOfPayment _top;
        private Currency _currency;
        #region Bank
        private XPCollection<BankAccount> _availableBankAccountCompany;
        private BankAccount _bankAccountCompany;
        private string _companyAccountNo;
        private string _companyAccountName;
        private XPCollection<BankAccount> _availableBankAccounts;
        private BankAccount _bankAccount;
        private string _accountNo;
        private string _accountName;
        #endregion Bank
        private string _taxNo;
        private double _maxPay;
        private double _pay;
        private double _dp_percentage;
        private double _dp_Amount;
        private double _plan;
        private double _outstanding;
        private DateTime _estimatedDate;
        private string _description;
        private int _postedCount;
        private Status _status;
        private DateTime _statusDate;
        private string _signCode;
        private XPCollection<PurchaseOrder> _availablePurchaseOrder;
        private PurchaseOrder _purchaseOrder;
        private PayableTransaction _payableTransaction;
        private ProjectHeader _projectHeader;
        private Company _company;
        private string _userAccess;
        private GlobalFunction _globFunc;

        private bool _activeApproved1;
        private bool _activeApproved2;
        private bool _activeApproved3;

        public PurchasePrePaymentInvoice(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.PurchasePrePaymentInvoice);
                DateTime now = DateTime.Now;
                this.Status = CustomProcess.Status.Open;
                this.StatusDate = now;
                this.Select = true;

                _userAccess = SecuritySystem.CurrentUserName;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                if (Session.IsNewObject(this))
                {
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null)
                            {
                                Company = _locUserAccess.Employee.Company;
                            }
                        }
                    }
                }

                this.Currency = _globFunc.GetDefaultCurrency(this.Session);
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [ImmediatePostData()]
        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [Appearance("PurchasePrePaymentInvoiceRedColor", Criteria = "ActiveApproved1 = true", BackColor = "#fd79a8")]
        [Appearance("PurchasePrePaymentInvoiceYellowColor", Criteria = "ActiveApproved2 = true", BackColor = "#ffeaa7")]
        [Appearance("PurchasePrePaymentInvoiceGreenOColor", Criteria = "ActiveApproved3 = true", BackColor = "#81ecec")]
        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("PurchasePrePaymentInvoiceCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("PurchasePrePaymentInvoiceSelectClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public bool Select
        {
            get { return _select; }
            set { SetPropertyValue("Select", ref _select, value); }
        }

        #region BuyFromVendor

        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        [Appearance("PurchasePrePaymentInvoiceBuyFromVendorClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BusinessPartner BuyFromVendor
        {
            get { return _buyFromVendor; }
            set
            {
                SetPropertyValue("BuyFromVendor", ref _buyFromVendor, value);
                if (!IsLoading)
                {
                    if (_buyFromVendor != null)
                    {
                        this.BuyFromContact = _buyFromVendor.Contact;
                        this.BuyFromCountry = _buyFromVendor.Country;
                        this.BuyFromCity = _buyFromVendor.City;
                        this.BuyFromAddress = this.BuyFromAddress;
                        this.PayToVendor = _buyFromVendor;
                    }
                }
            }
        }

        [Appearance("PurchasePrePaymentInvoiceBuyFromContactClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string BuyFromContact
        {
            get { return _buyFromContact; }
            set { SetPropertyValue("BuyFromContact", ref _buyFromContact, value); }
        }

        [DataSourceCriteria("Active = true")]
        [Appearance("PurchasePrePaymentInvoiceBuyFromCountryClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Country BuyFromCountry
        {
            get { return _buyFromCountry; }
            set { SetPropertyValue("BuyFromCountry", ref _buyFromCountry, value); }
        }

        [DataSourceCriteria("Active = true")]
        [Appearance("PurchasePrePaymentInvoiceBuyFromCityClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public City BuyFromCity
        {
            get { return _buyFromCity; }
            set { SetPropertyValue("BuyFromCity", ref _buyFromCity, value); }
        }

        [Appearance("PurchasePrePaymentInvoiceBuyFromAddressClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Size(512)]
        public string BuyFromAddress
        {
            get { return _buyFromAddress; }
            set { SetPropertyValue("BuyFromAddress", ref _buyFromAddress, value); }
        }

        #endregion BuyFromVendor

        #region PayToVendor

        [DataSourceCriteria("Active = true")]
        [Appearance("PurchasePrePaymentInvoicePayToVendorClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BusinessPartner PayToVendor
        {
            get { return _payToVendor; }
            set
            {
                SetPropertyValue("PayToVendor", ref _payToVendor, value);
                if (!IsLoading)
                {
                    if (this._payToVendor != null)
                    {
                        this.PayToContact = this._payToVendor.Contact;
                        this.PayToAddress = this._payToVendor.Address;
                        this.PayToCountry = this._payToVendor.Country;
                        this.PayToCity = this._payToVendor.City;
                    }
                }
            }
        }

        [Appearance("PurchasePrePaymentInvoicePayToContactClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string PayToContact
        {
            get { return _payToContact; }
            set { SetPropertyValue("PayToContact", ref _payToContact, value); }
        }

        [DataSourceCriteria("Active = true")]
        [Appearance("PurchasePrePaymentInvoicePayToCountryClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Country PayToCountry
        {
            get { return _payToCountry; }
            set { SetPropertyValue("PayToCountry", ref _payToCountry, value); }
        }

        [DataSourceCriteria("Active = true")]
        [Appearance("PurchasePrePaymentInvoicePayToCityClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public City PayToCity
        {
            get { return _payToCity; }
            set { SetPropertyValue("PayToCity", ref _payToCity, value); }
        }

        [Appearance("PurchasePrePaymentInvoicePayToAddressClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Size(512)]
        public string PayToAddress
        {
            get { return _payToAddress; }
            set { SetPropertyValue("PayToAddress", ref _payToAddress, value); }
        }

        #endregion PayToVendor

        [Appearance("PurchasePrePaymentInvoicePaymentMethodClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PaymentMethod PaymentMethod
        {
            get { return _paymentMethod; }
            set { SetPropertyValue("PaymentMethod", ref _paymentMethod, value); }
        }

        [Appearance("PurchasePrePaymentInvoicePaymentMethodTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PaymentMethodType PaymentMethodType
        {
            get { return _paymentMethodType; }
            set { SetPropertyValue("PaymentMethodType", ref _paymentMethodType, value); }
        }

        [Appearance("PurchasePrePaymentInvoicePaymentTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PaymentType PaymentType
        {
            get { return _paymentType; }
            set { SetPropertyValue("PaymentType", ref _paymentType, value); }
        }

        [Appearance("PurchasePrePaymentInvoiceTermOfPaymentClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public TermOfPayment TOP
        {
            get { return _top; }
            set { SetPropertyValue("TOP", ref _top, value); }
        }

        [Appearance("PurchasePrePaymentInvoiceCurrencyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Currency Currency
        {
            get { return _currency; }
            set { SetPropertyValue("Currency", ref _currency, value); }
        }

        #region Bank

        [Browsable(false)]
        public XPCollection<BankAccount> AvailableBankAccountCompany
        {
            get
            {
                if (this.Company == null)
                {
                    _availableBankAccountCompany = new XPCollection<BankAccount>(Session);
                }
                else
                {
                    _availableBankAccountCompany = new XPCollection<BankAccount>(Session, new GroupOperator
                                                (GroupOperatorType.And,
                                                new BinaryOperator("Company", this.Company)));
                }
                return _availableBankAccountCompany;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableBankAccountCompany", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("PurchasePrePaymentInvoiceBankAccountCompanyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BankAccount BankAccountCompany
        {
            get { return _bankAccountCompany; }
            set
            {
                SetPropertyValue("BankAccountCompany", ref _bankAccountCompany, value);
                if (!IsLoading)
                {
                    if (this._bankAccountCompany != null)
                    {
                        this.CompanyAccountNo = this._bankAccountCompany.AccountNo;
                        this.CompanyAccountName = this._bankAccountCompany.AccountName;
                    }
                }
            }
        }

        [Appearance("PurchasePrePaymentInvoiceCompanyAccountNoClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string CompanyAccountNo
        {
            get { return _companyAccountNo; }
            set { SetPropertyValue("CompanyAccountNo", ref _companyAccountNo, value); }
        }

        [Appearance("PurchasePrePaymentInvoiceCompanyAccountNameClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string CompanyAccountName
        {
            get { return _companyAccountName; }
            set { SetPropertyValue("CompanyAccountName", ref _companyAccountName, value); }
        }

        [Browsable(false)]
        public XPCollection<BankAccount> AvailableBankAccounts
        {
            get
            {
                if (this.PayToVendor == null)
                {
                    _availableBankAccounts = new XPCollection<BankAccount>(Session);
                }
                else
                {
                    _availableBankAccounts = new XPCollection<BankAccount>(Session,
                                             new GroupOperator(GroupOperatorType.And,
                                             new BinaryOperator("BusinessPartner", this.PayToVendor)));
                }

                return _availableBankAccounts;
            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableBankAccounts", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("PurchasePrePaymentInvoiceBankAccountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BankAccount BankAccount
        {
            get { return _bankAccount; }
            set
            {
                SetPropertyValue("BankAccount", ref _bankAccount, value);
                if (!IsLoading)
                {
                    if (this._bankAccount != null)
                    {
                        this.AccountNo = this._bankAccount.AccountNo;
                        this.AccountName = this._bankAccount.AccountName;
                    }
                }
            }
        }

        [Appearance("PurchasePrePaymentInvoiceAccountNoClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string AccountNo
        {
            get { return _accountNo; }
            set { SetPropertyValue("AccountNo", ref _accountNo, value); }
        }

        [Appearance("PurchasePrePaymentInvoiceAccountNameClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string AccountName
        {
            get { return _accountName; }
            set { SetPropertyValue("AccountName", ref _accountName, value); }
        }

        #endregion Bank

        [Appearance("PurchasePrePaymentInvoiceTaxNoClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string TaxNo
        {
            get { return _taxNo; }
            set { SetPropertyValue("TaxNo", ref _taxNo, value); }
        }

        #region Amount

        [Appearance("PurchasePrePaymentInvoiceMaxPayClose", Enabled = false)]
        public double MaxPay
        {
            get { return _maxPay; }
            set { SetPropertyValue("MaxPay", ref _maxPay, value); }
        }

        [ImmediatePostData()]
        [Appearance("PurchasePrePaymentInvoicePayClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double Pay
        {
            get { return _pay; }
            set
            {
                SetPropertyValue("Pay", ref _pay, value);
                if (!IsLoading)
                {
                    if (this.PostedCount > 0)
                    {
                        if (this.Outstanding > 0)
                        {
                            if (this._pay > 0 && this._pay > this.Outstanding)
                            {
                                this._pay = this.Outstanding;
                            }
                        }
                    }

                    if (this.PostedCount == 0)
                    {
                        if (this._pay > 0 && this._pay > this.MaxPay)
                        {
                            this._pay = this.MaxPay;
                        }
                    }
                }
            }
        }

        [ImmediatePostData()]
        [Appearance("PurchasePrePaymentInvoiceDP_PercentageClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double DP_Percentage
        {
            get { return _dp_percentage; }
            set
            {
                SetPropertyValue("DP_Percentage", ref _dp_percentage, value);
                if (!IsLoading)
                {
                    if (this._dp_percentage > 0)
                    {
                        this.DP_Amount = (this._dp_percentage / 100) * this.Pay;
                    }
                }
            }
        }

        [Appearance("PurchasePrePaymentInvoiceDP_AmountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double DP_Amount
        {
            get { return _dp_Amount; }
            set { SetPropertyValue("DP_Amount", ref _dp_Amount, value); }
        }

        [Appearance("PurchasePrePaymentInvoiceOutstandingClose", Enabled = false)]
        public double Outstanding
        {
            get { return _outstanding; }
            set { SetPropertyValue("Outstanding", ref _outstanding, value); }
        }

        [Appearance("PurchasePrePaymentInvoicePlanClose", Enabled = false)]
        public double Plan
        {
            get { return _plan; }
            set { SetPropertyValue("Plan", ref _plan, value); }
        }

        #endregion Amount

        [Appearance("PurchasePrePaymentInvoiceDescriptionClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Size(512)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        [Appearance("PurchasePrePaymentInvoicePostedCountClose", Enabled = false)]
        public int PostedCount
        {
            get { return _postedCount; }
            set { SetPropertyValue("PostedCount", ref _postedCount, value); }
        }

        [Appearance("PurchasePrePaymentInvoiceStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("PurchasePrePaymentInvoiceStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("PurchasePrePaymentInvoiceEstimatedDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime EstimatedDate
        {
            get { return _estimatedDate; }
            set { SetPropertyValue("EstimatedDate", ref _estimatedDate, value); }
        }

        [Appearance("PurchasePrePaymentInvoiceSignCodeClose", Enabled = false)]
        public string SignCode
        {
            get { return _signCode; }
            set { SetPropertyValue("SignCode", ref _signCode, value); }
        }

        [Appearance("PurchasePrePaymentInvoiceProjectHeaderClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public ProjectHeader ProjectHeader
        {
            get { return _projectHeader; }
            set { SetPropertyValue("ProjectHeader", ref _projectHeader, value); }
        }

        [ImmediatePostData()]
        [Browsable(false)]
        public XPCollection<PurchaseOrder> AvailablePurchaseOrder
        {
            get
            {
                if (!IsLoading)
                {

                    if (this.BuyFromVendor != null)
                    {
                        XPCollection<PurchaseOrder> _locCollectionPOs = new XPCollection<PurchaseOrder>
                                                                        (Session, new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("ActiveApproved3", true),
                                                                        new BinaryOperator("BuyFromVendor", this.BuyFromVendor),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("Status", Status.Progress),
                                                                        new BinaryOperator("Status", Status.Posted)))
                                                                        );

                        if (_locCollectionPOs != null && _locCollectionPOs.Count() > 0)
                        {
                            _availablePurchaseOrder = _locCollectionPOs;
                        }
                    }
                }
                return _availablePurchaseOrder;
            }
        }

        //Wahab 27-02-2020
        //[Association("PurchaseOrder-PurchasePrePaymentInvoices")]
        [DataSourceProperty("AvailableSalesOrder", DataSourcePropertyIsNullMode.SelectNothing)]
        [Appearance("PurchasePrePaymentInvoicePurchaseOrderClose", Criteria = "ActivationPosting = true", Enabled = false)]       
        public PurchaseOrder PurchaseOrder
        {
            get { return _purchaseOrder; }
            set { SetPropertyValue("PurchaseOrder", ref _purchaseOrder, value); }
        }

        [Appearance("PurchasePrePaymentInvoicePayableTransactionClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PayableTransaction PayableTransaction
        {
            get { return _payableTransaction; }
            set { SetPropertyValue("PayableTransaction", ref _payableTransaction, value); }
        }

        [Browsable(false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Association("PurchasePrePaymentInvoice-PaymentOutPlans")]
        public XPCollection<PaymentOutPlan> PaymentOutPlans
        {
            get { return GetCollection<PaymentOutPlan>("PaymentOutPlans"); }
        }

        [Association("PurchasePrePaymentInvoice-ApprovalLines")]
        public XPCollection<ApprovalLine> ApprovalLines
        {
            get { return GetCollection<ApprovalLine>("ApprovalLines"); }
        }

        #region Approved

        [Browsable(false)]
        [Appearance("SalesInvoiceActiveApproved1Close", Enabled = false)]
        public bool ActiveApproved1
        {
            get { return _activeApproved1; }
            set { SetPropertyValue("ActiveApproved1", ref _activeApproved1, value); }
        }

        [Browsable(false)]
        [Appearance("SalesInvoiceActiveApproved2Close", Enabled = false)]
        public bool ActiveApproved2
        {
            get { return _activeApproved2; }
            set { SetPropertyValue("ActiveApproved2", ref _activeApproved2, value); }
        }

        [Browsable(false)]
        [Appearance("SalesInvoiceActiveApproved3Close", Enabled = false)]
        public bool ActiveApproved3
        {
            get { return _activeApproved3; }
            set { SetPropertyValue("ActiveApproved3", ref _activeApproved3, value); }
        }

        #endregion Approved

        #endregion Field

    }
}