﻿#region Default

using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

#endregion Default

using DevExpress.ExpressApp.ConditionalAppearance;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using System.IO;

namespace FullDrive.Module.BusinessObjects
{
    #region Default

    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("PaymentRealizationRuleUnique", DefaultContexts.Save, "Code")]

    #endregion Default

    // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
    public class PaymentRealization : FullDriveSysBaseObject
    {
        #region Default

        private bool _activationPosting;
        private string _code;
        private string _name;
        private Employee _employee;
        private Position _position;
        private Section _section;
        private Division _division;
        private Department _department;
        private Company _company;
        private double _totalAmount;
        private double _totalUsedAmount;
        private double _totalRemainingAmount;
        private Status _status;
        private DateTime _statusDate;
        private GlobalFunction _globFunc;
        private string _userAccess;
        //clm
        private bool _activeApproved1;
        private bool _activeApproved2;
        private bool _activeApproved3;

        public PaymentRealization(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).

            this._globFunc = new GlobalFunction();
            this._code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.PaymentRealization);
            this._status = Status.Open;
            this._statusDate = DateTime.Now;

            #region UserAccess

            _userAccess = SecuritySystem.CurrentUserName;
            UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
            if (Session.IsNewObject(this))
            {
                if (_locUserAccess != null)
                {
                    if (_locUserAccess.Employee != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            Employee = _locUserAccess.Employee;
                        }

                        if (_locUserAccess.Employee.Company != null)
                        {
                            Company = _locUserAccess.Employee.Company;
                        }

                        if (_locUserAccess.Employee.Position != null)
                        {
                            Position = _locUserAccess.Employee.Position;
                        }

                        if (_locUserAccess.Employee.Section != null)
                        {
                            Section = _locUserAccess.Employee.Section;
                        }

                        if (_locUserAccess.Employee.Division != null)
                        {
                            Division = _locUserAccess.Employee.Division;
                        }

                        if (_locUserAccess.Employee.Department != null)
                        {
                            Department = _locUserAccess.Employee.Department;
                        }
                    }
                }
            }

            #endregion UserAccess
        }

        #endregion Default

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [ImmediatePostData()]
        [Appearance("PaymentRealizationCodeClose", Enabled = false)]
        [Appearance("PaymentRealizationRedColor", Criteria = "ActiveApproved1 = true", BackColor = "#fd79a8")]
        [Appearance("PaymentRealizationYellowColor", Criteria = "ActiveApproved2 = true", BackColor = "#ffeaa7")]
        [Appearance("PaymentRealizationGreenOColor", Criteria = "ActiveApproved3 = true", BackColor = "#81ecec")]
        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("PaymentRealizationNameClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        [Appearance("PaymentRealizationEmployeeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue("Employee", ref _employee, value); }
        }

        [Appearance("PaymentRealizationPositionClose", Enabled = false)]
        public Position Position
        {
            get { return _position; }
            set { SetPropertyValue("Position", ref _position, value); }
        }

        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        [Appearance("PaymentRealizationCompanyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [DataSourceCriteria("Active = true And Company = '@This.Company'"), ImmediatePostData()]
        [Appearance("PaymentRealizationDivisionClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        [DataSourceCriteria("Active = true And Division = '@This.Division'"), ImmediatePostData()]
        [Appearance("PaymentRealizationDepartmentClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        [DataSourceCriteria("Active = true And Department = '@This.Department'"), ImmediatePostData()]
        [Appearance("PaymentRealizationSectionClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Section Section
        {
            get { return _section; }
            set { SetPropertyValue("Section", ref _section, value); }
        }

        [Appearance("PaymentRealizationTotalAmountClose", Enabled = false)]
        public double TotalAmount
        {
            get { return _totalAmount; }
            set { SetPropertyValue("TotalAmount", ref _totalAmount, value); }
        }

        [Appearance("PaymentRealizationTotalUsedAmountClose", Enabled = false)]
        public double TotalUsedAmount
        {
            get
            {
                double _result = 0;
                if (!IsLoading)
                {
                    if (GetTotalUsedAmount() > 0)
                    {
                        _result = UpdateTotalApprovedAmount(true);
                    }
                }
                return _result;
            }
        }

        [Appearance("PaymentRealizationTotalRemainingAmountClose", Enabled = false)]
        public double TotalRemainingAmount
        {
            get
            {
                double _result = 0;
                if (!IsLoading)
                {
                    if (GetTotalUsedAmount() > 0)
                    {
                        _result = UpdateTotalRemainingAmount(true);
                    }
                }
                return _result;
            }
        }

        //[Appearance("PaymentRealizationStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("PaymentRealizationStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Association("PaymentRealization-PaymentRealizationLines")]
        public XPCollection<PaymentRealizationLine> PaymentRealizationLines
        {
            get { return GetCollection<PaymentRealizationLine>("PaymentRealizationLines"); }
        }

        [Association("PaymentRealization-ApprovalLines")]
        public XPCollection<ApprovalLine> ApprovalLines
        {
            get { return GetCollection<ApprovalLine>("ApprovalLines"); }
        }

        [Association("PaymentRealization-CashAdvanceCollections")]
        public XPCollection<CashAdvanceCollection> CashAdvanceCollections
        {
            get { return GetCollection<CashAdvanceCollection>("CashAdvanceCollections"); }
        }

        //clm
        [Browsable(false)]
        [Appearance("PaymentRealizationActiveApproved1Close", Enabled = false)]
        public bool ActiveApproved1
        {
            get { return _activeApproved1; }
            set { SetPropertyValue("ActiveApproved1", ref _activeApproved1, value); }
        }

        [Browsable(false)]
        [Appearance("PaymentRealizationActiveApproved2Close", Enabled = false)]
        public bool ActiveApproved2
        {
            get { return _activeApproved2; }
            set { SetPropertyValue("ActiveApproved2", ref _activeApproved2, value); }
        }

        [Browsable(false)]
        [Appearance("PaymentRealizationActiveApproved3Close", Enabled = false)]
        public bool ActiveApproved3
        {
            get { return _activeApproved3; }
            set { SetPropertyValue("ActiveApproved3", ref _activeApproved3, value); }
        }

        #endregion Field

        //==================================================== Code Only =====================================================

        private void UpdateName()
        {
            try
            {
                this._name = string.Format("PaymentRealization {0} [ {1} ]", Employee.Name);
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PaymentRealization" + ex.ToString());
            }
        }

        #region GetTotalUsedAmount

        public int GetTotalUsedAmount()
        {
            int _result = 0;

            try
            {
                XPCollection<PaymentRealizationLine> _locPaymentRealizationLines = new XPCollection<PaymentRealizationLine>
                                                                                   (Session, new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("PaymentRealization", this)));

                if (_locPaymentRealizationLines != null && _locPaymentRealizationLines.Count > 0)
                {
                    _result = _locPaymentRealizationLines.Count();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PaymentRealization", ex.ToString());
            }
            return _result;
        }

        public double UpdateTotalApprovedAmount(bool forceChangeEvents)
        {
            double _result = 0;
            //double _result1 = 0;

            try
            {
                double _locUsedAmount = 0;
                double _locTotalAmount = this._totalAmount;
                if (!IsLoading)
                {
                    XPCollection<PaymentRealizationLine> _locPaymentRealizationLines = new XPCollection<PaymentRealizationLine>
                                                                         (Session, new GroupOperator(GroupOperatorType.And,
                                                                          new BinaryOperator("PaymentRealization", this)));

                    if (_locPaymentRealizationLines != null && _locPaymentRealizationLines.Count() > 0)
                    {
                        foreach (PaymentRealizationLine _locPaymentRealizationLine in _locPaymentRealizationLines)
                        {
                            _locUsedAmount = _locUsedAmount + _locPaymentRealizationLine.AmountRealized;
                        }
                        _result = _locUsedAmount;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PaymentRealization " + ex.ToString());
            }
            return _result;
        }

        public double GetSumTotalUsedAmount()
        {
            double _result = 0;
            //double _result1 = 0;

            try
            {
                double _locSumTotalUsedAmount = 0;

                if (!IsLoading)
                {
                    if (this.Code != null)
                    {
                        XPCollection<PaymentRealization> _locPaymentRealizations = new XPCollection<PaymentRealization>
                                                                                   (Session, new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("PaymentRealization", this)));

                        if (_locPaymentRealizations != null && _locPaymentRealizations.Count > 0)
                        {
                            foreach (PaymentRealization _locPaymentRealization in _locPaymentRealizations)
                            {
                                _locSumTotalUsedAmount = _locSumTotalUsedAmount + _locPaymentRealization.TotalUsedAmount;
                            }
                        }
                        _result = _locSumTotalUsedAmount;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PaymentRealization", ex.ToString());
            }
            return _result;
        }

        #endregion GetTotalUsedAmount

        #region GetTotalRemainingAmount

        public int GetTotalRemainingAmount()
        {
            int _result = 0;

            try
            {
                XPCollection<PaymentRealizationLine> _locPaymentRealizationLines = new XPCollection<PaymentRealizationLine>
                                                                                   (Session, new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("PaymentRealization", this)));

                if (_locPaymentRealizationLines != null && _locPaymentRealizationLines.Count > 0)
                {
                    _result = _locPaymentRealizationLines.Count();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PaymentRealization", ex.ToString());
            }
            return _result;
        }

        public double UpdateTotalRemainingAmount(bool forceChangeEvents)
        {
            double _result = 0;
            try
            {
                double _locTotalAmount = this._totalAmount;
                double _locTotalUsedAmount = 0;
                double _locRemainingAmount = 0;

                if (!IsLoading)
                {
                    XPCollection<PaymentRealizationLine> _locPaymentRealizationLines = new XPCollection<PaymentRealizationLine>
                                                                                       (Session, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("PaymentRealization", this)));

                    if (_locPaymentRealizationLines != null && _locPaymentRealizationLines.Count() > 0)
                    {
                        foreach (PaymentRealizationLine _locPaymentRealizationLine in _locPaymentRealizationLines)
                        {
                            _locTotalUsedAmount = _locTotalUsedAmount + _locPaymentRealizationLine.AmountRealized;
                            _locRemainingAmount = _locTotalAmount - _locTotalUsedAmount;
                        }
                        _result = _locRemainingAmount;
                    }

                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PaymentRealization " + ex.ToString());
            }
            return _result;
        }

        public double GetSumTotalRemainingAmount()
        {
            double _result = 0;
            //double _result1 = 0;

            try
            {
                double _locTotalAmount = this._totalAmount;
                double _locTotalUsedAmount = this._totalUsedAmount;
                double _locRemainingAmount = 0;

                if (!IsLoading)
                {
                    if (this.Code != null)
                    {
                        XPCollection<PaymentRealization> _locPaymentRealizations = new XPCollection<PaymentRealization>
                                                                     (Session, new GroupOperator(GroupOperatorType.And,
                                                                      new BinaryOperator("PaymentRealization", this)));

                        if (_locPaymentRealizations != null && _locPaymentRealizations.Count > 0)
                        {
                            foreach (PaymentRealization _locPaymentRealization in _locPaymentRealizations)
                            {
                                //_locTotalUsedAmount = _locTotalUsedAmount - _locPaymentRealizationLine.RealizationAmount;
                                _locRemainingAmount = _locTotalAmount - _locTotalUsedAmount;
                            }
                        }
                        _result = _locRemainingAmount;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PaymentRealization", ex.ToString());
            }
            return _result;
        }

        public double GetRemainingAmount(bool forceChangeEvents)
        {
            double _result = 0;
            try
            {
                double _locRemainingAmount = 0;

                if (!IsLoading != null)
                {
                    if (this._totalUsedAmount > 0)
                    {
                        _locRemainingAmount = this._totalAmount - this._totalUsedAmount;
                    }
                }
                _result = _locRemainingAmount;
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PaymentRealization" + ex.ToString());
            }

            return _result;
        }

        #endregion GetTotalRemainingAmount

    }
}