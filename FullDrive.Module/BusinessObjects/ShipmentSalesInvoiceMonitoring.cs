﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using DevExpress.ExpressApp.Editors;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("ShipmentSalesInvoiceMonitoringRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class ShipmentSalesInvoiceMonitoring : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).

        private bool _activationPosting;
        private bool _select;
        private string _code;
        private ShipmentSalesInvoice _shipmentSalesInvoice;
        private ShipmentSalesInvoiceLine _shipmentSalesInvoiceLine;
        private Routing _routing;
        private RoutingPackage _routingPackage;
        private ShipmentType _shipmentType;
        private TransportType _transportType;
        private TransactionTerm _transactionTerm;
        private DeliveryType _deliveryType;
        private ContainerType _containerType;
        #region VariableFrom
        private Country _countryFrom;
        private City _cityFrom;
        private XPCollection<Area> _availableAreaFrom;
        private Area _areaFrom;
        private XPCollection<Location> _availableLocationFrom;
        private Location _locationFrom;
        private XPCollection<TransportLocation> _availableTransportLocationFrom;
        private TransportLocation _transportLocationFrom;
        private XPCollection<BusinessPartner> _availableShipper;
        private BusinessPartner _shipper;
        private string _shipperContact;
        private string _shipperAddress;
        #endregion VariableFrom
        #region VariableTo
        private Country _countryTo;
        private City _cityTo;
        private XPCollection<Area> _availableAreaTo;
        private Area _areaTo;
        private XPCollection<Location> _availableLocationTo;
        private Location _locationTo;
        private XPCollection<TransportLocation> _availableTransportLocationTo;
        private TransportLocation _transportLocationTo;
        private XPCollection<BusinessPartner> _availableConsignee;
        private BusinessPartner _consignee;
        private string _consigneeContact;
        private string _consigneeAddress;
        #endregion VariableTo
        #region VariableBill
        private BusinessPartner _billToBusinessPartner;
        private string _billToContact;
        private Country _billToCountry;
        private City _billToCity;
        private string _billToAddress;
        #endregion VariableBill
        private PriceGroup _priceGroup;
        private PriceType _priceType;
        private ItemGroup _itemGroup;
        private XPCollection<Item> _availableItem;
        private Item _item;
        private string _name;
        private string _description;
        private XPCollection<UnitOfMeasure> _availableUnitOfMeasure;
        #region InisialisasiDefaultQty
        private double _dQty;
        private UnitOfMeasure _dUom;
        private double _qty;
        private UnitOfMeasure _uom;
        private double _tQty;
        #endregion InisialisasiDefaultQty
        private double _maxAmount;
        private double _amount;
        private double _postedAmount;
        private double _outstandingAmount;
        private Status _status;
        private DateTime _statusDate;
        private int _postedCount;
        private ShipmentSalesMonitoring _shipmentSalesMonitoring;
        private GlobalFunction _globFunc;

        public ShipmentSalesInvoiceMonitoring(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            DateTime now = DateTime.Now;
            _globFunc = new GlobalFunction();
            this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.ShipmentSalesInvoiceMonitoring);
            this.Status = Status.Open;
            this.StatusDate = now;
            this.Select = true;
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [Appearance("ShipmentSalesMonitoringSelectClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public bool Select
        {
            get { return _select; }
            set { SetPropertyValue("Select", ref _select, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("ShipmentSalesMonitoringCodeEnabled", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("ShipmentSalesInvoiceMonitoringShipmentSalesInvoiceEnabled", Criteria = "ActivationPosting = true", Enabled = false)]
        public ShipmentSalesInvoice ShipmentSalesInvoice
        {
            get { return _shipmentSalesInvoice; }
            set { SetPropertyValue("ShipmentSalesInvoice", ref _shipmentSalesInvoice, value); }
        }

        [Appearance("ShipmentSalesInvoiceMonitoringShipmentSalesInvoiceLineEnabled", Criteria = "ActivationPosting = true", Enabled = false)]
        public ShipmentSalesInvoiceLine ShipmentSalesInvoiceLine
        {
            get { return _shipmentSalesInvoiceLine; }
            set { SetPropertyValue("ShipmentSalesInvoiceLine", ref _shipmentSalesInvoiceLine, value); }
        }

        [ImmediatePostData()]
        [Appearance("ShipmentSalesInvoiceMonitoringRoutingEnabled", Criteria = "ActivationPosting = true", Enabled = false)]
        public Routing Routing
        {
            get { return _routing; }
            set { SetPropertyValue("Routing", ref _routing, value); }
        }

        [Appearance("ShipmentSalesInvoiceMonitoringRoutingPackageEnabled", Criteria = "ActivationPosting = true", Enabled = false)]
        public RoutingPackage RoutingPackage
        {
            get { return _routingPackage; }
            set { SetPropertyValue("RoutingPackage", ref _routingPackage, value); }
        }

        [Appearance("ShipmentSalesInvoiceMonitoringShipmentTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public ShipmentType ShipmentType
        {
            get { return _shipmentType; }
            set { SetPropertyValue("ShipmentType", ref _shipmentType, value); }
        }

        [Appearance("ShipmentSalesInvoiceMonitoringTransportTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public TransportType TransportType
        {
            get { return _transportType; }
            set { SetPropertyValue("TransportType", ref _transportType, value); }
        }

        [Appearance("ShipmentSalesInvoiceMonitoringDeliveryTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DeliveryType DeliveryType
        {
            get { return _deliveryType; }
            set { SetPropertyValue("DeliveryType", ref _deliveryType, value); }
        }

        [Appearance("ShipmentSalesInvoiceMonitoringContainerTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public ContainerType ContainerType
        {
            get { return _containerType; }
            set { SetPropertyValue("ContainerType", ref _containerType, value); }
        }

        [Appearance("ShipmentSalesInvoiceMonitoringTransactionTermClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public TransactionTerm TransactionTerm
        {
            get { return _transactionTerm; }
            set { SetPropertyValue("TransactionTerm", ref _transactionTerm, value); }
        }

        #region From

        [ImmediatePostData()]
        [Appearance("ShipmentSalesInvoiceMonitoringCountryFromClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Country CountryFrom
        {
            get { return _countryFrom; }
            set { SetPropertyValue("CountryFrom", ref _countryFrom, value); }
        }

        [ImmediatePostData()]
        [Appearance("ShipmentSalesInvoiceMonitoringCityFromClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public City CityFrom
        {
            get { return _cityFrom; }
            set { SetPropertyValue("CityFrom", ref _cityFrom, value); }
        }

        [Browsable(false)]
        public XPCollection<Area> AvailableAreaFrom
        {
            get
            {
                if (this.CountryFrom != null && this.CityFrom != null)
                {
                    _availableAreaFrom = new XPCollection<Area>(Session,
                                    new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("Country", this.CountryFrom),
                                    new BinaryOperator("City", this.CityFrom)));

                }
                else if (this.CountryFrom != null && this.CityFrom == null)
                {
                    _availableAreaFrom = new XPCollection<Area>(Session,
                                    new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("Country", this.CountryFrom)));
                }
                else if (this.CountryFrom == null && this.CityFrom != null)
                {
                    _availableAreaFrom = new XPCollection<Area>(Session,
                                    new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("City", this.CityFrom)));
                }
                else
                {
                    _availableAreaFrom = new XPCollection<Area>(Session);
                }

                return _availableAreaFrom;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableAreaFrom", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("ShipmentSalesInvoiceMonitoringAreaFromClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public Area AreaFrom
        {
            get { return _areaFrom; }
            set { SetPropertyValue("AreaFrom", ref _areaFrom, value); }
        }

        [Browsable(false)]
        public XPCollection<Location> AvailableLocationFrom
        {
            get
            {
                if (this.AreaFrom != null)
                {
                    _availableLocationFrom = new XPCollection<Location>(Session,
                                          new GroupOperator(GroupOperatorType.And,
                                          new BinaryOperator("Area", this.AreaFrom),
                                          new BinaryOperator("Active", true)));

                }
                else
                {
                    _availableLocationFrom = new XPCollection<Location>(Session);
                }

                return _availableLocationFrom;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableLocationFrom", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("ShipmentSalesInvoiceMonitoringLocationFromClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public Location LocationFrom
        {
            get { return _locationFrom; }
            set { SetPropertyValue("LocationFrom", ref _locationFrom, value); }
        }

        [Browsable(false)]
        public XPCollection<TransportLocation> AvailableTransportLocationFrom
        {
            get
            {
                if (this.CountryFrom != null)
                {
                    _availableTransportLocationFrom = new XPCollection<TransportLocation>(Session,
                                          new GroupOperator(GroupOperatorType.And,
                                          new BinaryOperator("Country", this.CountryFrom),
                                          new BinaryOperator("Active", true)));

                }
                else
                {
                    _availableTransportLocationFrom = new XPCollection<TransportLocation>(Session);
                }

                return _availableTransportLocationFrom;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableTransportLocationFrom", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("ShipmentSalesInvoiceMonitoringTransportLocationFromClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public TransportLocation TransportLocationFrom
        {
            get { return _transportLocationFrom; }
            set { SetPropertyValue("TransportLocationFrom", ref _transportLocationFrom, value); }
        }

        [Browsable(false)]
        public XPCollection<BusinessPartner> AvailableShipper
        {
            get
            {
                if (this.CountryFrom != null)
                {
                    if (this.CityFrom != null)
                    {
                        _availableShipper = new XPCollection<BusinessPartner>
                                            (Session, new GroupOperator(GroupOperatorType.And,
                                            new BinaryOperator("Country", this.CountryFrom),
                                            new BinaryOperator("City", this.CityFrom)));
                    }
                    else
                    {
                        _availableShipper = new XPCollection<BusinessPartner>
                                            (Session, new GroupOperator(GroupOperatorType.And,
                                            new BinaryOperator("Country", this.CountryFrom)));
                    }
                }
                else
                {
                    _availableShipper = new XPCollection<BusinessPartner>(Session);
                }


                return _availableShipper;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableShipper", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("ShipmentSalesInvoiceMonitoringShipperClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BusinessPartner Shipper
        {
            get { return _shipper; }
            set
            {
                SetPropertyValue("Shipper", ref _shipper, value);
                if (!IsLoading)
                {
                    if (this._shipper != null)
                    {
                        this.ShipperContact = this._shipper.Contact;
                        this.ShipperAddress = this._shipper.Address;
                    }
                }
            }
        }

        [Appearance("ShipmentSalesInvoiceMonitoringShipperContactClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string ShipperContact
        {
            get { return _shipperContact; }
            set { SetPropertyValue("ShipperContact", ref _shipperContact, value); }
        }

        [Size(512)]
        [Appearance("ShipmentSalesInvoiceMonitoringShipperAddressClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string ShipperAddress
        {
            get { return _shipperAddress; }
            set { SetPropertyValue("ShipperAddress", ref _shipperAddress, value); }
        }

        #endregion From

        #region To

        [ImmediatePostData()]
        [Appearance("ShipmentSalesInvoiceMonitoringCountryToClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Country CountryTo
        {
            get { return _countryTo; }
            set { SetPropertyValue("CountryTo", ref _countryTo, value); }
        }

        [Appearance("ShipmentSalesInvoiceMonitoringCityToClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public City CityTo
        {
            get { return _cityTo; }
            set { SetPropertyValue("CityTo", ref _cityTo, value); }
        }

        [Browsable(false)]
        public XPCollection<Area> AvailableAreaTo
        {
            get
            {
                if (this.CountryTo != null && this.CityTo != null)
                {
                    _availableAreaTo = new XPCollection<Area>(Session,
                                    new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("Country", this.CountryTo),
                                    new BinaryOperator("City", this.CityTo)));

                }
                else if (this.CountryTo != null && this.CityTo == null)
                {
                    _availableAreaTo = new XPCollection<Area>(Session,
                                    new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("Country", this.CountryTo)));
                }
                else if (this.CountryTo == null && this.CityTo != null)
                {
                    _availableAreaTo = new XPCollection<Area>(Session,
                                    new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("City", this.CityTo)));
                }
                else
                {
                    _availableAreaTo = new XPCollection<Area>(Session);
                }

                return _availableAreaTo;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableAreaTo", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("ShipmentSalesInvoiceMonitoringAreaToClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public Area AreaTo
        {
            get { return _areaTo; }
            set { SetPropertyValue("AreaTo", ref _areaTo, value); }
        }

        [Browsable(false)]
        public XPCollection<Location> AvailableLocationTo
        {
            get
            {
                if (this.AreaTo != null)
                {
                    _availableLocationTo = new XPCollection<Location>(Session,
                                          new GroupOperator(GroupOperatorType.And,
                                          new BinaryOperator("Area", this.AreaTo),
                                          new BinaryOperator("Active", true)));

                }
                else
                {
                    _availableLocationTo = new XPCollection<Location>(Session);
                }

                return _availableLocationTo;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableLocationTo", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("ShipmentSalesInvoiceMonitoringLocationToClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public Location LocationTo
        {
            get { return _locationTo; }
            set { SetPropertyValue("LocationTo", ref _locationTo, value); }
        }

        [Browsable(false)]
        public XPCollection<TransportLocation> AvailableTransportLocationTo
        {
            get
            {
                if (this.CountryTo != null)
                {
                    _availableTransportLocationTo = new XPCollection<TransportLocation>(Session,
                                          new GroupOperator(GroupOperatorType.And,
                                          new BinaryOperator("Country", this.CountryTo),
                                          new BinaryOperator("Active", true)));

                }
                else
                {
                    _availableTransportLocationTo = new XPCollection<TransportLocation>(Session);
                }

                return _availableTransportLocationTo;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableTransportLocationTo", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("ShipmentSalesInvoiceMonitoringTransportLocationToClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public TransportLocation TransportLocationTo
        {
            get { return _transportLocationTo; }
            set { SetPropertyValue("TransportLocationTo", ref _transportLocationTo, value); }
        }

        [Browsable(false)]
        public XPCollection<BusinessPartner> AvailableConsignee
        {
            get
            {
                if (this.CountryFrom != null)
                {
                    if (this.CityFrom != null)
                    {
                        _availableConsignee = new XPCollection<BusinessPartner>
                                            (Session, new GroupOperator(GroupOperatorType.And,
                                            new BinaryOperator("Country", this.CountryTo),
                                            new BinaryOperator("City", this.CityTo)));
                    }
                    else
                    {
                        _availableConsignee = new XPCollection<BusinessPartner>
                                            (Session, new GroupOperator(GroupOperatorType.And,
                                            new BinaryOperator("Country", this.CountryTo)));
                    }
                }
                else
                {
                    _availableConsignee = new XPCollection<BusinessPartner>(Session);
                }


                return _availableConsignee;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableConsignee", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("ShipmentSalesInvoiceMonitoringConsigneeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BusinessPartner Consignee
        {
            get { return _consignee; }
            set
            {
                SetPropertyValue("Consignee", ref _consignee, value);
                if (!IsLoading)
                {
                    if (this._consignee != null)
                    {
                        this.ConsigneeContact = this._consignee.Contact;
                        this.ConsigneeAddress = this._consignee.Address;
                    }
                }
            }
        }

        [Appearance("ShipmentSalesInvoiceMonitoringConsigneeContactClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string ConsigneeContact
        {
            get { return _consigneeContact; }
            set { SetPropertyValue("ConsigneeContact", ref _consigneeContact, value); }
        }

        [Size(512)]
        [Appearance("ShipmentSalesInvoiceMonitoringConsigneeAddressClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string ConsigneeAddress
        {
            get { return _consigneeAddress; }
            set { SetPropertyValue("ConsigneeAddress", ref _consigneeAddress, value); }
        }

        #endregion To

        #region BillToCustomer

        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        [Appearance("ShipmentSalesInvoiceMonitoringBillToBusinessPartnerClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BusinessPartner BillToBusinessPartner
        {
            get { return _billToBusinessPartner; }
            set
            {
                SetPropertyValue("BillToCostumer", ref _billToBusinessPartner, value);
                if (!IsLoading)
                {
                    if (this._billToBusinessPartner != null)
                    {
                        this.BillToContact = this._billToBusinessPartner.Contact;
                        this.BillToAddress = this._billToBusinessPartner.Address;
                        this.BillToCountry = this._billToBusinessPartner.Country;
                        this.BillToCity = this._billToBusinessPartner.City;
                    }
                }
            }
        }

        [Appearance("ShipmentSalesInvoiceMonitoringBillToContactClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string BillToContact
        {
            get { return _billToContact; }
            set { SetPropertyValue("BillToContact", ref _billToContact, value); }
        }

        [DataSourceCriteria("Active = true")]
        [Appearance("ShipmentSalesInvoiceMonitoringBillCountryClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Country BillToCountry
        {
            get { return _billToCountry; }
            set { SetPropertyValue("BillToCountry", ref _billToCountry, value); }
        }

        [DataSourceCriteria("Active = true")]
        [Appearance("ShipmentSalesInvoiceMonitoringBillToCityClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public City BillToCity
        {
            get { return _billToCity; }
            set { SetPropertyValue("BillToCity", ref _billToCity, value); }
        }

        [Appearance("ShipmentSalesInvoiceMonitoringBillToAddressClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Size(512)]
        public string BillToAddress
        {
            get { return _billToAddress; }
            set { SetPropertyValue("BillToAddress", ref _billToAddress, value); }
        }

        #endregion BillToCostumer

        [ImmediatePostData()]
        [Appearance("ShipmentSalesInvoiceMonitoringPriceGroupClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PriceGroup PriceGroup
        {
            get { return _priceGroup; }
            set { SetPropertyValue("PriceGroup", ref _priceGroup, value); }
        }

        [ImmediatePostData()]
        [Appearance("ShipmentSalesInvoiceMonitoringPriceTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PriceType PriceType
        {
            get { return _priceType; }
            set { SetPropertyValue("PriceType", ref _priceType, value); }
        }

        [ImmediatePostData()]
        [Appearance("ShipmentSalesInvoiceMonitoringItemGroupClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public ItemGroup ItemGroup
        {
            get { return _itemGroup; }
            set { SetPropertyValue("ItemGroup", ref _itemGroup, value); }
        }

        [Browsable(false)]
        public XPCollection<Item> AvailableItem
        {
            get
            {
                if (this.ItemGroup != null)
                {
                    _availableItem = new XPCollection<Item>(Session,
                                    new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("ItemGroup", this.ItemGroup)));

                }
                else
                {
                    _availableItem = new XPCollection<Item>(Session);
                }

                return _availableItem;
            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableItem", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("ShipmentSalesInvoiceMonitoringItemClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Item Item
        {
            get { return _item; }
            set
            {
                SetPropertyValue("Item", ref _item, value);
                if (!IsLoading)
                {
                    if (this._item != null)
                    {
                        //this.DUOM = this._item.BasedUOM;
                        this.Description = GetDescriptions();
                    }
                }
            }
        }

        [Appearance("ShipmentSalesInvoiceMonitoringNameClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        [Appearance("ShipmentSalesInvoiceMonitoringDescriptionClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Size(512)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        [Browsable(false)]
        public XPCollection<UnitOfMeasure> AvailableUnitOfMeasure
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;
                if (Item == null)
                {
                    _availableUnitOfMeasure = new XPCollection<UnitOfMeasure>(Session);
                }
                else
                {
                    List<string> _stringUOM = new List<string>();

                    XPCollection<ItemUnitOfMeasure> _locItemUnitOfMeasures = new XPCollection<ItemUnitOfMeasure>
                                                                              (Session, new GroupOperator(GroupOperatorType.And,
                                                                               new BinaryOperator("Item", this.Item),
                                                                               new BinaryOperator("Active", true)));

                    if (_locItemUnitOfMeasures != null && _locItemUnitOfMeasures.Count() > 0)
                    {
                        foreach (ItemUnitOfMeasure _locItemUnitOfMeasure in _locItemUnitOfMeasures)
                        {
                            _stringUOM.Add(_locItemUnitOfMeasure.UOM.Code);
                        }
                    }

                    IEnumerable<string> _stringArrayUOMDistinct = _stringUOM.Distinct();
                    string[] _stringArrayUOMList = _stringArrayUOMDistinct.ToArray();
                    if (_stringArrayUOMList.Length == 1)
                    {
                        for (int i = 0; i < _stringArrayUOMList.Length; i++)
                        {
                            UnitOfMeasure _locUOM = Session.FindObject<UnitOfMeasure>(new BinaryOperator("Code", _stringArrayUOMList[i]));
                            if (_locUOM != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locUOM.Code + "'";
                                }
                            }
                        }
                    }
                    else if (_stringArrayUOMList.Length > 1)
                    {
                        for (int i = 0; i < _stringArrayUOMList.Length; i++)
                        {
                            UnitOfMeasure _locUOM = Session.FindObject<UnitOfMeasure>(new BinaryOperator("Code", _stringArrayUOMList[i]));
                            if (_locUOM != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locUOM.Code + "'";
                                }
                                else
                                {
                                    _endString = _endString + " OR [Code]=='" + _locUOM.Code + "'";
                                }
                            }
                        }
                    }
                    _fullString = _beginString + _endString;

                    if (_fullString != null)
                    {
                        _availableUnitOfMeasure = new XPCollection<UnitOfMeasure>(Session, CriteriaOperator.Parse(_fullString));
                    }

                }
                return _availableUnitOfMeasure;

            }
        }

        #region DefaultQty

        [Appearance("ShipmentSalesInvoiceMonitoringDQtyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double DQty
        {
            get { return _dQty; }
            set
            {
                SetPropertyValue("DQty", ref _dQty, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                }
            }
        }

        [Appearance("ShipmentSalesInvoiceMonitoringDUOMClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure DUOM
        {
            get { return _dUom; }
            set
            {
                SetPropertyValue("DUOM", ref _dUom, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                }
            }
        }

        [Appearance("ShipmentSalesInvoiceMonitoringQtyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double Qty
        {
            get { return _qty; }
            set
            {
                SetPropertyValue("Qty", ref _qty, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                }
            }
        }

        [Appearance("ShipmentSalesInvoiceMonitoringUOMClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceProperty("AvailableUnitOfMeasure", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure UOM
        {
            get { return _uom; }
            set
            {
                SetPropertyValue("UOM", ref _uom, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                }
            }
        }

        [Appearance("ShipmentSalesInvoiceMonitoringTotalQtyEnabled", Enabled = false)]
        public double TQty
        {
            get { return _tQty; }
            set { SetPropertyValue("TotalQty", ref _tQty, value); }
        }

        #endregion DefaultQty

        #region Amount

        [Appearance("ShipmentSalesInvoiceMonitoringMaxAmountClose", Enabled = false)]
        public double MaxAmount
        {
            get { return _maxAmount; }
            set { SetPropertyValue("MaxAmount", ref _maxAmount, value); }
        }

        [Appearance("ShipmentSalesInvoiceMonitoringDescriptionClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double Amount
        {
            get { return _amount; }
            set { SetPropertyValue("Amount", ref _amount, value); }
        }

        [Appearance("ShipmentSalesInvoiceMonitoringPostedAmountClose", Enabled = false)]
        public double PostedAmount
        {
            get { return _postedAmount; }
            set { SetPropertyValue("PostedAmount", ref _postedAmount, value); }
        }

        [Appearance("ShipmentSalesInvoiceMonitoringOutstandingAmountClose", Enabled = false)]
        public double OutstandingAmount
        {
            get { return _outstandingAmount; }
            set { SetPropertyValue("OutstandingAmount", ref _outstandingAmount, value); }
        }

        #endregion Amount

        [Appearance("ShipmentSalesInvoiceMonitoringStatusEnabled", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("ShipmentSalesInvoiceMonitoringStatusDateEnabled", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("ShipmentSalesInvoiceMonitoringPostedCountEnabled", Enabled = false)]
        public int PostedCount
        {
            get { return _postedCount; }
            set { SetPropertyValue("PostedCount", ref _postedCount, value); }
        }

        [Appearance("ShipmentSalesInvoiceMonitoringShipmentSalesMonitoringEnabled", Enabled = false)]
        public ShipmentSalesMonitoring ShipmentSalesMonitoring
        {
            get { return _shipmentSalesMonitoring; }
            set { SetPropertyValue("ShipmentSalesMonitoring", ref _shipmentSalesMonitoring, value); }
        }

        #endregion Field

        //=========================================================== Code Only =====================================================

        #region CodeOnly

        public string GetDescriptions()
        {
            string _result = "";
            try
            {
                if (this._item != null)
                {
                    string _result1 = null;
                    string _result2 = null;
                    if (this._item.Name != null)
                    {
                        _result1 = this._item.Name;
                    }
                    if (this._item.Description != null)
                    {
                        _result2 = this._item.Description;
                    }
                    _result = _result1 + " " + _result2;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = RoutingPackage " + ex.ToString());
            }

            return _result;
        }

        #region Set

        private void SetTotalQty()
        {
            try
            {
                double _locInvLineTotal = 0;
                DateTime now = DateTime.Now;
                if (this.Routing != null)
                {
                    if (this.Item != null && this.UOM != null && this.DUOM != null)
                    {
                        ItemUnitOfMeasure _locItemUOM = Session.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", this.Item),
                                                         new BinaryOperator("UOM", this.UOM),
                                                         new BinaryOperator("DefaultUOM", this.DUOM),
                                                         new BinaryOperator("Active", true)));
                        if (_locItemUOM != null)
                        {
                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.Qty * _locItemUOM.DefaultConversion + this.DQty;
                            }
                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.Qty / _locItemUOM.Conversion + this.DQty;
                            }
                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.Qty + this.DQty;
                            }

                            this.TQty = _locInvLineTotal;
                        }
                    }
                    else
                    {
                        _locInvLineTotal = this.Qty + this.DQty;
                        this.TQty = _locInvLineTotal;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = RoutingPackage " + ex.ToString());
            }
        }

        #endregion Set

        #endregion CodeOnly

    }
}