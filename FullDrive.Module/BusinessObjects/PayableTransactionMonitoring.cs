﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.SystemModule;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Editors;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("PayableTransactionMonitoringRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class PayableTransactionMonitoring : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private string _code;
        private PayableTransaction _payableTransaction;
        private PurchaseInvoice _purchaseInvoice;
        private PurchaseOrder _purchaseOrder;
        private PurchasePrePaymentInvoice _purchasePrePaymentInvoice;
        private Currency _currency;
        private double _totAmountDebit;
        private double _totAmountCredit;
        private Status _status;
        private DateTime _statusDate;
        private int _postedCount;
        private string _signCode;
        private PurchaseInvoiceMonitoring _purchaseInvoiceMonitoring;
        private GlobalFunction _globFunc;

        public PayableTransactionMonitoring(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            _globFunc = new GlobalFunction();
            DateTime now = DateTime.Now;
            this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.PayableTransactionMonitoring);
            this.Status = Status.Open;
            this.StatusDate = now;
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("PayableTransactionMonitoringCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("PayableTransactionMonitoringPayableTransactionClose", Enabled = false)]
        public PayableTransaction PayableTransaction
        {
            get { return _payableTransaction; }
            set { SetPropertyValue("PayableTransaction", ref _payableTransaction, value); }
        }

        [Appearance("PayableTransactionMonitoringSalesInvoiceClose", Enabled = false)]
        public PurchaseInvoice PurchaseInvoice
        {
            get { return _purchaseInvoice; }
            set { SetPropertyValue("PurchaseInvoice", ref _purchaseInvoice, value); }
        }

        [Appearance("PayableTransactionMonitoringSalesOrderClose", Enabled = false)]
        public PurchaseOrder PurchaseOrder
        {
            get { return _purchaseOrder; }
            set { SetPropertyValue("PurchaseOrder", ref _purchaseOrder, value); }
        }

        [Appearance("PayableTransactionMonitoringSalesPrePaymentInvoiceClose", Enabled = false)]
        public PurchasePrePaymentInvoice PurchasePrePaymentInvoice
        {
            get { return _purchasePrePaymentInvoice; }
            set { SetPropertyValue("PurchasePrePaymentInvoice", ref _purchasePrePaymentInvoice, value); }
        }

        [Appearance("PayableTransactionMonitoringCurrencyClose", Enabled = false)]
        public Currency Currency
        {
            get { return _currency; }
            set { SetPropertyValue("Currency", ref _currency, value); }
        }

        [Appearance("PayableTransactionMonitoringTotAmountDebitClose", Enabled = false)]
        public double TotAmountDebit
        {
            get { return _totAmountDebit; }
            set { SetPropertyValue("TotAmountDebit", ref _totAmountDebit, value); }
        }

        [Appearance("PayableTransactionMonitoringTotAmountCreditClose", Enabled = false)]
        public double TotAmountCredit
        {
            get { return _totAmountCredit; }
            set { SetPropertyValue("TotAmountCredit", ref _totAmountCredit, value); }
        }

        [Appearance("PayableTransactionMonitoringStatusEnabled", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("PayableTransactionMonitoringStatusDateEnabled", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("PayableTransactionMonitoringPostedCountEnabled", Enabled = false)]
        public int PostedCount
        {
            get { return _postedCount; }
            set { SetPropertyValue("PostedCount", ref _postedCount, value); }
        }

        [Appearance("PayableTransactionMonitoringSignCodeClose", Enabled = false)]
        public string SignCode
        {
            get { return _signCode; }
            set { SetPropertyValue("SignCode", ref _signCode, value); }
        }

        [Appearance("PayableTransactionMonitoringSalesInvoiceMonitoringEnabled", Enabled = false)]
        public PurchaseInvoiceMonitoring PurchaseInvoiceMonitoring
        {
            get { return _purchaseInvoiceMonitoring; }
            set { SetPropertyValue("PurchaseInvoiceMonitoring", ref _purchaseInvoiceMonitoring, value); }
        }

        #endregion Field

        //private string _PersistentProperty;
        //[XafDisplayName("My display name"), ToolTip("My hint message")]
        //[ModelDefault("EditMask", "(000)-00"), Index(0), VisibleInListView(false)]
        //[Persistent("DatabaseColumnName"), RuleRequiredField(DefaultContexts.Save)]
        //public string PersistentProperty {
        //    get { return _PersistentProperty; }
        //    set { SetPropertyValue("PersistentProperty", ref _PersistentProperty, value); }
        //}

        //[Action(Caption = "My UI Action", ConfirmationMessage = "Are you sure?", ImageName = "Attention", AutoCommit = true)]
        //public void ActionMethod() {
        //    // Trigger a custom business logic for the current record in the UI (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112619.aspx).
        //    this.PersistentProperty = "Paid";
        //}
    }
}