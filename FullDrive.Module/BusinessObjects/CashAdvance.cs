﻿#region Default

using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

#endregion Default

using DevExpress.ExpressApp.ConditionalAppearance;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using System.IO;

namespace FullDrive.Module.BusinessObjects
{
    #region Default

    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("CashAdvanceRuleUnique", DefaultContexts.Save, "Code")]

    #endregion Default

    // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
    public class CashAdvance : FullDriveSysBaseObject
    {
        #region Default

        private GlobalFunction _globFunc;
        private bool _activationPosting;
        private string _code;
        private string _name;
        private Company _company;
        private Division _division;
        private Department _department;
        private Section _section;
        private Employee _employee;
        private Position _position;
        private double _amount;
        private double _amountRealized;
        private double _maxAmountRealized;        
        private double _cashAdvanceTotalAmount;
        private double _cashAdvanceTotalApprovedAmount;
        private double _cashAdvanceTotalUsedAmount;
        private double _cashAdvcanceTotalRemainingAmount;
        private Status _status;
        private DateTime _statusDate;
        private string _userAccess;
        private bool _activeApproved1;
        private bool _activeApproved2;
        private bool _activeApproved3;

        public CashAdvance(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).

            this._globFunc = new GlobalFunction();
            this._code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.CashAdvance);
            this._status = Status.Open;
            this._statusDate = DateTime.Now;

            #region UserAccess

            _userAccess = SecuritySystem.CurrentUserName;
            UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));

            if (Session.IsNewObject(this))
            {
                if (_locUserAccess != null)
                {
                    if (_locUserAccess.Employee != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            Employee = _locUserAccess.Employee;
                        }
                        if (_locUserAccess.Employee.Company != null)
                        {
                            Company = _locUserAccess.Employee.Company;
                        }
                        if (_locUserAccess.Employee.Division != null)
                        {
                            Division = _locUserAccess.Employee.Division;
                        }
                        if (_locUserAccess.Employee.Department != null)
                        {
                            Department = _locUserAccess.Employee.Department;
                        }
                        if (_locUserAccess.Employee.Section != null)
                        {
                            Section = _locUserAccess.Employee.Section;
                        }
                        if (_locUserAccess.Employee.Position != null)
                        {
                            Position = _locUserAccess.Employee.Position;
                        }                        
                    }
                }
            }

            #endregion UserAccess
        }

        #endregion Default

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [ImmediatePostData()]
        [Appearance("CashAdvanceCodeClose", Enabled = false)]
        [Appearance("CashAdvanceRedColor", Criteria = "ActiveApproved1 = true", BackColor = "#fd79a8")]
        [Appearance("CashAdvanceYellowColor", Criteria = "ActiveApproved2 = true", BackColor = "#ffeaa7")]
        [Appearance("CashAdvanceGreenOColor", Criteria = "ActiveApproved3 = true", BackColor = "#81ecec")]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [ImmediatePostData()]
        [Appearance("CashAdvanceNameClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Name
        {
            get
            {
                if (Employee != null)
                {
                    UpdateName();
                }
                return _name;
            }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        [ImmediatePostData()]
        [Appearance("CashAdvanceEmployeeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Employee Employee
        {
            get { return _employee; }
            set
            {
                SetPropertyValue("Employee", ref _employee, value);
                UpdateName();
            }
        }

        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        [Appearance("CashAdvanceCompanyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [DataSourceCriteria("Active = true And Company = '@This.Company'"), ImmediatePostData()]
        [Appearance("CashAdvanceDivisionClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        [DataSourceCriteria("Active = true And Division = '@This.Division'"), ImmediatePostData()]
        [Appearance("CashAdvanceDepartmentClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        [DataSourceCriteria("Active = true And Department = '@This.Department'"), ImmediatePostData()]
        [Appearance("CashAdvanceSectionClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Section Section
        {
            get { return _section; }
            set { SetPropertyValue("Section", ref _section, value); }
        }

        [ImmediatePostData()]
        [Appearance("CashAdvancePositionClose", Enabled = false)]
        public Position Position
        {
            get { return _position; }
            set { SetPropertyValue("Position", ref _position, value); }
        }

        [ImmediatePostData()]
        [Appearance("CashAdvanceAmountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double Amount
        {
            get { return _amount; }
            set {
                SetPropertyValue("Amount", ref _amount, value);
                if(!IsLoading)
                {
                    if(this._amount > 0)
                    {
                        this.AmountRealized = this.Amount;
                    }
                }
            }
        }

        [ImmediatePostData()]
        [Appearance("CashAdvanceAmountRealizedClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double AmountRealized
        {
            get { return _amountRealized; }
            set {
                SetPropertyValue("AmountRealized", ref _amountRealized, value);
                if(!IsLoading)
                {
                    if(this._amountRealized > 0)
                    {
                        if(this._amountRealized > this.Amount )
                        {
                            this._amountRealized = this.Amount;
                        }
                    }
                }
            }
        }

        [ImmediatePostData()]
        [Appearance("CashAdvanceCashAdvanceTotalAmountClose", Enabled = false)]
        public double CashAdvanceTotalAmount
        {
            get
            {
                double _result = 0;
                if (!IsLoading)
                {
                    if (GetTotalAmount() > 0)
                    {
                        _result = UpdateTotalAmount(true);
                    }
                }
                return _result;
            }
        }

        [ImmediatePostData()]
        [Appearance("CashAdvanceCashAdvanceTotalApprovedAmountClose", Enabled = false)]
        public double CashAdvanceTotalApprovedAmount
        {
            get
            {
                double _result = 0;
                if (!IsLoading)
                {
                    if (GetTotalApprovedAmount() > 0)
                    {
                        _result = UpdateTotalApprovedAmount(true);
                    }
                }
                return _result;
            }
        }

        [ImmediatePostData()]
        [Appearance("CashAdvanceCashAdvanceTotalUsedAmountClose", Enabled = false)]
        public double CashAdvanceTotalUsedAmount
        {
            get
            {
                double _result = 0;
                if (!IsLoading)
                {
                    if (GetTotalUsedAmount() > 0)
                    {
                        _result = UpdateTotalUsedAmount(true);
                    }
                }
                return _result;
            }
        }

        [ImmediatePostData()]
        [Appearance("CashAdvanceCashAdvanceTotalRemainingAmountClose", Enabled = false)]
        public double CashAdvanceTotalRemainingAmount
        {
            get
            {
                double _result = 0;
                if (!IsLoading)
                {
                    if (GetTotalRemainingAmount() > 0)
                    {
                        _result = UpdateTotalRemainingAmount(true);
                    }
                }
                return _result;
            }
        }

        //[Appearance("CashAdvanceStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("CashAdvanceStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Association("CashAdvance-CashAdvanceLines")]
        public XPCollection<CashAdvanceLine> CashAdvanceLines
        {
            get { return GetCollection<CashAdvanceLine>("CashAdvanceLines"); }
        }

        [Association("CashAdvance-ApprovalLines")]
        public XPCollection<ApprovalLine> ApprovalLines
        {
            get { return GetCollection<ApprovalLine>("ApprovalLines"); }
        }

        //clm
        [Browsable(false)]
        [Appearance("CashAdvanceActiveApproved1Close", Enabled = false)]
        public bool ActiveApproved1
        {
            get { return _activeApproved1; }
            set { SetPropertyValue("ActiveApproved1", ref _activeApproved1, value); }
        }

        [Browsable(false)]
        [Appearance("CashAdvanceActiveApproved2Close", Enabled = false)]
        public bool ActiveApproved2
        {
            get { return _activeApproved2; }
            set { SetPropertyValue("ActiveApproved2", ref _activeApproved2, value); }
        }

        [Browsable(false)]
        [Appearance("CashAdvanceActiveApproved3Close", Enabled = false)]
        public bool ActiveApproved3
        {
            get { return _activeApproved3; }
            set { SetPropertyValue("ActiveApproved3", ref _activeApproved3, value); }
        }

        #endregion Field

        //==== Code Only ====

        private void UpdateName()
        {
            try
            {
                this._name = string.Format("Cash Advance {0} [ {1} ]", Code, Employee.Name);
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = CashAdvance" + ex.ToString());
            }
        }

        #region GetTotalAmount

        public int GetTotalAmount()
        {
            int _result = 0;
            try
            {
                XPCollection<CashAdvanceLine> _locCashAdvanceLines = new XPCollection<CashAdvanceLine>
                                                                     (Session, new GroupOperator(GroupOperatorType.And,
                                                                      new BinaryOperator("CashAdvance", this)));

                if (_locCashAdvanceLines != null && _locCashAdvanceLines.Count > 0)
                {
                    _result = _locCashAdvanceLines.Count();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = CashAdvance", ex.ToString());
            }
            return _result;
        }

        public double UpdateTotalAmount(bool forceChangeEvents)
        {
            double _result = 0;
            try
            {
                double _locTotalAmount = 0;
                if (!IsLoading)
                {
                    XPCollection<CashAdvanceLine> _locCashAdvances = new XPCollection<CashAdvanceLine>
                                                                     (Session, new GroupOperator(GroupOperatorType.And,
                                                                      new BinaryOperator("CashAdvance", this)));

                    if (_locCashAdvances != null && _locCashAdvances.Count() > 0)
                    {
                        foreach (CashAdvanceLine _locCashAdvance in _locCashAdvances)
                        {
                            _locTotalAmount = _locTotalAmount + _locCashAdvance.Amount;
                        }
                        _result = _locTotalAmount;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CashAdvance " + ex.ToString());
            }
            return _result;
        }

        public double GetSumTotalAmount()
        {
            double _result = 0;
            try
            {
                double _locSumTotalAmount = 0;

                if (!IsLoading)
                {
                    if (this.Code != null)
                    {
                        XPCollection<CashAdvance> _locCashAdvances = new XPCollection<CashAdvance>
                                                                     (Session, new GroupOperator(GroupOperatorType.And,
                                                                      new BinaryOperator("CashAdvance", this)));

                        if (_locCashAdvances != null && _locCashAdvances.Count > 0)
                        {
                            foreach (CashAdvance _locCashAdvance in _locCashAdvances)
                            {
                                _locSumTotalAmount = _locSumTotalAmount + _locCashAdvance.CashAdvanceTotalAmount;
                            }
                        }
                        _result = _locSumTotalAmount;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = CashAdvance", ex.ToString());
            }
            return _result;
        }

        #endregion GetTotalAmount

        #region GetTotalApprovedAmount

        public int GetTotalApprovedAmount()
        {
            int _result = 0;
            try
            {
                XPCollection<CashAdvanceLine> _locCashAdvanceLines = new XPCollection<CashAdvanceLine>
                                                                     (Session, new GroupOperator(GroupOperatorType.And,
                                                                      new BinaryOperator("CashAdvance", this)));

                if (_locCashAdvanceLines != null && _locCashAdvanceLines.Count > 0)
                {
                    _result = _locCashAdvanceLines.Count();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = CashAdvance", ex.ToString());
            }
            return _result;
        }

        public double UpdateTotalApprovedAmount(bool forceChangeEvents)
        {
            double _result = 0;
            try
            {
                double _locTotalApprovedAmount = 0;
                if (!IsLoading)
                {
                    XPCollection<CashAdvanceLine> _locCashAdvances = new XPCollection<CashAdvanceLine>
                                                                     (Session, new GroupOperator(GroupOperatorType.And,
                                                                      new BinaryOperator("CashAdvance", this)));

                    if (_locCashAdvances != null && _locCashAdvances.Count() > 0)
                    {
                        foreach (CashAdvanceLine _locCashAdvance in _locCashAdvances)
                        {
                            //_locTotalApprovedAmount = _locTotalApprovedAmount + _locCashAdvance.ApprovedAmount;
                        }
                        _result = _locTotalApprovedAmount;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CashAdvance " + ex.ToString());
            }
            return _result;
        }

        public double GetSumTotalApprovedAmount()
        {
            double _result = 0;
            try
            {
                double _locSumTotalApprovedAmount = 0;

                if (!IsLoading)
                {
                    if (this.Code != null)
                    {
                        XPCollection<CashAdvance> _locCashAdvances = new XPCollection<CashAdvance>
                                                                     (Session, new GroupOperator(GroupOperatorType.And,
                                                                      new BinaryOperator("CashAdvance", this)));

                        if (_locCashAdvances != null && _locCashAdvances.Count > 0)
                        {
                            foreach (CashAdvance _locCashAdvance in _locCashAdvances)
                            {
                                _locSumTotalApprovedAmount = _locSumTotalApprovedAmount + _locCashAdvance.CashAdvanceTotalApprovedAmount;
                            }
                        }
                        _result = _locSumTotalApprovedAmount;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = CashAdvance", ex.ToString());
            }
            return _result;
        }

        #endregion GetTotalApprovedAmount

        #region GetTotalUsedAmount

        public int GetTotalUsedAmount()
        {
            int _result = 0;
            try
            {
                XPCollection<PaymentRealization> _locPaymentRealizations = new XPCollection<PaymentRealization>
                                                                           (Session, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("CashAdvance", this)));

                if (_locPaymentRealizations != null && _locPaymentRealizations.Count > 0)
                {
                    _result = _locPaymentRealizations.Count();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = CashAdvance", ex.ToString());
            }
            return _result;
        }

        public double UpdateTotalUsedAmount(bool forceChangeEvents)
        {
            double _result = 0;
            try
            {
                double _locTotalUsedAmount = 0;
                if (!IsLoading)
                {
                    XPCollection<PaymentRealization> _locPaymentRealizations = new XPCollection<PaymentRealization>
                                                                 (Session, new GroupOperator(GroupOperatorType.And,
                                                                  new BinaryOperator("CashAdvance", this)));

                    if (_locPaymentRealizations != null && _locPaymentRealizations.Count() > 0)
                    {
                        foreach (PaymentRealization _locPaymentRealization in _locPaymentRealizations)
                        {
                            _locTotalUsedAmount = _locPaymentRealization.TotalUsedAmount;
                        }
                        _result = _locTotalUsedAmount;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CashAdvance " + ex.ToString());
            }
            return _result;
        }

        #endregion GetTotalUsedAmount

        #region GetTotalRemainingAmount

        public int GetTotalRemainingAmount()
        {
            int _result = 0;
            try
            {
                XPCollection<PaymentRealization> _locPaymentRealizations = new XPCollection<PaymentRealization>
                                                             (Session, new GroupOperator(GroupOperatorType.And,
                                                              new BinaryOperator("CashAdvance", this)));

                if (_locPaymentRealizations != null && _locPaymentRealizations.Count > 0)
                {
                    _result = _locPaymentRealizations.Count();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = CashAdvance", ex.ToString());
            }
            return _result;
        }

        public double UpdateTotalRemainingAmount(bool forceChangeEvents)
        {
            double _result = 0;
            try
            {
                double _locTotalRemainingAmount = 0;
                if (!IsLoading)
                {
                    XPCollection<PaymentRealization> _locPaymentRealizations = new XPCollection<PaymentRealization>
                                                                 (Session, new GroupOperator(GroupOperatorType.And,
                                                                  new BinaryOperator("CashAdvance", this)));

                    if (_locPaymentRealizations != null && _locPaymentRealizations.Count() > 0)
                    {
                        foreach (PaymentRealization _locPaymentRealization in _locPaymentRealizations)
                        {
                            _locTotalRemainingAmount = _locPaymentRealization.TotalRemainingAmount;
                        }
                        _result = _locTotalRemainingAmount;
                    }

                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CashAdvance " + ex.ToString());
            }
            return _result;
        }

        #endregion GetTotalRemainingAmount

    }
}