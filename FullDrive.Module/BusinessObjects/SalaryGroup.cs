﻿        #region Default
using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using DevExpress.ExpressApp.Xpo;
using DevExpress.ExpressApp.ConditionalAppearance;
using System.Collections;
using FullDrive.Module.CustomProcess;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    [NavigationItem("Payroll")]
    [RuleCombinationOfPropertiesIsUnique("SalaryGroupRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class SalaryGroup : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private string _code;
        private string _name;
        private double _salaryTestValue;
        private string _salaryTestValueText;
        private GlobalFunction _globFunc;
        private SalaryGroupDetail _salaryGroupDetail;
        private Employee _employee;
        public SalaryGroup(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.SalaryGroup);
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }
        #endregion 

        #region Field
        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        public double SalaryTestValue
        {
            get { return _salaryTestValue; }
            set { SetPropertyValue("SalaryTestValue", ref _salaryTestValue, value); }
        }

        public string SalaryTestValueText
        {
            get { return _salaryTestValueText; }
            set { SetPropertyValue("SalaryTestValueText", ref _salaryTestValueText, value); }
        }

        [Association("SalaryGroup-Employees")]
        public XPCollection<Employee> Employees
        {
            get { return GetCollection<Employee>("Employees"); }
        }

        [Association("SalaryGroup-SalaryGroupDetails")]
        public XPCollection<SalaryGroupDetail> SalaryGroupDetails
        {
            get { return GetCollection<SalaryGroupDetail>("SalaryGroupDetails"); }
        }
        #endregion Field

        #region Calculate Salary Test value
        [Action(ToolTip = "Only Test for counting Salary Value", Caption = "Test Process")]
        public void CalculateSalaryTestValue()
        {
            this.SalaryTestValue = 0;
            foreach (SalaryGroupDetail PayProcDetRec in this.SalaryGroupDetails)
            {
                {
                    var withBlock = PayProcDetRec;
                    withBlock.Calculate();
                    if (withBlock.Component.SalaryType == SalaryType.Deduction)
                        this.SalaryTestValue -= withBlock.ComponentValue;
                    else
                        this.SalaryTestValue += withBlock.ComponentValue;
                }
            }
        }
        #endregion

        #region Get New Code And Name
        private void GetNewCodeAndName(ref string NewSalaryGroupCode, ref string NewSalaryGroupName)
        {
            UInt16 _CountNewCode = 0;
            SalaryGroup tSalaryGroup = Session.FindObject<SalaryGroup>(new GroupOperator(GroupOperatorType.And, 
                                       new BinaryOperator("Code", NewSalaryGroupCode),
                                       new BinaryOperator("Name", NewSalaryGroupName)));
            if (tSalaryGroup ==  null)
                return;
            else
            {
                // recursive
                _CountNewCode += 1;
                NewSalaryGroupCode = this.Code + "Copy" + _CountNewCode.ToString();
                NewSalaryGroupName = this.Name + " Copy" + _CountNewCode.ToString();
                GetNewCodeAndName(ref NewSalaryGroupCode, ref NewSalaryGroupName);
            }
        }

        #endregion

        //#region Save As
        //[Action(ToolTip = "Save as this Group to new salary Group", Caption = "Save As")]
        //public void SaveAs()
        //{
        //    string NewSalaryGroupCode = this.Code + "Copy";
        //    string NewSalaryGroupName = this.Name + " Copy";
        //    GetNewCodeAndName(ref NewSalaryGroupCode, ref NewSalaryGroupName);

        //    var NewSession = new Session();
        //    NewSession = Session;
        //    NewSession.BeginTransaction();

        //    var NewSalaryGroup = new SalaryGroup(NewSession);
        //    {
        //        var withBlock = NewSalaryGroup;
        //        withBlock.Code = NewSalaryGroupCode;
        //        withBlock.Name = NewSalaryGroupName;
        //        withBlock.Save();
        //    }
        //    foreach (SalaryGroupDetail SalGroupDetRec in this.SalaryGroupDetails)
        //    {
        //        {
        //            var withBlock1 = SalGroupDetRec;
        //            var NewSalGroupDetRec = new SalaryGroupDetail(NewSession);
        //            {
        //                var withBlock2 = NewSalGroupDetRec;
        //                withBlock2.Component = SalGroupDetRec.Component;
        //                withBlock2.ComponentFormula = SalGroupDetRec.ComponentFormula;
        //                withBlock2.ComponentValue = SalGroupDetRec.ComponentValue;
        //                withBlock2.SalaryGroup = NewSalaryGroup;
        //                withBlock2.Save();
        //            }
        //            if (SalGroupDetRec.SalaryGroupDetails != null)
        //                // recursive
        //                SaveAsChildren(ref NewSession, ref NewSalGroupDetRec, ref SalGroupDetRec.sal); // , NewSalaryGroup)
        //        }
        //    }
        //    NewSession.CommitTransaction();
        //    //MsgBox[string.Format("Salary Group with Code : {0} " + Constants.vbCrLf + "and Name : {1} was successfully created", NewSalaryGroupCode, NewSalaryGroupName), Microsoft.VisualBasic.MsgBoxStyle.Information];
        //}
        //#endregion

        //#region Save As Children
        //private void SaveAsChildren(ref Session NewSession, ref SalaryGroupDetail NewSalaryGroup, ref object SalaryGroupDetails)
        //{
        //    foreach (SalaryGroupDetail SalGroupDetRec in this.SalaryGroupDetails)
        //    {
        //        {
        //            var withBlock = SalGroupDetRec;
        //            var NewSalGroupDetRec = new SalaryGroupDetail(NewSession);
        //            {
        //                var withBlock1 = NewSalGroupDetRec;
        //                withBlock1.Component = SalGroupDetRec.Component;
        //                withBlock1.ComponentFormula = SalGroupDetRec.ComponentFormula;
        //                withBlock1.ComponentValue = SalGroupDetRec.ComponentValue;
        //                withBlock1.SalaryGroupDetails = NewSalaryGroup;
        //                withBlock1.Save();
        //            }
        //            if (SalGroupDetRec.SalaryGroupDetails != null)
        //                // recursive
        //                SaveAsChildren(ref NewSession, ref NewSalGroupDetRec, ref SalGroupDetRec.SalaryGroupDetails);
        //        }
        //    }
        //}
        //#endregion

        //#region Process To Payroll
        //[Action(ToolTip = "Send to Process Payroll for current Period based on Employee Details", Caption = "Process")]
        //public void ProcessToPayroll()
        //{
        //    Period CurrPeriodRec = Session.FindObject<Period>(new BinaryOperator("Active", true));
        //    if (CurrPeriodRec == null)
        //    {
        //        //MsgBox["Process aborted, there is no active period"];
        //        return;
        //    }
        //    // If IsNothing(Me.AppSetting.Period) Then
        //    // MsgBox("Process aborted, there is no active period")
        //    // Exit Sub
        //    // End If
        //    foreach (Employee EmpRec in this.Employees)
        //    {
        //        PayrollProcess PayProcRec = Session.FindObject<PayrollProcess>(new GroupOperator(GroupOperatorType.And,
        //                                    new BinaryOperator("Period", CurrPeriodRec),
        //                                    new BinaryOperator("Employee", EmpRec)));
        //        if (PayProcRec == null)
        //            PayProcRec = new PayrollProcess(Session);
        //        {
        //            var withBlock = PayProcRec;
        //            withBlock.Period = CurrPeriodRec;
        //            withBlock.Employee = EmpRec;
        //            // .SalaryFormula = Me.SalaryFormula
        //            withBlock.SalaryBasic = 0;
        //            withBlock.SalaryProcessValue = 0;
        //            withBlock.Save();
        //        }
        //        ProcessDetail(this.SalaryGroupDetails, PayProcRec);
        //    }
        //}
        //#endregion

        //#region Process Detail
        //private void ProcessDetail(object ObjDetail, object ObjParent)
        //{
        //    foreach (SalaryGroupDetail SalGroupRec in ObjDetail)
        //    {
        //        PayrollProcessDetail PayProcDetRec = Session.FindObject<PayrollProcessDetail>
        //                                             (new BinaryOperator("Component", SalGroupRec.Component));
        //        if (PayProcDetRec == null)
        //            PayProcDetRec = new PayrollProcessDetail(Session);
        //        {
        //            var withBlock = PayProcDetRec;
        //            if (ObjParent is PayrollProcess)
        //                withBlock.PayrollProcess = ObjParent;
        //            else
        //                withBlock.PayrollProcessDetail = ObjParent;

        //            withBlock.Component = SalGroupRec.Component;
        //            withBlock.ComponentFormula = SalGroupRec.ComponentFormula;
        //            withBlock.ComponentValue = SalGroupRec.ComponentValue;
        //            withBlock.Save();
        //        }
        //        if (SalGroupRec.SalaryGroupDetails != null)
        //            ProcessDetail(SalGroupRec.SalaryGroupDetails, PayProcDetRec);
        //    }
        //}
        //#endregion


        //
    }
}