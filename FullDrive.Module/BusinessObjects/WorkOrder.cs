﻿#region Default

using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

#endregion Default

using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.SystemModule;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Editors;

namespace FullDrive.Module.BusinessObjects
{
    #region Default

    [DefaultClassOptions]
    [NavigationItem("PPIC")]
    [DefaultProperty("Code")]
    [RuleCombinationOfPropertiesIsUnique("WorkOrderRuleUnique", DefaultContexts.Save, "Code")]

    #endregion Default

    // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
    public class WorkOrder : FullDriveSysBaseObject
    {
        #region Default

        private bool _activationPosting;
        private string _code;
        private string _name;
        private string _description;
        private bool _collection;
        private DateTime _startDate;
        private DateTime _endDate;
        private DateTime _estimatedDate;
        private Status _status;
        private DateTime _statusDate;
        private Status _materialStatus;
        private DateTime _materialStatusDate;
        private MachineMapVersion _machineMapVersion;
        private WorkRequisition _workRequisition;
        private string _signCode;
        private Company _company;
        private string _userAccess;
        private GlobalFunction _globFunc;
        //clm
        private bool _activeApproved1;
        private bool _activeApproved2;
        private bool _activeApproved3;

        public WorkOrder(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.WorkOrder);
                DateTime now = DateTime.Now;
                this.Status = CustomProcess.Status.Open;
                this.StatusDate = now;

                #region UserAccess
                _userAccess = SecuritySystem.CurrentUserName;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                if (Session.IsNewObject(this))
                {
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null)
                            {
                                Company = _locUserAccess.Employee.Company;
                            }
                        }
                    }
                }
                #endregion UserAccess
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #endregion Default

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [ImmediatePostData()]
        [Appearance("WorkOrderCodeClose", Enabled = false)]
        [Appearance("WorkOrderRedColor", Criteria = "ActiveApproved1 = true", BackColor = "#fd79a8")]
        [Appearance("WorkOrderYellowColor", Criteria = "ActiveApproved2 = true", BackColor = "#ffeaa7")]
        [Appearance("WorkOrderGreenOColor", Criteria = "ActiveApproved3 = true", BackColor = "#81ecec")]
        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("WorkOrderCodeEnabled", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("WorkOrderNameClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        [Size(512)]
        [Appearance("WorkOrderDescriptionClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        [ImmediatePostData()]
        [Appearance("WorkOrderCollectionClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public bool Collection
        {
            get { return _collection; }
            set { SetPropertyValue("Collection", ref _collection, value); }
        }

        [Appearance("WorkOrderStartDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime StartDate
        {
            get { return _startDate; }
            set { SetPropertyValue("StartDate", ref _startDate, value); }
        }

        [Appearance("WorkOrderEndDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime EndDate
        {
            get { return _endDate; }
            set { SetPropertyValue("EndDate", ref _endDate, value); }
        }

        [Appearance("WorkOrderEstimatedDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime EstimatedDate
        {
            get { return _estimatedDate; }
            set { SetPropertyValue("EstimatedDate", ref _estimatedDate, value); }
        }

        [Appearance("WorkOrderStatusEnabled", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("WorkOrderStatusDateEnabled", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("WorkOrderMaterialMaterialStatusEnabled", Enabled = false)]
        public Status MaterialStatus
        {
            get { return _materialStatus; }
            set { SetPropertyValue("MaterialStatus", ref _materialStatus, value); }
        }

        [Appearance("WorkOrderMaterialMaterialStatusDateEnabled", Enabled = false)]
        public DateTime MaterialStatusDate
        {
            get { return _materialStatusDate; }
            set { SetPropertyValue("MaterialStatusDate", ref _materialStatusDate, value); }
        }

        [Appearance("WorkOrderMachineMapClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public MachineMapVersion MachineMapVersion
        {
            get { return _machineMapVersion; }
            set { SetPropertyValue("MachineMapVersion", ref _machineMapVersion, value); }
        }

        [Appearance("WorkOrderWorkRequisitionEnabled", Enabled = false)]
        public WorkRequisition WorkRequisition
        {
            get { return _workRequisition; }
            set { SetPropertyValue("WorkRequisition", ref _workRequisition, value); }
        }

        [Appearance("PurchaseOrderSignCodeClose", Enabled = false)]
        public string SignCode
        {
            get { return _signCode; }
            set { SetPropertyValue("SignCode", ref _signCode, value); }
        }
        
        [Appearance("WorkOrderCompanyEnabled", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Association("WorkOrder-WorkOrderLines")]
        public XPCollection<WorkOrderLine> WorkOrderLines
        {
            get { return GetCollection<WorkOrderLine>("WorkOrderLines"); }
        }

        [Association("WorkOrder-ApprovalLines")]
        public XPCollection<ApprovalLine> ApprovalLines
        {
            get { return GetCollection<ApprovalLine>("ApprovalLines"); }
        }

        [Association("WorkOrder-WorkOrderMaterials")]
        public XPCollection<WorkOrderMaterial> WorkOrderMaterials
        {
            get { return GetCollection<WorkOrderMaterial>("WorkOrderMaterials"); }
        }

        //Wahab 01-03-2020
        //[Association("WorkOrder-Productions")]
        //public XPCollection<Production> Productions
        //{
        //    get { return GetCollection<Production>("Productions"); }
        //}

        //[Association("WorkOrder-Consumptions")]
        //public XPCollection<Consumption> Consumptions
        //{
        //    get { return GetCollection<Consumption>("Consumptions"); }
        //}

        //[Association("WorkOrder-OutputProductions")]
        //public XPCollection<OutputProduction> OutputProductions
        //{
        //    get { return GetCollection<OutputProduction>("OutputProductions"); }
        //}

        //[Association("WorkOrder-FinishProductions")]
        //public XPCollection<FinishProduction> FinishProductions
        //{
        //    get { return GetCollection<FinishProduction>("FinishProductions"); }
        //}

        //clm
        [Browsable(false)]
        [Appearance("WorkOrderActiveApproved1Close", Enabled = false)]
        public bool ActiveApproved1
        {
            get { return _activeApproved1; }
            set { SetPropertyValue("ActiveApproved1", ref _activeApproved1, value); }
        }

        [Browsable(false)]
        [Appearance("WorkOrderActiveApproved2Close", Enabled = false)]
        public bool ActiveApproved2
        {
            get { return _activeApproved2; }
            set { SetPropertyValue("ActiveApproved2", ref _activeApproved2, value); }
        }

        [Browsable(false)]
        [Appearance("WorkOrderActiveApproved3Close", Enabled = false)]
        public bool ActiveApproved3
        {
            get { return _activeApproved3; }
            set { SetPropertyValue("ActiveApproved3", ref _activeApproved3, value); }
        }

        #endregion Field

    }
}