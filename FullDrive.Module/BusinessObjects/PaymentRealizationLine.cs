﻿#region Default

using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

#endregion Default

using DevExpress.ExpressApp.ConditionalAppearance;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using System.IO;

namespace FullDrive.Module.BusinessObjects
{
    #region Default

    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("PaymentRealizationLineRuleUnique", DefaultContexts.Save, "Code")]

    #endregion Default

    // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
    public class PaymentRealizationLine : FullDriveSysBaseObject
    {
        #region Default

        private bool _activationPosting;
        private string _code;
        private bool _select;
        private string _name;
        private CashAdvanceType _cashAdvanceType;
        private Employee _employee;
        private FileData _attachment;
        private double _amount;
        private double _amountRealized;
        private DateTime _uploadDate;
        private string _description;
        private Status _status;
        private DateTime _statusDate;
        private int _processCount;
        private Company _company;
        private Division _division;
        private Department _department;
        private Section _section;
        private CashAdvance _cashAdvance;
        private CashAdvanceMonitoring _cashAdvanceMonitoring;
        private PaymentRealization _paymentRealization;
        private GlobalFunction _globFunc;

        public PaymentRealizationLine(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).

            this._globFunc = new GlobalFunction();
            this._code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.PaymentRealizationLine);
            this.Select = true;
            this._status = Status.Open;
            this._statusDate = DateTime.Now;
        }

        #endregion Default

        #region Field 

        //[Browsable(false)]
        [ImmediatePostData()]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("PaymentRealizationLineCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("PaymentRealizationLineSelectClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public bool Select
        {
            get { return _select; }
            set { SetPropertyValue("Select", ref _select, value); }
        }

        [Appearance("PaymentRealizationLineNameClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        [Appearance("PaymentRealizationLineCashAdvanceTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public CashAdvanceType CashAdvanceType
        {
            get { return _cashAdvanceType; }
            set { SetPropertyValue("CashAdvanceType", ref _cashAdvanceType, value); }
        }

        [Appearance("PaymentRealizationLineEmployeeClose", Enabled = false)]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue("Employee", ref _employee, value); }
        }

        [ImmediatePostData()]
        [Appearance("PaymentRealizationLineAttachmentRealizationClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public FileData Attachment
        {
            get { return _attachment; }
            set {
                SetPropertyValue("Attachment", ref _attachment, value);
                if(!IsLoading)
                {
                    if(this._attachment != null)
                    {
                        this.UploadDate = DateTime.Now;
                    }
                }
            }
        }

        [Appearance("PaymentRealizationLineAmountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double Amount
        {
            get { return _amount; }
            set { SetPropertyValue("Amount", ref _amount, value); }
        }

        [ImmediatePostData()]
        [Appearance("PaymentRealizationLineAmountRealizedClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double AmountRealized
        {
            get { return _amountRealized; }
            set { SetPropertyValue("AmountRealized", ref _amountRealized, value); }
        }

        [Appearance("PaymentRealizationLineUploadDateClose", Enabled = false)]
        public DateTime UploadDate
        {
            get { return _uploadDate; }
            set { SetPropertyValue("UploadDate", ref _uploadDate, value); }
        }

        [Size(512)]
        [Appearance("PaymentRealizationLineNotesClose", Criteria = "ActivationPosting = True", Enabled = false)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        //[Appearance("PaymentRealizationLineStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("PaymentRealizationLineStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("PaymentRealizationLineProcessCountEnabled", Enabled = false)]
        public int ProcessCount
        {
            get { return _processCount; }
            set { SetPropertyValue("ProcessCount", ref _processCount, value); }
        }

        [Browsable(false)]
        [Appearance("PaymentRealizationLineCompanyEnabled", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Browsable(false)]
        [Appearance("PaymentRealizationLineDivisionEnabled", Enabled = false)]
        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        [Browsable(false)]
        [Appearance("PaymentRealizationLineDepartmentEnabled", Enabled = false)]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        [Browsable(false)]
        [Appearance("PaymentRealizationLineSectionEnabled", Enabled = false)]
        public Section Section
        {
            get { return _section; }
            set { SetPropertyValue("Section", ref _section, value); }
        }

        [Appearance("PaymentRealizationLineCashAdvanceClose", Enabled = false)]
        public CashAdvance CashAdvance
        {
            get { return _cashAdvance; }
            set { SetPropertyValue("CashAdvance", ref _cashAdvance, value); }
        }

        [Appearance("PaymentRealizationLineCashAdvanceMonitoringEnabled", Enabled = false)]
        public CashAdvanceMonitoring CashAdvanceMonitoring
        {
            get { return _cashAdvanceMonitoring; }
            set { SetPropertyValue("CashAdvanceMonitoring", ref _cashAdvanceMonitoring, value); }
        }

        [Association("PaymentRealization-PaymentRealizationLines")]
        [Appearance("PaymentRealizationLinePaymentRealizationClose", Enabled = false)]
        public PaymentRealization PaymentRealization
        {
            get { return _paymentRealization; }
            set
            {
                SetPropertyValue("PaymentRealization", ref _paymentRealization, value);
                if (this._paymentRealization != null)
                {
                    if(this._paymentRealization.Employee != null)
                    {
                        this.Employee = this._paymentRealization.Employee;
                    }
                    if(this._paymentRealization.Division != null)
                    {
                        this.Division = this._paymentRealization.Division;
                    }
                    if(this._paymentRealization.Department != null)
                    {
                        this.Department = this._paymentRealization.Department;
                    }
                    if(this._paymentRealization.Section != null)
                    {
                        this.Section = this._paymentRealization.Section;
                    }
                    if(this._paymentRealization.Company != null)
                    {
                        this.Company = this._paymentRealization.Company;
                    }  
                }
            }
        }

        #endregion Field

        //================================================ Code Only =======================================================

        #region UpdateName

        private void UpdateName()
        {
            try
            {
                this._name = string.Format("PaymentRealizationLine [ {0} ]", Employee.Name);
            }

            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PaymentRealizationLine" + ex.ToString());
            }
        }

        #endregion UpdateName

    }
}