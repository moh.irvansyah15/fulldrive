﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.ConditionalAppearance;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Editors;


namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Inventory")]
    [RuleCombinationOfPropertiesIsUnique("InventoryTransferInMonitoringRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class InventoryTransferInMonitoring : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private string _code;
        private bool _select;
        private InventoryTransferIn _inventoryTransferIn;
        private InventoryTransferInLine _inventoryTransferInLine;
        private PurchaseInvoice _purchaseInvoice;
        private PurchaseInvoiceLine _purchaseInvoiceLine;
        private PurchaseReturn _purchaseReturn;
        private PurchaseReturnLine _purchaseReturnLine;
        private OrderType _purchaseType;
        private XPCollection<Item> _availableItem;
        private Item _item;
        #region InisialisasiDefaultQty
        private double _dQty;
        private UnitOfMeasure _dUom;
        private double _qty;
        private UnitOfMeasure _uom;
        private double _tQty;
        private GlobalFunction _globFunc;
        #endregion InisialisasiDefaultQty
        private Status _status;
        private DateTime _statusDate;
        private Status _returnStatus;
        private DateTime _returnStatusDate;
        private int _postedCount;
        private ReturnType _returnType;
        private PurchaseOrderMonitoring _purchaseOrderMonitoring;

        public InventoryTransferInMonitoring(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.InventoryTransferInMonitoring);
                this.Select = true;
                this.Status = Status.Open;
                this.StatusDate = now;
            }
                
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        [Appearance("InventoryTranferInMonitoringCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        public bool Select
        {
            get { return _select; }
            set { SetPropertyValue("Select", ref _select, value); }
        }

        [Appearance("InventoryTransferInMonitoringInventoryTransferInClose", Enabled = false)]
        public InventoryTransferIn InventoryTransferIn
        {
            get { return _inventoryTransferIn; }
            set { SetPropertyValue("InventoryTransferIn", ref _inventoryTransferIn, value); }
        }

        [Appearance("InventoryTransferInMonitoringInventoryTransferInLineClose", Enabled = false)]
        public InventoryTransferInLine InventoryTransferInLine
        {
            get { return _inventoryTransferInLine; }
            set { SetPropertyValue("InventoryTransferInLine", ref _inventoryTransferInLine, value); }
        }

        //[Appearance("InventoryTransferInMonitoringPurchaseInvoiceClose", Enabled = false)]
        public PurchaseInvoice PurchaseInvoice
        {
            get { return _purchaseInvoice; }
            set { SetPropertyValue("PurchaseInvoice", ref _purchaseInvoice, value); }
        }

        //[Appearance("InventoryTransferInMonitoringPurchaseInvoiceLineClose", Enabled = false)]
        public PurchaseInvoiceLine PurchaseInvoiceLine
        {
            get { return _purchaseInvoiceLine; }
            set { SetPropertyValue("PurchaseInvoiceLine", ref _purchaseInvoiceLine, value); }
        }

        [Appearance("InventoryTransferInMonitoringPurchaseReturnClose", Enabled = false)]
        public PurchaseReturn PurchaseReturn
        {
            get { return _purchaseReturn; }
            set { SetPropertyValue("PurchaseReturn", ref _purchaseReturn, value); }
        }

        [Appearance("InventoryTransferInMonitoringPurchaseReturnLineClose", Enabled = false)]
        public PurchaseReturnLine PurchaseReturnLine
        {
            get { return _purchaseReturnLine; }
            set { SetPropertyValue("PurchaseReturnLine", ref _purchaseReturnLine, value); }
        }

        [ImmediatePostData()]
        [Appearance("InventoryTransferInMonitoringPurchaseTypeClose", Enabled = false)]
        public OrderType PurchaseType
        {
            get { return _purchaseType; }
            set { SetPropertyValue("PurchaseType", ref _purchaseType, value); }
        }

        [Browsable(false)]
        public XPCollection<Item> AvailableItem
        {
            get
            {
                if (PurchaseType == OrderType.Item)
                {
                    _availableItem = new XPCollection<Item>(Session,
                                    new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("OrderType", OrderType.Item)));

                }
                else if (PurchaseType == OrderType.Service)
                {
                    _availableItem = new XPCollection<Item>(Session,
                                    new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("OrderType", OrderType.Service)));
                }
                else
                {
                    _availableItem = new XPCollection<Item>(Session);
                }

                return _availableItem;

            }
        }

        [DataSourceProperty("AvailableItem", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("InventoryTransferInMonitoringItemClose", Enabled = false)]
        public Item Item
        {
            get { return _item; }
            set { SetPropertyValue("Item", ref _item, value); }
        }

        #region DefaultQty

        [Appearance("InventoryTransferInMonitoringDQtyClose", Enabled = false)]
        [ImmediatePostData()]
        public double DQty
        {
            get { return _dQty; }
            set { SetPropertyValue("DQty", ref _dQty, value); }
        }

        [Appearance("InventoryTransferInMonitoringDUOMClose", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure DUOM
        {
            get { return _dUom; }
            set { SetPropertyValue("DUOM", ref _dUom, value); }
        }

        [Appearance("InventoryTransferInMonitoringQtyClose", Enabled = false)]
        [ImmediatePostData()]
        public double Qty
        {
            get { return _qty; }
            set { SetPropertyValue("Qty", ref _qty, value); }
        }

        [Appearance("InventoryTransferInMonitoringUOMClose", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure UOM
        {
            get { return _uom; }
            set { SetPropertyValue("UOM", ref _uom, value); }
        }

        [Appearance("InventoryTransferInMonitoringTotalQtyEnabled", Enabled = false)]
        public double TQty
        {
            get { return _tQty; }
            set { SetPropertyValue("TotalQty", ref _tQty, value); }
        }

        #endregion DefaultQty

        //[Appearance("InventoryTransferInMonitoringStatusEnabled", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("InventoryTransferInMonitoringStatusDateEnabled", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("InventoryTransferInMonitoringReturnStatusEnabled", Enabled = false)]
        public Status ReturnStatus
        {
            get { return _returnStatus; }
            set { SetPropertyValue("ReturnStatus", ref _returnStatus, value); }
        }

        [Appearance("InventoryTransferInMonitoringReturnStatusDateEnabled", Enabled = false)]
        public DateTime ReturnStatusDate
        {
            get { return _returnStatusDate; }
            set { SetPropertyValue("ReturnStatusDate", ref _returnStatusDate, value); }
        }

        [Appearance("InventoryTranferInMonitoringPostedCountEnabled", Enabled = false)]
        public int PostedCount
        {
            get { return _postedCount; }
            set { SetPropertyValue("PostedCount", ref _postedCount, value); }
        }

        [Appearance("InventoryTransferInMonitoringReturnTypeEnabled", Enabled = false)]
        public ReturnType ReturnType
        {
            get { return _returnType; }
            set { SetPropertyValue("ReturnType", ref _returnType, value); }
        }

        [Browsable(false)]
        public PurchaseOrderMonitoring PurchaseOrderMonitoring
        {
            get { return _purchaseOrderMonitoring; }
            set { SetPropertyValue("PurchaseOrderMonitoring", ref _purchaseOrderMonitoring, value); }
        }

       
    }
}