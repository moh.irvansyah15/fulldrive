﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using DevExpress.ExpressApp.Editors;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("ShipmentSalesInvoiceRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class ShipmentSalesInvoice : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).

        private bool _activationPosting;
        private bool _activeApproved1;
        private bool _activeApproved2;
        private bool _activeApproved3;
        private string _code;
        private ShipmentType _shipmentType;
        #region VariableFrom
        private Country _countryFrom;
        private City _cityFrom;
        private XPCollection<BusinessPartner> _availableShipper;
        private BusinessPartner _shipper;
        private string _shipperContact;
        private string _shipperAddress;
        #endregion VariableFrom
        #region VariableTo
        private Country _countryTo;
        private City _cityTo;
        private XPCollection<BusinessPartner> _availableConsignee;
        private BusinessPartner _consignee;
        private string _consigneeContact;
        private string _consigneeAddress;
        #endregion VariableTo
        #region Bill
        private BusinessPartner _billToBusinessPartner;
        private string _billToContact;
        private Country _billToCountry;
        private City _billToCity;
        private string _billToAddress;
        #endregion Bill
        private PaymentMethod _paymentMethod;
        private PaymentMethodType _paymentMethodType;
        private PaymentType _paymentType;
        private TermOfPayment _top;
        private Currency _currency;
        #region Bank
        private XPCollection<BankAccount> _availableBankAccountCompany;
        private BankAccount _bankAccountCompany;
        private string _companyAccountNo;
        private string _companyAccountName;
        private XPCollection<BankAccount> _availableBankAccounts;
        private BankAccount _bankAccount;
        private string _accountNo;
        private string _accountName;
        #endregion Bank
        private double _maxAmount;
        private double _amount;
        private int _postedCount;
        private Status _status;
        private DateTime _statusDate;
        private Company _company;
        private string _userAccess;
        private GlobalFunction _globFunc;


        public ShipmentSalesInvoice(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.ShipmentSalesInvoice);
                DateTime now = DateTime.Now;
                this.Status = CustomProcess.Status.Open;
                this.StatusDate = now;

                _userAccess = SecuritySystem.CurrentUserName;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                if (Session.IsNewObject(this))
                {
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null)
                            {
                                Company = _locUserAccess.Employee.Company;
                            }
                        }
                    }
                }

                this.Currency = _globFunc.GetDefaultCurrency(this.Session);
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [ImmediatePostData()]
        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        #region ColorApproval

        [Browsable(false)]
        [Appearance("ShipmentSalesInvoiceActiveApproved1Close", Enabled = false)]
        public bool ActiveApproved1
        {
            get { return _activeApproved1; }
            set { SetPropertyValue("ActiveApproved1", ref _activeApproved1, value); }
        }

        [Browsable(false)]
        [Appearance("ShipmentSalesInvoiceActiveApproved2Close", Enabled = false)]
        public bool ActiveApproved2
        {
            get { return _activeApproved2; }
            set { SetPropertyValue("ActiveApproved2", ref _activeApproved2, value); }
        }

        [Browsable(false)]
        [Appearance("ShipmentSalesInvoiceActiveApproved3Close", Enabled = false)]
        public bool ActiveApproved3
        {
            get { return _activeApproved3; }
            set { SetPropertyValue("ActiveApproved3", ref _activeApproved3, value); }
        }

        #endregion ColorApproval

        [Appearance("ShipmentSalesInvoiceRedColor", Criteria = "ActiveApproved1 = true", BackColor = "#fd79a8")]
        [Appearance("ShipmentSalesInvoiceYellowColor", Criteria = "ActiveApproved2 = true", BackColor = "#ffeaa7")]
        [Appearance("ShipmentSalesInvoiceGreenOColor", Criteria = "ActiveApproved3 = true", BackColor = "#81ecec")]
        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("ShipmentSalesInvoiceCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("ShipmentSalesInvoiceShipmentTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public ShipmentType ShipmentType
        {
            get { return _shipmentType; }
            set { SetPropertyValue("ShipmentType", ref _shipmentType, value); }
        }

        #region From

        [ImmediatePostData()]
        [Appearance("ShipmentSalesInvoiceCountryFromClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Country CountryFrom
        {
            get { return _countryFrom; }
            set { SetPropertyValue("CountryFrom", ref _countryFrom, value); }
        }

        [ImmediatePostData()]
        [Appearance("ShipmentSalesInvoiceCityFromClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public City CityFrom
        {
            get { return _cityFrom; }
            set { SetPropertyValue("CityFrom", ref _cityFrom, value); }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableShipper", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("ShipmentSalesInvoiceShipperClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BusinessPartner Shipper
        {
            get { return _shipper; }
            set
            {
                SetPropertyValue("Shipper", ref _shipper, value);
                if (!IsLoading)
                {
                    if (this._shipper != null)
                    {
                        if(this._shipper.Contact != null)
                        {
                            this.ShipperContact = this._shipper.Contact;
                        }
                        if(this._shipper.Address != null)
                        {
                            this.ShipperAddress = this._shipper.Address;
                        }
                        this.BillToBusinessPartner = this._shipper;
                    }
                }
            }
        }

        [Appearance("ShipmentSalesInvoiceShipperContactClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string ShipperContact
        {
            get { return _shipperContact; }
            set { SetPropertyValue("ShipperContact", ref _shipperContact, value); }
        }

        [Size(512)]
        [Appearance("ShipmentSalesInvoiceShipperAddressClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string ShipperAddress
        {
            get { return _shipperAddress; }
            set { SetPropertyValue("ShipperAddress", ref _shipperAddress, value); }
        }

        #endregion From

        #region To

        [ImmediatePostData()]
        [Appearance("ShipmentSalesInvoiceCountryToClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Country CountryTo
        {
            get { return _countryTo; }
            set { SetPropertyValue("CountryTo", ref _countryTo, value); }
        }

        [Appearance("ShipmentSalesInvoiceCityToClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public City CityTo
        {
            get { return _cityTo; }
            set { SetPropertyValue("CityTo", ref _cityTo, value); }
        }

        [Browsable(false)]
        public XPCollection<BusinessPartner> AvailableConsignee
        {
            get
            {
                if (this.CountryFrom != null)
                {
                    if (this.CityFrom != null)
                    {
                        _availableConsignee = new XPCollection<BusinessPartner>
                                            (Session, new GroupOperator(GroupOperatorType.And,
                                            new BinaryOperator("Country", this.CountryTo),
                                            new BinaryOperator("City", this.CityTo)));
                    }
                    else
                    {
                        _availableConsignee = new XPCollection<BusinessPartner>
                                            (Session, new GroupOperator(GroupOperatorType.And,
                                            new BinaryOperator("Country", this.CountryTo)));
                    }
                }
                else
                {
                    _availableConsignee = new XPCollection<BusinessPartner>(Session);
                }


                return _availableConsignee;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableConsignee", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("ShipmentSalesInvoiceConsigneeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BusinessPartner Consignee
        {
            get { return _consignee; }
            set
            {
                SetPropertyValue("Consignee", ref _consignee, value);
                if (!IsLoading)
                {
                    if (this._consignee != null)
                    {
                        this.ConsigneeContact = this._consignee.Contact;
                        this.ConsigneeAddress = this._consignee.Address;
                    }
                }
            }
        }

        [Appearance("ShipmentSalesInvoiceConsigneeContactClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string ConsigneeContact
        {
            get { return _consigneeContact; }
            set { SetPropertyValue("ConsigneeContact", ref _consigneeContact, value); }
        }

        [Size(512)]
        [Appearance("ShipmentSalesInvoiceConsigneeAddressClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string ConsigneeAddress
        {
            get { return _consigneeAddress; }
            set { SetPropertyValue("ConsigneeAddress", ref _consigneeAddress, value); }
        }

        #endregion To

        #region BillToBusinessPartner

        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        [Appearance("ShipmentSalesInvoiceBillToBusinessPartnerClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BusinessPartner BillToBusinessPartner
        {
            get { return _billToBusinessPartner; }
            set
            {
                SetPropertyValue("BillToBusinessPartner", ref _billToBusinessPartner, value);
                if (!IsLoading)
                {
                    if (this._billToBusinessPartner != null)
                    {
                        this.BillToContact = this._billToBusinessPartner.Contact;
                        this.BillToAddress = this._billToBusinessPartner.Address;
                        this.BillToCountry = this._billToBusinessPartner.Country;
                        this.BillToCity = this._billToBusinessPartner.City;
                    }
                }
            }
        }

        [Appearance("ShipmentSalesInvoiceBillToContactClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string BillToContact
        {
            get { return _billToContact; }
            set { SetPropertyValue("BillToContact", ref _billToContact, value); }
        }

        [DataSourceCriteria("Active = true")]
        [Appearance("ShipmentSalesInvoiceBillCountryClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Country BillToCountry
        {
            get { return _billToCountry; }
            set { SetPropertyValue("BillToCountry", ref _billToCountry, value); }
        }

        [DataSourceCriteria("Active = true")]
        [Appearance("ShipmentSalesInvoiceBillToCityClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public City BillToCity
        {
            get { return _billToCity; }
            set { SetPropertyValue("BillToCity", ref _billToCity, value); }
        }

        [Appearance("ShipmentSalesInvoiceBillToAddressClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Size(512)]
        public string BillToAddress
        {
            get { return _billToAddress; }
            set { SetPropertyValue("BillToAddress", ref _billToAddress, value); }
        }

        #endregion BillToBusinessPartner

        [Appearance("ShipmentSalesInvoicePaymentMethodClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PaymentMethod PaymentMethod
        {
            get { return _paymentMethod; }
            set { SetPropertyValue("PaymentMethod", ref _paymentMethod, value); }
        }

        [Appearance("ShipmentSalesInvoicePaymentMethodTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PaymentMethodType PaymentMethodType
        {
            get { return _paymentMethodType; }
            set { SetPropertyValue("PaymentMethodType", ref _paymentMethodType, value); }
        }

        [Appearance("ShipmentSalesInvoicePaymentTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PaymentType PaymentType
        {
            get { return _paymentType; }
            set { SetPropertyValue("PaymentType", ref _paymentType, value); }
        }

        [Appearance("ShipmentSalesInvoiceTermOfPaymentClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public TermOfPayment TOP
        {
            get { return _top; }
            set { SetPropertyValue("TOP", ref _top, value); }
        }

        [Appearance("ShipmentSalesInvoiceCurrencyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Currency Currency
        {
            get { return _currency; }
            set { SetPropertyValue("Currency", ref _currency, value); }
        }

        #region Bank

        [Browsable(false)]
        public XPCollection<BankAccount> AvailableBankAccountCompany
        {
            get
            {
                if (this.Company == null)
                {
                    _availableBankAccountCompany = new XPCollection<BankAccount>(Session);
                }
                else
                {
                    _availableBankAccountCompany = new XPCollection<BankAccount>(Session, new GroupOperator
                                                (GroupOperatorType.And,
                                                new BinaryOperator("Company", this.Company)));
                }
                return _availableBankAccountCompany;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableBankAccountCompany", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("ShipmentSalesInvoiceBankAccountCompanyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BankAccount BankAccountCompany
        {
            get { return _bankAccountCompany; }
            set
            {
                SetPropertyValue("BankAccountCompany", ref _bankAccountCompany, value);
                if (!IsLoading)
                {
                    if (this._bankAccountCompany != null)
                    {
                        this.CompanyAccountNo = this._bankAccountCompany.AccountNo;
                        this.CompanyAccountName = this._bankAccountCompany.AccountName;
                    }
                }
            }
        }

        [Appearance("ShipmentSalesInvoiceCompanyAccountNoClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string CompanyAccountNo
        {
            get { return _companyAccountNo; }
            set { SetPropertyValue("CompanyAccountNo", ref _companyAccountNo, value); }
        }

        [Appearance("ShipmentSalesInvoiceCompanyAccountNameClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string CompanyAccountName
        {
            get { return _companyAccountName; }
            set { SetPropertyValue("CompanyAccountName", ref _companyAccountName, value); }
        }

        [Browsable(false)]
        public XPCollection<BankAccount> AvailableBankAccounts
        {
            get
            {
                if (this.BillToBusinessPartner == null)
                {
                    _availableBankAccounts = new XPCollection<BankAccount>(Session);
                }
                else
                {
                    _availableBankAccounts = new XPCollection<BankAccount>(Session, new GroupOperator
                                                (GroupOperatorType.And,
                                                new BinaryOperator("BusinessPartner", this.BillToBusinessPartner)));
                }
                return _availableBankAccounts;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableBankAccounts", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("ShipmentSalesInvoiceBankAccountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BankAccount BankAccount
        {
            get { return _bankAccount; }
            set
            {
                SetPropertyValue("BankAccount", ref _bankAccount, value);
                if (!IsLoading)
                {
                    if (this._bankAccount != null)
                    {
                        this.AccountNo = this._bankAccount.AccountNo;
                        this.AccountName = this._bankAccount.AccountName;
                    }
                }
            }
        }

        [Appearance("ShipmentSalesInvoiceAccountNoClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string AccountNo
        {
            get { return _accountNo; }
            set { SetPropertyValue("AccountNo", ref _accountNo, value); }
        }

        [Appearance("ShipmentSalesInvoiceAccountNameClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string AccountName
        {
            get { return _accountName; }
            set { SetPropertyValue("AccountName", ref _accountName, value); }
        }

        #endregion Bank

        [Appearance("ShipmentSalesInvoiceMaxAmountClose", Enabled = false)]
        public double MaxAmount
        {
            get { return _maxAmount; }
            set { SetPropertyValue("MaxAmount", ref _maxAmount, value); }
        }

        [ImmediatePostData()]
        [Appearance("ShipmentSalesInvoiceAmountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double Amount
        {
            get { return _amount; }
            set { SetPropertyValue("Amount", ref _amount, value); }
        }

        [Appearance("ShipmentSalesInvoicePostedCountClose", Enabled = false)]
        public int PostedCount
        {
            get { return _postedCount; }
            set { SetPropertyValue("PostedCount", ref _postedCount, value); }
        }

        [Appearance("ShipmentSalesInvoiceStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("ShipmentSalesInvoiceStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Browsable(false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Association("ShipmentSalesInvoice-ShipmentSalesInvoiceLines")]
        public XPCollection<ShipmentSalesInvoiceLine> ShipmentSalesInvoiceLines
        {
            get { return GetCollection<ShipmentSalesInvoiceLine>("ShipmentSalesInvoiceLines"); }
        }

        [Association("ShipmentSalesInvoice-ShipmentApprovalLines")]
        public XPCollection<ShipmentApprovalLine> ShipmentApprovalLines
        {
            get { return GetCollection<ShipmentApprovalLine>("ShipmentApprovalLines"); }
        }

        [Association("ShipmentSalesInvoice-ShipmentSalesCollections")]
        public XPCollection<ShipmentSalesCollection> ShipmentSalesCollections
        {
            get { return GetCollection<ShipmentSalesCollection>("ShipmentSalesCollections"); }
        }

        
        #endregion Field
    }
}