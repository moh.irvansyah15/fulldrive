﻿        #region Default
using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using System.Collections;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Xpo;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.WebUtils;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    [NavigationItem("HRIS")]
    [RuleCombinationOfPropertiesIsUnique("SalaryPeriodRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class Period : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private string _code;
        private string _name;
        private string _notes;
        private bool _active;
        private DateTime _startDate;
        private DateTime _endDate;
        private UInt16  _periodMonth;
        private UInt16  _periodYear;
        private Employee _employee;
        private GlobalFunction _globFunc;
        public Period(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.Period);
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }
        #endregion

        #region Field
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [ImmediatePostData]
        public string Name
        {
            get
            {
                if ((StartDate == null) && (EndDate == null))
                {
                    return "";
                }
                else
                {
                    return string.Format("Period {0:dd/MM/yyyy} - {1:dd/MM/yyyy} ", StartDate, EndDate );
                }

            }
        }

        public UInt16 PeriodYear
        {
            get { return _periodYear; }
            set { SetPropertyValue("PeriodYear", ref _periodYear, value); }
        }

        public UInt16 PeriodMonth
        {
            get { return _periodMonth; }
            set { SetPropertyValue("PeriodMonth", ref _periodMonth, value); }
        }

        [ImmediatePostData()]
        public DateTime StartDate
        {
            get { return _startDate; }
            set { SetPropertyValue("StartDate", ref _startDate, value); }
        }

        [ImmediatePostData()]
        public DateTime EndDate
        {
            get { return _endDate; }
            set { SetPropertyValue("EndDate", ref _endDate, value); }
        }

        public string Notes
        {
            get { return _notes; }
            set { SetPropertyValue("Notes", ref _notes, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue("Active", ref _active, value); }
        }

        [Association("Period-PayrollProcesses")]
        public XPCollection<PayrollProcess> PayrollProcesses
        {
            get { return GetCollection<PayrollProcess>("PayrollProcesses"); }
        }

        [Association("Period-SalaryBasics")]
        public XPCollection<SalaryBasic> SalaryBasic
        {
            get { return GetCollection<SalaryBasic>("SalaryBasic"); }
        }

        [Association("Period-TaxProcesses")]
        public XPCollection<TaxProcess> TaxProcesses
        {
            get { return GetCollection<TaxProcess>("TaxProcesses"); }
        }

        [Association("Period-LoanApprovals")]
        public XPCollection<LoanApproval> LoanApprovals
        {
            get { return GetCollection<LoanApproval>("LoanApprovals"); }
        }

        [Association("Period-LoanProcesses")]
        public XPCollection<LoanProcess> LoanProcesses
        {
            get { return GetCollection<LoanProcess>("LoanProcesses"); }
        }
        #endregion


        //
    }
}