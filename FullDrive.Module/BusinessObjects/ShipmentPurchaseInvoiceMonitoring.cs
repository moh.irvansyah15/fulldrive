﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using DevExpress.ExpressApp.Editors;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("ShipmentPurchaseInvoiceMonitoringRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class ShipmentPurchaseInvoiceMonitoring : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).

        private bool _activationPosting;
        private bool _select;
        private string _code;
        private ShipmentPurchaseInvoice _shipmentPurchaseInvoice;
        private ShipmentPurchaseInvoiceLine _shipmentPurchaseInvoiceLine;
        private Routing _routing;
        private ShipmentType _shipmentType;
        private TransactionTerm _transactionTerm;
        private string _workOrder;
        private TransportType _transportType;
        private XPCollection<ContainerType> _availableContainerType;
        private ContainerType _containerType;
        private PriceType _priceType;
        private DeliveryType _deliveryType;
        private BusinessPartner _carrier;
        private VehicleMode _vehicleMode;
        private XPCollection<Vehicle> _availableVehicle;
        private Vehicle _vehicle;
        private VehicleType _vehicleType;
        private double _actualQuantity;
        private UnitOfMeasure _uom;
        private string _containerNo;
        #region VariableFrom
        private Country _countryFrom;
        private City _cityFrom;
        private XPCollection<Area> _availableAreaFrom;
        private Area _areaFrom;
        private XPCollection<Location> _availableLocationFrom;
        private Location _locationFrom;
        private XPCollection<TransportLocation> _availableTransportLocationFrom;
        private TransportLocation _transportLocationFrom;
        #endregion VariableFrom
        #region VariableTo
        private Country _countryTo;
        private City _cityTo;
        private XPCollection<Area> _availableAreaTo;
        private Area _areaTo;
        private XPCollection<Location> _availableLocationTo;
        private Location _locationTo;
        private XPCollection<TransportLocation> _availableTransportLocationTo;
        private TransportLocation _transportLocationTo;
        #endregion VariableTo
        private double _maxAmount;
        private double _amount;
        private double _postedAmount;
        private double _outstandingAmount;
        private string _remark;
        private Status _status;
        private DateTime _statusDate;
        private int _postedCount;
        private GlobalFunction _globFunc;

        public ShipmentPurchaseInvoiceMonitoring(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            DateTime now = DateTime.Now;
            _globFunc = new GlobalFunction();
            this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.ShipmentPurchaseInvoiceMonitoring);
            this.Status = Status.Open;
            this.StatusDate = now;
            this.Select = true;
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [Browsable(false)]
        [ImmediatePostData()]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [Appearance("ShipmentPurchaseInvoiceMonitoringSelectClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public bool Select
        {
            get { return _select; }
            set { SetPropertyValue("Select", ref _select, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("ShipmentPurchaseInvoiceMonitoringCodeEnabled", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        
        [Appearance("ShipmentPurchaseInvoiceMonitoringShipmentPurchaseInvoiceClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public ShipmentPurchaseInvoice ShipmentPurchaseInvoice
        {
            get { return _shipmentPurchaseInvoice; }
            set { SetPropertyValue("ShipmentPurchaseInvoice", ref _shipmentPurchaseInvoice, value); }
        }

        [Appearance("ShipmentPurchaseInvoiceMonitoringShipmentPurchaseInvoiceLineClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public ShipmentPurchaseInvoiceLine ShipmentPurchaseInvoiceLine
        {
            get { return _shipmentPurchaseInvoiceLine; }
            set { SetPropertyValue("ShipmentPurchaseInvoiceLine", ref _shipmentPurchaseInvoiceLine, value); }
        }

        [ImmediatePostData()]
        [Appearance("ShipmentPurchaseInvoiceMonitoringRoutingClose", Enabled = false)]
        public Routing Routing
        {
            get { return _routing; }
            set { SetPropertyValue("Routing", ref _routing, value); }
        }

        [Appearance("ShipmentPurchaseInvoiceMonitoringShipmentTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public ShipmentType ShipmentType
        {
            get { return _shipmentType; }
            set { SetPropertyValue("ShipmentType", ref _shipmentType, value); }
        }

        [Appearance("ShipmentPurchaseInvoiceMonitoringTransactionTermClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public TransactionTerm TransactionTerm
        {
            get { return _transactionTerm; }
            set { SetPropertyValue("TransactionTerm", ref _transactionTerm, value); }
        }

        [Appearance("ShipmentPurchaseInvoiceMonitoringTransportTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        public TransportType TransportType
        {
            get { return _transportType; }
            set { SetPropertyValue("TransportType", ref _transportType, value); }
        }

        [Browsable(false)]
        public XPCollection<ContainerType> AvailableContainerType
        {
            get
            {
                if (_transportType == null)
                {
                    _availableContainerType = new XPCollection<ContainerType>(Session);
                }
                else
                {
                    _availableContainerType = new XPCollection<ContainerType>(Session,
                                                new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("TransportType", _transportType, BinaryOperatorType.Equal),
                                                    new BinaryOperator("Active", true)));
                }
                return _availableContainerType;
            }
        }

        [Appearance("ShipmentPurchaseInvoiceMonitoringContainerTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceProperty("AvailableContainerType", DataSourcePropertyIsNullMode.SelectAll)]
        public ContainerType ContainerType
        {
            get { return _containerType; }
            set { SetPropertyValue("ContainerType", ref _containerType, value); }
        }

        [Appearance("ShipmentPurchaseInvoiceMonitoringPriceTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PriceType PriceType
        {
            get { return _priceType; }
            set { SetPropertyValue("PriceType", ref _priceType, value); }
        }

        [Appearance("ShipmentPurchaseInvoiceMonitoringDeliveryTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DeliveryType DeliveryType
        {
            get { return _deliveryType; }
            set { SetPropertyValue("DeliveryType", ref _deliveryType, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("ShipmentPurchaseInvoiceMonitoringWorkOrderClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string WorkOrder
        {
            get { return _workOrder; }
            set { SetPropertyValue("WorkOrder", ref _workOrder, value); }
        }

        [ImmediatePostData()]
        [Appearance("ShipmentPurchaseInvoiceMonitoringCarrierClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BusinessPartner Carrier
        {
            get { return _carrier; }
            set { SetPropertyValue("Carrier", ref _carrier, value); }
        }

        [ImmediatePostData()]
        [Appearance("ShipmentPurchaseInvoiceMonitoringVehicleModeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public VehicleMode VehicleMode
        {
            get { return _vehicleMode; }
            set { SetPropertyValue("VehicleMode", ref _vehicleMode, value); }
        }

        [Browsable(false)]
        public XPCollection<Vehicle> AvailableVehicle
        {
            get
            {
                if (this.Carrier == null && this.VehicleMode == VehicleMode.None)
                {
                    _availableVehicle = new XPCollection<Vehicle>(Session);
                }
                else
                {
                    if (this.VehicleMode != VehicleMode.None)
                    {
                        if (this.Carrier != null)
                        {
                            _availableVehicle = new XPCollection<Vehicle>(Session,
                                                new GroupOperator(GroupOperatorType.And,
                                                new BinaryOperator("VehicleMode", this.VehicleMode),
                                                new BinaryOperator("BusinessPartner", this.Carrier),
                                                new BinaryOperator("Active", true)));
                        }
                        else
                        {
                            _availableVehicle = new XPCollection<Vehicle>(Session,
                                                new GroupOperator(GroupOperatorType.And,
                                                new BinaryOperator("VehicleMode", this.VehicleMode),
                                                new BinaryOperator("Active", true)));
                        }
                    }

                }
                return _availableVehicle;
            }
        }

        [ImmediatePostData()]
        [Appearance("ShipmentPurchaseInvoiceMonitoringVehicleClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceProperty("AvailableVehicle", DataSourcePropertyIsNullMode.SelectAll)]
        public Vehicle Vehicle
        {
            get { return _vehicle; }
            set
            {
                SetPropertyValue("Vehicle", ref _vehicle, value);
                if (this._vehicle != null)
                {
                    if (this._vehicle.UOM != null)
                    {
                        this.UOM = this._vehicle.UOM;
                    }
                    if (this._vehicle.Capacity > 0)
                    {
                        this.ActualQuantity = this._vehicle.Capacity;
                    }
                    if (this._vehicle.VehicleType != null)
                    {
                        this._vehicleType = this._vehicle.VehicleType;
                    }
                }
            }
        }

        [Appearance("ShipmentPurchaseInvoiceMonitoringVehicleTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public VehicleType VehicleType
        {
            get { return _vehicleType; }
            set { SetPropertyValue("VehicleType", ref _vehicleType, value); }
        }

        [Appearance("ShipmentPurchaseInvoiceMonitoringActualQuantityClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double ActualQuantity
        {
            get { return _actualQuantity; }
            set { SetPropertyValue("ActualQuantity", ref _actualQuantity, value); }
        }

        [Appearance("ShipmentPurchaseInvoiceMonitoringUOMClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure UOM
        {
            get { return _uom; }
            set { SetPropertyValue("UOM", ref _uom, value); }
        }

        [Appearance("ShipmentPurchaseInvoiceMonitoringContainerNoClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string ContainerNo
        {
            get { return _containerNo; }
            set { SetPropertyValue("ContainerNo", ref _containerNo, value); }
        }

        #region From

        [ImmediatePostData()]
        [Appearance("ShipmentPurchaseInvoiceMonitoringCountryFromClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Country CountryFrom
        {
            get { return _countryFrom; }
            set { SetPropertyValue("CountryFrom", ref _countryFrom, value); }
        }

        [ImmediatePostData()]
        [Appearance("ShipmentPurchaseInvoiceMonitoringCityFromClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public City CityFrom
        {
            get { return _cityFrom; }
            set { SetPropertyValue("CityFrom", ref _cityFrom, value); }
        }

        [Browsable(false)]
        public XPCollection<Area> AvailableAreaFrom
        {
            get
            {
                if (this.CountryFrom != null && this.CityFrom != null)
                {
                    _availableAreaFrom = new XPCollection<Area>(Session,
                                    new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("Country", this.CountryFrom),
                                    new BinaryOperator("City", this.CityFrom)));

                }
                else if (this.CountryFrom != null && this.CityFrom == null)
                {
                    _availableAreaFrom = new XPCollection<Area>(Session,
                                    new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("Country", this.CountryFrom)));
                }
                else if (this.CountryFrom == null && this.CityFrom != null)
                {
                    _availableAreaFrom = new XPCollection<Area>(Session,
                                    new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("City", this.CityFrom)));
                }
                else
                {
                    _availableAreaFrom = new XPCollection<Area>(Session);
                }

                return _availableAreaFrom;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableAreaFrom", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("ShipmentPurchaseInvoiceMonitoringAreaFromClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public Area AreaFrom
        {
            get { return _areaFrom; }
            set { SetPropertyValue("AreaFrom", ref _areaFrom, value); }
        }

        [Browsable(false)]
        public XPCollection<Location> AvailableLocationFrom
        {
            get
            {
                if (this.AreaFrom != null)
                {
                    _availableLocationFrom = new XPCollection<Location>(Session,
                                          new GroupOperator(GroupOperatorType.And,
                                          new BinaryOperator("Area", this.AreaFrom),
                                          new BinaryOperator("Active", true)));

                }
                else
                {
                    _availableLocationFrom = new XPCollection<Location>(Session);
                }

                return _availableLocationFrom;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableLocationFrom", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("ShipmentPurchaseInvoiceMonitoringLocationFromClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public Location LocationFrom
        {
            get { return _locationFrom; }
            set { SetPropertyValue("LocationFrom", ref _locationFrom, value); }
        }

        [Browsable(false)]
        public XPCollection<TransportLocation> AvailableTransportLocationFrom
        {
            get
            {
                if (this.CountryFrom != null)
                {
                    _availableTransportLocationFrom = new XPCollection<TransportLocation>(Session,
                                          new GroupOperator(GroupOperatorType.And,
                                          new BinaryOperator("Country", this.CountryFrom),
                                          new BinaryOperator("Active", true)));

                }
                else
                {
                    _availableTransportLocationFrom = new XPCollection<TransportLocation>(Session);
                }

                return _availableTransportLocationFrom;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableTransportLocationFrom", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("ShipmentPurchaseInvoiceMonitoringTransportLocationFromClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public TransportLocation TransportLocationFrom
        {
            get { return _transportLocationFrom; }
            set { SetPropertyValue("TransportLocationFrom", ref _transportLocationFrom, value); }
        }

        #endregion From

        #region To

        [ImmediatePostData()]
        [Appearance("ShipmentPurchaseInvoiceMonitoringCountryToClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Country CountryTo
        {
            get { return _countryTo; }
            set { SetPropertyValue("CountryTo", ref _countryTo, value); }
        }

        [Appearance("ShipmentPurchaseInvoiceMonitoringCityToClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public City CityTo
        {
            get { return _cityTo; }
            set { SetPropertyValue("CityTo", ref _cityTo, value); }
        }

        [Browsable(false)]
        public XPCollection<Area> AvailableAreaTo
        {
            get
            {
                if (this.CountryTo != null && this.CityTo != null)
                {
                    _availableAreaTo = new XPCollection<Area>(Session,
                                    new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("Country", this.CountryTo),
                                    new BinaryOperator("City", this.CityTo)));

                }
                else if (this.CountryTo != null && this.CityTo == null)
                {
                    _availableAreaTo = new XPCollection<Area>(Session,
                                    new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("Country", this.CountryTo)));
                }
                else if (this.CountryTo == null && this.CityTo != null)
                {
                    _availableAreaTo = new XPCollection<Area>(Session,
                                    new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("City", this.CityTo)));
                }
                else
                {
                    _availableAreaTo = new XPCollection<Area>(Session);
                }

                return _availableAreaTo;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableAreaTo", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("ShipmentPurchaseInvoiceMonitoringAreaToClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public Area AreaTo
        {
            get { return _areaTo; }
            set { SetPropertyValue("AreaTo", ref _areaTo, value); }
        }

        [Browsable(false)]
        public XPCollection<Location> AvailableLocationTo
        {
            get
            {
                if (this.AreaTo != null)
                {
                    _availableLocationTo = new XPCollection<Location>(Session,
                                          new GroupOperator(GroupOperatorType.And,
                                          new BinaryOperator("Area", this.AreaTo),
                                          new BinaryOperator("Active", true)));

                }
                else
                {
                    _availableLocationTo = new XPCollection<Location>(Session);
                }

                return _availableLocationTo;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableLocationTo", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("ShipmentPurchaseInvoiceMonitoringLocationToClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public Location LocationTo
        {
            get { return _locationTo; }
            set { SetPropertyValue("LocationTo", ref _locationTo, value); }
        }

        [Browsable(false)]
        public XPCollection<TransportLocation> AvailableTransportLocationTo
        {
            get
            {
                if (this.CountryTo != null)
                {
                    _availableTransportLocationTo = new XPCollection<TransportLocation>(Session,
                                          new GroupOperator(GroupOperatorType.And,
                                          new BinaryOperator("Country", this.CountryTo),
                                          new BinaryOperator("Active", true)));

                }
                else
                {
                    _availableTransportLocationTo = new XPCollection<TransportLocation>(Session);
                }

                return _availableTransportLocationTo;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableTransportLocationTo", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("ShipmentPurchaseInvoiceMonitoringTransportLocationToClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public TransportLocation TransportLocationTo
        {
            get { return _transportLocationTo; }
            set { SetPropertyValue("TransportLocationTo", ref _transportLocationTo, value); }
        }

        #endregion To

        #region Amount

        [Appearance("ShipmentPurchaseMonitoringMaxAmountClose", Enabled = false)]
        public double MaxAmount
        {
            get { return _maxAmount; }
            set { SetPropertyValue("MaxAmount", ref _maxAmount, value); }
        }

        [ImmediatePostData()]
        [Appearance("ShipmentPurchaseInvoiceMonitoringAmountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double Amount
        {
            get { return _amount; }
            set
            {
                SetPropertyValue("Amount", ref _amount, value);
                if (!IsLoading)
                {
                    if (this._amount > 0)
                    {
                        SetMxAmount();
                    }
                }
            }
        }

        [Appearance("ShipmentPurchaseInvoiceMonitoringPostedAmountClose", Enabled = false)]
        public double PostedAmount
        {
            get { return _postedAmount; }
            set { SetPropertyValue("PostedAmount", ref _postedAmount, value); }
        }

        [Appearance("ShipmentPurchaseInvoiceMonitoringOutstandingAmountClose", Enabled = false)]
        public double OutstandingAmount
        {
            get { return _outstandingAmount; }
            set { SetPropertyValue("OutstandingAmount", ref _outstandingAmount, value); }
        }

        #endregion Amount

        [Appearance("ShipmentPurchaseInvoiceMonitoringRemarkClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Size(512)]
        public string Remark
        {
            get { return _remark; }
            set { SetPropertyValue("Remark", ref _remark, value); }
        }

        [Appearance("ShipmentPurchaseInvoiceMonitoringPostedCountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public int PostedCount
        {
            get { return _postedCount; }
            set { SetPropertyValue("PostedCount", ref _postedCount, value); }
        }

        [Appearance("ShipmentPurchaseInvoiceMonitoringStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("ShipmentPurchaseInvoiceMonitoringStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        #endregion Field

        //================================================== Code Only ==================================================

        private void SetMxAmount()
        {
            try
            {
                if (this.ShipmentPurchaseInvoice != null)
                {
                    if (this.Status == Status.Posted || this.PostedCount > 0)
                    {
                        if (this.OutstandingAmount > 0)
                        {
                            if (this._amount > this.OutstandingAmount)
                            {
                                this._amount = this.OutstandingAmount;
                            }
                        }
                    }
                    else if (this.Status == Status.Close || this.PostedCount > 0)
                    {
                        this._amount = 0;
                    }

                }
                else
                {
                    if ((this.Status == Status.Open || this.Status == Status.Progress) && this.PostedCount <= 0)
                    {
                        if (this._amount > 0)
                        {
                            this.MaxAmount = this._amount;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ShipmentPurchaseInvoiceMonitoring " + ex.ToString());
            }
        }
    }
}