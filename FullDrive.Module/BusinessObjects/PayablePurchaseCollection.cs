﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("PayablePurchaseCollectionRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class PayablePurchaseCollection : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private string _code;
        private XPCollection<PurchaseInvoice> _availablePurchaseInvoice;
        private PurchaseInvoice _purchaseInvoice;
        private XPCollection<PurchasePrePaymentInvoice> _availablePurchasePrePaymentInvoice;
        private PurchasePrePaymentInvoice _purchasePrePaymentInvoice;
        private PayableTransaction _payableTransaction;
        private Status _status;
        private DateTime _statusDate;
        private int _postedCount;
        private GlobalFunction _globFunc;

        public PayablePurchaseCollection(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.PayablePurchaseCollection);
                this.Status = Status.Open;
                this.StatusDate = now;
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("PayablePurchaseCollectionCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Browsable(false)]
        public XPCollection<PurchaseInvoice> AvailablePurchaseInvoice
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;

                if (!IsLoading)
                {
                    if (this.PayableTransaction != null)
                    {
                        if (this.PayableTransaction.BuyFromVendor != null && this.PayableTransaction.PayToVendor != null)
                        {
                            XPQuery<PurchaseInvoiceMonitoring> _purchaseInvoiceMonitoringsQuery = new XPQuery<PurchaseInvoiceMonitoring>(Session);

                            var _purchaseInvoiceMonitorings = from pim in _purchaseInvoiceMonitoringsQuery
                                                           where (pim.Status != Status.Close
                                                           && pim.PurchaseInvoice.BuyFromVendor == this.PayableTransaction.BuyFromVendor
                                                           && pim.PurchaseInvoice.PayToVendor == this.PayableTransaction.PayToVendor
                                                           && pim.PayableTransaction == null
                                                           )
                                                           group pim by pim.PurchaseInvoice into g
                                                           select new { PurchaseInvoice = g.Key };

                            if (_purchaseInvoiceMonitorings != null && _purchaseInvoiceMonitorings.Count() > 0)
                            {
                                List<string> _stringPIM = new List<string>();

                                foreach (var _purchaseInvoiceMonitoring in _purchaseInvoiceMonitorings)
                                {
                                    if (_purchaseInvoiceMonitoring != null)
                                    {
                                        _stringPIM.Add(_purchaseInvoiceMonitoring.PurchaseInvoice.Code);
                                    }
                                }

                                IEnumerable<string> _stringArrayPIMDistinct = _stringPIM.Distinct();
                                string[] _stringArrayPIMList = _stringArrayPIMDistinct.ToArray();
                                if (_stringArrayPIMList.Length == 1)
                                {
                                    for (int i = 0; i < _stringArrayPIMList.Length; i++)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _stringArrayPIMList[i] + "'";
                                        }
                                    }
                                }
                                else if (_stringArrayPIMList.Length > 1)
                                {
                                    for (int i = 0; i < _stringArrayPIMList.Length; i++)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _stringArrayPIMList[i] + "'";
                                        }
                                        else
                                        {
                                            _endString = _endString + " OR [Code]=='" + _stringArrayPIMList[i] + "'";
                                        }
                                    }
                                }
                                _fullString = _beginString + _endString;

                                if (_fullString != null)
                                {
                                    _availablePurchaseInvoice = new XPCollection<PurchaseInvoice>(Session, CriteriaOperator.Parse(_fullString));
                                }
                            }
                        }
                    }
                }
                return _availablePurchaseInvoice;
            }
        }

        [DataSourceProperty("AvailablePurchaseInvoice", DataSourcePropertyIsNullMode.SelectNothing)]
        public PurchaseInvoice PurchaseInvoice
        {
            get { return _purchaseInvoice; }
            set { SetPropertyValue("PurchaseInvoice", ref _purchaseInvoice, value); }
        }

        [Browsable(false)]
        public XPCollection<PurchasePrePaymentInvoice> AvailablePurchasePrePaymentInvoice
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;

                if (!IsLoading)
                {
                    if (this.PayableTransaction != null)
                    {
                        if (this.PayableTransaction.BuyFromVendor != null && this.PayableTransaction.PayToVendor != null)
                        {
                            XPQuery<PurchasePrePaymentInvoice> _purchasePrePaymentInvoiceQuery = new XPQuery<PurchasePrePaymentInvoice>(Session);

                            var _purchasePrePaymentInvoices = from pim in _purchasePrePaymentInvoiceQuery
                                                           where (pim.Status != Status.Close
                                                           && pim.BuyFromVendor == this.PayableTransaction.BuyFromVendor
                                                           && pim.PayToVendor == this.PayableTransaction.PayToVendor
                                                           && pim.PayableTransaction == null
                                                           )
                                                           group pim by pim.Code into g
                                                           select new { Code = g.Key };

                            if (_purchasePrePaymentInvoices != null && _purchasePrePaymentInvoices.Count() > 0)
                            {
                                List<string> _stringPPI = new List<string>();

                                foreach (var _purchasePrePaymentInvoice in _purchasePrePaymentInvoices)
                                {
                                    if (_purchasePrePaymentInvoice != null)
                                    {
                                        _stringPPI.Add(_purchasePrePaymentInvoice.Code);
                                    }
                                }

                                IEnumerable<string> _stringArrayPPIDistinct = _stringPPI.Distinct();
                                string[] _stringArrayPPIList = _stringArrayPPIDistinct.ToArray();
                                if (_stringArrayPPIList.Length == 1)
                                {
                                    for (int i = 0; i < _stringArrayPPIList.Length; i++)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _stringArrayPPIList[i] + "'";
                                        }
                                    }
                                }
                                else if (_stringArrayPPIList.Length > 1)
                                {
                                    for (int i = 0; i < _stringArrayPPIList.Length; i++)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _stringArrayPPIList[i] + "'";
                                        }
                                        else
                                        {
                                            _endString = _endString + " OR [Code]=='" + _stringArrayPPIList[i] + "'";
                                        }
                                    }
                                }
                                _fullString = _beginString + _endString;

                                if (_fullString != null)
                                {
                                    _availablePurchasePrePaymentInvoice = new XPCollection<PurchasePrePaymentInvoice>(Session, CriteriaOperator.Parse(_fullString));
                                }
                            }
                        }
                    }
                }
                return _availablePurchasePrePaymentInvoice;
            }
        }

        [DataSourceProperty("AvailablePurchasePrePaymentInvoice", DataSourcePropertyIsNullMode.SelectNothing)]
        public PurchasePrePaymentInvoice PurchasePrePaymentInvoice
        {
            get { return _purchasePrePaymentInvoice; }
            set { SetPropertyValue("PurchasePrePaymentInvoice", ref _purchasePrePaymentInvoice, value); }
        }

        [ImmediatePostData()]
        [Appearance("PayablePurchaseCollectionPayableTransactionClose", Enabled = false)]
        [Association("PayableTransaction-PayablePurchaseCollections")]
        public PayableTransaction PayableTransaction
        {
            get { return _payableTransaction; }
            set { SetPropertyValue("PayableTransaction", ref _payableTransaction, value); }
        }

        [Appearance("PayablePurchaseCollectionStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("PayablePurchaseCollectionStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("PayablePurchaseCollectionPostedCountEnabled", Enabled = false)]
        public int PostedCount
        {
            get { return _postedCount; }
            set { SetPropertyValue("PostedCount", ref _postedCount, value); }
        }
        #endregion Field

        //private string _PersistentProperty;
        //[XafDisplayName("My display name"), ToolTip("My hint message")]
        //[ModelDefault("EditMask", "(000)-00"), Index(0), VisibleInListView(false)]
        //[Persistent("DatabaseColumnName"), RuleRequiredField(DefaultContexts.Save)]
        //public string PersistentProperty {
        //    get { return _PersistentProperty; }
        //    set { SetPropertyValue("PersistentProperty", ref _PersistentProperty, value); }
        //}

        //[Action(Caption = "My UI Action", ConfirmationMessage = "Are you sure?", ImageName = "Attention", AutoCommit = true)]
        //public void ActionMethod() {
        //    // Trigger a custom business logic for the current record in the UI (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112619.aspx).
        //    this.PersistentProperty = "Paid";
        //}
    }
}