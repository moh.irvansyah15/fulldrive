﻿#region Default

using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

#endregion Default

using DevExpress.ExpressApp.Editors;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    #region Default

    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("AccountReclassJournalRuleUnique", DefaultContexts.Save, "Code")]

    #endregion Default

    // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
    public class AccountReclassJournal : FullDriveSysBaseObject
    {
        #region Default

        private bool _activationPosting;
        private string _name;
        private string _code;
        private PostingType _postingType;
        private PostingMethod _postingMethod;
        private PostingMethodType _postingMethodType;
        private string _description;
        private Status _status;
        private DateTime _statusDate;
        private string _userAccess;
        private Company _companyDefault;
        private CashAdvance _cashAdvance;
        private GlobalFunction _globFunc;

        public AccountReclassJournal(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
            if (!IsLoading)
            {
                DateTime now = DateTime.Now;
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.AccountReclassJournal);
                this.Status = Status.Open;
                this.StatusDate = now;
                this.PostingType = PostingType.Account;
                this.PostingMethod = PostingMethod.ReclassJournal;
                this.PostingMethodType = PostingMethodType.Normal;

                _userAccess = SecuritySystem.CurrentUserName;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                if (Session.IsNewObject(this))
                {
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null)
                            {
                                CompanyDefault = _locUserAccess.Employee.Company;
                            }
                        }
                    }
                }
            }
        }

        #endregion Default

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [Appearance("AccountReclassJournalCodeEnabled", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("AccountReclassJournalNameClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        [Appearance("AccountReclassJournalPostingTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PostingType PostingType
        {
            get { return _postingType; }
            set { SetPropertyValue("PostingType", ref _postingType, value); }
        }

        [Appearance("AccountReclassJournalPostingMethodClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PostingMethod PostingMethod
        {
            get { return _postingMethod; }
            set { SetPropertyValue("PostingMethod", ref _postingMethod, value); }
        }

        [Appearance("AccountReclassJournalPostingMethodTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PostingMethodType PostingMethodType
        {
            get { return _postingMethodType; }
            set { SetPropertyValue("PostingMethodType", ref _postingMethodType, value); }
        }

        [Size(512)]
        [Appearance("AccountReclassJournalDescriptionClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        [Appearance("AccountReclassJournalStatusEnabled", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("AccountReclassJournalStatusDateEnabled", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("AccountReclassJournalCompanyDefaultClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Company CompanyDefault
        {
            get { return _companyDefault; }
            set { SetPropertyValue("CompanyDefault", ref _companyDefault, value); }
        }

        [Association("AccountReclassJournal-AccountReclassJournalLines")]
        public XPCollection<AccountReclassJournalLine>AccountReclassJournalLines
        {
            get { return GetCollection<AccountReclassJournalLine>("AccountReclassJournalLines"); }
        }

        #endregion Field

    }
}