﻿#region Default

using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

#endregion Default

using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    #region Default

    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Inventory")]
    [RuleCombinationOfPropertiesIsUnique("InventoryTransferOrderRuleUnique", DefaultContexts.Save, "Code")]

    #endregion Default

    // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
    public class InventoryTransferOrder : FullDriveSysBaseObject
    {
        #region Default

        private bool _activationPosting;
        private string _code;
        private string _name;
        private DirectionType _transferType;
        private InventoryMovingType _inventoryMovingType;
        private DocumentRule _documentRule;
        private ObjectList _objectList;
        XPCollection<DocumentType> _availableDocumentType;
        private DocumentType _documentType;
        private string _docNo;
        private BusinessPartner _businessPartner;
        private Country _businessPartnerCountry;
        private City _businessPartnerCity;
        private string _businessPartnerAddress;
        private DateTime _estimatedDate;
        private XPCollection<Location> _availableLocation;
        private Location _location;
        private StockType _stockType;
        private Status _status;
        private DateTime _statusDate;
        private bool _enabledHold;
        private bool _hold;
        private Company _company;
        private ProjectHeader _projectHeader;
        private PurchaseOrder _purchaseOrder;
        private SalesOrder _salesOrder;
        private InventoryTransferOrder _internalInventoryTransferOrder;
        private string _userAccess;
        private bool _stockControlling;
        private double _maxBill;
        private bool _splitInvoice;
        private GlobalFunction _globFunc;
        //clm
        private bool _activeApproved1;
        private bool _activeApproved2;
        private bool _activeApproved3;

        public InventoryTransferOrder(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.InventoryTransferOrder);
                this.Status = Status.Open;
                this.StatusDate = now;

                _userAccess = SecuritySystem.CurrentUserName;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                if (Session.IsNewObject(this))
                {
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null)
                            {
                                Company = _locUserAccess.Employee.Company;
                            }
                        }
                    }
                }
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #endregion Default

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [ImmediatePostData()]
        [Appearance("InventoryTransferOrderCodeClose", Enabled = false)]
        [Appearance("InventoryTransferOrderRedColor", Criteria = "ActiveApproved1 = true", BackColor = "#fd79a8")]
        [Appearance("InventoryTransferOrderYellowColor", Criteria = "ActiveApproved2 = true", BackColor = "#ffeaa7")]
        [Appearance("InventoryTransferOrderGreenOColor", Criteria = "ActiveApproved3 = true", BackColor = "#81ecec")]
        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("InventoryTransferOrderCodeEnabled", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [ImmediatePostData()]
        [Appearance("InventoryTransferOrderNameClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        [Appearance("InventoryTransferOrderTransferTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public DirectionType TransferType
        {
            get { return _transferType; }
            set { SetPropertyValue("DirectionType", ref _transferType, value); }
        }

        [Appearance("InventoryTransferOrderInventoryMovingTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public InventoryMovingType InventoryMovingType
        {
            get { return _inventoryMovingType; }
            set
            {
                SetPropertyValue("InventoryMovingType", ref _inventoryMovingType, value);
                if(!IsLoading)
                {
                    if(this._inventoryMovingType == InventoryMovingType.Deliver )
                    {
                        this.ObjectList = ObjectList.InventoryTransferOut;
                    }
                    else if(this._inventoryMovingType == InventoryMovingType.Receive )
                    {
                        this.ObjectList = ObjectList.InventoryTransferIn;
                    }
                }
            }
        }

        [ImmediatePostData()]
        [Appearance("InventoryTransferOrderDocumentRuleClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DocumentRule DocumentRule
        {
            get { return _documentRule; }
            set { SetPropertyValue("DocumentRule", ref _documentRule, value); }
        }

        [Browsable(false)]
        public ObjectList ObjectList
        {
            get { return _objectList; }
            set { SetPropertyValue("ObjectList", ref _objectList, value); }
        }

        [Browsable(false)]
        public XPCollection<DocumentType> AvailableDocumentType
        {
            get
            {
                if (ObjectList == ObjectList.None)
                {
                    _availableDocumentType = new XPCollection<DocumentType>(Session);
                }
                else
                {
                    _availableDocumentType = new XPCollection<DocumentType>(Session,
                                              new GroupOperator(GroupOperatorType.And,
                                              new BinaryOperator("ObjectList", this.ObjectList),
                                              new BinaryOperator("InventoryMovingType", this.InventoryMovingType),
                                              new BinaryOperator("TransferType", this.TransferType),
                                              new BinaryOperator("DocumentRule", this.DocumentRule),
                                              new BinaryOperator("Active", true)));
                }
                return _availableDocumentType;

            }
        }

        [Appearance("InventoryTransferOrderDocumentTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceProperty("AvailableDocumentType", DataSourcePropertyIsNullMode.SelectAll)]
        public DocumentType DocumentType
        {
            get { return _documentType; }
            set
            {
                SetPropertyValue("DocumentType", ref _documentType, value);
                if (!IsLoading)
                {
                    if (_documentType != null)
                    {
                        _globFunc = new GlobalFunction();
                        this.DocNo = _globFunc.GetDocumentNumberingUnlockOptimisticRecord(this.Session.DataLayer, this._documentType);
                    }
                }
            }
        }

        [Appearance("InventoryTransferOrderDocNoClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string DocNo
        {
            get { return _docNo; }
            set { SetPropertyValue("DocNo", ref _docNo, value); }
        }

        [Appearance("InventoryTransferOrderBusinessPartnerClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public BusinessPartner BusinessPartner
        {
            get { return _businessPartner; }
            set
            {
                SetPropertyValue("BusinessPartner", ref _businessPartner, value);
                if (!IsLoading)
                {
                    if (_businessPartner != null)
                    {
                        this.BusinessPartnerCountry = _businessPartner.Country;
                        this.BusinessPartnerCity = _businessPartner.City;
                        this.BusinessPartnerAddress = _businessPartner.Address;
                    }
                }
            }
        }

        [Appearance("InventoryTransferOrderBusinessPartnerCountryClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public Country BusinessPartnerCountry
        {
            get { return _businessPartnerCountry; }
            set { SetPropertyValue("BusinessPartnerCountry", ref _businessPartnerCountry, value); }
        }

        [Appearance("InventoryTransferOrderBusinessPartnerCityClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public City BusinessPartnerCity
        {
            get { return _businessPartnerCity; }
            set { SetPropertyValue("BusinessPartnerCity", ref _businessPartnerCity, value); }
        }

        [Size(512)]
        [Appearance("InventoryTransferOrderBusinessPartnerAddressClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string BusinessPartnerAddress
        {
            get { return _businessPartnerAddress; }
            set { SetPropertyValue("BusinessPartnerAddress", ref _businessPartnerAddress, value); }
        }

        [Appearance("InventoryTransferOrderEstimatedDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime EstimatedDate
        {
            get { return _estimatedDate; }
            set { SetPropertyValue("EstimatedDate", ref _estimatedDate, value); }
        }

        [Appearance("InventoryTransferOrderStatusEnabled", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("InventoryTransferOrderStatusDateEnabled", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Browsable(false)]
        public XPCollection<Location> AvailableLocation
        {
            get
            {
                _userAccess = SecuritySystem.CurrentUserName;
                string _beginString = null;
                string _endString = null;
                string _fullString = null;
                string _locLocationCode = null;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                if (_locUserAccess != null)
                {
                    if ((this.TransferType == DirectionType.External || this.TransferType == DirectionType.Internal) && 
                        (this.InventoryMovingType == InventoryMovingType.Receive || this.InventoryMovingType == InventoryMovingType.Deliver))
                    {
                        List<string> _stringLocation = new List<string>();

                        XPCollection<WarehouseSetupDetail> _locWhsSetupDetails = new XPCollection<WarehouseSetupDetail>
                                                                                  (Session, new GroupOperator(GroupOperatorType.And,
                                                                                   new BinaryOperator("UserAccess", _locUserAccess),
                                                                                   new BinaryOperator("ObjectList", ObjectList.InventoryTransferOrder),
                                                                                   new BinaryOperator("Owner", true),
                                                                                   new BinaryOperator("TransferType", this.TransferType),
                                                                                   new BinaryOperator("InventoryMovingType", this.InventoryMovingType),
                                                                                   new BinaryOperator("LocationType", LocationType.Transit),
                                                                                   new BinaryOperator("Active", true)));

                        if (_locWhsSetupDetails != null && _locWhsSetupDetails.Count() > 0)
                        {
                            foreach (WarehouseSetupDetail _locWhsSetupDetail in _locWhsSetupDetails)
                            {
                                if(_locWhsSetupDetail.Location != null)
                                {
                                    if(_locWhsSetupDetail.Location.Code != null)
                                    {
                                        _locLocationCode = _locWhsSetupDetail.Location.Code;
                                        _stringLocation.Add(_locLocationCode);
                                    }
                                }
                                
                            }
                        }

                        IEnumerable<string> _stringArrayLocationFromDistinct = _stringLocation.Distinct();
                        string[] _stringArrayLocationFromList = _stringArrayLocationFromDistinct.ToArray();
                        if (_stringArrayLocationFromList.Length == 1)
                        {
                            for (int i = 0; i < _stringArrayLocationFromList.Length; i++)
                            {
                                Location _locLocationFrom = Session.FindObject<Location>(new BinaryOperator("Code", _stringArrayLocationFromList[i]));
                                if (_locLocationFrom != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString = "[Code]=='" + _locLocationFrom.Code + "'";
                                    }
                                }
                            }
                        }
                        else if (_stringArrayLocationFromList.Length > 1)
                        {
                            for (int i = 0; i < _stringArrayLocationFromList.Length; i++)
                            {
                                Location _locLocationFrom = Session.FindObject<Location>(new BinaryOperator("Code", _stringArrayLocationFromList[i]));
                                if (_locLocationFrom != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString = "[Code]=='" + _locLocationFrom.Code + "'";
                                    }
                                    else
                                    {
                                        _endString = " OR [Code]=='" + _locLocationFrom.Code + "'";
                                    }
                                }
                            }
                        }
                        _fullString = _beginString + _endString;

                        if (_fullString != null)
                        {
                            _availableLocation = new XPCollection<Location>(Session, CriteriaOperator.Parse(_fullString));
                        }
                    }
                }
                else
                {
                    _availableLocation = new XPCollection<Location>(Session);
                }

                return _availableLocation;

            }
        }

        [Appearance("InventoryTransferOrderLocationClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceProperty("AvailableLocation", DataSourcePropertyIsNullMode.SelectAll)]
        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public Location Location
        {
            get { return _location; }
            set { SetPropertyValue("Location", ref _location, value); }
        }

        [Appearance("InventoryTransferOrderStockTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public StockType StockType
        {
            get { return _stockType; }
            set { SetPropertyValue("StockType", ref _stockType, value); }
        }

        [Appearance("InventoryTransferOrderStockControllingClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public bool StockControlling
        {
            get { return _stockControlling; }
            set { SetPropertyValue("StockControlling", ref _stockControlling, value); }
        }

        [Appearance("InventoryTransferOrderSplitInvoiceClose", Enabled = false)]
        public bool SplitInvoice
        {
            get { return _splitInvoice; }
            set { SetPropertyValue("SplitInvoice", ref _splitInvoice, value); }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        public bool EnabledHold
        {
            get { return _enabledHold; }
            set { SetPropertyValue("EnabledHold", ref _enabledHold, value); }
        }

        [Appearance("SalesOrderHoldClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Appearance("SalesOrderHoldEnabled", Criteria = "EnabledHold = false", Enabled = false)]
        public bool Hold
        {
            get { return _hold; }
            set
            {
                SetPropertyValue("Hold", ref _hold, value);
            }
        }

        [Appearance("InventoryTransferOrderCompanyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Appearance("InventoryTransferOrderProjectHeaderClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Association("ProjectHeader-InventoryTransferOrders")]
        public ProjectHeader ProjectHeader
        {
            get { return _projectHeader; }
            set { SetPropertyValue("ProjectHeader", ref _projectHeader, value); }
        }

        //Wahab 27-02-2020
        //[Association("PurchaseOrder-InventoryTransferOrders")]
        [Appearance("InventoryTransferOrderPurchaseOrderClose", Criteria = "ActivationPosting = true", Enabled = false)]        
        public PurchaseOrder PurchaseOrder
        {
            get { return _purchaseOrder; }
            set { SetPropertyValue("PurchaseOrder", ref _purchaseOrder, value); }
        }

        //Wahab 27-02-2020
        //[Association("SalesOrder-InventoryTransferOrders")]
        public SalesOrder SalesOrder
        {
            get { return _salesOrder; }
            set { SetPropertyValue("SalesOrder", ref _salesOrder, value); }
        }

        [Browsable(false)]
        public double MaxBill
        {
            get { return _maxBill; }
            set { SetPropertyValue("MaxBill", ref _maxBill, value); }
        }

        [Browsable(false)]
        public InventoryTransferOrder InternalInventoryTransferOrder
        {
            get { return _internalInventoryTransferOrder; }
            set { SetPropertyValue("InternalInventoryTransferOrder", ref _internalInventoryTransferOrder, value); }
        }

        [Association("InventoryTransferOrder-InventoryTransferOrderLines")]
        public XPCollection<InventoryTransferOrderLine> InventoryTransferOrderLines
        {
            get { return GetCollection<InventoryTransferOrderLine>("InventoryTransferOrderLines"); }
        }

        [Association("InventoryTransferOrder-ApprovalLines")]
        public XPCollection<ApprovalLine> ApprovalLines
        {
            get { return GetCollection<ApprovalLine>("ApprovalLines"); }
        }

        //Wahab 27-02-2020

        //[Association("InventoryTransferOrder-InventoryTransferOuts")]
        //public XPCollection<InventoryTransferOut> InventoryTransferOuts
        //{
        //    get { return GetCollection<InventoryTransferOut>("InventoryTransferOuts"); }
        //}

        //[Association("InventoryTransferOrder-InventoryTransferIns")]
        //public XPCollection<InventoryTransferIn> InventoryTransferIns
        //{
        //    get { return GetCollection<InventoryTransferIn>("InventoryTransferIns"); }
        //}

        //[Association("InventoryTransferOrder-TransferOrders")]
        //public XPCollection<TransferOrder> TransferOrders
        //{
        //    get { return GetCollection<TransferOrder>("TransferOrders"); }
        //}

        //clm
        [Browsable(false)]
        [Appearance("InventoryTransferOrderActiveApproved1Close", Enabled = false)]
        public bool ActiveApproved1
        {
            get { return _activeApproved1; }
            set { SetPropertyValue("ActiveApproved1", ref _activeApproved1, value); }
        }

        [Browsable(false)]
        [Appearance("InventoryTransferOrderActiveApproved2Close", Enabled = false)]
        public bool ActiveApproved2
        {
            get { return _activeApproved2; }
            set { SetPropertyValue("ActiveApproved2", ref _activeApproved2, value); }
        }

        [Browsable(false)]
        [Appearance("InventoryTransferOrderActiveApproved3Close", Enabled = false)]
        public bool ActiveApproved3
        {
            get { return _activeApproved3; }
            set { SetPropertyValue("ActiveApproved3", ref _activeApproved3, value); }
        }

        #endregion Field
        
    }
}