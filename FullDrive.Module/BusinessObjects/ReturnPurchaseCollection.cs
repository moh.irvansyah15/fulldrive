﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Purchase")]
    [RuleCombinationOfPropertiesIsUnique("ReturnPurchaseCollectionRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class ReturnPurchaseCollection : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private bool _activationPosting;
        private string _code;
        private XPCollection<PurchaseOrder> _availablePurchaseOrder;
        private PurchaseOrder _purchaseOrder;
        private XPCollection<InventoryTransferIn> _availableInventoryTransferIn;
        private InventoryTransferIn _inventoryTransferIn;
        private PurchaseReturn _purchaseReturn;
        private Status _status;
        private DateTime _statusDate;
        private int _postedCount;
        private GlobalFunction _globFunc;

        public ReturnPurchaseCollection(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.ReturnPurchaseCollection);
                DateTime now = DateTime.Now;
                this.Status = Status.Open;
                this.StatusDate = now;
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("ReturnPurchaseCollectionCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Browsable(false)]
        public XPCollection<PurchaseOrder> AvailablePurchaseOrder
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;

                if (!IsLoading)
                {
                    if (this.PurchaseReturn != null)
                    {
                        if (this.PurchaseReturn.BuyFromVendor != null)
                        {
                            #region InventoryTransferIn
                            if (this.InventoryTransferIn != null)
                            {
                                XPQuery<InventoryTransferInMonitoring> _inventoryTransferInMonitoringsQuery = new XPQuery<InventoryTransferInMonitoring>(Session);

                                var _inventoryTransferInMonitorings = from itim in _inventoryTransferInMonitoringsQuery
                                                                       where (itim.Status != Status.Open && itim.PostedCount > 0
                                                                       && itim.InventoryTransferIn.BusinessPartner == this.PurchaseReturn.BuyFromVendor
                                                                       && itim.InventoryTransferIn == this.InventoryTransferIn)
                                                                       group itim by itim.PurchaseOrderMonitoring.PurchaseOrder into g
                                                                       select new { PurchaseOrder = g.Key };

                                if (_inventoryTransferInMonitorings != null && _inventoryTransferInMonitorings.Count() > 0)
                                {
                                    List<string> _stringITIM = new List<string>();

                                    foreach (var _inventoryTransferInMonitoring in _inventoryTransferInMonitorings)
                                    {
                                        if (_inventoryTransferInMonitoring.PurchaseOrder != null)
                                        {
                                            if (_inventoryTransferInMonitoring.PurchaseOrder.Code != null)
                                            {
                                                _stringITIM.Add(_inventoryTransferInMonitoring.PurchaseOrder.Code);
                                            }
                                        }
                                    }

                                    IEnumerable<string> _stringArrayITIDistinct = _stringITIM.Distinct();
                                    string[] _stringArrayITIMList = _stringArrayITIDistinct.ToArray();
                                    if (_stringArrayITIMList.Length == 1)
                                    {
                                        for (int i = 0; i < _stringArrayITIMList.Length; i++)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _stringArrayITIMList[i] + "'";
                                            }
                                        }
                                    }
                                    else if (_stringArrayITIMList.Length > 1)
                                    {
                                        for (int i = 0; i < _stringArrayITIMList.Length; i++)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _stringArrayITIMList[i] + "'";
                                            }
                                            else
                                            {
                                                _endString = _endString + " OR [Code]=='" + _stringArrayITIMList[i] + "'";
                                            }
                                        }
                                    }
                                    _fullString = _beginString + _endString;

                                    if (_fullString != null)
                                    {
                                        _availablePurchaseOrder = new XPCollection<PurchaseOrder>(Session, CriteriaOperator.Parse(_fullString));
                                    }
                                }
                            }
                            #endregion InventoryTransferIn

                            #region UnInventoryTransferIn
                            else
                            {
                                XPQuery<InventoryTransferInMonitoring> _inventoryTransferInMonitoringsQuery = new XPQuery<InventoryTransferInMonitoring>(Session);

                                var _inventoryTransferInMonitorings = from itim in _inventoryTransferInMonitoringsQuery
                                                                       where (itim.Status != Status.Open && itim.PostedCount > 0
                                                                       && itim.InventoryTransferIn.BusinessPartner == this.PurchaseReturn.BuyFromVendor)
                                                                       group itim by itim.PurchaseOrderMonitoring.PurchaseOrder into g
                                                                       select new { PurchaseOrder = g.Key };

                                if (_inventoryTransferInMonitorings != null && _inventoryTransferInMonitorings.Count() > 0)
                                {
                                    List<string> _stringITIM = new List<string>();

                                    foreach (var _inventoryTransferInMonitoring in _inventoryTransferInMonitorings)
                                    {
                                        if (_inventoryTransferInMonitoring.PurchaseOrder != null)
                                        {
                                            if (_inventoryTransferInMonitoring.PurchaseOrder.Code != null)
                                            {
                                                _stringITIM.Add(_inventoryTransferInMonitoring.PurchaseOrder.Code);
                                            }
                                        }
                                    }

                                    IEnumerable<string> _stringArrayITIDistinct = _stringITIM.Distinct();
                                    string[] _stringArrayITIMList = _stringArrayITIDistinct.ToArray();
                                    if (_stringArrayITIMList.Length == 1)
                                    {
                                        for (int i = 0; i < _stringArrayITIMList.Length; i++)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _stringArrayITIMList[i] + "'";
                                            }
                                        }
                                    }
                                    else if (_stringArrayITIMList.Length > 1)
                                    {
                                        for (int i = 0; i < _stringArrayITIMList.Length; i++)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _stringArrayITIMList[i] + "'";
                                            }
                                            else
                                            {
                                                _endString = _endString + " OR [Code]=='" + _stringArrayITIMList[i] + "'";
                                            }
                                        }
                                    }
                                    _fullString = _beginString + _endString;

                                    if (_fullString != null)
                                    {
                                        _availablePurchaseOrder = new XPCollection<PurchaseOrder>(Session, CriteriaOperator.Parse(_fullString));
                                    }
                                }
                            }
                            #endregion UnInventoryTransferIn
                        }
                    }
                }
                return _availablePurchaseOrder;
            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailablePurchaseOrder", DataSourcePropertyIsNullMode.SelectNothing)]
        public PurchaseOrder PurchaseOrder
        {
            get { return _purchaseOrder; }
            set { SetPropertyValue("PurchaseOrder", ref _purchaseOrder, value); }
        }

        public XPCollection<InventoryTransferIn> AvailableInventoryTransferIn
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;

                if (!IsLoading)
                {
                    if (this.PurchaseReturn != null)
                    {
                        if (this.PurchaseReturn.BuyFromVendor != null)
                        {
                            #region PurchaseOrder
                            if (this.PurchaseOrder != null)
                            {
                                XPQuery<InventoryTransferInMonitoring> _inventoryTransferInMonitoringsQuery = new XPQuery<InventoryTransferInMonitoring>(Session);

                                var _inventoryTransferInMonitorings = from itim in _inventoryTransferInMonitoringsQuery
                                                                       where (itim.Status != Status.Open && itim.PostedCount > 0
                                                                       && itim.InventoryTransferIn.BusinessPartner == this.PurchaseReturn.BuyFromVendor
                                                                       && itim.PurchaseOrderMonitoring.PurchaseOrder == this.PurchaseOrder)
                                                                       group itim by itim.InventoryTransferIn into g
                                                                       select new { InventoryTransferIn = g.Key };

                                if (_inventoryTransferInMonitorings != null && _inventoryTransferInMonitorings.Count() > 0)
                                {
                                    List<string> _stringITIM = new List<string>();

                                    foreach (var _inventoryTransferInMonitoring in _inventoryTransferInMonitorings)
                                    {
                                        if (_inventoryTransferInMonitoring != null)
                                        {
                                            _stringITIM.Add(_inventoryTransferInMonitoring.InventoryTransferIn.Code);
                                        }
                                    }

                                    IEnumerable<string> _stringArrayITIDistinct = _stringITIM.Distinct();
                                    string[] _stringArrayITIMList = _stringArrayITIDistinct.ToArray();
                                    if (_stringArrayITIMList.Length == 1)
                                    {
                                        for (int i = 0; i < _stringArrayITIMList.Length; i++)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _stringArrayITIMList[i] + "'";
                                            }
                                        }
                                    }
                                    else if (_stringArrayITIMList.Length > 1)
                                    {
                                        for (int i = 0; i < _stringArrayITIMList.Length; i++)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _stringArrayITIMList[i] + "'";
                                            }
                                            else
                                            {
                                                _endString = _endString + " OR [Code]=='" + _stringArrayITIMList[i] + "'";
                                            }
                                        }
                                    }
                                    _fullString = _beginString + _endString;

                                    if (_fullString != null)
                                    {
                                        _availableInventoryTransferIn = new XPCollection<InventoryTransferIn>(Session, CriteriaOperator.Parse(_fullString));
                                    }
                                }
                            }
                            #endregion PurchaseOrder

                            #region UnPurchaseOrder
                            else
                            {
                                XPQuery<InventoryTransferInMonitoring> _inventoryTransferInMonitoringsQuery = new XPQuery<InventoryTransferInMonitoring>(Session);

                                var _inventoryTransferInMonitorings = from itim in _inventoryTransferInMonitoringsQuery
                                                                       where (itim.Status != Status.Open && itim.PostedCount > 0
                                                                       && itim.InventoryTransferIn.BusinessPartner == this.PurchaseReturn.BuyFromVendor)
                                                                       group itim by itim.InventoryTransferIn into g
                                                                       select new { InventoryTransferIn = g.Key };

                                if (_inventoryTransferInMonitorings != null && _inventoryTransferInMonitorings.Count() > 0)
                                {
                                    List<string> _stringITIM = new List<string>();

                                    foreach (var _inventoryTransferInMonitoring in _inventoryTransferInMonitorings)
                                    {
                                        if (_inventoryTransferInMonitoring != null)
                                        {
                                            _stringITIM.Add(_inventoryTransferInMonitoring.InventoryTransferIn.Code);
                                        }
                                    }

                                    IEnumerable<string> _stringArrayITIDistinct = _stringITIM.Distinct();
                                    string[] _stringArrayITIMList = _stringArrayITIDistinct.ToArray();
                                    if (_stringArrayITIMList.Length == 1)
                                    {
                                        for (int i = 0; i < _stringArrayITIMList.Length; i++)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _stringArrayITIMList[i] + "'";
                                            }
                                        }
                                    }
                                    else if (_stringArrayITIMList.Length > 1)
                                    {
                                        for (int i = 0; i < _stringArrayITIMList.Length; i++)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _stringArrayITIMList[i] + "'";
                                            }
                                            else
                                            {
                                                _endString = _endString + " OR [Code]=='" + _stringArrayITIMList[i] + "'";
                                            }
                                        }
                                    }
                                    _fullString = _beginString + _endString;

                                    if (_fullString != null)
                                    {
                                        _availableInventoryTransferIn = new XPCollection<InventoryTransferIn>(Session, CriteriaOperator.Parse(_fullString));
                                    }
                                }
                            }
                            #endregion UnPurchaseOrder    
                        }
                    }
                }
                return _availableInventoryTransferIn;
            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableInventoryTransferIn", DataSourcePropertyIsNullMode.SelectNothing)]
        public InventoryTransferIn InventoryTransferIn
        {
            get { return _inventoryTransferIn; }
            set { SetPropertyValue("InventoryTransferIn", ref _inventoryTransferIn, value); }
        }

        [ImmediatePostData()]
        [Appearance("ReturnPurchaseCollectionSalesInvoiceClose", Enabled = false)]
        [Association("PurchaseReturn-ReturnPurchaseCollections")]
        public PurchaseReturn PurchaseReturn
        {
            get { return _purchaseReturn; }
            set { SetPropertyValue("PurchaseReturn", ref _purchaseReturn, value); }
        }

        [Appearance("ReturnPurchaseCollectionStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("ReturnPurchaseCollectionStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("ReturnPurchaseCollectionPostedCountEnabled", Enabled = false)]
        public int PostedCount
        {
            get { return _postedCount; }
            set { SetPropertyValue("PostedCount", ref _postedCount, value); }
        }

        #endregion Field

        //private string _PersistentProperty;
        //[XafDisplayName("My display name"), ToolTip("My hint message")]
        //[ModelDefault("EditMask", "(000)-00"), Index(0), VisibleInListView(false)]
        //[Persistent("DatabaseColumnName"), RuleRequiredField(DefaultContexts.Save)]
        //public string PersistentProperty {
        //    get { return _PersistentProperty; }
        //    set { SetPropertyValue("PersistentProperty", ref _PersistentProperty, value); }
        //}

        //[Action(Caption = "My UI Action", ConfirmationMessage = "Are you sure?", ImageName = "Attention", AutoCommit = true)]
        //public void ActionMethod() {
        //    // Trigger a custom business logic for the current record in the UI (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112619.aspx).
        //    this.PersistentProperty = "Paid";
        //}
    }
}