﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Cost Of Goods Sold")]
    [RuleCombinationOfPropertiesIsUnique("CostOfGoodsSoldRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class CostOfGoodsSold : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private int _no;
        private string _code;
        private Item _item;
        private MaterialPrice _material;
        private double _materialPrice;
        private OverheadCost _overhead;
        private double _overheadCostElectric;
        private double _overheadCostEmployee;
        private double _overheadCostDepreciation;
        private double _packing;
        private double _avalan;
        private double _costOfFund;
        private double _hedgingCost;
        private double _wasteCost;
        private double _fixCost;
        private double _totalHpp;
        private GlobalFunction _globFunc;

        public CostOfGoodsSold(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if(!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(Session.DataLayer, ObjectList.CostOfGoodsSold);
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        protected override void OnSaving()
        {
            base.OnSaving();
            if (!IsLoading && IsSaving)
            {
                UpdateNo();
            }
        }

        protected override void OnDeleting()
        {
            base.OnDeleting();
            RecoveryDeleteNo();
        }

        [Appearance("CostOfGoodsSoldNoEnabled", Enabled = false)]
        public int No
        {
            get { return _no; }
            set { SetPropertyValue("No", ref _no, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Association("Item-CostOfGoodsSolds")]
        [DataSourceCriteria("Active = true")]
        public Item Item
        {
            get { return _item; }
            set { SetPropertyValue("Item", ref _item, value); }
        }

        [ImmediatePostData()]
        public MaterialPrice Material
        {
            get { return _material; }
            set
            {
                SetPropertyValue("Material", ref _material, value);
                if(!IsLoading)
                {
                    if(this._materialPrice != null)
                    {
                        this.MaterialPrice = _material.TotalMaterialPrice;
                    }
                }
            }
        }

        [ImmediatePostData()]
        public double MaterialPrice
        {
            get { return _materialPrice; }
            set
            {
                SetPropertyValue("MaterialPrice", ref _materialPrice, value);
                if(!IsLoading)
                {
                    SetTotalHPP();
                }
            }
        }

        [ImmediatePostData()]
        public OverheadCost Overhead
        {
            get { return _overhead; }
            set
            {
                SetPropertyValue("Overhead", ref _overhead, value);
                if(!IsLoading)
                {
                    if(this._overhead != null)
                    {
                        this.OverheadCostElectric = this._overhead.TotalOverheadCostElectric;
                        this.OverheadCostEmployee = this._overhead.TotalOverheadCostEmployee;
                        this.OverheadCostDepreciation = this._overhead.TotalOverheadCostDepreciation;
                    }
                }
            }
        }

        [ImmediatePostData()]
        public double OverheadCostElectric
        {
            get { return _overheadCostElectric; }
            set
            {
                SetPropertyValue("OverheadCostElectric", ref _overheadCostElectric, value);
                if (!IsLoading)
                {
                    SetTotalHPP();
                }
            }
        }

        [ImmediatePostData()]
        public double OverheadCostEmployee
        {
            get { return _overheadCostEmployee; }
            set
            {
                SetPropertyValue("OverheadCostEmployee", ref _overheadCostEmployee, value);
                if (!IsLoading)
                {
                    SetTotalHPP();
                }
            }
        }

        [ImmediatePostData()]
        public double OverheadCostDepreciation
        {
            get { return _overheadCostDepreciation; }
            set
            {
                SetPropertyValue("OverheadCostDepreciation", ref _overheadCostDepreciation, value);
                if (!IsLoading)
                {
                    SetTotalHPP();
                }
            }
        }

        [ImmediatePostData()]
        public double Packing
        {
            get { return _packing; }
            set
            {
                SetPropertyValue("Packing", ref _packing, value);
                if (!IsLoading)
                {
                    SetTotalHPP();
                }
            }
        }

        [ImmediatePostData()]
        public double Avalan
        {
            get { return _avalan; }
            set
            {
                SetPropertyValue("Avalan", ref _avalan, value);
                if (!IsLoading)
                {
                    SetTotalHPP();
                }
            }
        }

        [ImmediatePostData()]
        public double CostOfFund
        {
            get { return _costOfFund; }
            set
            {
                SetPropertyValue("CostOfFund", ref _costOfFund, value);
                if (!IsLoading)
                {
                    SetTotalHPP();
                }
            }
        }

        [ImmediatePostData()]
        public double HedgingCost
        {
            get { return _hedgingCost; }
            set
            {
                SetPropertyValue("HedgingCost", ref _hedgingCost, value);
                if (!IsLoading)
                {
                    SetTotalHPP();
                }
            }
        }

        [ImmediatePostData()]
        public double WasteCost
        {
            get { return _wasteCost; }
            set
            {
                SetPropertyValue("WasteCost", ref _wasteCost, value);
                if (!IsLoading)
                {
                    SetTotalHPP();
                }
            }
        }

        [ImmediatePostData()]
        public double FixCost
        {
            get { return _fixCost; }
            set
            {
                SetPropertyValue("FixCost", ref _fixCost, value);
                if (!IsLoading)
                {
                    SetTotalHPP();
                }
            }
        }

        public double TotalHPP
        {
            get { return _totalHpp; }
            set { SetPropertyValue("TotalHPP", ref _totalHpp, value); }
        }

        //private string _PersistentProperty;
        //[XafDisplayName("My display name"), ToolTip("My hint message")]
        //[ModelDefault("EditMask", "(000)-00"), Index(0), VisibleInListView(false)]
        //[Persistent("DatabaseColumnName"), RuleRequiredField(DefaultContexts.Save)]
        //public string PersistentProperty {
        //    get { return _PersistentProperty; }
        //    set { SetPropertyValue("PersistentProperty", ref _PersistentProperty, value); }
        //}

        //[Action(Caption = "My UI Action", ConfirmationMessage = "Are you sure?", ImageName = "Attention", AutoCommit = true)]
        //public void ActionMethod() {
        //    // Trigger a custom business logic for the current record in the UI (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112619.aspx).
        //    this.PersistentProperty = "Paid";
        //}

        //=============================================== Code In Here ===============================================

        #region Numbering
        public void UpdateNo()
        {
            try
            {
                if (!IsLoading)
                {
                    if (!(Session is NestedUnitOfWork) && Session.IsNewObject(this))
                    {
                        if (this.Item != null)
                        {
                            object _makRecord = Session.Evaluate<CostOfGoodsSold>(CriteriaOperator.Parse("Max(No)"),
                                                CriteriaOperator.Parse("Item=?", this.Item));
                            this.No = Convert.ToInt32(_makRecord) + 1;
                            this.Save();
                            RecoveryUpdateNo();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CostOfGoodsSold " + ex.ToString());
            }
        }

        public void RecoveryUpdateNo()
        {
            try
            {
                if (this.Item != null)
                {
                    Item _numHeader = Session.FindObject<Item>
                                                (new BinaryOperator("Code", this.Item.Code));

                    XPCollection<CostOfGoodsSold> _numLines = new XPCollection<CostOfGoodsSold>
                                                (Session, new BinaryOperator("Item", _numHeader),
                                                new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i1 = 0;
                        foreach (CostOfGoodsSold _numLine in _numLines)
                        {
                            i1 += 1;
                            _numLine.No = i1;
                            _numLine.Save();
                        }
                        i1 = 1;
                        //Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CostOfGoodsSold " + ex.ToString());
            }
        }

        public void RecoveryDeleteNo()
        {
            try
            {
                if (this.Item != null)
                {
                    Item _numHeader = Session.FindObject<Item>
                                                (new BinaryOperator("Code", this.Item.Code));

                    XPCollection<CostOfGoodsSold> _numLines = new XPCollection<CostOfGoodsSold>
                                                (Session, new GroupOperator(GroupOperatorType.And,
                                                 new BinaryOperator("This", this, BinaryOperatorType.NotEqual),
                                                 new BinaryOperator("Item", _numHeader)),
                                                 new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i = 0;
                        foreach (CostOfGoodsSold _numLine in _numLines)
                        {
                            i += 1;
                            _numLine.No = i;
                            _numLine.Save();
                        }
                        //Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CostOfGoodsSold " + ex.ToString());
            }
        }
        #endregion Numbering

        private void SetTotalHPP()
        {
            try
            {
                this._totalHpp = this._materialPrice + this._overheadCostElectric + this._overheadCostEmployee + this._overheadCostDepreciation + this._packing
                                 + this._avalan + this._costOfFund + this._hedgingCost + this._wasteCost + this._fixCost;
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CostOfGoodsSold " + ex.ToString());
            }
        }

    }
}