﻿#region Default

using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

#endregion Default

using FullDrive.Module.CustomProcess;

namespace FullDrive.Module.BusinessObjects
{
    #region Default

    [DefaultClassOptions]
    [DefaultProperty("SetupName")]
    [NavigationItem("Setup")]

    #endregion Default

    // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
    public class ApplicationSetup : FullDriveSysBaseObject
    {
        #region Default

        private string _setupCode;
        private string _setupName;
        private bool _active;
        private bool _defaultSystem;

        public ApplicationSetup(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        protected override void OnSaving()
        {
            base.OnSaving();
            if (this.DefaultSystem == true)
            {
                CheckDefaultSystem();
            }
        }

        #endregion Default

        #region Field

        public string SetupCode
        {
            get { return _setupCode; }
            set { SetPropertyValue("SetupCode", ref _setupCode, value); }
        }

        public string SetupName
        {
            get { return _setupName; }
            set { SetPropertyValue("SetupName", ref _setupName, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue("Active", ref _active, value); }
        }

        public bool DefaultSystem
        {
            get { return _defaultSystem; }
            set { SetPropertyValue("DefaultSystem", ref _defaultSystem, value); }
        }

        [Association("ApplicationSetup-NumberingHeaders")]
        public XPCollection<NumberingHeader> NumberingHeaders
        {
            get { return GetCollection<NumberingHeader>("NumberingHeaders"); }
        }

        [Association("ApplicationSetup-ApplicationSetupDetails")]
        public XPCollection<ApplicationSetupDetail> ApplicationSetupDetails
        {
            get { return GetCollection<ApplicationSetupDetail>("ApplicationSetupDetails"); }
        }

        [Association("ApplicationSetup-RoundingSetupDetails")]
        public XPCollection<RoundingSetupDetail> RoundingSetupDetails
        {
            get { return GetCollection<RoundingSetupDetail>("RoundingSetupDetails"); }
        }

        [Association("ApplicationSetup-WarehouseSetupDetails")]
        public XPCollection<WarehouseSetupDetail> WarehouseSetupDetails
        {
            get { return GetCollection<WarehouseSetupDetail>("WarehouseSetupDetails"); }
        }

        [Association("ApplicationSetup-OrganizationSetupDetails")]
        public XPCollection<OrganizationSetupDetail> OrganizationSetupDetails
        {
            get { return GetCollection<OrganizationSetupDetail>("OrganizationSetupDetails"); }
        }

        [Association("ApplicationSetup-PurchaseSetupDetails")]
        public XPCollection<PurchaseSetupDetail> PurchaseSetupDetails
        {
            get { return GetCollection<PurchaseSetupDetail>("PurchaseSetupDetails"); }
        }

        [Association("ApplicationSetup-SalesSetupDetails")]
        public XPCollection<SalesSetupDetail> SalesSetupDetails
        {
            get { return GetCollection<SalesSetupDetail>("SalesSetupDetails"); }
        }

        //clm
        [Association("ApplicationSetup-MailSetupDetails")]
        public XPCollection<MailSetupDetail> MailSetupDetails
        {
            get { return GetCollection<MailSetupDetail>("MailSetupDetails"); }
        }

        #endregion Field

        //===== Code Only =====

        private void CheckDefaultSystem()
        {
            try
            {
                XPCollection<ApplicationSetup> _appSetups = new XPCollection<ApplicationSetup>(Session, new BinaryOperator("This", this, BinaryOperatorType.NotEqual));
                if (_appSetups == null)
                {
                    return;
                }
                else
                {
                    foreach (ApplicationSetup _appSetup in _appSetups)
                    {
                        _appSetup.DefaultSystem = false;
                        _appSetup.Save();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business Object = ApplicationSetup", ex.ToString());
            }
        }

    }
}