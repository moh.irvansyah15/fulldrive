﻿#region Default

using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

#endregion Default

using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.Editors;

namespace FullDrive.Module.BusinessObjects
{
    #region Default

    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Purchase")]
    [RuleCombinationOfPropertiesIsUnique("PurchaseRequisitionRuleUnique", DefaultContexts.Save, "Code")]

    #endregion Default

    public class PurchaseRequisition : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).

        #region Default

        private bool _activationPosting;
        private string _code;
        private string _description;
        private DateTime _requestDate;
        private DateTime _requiredDate;
        private FileData _attachment;
        private DirectionType _transferType;
        private Status _status;
        private DateTime _statusDate;
        private PrePurchaseOrder _prePurchaseOrder;
        private ProjectHeader _projectHeader;
        private string _signCode;
        private Company _company;
        private Division _division;
        private Department _department;
        private Section _section;
        private Employee _employee;
        private string _userAccess;
        private GlobalFunction _globFunc;
        private string _approvalDate1;
        private string _approvalDate2;
        //clm
        private bool _activeApproved1;
        private bool _activeApproved2;
        private bool _activeApproved3;

        public PurchaseRequisition(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).

            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.PurchaseRequisition);
                DateTime now = DateTime.Now;
                this.RequestDate = DateTime.Now;
                //this.RequiredDate = now;
                this.Status = CustomProcess.Status.Open;
                this.StatusDate = now;

                #region UserAccess
                _userAccess = SecuritySystem.CurrentUserName;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                if (Session.IsNewObject(this))
                {
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            this.Employee = _locUserAccess.Employee;

                            if (_locUserAccess.Employee.Company != null)
                            {
                                Company = _locUserAccess.Employee.Company;
                            }
                            if (_locUserAccess.Employee.Division != null)
                            {
                                Division = _locUserAccess.Employee.Division;
                            }
                            if (_locUserAccess.Employee.Department != null)
                            {
                                Department = _locUserAccess.Employee.Department;
                            }
                            if (_locUserAccess.Employee.Section != null)
                            {
                                Section = _locUserAccess.Employee.Section;
                            }
                        }
                    }
                }
                #endregion UserAccess
            }
        }

        #endregion Default

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [ImmediatePostData()]
        [Appearance("PurchaseRequisitionCodeClose", Enabled = false)]
        [Appearance("PurchaseRequisitionRedColor", Criteria = "ActiveApproved1 = true", BackColor = "#fd79a8")]
        [Appearance("PurchaseRequisitionYellowColor", Criteria = "ActiveApproved2 = true", BackColor = "#ffeaa7")]
        [Appearance("PurchaseRequisitionGreenOColor", Criteria = "ActiveApproved3 = true", BackColor = "#81ecec")]
        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("ProjectRequisitionCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("PurchaseRequisitionDescriptionClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Size(512)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        [Appearance("PurchaseRequisitionRequestDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime RequestDate
        {
            get { return _requestDate; }
            set { SetPropertyValue("RequestDate", ref _requestDate, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("PurchaseRequisitionRequiredDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime RequiredDate
        {
            get { return _requiredDate; }
            set { SetPropertyValue("RequiredDate", ref _requiredDate, value); }
        }

        [Appearance("PurchaseRequisitionAttachmentClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public FileData Attachment
        {
            get { return _attachment; }
            set { SetPropertyValue("Attachment", ref _attachment, value); }
        }

        [Appearance("PurchaseRequisitionTransferTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DirectionType TransferType
        {
            get { return _transferType; }
            set { SetPropertyValue("TransferType", ref _transferType, value); }
        }

        [Appearance("ProjectRequisitionStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("ProjectRequisitionStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("PurchaseRequisitionPrePurchaseOrderClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Association("PrePurchaseOrder-PurchaseRequisitions")]
        public PrePurchaseOrder PrePurchaseOrder
        {
            get { return _prePurchaseOrder; }
            set { SetPropertyValue("PrePurchaseOrder", ref _prePurchaseOrder, value); }
        }

        [Appearance("PurchaseRequisitionProjectHeaderClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Association("ProjectHeader-PurchaseRequisitions")]
        public ProjectHeader ProjectHeader
        {
            get { return _projectHeader; }
            set { SetPropertyValue("ProjectHeader", ref _projectHeader, value); }
        }

        [Appearance("PurchaseRequisitionSignCodeClose", Enabled = false)]
        public string SignCode
        {
            get { return _signCode; }
            set { SetPropertyValue("SignCode", ref _signCode, value); }
        }

        [Appearance("PurchaseRequisitionEmployeeClose", Enabled = false)]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue("Employee", ref _employee, value); }
        }

        [Appearance("PurchaseRequisitionCompanyClose", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Browsable(false)]
        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        [Browsable(false)]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        [Browsable(false)]
        public Section Section
        {
            get { return _section; }
            set { SetPropertyValue("Section", ref _section, value); }
        }
        
        [Association("PurchaseRequisition-PurchaseRequisitionLines")]
        public XPCollection<PurchaseRequisitionLine> PurchaseRequisitionLines
        {
            get { return GetCollection<PurchaseRequisitionLine>("PurchaseRequisitionLines"); }
        }

        [Association("PurchaseRequisition-ApprovalLines")]
        public XPCollection<ApprovalLine> ApprovalLines
        {
            get { return GetCollection<ApprovalLine>("ApprovalLines"); }
        }

        //clm
        [Browsable(false)]
        [Appearance("PurchaseRequisitionActiveApproved1Close", Enabled = false)]
        public bool ActiveApproved1
        {
            get { return _activeApproved1; }
            set { SetPropertyValue("ActiveApproved1", ref _activeApproved1, value); }
        }

        [Browsable(false)]
        [Appearance("PurchaseRequisitionActiveApproved2Close", Enabled = false)]
        public bool ActiveApproved2
        {
            get { return _activeApproved2; }
            set { SetPropertyValue("ActiveApproved2", ref _activeApproved2, value); }
        }

        [Browsable(false)]
        [Appearance("PurchaseRequisitionActiveApproved3Close", Enabled = false)]
        public bool ActiveApproved3
        {
            get { return _activeApproved3; }
            set { SetPropertyValue("ActiveApproved3", ref _activeApproved3, value); }
        }

        [ImmediatePostData()]
        [Appearance("PurchaseRequisitionApprovalDate1Enabled", Enabled = false, Visibility = ViewItemVisibility.Hide)]
        public string ApprovalDate1
        {
            get
            {
                if (this._approvalDate1 == null)
                {
                    return GetApprovalDate1();
                }
                else
                {
                    return null;
                }
            }
        }
        
        [ImmediatePostData()]
        [Appearance("PurchaseRequisitionApprovalDate2Enabled", Enabled = false, Visibility = ViewItemVisibility.Hide)]
        public string ApprovalDate2
        {
            get
            {
                if (this._approvalDate2 == null)
                {
                    return GetApprovalDate2();
                }
                else
                {
                    return null;
                }
            }
        }

        #endregion Field

        #region ApprovalDate
        public string GetApprovalDate1()
        {
            string _result = null;
            try
            {
                DateTime _date;
                if (!IsLoading)
                {
                    XPCollection<ApprovalLine> _locApprovalLines = new XPCollection<ApprovalLine>(Session,
                                                                       new BinaryOperator("PurchaseRequisition", this));

                    if (_locApprovalLines.Count() > 0)
                    {
                        foreach (ApprovalLine _locApprovalLine in _locApprovalLines)
                        {
                            if (_locApprovalLine.ApprovalLevel == ApprovalLevel.Level1)
                            {
                                _date = _locApprovalLine.ApprovalDate;
                                _result = _date.ToString("dd/MM/yyyy HH:mm:ss");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PurchaseRequisition" + ex.ToString());
            }
            return _result;
        }

        public string GetApprovalDate2()
        {
            string _result = null;
            try
            {
                DateTime _date;
                if (!IsLoading)
                {
                    XPCollection<ApprovalLine> _locApprovalLines = new XPCollection<ApprovalLine>(Session,
                                                                       new BinaryOperator("PurchaseRequisition", this));

                    if (_locApprovalLines.Count() > 0)
                    {
                        foreach (ApprovalLine _locApprovalLine in _locApprovalLines)
                        {
                            if (_locApprovalLine.ApprovalLevel == ApprovalLevel.Level2)
                            {
                                _date = _locApprovalLine.ApprovalDate;
                                _result = _date.ToString("dd/MM/yyyy HH:mm:ss");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PurchaseRequisition" + ex.ToString());
            }
            return _result;
        }


        #endregion ApprovalDate

        [Action(Caption = "UpdateRequestDate", ConfirmationMessage = "Are you sure?", AutoCommit = true)]
        public void UpdatePR()
        {
            try
            {
                PurchaseRequisition _locUpdate = Session.FindObject<PurchaseRequisition>(new BinaryOperator("CreateDate", this.CreateDate));

                if (_locUpdate != null)
                {
                    if (_locUpdate.CreateDate != null)
                    {
                        this.RequestDate = _locUpdate.CreateDate;
                        this.Save();
                        this.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business Object = PurchaseRequisition ", ex.ToString());
            }
        }

    }
}