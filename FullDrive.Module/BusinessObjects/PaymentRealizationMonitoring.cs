﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.SystemModule;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Editors;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("PaymentRealizationMonitoringRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class PaymentRealizationMonitoring : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private string _code;
        private PaymentRealization _paymentRealization;
        private PaymentRealizationLine _paymentRealizationLine;
        private CashAdvanceType _cashAdvanceType;
        private Employee _employee;
        private double _amount;
        private double _amountRealized;
        private string _description;
        private Company _company;
        private Division _division;
        private Department _department;
        private Section _section;
        private Status _status;
        private DateTime _statusDate;
        private int _processCount;
        private string _signCode;
        private CashAdvance _cashAdvance;
        private CashAdvanceMonitoring _cashAdvanceMonitoring;
        private GlobalFunction _globFunc;

        public PaymentRealizationMonitoring(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            _globFunc = new GlobalFunction();
            DateTime now = DateTime.Now;
            this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.PaymentRealizationMonitoring);
            this.Status = Status.Open;
            this.StatusDate = now;
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("PaymentRealizationMonitoringCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("PaymentRealizationMonitoringPaymentRealizationClose", Enabled = false)]
        public PaymentRealization PaymentRealization
        {
            get { return _paymentRealization; }
            set { SetPropertyValue("PaymentRealization", ref _paymentRealization, value); }
        }

        [Appearance("PaymentRealizationMonitoringPaymentRealizationLineClose", Enabled = false)]
        public PaymentRealizationLine PaymentRealizationLine
        {
            get { return _paymentRealizationLine; }
            set { SetPropertyValue("PaymentRealizationLine", ref _paymentRealizationLine, value); }
        }

        [Appearance("PaymentRealizationMonitoringCashAdvanceTypeClose", Enabled = false)]
        public CashAdvanceType CashAdvanceType
        {
            get { return _cashAdvanceType; }
            set { SetPropertyValue("CashAdvanceType", ref _cashAdvanceType, value); }
        }

        [Appearance("PaymentRealizationMonitoringEmployeeClose", Enabled = false)]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue("Employee", ref _employee, value); }
        }

        [Appearance("PaymentRealizationMonitoringAmountClose", Enabled = false)]
        public double Amount
        {
            get { return _amount; }
            set { SetPropertyValue("Amount", ref _amount, value); }
        }

        [Appearance("PaymentRealizationMonitoringAmountRealizedClose", Enabled = false)]
        public double AmountRealized
        {
            get { return _amountRealized; }
            set { SetPropertyValue("AmountRealized", ref _amountRealized, value); }
        }

        [Size(512)]
        [Appearance("PaymentRealizationMonitoringDescriptionClose", Enabled = false)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        [Appearance("PaymentRealizationMonitoringCompanyClose", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Appearance("PaymentRealizationMonitoringDivisionClose", Enabled = false)]
        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        [Appearance("PaymentRealizationMonitoringDepartmentClose", Enabled = false)]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        [Appearance("PaymentRealizationMonitoringSectionClose", Enabled = false)]
        public Section Section
        {
            get { return _section; }
            set { SetPropertyValue("Section", ref _section, value); }
        }

        [Appearance("PaymentRealizationMonitoringSignCodeClose", Enabled = false)]
        public string SignCode
        {
            get { return _signCode; }
            set { SetPropertyValue("SignCode", ref _signCode, value); }
        }

        [Appearance("PaymentRealizationMonitoringStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("PaymentRealizationMonitoringStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("PaymentRealizationMonitoringProcessCountClose", Enabled = false)]
        public int ProcessCount
        {
            get { return _processCount; }
            set { SetPropertyValue("ProcessCount", ref _processCount, value); }
        }

        [Appearance("PaymentRealizationMonitoringCashAdvanceClose", Enabled = false)]
        public CashAdvance CashAdvance
        {
            get { return _cashAdvance; }
            set { SetPropertyValue("CashAdvance", ref _cashAdvance, value); }
        }

        [Appearance("PaymentRealizationMonitoringCashAdvanceMonitoringClose", Enabled = false)]
        public CashAdvanceMonitoring CashAdvanceMonitoring
        {
            get { return _cashAdvanceMonitoring; }
            set { SetPropertyValue("CashAdvanceMonitoring", ref _cashAdvanceMonitoring, value); }
        }

        #endregion Field
    }
}