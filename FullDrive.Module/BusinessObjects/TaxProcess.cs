﻿        #region Default
using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Xpo;
using DevExpress.ExpressApp.ConditionalAppearance;
using System.Collections;
using FullDrive.Module.CustomProcess;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    [NavigationItem("Tax")]
    [RuleCombinationOfPropertiesIsUnique("TaxProcessRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class TaxProcess : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private string _code;
        private string _name;
        private Employee _employee;
        private Period _salaryPeriod;
        private string _npwp;
        private string _ptkpStatus;
        private double _ptkpValue;
        private TaxCalculationMethod _taxMethod;
        private double _totalMonth;
        private double _irregularTax;
        private string _irregularTaxText;
        private double _currentMonthTax;
        private string _currentMonthTaxText;
        private double _totalTaxUntilLastMonth;
        private string _totalTaxUntilLastMonthText;
        private double _totalTaxUntilCurrentMonth;
        private string _totalTaxUntilCurrentMonthText;
        private double _taxYear;
        private double _taxMonth;
        private double _kolom1;
        private string _kolom1Text;
        private double _kolom2;
        private string _kolom2Text;
        private double _kolom3;
        private string _kolom3Text;
        private double _kolom4;
        private string _kolom4Text;
        private double _kolom5;
        private string _kolom5Text;
        private double _kolom6;
        private string _kolom6Text;
        private double _kolom7;
        private string _kolom7Text;
        private double _kolom8;
        private string _kolom8Text;
        private double _kolom9;
        private string _kolom9Text;
        private double _kolom10;
        private string _kolom10Text;
        private double _kolom11;
        private string _kolom11Text;
        private double _kolom12;
        private string _kolom12Text;
        private double _kolom13;
        private string _kolom13Text;
        private double _kolom14;
        private string _kolom14Text;
        private double _kolom15;
        private string _kolom15Text;
        private double _kolom16;
        private string _kolom16Text;
        private double _kolom17;
        private string _kolom17Text;
        private double _kolom18;
        private string _kolom18Text;
        private double _kolom19;
        private string _kolom19Text;
        private double _kolom20;
        private string _kolom20Text;
        private double _kolom21;
        private string _kolom21Text;
        private double _kolom22;
        private string _kolom22Text;
        private double _kolom23;
        private string _kolom23Text;
        private GlobalFunction _globFunc;
        public TaxProcess(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                this._globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.TaxProcess);
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }
        #endregion

        #region OnSaving
        protected override void OnSaving()
        {
            base.OnSaving();
        }
        #endregion

        #region Onchanged
        protected override void OnChanged(string propertyName, object oldValue, object newValue)
        {
            base.OnChanged(propertyName, oldValue, newValue);
            switch (propertyName)
            {
                case "Period":
                    {
                        if (this.Employee != null)
                        {
                            TaxProcess o = Session.FindObject<TaxProcess>(new GroupOperator(GroupOperatorType.And, new BinaryOperator("Period", this.Period), new BinaryOperator("Employee", this.Employee)));
                            if (o != null)
                            {
                                //MsgBox["Data already exists"];
                                this.CancelEdit();
                                this.Reload();
                            }
                        }

                        break;
                    }

                case "Employee":
                    {
                        if (this.Period != null)
                        {
                            TaxProcess o = Session.FindObject<TaxProcess>(new GroupOperator(GroupOperatorType.And, new BinaryOperator("Period", this.Period), new BinaryOperator("Employee", this.Employee)));
                            if (o != null)
                            {
                                //MsgBox["Data already exists"];
                                this.CancelEdit();
                                this.Reload();
                            }
                        }

                        break;
                    }
            }
        }
        #endregion

        #region Field
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        //public string Name
        //{
        //    get { return _name; }
        //    set { SetPropertyValue("Name", ref _name, value); }
        //}

        [Association("Employee-TaxProcesses")]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue("Employee", ref _employee, value); }
        }

        [Association("Period-TaxProcesses")]
        public Period Period
        {
            get { return _salaryPeriod; }
            set { SetPropertyValue("Period", ref _salaryPeriod, value); }
        }

        public string NPWP
        {
            get { return _npwp; }
            set { SetPropertyValue("NPWP", ref _npwp, value); }
        }

        [ImmediatePostData()]
        public string PTKPStatus
        {
            get { return _ptkpStatus; }
            set {
                    SetPropertyValue("PTKPStatus", ref _ptkpStatus, value);
                    //SetPTKPValue();
                }
        }

        public double PTKPValue
        {
            get { return _ptkpValue; }
            set { SetPropertyValue("PTKPValue", ref _ptkpValue, value); }
        }

        public TaxCalculationMethod TaxMethod
        {
            get { return _taxMethod; }
            set { SetPropertyValue("TaxMethod", ref _taxMethod, value); }
        }

        public double TotalMonth
        {
            get { return _totalMonth; }
            set { SetPropertyValue("TotalMonth", ref _totalMonth, value); }
        }

        public double IrregularTax
        {
            get { return _irregularTax; }
            set { SetPropertyValue("IrregularTax", ref _irregularTax, value); }
        }

        public string IrregularTaxText
        {
            get { return _irregularTaxText; }
            set { SetPropertyValue("IrregularTaxText", ref _irregularTaxText, value); }
        }

        public double CurrentMonthTax
        {
            get { return _currentMonthTax; }
            set { SetPropertyValue("CurrentMonthTax", ref _currentMonthTax, value); }
        }

        public string CurrentMonthTaxText
        {
            get { return _currentMonthTaxText; }
            set { SetPropertyValue("CurrentMonthTaxText", ref _currentMonthTaxText, value); }
        }

        public double TotalTaxUntilLastMonth
        {
            get { return _totalTaxUntilLastMonth; }
            set { SetPropertyValue("TotalTaxUntilLastMonth", ref _totalTaxUntilLastMonth, value); }
        }

        public string TotalTaxUntilLastMonthText
        {
            get { return _totalTaxUntilLastMonthText; }
            set { SetPropertyValue("TotalTaxUntilLastMonthText", ref _totalTaxUntilLastMonthText, value); }
        }

        public double TotalTaxUntilCurrentMonth
        {
            get { return _totalTaxUntilCurrentMonth; }
            set { SetPropertyValue("TotalTaxUntilCurrentMonth", ref _totalTaxUntilCurrentMonth, value); }
        }

        public string TotalTaxUntilCurrentMonthText
        {
            get { return _totalTaxUntilCurrentMonthText; }
            set { SetPropertyValue("TotalTaxUntilCurrentMonthText", ref _totalTaxUntilCurrentMonthText, value); }
        }

        public double TaxYear
        {
            get { return _taxYear; }
            set { SetPropertyValue("TaxYear", ref _taxYear, value); }
        }

        public double TaxMonth
        {
            get { return _totalMonth; }
            set { SetPropertyValue("TaxMonth", ref _totalMonth, value); }
        }

        public double Kolom1   
        {
            get { return _kolom1; }
            set { SetPropertyValue("Kolom1", ref _kolom1, value); }
           
        }

        public string Kolom1Text
        {
            get { return _kolom1Text; }
            set { SetPropertyValue("Kolom1Text", ref _kolom1Text, value); }
        }

        public double Kolom2
        {
            get { return _kolom2; }
            set { SetPropertyValue("Kolom2", ref _kolom2, value); }
        }

        public string Kolom2text
        {
            get { return _kolom2Text; }
            set { SetPropertyValue("Kolom2Text", ref _kolom2Text, value); }
        }

        public double Kolom3
        {
            get { return _kolom3; }
            set { SetPropertyValue("Kolom3", ref _kolom3, value); }
        }

        public string Kolom3Text
        {
            get { return _kolom3Text; }
            set { SetPropertyValue("Kolom3Text", ref _kolom3Text, value); }
        }

        public double Kolom4
        {
            get { return _kolom4; }
            set { SetPropertyValue("Kolom4", ref _kolom4, value); }
        }

        public string Kolom4Text
        {
            get { return _kolom4Text; }
            set { SetPropertyValue("Kolom4Text", ref _kolom4Text, value); }
        }

        public double Kolom5
        {
            get { return _kolom5; }
            set { SetPropertyValue("Kolom5", ref _kolom5, value); }
        }

        public string Kolom5Text
        {
            get { return _kolom5Text; }
            set { SetPropertyValue("Kolom5Text", ref _kolom5Text, value); }
        }

        public double Kolom6
        {
            get { return _kolom6; }
            set { SetPropertyValue("Kolom6", ref _kolom6, value); }
        }

        public string Kolom6Text
        {
            get { return _kolom6Text; }
            set { SetPropertyValue("Kolom6Text", ref _kolom6Text, value); }
        }

        public double Kolom7
        {
            get { return _kolom7; }
            set { SetPropertyValue("Kolom7", ref _kolom7, value); }
        }

        public string Kolom7Text
        {
            get { return _kolom7Text; }
            set { SetPropertyValue("Kolom7Text", ref _kolom7Text, value); }
        }

        public double Kolom8
        {
            get { return _kolom8; }
            set { SetPropertyValue("Kolom8", ref _kolom8, value); }
        }

        public string Kolom8Text
        {
            get { return _kolom8Text; }
            set { SetPropertyValue("Kolom8Text", ref _kolom8Text, value); }
        }

        public double Kolom9
        {
            get { return _kolom9; }
            set { SetPropertyValue("Kolom9", ref _kolom9, value); }
        }

        public string Kolom9Text
        {
            get { return _kolom9Text; }
            set { SetPropertyValue("Kolom9Text", ref _kolom9Text, value); }
        }

        public double Kolom10
        {
            get { return _kolom10; }
            set { SetPropertyValue("Kolom10", ref _kolom10, value); }
        }

        public string Kolom10Text
        {
            get { return _kolom10Text; }
            set { SetPropertyValue("Kolom10Text", ref _kolom10Text, value); }
        }

        public double Kolom11
        {
            get { return _kolom11; }
            set { SetPropertyValue("Kolom11", ref _kolom11, value); }
        }

        public string Kolom11Text
        {
            get { return _kolom11Text; }
            set { SetPropertyValue("Kolom11Text", ref _kolom11Text, value); }
        }

        public double Kolom12
        {
            get { return _kolom12; }
            set { SetPropertyValue("Kolom12", ref _kolom12, value); }
        }

        public string Kolom12Text
        {
            get { return _kolom12Text; }
            set { SetPropertyValue("Kolom12Text", ref _kolom12Text, value); }
        }

        public double Kolom13
        {
            get { return _kolom13; }
            set { SetPropertyValue("Kolom13", ref _kolom13, value); }
        }

        public string Kolom13Text
        {
            get { return _kolom13Text; }
            set { SetPropertyValue("Kolom13Text", ref _kolom13Text, value); }
        }

        public double Kolom14
        {
            get { return _kolom14; }
            set { SetPropertyValue("Kolom14", ref _kolom14, value); }
        }

        public string Kolom14Text
        {
            get { return _kolom14Text; }
            set { SetPropertyValue("Kolom14Text", ref _kolom14Text, value); }
        }

        public double Kolom15
        {
            get { return _kolom15; }
            set { SetPropertyValue("Kolom15", ref _kolom15, value); }
        }

        public string Kolom15Text
        {
            get { return _kolom15Text; }
            set { SetPropertyValue("Kolom15Text", ref _kolom15Text, value); }
        }

        public double Kolom16
        {
            get { return _kolom16; }
            set { SetPropertyValue("Kolom16", ref _kolom16, value); }
        }

        public string Kolom16Text
        {
            get { return _kolom16Text; }
            set { SetPropertyValue("Kolom16Text", ref _kolom16Text, value); }
        }

        public double Kolom17
        {
            get { return _kolom17; }
            set { SetPropertyValue("Kolom17", ref _kolom17, value); }
        }

        public string Kolom17Text
        {
            get { return _kolom17Text; }
            set { SetPropertyValue("Kolom17Text", ref _kolom17Text, value); }
        }

        public double Kolom18
        {
            get { return _kolom18; }
            set { SetPropertyValue("Kolom18", ref _kolom18, value); }
        }

        public string Kolom18Text
        {
            get { return _kolom18Text; }
            set { SetPropertyValue("Kolom18Text", ref _kolom18Text, value); }
        }

        public double Kolom19
        {
            get { return _kolom19; }
            set { SetPropertyValue("Kolom19", ref _kolom19, value); }
        }

        public string Kolom19Text
        {
            get { return _kolom19Text; }
            set { SetPropertyValue("Kolom19Text", ref _kolom19Text, value); }
        }

        public double Kolom20
        {
            get { return _kolom20; }
            set { SetPropertyValue("Kolom20", ref _kolom20, value); }
        }

        public string Kolom20Text
        {
            get { return _kolom20Text; }
            set { SetPropertyValue("Kolom20Text", ref _kolom20Text, value); }
        }

        public double Kolom21
        {
            get { return _kolom21; }
            set { SetPropertyValue("Kolom21", ref _kolom21, value); }
        }

        public string Kolom21Text
        {
            get { return _kolom21Text; }
            set { SetPropertyValue("Kolom21Text", ref _kolom21Text, value); }
        }

        public double Kolom22
        {
            get { return _kolom22; }
            set { SetPropertyValue("Kolom22", ref _kolom22, value); }
        }

        public string Kolom22Text
        {
            get { return _kolom22Text; }
            set { SetPropertyValue("Kolom22Text", ref _kolom22Text, value); }
        }

        public double Kolom23
        {
            get { return _kolom23; }
            set { SetPropertyValue("Kolom23", ref _kolom23, value); }
        }

        public string Kolom23Text
        {
            get { return _kolom23Text; }
            set { SetPropertyValue("Kolom23Text", ref _kolom23Text, value); }
        }

        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        public string MaritalStatus
        {
            get
            {
                string val1;
                if (_employee.NonTaxableIncomeStatus != null)
                {
                    val1 = _employee.NonTaxableIncomeStatus.Name;
                    if (val1 != null)
                        return val1;
                }
                return " ";
            }
        }

        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        public double Children
        {
            get
            {
                double val2;
                if (_employee.NonTaxableIncomeStatus != null)
                {
                    val2 = _employee.NonTaxableIncomeStatus.Children;
                    return val2;
                }
                return 0;
            }
        }


        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        public double Spouse
        {
            get
            {
                double val3;
                if (_employee.NonTaxableIncomeStatus != null)
                {
                    val3 = _employee.NonTaxableIncomeStatus.Spouse;
                    return val3;
                }
                return 0;
            }
        }

        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        public string Gender
        {
            get
            {
                string val4;
                if (_employee.GenderType != null)
                {
                    val4 = _employee.GenderType.Name;
                    return val4;
                }
                return null;
            }
        }

        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        public string Name
        {
            get
            {
                string val5;
                if (_employee.Name != null)
                {
                    val5 = _employee.Name;
                    return val5;
                }
                return null;
            }
        }

        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        public bool ExpatriateStatus
        {
            get
            {
                bool val6;
                if (_employee.IsExpatriate != null)
                {
                    val6 = _employee.IsExpatriate;
                    return val6;
                }
                return false;
            }
        }

        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        public string CompanyName
        {
            get
            {
                string val7;
                if (_employee.Company.Name != null)
                {
                    val7 = _employee.Company.Name;
                    return val7;
                }
                return null;
            }
        }

        //[VisibleInDetailView(false)]
        //[VisibleInListView(false)]
        //public string CompanyNPWPNumber
        //{
        //    get
        //    {
        //        string val8;
        //        if (_employee.Company.npwpNumber != null)
        //        {
        //            val8 = _employee.Company.npwpNumber;
        //            return val8;
        //        }
        //        return val8;
        //    }
        //}

        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        public string TaxAddress
        {
            get
            {
                string val9;
                if (_employee.TaxStreet != null)
                {
                    val9 = _employee.TaxStreet;
                    return val9;
                }
                else if (_employee.Address != null)
                {
                    val9 = _employee.Address;
                    return val9;
                }
                return " ";
            }
        }

        //[VisibleInDetailView(false)]
        //[VisibleInListView(false)]
        //public string CompanyLeadName
        //{
        //    get
        //    {
        //        string val10;
        //        if (_employee.Company != null)
        //        {
        //            if (_employee.Company.LeadBy != null)
        //            {
        //                val10 = _employee.Company.LeadBy.FullName;
        //                if (val10 != null)
        //                    return val10;
        //            }
        //        }
        //        return " ";
        //    }
        //}

        //[VisibleInDetailView(false)]
        //[VisibleInListView(false)]
        //public string CompanyLeadNPWP
        //{
        //    get
        //    {
        //        string val11;
        //        if (_employee.Company != null)
        //        {
        //            if (_employee.Company.LeadBy != null)
        //            {
        //                val11 = _employee.Company.LeadBy.NPWPNumber;
        //                if (val11 != null)
        //                    return val10;
        //            }
        //        }
        //        return " ";
        //    }
        //}
        #endregion

        #region MappingKolom
        //private void MappingKolom(bool GetData)
        //{
        //    // get new data from PayRollProcess detail
        //    // allowance, deduction, salarybasic
        //    PayrollProcess PayProcRec = Session.FindObject<PayrollProcess>(new GroupOperator(GroupOperatorType.And, new BinaryOperator("Employee", this.Employee), new BinaryOperator("Period", this.Period)));
        //    // For Each oSalProc As PayrollProcess In PayProcRec
        //    // Next
        //    // For Each oSalProc As PayrollProcess In PayProcRec
        //    XPCollection<PayrollProcessDetail> PayProcDetRec = new XPCollection<PayrollProcessDetail>(Session, new BinaryOperator("PayrollProcess", PayProcRec));
        //    foreach (PayrollProcessDetail o in PayProcDetRec)
        //    {
        //        switch (o.Component.PPH21Mapping)
        //        {
        //            case PPH21Coloumn.Kolom1:
        //        {
        //                    if (GetData)
        //                        this.Kolom1 += Rounding(o.ComponentValue);
        //                    else
        //                        o.ComponentValue = this.Kolom1;
        //                    break;
        //                }

        //            case PPH21Coloumn.Kolom2:
        //        {
        //                    if (GetData)
        //                        this.Kolom2 += Rounding(o.ComponentValue);
        //                    else
        //                        o.ComponentValue = this.Kolom2;
        //                    break;
        //                }

        //            case PPH21Coloumn.Kolom3:
        //        {
        //                    if (GetData)
        //                        this.Kolom3 += Rounding(o.ComponentValue);
        //                    else
        //                        o.ComponentValue = this.Kolom3;
        //                    break;
        //                }

        //            case PPH21Coloumn.Kolom4:
        //        {
        //                    if (GetData)
        //                        this.Kolom4 += Rounding(o.ComponentValue);
        //                    else
        //                        o.ComponentValue = this.Kolom4;
        //                    break;
        //                }

        //            case PPH21Coloumn.Kolom5:
        //        {
        //                    if (GetData)
        //                        this.Kolom5 += Rounding(o.ComponentValue);
        //                    else
        //                        o.ComponentValue = this.Kolom5;
        //                    break;
        //                }

        //            case PPH21Coloumn.Kolom6:
        //        {
        //                    if (GetData)
        //                        this.Kolom6 += Rounding(o.ComponentValue);
        //                    else
        //                        o.ComponentValue = this.Kolom6;
        //                    break;
        //                }

        //            case PPH21Coloumn.Kolom7:
        //        {
        //                    if (GetData)
        //                        this.Kolom7 += Rounding(o.ComponentValue);
        //                    else
        //                        o.ComponentValue = this.Kolom7;
        //                    break;
        //                }

        //            case PPH21Coloumn.Kolom8:
        //        {
        //                    if (GetData)
        //                        this.Kolom8 += Rounding(o.ComponentValue);
        //                    else
        //                        o.ComponentValue = this.Kolom8;
        //                    break;
        //                }

        //            case PPH21Coloumn.Kolom9:
        //        {
        //                    if (GetData)
        //                        this.Kolom9 += Rounding(o.ComponentValue);
        //                    else
        //                        o.ComponentValue = this.Kolom9;
        //                    break;
        //                }

        //            case PPH21Coloumn.Kolom10:
        //        {
        //                    if (GetData)
        //                        this.Kolom10 += Rounding(o.ComponentValue);
        //                    else
        //                        o.ComponentValue = this.Kolom10;
        //                    break;
        //                }

        //            case PPH21Coloumn.Kolom11:
        //        {
        //                    if (GetData)
        //                        this.Kolom11 += Rounding(o.ComponentValue);
        //                    else
        //                        o.ComponentValue = this.Kolom11;
        //                    break;
        //                }

        //            case PPH21Coloumn.Kolom12:
        //        {
        //                    if (GetData)
        //                        this.Kolom12 += Rounding(o.ComponentValue);
        //                    else
        //                        o.ComponentValue = this.Kolom12;
        //                    break;
        //                }

        //            case PPH21Coloumn.Kolom13:
        //        {
        //                    if (GetData)
        //                        this.Kolom13 += Rounding(o.ComponentValue);
        //                    else
        //                        o.ComponentValue = this.Kolom13;
        //                    break;
        //                }

        //            case PPH21Coloumn.Kolom14:
        //        {
        //                    if (GetData)
        //                        this.Kolom14 += Rounding(o.ComponentValue);
        //                    else
        //                        o.ComponentValue = this.Kolom14;
        //                    break;
        //                }

        //            case PPH21Coloumn.Kolom15:
        //        {
        //                    if (GetData)
        //                        this.Kolom15 += Rounding(o.ComponentValue);
        //                    else
        //                        o.ComponentValue = this.Kolom15;
        //                    break;
        //                }

        //            case PPH21Coloumn.Kolom16:
        //        {
        //                    if (GetData)
        //                        this.Kolom16 += Rounding(o.ComponentValue);
        //                    else
        //                        o.ComponentValue = this.Kolom16;
        //                    break;
        //                }

        //            case PPH21Coloumn.Kolom17:
        //        {
        //                    if (GetData)
        //                        this.Kolom17 += Rounding(o.ComponentValue);
        //                    else
        //                        o.ComponentValue = this.Kolom17;
        //                    break;
        //                }

        //            case PPH21Coloumn.Kolom18:
        //        {
        //                    if (GetData)
        //                        this.Kolom18 += Rounding(o.ComponentValue);
        //                    else
        //                        o.ComponentValue = this.Kolom18;
        //                    break;
        //                }

        //            case PPH21Coloumn.Kolom19:
        //        {
        //                    if (GetData)
        //                        this.Kolom19 += Rounding(o.ComponentValue);
        //                    else
        //                        o.ComponentValue = this.Kolom19;
        //                    break;
        //                }

        //            case PPH21Coloumn.Kolom20:
        //        {
        //                    if (GetData)
        //                        this.Kolom20 += Rounding(o.ComponentValue);
        //                    else
        //                        o.ComponentValue = this.Kolom20;
        //                    break;
        //                }

        //            case PPH21Coloumn.Kolom21:
        //        {
        //                    if (GetData)
        //                        this.Kolom21 += Rounding(o.ComponentValue);
        //                    else
        //                        o.ComponentValue = this.Kolom21;
        //                    break;
        //                }

        //            case PPH21Coloumn.Kolom22:
        //        {
        //                    if (GetData)
        //                        this.Kolom22 += Rounding(o.ComponentValue);
        //                    else
        //                        o.ComponentValue = this.Kolom22;
        //                    break;
        //                }

        //            case PPH21Coloumn.Kolom23:
        //        {
        //                    if (GetData)
        //                        this.Kolom23 += Rounding(o.ComponentValue);
        //                    else
        //                        o.ComponentValue = this.Kolom23;
        //                    break;
        //                }
        //        }
        //        if (!GetData)
        //            o.Save();
        //    }
        //}
        #endregion MappingKolom

        #region CalculatesPPH21
        [Action(ToolTip = "Calculates PPh21 Values")]
        public void CalculatesPPh21()
        {
            if (Period == null)
            {
                //Interaction.MsgBox("Period cannot be empty");
                return;
            }
            if (Employee == null)
            {
                //Interaction.MsgBox("Employee cannot be empty");
                //return;
            }
            if (Employee.StartDate > Period.EndDate)
            {
                //Interaction.MsgBox("PPh21 Process cannot be process because of Employee.StartDate > Period.EndDate");
                //return;
            }
            if (Employee.TaxCalculationMethod == TaxCalculationMethod.None)
            {
                //Interaction.MsgBox("PPh21 Process cannot be process because of Employee.TaxCalculationMethode = None");
                //return;
            }
            if (Employee.NonTaxableIncomeStatus == null)
            {
                //Interaction.MsgBox("PPh21 Process cannot be process because of Employee.NonTaxAbleIncomeStatus.Name is blank ");
                //return;
            }
            this.NPWP = Employee.NPWP;
            this.PTKPStatus = Employee.NonTaxableIncomeStatus.Name;
            this.PTKPValue = Employee.NonTaxableIncomeStatus.NonTaxAbleIncomeValue;
            this.TotalMonth = 12;
            this.TaxMonth = Period.PeriodMonth;
            this.TaxYear = Period.PeriodYear;
            switch (Employee.TaxCalculationMethod)
            {
                case TaxCalculationMethod.Gross:
            {
                        this.TaxMethod = TaxCalculationMethod.Gross;
                        break;
                    }

                case TaxCalculationMethod.Net:
            {
                        this.TaxMethod = TaxCalculationMethod.Net;
                        break;
                    }

                case TaxCalculationMethod.Mixed:
            {
                        this.TaxMethod = TaxCalculationMethod.Mixed;
                        break;
                    }

                case TaxCalculationMethod.None:
            {
                        this.TaxMethod = TaxCalculationMethod.None;
                        break;
                    }
            }
            // check resign employee
            if (Employee.EndDate.Year != 1)
            {
                // check period
                if (Period.PeriodMonth > Employee.EndDate.Month)
                {
                    //Interaction.MsgBox("Employee has resigned, action's cancelled");
                    return;
                }
                if (Employee.StartDate.Year < Period.PeriodYear)
                    this.TotalMonth = Employee.EndDate.Month;
                else if (Employee.StartDate.Year == Period.PeriodYear)
                    this.TotalMonth = Employee.EndDate.Month - Employee.StartDate.Month + 1;
            }
            //ResetKolom();
            //// Me.Save()
            //MappingKolom(true);
            //// get tax setup
            //XPCollection<TaxSetup> TaxSetups = new XPCollection<TaxSetup>(Session, new BinaryOperator("Active", true));
            //foreach (var oTax in TaxSetups)
            //{
            //    OcupationFeeRate = oTax.OccupationFee / (double)100;
            //    OcupationFee = oTax.MaximumAmount / (double)12; // TotalMonth
            //}
            //switch (Employee.TaxCalculationMethode)
            //{
            //    case TaxCalculationMethod.Gross:
            //{
            //            TaxProcess(TaxSetups);
            //            // check for bonus to fill irregular tax
            //            if (Kolom8 != 0)
            //                IrregularTax = Rounding(Kolom19 - CalculateTheTax(Kolom18 - Kolom8, TaxSetups));
            //            break;
            //        }

            //    case object _ when BiaTaxCalculationMethode.Net:
            //{
            //            // check for bonus to fill irregular tax
            //            if (Kolom8 != 0)
            //            {
            //                // get tax without bonus
            //                decimal tmpBonus = Kolom8;
            //                Kolom8 = 0;
            //                ProcessLoop(TaxSetups);
            //                decimal tmpKolom19 = Kolom19;
            //                Kolom8 = tmpBonus;
            //                ProcessLoop(TaxSetups);
            //                IrregularTax = Rounding(Kolom19 - tmpKolom19);
            //            }
            //            else
            //                ProcessLoop(TaxSetups);
            //            break;
            //        }

            //    case object _ when BiaTaxCalculationMethode.Mixed:
            //{
            //            Interaction.MsgBox("Under Development !!!");
            //            break;
            //        }
            //}
            //this.CurrentMonthTax = (Kolom19 / (double)TotalMonth);
            //this.TotalTaxUntilLastMonth = (TotalTaxUntilLastMonth);
            //this.TotalTaxUntilCurrentMonth = (CurrentMonthTax + TotalTaxUntilLastMonth);
            //this.Save();
            //MappingKolom(false);
        }

        #endregion CalculatesPPH21

        #region ResetKolom
        private void ResetKolom()
        {
            // reset value
            this.IrregularTax = 0;
            this.CurrentMonthTax = 0;
            this.TotalTaxUntilCurrentMonth = 0;
            this.Kolom1 = 0;
            this.Kolom2 = 0;
            this.Kolom3 = 0;
            this.Kolom4 = 0;
            this.Kolom5 = 0;
            this.Kolom6 = 0;
            this.Kolom7 = 0;
            this.Kolom8 = 0;
            this.Kolom9 = 0;
            this.Kolom10 = 0;
            this.Kolom11 = 0;
            this.Kolom12 = 0;
            this.Kolom13 = 0;
            this.Kolom14 = 0;
            this.Kolom15 = 0;
            this.Kolom16 = 0;
            this.Kolom17 = 0;
            this.Kolom18 = 0;
            this.Kolom19 = 0;
            this.Kolom20 = 0;
            this.Kolom21 = 0;
            this.Kolom22 = 0;
            this.Kolom23 = 0;
            this.IrregularTax = 0;
        }
        #endregion ResetKolom

        //private string _PersistentProperty;
        //[XafDisplayName("My display name"), ToolTip("My hint message")]
        //[ModelDefault("EditMask", "(000)-00"), Index(0), VisibleInListView(false)]
        //[Persistent("DatabaseColumnName"), RuleRequiredField(DefaultContexts.Save)]
        //public string PersistentProperty {
        //    get { return _PersistentProperty; }
        //    set { SetPropertyValue("PersistentProperty", ref _PersistentProperty, value); }
        //}

        //[Action(Caption = "My UI Action", ConfirmationMessage = "Are you sure?", ImageName = "Attention", AutoCommit = true)]
        //public void ActionMethod() {
        //    // Trigger a custom business logic for the current record in the UI (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112619.aspx).
        //    this.PersistentProperty = "Paid";
        //}

        #region SetPTKP
        //public double SetPTKPValue()
        //{
        //    if (!IsLoading)
        //    {
        //        double ptkp = 0;
        //        double ptkp1 = 54000000;
        //        if (this.PTKPStatus != null)
        //        {
        //            //ptkp = 54000000;
        //            if (_ptkpStatus == CustomProcess.PTKPStatus.TK0)
        //            {
        //                ptkp = 0;
        //            }
        //            else if (_ptkpStatus == CustomProcess.PTKPStatus.TK1)
        //            {
        //                ptkp = 4500000;
        //            }

        //            else if (_ptkpStatus == CustomProcess.PTKPStatus.TK2)
        //            {
        //                ptkp = 9000000;
        //            }

        //            else if (_ptkpStatus == CustomProcess.PTKPStatus.TK3)
        //            {
        //                ptkp = 13500000;
        //            }

        //            if (_ptkpStatus == CustomProcess.PTKPStatus.K0)
        //            {
        //                ptkp = 4500000;
        //            }
        //            else if (_ptkpStatus == CustomProcess.PTKPStatus.K1)
        //            {
        //                ptkp = 9000000;
        //            }
        //            else if (_ptkpStatus == CustomProcess.PTKPStatus.K2)
        //            {
        //                ptkp = 13500000;
        //            }
        //            else if (_ptkpStatus == CustomProcess.PTKPStatus.K3)
        //            {
        //                ptkp = 18000000;
        //            }
        //            else if (_ptkpStatus == CustomProcess.PTKPStatus.KI0)
        //            {
        //                ptkp = 54000000;
        //            }
        //            else if (_ptkpStatus == CustomProcess.PTKPStatus.KI1)
        //            {
        //                ptkp = 58500000;
        //            }
        //            else if (_ptkpStatus == CustomProcess.PTKPStatus.KI2)
        //            {
        //                ptkp = 63000000;
        //            }
        //            else if (_ptkpStatus == CustomProcess.PTKPStatus.KI3)
        //            {
        //                ptkp = 67500000;
        //            }
        //            this._ptkpValue = ptkp + ptkp1;
        //        }
        //    }
        //    return _ptkpValue;
        //}
        #endregion SetPTKP


        //
    }
}