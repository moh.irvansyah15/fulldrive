﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Sales")]
    [RuleCombinationOfPropertiesIsUnique("ReturnSalesCollectionRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class ReturnSalesCollection : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).

        private bool _activationPosting;
        private string _code;
        private XPCollection<SalesOrder> _availableSalesOrder;
        private SalesOrder _salesOrder;
        private XPCollection<InventoryTransferOut> _availableInventoryTransferOut;
        private InventoryTransferOut _inventoryTransferOut;
        private SalesReturn _salesReturn;
        private Status _status;
        private DateTime _statusDate;
        private int _postedCount;
        private GlobalFunction _globFunc;

        public ReturnSalesCollection(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.ReturnSalesCollection);
                DateTime now = DateTime.Now;
                this.Status = Status.Open;
                this.StatusDate = now;
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("ReturnSalesCollectionCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Browsable(false)]
        public XPCollection<SalesOrder> AvailableSalesOrder
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;

                if (!IsLoading)
                {
                   
                    if (this.SalesReturn != null)
                    {
                        if (this.SalesReturn.SalesToCustomer != null)
                        {
                            #region InventoryTransferOut
                            if (this.InventoryTransferOut != null)
                            {
                                XPQuery<InventoryTransferOutMonitoring> _inventoryTransferOutMonitoringsQuery = new XPQuery<InventoryTransferOutMonitoring>(Session);

                                var _inventoryTransferOutMonitorings = from itom in _inventoryTransferOutMonitoringsQuery
                                                                       where (itom.Status != Status.Open && itom.PostedCount > 0
                                                                       && itom.InventoryTransferOut.BusinessPartner == this.SalesReturn.SalesToCustomer
                                                                       && itom.InventoryTransferOut == this.InventoryTransferOut)
                                                                       group itom by itom.SalesOrderMonitoring.SalesOrder into g
                                                                       select new { SalesOrder = g.Key };

                                if (_inventoryTransferOutMonitorings != null && _inventoryTransferOutMonitorings.Count() > 0)
                                {
                                    List<string> _stringITOM = new List<string>();

                                    foreach (var _inventoryTransferOutMonitoring in _inventoryTransferOutMonitorings)
                                    {
                                        if (_inventoryTransferOutMonitoring.SalesOrder != null)
                                        {
                                            if (_inventoryTransferOutMonitoring.SalesOrder.Code != null)
                                            {
                                                _stringITOM.Add(_inventoryTransferOutMonitoring.SalesOrder.Code);
                                            }
                                        }
                                    }

                                    IEnumerable<string> _stringArrayITODistinct = _stringITOM.Distinct();
                                    string[] _stringArrayITOMList = _stringArrayITODistinct.ToArray();
                                    if (_stringArrayITOMList.Length == 1)
                                    {
                                        for (int i = 0; i < _stringArrayITOMList.Length; i++)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _stringArrayITOMList[i] + "'";
                                            }
                                        }
                                    }
                                    else if (_stringArrayITOMList.Length > 1)
                                    {
                                        for (int i = 0; i < _stringArrayITOMList.Length; i++)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _stringArrayITOMList[i] + "'";
                                            }
                                            else
                                            {
                                                _endString = _endString + " OR [Code]=='" + _stringArrayITOMList[i] + "'";
                                            }
                                        }
                                    }
                                    _fullString = _beginString + _endString;

                                    if (_fullString != null)
                                    {
                                        _availableSalesOrder = new XPCollection<SalesOrder>(Session, CriteriaOperator.Parse(_fullString));
                                    }
                                }
                            }
                            #endregion InventoryTransferOut
                            #region UnInventoryTransferOut
                            else
                            {
                                XPQuery<InventoryTransferOutMonitoring> _inventoryTransferOutMonitoringsQuery = new XPQuery<InventoryTransferOutMonitoring>(Session);

                                var _inventoryTransferOutMonitorings = from itom in _inventoryTransferOutMonitoringsQuery
                                                                       where (itom.Status != Status.Open && itom.PostedCount > 0
                                                                       && itom.InventoryTransferOut.BusinessPartner == this.SalesReturn.SalesToCustomer)
                                                                       group itom by itom.SalesOrderMonitoring.SalesOrder into g
                                                                       select new { SalesOrder = g.Key };

                                if (_inventoryTransferOutMonitorings != null && _inventoryTransferOutMonitorings.Count() > 0)
                                {
                                    List<string> _stringITOM = new List<string>();

                                    foreach (var _inventoryTransferOutMonitoring in _inventoryTransferOutMonitorings)
                                    {
                                        if (_inventoryTransferOutMonitoring.SalesOrder != null)
                                        {
                                            if (_inventoryTransferOutMonitoring.SalesOrder.Code != null)
                                            {
                                                _stringITOM.Add(_inventoryTransferOutMonitoring.SalesOrder.Code);
                                            }
                                        }
                                    }

                                    IEnumerable<string> _stringArrayITODistinct = _stringITOM.Distinct();
                                    string[] _stringArrayITOMList = _stringArrayITODistinct.ToArray();
                                    if (_stringArrayITOMList.Length == 1)
                                    {
                                        for (int i = 0; i < _stringArrayITOMList.Length; i++)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _stringArrayITOMList[i] + "'";
                                            }
                                        }
                                    }
                                    else if (_stringArrayITOMList.Length > 1)
                                    {
                                        for (int i = 0; i < _stringArrayITOMList.Length; i++)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _stringArrayITOMList[i] + "'";
                                            }
                                            else
                                            {
                                                _endString = _endString + " OR [Code]=='" + _stringArrayITOMList[i] + "'";
                                            }
                                        }
                                    }
                                    _fullString = _beginString + _endString;

                                    if (_fullString != null)
                                    {
                                        _availableSalesOrder = new XPCollection<SalesOrder>(Session, CriteriaOperator.Parse(_fullString));
                                    }
                                }
                            }
                            #endregion UnInventoryTransferOut 
                        }
                    }
                }
                return _availableSalesOrder;
            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableSalesOrder", DataSourcePropertyIsNullMode.SelectNothing)]
        public SalesOrder SalesOrder
        {
            get { return _salesOrder; }
            set { SetPropertyValue("SalesOrder", ref _salesOrder, value); }
        }

        public XPCollection<InventoryTransferOut> AvailableInventoryTransferOut
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;

                if (!IsLoading)
                {
                    if (this.SalesReturn != null )
                    {
                        if (this.SalesReturn.SalesToCustomer != null)
                        {
                            #region SalesOrder
                            if(this.SalesOrder != null)
                            {
                                XPQuery<InventoryTransferOutMonitoring> _inventoryTransferOutMonitoringsQuery = new XPQuery<InventoryTransferOutMonitoring>(Session);

                                var _inventoryTransferOutMonitorings = from itom in _inventoryTransferOutMonitoringsQuery
                                                                       where (itom.Status != Status.Open && itom.PostedCount > 0
                                                                       && itom.InventoryTransferOut.BusinessPartner == this.SalesReturn.SalesToCustomer
                                                                       && itom.SalesOrderMonitoring.SalesOrder == this.SalesOrder)
                                                                       group itom by itom.InventoryTransferOut into g
                                                                       select new { InventoryTransferOut = g.Key };

                                if (_inventoryTransferOutMonitorings != null && _inventoryTransferOutMonitorings.Count() > 0)
                                {
                                    List<string> _stringITOM = new List<string>();

                                    foreach (var _inventoryTransferOutMonitoring in _inventoryTransferOutMonitorings)
                                    {
                                        if (_inventoryTransferOutMonitoring != null)
                                        {
                                            _stringITOM.Add(_inventoryTransferOutMonitoring.InventoryTransferOut.Code);
                                        }
                                    }

                                    IEnumerable<string> _stringArrayITODistinct = _stringITOM.Distinct();
                                    string[] _stringArrayITOMList = _stringArrayITODistinct.ToArray();
                                    if (_stringArrayITOMList.Length == 1)
                                    {
                                        for (int i = 0; i < _stringArrayITOMList.Length; i++)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _stringArrayITOMList[i] + "'";
                                            }
                                        }
                                    }
                                    else if (_stringArrayITOMList.Length > 1)
                                    {
                                        for (int i = 0; i < _stringArrayITOMList.Length; i++)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _stringArrayITOMList[i] + "'";
                                            }
                                            else
                                            {
                                                _endString = _endString + " OR [Code]=='" + _stringArrayITOMList[i] + "'";
                                            }
                                        }
                                    }
                                    _fullString = _beginString + _endString;

                                    if (_fullString != null)
                                    {
                                        _availableInventoryTransferOut = new XPCollection<InventoryTransferOut>(Session, CriteriaOperator.Parse(_fullString));
                                    }
                                }
                            }
                            #endregion SalesOrder
                            #region UnSalesOrder
                            else
                            {
                                XPQuery<InventoryTransferOutMonitoring> _inventoryTransferOutMonitoringsQuery = new XPQuery<InventoryTransferOutMonitoring>(Session);

                                var _inventoryTransferOutMonitorings = from itom in _inventoryTransferOutMonitoringsQuery
                                                                       where (itom.Status != Status.Open && itom.PostedCount > 0
                                                                       && itom.InventoryTransferOut.BusinessPartner == this.SalesReturn.SalesToCustomer)
                                                                       group itom by itom.InventoryTransferOut into g
                                                                       select new { InventoryTransferOut = g.Key };

                                if (_inventoryTransferOutMonitorings != null && _inventoryTransferOutMonitorings.Count() > 0)
                                {
                                    List<string> _stringITOM = new List<string>();

                                    foreach (var _inventoryTransferOutMonitoring in _inventoryTransferOutMonitorings)
                                    {
                                        if (_inventoryTransferOutMonitoring != null)
                                        {
                                            _stringITOM.Add(_inventoryTransferOutMonitoring.InventoryTransferOut.Code);
                                        }
                                    }

                                    IEnumerable<string> _stringArrayITODistinct = _stringITOM.Distinct();
                                    string[] _stringArrayITOMList = _stringArrayITODistinct.ToArray();
                                    if (_stringArrayITOMList.Length == 1)
                                    {
                                        for (int i = 0; i < _stringArrayITOMList.Length; i++)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _stringArrayITOMList[i] + "'";
                                            }
                                        }
                                    }
                                    else if (_stringArrayITOMList.Length > 1)
                                    {
                                        for (int i = 0; i < _stringArrayITOMList.Length; i++)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _stringArrayITOMList[i] + "'";
                                            }
                                            else
                                            {
                                                _endString = _endString + " OR [Code]=='" + _stringArrayITOMList[i] + "'";
                                            }
                                        }
                                    }
                                    _fullString = _beginString + _endString;

                                    if (_fullString != null)
                                    {
                                        _availableInventoryTransferOut = new XPCollection<InventoryTransferOut>(Session, CriteriaOperator.Parse(_fullString));
                                    }
                                }
                            }
                            #endregion UnSalesOrder    
                        }
                    }
                }
                return _availableInventoryTransferOut;
            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableInventoryTransferOut", DataSourcePropertyIsNullMode.SelectNothing)]
        public InventoryTransferOut InventoryTransferOut
        {
            get { return _inventoryTransferOut; }
            set { SetPropertyValue("InventoryTransferOut", ref _inventoryTransferOut, value); }
        }

        [ImmediatePostData()]
        [Appearance("ReturnSalesCollectionSalesInvoiceClose", Enabled = false)]
        [Association("SalesReturn-ReturnSalesCollections")]
        public SalesReturn SalesReturn
        {
            get { return _salesReturn; }
            set { SetPropertyValue("SalesReturn", ref _salesReturn, value); }
        }

        [Appearance("ReturnSalesCollectionStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("ReturnSalesCollectionStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("ReturnSalesCollectionPostedCountEnabled", Enabled = false)]
        public int PostedCount
        {
            get { return _postedCount; }
            set { SetPropertyValue("PostedCount", ref _postedCount, value); }
        }

        #endregion Field

    }
}