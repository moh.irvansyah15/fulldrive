﻿#region Default

using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

#endregion Default

using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.SystemModule;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Editors;

namespace FullDrive.Module.BusinessObjects
{
    #region Default

    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Inventory")]
    [RuleCombinationOfPropertiesIsUnique("InventoryTransferOutLineRuleUnique", DefaultContexts.Save, "Code")]
    [ListViewFilter("AllDataInventoryTransferOutLine", "", "All Data", "All Data In Inventory Transfer Out Line", 1, true)]
    [ListViewFilter("OpenInventoryTransferOutLine", "Status = 'Open'", "Open", "Open Data Status In Inventory Transfer Out Line", 2, true)]
    [ListViewFilter("ProgressInventoryTransferOutLine", "Status = 'Progress'", "Progress", "Progress Data Status In Inventory Transfer Out Line", 3, true)]
    [ListViewFilter("LockInventoryTransferOutLine", "Status = 'Close'", "Close", "Close Data Status In Inventory Transfer Out Line", 4, true)]

    #endregion Default

    // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
    public class InventoryTransferOutLine : FullDriveSysBaseObject
    {
        #region Default

        private bool _activationPosting;
        private int _no;
        private bool _select;
        private string _code;
        private string _name;
        private OrderType _salesType;
        private XPCollection<Item> _availableItem;
        private Item _item;
        private string _description;
        private Brand _brand;
        private XPCollection<UnitOfMeasure> _availableUnitOfMeasure;
        #region InitialMaxQty
        private double _mxDQTY;
        private UnitOfMeasure _mxDUom;
        private double _mxQty;
        private UnitOfMeasure _mxUom;
        private double _mxTQty;
        #endregion InitialMaxQty
        #region InitialDefaultQty
        private double _dQty;
        private UnitOfMeasure _dUom;
        private double _qty;
        private UnitOfMeasure _uom;
        private double _tQty;
        #endregion InitialDefaultQty
        #region InitialRemainQty
        private double _rmDQty;
        private double _rmQty;
        private double _rmTQty;
        #endregion InitialRemainQty
        #region InitialPostingQuantityTo
        private double _pDQty;
        private UnitOfMeasure _pDUom;
        private double _pQty;
        private UnitOfMeasure _pUom;
        private double _pTQty;
        #endregion InitialPostingQuantityTo
        //#region InitialLot
        //private double _count;
        //private double _qtyPackage;
        //private UnitOfMeasure _unitPackage;
        //private int _processLotCount;
        //#endregion InitialLot
        private XPCollection<Location> _availableLocation;
        private bool _closeLocation;
        private Location _location;
        private XPCollection<BinLocation> _availableBinLocation;
        private BinLocation _binLocation;
        private bool _stockControlling;
        private XPCollection<Location> _availableLocationTo;
        private Location _locationTo;
        private XPCollection<BinLocation> _availableBinLocationTo;
        private BinLocation _binLocationTo;
        private StockType _stockTypeTo;
        private DateTime _estimatedDate;
        private DateTime _actualDate;
        private Status _status;
        private DateTime _statusDate;
        private StockType _stockType;
        private ReturnType _returnType;
        private int _processCount;
        private string _signCode;
        private InventoryTransferOut _inventoryTransferOut;
        private Company _company;
        private SalesOrderMonitoring _salesOrderMonitoring;
        private PurchaseReturnMonitoring _purchaseReturnMonitoring;
        private string _userAccess;
        private GlobalFunction _globFunc;

        public InventoryTransferOutLine(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.InventoryTransferOutLine);
                this.Status = Status.Open;
                this.StatusDate = now;
                this.StockType = StockType.Good;
                this.Select = true;
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        //No
        protected override void OnSaving()
        {
            base.OnSaving();
            if (!IsLoading && IsSaving)
            {
                UpdateNo();
            }
        }
        protected override void OnDeleting()
        {
            base.OnDeleting();
            if (!IsLoading)
            {
                RecoveryDeleteNo();
            }
        }

        #endregion Default

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [Appearance("InventoryTransferOutLineNoEnabled", Enabled = false)]
        public int No
        {
            get { return _no; }
            set { SetPropertyValue("No", ref _no, value); }
        }

        [Appearance("InventoryTransferOutLineSelectClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public bool Select
        {
            get { return _select; }
            set { SetPropertyValue("Select", ref _select, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("InventoryTransferOutLineCodeEnabled", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("InventoryTransferOutLineNameClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        [ImmediatePostData()]
        [Appearance("InventoryTransferOutLineSalesTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public OrderType SalesType
        {
            get { return _salesType; }
            set { SetPropertyValue("SalesType", ref _salesType, value); }
        }

        [Browsable(false)]
        public XPCollection<Item> AvailableItem
        {
            get
            {
                if (SalesType == OrderType.Item)
                {
                    _availableItem = new XPCollection<Item>(Session,
                                    new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("OrderType", OrderType.Item)));

                }
                else if (SalesType == OrderType.Service)
                {
                    _availableItem = new XPCollection<Item>(Session,
                                    new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("OrderType", OrderType.Service)));
                }
                else
                {
                    _availableItem = new XPCollection<Item>(Session);
                }

                return _availableItem;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableItem", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("InventoryTransferOutLineItemClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Item Item
        {
            get { return _item; }
            set
            {
                SetPropertyValue("Item", ref _item, value);
                if (!IsLoading)
                {
                    if (this._item != null)
                    {
                        this.DUOM = this._item.BasedUOM;
                        this.Brand = this._item.Brand;
                    }
                }
            }
        }

        [Appearance("InventoryTransferOutLineDescriptionClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Size(512)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        [Appearance("InventoryTransferOutLineBrandClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Brand Brand
        {
            get { return _brand; }
            set { SetPropertyValue("Brand", ref _brand, value); }
        }

        [Browsable(false)]
        public XPCollection<UnitOfMeasure> AvailableUnitOfMeasure
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;
                if (Item == null)
                {
                    _availableUnitOfMeasure = new XPCollection<UnitOfMeasure>(Session);
                }
                else
                {
                    List<string> _stringUOM = new List<string>();

                    XPCollection<ItemUnitOfMeasure> _locItemUnitOfMeasures = new XPCollection<ItemUnitOfMeasure>
                                                                              (Session, new GroupOperator(GroupOperatorType.And,
                                                                               new BinaryOperator("Item", this.Item),
                                                                               new BinaryOperator("Active", true)));

                    if (_locItemUnitOfMeasures != null && _locItemUnitOfMeasures.Count() > 0)
                    {
                        foreach (ItemUnitOfMeasure _locItemUnitOfMeasure in _locItemUnitOfMeasures)
                        {
                            _stringUOM.Add(_locItemUnitOfMeasure.UOM.Code);
                        }
                    }

                    IEnumerable<string> _stringArrayUOMDistinct = _stringUOM.Distinct();
                    string[] _stringArrayUOMList = _stringArrayUOMDistinct.ToArray();
                    if (_stringArrayUOMList.Length == 1)
                    {
                        for (int i = 0; i < _stringArrayUOMList.Length; i++)
                        {
                            UnitOfMeasure _locUOM = Session.FindObject<UnitOfMeasure>(new BinaryOperator("Code", _stringArrayUOMList[i]));
                            if (_locUOM != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locUOM.Code + "'";
                                }
                            }
                        }
                    }
                    else if (_stringArrayUOMList.Length > 1)
                    {
                        for (int i = 0; i < _stringArrayUOMList.Length; i++)
                        {
                            UnitOfMeasure _locUOM = Session.FindObject<UnitOfMeasure>(new BinaryOperator("Code", _stringArrayUOMList[i]));
                            if (_locUOM != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locUOM.Code + "'";
                                }
                                else
                                {
                                    _endString = " OR [Code]=='" + _locUOM.Code + "'";
                                }
                            }
                        }
                    }
                    _fullString = _beginString + _endString;

                    if (_fullString != null)
                    {
                        _availableUnitOfMeasure = new XPCollection<UnitOfMeasure>(Session, CriteriaOperator.Parse(_fullString));
                    }

                }
                return _availableUnitOfMeasure;

            }
        }

        #region MaxDefaultQty

        [Appearance("InventoryTransferOutLineMxDQtyClose", Enabled = false)]
        [ImmediatePostData()]
        public double MxDQty
        {
            get { return _mxDQTY; }
            set
            {
                SetPropertyValue("MxDQty", ref _mxDQTY, value);
                if (!IsLoading)
                {
                    SetMaxTotalQty();
                }
            }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        [Appearance("InventoryTransferOutLineMxDUOMClose", Enabled = false)]
        public UnitOfMeasure MxDUOM
        {
            get { return _mxDUom; }
            set
            {
                SetPropertyValue("MxDUOM", ref _mxDUom, value);
                if (!IsLoading)
                {
                    SetMaxTotalQty();
                }
            }
        }

        [ImmediatePostData()]
        [Appearance("InventoryTransferOutLineMxQtyClose", Enabled = false)]
        public double MxQty
        {
            get { return _mxQty; }
            set
            {
                SetPropertyValue("MxQty", ref _mxQty, value);
                if (!IsLoading)
                {
                    SetMaxTotalQty();
                }
            }
        }

        [DataSourceProperty("AvailableUnitOfMeasure", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        [Appearance("InventoryTransferOutLineMxUOMClose", Enabled = false)]
        public UnitOfMeasure MxUOM
        {
            get { return _mxUom; }
            set
            {
                SetPropertyValue("MxUOM", ref _mxUom, value);
                if (!IsLoading)
                {
                    SetMaxTotalQty();
                }
            }
        }

        [Appearance("InventoryTransferOutLineMxTQtyClose", Enabled = false)]
        public double MxTQty
        {
            get { return _mxTQty; }
            set { SetPropertyValue("MxTQty", ref _mxTQty, value); }
        }

        #endregion MaxDefaultQty

        #region DefaultQty

        [Appearance("InventoryTransferOutLineDQtyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double DQty
        {
            get { return _dQty; }
            set
            {
                SetPropertyValue("DQty", ref _dQty, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                    if(this.MxDQty > 0)
                    {
                        if (this.ProcessCount > 0)
                        {
                            if (this.RmDQty == 0)
                            {
                                if (this._dQty > this.MxDQty || this._dQty <= this.MxDQty)
                                {
                                    this._dQty = 0;
                                    SetTotalQty();
                                }
                            }

                            if (this.RmDQty > 0)
                            {
                                if (this._dQty > this.RmDQty)
                                {
                                    this._dQty = this.RmDQty;
                                    SetTotalQty();
                                }
                            }
                        }

                        if (this.ProcessCount == 0)
                        {
                            if (this.DQty > this.MxDQty)
                            {
                                this.DQty = this.MxDQty;
                                SetTotalQty();
                            }
                        }
                    }  
                }
            }
        }

        [Appearance("InventoryTransferOutLineDUOMClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure DUOM
        {
            get { return _dUom; }
            set
            {
                SetPropertyValue("DUOM", ref _dUom, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                }
            }
        }

        [Appearance("InventoryTransferOutLineQtyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double Qty
        {
            get { return _qty; }
            set
            {
                SetPropertyValue("Qty", ref _qty, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                    if(this.MxQty > 0)
                    {
                        if (this.ProcessCount > 0)
                        {
                            if (this.RmQty == 0)
                            {
                                if (this._qty > this.MxQty || this._qty <= this.MxQty)
                                {
                                    this._qty = 0;
                                    SetTotalQty();
                                }
                            }

                            if (this.RmQty > 0)
                            {
                                if (this._qty > this.RmQty)
                                {
                                    this._qty = this.RmQty;
                                    SetTotalQty();
                                }
                            }
                        }

                        if (this.ProcessCount == 0)
                        {
                            if (this._qty > this.MxQty)
                            {
                                this._qty = this.MxQty;
                                SetTotalQty();
                            }
                        }
                    }
                    
                }
            }
        }

        [Appearance("InventoryTransferOutLineUOMClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceProperty("AvailableUnitOfMeasure", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure UOM
        {
            get { return _uom; }
            set
            {
                SetPropertyValue("UOM", ref _uom, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                }
            }
        }

        [Appearance("InventoryTransferOutLineTotalQtyEnabled", Enabled = false)]
        public double TQty
        {
            get { return _tQty; }
            set { SetPropertyValue("TQty", ref _tQty, value); }
        }

        #endregion DefaultQty

        #region RemainQty

        [ImmediatePostData()]
        [Appearance("InventoryTransferOutLineRmDQtyClose", Enabled = false)]
        public double RmDQty
        {
            get { return _rmDQty; }
            set
            {
                SetPropertyValue("RmDQty", ref _rmDQty, value);
                if (!IsLoading)
                {
                    SetRemainTotalQty();
                }
            }
        }

        [ImmediatePostData()]
        [Appearance("InventoryTransferOutLineRmQtyClose", Enabled = false)]
        public double RmQty
        {
            get { return _rmQty; }
            set
            {
                SetPropertyValue("RmQty", ref _rmQty, value);
                if (!IsLoading)
                {
                    SetRemainTotalQty();
                }
            }
        }

        [Appearance("InventoryTransferOutLineRmTQtyClose", Enabled = false)]
        public double RmTQty
        {
            get { return _rmTQty; }
            set { SetPropertyValue("RmTQty", ref _rmTQty, value); }
        }

        #endregion RemainQty

        #region PostingQty

        [Appearance("InventoryTransferOutLinePDQtyClose", Enabled = false)]
        public double PDQty
        {
            get { return _pDQty; }
            set
            {
                SetPropertyValue("PDQty", ref _pDQty, value);
                if (!IsLoading)
                {
                    SetTotalPQty();
                }
            }
        }

        [Appearance("InventoryTransferOutLinePDUOMClose", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure PDUOM
        {
            get { return _pDUom; }
            set
            {
                SetPropertyValue("PDUOM", ref _pDUom, value);
                if (!IsLoading)
                {
                    SetTotalPQty();
                }
            }
        }

        [Appearance("InventoryTransferOutLinePQtyToClose", Enabled = false)]
        public double PQty
        {
            get { return _pQty; }
            set
            {
                SetPropertyValue("PQty", ref _pQty, value);
                if (!IsLoading)
                {
                    SetTotalPQty();
                }
            }
        }

        [Appearance("InventoryTransferOutLinePUOMClose", Enabled = false)]
        [DataSourceProperty("AvailableUnitOfMeasure", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure PUOM
        {
            get { return _pUom; }
            set
            {
                SetPropertyValue("PUOM", ref _pUom, value);
                if (!IsLoading)
                {
                    SetTotalPQty();
                }
            }
        }

        [Appearance("InventoryTransferOutLinePTQtyEnabled", Enabled = false)]
        public double PTQty
        {
            get { return _pTQty; }
            set { SetPropertyValue("PTQty", ref _pTQty, value); }
        }

        #endregion PostingQty

        #region Location

        [Browsable(false)]
        public XPCollection<Location> AvailableLocation
        {
            get
            {
                _userAccess = SecuritySystem.CurrentUserName;
                string _beginString = null;
                string _endString = null;
                string _fullString = null;
                string _locLocationCode = null;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                if (_locUserAccess != null)
                {
                    if (this.InventoryTransferOut != null)
                    {
                        if ((this.InventoryTransferOut.TransferType == DirectionType.External || this.InventoryTransferOut.TransferType == DirectionType.Internal)
                            && this.InventoryTransferOut.InventoryMovingType == InventoryMovingType.Deliver)
                        {
                            List<string> _stringLocation = new List<string>();

                            XPCollection<WarehouseSetupDetail> _locWhsSetupDetails = new XPCollection<WarehouseSetupDetail>
                                                                                      (Session, new GroupOperator(GroupOperatorType.And,
                                                                                       new BinaryOperator("UserAccess", _locUserAccess),
                                                                                       new BinaryOperator("ObjectList", ObjectList.InventoryTransferOut),
                                                                                       new BinaryOperator("Owner", true),
                                                                                       new BinaryOperator("TransferType", this.InventoryTransferOut.TransferType),
                                                                                       new BinaryOperator("InventoryMovingType", this.InventoryTransferOut.InventoryMovingType),
                                                                                       new BinaryOperator("LocationType", LocationType.Transit),
                                                                                       new BinaryOperator("Active", true)));

                            if (_locWhsSetupDetails != null && _locWhsSetupDetails.Count() > 0)
                            {
                                foreach (WarehouseSetupDetail _locWhsSetupDetail in _locWhsSetupDetails)
                                {
                                    if(_locWhsSetupDetail.Location != null)
                                    {
                                        if(_locWhsSetupDetail.Location.Code != null)
                                        {
                                            _locLocationCode = _locWhsSetupDetail.Location.Code;
                                            _stringLocation.Add(_locLocationCode);
                                        }
                                    }  
                                }
                            }

                            IEnumerable<string> _stringArrayLocationFromDistinct = _stringLocation.Distinct();
                            string[] _stringArrayLocationFromList = _stringArrayLocationFromDistinct.ToArray();
                            if (_stringArrayLocationFromList.Length == 1)
                            {
                                for (int i = 0; i < _stringArrayLocationFromList.Length; i++)
                                {
                                    Location _locLocationFrom = Session.FindObject<Location>(new BinaryOperator("Code", _stringArrayLocationFromList[i]));
                                    if (_locLocationFrom != null)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _locLocationFrom.Code + "'";
                                        }
                                    }
                                }
                            }
                            else if (_stringArrayLocationFromList.Length > 1)
                            {
                                for (int i = 0; i < _stringArrayLocationFromList.Length; i++)
                                {
                                    Location _locLocationFrom = Session.FindObject<Location>(new BinaryOperator("Code", _stringArrayLocationFromList[i]));
                                    if (_locLocationFrom != null)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _locLocationFrom.Code + "'";
                                        }
                                        else
                                        {
                                            _endString = " OR [Code]=='" + _locLocationFrom.Code + "'";
                                        }
                                    }
                                }
                            }
                            _fullString = _beginString + _endString;

                            if (_fullString != null)
                            {
                                _availableLocation = new XPCollection<Location>(Session, CriteriaOperator.Parse(_fullString));
                            }
                        }
                    }
                }
                else
                {
                    _availableLocation = new XPCollection<Location>(Session);
                }

                return _availableLocation;

            }
        }

        [Browsable(false)]
        public bool CloseLocation
        {
            get { return _closeLocation; }
            set { SetPropertyValue("CloseLocation", ref _closeLocation, value); }
        }

        [Appearance("InventoryTransferOutLineLocationClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Appearance("InventoryTransferOutLineLocationClose1", Criteria = "CloseLocation = true", Enabled = false)]
        [DataSourceProperty("AvailableLocation", DataSourcePropertyIsNullMode.SelectAll)]
        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public Location Location
        {
            get { return _location; }
            set { SetPropertyValue("Location", ref _location, value); }
        }

        [Browsable(false)]
        public XPCollection<BinLocation> AvailableBinLocation
        {
            get
            {
                _userAccess = SecuritySystem.CurrentUserName;
                string _beginString = null;
                string _endString = null;
                string _fullString = null;
                string _locBinLocationCode = null;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                if (_locUserAccess != null)
                {
                    if (this.Location != null)
                    {
                        if (this.InventoryTransferOut != null)
                        {

                            if ((this.InventoryTransferOut.TransferType == DirectionType.External || this.InventoryTransferOut.TransferType == DirectionType.Internal) 
                                && this.InventoryTransferOut.InventoryMovingType == InventoryMovingType.Deliver)
                            {
                                List<string> _stringBinLocation = new List<string>();

                                XPCollection<WarehouseSetupDetail> _locWhsSetupDetails = new XPCollection<WarehouseSetupDetail>
                                                                                          (Session, new GroupOperator(GroupOperatorType.And,
                                                                                           new BinaryOperator("UserAccess", _locUserAccess),
                                                                                           new BinaryOperator("Location", this.Location),
                                                                                           new BinaryOperator("Owner", true),
                                                                                           new BinaryOperator("Active", true)));

                                if (_locWhsSetupDetails != null && _locWhsSetupDetails.Count() > 0)
                                {
                                    foreach (WarehouseSetupDetail _locWhsSetupDetail in _locWhsSetupDetails)
                                    {
                                        if(_locWhsSetupDetail.BinLocation != null)
                                        {
                                            if(_locWhsSetupDetail.BinLocation.Code != null)
                                            {
                                                _locBinLocationCode = _locWhsSetupDetail.BinLocation.Code;
                                                _stringBinLocation.Add(_locBinLocationCode);
                                            }
                                        } 
                                    }
                                }

                                IEnumerable<string> _stringArrayBinLocationFromDistinct = _stringBinLocation.Distinct();
                                string[] _stringArrayBinLocationFromList = _stringArrayBinLocationFromDistinct.ToArray();
                                if (_stringArrayBinLocationFromList.Length == 1)
                                {
                                    for (int i = 0; i < _stringArrayBinLocationFromList.Length; i++)
                                    {
                                        BinLocation _locBinLocationFrom = Session.FindObject<BinLocation>(new BinaryOperator("Code", _stringArrayBinLocationFromList[i]));
                                        if (_locBinLocationFrom != null)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _locBinLocationFrom.Code + "'";
                                            }
                                        }
                                    }
                                }
                                else if (_stringArrayBinLocationFromList.Length > 1)
                                {
                                    for (int i = 0; i < _stringArrayBinLocationFromList.Length; i++)
                                    {
                                        BinLocation _locBinLocationFrom = Session.FindObject<BinLocation>(new BinaryOperator("Code", _stringArrayBinLocationFromList[i]));
                                        if (_locBinLocationFrom != null)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _locBinLocationFrom.Code + "'";
                                            }
                                            else
                                            {
                                                _endString = " OR [Code]=='" + _locBinLocationFrom.Code + "'";
                                            }
                                        }
                                    }
                                }
                                _fullString = _beginString + _endString;

                                if (_fullString != null)
                                {
                                    _availableBinLocation = new XPCollection<BinLocation>(Session, CriteriaOperator.Parse(_fullString));
                                }
                            }
                        }
                    }
                }
                else
                {
                    _availableBinLocation = new XPCollection<BinLocation>(Session);
                }

                return _availableBinLocation;

            }
        }

        [Appearance("InventoryTransferOutLineBinLocationClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceProperty("AvailableBinLocation", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        public BinLocation BinLocation
        {
            get { return _binLocation; }
            set { SetPropertyValue("BinLocation", ref _binLocation, value); }
        }

        [Appearance("InventoryTransferOutLineStockTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public StockType StockType
        {
            get { return _stockType; }
            set { SetPropertyValue("StockType", ref _stockType, value); }
        }

        #endregion Location

        #region LocationTo

        [ImmediatePostData()]
        [Appearance("InventoryTransferOutLineStockControllingClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public bool StockControlling
        {
            get { return _stockControlling; }
            set { SetPropertyValue("StockControlling", ref _stockControlling, value); }
        }

        [Browsable(false)]
        public XPCollection<Location> AvailableLocationTo
        {
            get
            {
                _userAccess = SecuritySystem.CurrentUserName;
                string _beginString = null;
                string _endString = null;
                string _fullString = null;
                string _locLocationCode = null;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                List<string> _stringLocation = new List<string>();
                if (_locUserAccess != null)
                {
                    if (this.InventoryTransferOut != null)
                    {
                        #region InventoryTransferOut
                        if (this.InventoryTransferOut.InventoryMovingType == InventoryMovingType.Deliver)
                        {
                            XPCollection<WarehouseSetupDetail> _locWhsSetupDetails = new XPCollection<WarehouseSetupDetail>
                                                                                  (Session, new GroupOperator(GroupOperatorType.And,
                                                                                   new BinaryOperator("UserAccess", _locUserAccess),
                                                                                   new BinaryOperator("Active", true)));

                            if (_locWhsSetupDetails != null && _locWhsSetupDetails.Count() > 0)
                            {
                                foreach (WarehouseSetupDetail _locWhsSetupDetail in _locWhsSetupDetails)
                                {
                                    if (_locWhsSetupDetail.Location != null)
                                    {
                                        if (_locWhsSetupDetail.Location.Code != null)
                                        {
                                            _locLocationCode = _locWhsSetupDetail.Location.Code;
                                            _stringLocation.Add(_locLocationCode);
                                        }
                                    }
                                }
                            }

                            IEnumerable<string> _stringArrayLocationToDistinct = _stringLocation.Distinct();
                            string[] _stringArrayLocationToList = _stringArrayLocationToDistinct.ToArray();
                            if (_stringArrayLocationToList.Length == 1)
                            {
                                for (int i = 0; i < _stringArrayLocationToList.Length; i++)
                                {
                                    Location _locLocationTo = Session.FindObject<Location>(new BinaryOperator("Code", _stringArrayLocationToList[i]));
                                    if (_locLocationTo != null)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _locLocationTo.Code + "'";
                                        }
                                    }
                                }
                            }
                            else if (_stringArrayLocationToList.Length > 1)
                            {
                                for (int i = 0; i < _stringArrayLocationToList.Length; i++)
                                {
                                    Location _locLocationTo = Session.FindObject<Location>(new BinaryOperator("Code", _stringArrayLocationToList[i]));
                                    if (_locLocationTo != null)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _locLocationTo.Code + "'";
                                        }
                                        else
                                        {
                                            _endString = " OR [Code]=='" + _locLocationTo.Code + "'";
                                        }
                                    }
                                }
                            }
                            _fullString = _beginString + _endString;

                            if (_fullString != null)
                            {
                                _availableLocationTo = new XPCollection<Location>(Session, CriteriaOperator.Parse(_fullString));
                            }
                        }
                        #endregion InventoryTransferOut
                    }
                }
                else
                {
                    _availableLocationTo = new XPCollection<Location>(Session);
                }

                return _availableLocationTo;

            }
        }

        [Appearance("InventoryTransferOutLineLocationToEnabled", Criteria = "StockControlling = false", Enabled = false)]
        [Appearance("InventoryTransferOutLineLocationToClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceProperty("AvailableLocationTo", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        [ImmediatePostData]
        public Location LocationTo
        {
            get { return _locationTo; }
            set { SetPropertyValue("LocationTo", ref _locationTo, value); }
        }

        [Browsable(false)]
        public XPCollection<BinLocation> AvailableBinLocationTo
        {
            get
            {
                _userAccess = SecuritySystem.CurrentUserName;
                string _beginString = null;
                string _endString = null;
                string _fullString = null;
                string _locBinLocationCode = null;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                List<string> _stringBinLocation = new List<string>();
                if (_locUserAccess != null)
                {
                    if (this.LocationTo != null)
                    {
                        if (this.InventoryTransferOut != null)
                        {
                            #region InventoryTransferOut
                            if (this.InventoryTransferOut.InventoryMovingType == InventoryMovingType.TransferOut)
                            {
                                XPCollection<WarehouseSetupDetail> _locWhsSetupDetails = new XPCollection<WarehouseSetupDetail>
                                                                                  (Session, new GroupOperator(GroupOperatorType.And,
                                                                                   new BinaryOperator("UserAccess", _locUserAccess),
                                                                                   new BinaryOperator("Location", this.LocationTo),
                                                                                   new BinaryOperator("Active", true)));

                                if (_locWhsSetupDetails != null && _locWhsSetupDetails.Count() > 0)
                                {
                                    foreach (WarehouseSetupDetail _locWhsSetupDetail in _locWhsSetupDetails)
                                    {
                                        if (_locWhsSetupDetail.BinLocation != null)
                                        {
                                            if (_locWhsSetupDetail.BinLocation.Code != null)
                                            {
                                                _locBinLocationCode = _locWhsSetupDetail.BinLocation.Code;
                                                _stringBinLocation.Add(_locBinLocationCode);
                                            }
                                        }
                                    }
                                }

                                IEnumerable<string> _stringArrayBinLocationToDistinct = _stringBinLocation.Distinct();
                                string[] _stringArrayBinLocationToList = _stringArrayBinLocationToDistinct.ToArray();
                                if (_stringArrayBinLocationToList.Length == 1)
                                {
                                    for (int i = 0; i < _stringArrayBinLocationToList.Length; i++)
                                    {
                                        BinLocation _locBinLocationTo = Session.FindObject<BinLocation>(new BinaryOperator("Code", _stringArrayBinLocationToList[i]));
                                        if (_locBinLocationTo != null)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _locBinLocationTo.Code + "'";
                                            }
                                        }
                                    }
                                }
                                else if (_stringArrayBinLocationToList.Length > 1)
                                {
                                    for (int i = 0; i < _stringArrayBinLocationToList.Length; i++)
                                    {
                                        BinLocation _locBinLocationTo = Session.FindObject<BinLocation>(new BinaryOperator("Code", _stringArrayBinLocationToList[i]));
                                        if (_locBinLocationTo != null)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _locBinLocationTo.Code + "'";
                                            }
                                            else
                                            {
                                                _endString = " OR [Code]=='" + _locBinLocationTo.Code + "'";
                                            }
                                        }
                                    }
                                }
                                _fullString = _beginString + _endString;

                                if (_fullString != null)
                                {
                                    _availableBinLocationTo = new XPCollection<BinLocation>(Session, CriteriaOperator.Parse(_fullString));
                                }
                            }
                            #endregion TransferOut
                        }

                    }
                }
                else
                {
                    _availableBinLocationTo = new XPCollection<BinLocation>(Session);
                }

                return _availableBinLocationTo;

            }
        }

        [Appearance("InventoryTransferOutLineBinLocationToEnabled", Criteria = "StockControlling = false", Enabled = false)]
        [Appearance("InventoryTransferOutLineBinLocationToClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceProperty("AvailableBinLocationTo", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        public BinLocation BinLocationTo
        {
            get { return _binLocationTo; }
            set { SetPropertyValue("BinLocationTo", ref _binLocationTo, value); }
        }

        [Appearance("InventoryTransferOutLineStockTypeToEnabled", Criteria = "StockControlling = false", Enabled = false)]
        [Appearance("InventoryTransferOutLineStockTypeToClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public StockType StockTypeTo
        {
            get { return _stockTypeTo; }
            set { SetPropertyValue("StockTypeTo", ref _stockTypeTo, value); }
        }

        #endregion LocationTo

        //#region Lot

        //[ImmediatePostData]
        //[Appearance("InventoryOutLineQtyPackageClose", Criteria = "ActivationPosting = true", Enabled = false)]
        //public double QtyPackage
        //{
        //    get { return _qtyPackage; }
        //    set { SetPropertyValue("QtyPackage", ref _qtyPackage, value); }
        //}

        //[Appearance("InventoryOutLineCountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        //public double Count
        //{
        //    get
        //    {
        //        if (DQty != 0)
        //        {
        //            this._count = this._dQty / this._qtyPackage;
        //        }

        //        return _count;
        //    }
        //    set { SetPropertyValue("Count", ref _count, value); }
        //}

        //[Appearance("InventoryOutLineUnitPackageClose", Criteria = "ActivationPosting = true", Enabled = false)]
        //public UnitOfMeasure UnitPackage
        //{
        //    get { return _unitPackage; }
        //    set { SetPropertyValue("DefaultUOM", ref _unitPackage, value); }
        //}

        //[Appearance("InventoryTransferOutLineProcessLotCountEnabled", Enabled = false, Visibility = ViewItemVisibility.Hide)]
        //public int ProcessLotCount
        //{
        //    get { return _processLotCount; }
        //    set { SetPropertyValue("ProcessLotCount", ref _processLotCount, value); }
        //}

        //#endregion Lot

        [Appearance("InventoryTransferOutLineEstimatedDateEnabled", Enabled = false)]
        public DateTime EstimatedDate
        {
            get { return _estimatedDate; }
            set { SetPropertyValue("EstimatedDate", ref _estimatedDate, value); }
        }

        [Appearance("InventoryTransferOutLineActualDateEnabled", Enabled = false)]
        public DateTime ActualDate
        {
            get { return _actualDate; }
            set { SetPropertyValue("ActualDate", ref _actualDate, value); }
        }

        [Appearance("InventoryTransferOutLineStatusEnabled", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("InventoryTransferOutLineStatusDateEnabled", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("InventoryTransferOutLineReturnTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public ReturnType ReturnType
        {
            get { return _returnType; }
            set { SetPropertyValue("ReturnType", ref _returnType, value); }
        }

        [Appearance("InventoryTransferOutLineProcessCountEnabled", Enabled = false)]
        public int ProcessCount
        {
            get { return _processCount; }
            set { SetPropertyValue("ProcessCount", ref _processCount, value); }
        }

        [Appearance("InventoryTransferOutLineSignCodeClose", Enabled = false)]
        public string SignCode
        {
            get { return _signCode; }
            set { SetPropertyValue("SignCode", ref _signCode, value); }
        }

        [Appearance("InventoryTransferOutLineInventoryTransferEnabled", Enabled = false)]
        [RuleRequiredField(DefaultContexts.Save)]
        [ImmediatePostData()]
        [Association("InventoryTransferOut-InventoryTransferOutLines")]
        public InventoryTransferOut InventoryTransferOut
        {
            get { return _inventoryTransferOut; }
            set
            {
                SetPropertyValue("InventoryTransferOut", ref _inventoryTransferOut, value);
                if (!IsLoading)
                {
                    if (_inventoryTransferOut != null)
                    {
                        if (_inventoryTransferOut.Location != null)
                        {
                            this.CloseLocation = true;
                            this.Location = _inventoryTransferOut.Location;
                        }
                        this.EstimatedDate = _inventoryTransferOut.EstimatedDate;
                        this.StockType = _inventoryTransferOut.StockType;
                        if (this._inventoryTransferOut.Company != null)
                        {
                            this.Company = this._inventoryTransferOut.Company;
                        }
                    }
                }
            }
        }

        [Association("InventoryTransferOutLine-InventoryTransferOutLots")]
        public XPCollection<InventoryTransferOutLot> InventoryTransferOutLots
        {
            get { return GetCollection<InventoryTransferOutLot>("InventoryTransferOutLots"); }
        }

        [Browsable(false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Browsable(false)]
        public SalesOrderMonitoring SalesOrderMonitoring
        {
            get { return _salesOrderMonitoring; }
            set { SetPropertyValue("SalesOrderMonitoring", ref _salesOrderMonitoring, value); }
        }

        [Browsable(false)]
        public PurchaseReturnMonitoring PurchaseReturnMonitoring
        {
            get { return _purchaseReturnMonitoring; }
            set { SetPropertyValue("PurchaseReturnMonitoring", ref _purchaseReturnMonitoring, value); }
        }

        [Appearance("InventoryTransferOutLineFullNameLotNumberEnabled", Enabled = false, Visibility = ViewItemVisibility.Hide)]
        public string FullNameLotNumber
        {
            get
            {
                string _locName = null;

                if (this != null)
                {
                    XPCollection<InventoryTransferOutLot> _locInventoryTransferOutLots = new XPCollection<InventoryTransferOutLot>
                                                                                     (this.Session, new GroupOperator(GroupOperatorType.And,
                                                                                      new BinaryOperator("InventoryTransferOutLine", this)));

                    if (_locInventoryTransferOutLots != null && _locInventoryTransferOutLots.Count() > 0)
                    {
                        foreach (InventoryTransferOutLot _locInventoryTransferOutLot in _locInventoryTransferOutLots)
                        {
                            _locName = _locName + _locInventoryTransferOutLot.LotNumber + ',';
                        }

                        return String.Format("{0}", _locName);
                    }
                    else
                    {
                        return null;
                    }
                }
                return null;
            }
        }

        [Appearance("InventoryTransferOutLineFullNameDescriptionEnabled", Enabled = false, Visibility = ViewItemVisibility.Hide)]
        public string FullNameDescription
        {
            get
            {
                if (this != null)
                {
                    XPCollection<InventoryTransferOutLot> _locInventoryTransferOutLots = new XPCollection<InventoryTransferOutLot>
                                                                                         (this.Session, new GroupOperator(GroupOperatorType.And,
                                                                                          new BinaryOperator("InventoryTransferOutLine", this)));

                    if (_locInventoryTransferOutLots != null && _locInventoryTransferOutLots.Count() > 0)
                    {
                        foreach (InventoryTransferOutLot _locInventoryTransferOutLot in _locInventoryTransferOutLots)
                        {
                            return String.Format("{0} {1} {2} {3}", _locInventoryTransferOutLots.Count(), '@', _locInventoryTransferOutLot.DQty, this.DUOM);
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
                return null;
            }
        }

        #endregion Field

        //===== Code Only =====

        #region No

        public void UpdateNo()
        {
            try
            {
                if (!IsLoading && Session.IsNewObject(this))
                {
                    if (this.InventoryTransferOut != null)
                    {
                        object _makRecord = Session.Evaluate<InventoryTransferOutLine>(CriteriaOperator.Parse("Max(No)"), CriteriaOperator.Parse("InventoryTransferOut=?", this.InventoryTransferOut));
                        this.No = Convert.ToInt32(_makRecord) + 1;
                        this.Save();
                        RecoveryUpdateNo();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferOutLine " + ex.ToString());
            }
        }

        public void RecoveryUpdateNo()
        {
            try
            {
                if (this.InventoryTransferOut != null)
                {
                    InventoryTransferOut _numHeader = Session.FindObject<InventoryTransferOut>
                                                (new BinaryOperator("Code", this.InventoryTransferOut.Code));

                    XPCollection<InventoryTransferOutLine> _numLines = new XPCollection<InventoryTransferOutLine>
                                                (Session, new BinaryOperator("InventoryTransferOut", _numHeader),
                                                new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i1 = 0;
                        foreach (InventoryTransferOutLine _numLine in _numLines)
                        {
                            i1 += 1;
                            _numLine.No = i1;
                            _numLine.Save();
                        }
                        i1 = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferOutLine " + ex.ToString());
            }
        }

        public void RecoveryDeleteNo()
        {
            try
            {
                if (this.InventoryTransferOut != null)
                {
                    InventoryTransferOut _numHeader = Session.FindObject<InventoryTransferOut>
                                                (new BinaryOperator("Code", this.InventoryTransferOut.Code));

                    XPCollection<InventoryTransferOutLine> _numLines = new XPCollection<InventoryTransferOutLine>
                                                (Session, new GroupOperator(GroupOperatorType.And,
                                                 new BinaryOperator("This", this, BinaryOperatorType.NotEqual),
                                                 new BinaryOperator("InventoryTransferOut", _numHeader)),
                                                 new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i = 0;
                        foreach (InventoryTransferOutLine _numLine in _numLines)
                        {
                            i += 1;
                            _numLine.No = i;
                            _numLine.Save();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferOutLine " + ex.ToString());
            }
        }

        #endregion No

        #region Set

        private void SetTotalQty()
        {
            try
            {
                double _locInvLineTotal = 0;
                DateTime now = DateTime.Now;
                if (this.InventoryTransferOut != null)
                {
                    ItemUnitOfMeasure _locItemUOM = Session.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", this.Item),
                                                         new BinaryOperator("UOM", this.UOM),
                                                         new BinaryOperator("DefaultUOM", this.DUOM),
                                                         new BinaryOperator("Active", true)));
                    if (_locItemUOM != null)
                    {
                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                        {
                            _locInvLineTotal = this.Qty * _locItemUOM.DefaultConversion + this.DQty;
                        }
                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                        {
                            _locInvLineTotal = this.Qty / _locItemUOM.Conversion + this.DQty;
                        }
                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                        {
                            _locInvLineTotal = this.Qty + this.DQty;
                        }

                        this.TQty = _locInvLineTotal;
                    }
                    else
                    {
                        _locInvLineTotal = this.Qty + this.DQty;
                        this.TQty = _locInvLineTotal;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferOutLine " + ex.ToString());
            }
        }

        private void SetMaxTotalQty()
        {
            try
            {
                double _locInvLineTotal = 0;
                DateTime now = DateTime.Now;
                if (this.InventoryTransferOut != null)
                {
                    ItemUnitOfMeasure _locItemUOM = Session.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", this.Item),
                                                         new BinaryOperator("UOM", this.MxUOM),
                                                         new BinaryOperator("DefaultUOM", this.MxDUOM),
                                                         new BinaryOperator("Active", true)));
                    if (_locItemUOM != null)
                    {
                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                        {
                            _locInvLineTotal = this.MxQty * _locItemUOM.DefaultConversion + this.MxDQty;
                        }
                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                        {
                            _locInvLineTotal = this.MxQty / _locItemUOM.Conversion + this.MxDQty;
                        }
                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                        {
                            _locInvLineTotal = this.MxQty + this.MxDQty;
                        }

                        this.MxTQty = _locInvLineTotal;
                    }
                    else
                    {
                        _locInvLineTotal = this.MxQty + this.MxDQty;
                        this.MxTQty = _locInvLineTotal;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferOutLine " + ex.ToString());
            }
        }

        private void SetRemainTotalQty()
        {
            try
            {
                double _locInvLineTotal = 0;
                DateTime now = DateTime.Now;
                if (this.InventoryTransferOut != null)
                {
                    ItemUnitOfMeasure _locItemUOM = Session.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", this.Item),
                                                         new BinaryOperator("UOM", this.UOM),
                                                         new BinaryOperator("DefaultUOM", this.DUOM),
                                                         new BinaryOperator("Active", true)));
                    if (_locItemUOM != null)
                    {
                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                        {
                            _locInvLineTotal = this.RmQty * _locItemUOM.DefaultConversion + this.RmDQty;
                        }
                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                        {
                            _locInvLineTotal = this.RmQty / _locItemUOM.Conversion + this.RmDQty;
                        }
                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                        {
                            _locInvLineTotal = this.RmQty + this.RmDQty;
                        }

                        this.RmTQty = _locInvLineTotal;
                    }
                    else
                    {
                        _locInvLineTotal = this.RmQty + this.RmDQty;
                        this.RmTQty = _locInvLineTotal;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferOutLine " + ex.ToString());
            }
        }

        private void SetTotalPQty()
        {
            try
            {
                double _locInvLineTotal = 0;
                DateTime now = DateTime.Now;
                if (this.InventoryTransferOut != null)
                {
                    if (this.Item != null && this.UOM != null && this.DUOM != null)
                    {
                        ItemUnitOfMeasure _locItemUOM = Session.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", this.Item),
                                                         new BinaryOperator("UOM", this.UOM),
                                                         new BinaryOperator("DefaultUOM", this.DUOM),
                                                         new BinaryOperator("Active", true)));
                        if (_locItemUOM != null)
                        {
                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.PQty * _locItemUOM.DefaultConversion + this.PDQty;
                            }
                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.PQty / _locItemUOM.Conversion + this.PDQty;
                            }
                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.PQty + this.PDQty;
                            }

                            this.PTQty = _locInvLineTotal;
                        }
                    }
                    else
                    {
                        _locInvLineTotal = this.PQty + this.PDQty;
                        this.PTQty = _locInvLineTotal;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferOutLine " + ex.ToString());
            }
        }

        #endregion Set

    }
}