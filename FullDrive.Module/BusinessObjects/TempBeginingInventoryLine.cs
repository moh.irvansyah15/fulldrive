﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("FullName")]
    [NavigationItem("Setup")]

    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class TempBeginingInventoryLine : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private string _code;
        private string _itemTemp;
        //private Item _item;
        private string _codeProduct;
        private Brand _brand;
        private Location _location;
        private BinLocation _binLocation;
        private UnitOfMeasure _unitPack;
        private double _qtyBegining;
        private double _qtyMinimal;
        private double _qtyAvailable;
        private double _qtySale;
        private UnitOfMeasure _defaultUom;
        private StockType _stockType;
        private string _lotNumber;
        private DateTime _inDate;
        private Status _status;
        private DateTime _statusDate;
        private Company _company;
        private bool _active;
        private GlobalFunction _globFunc;

        public TempBeginingInventoryLine(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.TempBeginingInventoryLine);
                this.Status = Status.Open;
                this.StatusDate = now;
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("BeginingInventoryLineCodeEnabled", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        //[Persistent]
        //public string FullName
        //{
        //    get
        //    {
        //        string _locName = null;
        //        if (this.Item != null && this.LotNumber != null)
        //        {
        //            if (this.Item.Name != null)
        //            {
        //                _locName = this.Item.Name;
        //            }
        //            return String.Format("({0}) {1}", _locName, this.LotNumber);
        //        }
        //        return null;
        //    }
        //}


        public string ItemTemp
        {
            get { return _itemTemp; }
            set
            {
                SetPropertyValue("ItemTemp", ref _itemTemp, value);

            }
        }

        //public Item Item
        //{
        //    get { return _item; }
        //    set { SetPropertyValue("Item", ref _item, value); }
        //}

        public string CodeProduct
        {
            get { return _codeProduct; }
            set { SetPropertyValue("CodeProduct", ref _codeProduct, value); }
        }

        public Brand Brand
        {
            get { return _brand; }
            set { SetPropertyValue("Brand", ref _brand, value); }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public Location Location
        {
            get { return _location; }
            set { SetPropertyValue("Location", ref _location, value); }
        }

        [DataSourceCriteria("Active = true And Location = '@This.Location'")]
        public BinLocation BinLocation
        {
            get { return _binLocation; }
            set { SetPropertyValue("BinLocation", ref _binLocation, value); }
        }

        public double QtyBegining
        {
            get { return _qtyBegining; }
            set { SetPropertyValue("QtyBegining", ref _qtyBegining, value); }
        }

        public double QtyMinimal
        {
            get { return _qtyMinimal; }
            set { SetPropertyValue("QtyMinimal", ref _qtyMinimal, value); }
        }

        public double QtyAvailable
        {
            get { return _qtyAvailable; }
            set { SetPropertyValue("QtyAvailable", ref _qtyAvailable, value); }
        }

        public double QtySale
        {
            get { return _qtySale; }
            set { SetPropertyValue("QtySale", ref _qtySale, value); }
        }

        public UnitOfMeasure UnitPack
        {
            get { return _unitPack; }
            set { SetPropertyValue("UnitPack", ref _unitPack, value); }
        }

        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure DefaultUOM
        {
            get { return _defaultUom; }
            set { SetPropertyValue("DefaultUOM", ref _defaultUom, value); }
        }

        public StockType StockType
        {
            get { return _stockType; }
            set { SetPropertyValue("StockType", ref _stockType, value); }
        }

        public string LotNumber
        {
            get { return _lotNumber; }
            set { SetPropertyValue("LotNumber", ref _lotNumber, value); }
        }

        public DateTime InDate
        {
            get { return _inDate; }
            set { SetPropertyValue("InDate", ref _inDate, value); }
        }

        
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue("Active", ref _active, value); }
        }

        #endregion Field

    }
}