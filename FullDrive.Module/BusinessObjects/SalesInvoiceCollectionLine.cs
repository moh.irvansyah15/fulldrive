﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Editors;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("SalesInvoiceCollectionLineRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class SalesInvoiceCollectionLine : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private bool _activationPosting;
        private string _code;
        private string _name;
        private InventoryTransferOut _inventoryTransferOut;
        private SalesInvoice _salesInvoice;
        private SalesInvoiceCollection _salesInvoiceCollection;
        private SalesPrePaymentInvoice _salesPrePaymentInvoice;
        private GlobalFunction _globFunc;

        public SalesInvoiceCollectionLine(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.SalesInvoiceCollectionLine);

            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [Appearance("SalesInvoiceCollectionLineCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("SalesInvoiceCollectionLineNameClose", Enabled = false)]
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        [Appearance("SalesInvoiceCollectionLineInventoryTransferOutClose", Enabled = false)]
        public InventoryTransferOut InventoryTransferOut
        {
            get { return _inventoryTransferOut; }
            set { SetPropertyValue("InventoryTransferOut", ref _inventoryTransferOut, value); }
        }

        [Appearance("SalesInvoiceCollectionLineSalesInvoiceClose", Enabled = false)]
        public SalesInvoice SalesInvoice
        {
            get { return _salesInvoice; }
            set { SetPropertyValue("SalesInvoice", ref _salesInvoice, value); }
        }

        [Appearance("SalesInvoiceCollectionLineSalesPrePaymentInvoiceClose", Enabled = false)]
        public SalesPrePaymentInvoice SalesPrePaymentInvoice
        {
            get { return _salesPrePaymentInvoice; }
            set { SetPropertyValue("SalesPrePaymentInvoice", ref _salesPrePaymentInvoice, value); }
        }

        [Appearance("SalesInvoiceCollectionLineSalesInvoiceCollectionClose", Enabled = false)]
        [Association("SalesInvoiceCollection-SalesInvoiceCollectionLines")]
        public SalesInvoiceCollection SalesInvoiceCollection
        {
            get { return _salesInvoiceCollection; }
            set { SetPropertyValue("SalesInvoiceCollection", ref _salesInvoiceCollection, value); }
        }

        #endregion Field
    }
}