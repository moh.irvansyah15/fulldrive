﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.SystemModule;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("ReceivableTransactionLineRuleUnique", DefaultContexts.Save, "Code")]
    [ListViewFilter("AllDataReceivableTransactionLine", "", "All Data", "All Data In Receivable Transaction Line", 1, true)]
    [ListViewFilter("OpenReceivableTransactionLine", "Status = 'Open'", "Open", "Open Data Status In Receivable Transaction Line", 2, true)]
    [ListViewFilter("ProgressReceivableTransactionLine", "Status = 'Progress'", "Progress", "Progress Data Status In Receivable Transaction Line", 3, true)]
    [ListViewFilter("LockReceivableTransactionLine", "Status = 'Close'", "Close", "Close Data Status In Receivable Transaction Line", 4, true)]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class ReceivableTransactionLine : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private bool _activationPosting;
        private int _no;
        private string _code;
        private bool _select;
        private Currency _currency;
        private DateTime _postingDate;
        private PostingType _salesType;
        private PostingMethod _postingMethod;
        private XPCollection<BankAccount> _availableBankAccount;
        private bool _openCompany;
        private Company _company;
        private bool _openCustomer;
        private BusinessPartner _customer;
        private BankAccount _bankAccount;
        private string _accountNo;
        private string _accountName;
        private ChartOfAccount _account;
        private string _description;
        private bool _closeDebit;
        private double _debit;
        private bool _closeCredit;
        private double _credit;
        private Status _status;
        private DateTime _statusdate;
        private Company _companyDefault;
        private SalesInvoice _salesInvoice;
        private SalesPrePaymentInvoice _salesPrePaymentInvoice;
        private SalesOrder _salesOrder;
        private string _signCode;
        private ReceivableTransaction _receivableTransaction;
        private ExchangeRate _exchangeRate;
        private SalesInvoiceMonitoring _salesInvoiceMonitoring;
        private GlobalFunction _globFunc;

        public ReceivableTransactionLine(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                DateTime now = DateTime.Now;
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.ReceivableTransactionLine);
                this.Select = true;
                this.Status = Status.Open;
                this.StatusDate = now;
                this.SalesType = PostingType.Sales;
                this.PostingMethod = PostingMethod.Bill;
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        protected override void OnSaving()
        {
            base.OnSaving();
            if (!IsLoading && IsSaving)
            {
                UpdateNo();
            }
        }

        protected override void OnDeleting()
        {
            base.OnDeleting();
            if (!IsLoading)
            {
                RecoveryDeleteNo();
            }
        }

        #region Field

        //[Browsable(false)]
        [ImmediatePostData()]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [Appearance("ReceivableTransactionLineNoClose", Enabled = false)]
        public int No
        {
            get { return _no; }
            set { SetPropertyValue("No", ref _no, value); }
        }

        [Appearance("ReceivableTransactionLineClose", Enabled = false)]
        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("ReceivableTransactionLineSelectClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public bool Select
        {
            get { return _select; }
            set { SetPropertyValue("Select", ref _select, value); }
        }

        [Appearance("ReceivableTransactionLineCurrencyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Currency Currency
        {
            get { return _currency; }
            set { SetPropertyValue("Currency", ref _currency, value); }
        }

        [Appearance("ReceivableTransactionLinePostingDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime PostingDate
        {
            get { return _postingDate; }
            set { SetPropertyValue("PostingDate", ref _postingDate, value); }
        }

        [Appearance("ReceivableTransactionLineSalesTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PostingType SalesType
        {
            get { return _salesType; }
            set { SetPropertyValue("PostingType", ref _salesType, value); }
        }

        [Appearance("ReceivableTransactionLinePostingMethodClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PostingMethod PostingMethod
        {
            get { return _postingMethod; }
            set { SetPropertyValue("PostingMethod", ref _postingMethod, value); }
        }

        [Appearance("ReceivableTransactionLineOpenCompanyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData]
        public bool OpenCompany
        {
            get { return _openCompany; }
            set
            {
                SetPropertyValue("OpenCompany", ref _openCompany, value);
                if (!IsLoading)
                {
                    if (this._openCompany == true)
                    {
                        this.OpenCustomer = false;
                        this.Customer = null;
                        if(this.ReceivableTransaction != null)
                        {
                            if(this.ReceivableTransaction.CompanyDefault != null)
                            {
                                this.Company = this.ReceivableTransaction.CompanyDefault;
                            }
                        }
                    }
                }
            }
        }

        [Appearance("ReceivableTransactionLineCompanyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Appearance("ReceivableTransactionLineCompanyClose1", Criteria = "OpenCompany = false", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Appearance("ReceivableTransactionLineOpenCustomerClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData]
        public bool OpenCustomer
        {
            get { return _openCustomer; }
            set {
                SetPropertyValue("OpenCustomer", ref _openCustomer, value);
                if(!IsLoading)
                {
                    if(this._openCustomer == true)
                    {
                        this.OpenCompany = false;
                        this.Company = null;
                        if (this.ReceivableTransaction != null)
                        {
                            if (this.ReceivableTransaction.BillToCustomer != null)
                            {
                                this.Customer = this.ReceivableTransaction.BillToCustomer;
                            }
                        }
                    }
                }
            }
        }

        [Appearance("ReceivableTransactionLineVendorClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Appearance("ReceivableTransactionLineVendorClose1", Criteria = "OpenCustomer = false", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public BusinessPartner Customer
        {
            get { return _customer; }
            set { SetPropertyValue("Customer", ref _customer, value); }
        }

        [Browsable(false)]
        public XPCollection<BankAccount> AvailableBankAccount
        {
            get
            {
                if (this.Company != null && this.Customer != null)
                {
                    _availableBankAccount = new XPCollection<BankAccount>(Session);
                }
                else
                {
                    if (this.Company != null)
                    {
                        _availableBankAccount = new XPCollection<BankAccount>(Session,
                                                new GroupOperator(GroupOperatorType.And,
                                                new BinaryOperator("Company", this.Company)));
                    }
                    if (this.Customer != null)
                    {
                        _availableBankAccount = new XPCollection<BankAccount>(Session,
                                                new GroupOperator(GroupOperatorType.And,
                                                new BinaryOperator("BusinessPartner", this.Customer)));
                    }

                }
                return _availableBankAccount;

            }
        }

        [Appearance("ReceivableTransactionLineBankAccountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceProperty("AvailableBankAccount", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        public BankAccount BankAccount
        {
            get { return _bankAccount; }
            set {
                SetPropertyValue("BankAccount", ref _bankAccount, value);
                if (!IsLoading)
                {
                    if (this._bankAccount != null)
                    {
                        this.AccountNo = this._bankAccount.AccountNo;
                        this.AccountName = this._bankAccount.AccountName;
                    }
                }
            }
        }

        [Appearance("ReceivableTransactionLineAccountNoClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string AccountNo
        {
            get { return _accountNo; }
            set { SetPropertyValue("AccountNo", ref _accountNo, value); }
        }

        [Appearance("ReceivableTransactionLineAccountNameClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string AccountName
        {
            get { return _accountName; }
            set { SetPropertyValue("AccountName", ref _accountName, value); }
        }

        [Appearance("ReceivableTransactionLineAccountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public ChartOfAccount Account
        {
            get { return _account; }
            set { SetPropertyValue("Account", ref _account, value); }
        }

        [Appearance("ReceivableTransactionLineDescriptionClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        public bool CloseDebit
        {
            get { return _closeDebit; }
            set { SetPropertyValue("CloseDebit", ref _closeDebit, value); }
        }

        [Appearance("ReceivableTransactionLineDebitClose1", Criteria = "CloseDebit = true", Enabled = false)]
        [Appearance("ReceivableTransactionLineDebitClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double Debit
        {
            get { return _debit; }
            set {
                SetPropertyValue("Debit", ref _debit, value);
                if (!IsLoading)
                {
                    if (this._debit > 0)
                    {
                        this.CloseCredit = true;
                        this.CloseDebit = false;
                    }
                    else
                    {
                        this.CloseCredit = false;
                    }
                }
            }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        public bool CloseCredit
        {
            get { return _closeCredit; }
            set { SetPropertyValue("CloseCredit", ref _closeCredit, value); }
        }

        [Appearance("ReceivableTransactionLineCreditClose1", Criteria = "CloseCredit = true", Enabled = false)]
        [Appearance("ReceivableTransactionLineCreditClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double Credit
        {
            get { return _credit; }
            set {
                SetPropertyValue("Credit", ref _credit, value);
                if (!IsLoading)
                {
                    if (this._credit > 0)
                    {
                        this.CloseCredit = false;
                        this.CloseDebit = true;
                    }
                    else
                    {
                        this.CloseDebit = false;
                    }
                }
            }
        }

        //[Appearance("ReceivableTransactionLineStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("ReceivableTransactionLineStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusdate; }
            set { SetPropertyValue("StatusDate", ref _statusdate, value); }
        }

        [Appearance("ReceivableTransactionLineCompanyDefaultClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Company CompanyDefault
        {
            get { return _companyDefault; }
            set { SetPropertyValue("CompanyDefault", ref _companyDefault, value); }
        }

        [Appearance("ReceivableTransactionLineSalesInvoiceClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public SalesInvoice SalesInvoice
        {
            get { return _salesInvoice; }
            set { SetPropertyValue("SalesInvoice", ref _salesInvoice, value); }
        }

        [Appearance("ReceivableTransactionLineSalesOrderClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public SalesOrder SalesOrder
        {
            get { return _salesOrder; }
            set { SetPropertyValue("SalesOrder", ref _salesOrder, value); }
        }

        [Appearance("ReceivableTransactionLineSalesPrePaymentInvoiceClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public SalesPrePaymentInvoice SalesPrePaymentInvoice
        {
            get { return _salesPrePaymentInvoice; }
            set { SetPropertyValue("SalesPrePaymentInvoice", ref _salesPrePaymentInvoice, value); }
        }

        [Appearance("ReceivableTransactionLineSignCodeClose", Enabled = false)]
        public string SignCode
        {
            get { return _signCode; }
            set { SetPropertyValue("SignCode", ref _signCode, value); }
        }

        [Appearance("ReceivableTransactionLineSalesInvoiceMonitoringClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public SalesInvoiceMonitoring SalesInvoiceMonitoring
        {
            get { return _salesInvoiceMonitoring; }
            set { SetPropertyValue("SalesInvoiceMonitoring", ref _salesInvoiceMonitoring, value); }
        }

        [Association("ReceivableTransaction-ReceivableTransactionLines")]
        public ReceivableTransaction ReceivableTransaction
        {
            get { return _receivableTransaction; }
            set {
                SetPropertyValue("ReceivableTransaction", ref _receivableTransaction, value);
                if(!IsLoading)
                {
                    if(this._receivableTransaction != null)
                    {
                        if (this._receivableTransaction.Currency != null)
                        {
                            this.Currency = this._receivableTransaction.Currency;
                        }
                    }
                }
            }
        }

        [Association("ReceivableTransactionLine-ExchangeRates")]
        public XPCollection<ExchangeRate> ExchangeRates
        {
            get { return GetCollection<ExchangeRate>("ExchangeRates"); }
        }

        #endregion Field


        //================================================== Code Only ==================================================

        #region No

        public void UpdateNo()
        {
            try
            {
                if (!IsLoading && Session.IsNewObject(this))
                {
                    if (this.ReceivableTransaction != null)
                    {
                        object _makRecord = Session.Evaluate<ReceivableTransactionLine>(CriteriaOperator.Parse("Max(No)"), CriteriaOperator.Parse("ReceivableTransaction=?", this.ReceivableTransaction));
                        this.No = Convert.ToInt32(_makRecord) + 1;
                        this.Save();
                        RecoveryUpdateNo();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ReceivableTransactionLine " + ex.ToString());
            }
        }

        public void RecoveryUpdateNo()
        {
            try
            {
                if (this.ReceivableTransaction != null)
                {
                    ReceivableTransaction _numHeader = Session.FindObject<ReceivableTransaction>
                                            (new BinaryOperator("Code", this.ReceivableTransaction.Code));

                    XPCollection<ReceivableTransactionLine> _numLines = new XPCollection<ReceivableTransactionLine>
                                                             (Session, new BinaryOperator("ReceivableTransaction", _numHeader),
                                                              new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i1 = 0;
                        foreach (ReceivableTransactionLine _numLine in _numLines)
                        {
                            i1 += 1;
                            _numLine.No = i1;
                            _numLine.Save();
                        }
                        i1 = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ReceivableTransactionLine " + ex.ToString());
            }
        }

        public void RecoveryDeleteNo()
        {
            try
            {
                if (this.ReceivableTransaction != null)
                {
                    ReceivableTransaction _numHeader = Session.FindObject<ReceivableTransaction>
                                            (new BinaryOperator("Code", this.ReceivableTransaction.Code));

                    XPCollection<ReceivableTransactionLine> _numLines = new XPCollection<ReceivableTransactionLine>
                                                             (Session, new GroupOperator(GroupOperatorType.And,
                                                              new BinaryOperator("This", this, BinaryOperatorType.NotEqual),
                                                              new BinaryOperator("ReceivableTransaction", _numHeader)),
                                                              new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i = 0;
                        foreach (ReceivableTransactionLine _numLine in _numLines)
                        {
                            i += 1;
                            _numLine.No = i;
                            _numLine.Save();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ReceivableTransactionLine " + ex.ToString());
            }
        }

        #endregion No

    }
}