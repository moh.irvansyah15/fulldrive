﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Editors;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Purchase")]
    [RuleCombinationOfPropertiesIsUnique("PurchaseReturnLineRuleUnique", DefaultContexts.Save, "Code")]
    [ListViewFilter("AllDataPurchaseReturnLine", "", "All Data", "All Data In Purchase Return Line", 1, true)]
    [ListViewFilter("OpenPurchaseReturnLine", "Status = 'Open'", "Open", "Open Data Status In Purchase Return Line", 2, true)]
    [ListViewFilter("ProgressPurchaseReturnLine", "Status = 'Progress'", "Progress", "Progress Data Status In Purchase Return Line", 3, true)]
    [ListViewFilter("LockPurchaseReturnLine", "Status = 'Close'", "Close", "Close Data Status In Purchase Return Line", 4, true)]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class PurchaseReturnLine : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private bool _activationQuantity;
        private bool _activationPosting;
        private bool _selectEnabled;
        private int _no;
        private string _code;
        private bool _select;        
        private OrderType _orderType;
        private XPCollection<Item> _availableItem;
        private Item _item;
        private string _name;
        private string _description;
        private XPCollection<UnitOfMeasure> _availableUnitOfMeasure;
        #region InisialisasiMaxQty
        private double _mxDQTY;
        private UnitOfMeasure _mxDUom;
        private double _mxQty;
        private UnitOfMeasure _mxUom;
        private double _mxTQty;
        private double _mxUAmount;
        private double _mxTUAmount;
        #endregion InisialisasiMaxQty
        #region InisialisasiDefaultQty
        private double _dQty;
        private UnitOfMeasure _dUom;
        private double _qty;
        private UnitOfMeasure _uom;
        private double _tQty;
        #endregion InisialisasiDefaultQty
        #region InisialisasiRemainQty
        private double _rmDQty;
        private double _rmQty;
        private double _rmTQty;
        #endregion InisialisasiRemainQty
        #region InitialPostingQuantityTo
        private double _pDQty;
        private UnitOfMeasure _pDUom;
        private double _pQty;
        private UnitOfMeasure _pUom;
        private double _pTQty;
        #endregion InitialPostingQuantityTo
        #region InisialisasiAmount
        private Currency _currency;
        private double _uAmount;
        private double _tUAmount;
        private bool _multiTax;
        private Tax _tax;
        private double _txValue;
        private double _txAmount;
        private bool _multiDiscount;
        private Discount _discount;
        private double _disc;
        private double _discAmount;
        private double _tAmount;
        #endregion InisialisasiAmount
        private ReturnType _returnType;
        private Status _status;
        private DateTime _statusDate;
        private DateTime _estimatedDate;
        private int _postedCount;
        private string _signCode;
        private Company _company;
        private PurchaseOrderMonitoring _purchaseOrderMonitoring;
        private InventoryTransferInMonitoring _inventoryTransferInMonitoring;
        private PurchaseReturn _purchaseReturn;
        private GlobalFunction _globFunc;

        public PurchaseReturnLine(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.PurchaseReturnLine);
                this.Status = Status.Open;
                this.StatusDate = now;
                this.OrderType = OrderType.Item;
                this.Select = true;
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        protected override void OnSaving()
        {
            base.OnSaving();
            if (!IsLoading && IsSaving)
            {
                UpdateNo();
                SetActivationPostingBasedActivationQuantity();
            }
        }

        protected override void OnDeleting()
        {
            base.OnDeleting();
            if (!IsLoading)
            {
                RecoveryDeleteNo();
            }
        }

        [Browsable(false)]
        public bool ActivationQuantity
        {
            get { return _activationQuantity; }
            set { SetPropertyValue("ActivationQuantity", ref _activationQuantity, value); }
        }

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        public bool SelectEnabled
        {
            get { return _selectEnabled; }
            set { SetPropertyValue("SelectEnabled", ref _selectEnabled, value); }
        }

        [Appearance("PurchaseReturnLineNoClose", Enabled = false)]
        public int No
        {
            get { return _no; }
            set { SetPropertyValue("No", ref _no, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("PurchaseReturnLineCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("PurchaseReturnLineSelectClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public bool Select
        {
            get { return _select; }
            set { SetPropertyValue("Select", ref _select, value); }
        }

        [ImmediatePostData()]
        [Appearance("PurchaseReturnLineOrderTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Appearance("PurchaseReturnLineOrderTypeClose2", Criteria = "ActivationQuantity = true", Enabled = false)]
        public OrderType OrderType
        {
            get { return _orderType; }
            set { SetPropertyValue("OrderType", ref _orderType, value); }
        }

        [Browsable(false)]
        public XPCollection<Item> AvailableItem
        {
            get
            {
                if (OrderType == OrderType.Item)
                {
                    _availableItem = new XPCollection<Item>(Session,
                                    new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("OrderType", OrderType.Item)));

                }
                else if (OrderType == OrderType.Service)
                {
                    _availableItem = new XPCollection<Item>(Session,
                                    new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("OrderType", OrderType.Service)));
                }
                else
                {
                    _availableItem = new XPCollection<Item>(Session);
                }

                return _availableItem;

            }
        }

        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        [DataSourceProperty("AvailableItem", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("PurchaseReturnLineItemClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Appearance("PurchaseReturnLineItemClose2", Criteria = "ActivationQuantity = true", Enabled = false)]
        public Item Item
        {
            get { return _item; }
            set
            {
                SetPropertyValue("Item", ref _item, value);
                if (!IsLoading)
                {
                    if (this._item != null)
                    {
                        this.DUOM = this._item.BasedUOM;
                        this.Description = GetDescriptions();
                    }
                }
            }
        }

        [Appearance("PurchaseReturnLineNameClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Appearance("PurchaseReturnLineNameClose2", Criteria = "ActivationQuantity = true", Enabled = false)]
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        [Appearance("PurchaseReturnLineDescriptionClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Appearance("PurchaseReturnLineDescriptionClose2", Criteria = "ActivationQuantity = true", Enabled = false)]
        [Size(512)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        [Browsable(false)]
        public XPCollection<UnitOfMeasure> AvailableUnitOfMeasure
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;
                if (Item == null)
                {
                    _availableUnitOfMeasure = new XPCollection<UnitOfMeasure>(Session);
                }
                else
                {
                    List<string> _stringUOM = new List<string>();

                    XPCollection<ItemUnitOfMeasure> _locItemUnitOfMeasures = new XPCollection<ItemUnitOfMeasure>
                                                                              (Session, new GroupOperator(GroupOperatorType.And,
                                                                               new BinaryOperator("Item", this.Item),
                                                                               new BinaryOperator("Active", true)));

                    if (_locItemUnitOfMeasures != null && _locItemUnitOfMeasures.Count() > 0)
                    {
                        foreach (ItemUnitOfMeasure _locItemUnitOfMeasure in _locItemUnitOfMeasures)
                        {
                            _stringUOM.Add(_locItemUnitOfMeasure.UOM.Code);
                        }
                    }

                    IEnumerable<string> _stringArrayUOMDistinct = _stringUOM.Distinct();
                    string[] _stringArrayUOMList = _stringArrayUOMDistinct.ToArray();
                    if (_stringArrayUOMList.Length == 1)
                    {
                        for (int i = 0; i < _stringArrayUOMList.Length; i++)
                        {
                            UnitOfMeasure _locUOM = Session.FindObject<UnitOfMeasure>(new BinaryOperator("Code", _stringArrayUOMList[i]));
                            if (_locUOM != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locUOM.Code + "'";
                                }
                            }
                        }
                    }
                    else if (_stringArrayUOMList.Length > 1)
                    {
                        for (int i = 0; i < _stringArrayUOMList.Length; i++)
                        {
                            UnitOfMeasure _locUOM = Session.FindObject<UnitOfMeasure>(new BinaryOperator("Code", _stringArrayUOMList[i]));
                            if (_locUOM != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locUOM.Code + "'";
                                }
                                else
                                {
                                    _endString = _endString + " OR [Code]=='" + _locUOM.Code + "'";
                                }
                            }
                        }
                    }
                    _fullString = _beginString + _endString;

                    if (_fullString != null)
                    {
                        _availableUnitOfMeasure = new XPCollection<UnitOfMeasure>(Session, CriteriaOperator.Parse(_fullString));
                    }

                }
                return _availableUnitOfMeasure;

            }
        }

        #region MaxDefaultQty

        [Appearance("PurchaseReturnLineMxDQtyClose", Enabled = false)]
        [ImmediatePostData()]
        public double MxDQty
        {
            get { return _mxDQTY; }
            set
            {
                SetPropertyValue("MxDQty", ref _mxDQTY, value);
                if (!IsLoading)
                {
                    SetMaxTotalQty();
                }
            }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        [Appearance("PurchaseReturnLineMxDUOMClose", Enabled = false)]
        public UnitOfMeasure MxDUOM
        {
            get { return _mxDUom; }
            set
            {
                SetPropertyValue("MxDUOM", ref _mxDUom, value);
                if (!IsLoading)
                {
                    SetMaxTotalQty();
                }
            }
        }

        [ImmediatePostData()]
        [Appearance("PurchaseReturnLineMxQtyClose", Enabled = false)]
        public double MxQty
        {
            get { return _mxQty; }
            set
            {
                SetPropertyValue("MxQty", ref _mxQty, value);
                if (!IsLoading)
                {
                    SetMaxTotalQty();
                }
            }
        }

        [DataSourceProperty("AvailableUnitOfMeasure", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        [Appearance("PurchaseReturnLineMxUOMClose", Enabled = false)]
        public UnitOfMeasure MxUOM
        {
            get { return _mxUom; }
            set
            {
                SetPropertyValue("MxUOM", ref _mxUom, value);
                if (!IsLoading)
                {
                    SetMaxTotalQty();
                }
            }
        }

        [Appearance("PurchaseReturnLineMxTQtyClose", Enabled = false)]
        public double MxTQty
        {
            get { return _mxTQty; }
            set { SetPropertyValue("MxTQty", ref _mxTQty, value); }
        }

        [Appearance("PurchaseReturnLineMxUAmountClose", Enabled = false)]
        public double MxUAmount
        {
            get { return _mxUAmount; }
            set { SetPropertyValue("MxUAmount", ref _mxUAmount, value); }
        }

        [Appearance("PurchaseReturnLineMxTUAmountClose", Enabled = false)]
        public double MxTUAmount
        {
            get { return _mxTUAmount; }
            set { SetPropertyValue("MxTUAmount", ref _mxTUAmount, value); }
        }
        #endregion MaxDefaultQty

        #region DefaultQty

        [Appearance("PurchaseReturnLineDQtyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double DQty
        {
            get { return _dQty; }
            set
            {
                SetPropertyValue("DQty", ref _dQty, value);
                if (!IsLoading)
                {
                    SetMxDQty();
                    SetTotalQty();
                    SetTotalUnitAmount();
                    SetTaxAmount();
                    SetDiscAmount();
                    SetTotalAmount();
                }
            }
        }

        [Appearance("PurchaseReturnLineDUOMClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure DUOM
        {
            get { return _dUom; }
            set
            {
                SetPropertyValue("DUOM", ref _dUom, value);
                if (!IsLoading)
                {
                    SetMxDUOM();
                    SetTotalQty();
                    SetTotalUnitAmount();
                    SetTaxAmount();
                    SetDiscAmount();
                    SetTotalAmount();
                }
            }
        }

        [Appearance("PurchaseReturnLineQtyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double Qty
        {
            get { return _qty; }
            set
            {
                SetPropertyValue("Qty", ref _qty, value);
                if (!IsLoading)
                {
                    SetMxQty();
                    SetTotalQty();
                    SetTotalUnitAmount();
                    SetTaxAmount();
                    SetDiscAmount();
                    SetTotalAmount();
                }
            }
        }

        [Appearance("PurchaseReturnLineUOMClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceProperty("AvailableUnitOfMeasure", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure UOM
        {
            get { return _uom; }
            set
            {
                SetPropertyValue("UOM", ref _uom, value);
                if (!IsLoading)
                {
                    SetMxUOM();
                    SetTotalQty();
                    SetTotalUnitAmount();
                    SetTaxAmount();
                    SetDiscAmount();
                    SetTotalAmount();
                }
            }
        }

        [Appearance("PurchaseReturnLineTotalQtyEnabled", Enabled = false)]
        public double TQty
        {
            get { return _tQty; }
            set { SetPropertyValue("TotalQty", ref _tQty, value); }
        }

        #endregion DefaultQty

        #region RemainQty

        [Appearance("PurchaseReturnLineRmDQtyClose", Enabled = false)]
        public double RmDQty
        {
            get { return _rmDQty; }
            set { SetPropertyValue("RmDQty", ref _rmDQty, value); }
        }

        [Appearance("PurchaseReturnLineRmQtyClose", Enabled = false)]
        public double RmQty
        {
            get { return _rmQty; }
            set { SetPropertyValue("RmQty", ref _rmQty, value); }
        }

        [Appearance("PurchaseReturnLineRmTQtyClose", Enabled = false)]
        public double RmTQty
        {
            get { return _rmTQty; }
            set { SetPropertyValue("RmTQty", ref _rmTQty, value); }
        }

        #endregion RemainQty

        #region PostingQty

        [Appearance("PurchaseReturnLinePDQtyClose", Enabled = false)]
        public double PDQty
        {
            get { return _pDQty; }
            set
            {
                SetPropertyValue("PDQty", ref _pDQty, value);
                if (!IsLoading)
                {
                    SetTotalPQty();
                }
            }
        }

        [Appearance("PurchaseReturnLinePDUOMClose", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure PDUOM
        {
            get { return _pDUom; }
            set
            {
                SetPropertyValue("PDUOM", ref _pDUom, value);
                if (!IsLoading)
                {
                    SetTotalPQty();
                }
            }
        }

        [Appearance("PurchaseReturnLinePQtyToClose", Enabled = false)]
        public double PQty
        {
            get { return _pQty; }
            set
            {
                SetPropertyValue("PQty", ref _pQty, value);
                if (!IsLoading)
                {
                    SetTotalPQty();
                }
            }
        }

        [Appearance("PurchaseReturnLinePUOMClose", Enabled = false)]
        [DataSourceProperty("AvailableUnitOfMeasure", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure PUOM
        {
            get { return _pUom; }
            set
            {
                SetPropertyValue("PUOM", ref _pUom, value);
                if (!IsLoading)
                {
                    SetTotalPQty();
                }
            }
        }

        [Appearance("PurchaseReturnLinePTQtyEnabled", Enabled = false)]
        public double PTQty
        {
            get { return _pTQty; }
            set { SetPropertyValue("PTQty", ref _pTQty, value); }
        }

        #endregion PostingQty

        #region Amount
        [Appearance("PurchaseReturnLineCurrencyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Appearance("PurchaseReturnLineCurrencyClose2", Criteria = "ActivationQuantity = true", Enabled = false)]
        public Currency Currency
        {
            get { return _currency; }
            set { SetPropertyValue("Currency", ref _currency, value); }
        }

        [Appearance("PurchaseReturnLineUAmountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Appearance("PurchaseReturnLineUAmountClose2", Criteria = "ActivationQuantity = true", Enabled = false)]
        [ImmediatePostData()]
        public double UAmount
        {
            get { return _uAmount; }
            set
            {
                SetPropertyValue("UAmount", ref _uAmount, value);
                if (!IsLoading)
                {
                    _globFunc = new GlobalFunction();
                    if (this._uAmount > 0)
                    {
                        if (_globFunc.GetRoundingList(Session, ObjectList.PurchaseReturnLine, FieldName.UAmount) == true)
                        {
                            this._uAmount = _globFunc.GetRoundUp(Session, this._uAmount, ObjectList.PurchaseReturnLine, FieldName.UAmount);
                        }
                    }
                    SetTotalUnitAmount();
                    SetTaxAmount();
                    SetDiscAmount();
                    SetTotalAmount();
                }
            }
        }

        [Appearance("PurchaseReturnLineTUAmountEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double TUAmount
        {
            get { return _tUAmount; }
            set { SetPropertyValue("TUAmount", ref _tUAmount, value); }
        }

        [Appearance("PurchaseReturnLineMultiTaxClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Appearance("PurchaseReturnLineMultiTaxClose2", Criteria = "ActivationQuantity = true", Enabled = false)]
        [ImmediatePostData()]
        public bool MultiTax
        {
            get { return _multiTax; }
            set
            {
                SetPropertyValue("MultiTax", ref _multiTax, value);
                if (!IsLoading)
                {
                    SetNormalTax();
                }
            }
        }

        [Appearance("PurchaseReturnLineTaxClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Appearance("PurchaseReturnLineTaxClose2", Criteria = "ActivationQuantity = true", Enabled = false)]
        [Appearance("PurchaseReturnLineTaxEnabled", Criteria = "MultiTax = true", Enabled = false)]
        [ImmediatePostData]
        [DataSourceCriteria("Active = true")]
        public Tax Tax
        {
            get { return _tax; }
            set
            {
                SetPropertyValue("Tax", ref _tax, value);
                if (!IsLoading)
                {
                    if (this._tax != null)
                    {
                        this.TxValue = this._tax.Value;
                        SetTaxAmount();
                        SetTotalAmount();
                    }
                    else
                    {
                        SetNormalTax();
                    }
                }
            }
        }

        [Appearance("PurchaseReturnLineTxValueClose", Enabled = false)]
        [ImmediatePostData()]
        public double TxValue
        {
            get { return _txValue; }
            set
            {
                SetPropertyValue("TxValue", ref _txValue, value);
                if (!IsLoading)
                {
                    SetTaxAmount();
                    SetTotalAmount();
                }
            }
        }

        [Appearance("PurchaseReturnLineTxAmountEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double TxAmount
        {
            get { return _txAmount; }
            set
            {
                SetPropertyValue("TxAmount", ref _txAmount, value);
                if (!IsLoading)
                {
                    _globFunc = new GlobalFunction();
                    if (this._txAmount > 0)
                    {
                        if (_globFunc.GetRoundingList(Session, ObjectList.PurchaseReturnLine, FieldName.TxAmount) == true)
                        {
                            this._txAmount = _globFunc.GetRoundUp(Session, this._txAmount, ObjectList.PurchaseReturnLine, FieldName.TxAmount);
                        }
                    }

                    SetTotalAmount();
                }
            }
        }

        [Appearance("PurchaseReturnLineMultiDiscountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Appearance("PurchaseReturnLineMultiDiscountClose2", Criteria = "ActivationQuantity = true", Enabled = false)]
        [ImmediatePostData()]
        public bool MultiDiscount
        {
            get { return _multiDiscount; }
            set
            {
                SetPropertyValue("MultiDiscount", ref _multiDiscount, value);
                if (!IsLoading)
                {
                    SetNormalDiscount();
                }
            }
        }

        [Appearance("PurchaseReturnLineDiscountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Appearance("PurchaseReturnLineDiscountClose2", Criteria = "ActivationQuantity = true", Enabled = false)]
        [Appearance("PurchaseReturnLineDiscountEnabled", Criteria = "MultiDiscount = true", Enabled = false)]
        [ImmediatePostData()]
        public Discount Discount
        {
            get { return _discount; }
            set
            {
                SetPropertyValue("Discount", ref _discount, value);
                if (!IsLoading)
                {
                    if (this._discount != null)
                    {
                        this.Disc = this._discount.Value;
                    }
                    else
                    {
                        SetNormalDiscount();
                    }
                }
            }
        }

        [Appearance("PurchaseReturnLineDiscClose", Enabled = false)]
        [Appearance("PurchaseReturnLineDiscEnabled", Criteria = "MultiDiscount = true", Enabled = false)]
        [ImmediatePostData()]
        public double Disc
        {
            get { return _disc; }
            set
            {
                SetPropertyValue("Disc", ref _disc, value);
                if (!IsLoading)
                {
                    SetDiscAmount();
                    SetTotalAmount();
                }
            }
        }

        [Appearance("PurchaseReturnLineDiscAmountEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double DiscAmount
        {
            get { return _discAmount; }
            set
            {
                SetPropertyValue("DiscAmount", ref _discAmount, value);
                if (!IsLoading)
                {
                    _globFunc = new GlobalFunction();
                    if (this._discAmount > 0)
                    {
                        if (_globFunc.GetRoundingList(Session, ObjectList.PurchaseReturnLine, FieldName.DiscAmount) == true)
                        {
                            this._discAmount = _globFunc.GetRoundUp(Session, this._discAmount, ObjectList.PurchaseReturnLine, FieldName.DiscAmount);
                        }
                    }
                }
            }
        }

        [Appearance("PurchaseReturnLineTPriceEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double TAmount
        {
            get { return _tAmount; }
            set
            {
                SetPropertyValue("TAmount", ref _tAmount, value);
                if (!IsLoading)
                {
                    _globFunc = new GlobalFunction();
                    if (this._tAmount > 0)
                    {
                        if (_globFunc.GetRoundingList(Session, ObjectList.PurchaseReturnLine, FieldName.TAmount) == true)
                        {
                            this._tAmount = _globFunc.GetRoundUp(Session, this._tAmount, ObjectList.PurchaseReturnLine, FieldName.TAmount);
                        }
                    }
                }
            }
        }
        #endregion Amount

        [Appearance("PurchaseReturnLineEstimationDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Appearance("PurchaseReturnLineEstimationDateClose2", Criteria = "ActivationQuantity = true", Enabled = false)]
        public DateTime EstimatedDate
        {
            get { return _estimatedDate; }
            set { SetPropertyValue("EstimatedDate", ref _estimatedDate, value); }
        }

        [Appearance("PurchaseReturnLineReturnTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public ReturnType ReturnType
        {
            get { return _returnType; }
            set { SetPropertyValue("ReturnType", ref _returnType, value); }
        }

        [Appearance("PurchaseReturnLineStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("PurchaseReturnLineStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("PurchaseReturnLinePostedCountEnabled", Enabled = false)]
        public int PostedCount
        {
            get { return _postedCount; }
            set { SetPropertyValue("PostedCount", ref _postedCount, value); }
        }

        [Appearance("PurchaseReturnLineSignCodeClose", Enabled = false)]
        public string SignCode
        {
            get { return _signCode; }
            set { SetPropertyValue("SignCode", ref _signCode, value); }
        }

        [Browsable(false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Browsable(false)]
        public PurchaseOrderMonitoring PurchaseOrderMonitoring
        {
            get { return _purchaseOrderMonitoring; }
            set { SetPropertyValue("PurchaseOrderMonitoring", ref _purchaseOrderMonitoring, value); }
        }


        [Browsable(false)]
        public InventoryTransferInMonitoring InventoryTransferInMonitoring
        {
            get { return _inventoryTransferInMonitoring; }
            set { SetPropertyValue("InventoryTransferInMonitoring", ref _inventoryTransferInMonitoring, value); }
        }

        [ImmediatePostData()]
        [Appearance("PurchaseReturnLinePurchaseReturnClose", Enabled = false)]
        [RuleRequiredField(DefaultContexts.Save)]
        [Association("PurchaseReturn-PurchaseReturnLines")]
        public PurchaseReturn PurchaseReturn
        {
            get { return _purchaseReturn; }
            set
            {
                SetPropertyValue("PurchaseReturn", ref _purchaseReturn, value);
                if (!IsLoading)
                {
                    if (this._purchaseReturn != null)
                    {
                        if (this._purchaseReturn.EstimatedDate != null)
                        {
                            this.EstimatedDate = this._purchaseReturn.EstimatedDate;
                        }
                        if (this._purchaseReturn.Company != null)
                        {
                            this.Company = this._purchaseReturn.Company;
                        }
                    }
                }
            }
        }

        //private string _PersistentProperty;
        //[XafDisplayName("My display name"), ToolTip("My hint message")]
        //[ModelDefault("EditMask", "(000)-00"), Index(0), VisibleInListView(false)]
        //[Persistent("DatabaseColumnName"), RuleRequiredField(DefaultContexts.Save)]
        //public string PersistentProperty {
        //    get { return _PersistentProperty; }
        //    set { SetPropertyValue("PersistentProperty", ref _PersistentProperty, value); }
        //}

        //[Action(Caption = "My UI Action", ConfirmationMessage = "Are you sure?", ImageName = "Attention", AutoCommit = true)]
        //public void ActionMethod() {
        //    // Trigger a custom business logic for the current record in the UI (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112619.aspx).
        //    this.PersistentProperty = "Paid";
        //}

        //================================================== Code Only ==================================================

        #region No

        public void UpdateNo()
        {
            try
            {
                if (!IsLoading && Session.IsNewObject(this))
                {
                    if (this.PurchaseReturn != null)
                    {
                        object _makRecord = Session.Evaluate<PurchaseReturnLine>(CriteriaOperator.Parse("Max(No)"), CriteriaOperator.Parse("PurchaseReturn=?", this.PurchaseReturn));
                        this.No = Convert.ToInt32(_makRecord) + 1;
                        this.Save();
                        RecoveryUpdateNo();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseReturnLine " + ex.ToString());
            }
        }

        public void RecoveryUpdateNo()
        {
            try
            {
                if (this.PurchaseReturn != null)
                {
                    PurchaseReturn _numHeader = Session.FindObject<PurchaseReturn>
                                                (new BinaryOperator("Code", this.PurchaseReturn.Code));

                    XPCollection<PurchaseReturnLine> _numLines = new XPCollection<PurchaseReturnLine>
                                                (Session, new BinaryOperator("PurchaseReturn", _numHeader),
                                                new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i1 = 0;
                        foreach (PurchaseReturnLine _numLine in _numLines)
                        {
                            i1 += 1;
                            _numLine.No = i1;
                            _numLine.Save();
                        }
                        i1 = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseReturnLine " + ex.ToString());
            }
        }

        public void RecoveryDeleteNo()
        {
            try
            {
                if (this.PurchaseReturn != null)
                {
                    PurchaseReturn _numHeader = Session.FindObject<PurchaseReturn>
                                                (new BinaryOperator("Code", this.PurchaseReturn.Code));

                    XPCollection<PurchaseReturnLine> _numLines = new XPCollection<PurchaseReturnLine>
                                                (Session, new GroupOperator(GroupOperatorType.And,
                                                 new BinaryOperator("This", this, BinaryOperatorType.NotEqual),
                                                 new BinaryOperator("PurchaseReturn", _numHeader)),
                                                 new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i = 0;
                        foreach (PurchaseReturnLine _numLine in _numLines)
                        {
                            i += 1;
                            _numLine.No = i;
                            _numLine.Save();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseReturnLine " + ex.ToString());
            }
        }

        #endregion No

        public string GetDescriptions()
        {
            string _result = "";
            try
            {
                if (this._item != null)
                {
                    string _result1 = null;
                    string _result2 = null;
                    if (this._item.Name != null)
                    {
                        _result1 = this._item.Name;
                    }
                    if (this._item.Description != null)
                    {
                        _result2 = this._item.Description;
                    }
                    _result = _result1 + " " + _result2;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseReturnLine " + ex.ToString());
            }

            return _result;
        }

        #region Set

        //Done
        private void SetTotalUnitAmount()
        {
            try
            {
                _globFunc = new GlobalFunction();

                if (_tQty >= 0 & _uAmount >= 0)
                {
                    if (_globFunc.GetRoundingList(Session, ObjectList.PurchaseReturnLine, FieldName.TUAmount) == true)
                    {
                        this.TUAmount = _globFunc.GetRoundUp(Session, (this.TQty * this.UAmount), ObjectList.PurchaseReturnLine, FieldName.TUAmount);
                    }
                    else
                    {
                        this.TUAmount = this.TQty * this.UAmount;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseReturnLine " + ex.ToString());
            }
        }

        //Done
        private void SetTotalAmount()
        {
            try
            {
                _globFunc = new GlobalFunction();

                if (_uAmount >= 0 && _txAmount >= 0 && _discAmount >= 0)
                {
                    if (!IsLoading)
                    {
                        if (this.Tax != null)
                        {
                            if (this.Tax.TaxNature == TaxNature.Increase)
                            {
                                if (_globFunc.GetRoundingList(Session, ObjectList.PurchaseReturnLine, FieldName.TAmount) == true)
                                {
                                    this.TAmount = _globFunc.GetRoundUp(Session, (this.TUAmount + this.TxAmount - this.DiscAmount), ObjectList.PurchaseReturnLine, FieldName.TAmount);
                                }
                                else
                                {
                                    this.TAmount = this.TUAmount + this.TxAmount - this.DiscAmount;
                                }
                            }
                            else if (this.Tax.TaxNature == TaxNature.Decrease)
                            {
                                if (_globFunc.GetRoundingList(Session, ObjectList.PurchaseReturnLine, FieldName.TAmount) == true)
                                {
                                    this.TAmount = _globFunc.GetRoundUp(Session, (this.TUAmount - this.TxAmount - this.DiscAmount), ObjectList.PurchaseReturnLine, FieldName.TAmount);
                                }
                                else
                                {
                                    this.TAmount = this.TUAmount - this.TxAmount - this.DiscAmount;
                                }
                            }
                            else if (this.Tax.TaxNature == TaxNature.None)
                            {
                                if (_globFunc.GetRoundingList(Session, ObjectList.PurchaseReturnLine, FieldName.TAmount) == true)
                                {
                                    this.TAmount = _globFunc.GetRoundUp(Session, (this.TUAmount - this.DiscAmount), ObjectList.PurchaseReturnLine, FieldName.TAmount);
                                }
                                else
                                {
                                    this.TAmount = this.TUAmount - this.DiscAmount;
                                }
                            }
                            else
                            {
                                if (_globFunc.GetRoundingList(Session, ObjectList.PurchaseReturnLine, FieldName.TAmount) == true)
                                {
                                    this.TAmount = _globFunc.GetRoundUp(Session, (this.TUAmount + this.TxAmount - this.DiscAmount), ObjectList.PurchaseReturnLine, FieldName.TAmount);
                                }
                                else
                                {
                                    this.TAmount = this.TUAmount + this.TxAmount - this.DiscAmount;
                                }
                            }
                        }
                        //clm
                        else
                        {
                            this.TAmount = this.TUAmount + this.TxAmount - this.DiscAmount;
                        }
                    }
                }
                //clm
                else
                {
                    this.TAmount = this.TUAmount + this.TxAmount - this.DiscAmount;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseReturnLine " + ex.ToString());
            }
        }

        //Done
        private void SetTotalQty()
        {
            try
            {
                double _locInvLineTotal = 0;
                DateTime now = DateTime.Now;
                if (this.PurchaseReturn != null)
                {
                    if (this.Item != null && this.UOM != null && this.DUOM != null)
                    {
                        ItemUnitOfMeasure _locItemUOM = Session.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", this.Item),
                                                         new BinaryOperator("UOM", this.UOM),
                                                         new BinaryOperator("DefaultUOM", this.DUOM),
                                                         new BinaryOperator("Active", true)));
                        if (_locItemUOM != null)
                        {
                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.Qty * _locItemUOM.DefaultConversion + this.DQty;
                            }
                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.Qty / _locItemUOM.Conversion + this.DQty;
                            }
                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.Qty + this.DQty;
                            }

                            this.TQty = _locInvLineTotal;
                        }
                    }
                    else
                    {
                        _locInvLineTotal = this.Qty + this.DQty;
                        this.TQty = _locInvLineTotal;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseReturnLine " + ex.ToString());
            }
        }

        //Done
        private void SetDiscAmount()
        {
            try
            {
                _globFunc = new GlobalFunction();

                if (_uAmount >= 0 && _discAmount >= 0)
                {
                    if (_globFunc.GetRoundingList(Session, ObjectList.PurchaseReturnLine, FieldName.DiscAmount) == true)
                    {
                        this.DiscAmount = _globFunc.GetRoundUp(Session, (this.TUAmount * this.Disc / 100), ObjectList.PurchaseReturnLine, FieldName.DiscAmount);
                    }
                    else
                    {
                        this.DiscAmount = this.TUAmount * this.Disc / 100;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseReturnLine " + ex.ToString());
            }
        }

        //Done
        private void SetTaxAmount()
        {
            try
            {
                _globFunc = new GlobalFunction();

                if (_uAmount >= 0 && Tax.Value >= 0)
                {
                    if (_globFunc.GetRoundingList(Session, ObjectList.PurchaseReturnLine, FieldName.TxAmount) == true)
                    {
                        this.TxAmount = _globFunc.GetRoundUp(Session, (this.TUAmount * this.TxValue / 100), ObjectList.PurchaseReturnLine, FieldName.TxAmount);
                    }
                    else
                    {
                        this.TxAmount = this.TUAmount * this.TxValue / 100;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseReturnLine " + ex.ToString());
            }
        }

        //Done
        private void SetMaxTotalQty()
        {
            try
            {
                double _locInvLineTotal = 0;
                DateTime now = DateTime.Now;
                if (this.PurchaseReturn != null)
                {
                    if (this.Item != null && this.MxUOM != null && this.MxDUOM != null)
                    {
                        ItemUnitOfMeasure _locItemUOM = Session.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", this.Item),
                                                         new BinaryOperator("UOM", this.MxUOM),
                                                         new BinaryOperator("DefaultUOM", this.MxDUOM),
                                                         new BinaryOperator("Active", true)));
                        if (_locItemUOM != null)
                        {
                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.MxQty * _locItemUOM.DefaultConversion + this.MxDQty;
                            }
                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.MxQty / _locItemUOM.Conversion + this.MxDQty;
                            }
                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.MxQty + this.MxDQty;
                            }

                            this.MxTQty = _locInvLineTotal;
                        }
                    }
                    else
                    {
                        _locInvLineTotal = this.MxQty + this.MxDQty;
                        this.MxTQty = _locInvLineTotal;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseReturnLine " + ex.ToString());
            }
        }

        private void SetMaxTotalUnitAmount()
        {
            try
            {
                double _locInvLineTotal = 0;
                DateTime now = DateTime.Now;
                if (this.PurchaseReturn != null)
                {
                    if (this._mxTQty >= 0 && this._mxUAmount >= 0)
                    {
                        this.MxTUAmount = this._mxTQty * this.MxUAmount;
                    }
                    else
                    {
                        _locInvLineTotal = this._mxTQty + this.MxDQty;
                        this.MxTQty = _locInvLineTotal;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseReturnLine " + ex.ToString());
            }
        }

        //Done
        private void SetTotalPQty()
        {
            try
            {
                double _locInvLineTotal = 0;
                DateTime now = DateTime.Now;
                if (this.PurchaseReturn != null)
                {
                    if (this.Item != null && this.UOM != null && this.DUOM != null)
                    {
                        ItemUnitOfMeasure _locItemUOM = Session.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", this.Item),
                                                         new BinaryOperator("UOM", this.UOM),
                                                         new BinaryOperator("DefaultUOM", this.DUOM),
                                                         new BinaryOperator("Active", true)));
                        if (_locItemUOM != null)
                        {
                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.PQty * _locItemUOM.DefaultConversion + this.PDQty;
                            }
                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.PQty / _locItemUOM.Conversion + this.PDQty;
                            }
                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.PQty + this.PDQty;
                            }

                            this.PTQty = _locInvLineTotal;
                        }
                    }
                    else
                    {
                        _locInvLineTotal = this.PQty + this.PDQty;
                        this.PTQty = _locInvLineTotal;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseReturnLine " + ex.ToString());
            }
        }

        #region SetMaxInQty

        //Done
        private void SetMxDQty()
        {
            try
            {
                if (this.PurchaseReturn != null)
                {

                    XPCollection<ApprovalLine> _locApprovalLineXPOs = new XPCollection<ApprovalLine>
                                                                   (Session, new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("PurchaseReturn", this.PurchaseReturn)));
                    if (_locApprovalLineXPOs != null && _locApprovalLineXPOs.Count() > 0)
                    {
                        if (this.Status == Status.Posted || this.PostedCount > 0)
                        {
                            if (this.RmDQty > 0)
                            {
                                if (this._dQty > this.RmDQty)
                                {
                                    this._dQty = this.RmDQty;
                                }
                            }
                        }
                        else if (this.Status == Status.Close || this.PostedCount > 0)
                        {
                            this._dQty = 0;
                        }
                    }
                    else
                    {
                        if ((this.Status == Status.Open || this.Status == Status.Progress) && this.PostedCount <= 0)
                        {
                            if (this._dQty > 0)
                            {
                                this.MxDQty = this._dQty;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseReturnLine " + ex.ToString());
            }
        }

        //Done
        private void SetMxDUOM()
        {
            try
            {
                if (this.PurchaseReturn != null)
                {
                    ApprovalLine _locApprovalLineXPO = Session.FindObject<ApprovalLine>
                                                                   (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("PurchaseReturn", this.PurchaseReturn)));
                    if (_locApprovalLineXPO == null)
                    {
                        if ((this.Status == Status.Open || this.Status == Status.Progress)
                            && this.PostedCount <= 0)
                        {
                            this.MxDUOM = this.DUOM;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseReturnLine " + ex.ToString());
            }
        }

        //Done
        private void SetMxQty()
        {
            try
            {
                if (this.PurchaseReturn != null)
                {
                    XPCollection<ApprovalLine> _locApprovalLineXPOs = new XPCollection<ApprovalLine>
                                                                   (Session, new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("PurchaseReturn", this.PurchaseReturn)));
                    if (_locApprovalLineXPOs != null && _locApprovalLineXPOs.Count() > 0)
                    {
                        if (this.Status == Status.Posted || this.PostedCount > 0)
                        {
                            if (this.RmQty > 0)
                            {
                                if (this._qty > this.RmQty)
                                {
                                    this._qty = this.RmQty;
                                }
                            }
                        }
                        else if (this.Status == Status.Close || this.PostedCount > 0)
                        {
                            this._qty = 0;
                        }
                    }
                    else
                    {
                        if ((this.Status == Status.Open || this.Status == Status.Progress) && this.PostedCount <= 0)
                        {
                            if (this._qty > 0)
                            {
                                this.MxQty = this._qty;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseReturnLine " + ex.ToString());
            }
        }

        //Done
        private void SetMxUOM()
        {
            try
            {
                if (this.PurchaseReturn != null)
                {
                    ApprovalLine _locApprovalLineXPO = Session.FindObject<ApprovalLine>
                                                                   (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("PurchaseReturn", this.PurchaseReturn)));
                    if (_locApprovalLineXPO == null)
                    {
                        if ((this.Status == Status.Open || this.Status == Status.Progress)
                            && this.PostedCount <= 0)
                        {
                            this.MxUOM = this.UOM;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseReturnLine " + ex.ToString());
            }
        }

        //Done
        public void SetActivationPostingBasedActivationQuantity()
        {
            try
            {
                if (this.PurchaseReturn != null)
                {
                    if (this.ActivationQuantity == true)
                    {
                        this.ActivationPosting = true;
                        this.Save();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseReturnLine " + ex.ToString());
            }
        }

        #endregion SetMaxInQty

        #endregion Set

        #region Tax

        [Association("PurchaseReturnLine-TaxLines")]
        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        [Appearance("PurchaseReturnLineTaxLineClose", Criteria = "MultiTax = false", Visibility = ViewItemVisibility.Hide, Context = "DetailView")]
        [Appearance("PurchaseReturnLineTaxLineEnabled", Criteria = "MultiTax = true", Visibility = ViewItemVisibility.Show, Context = "DetailView")]
        public XPCollection<TaxLine> TaxLines
        {
            get { return GetCollection<TaxLine>("TaxLines"); }
        }

        //Done
        private void SetNormalTax()
        {
            try
            {
                if (_tax != null && _txValue > 0 && _txAmount > 0)
                {
                    this.Tax = this.Tax;
                    this.TxValue = this.TxValue;
                    this.TxAmount = this.TxAmount;
                }
                else
                {
                    this.TxValue = 0;
                    this.TxAmount = 0;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseReturnLine " + ex.ToString());
            }
        }

        //Done
        public Tax UpdateTaxLine(bool forceChangeEvents)
        {
            Tax _result = null;
            try
            {
                Tax _locTax = null;
                XPCollection<TaxLine> _locTaxLines = new XPCollection<TaxLine>(Session,
                                                     new GroupOperator(GroupOperatorType.And,
                                                     new BinaryOperator("PurchaseReturnLine", this)),
                                                     new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));

                if (_locTaxLines != null && _locTaxLines.Count > 0)
                {
                    if (_locTaxLines.Count == 1)
                    {
                        foreach (TaxLine _locTaxLinerLine in _locTaxLines)
                        {
                            if (forceChangeEvents)
                            {
                                //OnChanged("CurrencyLine", oldCurrencyLine, _locSalesOrderLine.Currency);
                                _result = _locTaxLinerLine.Tax;
                            }
                        }
                    }
                    else
                    {
                        foreach (TaxLine _locTaxLine in _locTaxLines)
                        {
                            if (_locTaxLine.No == 1)
                            {
                                _locTax = _locTaxLine.Tax;
                            }
                            else
                            {
                                if (_locTaxLine.Tax != _locTax)
                                {
                                    _globFunc = new GlobalFunction();
                                    //OnChanged("CurrencyLine", oldCurrencyLine, _globFunc.GetDefaultCurrency(Session));
                                    _result = _globFunc.GetDefaultTax(Session, TaxMode.Purchase);
                                    break;
                                }
                                else
                                {
                                    _result = _locTaxLine.Tax;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PurchaseReturnLine ", ex.ToString());
            }
            return _result;
        }

        #endregion Tax

        #region Discount

        [Association("PurchaseReturnLine-DiscountLines")]
        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        [Appearance("PurchaseReturnLineDiscountLineClose", Criteria = "MultiDiscount = false", Visibility = ViewItemVisibility.Hide, Context = "DetailView")]
        [Appearance("PurchaseReturnLineDiscountLineEnabled", Criteria = "MultiDiscount = true", Visibility = ViewItemVisibility.Show, Context = "DetailView")]
        public XPCollection<DiscountLine> DiscountLines
        {
            get { return GetCollection<DiscountLine>("DiscountLines"); }
        }

        //Done
        private void SetNormalDiscount()
        {
            try
            {
                if (_discount != null)
                {
                    this.Disc = this.Disc;
                    this.DiscAmount = this.DiscAmount;
                }
                else
                {
                    this.Disc = 0;
                    this.DiscAmount = 0;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseReturnLine " + ex.ToString());
            }
        }

        //Done
        public Discount UpdateDiscountLine(bool forceChangeEvents)
        {
            Discount _result = null;
            try
            {
                Discount _locDiscount = null;
                XPCollection<DiscountLine> _locDiscountLines = new XPCollection<DiscountLine>(Session,
                                                               new GroupOperator(GroupOperatorType.And,
                                                               new BinaryOperator("PurchaseReturnLine", this)),
                                                               new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));

                if (_locDiscountLines != null && _locDiscountLines.Count > 0)
                {
                    if (_locDiscountLines.Count == 1)
                    {
                        foreach (DiscountLine _locDiscountLinerLine in _locDiscountLines)
                        {
                            if (forceChangeEvents)
                            {
                                //OnChanged("CurrencyLine", oldCurrencyLine, _locSalesOrderLine.Currency);
                                _result = _locDiscountLinerLine.Discount;
                            }
                        }
                    }
                    else
                    {
                        foreach (DiscountLine _locDiscountLine in _locDiscountLines)
                        {
                            if (_locDiscountLine.No == 1)
                            {
                                _locDiscount = _locDiscountLine.Discount;
                            }
                            else
                            {
                                if (_locDiscountLine.Discount != _locDiscount)
                                {
                                    _globFunc = new GlobalFunction();
                                    //OnChanged("CurrencyLine", oldCurrencyLine, _globFunc.GetDefaultCurrency(Session));
                                    _result = _globFunc.GetDefaultDiscount(Session, DiscountMode.Purchase);
                                    break;
                                }
                                else
                                {
                                    _result = _locDiscountLine.Discount;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PurchaseReturnLine ", ex.ToString());
            }
            return _result;
        }

        #endregion Discount

    }
}