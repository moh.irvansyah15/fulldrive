﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [NavigationItem("Production")]
    [DefaultProperty("Code")]
    [RuleCombinationOfPropertiesIsUnique("CuttingProcessRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class CuttingProcess : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private bool _activationPosting;
        private string _code;
        private BusinessPartner _customer;
        private string _contact;
        private Country _country;
        private City _city;
        private string _address;
        private DirectionType _transferType;
        private InventoryMovingType _inventoryMovingType;
        private DocumentRule _documentRule;
        private ObjectList _objectList;
        XPCollection<DocumentType> _availableDocumentType;
        private DocumentType _documentType;
        private string _docNo;
        private Status _status;
        private DateTime _statusDate;
        private string _signCode;
        private SalesQuotation _salesQuotation;
        private Company _company;
        private string _userAccess;
        private GlobalFunction _globFunc;

        public CuttingProcess(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.CuttingProcess);
                DateTime now = DateTime.Now;
                this.Status = CustomProcess.Status.Open;
                this.StatusDate = now;
                this.ObjectList = ObjectList.CuttingProcess;
                this.TransferType = DirectionType.Internal;
                this.InventoryMovingType = InventoryMovingType.Cutting;
                this.DocumentRule = DocumentRule.Customer;

                #region UserAccess
                _userAccess = SecuritySystem.CurrentUserName;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                if (Session.IsNewObject(this))
                {
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null)
                            {
                                Company = _locUserAccess.Employee.Company;
                            }
                        }
                    }
                }
                #endregion UserAccess
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("CuttingProcessCodeEnabled", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [ImmediatePostData()]
        [Appearance("CuttingProcessCustomerClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BusinessPartner Customer
        {
            get { return _customer; }
            set
            {
                SetPropertyValue("Customer", ref _customer, value);
                if (this._customer != null)
                {
                    this.Contact = this._customer.Contact;
                    this.Country = this._customer.Country;
                    this.City = this._customer.City;
                    this.Address = this._customer.Address;
                }
            }
        }

        [Appearance("CuttingProcessContactClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Contact
        {
            get { return _contact; }
            set { SetPropertyValue("Contact", ref _contact, value); }
        }

        [Appearance("CuttingProcessCountryClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Country Country
        {
            get { return _country; }
            set { SetPropertyValue("Country", ref _country, value); }
        }

        [Appearance("CuttingProcessCityClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public City City
        {
            get { return _city; }
            set { SetPropertyValue("City", ref _city, value); }
        }

        [Size(512)]
        [Appearance("CuttingProcessAddressClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Address
        {
            get { return _address; }
            set { SetPropertyValue("Address", ref _address, value); }
        }

        [Appearance("CuttingProcessTransferTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public DirectionType TransferType
        {
            get { return _transferType; }
            set { SetPropertyValue("DirectionType", ref _transferType, value); }
        }

        [Appearance("CuttingProcessInventoryMovingTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public InventoryMovingType InventoryMovingType
        {
            get { return _inventoryMovingType; }
            set
            {
                SetPropertyValue("InventoryMovingType", ref _inventoryMovingType, value);
                if (!IsLoading)
                {
                    if (this._inventoryMovingType == InventoryMovingType.Deliver)
                    {
                        this.ObjectList = ObjectList.InventoryTransferOut;
                    }
                    else if (this._inventoryMovingType == InventoryMovingType.Receive)
                    {
                        this.ObjectList = ObjectList.InventoryTransferIn;
                    }
                }
            }
        }

        [ImmediatePostData()]
        [Appearance("CuttingProcessOrderDocumentRuleClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DocumentRule DocumentRule
        {
            get { return _documentRule; }
            set { SetPropertyValue("DocumentRule", ref _documentRule, value); }
        }

        [Browsable(false)]
        public ObjectList ObjectList
        {
            get { return _objectList; }
            set { SetPropertyValue("ObjectList", ref _objectList, value); }
        }

        [Browsable(false)]
        public XPCollection<DocumentType> AvailableDocumentType
        {
            get
            {
                if (ObjectList == ObjectList.None)
                {
                    _availableDocumentType = new XPCollection<DocumentType>(Session);
                }
                else
                {
                    _availableDocumentType = new XPCollection<DocumentType>(Session,
                                              new GroupOperator(GroupOperatorType.And,
                                              new BinaryOperator("ObjectList", this.ObjectList),
                                              new BinaryOperator("InventoryMovingType", this.InventoryMovingType),
                                              new BinaryOperator("TransferType", this.TransferType),
                                              new BinaryOperator("DocumentRule", this.DocumentRule),
                                              new BinaryOperator("Active", true)));
                }
                return _availableDocumentType;

            }
        }

        [Appearance("CuttingProcessDocumentTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceProperty("AvailableDocumentType", DataSourcePropertyIsNullMode.SelectAll)]
        public DocumentType DocumentType
        {
            get { return _documentType; }
            set
            {
                SetPropertyValue("DocumentType", ref _documentType, value);
                if (!IsLoading)
                {
                    if (_documentType != null)
                    {
                        _globFunc = new GlobalFunction();
                        this.DocNo = _globFunc.GetDocumentNumberingUnlockOptimisticRecord(this.Session.DataLayer, this._documentType);
                    }
                }
            }
        }

        [Appearance("CuttingProcessDocNoClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string DocNo
        {
            get { return _docNo; }
            set { SetPropertyValue("DocNo", ref _docNo, value); }
        }

        [Appearance("CuttingProcessStatusEnabled", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("CuttingProcessStatusDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("CuttingProcessSignCodeClose", Enabled = false)]
        public string SignCode
        {
            get { return _signCode; }
            set { SetPropertyValue("SignCode", ref _signCode, value); }
        }

        [Appearance("CuttingProcessSalesQuotationClose", Enabled = false)]
        public SalesQuotation SalesQuotation
        {
            get { return _salesQuotation; }
            set { SetPropertyValue("SalesQuotation", ref _salesQuotation, value); }
        }

        [Browsable(false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Association("CuttingProcess-CuttingProcessLines")]
        public XPCollection<CuttingProcessLine> CuttingProcessLines
        {
            get { return GetCollection<CuttingProcessLine>("CuttingProcessLines"); }
        }

        #endregion Field

    }
}