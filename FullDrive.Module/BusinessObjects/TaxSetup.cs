﻿
using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using DevExpress.ExpressApp.Xpo;
using DevExpress.ExpressApp.ConditionalAppearance;
using System.Collections;
using FullDrive.Module.CustomProcess;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    [NavigationItem("Tax")]
    [RuleCombinationOfPropertiesIsUnique("TaxSetupRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class TaxSetup : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private string _code;
        private string _name;
        private double _nonTaxableIncomeCHildren;
        private double _maximumDependant;
        private double _nonTaxableIncomeSpouse;
        private double _nonTaxableIncomeEmployee;
        private double _maximumAmmount;
        private double _ocupationFee;
        private bool _active;
        private GlobalFunction _globFunc;
        public TaxSetup(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                this._globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.TaxSetup);
                //this._nonTaxableIncomeEmployee = 54000000;
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        public double OccupationFee
        {
            get { return _ocupationFee; }
            set { SetPropertyValue("OccupationFee", ref _ocupationFee, value); }
        }

        public double MaximumAmmount
        {
            get { return _maximumAmmount; }
            set { SetPropertyValue("MaximumAmmount", ref _maximumAmmount, value); }
        }

        [ImmediatePostData()]
        public double NonTaxableIncomeEmployee
        {
            get { return _nonTaxableIncomeEmployee; }
            set {
                    SetPropertyValue("NonTaxableIncomeEmployee", ref _nonTaxableIncomeEmployee, value);
                  //  if (!IsLoading)
                  //  {
                  //  this._nonTaxableIncomeSpouse = this.NonTaxableIncomeEmployee.Spouse;
                  //  this._nonTaxableIncomeCHildren = this.NonTaxableIncomeEmployee.Children;
                  //}
                }
        }

        [ImmediatePostData()]
        public double NonTaxableIncomeSpouse
        {
            get { return _nonTaxableIncomeSpouse; }
            set
            {
                SetPropertyValue("NonTaxableIncomeSpouse", ref _nonTaxableIncomeSpouse, value);
                //SetTotalTaxSpouse();
            }
        }

        [ImmediatePostData()]
        public double NonTaxableIncomeChildren
        {
            get { return _nonTaxableIncomeCHildren; }
            set {
                    SetPropertyValue("NonTaxableIncomeChildren", ref _nonTaxableIncomeCHildren, value);
                    //SetTotalTaxChildren();
                }
        }

        [ImmediatePostData()]
        public double MaximumDependant
        {
            get { return _maximumDependant; }
            set {
                    SetPropertyValue("MaximumDependant", ref _maximumDependant, value);
                    //SetTotalTaxChildren();
                }
        }
        
        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue("Active", ref _active, value); }
        }

        [Association("TaxSetup-TaxRateNPWPs")]
        public XPCollection<TaxRateNPWP> TaxRateNPWPs
        {
            get { return GetCollection<TaxRateNPWP>("TaxRateNPWPs"); }
        }

        [Association("TaxSetup-TaxRateNonNPWPs")]
        public XPCollection<TaxRateNonNPWP> TaxRateNonNPWPs
        {
            get { return GetCollection<TaxRateNonNPWP>("TaxRateNonNPWPs"); }
        }

        //[Association("NonTaxableIncome-TaxSetup")]
        //private string _PersistentProperty;
        //[XafDisplayName("My display name"), ToolTip("My hint message")]
        //[ModelDefault("EditMask", "(000)-00"), Index(0), VisibleInListView(false)]
        //[Persistent("DatabaseColumnName"), RuleRequiredField(DefaultContexts.Save)]
        //public string PersistentProperty {
        //    get { return _PersistentProperty; }
        //    set { SetPropertyValue("PersistentProperty", ref _PersistentProperty, value); }
        //}

        //[Action(Caption = "My UI Action", ConfirmationMessage = "Are you sure?", ImageName = "Attention", AutoCommit = true)]
        //public void ActionMethod() {
        //    // Trigger a custom business logic for the current record in the UI (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112619.aspx).
        //    this.PersistentProperty = "Paid";
        //}

        //public void SetTotalTaxChildren()
        //{
        //    if (!IsLoading)
        //    {
        //        double _ptkpChildren = 0;
        //        if (this._nonTaxableIncomeCHildren <= this._maximumDependant)
        //        {
        //            _ptkpChildren = this._nonTaxableIncomeCHildren * 4500000;
        //            this._nonTaxableIncomeEmployee = this._nonTaxableIncomeEmployee + _ptkpChildren;
        //            this._maximumAmmount = this.NonTaxableIncomeEmployee;
        //        }
        //        else if (this._nonTaxableIncomeCHildren > this._maximumDependant)
        //        {
        //            _ptkpChildren = this._maximumDependant * 4500000;
        //            this._nonTaxableIncomeEmployee = this._nonTaxableIncomeEmployee + _ptkpChildren;
        //            this._maximumAmmount = this.NonTaxableIncomeEmployee;
        //        }
        //    }
        //}

        //    public void SetTotalTaxSpouse()
        //{
        //    if (!IsLoading)
        //    {
                
        //        double _ptkpspouse = 0;
        //        if (_nonTaxableIncomeSpouse > 0)
        //        {
        //            _ptkpspouse = this._nonTaxableIncomeSpouse * 4500000;
        //            this._nonTaxableIncomeEmployee = _nonTaxableIncomeEmployee + _ptkpspouse;
        //            this._maximumAmmount = this.NonTaxableIncomeEmployee;
        //        }
        //    }
        //}
    }
}