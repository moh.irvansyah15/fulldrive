﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using DevExpress.ExpressApp.ConditionalAppearance;
using FullDrive.Module.CustomProcess;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [NavigationItem("Rate")]
    [DefaultProperty("Code")]
    [RuleCombinationOfPropertiesIsUnique("RateRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class Rate : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).

        private string _code;
        private ShipmentType _shipmentType;
        private PriceType _priceType;
        private TransportType _transportType;
        private Item _item;
        private DeliveryType _deliveryType;
        private XPCollection<ContainerType> _availableContainerType;
        private TransactionTerm _transactionTerm;
        private ContainerType _containerType;
        private PointType _pointType;
        private Country _countryFrom;
        private City _cityFrom;
        private Country _countryTo;
        private City _cityTo;
        private Area _origin;
        private Location _locationFrom;
        private TransportLocation _transportLocationFrom;
        private Area _destination1;
        private Area _destination2;
        private Area _destination3;
        private Area _destination4;
        private Area _destination;
        private Location _locationTo;
        private TransportLocation _transportLocationTo;
        private double _quantity;
        private UnitOfMeasure _uom;
        //private Volume _truckCapacity;
        private double _price;
        private BuyingRate _buyingRate;
        private SellingRate _sellingRate;
        private bool _active;
        private GlobalFunction _globFunc;

        public Rate(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.Rate);
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [Appearance("RateCodeEnabled", Enabled = false)]
        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        public ShipmentType ShipmentType
        {
            get { return _shipmentType; }
            set { SetPropertyValue("ShipmentType", ref _shipmentType, value); }
        }

        public PriceType PriceType
        {
            get { return _priceType; }
            set { SetPropertyValue("PriceType", ref _priceType, value); }
        }

        [ImmediatePostData()]
        public TransportType TransportType
        {
            get { return _transportType; }
            set { SetPropertyValue("TransportType", ref _transportType, value); }
        }

        public Item Item
        {
            get { return _item; }
            set { SetPropertyValue("Item", ref _item, value); }
        }

        [DataSourceCriteria("Active = true")]
        public DeliveryType DeliveryType
        {
            get { return _deliveryType; }
            set { SetPropertyValue("DeliveryType", ref _deliveryType, value); }
        }

        public TransactionTerm TransactionTerm
        {
            get { return _transactionTerm; }
            set { SetPropertyValue("TransactionTerm", ref _transactionTerm, value); }
        }

        [Browsable(false)]
        public XPCollection<ContainerType> AvailableContainerType
        {
            get
            {
                if (_transportType == null)
                {
                    _availableContainerType = new XPCollection<ContainerType>(Session);
                }
                else
                {
                    _availableContainerType = new XPCollection<ContainerType>(Session,
                                                new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("TransportType", _transportType, BinaryOperatorType.Equal),
                                                    new BinaryOperator("Active", true)));
                }
                return _availableContainerType;
            }
        }

        [DataSourceProperty("AvailableContainerType", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        public ContainerType ContainerType
        {
            get { return _containerType; }
            set { SetPropertyValue("ContainerType", ref _containerType, value); }
        }

        public PointType PointType
        {
            get { return _pointType; }
            set { SetPropertyValue("PointType", ref _pointType, value); }
        }

        [Persistent]
        public string FullName
        {
            get
            {
                string _locCountryFrom = "";
                string _locCityFrom = "";
                string _locLocationFrom = "";
                string _locTransportLocationFrom = "";
                string _locCountryTo = "";
                string _locCityTo = "";
                string _locLocationTo = "";
                string _locTransportLocationTo = "";
                string _origin = "";
                string _destination = "";
                string _destination1 = "";
                string _destination2 = "";
                string _destination3 = "";
                string _destination4 = "";
                if (this.Origin != null) { _origin = this.Origin.Name; }
                if (this.Destination != null) { _destination = this.Destination.Name; }
                if (this.Destination1 != null) { _destination1 = this.Destination1.Name; }
                if (this.Destination2 != null) { _destination2 = this.Destination2.Name; }
                if (this.Destination3 != null) { _destination3 = this.Destination3.Name; }
                if (this.Destination4 != null) { _destination4 = this.Destination4.Name; }
                if (this.CountryFrom != null) { _locCountryFrom = this.CountryFrom.Name; }
                if (this.CityFrom != null) { _locCityFrom = this.CityFrom.Name; }
                if (this.LocationFrom != null) { _locLocationFrom = this.LocationFrom.Name; }
                if (this.TransportLocationFrom != null) { _locTransportLocationFrom = this.TransportLocationFrom.Name; }
                if (this.CountryTo != null) { _locCountryTo = this.CountryTo.Name; }
                if (this.CityTo != null) { _locCityTo = this.CityTo.Name; }
                if (this.LocationTo != null) { _locLocationTo = this.LocationTo.Name; }
                if (this.TransportLocationTo != null) { _locTransportLocationTo = this.TransportLocationTo.Name; }
                return String.Format("{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}{10}{11}{12}{13}", _locCountryFrom, _locCityFrom, _origin, _locLocationFrom, TransportLocationFrom,
                                    CountryTo, CityTo, _destination, LocationTo, _destination1, _destination2, _destination3, _destination4,  TransportLocationTo).Trim().Replace(" ", "").ToLower();
            }
        }

        [DataSourceCriteria("Active = true")]
        public Country CountryFrom
        {
            get { return _countryFrom; }
            set { SetPropertyValue("CountryFrom", ref _countryFrom, value); }
        }

        [DataSourceCriteria("Active = true")]
        public City CityFrom
        {
            get { return _cityFrom; }
            set { SetPropertyValue("CityFrom", ref _cityFrom, value); }
        }

        [DataSourceCriteria("Active = true")]
        public Country CountryTo
        {
            get { return _countryTo; }
            set { SetPropertyValue("CountryTo", ref _countryTo, value); }
        }

        [DataSourceCriteria("Active = true")]
        public City CityTo
        {
            get { return _cityTo; }
            set { SetPropertyValue("CityTo", ref _cityTo, value); }
        }

        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        public Area Origin
        {
            get { return _origin; }
            set { SetPropertyValue("Origin", ref _origin, value); }
        }

        [DataSourceCriteria("Active = true")]
        public Location LocationFrom
        {
            get { return _locationFrom; }
            set { SetPropertyValue("LocationFrom", ref _locationFrom, value); }
        }

        [DataSourceCriteria("Active = true")]
        public TransportLocation TransportLocationFrom
        {
            get { return _transportLocationFrom; }
            set { SetPropertyValue("TransportLocationFrom", ref _transportLocationFrom, value); }
        }

        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        public Area Destination1
        {
            get { return _destination1; }
            set { SetPropertyValue("Destination1", ref _destination1, value); }
        }

        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        public Area Destination2
        {
            get { return _destination2; }
            set { SetPropertyValue("Destination2", ref _destination2, value); }
        }

        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        public Area Destination3
        {
            get { return _destination3; }
            set { SetPropertyValue("Destination3", ref _destination3, value); }
        }

        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        public Area Destination4
        {
            get { return _destination4; }
            set { SetPropertyValue("Destination4", ref _destination4, value); }
        }

        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        public Area Destination
        {
            get { return _destination; }
            set { SetPropertyValue("Destination", ref _destination, value); }
        }

        [DataSourceCriteria("Active = true")]
        public Location LocationTo
        {
            get { return _locationTo; }
            set { SetPropertyValue("LocationTo", ref _locationTo, value); }
        }

        [DataSourceCriteria("Active = true")]
        public TransportLocation TransportLocationTo
        {
            get { return _transportLocationTo; }
            set { SetPropertyValue("TransportLocationTo", ref _transportLocationTo, value); }
        }

        public double Quantity
        {
            get { return _quantity; }
            set { SetPropertyValue("Quantity", ref _quantity, value); }
        }

        public UnitOfMeasure UOM
        {
            get { return _uom; }
            set { SetPropertyValue("UOM", ref _uom, value); }
        }

        public double Price
        {
            get { return _price; }
            set { SetPropertyValue("Price", ref _price, value); }
        }

        [Appearance("RateBuyingRateEnabled", Enabled = false)]
        [Association("BuyingRate-Rates")]
        public BuyingRate BuyingRate
        {
            get { return _buyingRate; }
            set { SetPropertyValue("BuyingRate", ref _buyingRate, value); }
        }

        [Appearance("RateSellingRateEnabled", Enabled = false)]
        [Association("SellingRate-Rates")]
        public SellingRate SellingRate
        {
            get { return _sellingRate; }
            set { SetPropertyValue("SellingRate", ref _sellingRate, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue("Active", ref _active, value); }
        }

        #endregion Field
    }
}