﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("ShipmentPurchaseCollectionRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class ShipmentPurchaseCollection : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).

        private bool _activationPosting;
        private bool _select;
        private string _code;
        private ShipmentType _shipmentType;
        private XPCollection<ShipmentPurchaseMonitoring> _availableShipmentPurchaseMonitoring;
        private ShipmentPurchaseMonitoring _shipmentPurchaseMonitoring;
        private ShipmentPurchaseInvoice _shipmentPurchaseInvoice;
        private Status _status;
        private DateTime _statusDate;
        private int _postedCount;
        private GlobalFunction _globFunc;

        public ShipmentPurchaseCollection(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            DateTime now = DateTime.Now;
            _globFunc = new GlobalFunction();
            this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.ShipmentPurchaseCollection);
            this.Status = Status.Open;
            this.StatusDate = now;
            this.Select = true;
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [Appearance("ShipmentPurchaseCollectionSelectClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public bool Select
        {
            get { return _select; }
            set { SetPropertyValue("Select", ref _select, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("ShipmentPurchaseCollectionCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [ImmediatePostData()]
        public ShipmentType ShipmentType
        {
            get { return _shipmentType; }
            set { SetPropertyValue("ShipmentType", ref _shipmentType, value); }
        }

        [Browsable(false)]
        public XPCollection<ShipmentPurchaseMonitoring> AvailableShipmentPurchaseMonitoring
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;

                if (!IsLoading)
                {
                    if (this.ShipmentPurchaseInvoice != null )
                    {
                        if (this.ShipmentPurchaseInvoice.ShipmentType != ShipmentType.None && this.ShipmentPurchaseInvoice.BuyFromBusinessPartner != null)
                        {
                            XPQuery<ShipmentPurchaseMonitoring> _shipmentPurchaseMonitoringsQuery = new XPQuery<ShipmentPurchaseMonitoring>(Session);

                            var _shipmentPurchaseMonitorings = from spm in _shipmentPurchaseMonitoringsQuery
                                                         where (spm.Status != Status.Close 
                                                         && spm.ShipmentType == this.ShipmentPurchaseInvoice.ShipmentType
                                                         && spm.Carrier == this.ShipmentPurchaseInvoice.BuyFromBusinessPartner
                                                         && spm.ShipmentPurchaseInvoice == null)
                                                         group spm by spm.Code into g
                                                         select new { Code = g.Key };

                            if (_shipmentPurchaseMonitorings != null && _shipmentPurchaseMonitorings.Count() > 0)
                            {
                                List<string> _stringSPM = new List<string>();

                                foreach (var _shipmentPurchaseMonitoring in _shipmentPurchaseMonitorings)
                                {
                                    if (_shipmentPurchaseMonitoring != null)
                                    {
                                        if (_shipmentPurchaseMonitoring.Code != null)
                                        {
                                            _stringSPM.Add(_shipmentPurchaseMonitoring.Code);
                                        }
                                    }
                                }

                                IEnumerable<string> _stringArraySPMDistinct = _stringSPM.Distinct();
                                string[] _stringArraySPMList = _stringArraySPMDistinct.ToArray();
                                if (_stringArraySPMList.Length == 1)
                                {
                                    for (int i = 0; i < _stringArraySPMList.Length; i++)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _stringArraySPMList[i] + "'";
                                        }
                                    }
                                }
                                else if (_stringArraySPMList.Length > 1)
                                {
                                    for (int i = 0; i < _stringArraySPMList.Length; i++)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _stringArraySPMList[i] + "'";
                                        }
                                        else
                                        {
                                            _endString = _endString + " OR [Code]=='" + _stringArraySPMList[i] + "'";
                                        }
                                    }
                                }
                                _fullString = _beginString + _endString;

                                if (_fullString != null)
                                {
                                    _availableShipmentPurchaseMonitoring = new XPCollection<ShipmentPurchaseMonitoring>(Session, CriteriaOperator.Parse(_fullString));
                                }
                            }
                        }
                    }
                }

                return _availableShipmentPurchaseMonitoring;
            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableShipmentPurchaseMonitoring", DataSourcePropertyIsNullMode.SelectNothing)]
        public ShipmentPurchaseMonitoring ShipmentPurchaseMonitoring
        {
            get { return _shipmentPurchaseMonitoring; }
            set { SetPropertyValue("ShipmentPurchaseMonitoring", ref _shipmentPurchaseMonitoring, value); }
        }

        [Appearance("ShipmentPurchaseCollectionStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("ShipmentPurchaseCollectionStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("ShipmentPurchaseCollectionShipmentPurchaseInvoiceClose", Enabled = false)]
        [Association("ShipmentPurchaseInvoice-ShipmentPurchaseCollections")]
        public ShipmentPurchaseInvoice ShipmentPurchaseInvoice
        {
            get { return _shipmentPurchaseInvoice; }
            set { SetPropertyValue("ShipmentPurchaseInvoice", ref _shipmentPurchaseInvoice, value); }
        }

        #endregion Field
    }
}