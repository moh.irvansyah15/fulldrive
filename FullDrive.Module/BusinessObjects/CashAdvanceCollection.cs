﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("CashAdvanceCollectionRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class CashAdvanceCollection : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private bool _activationPosting;
        private string _code;
        private XPCollection<CashAdvance> _availableCashAdvance;
        private CashAdvance _cashAdvance;
        private PaymentRealization _paymentRealization;
        private Status _status;
        private DateTime _statusDate;
        private int _postedCount;
        private GlobalFunction _globFunc;

        public CashAdvanceCollection(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.CashAdvanceCollection);
                DateTime now = DateTime.Now;
                this.Status = Status.Open;
                this.StatusDate = now;
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("CashAdvanceCollectionCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Browsable(false)]
        public XPCollection<CashAdvance> AvailableCashAdvance
        {
            get
            {
                if (!IsLoading)
                {
                    if (this.PaymentRealization != null)
                    {
                        if (this.PaymentRealization.Employee != null)
                        {
                            XPCollection<CashAdvance> _locCollectionCAs = new XPCollection<CashAdvance>
                                                                        (Session, new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("ActiveApproved3", true),
                                                                        new BinaryOperator("Employee", this.PaymentRealization.Employee),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("Status", Status.Progress),
                                                                        new BinaryOperator("Status", Status.Posted)))
                                                                        );

                            if (_locCollectionCAs != null && _locCollectionCAs.Count() > 0)
                            {
                                _availableCashAdvance = _locCollectionCAs;
                            }
                        }    
                    }
                }
                return _availableCashAdvance;
            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableCashAdvance", DataSourcePropertyIsNullMode.SelectNothing)]
        public CashAdvance CashAdvance
        {
            get { return _cashAdvance; }
            set { SetPropertyValue("CashAdvance", ref _cashAdvance, value); }
        }

        [ImmediatePostData()]
        [Appearance("CashAdvanceCollectionPaymentRealizationClose", Enabled = false)]
        [Association("PaymentRealization-CashAdvanceCollections")]
        public PaymentRealization PaymentRealization
        {
            get { return _paymentRealization; }
            set { SetPropertyValue("PaymentRealization", ref _paymentRealization, value); }
        }

        [Appearance("CashAdvanceCollectionStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("CashAdvanceCollectionStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("CashAdvanceCollectionPostedCountEnabled", Enabled = false)]
        public int PostedCount
        {
            get { return _postedCount; }
            set { SetPropertyValue("PostedCount", ref _postedCount, value); }
        }
        #endregion Field
    }
}