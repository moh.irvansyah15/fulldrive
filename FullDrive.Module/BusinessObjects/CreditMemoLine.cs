﻿#region Default

using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

#endregion Default

using DevExpress.ExpressApp.Editors;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    #region Default

    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("CreditMemoLineRuleUnique", DefaultContexts.Save, "Code")]

    #endregion Default

    // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
    public class CreditMemoLine : FullDriveSysBaseObject
    {
        #region Default

        private bool _activationPosting;
        private bool _selectEnabled;
        private int _no;
        private string _code;
        private bool _select;
        private OrderType _orderType;
        private XPCollection<Item> _availableItem;
        private Item _item;
        private string _name;
        private string _description;
        private XPCollection<UnitOfMeasure> _availableUnitOfMeasure;
        #region InisialisasiMaxQty
        private double _mxDQTY;
        private UnitOfMeasure _mxDUom;
        private double _mxQty;
        private UnitOfMeasure _mxUom;
        private double _mxTQty;
        private double _mxUAmount;
        private double _mxTUAmount;
        #endregion InisialisasiMaxQty
        #region InisialisasiDefaultQty
        private double _dQty;
        private UnitOfMeasure _dUom;
        private double _qty;
        private UnitOfMeasure _uom;
        private double _tQty;
        #endregion InisialisasiDefaultQty
        #region InisialisasiRemainQty
        private double _rmDQty;
        private double _rmQty;
        private double _rmTQty;
        #endregion InisialisasiRemainQty
        #region InitialPostingQuantityTo
        private double _pDQty;
        private UnitOfMeasure _pDUom;
        private double _pQty;
        private UnitOfMeasure _pUom;
        private double _pTQty;
        #endregion InitialPostingQuantityTo
        #region InisialisasiAmount
        private Currency _currency;
        private double _uAmount;
        private double _tUAmount;
        private bool _multiTax;
        private Tax _tax;
        private double _txValue;
        private double _txAmount;
        private bool _multiDiscount;
        private Discount _discount;
        private double _disc;
        private double _discAmount;
        private double _tAmount;
        #endregion InisialisasiAmount
        private DateTime _estimatedDate;
        private ReturnType _returnType;
        private Status _status;
        private DateTime _statusDate;
        private int _postedCount;
        private string _signCode;
        private Company _company;
        private CreditMemo _creditMemo;
        private PurchaseReturnMonitoring _purchaseReturnMonitoring;
        private GlobalFunction _globFunc;
        //clm
        private bool _hideSumTotalItem;

        public CreditMemoLine(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.CreditMemoLine);
                this.Status = Status.Open;
                this.StatusDate = now;
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #endregion Default

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        public bool SelectEnabled
        {
            get { return _selectEnabled; }
            set { SetPropertyValue("SelectEnabled", ref _selectEnabled, value); }
        }

        [Appearance("CreditMemoLineNoEnabled", Enabled = false)]
        public int No
        {
            get { return _no; }
            set { SetPropertyValue("No", ref _no, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("CreditMemoLineCodeEnabled", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("DebitMemoLineSelectClose", Criteria = "SelectEnabled = true", Enabled = false)]
        public bool Select
        {
            get { return _select; }
            set { SetPropertyValue("Select", ref _select, value); }
        }

        [ImmediatePostData()]
        [Appearance("CreditMemoLineOrderTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public OrderType OrderType
        {
            get { return _orderType; }
            set { SetPropertyValue("OrderType", ref _orderType, value); }
        }

        [Browsable(false)]
        public XPCollection<Item> AvailableItem
        {
            get
            {
                if (OrderType == OrderType.Item)
                {
                    _availableItem = new XPCollection<Item>(Session,
                                    new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("OrderType", OrderType.Item)));

                }
                else if (OrderType == OrderType.Service)
                {
                    _availableItem = new XPCollection<Item>(Session,
                                    new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("OrderType", OrderType.Service)));
                }
                else
                {
                    _availableItem = new XPCollection<Item>(Session);
                }

                return _availableItem;

            }
        }

        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        [DataSourceProperty("AvailableItem", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("CreditMemoLineItemClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Item Item
        {
            get { return _item; }
            set
            {
                SetPropertyValue("Item", ref _item, value);
                if (!IsLoading)
                {
                    if (this._item != null)
                    {
                        this.DUOM = this._item.BasedUOM;
                        this.Description = GetDescriptions();
                    }
                }
            }
        }

        [Appearance("CreditMemoLineNameClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        [Appearance("CreditMemoLineDescriptionClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Size(512)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        [Browsable(false)]
        public XPCollection<UnitOfMeasure> AvailableUnitOfMeasure
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;
                if (Item == null)
                {
                    _availableUnitOfMeasure = new XPCollection<UnitOfMeasure>(Session);
                }
                else
                {
                    List<string> _stringUOM = new List<string>();

                    XPCollection<ItemUnitOfMeasure> _locItemUnitOfMeasures = new XPCollection<ItemUnitOfMeasure>
                                                                              (Session, new GroupOperator(GroupOperatorType.And,
                                                                               new BinaryOperator("Item", this.Item),
                                                                               new BinaryOperator("Active", true)));

                    if (_locItemUnitOfMeasures != null && _locItemUnitOfMeasures.Count() > 0)
                    {
                        foreach (ItemUnitOfMeasure _locItemUnitOfMeasure in _locItemUnitOfMeasures)
                        {
                            _stringUOM.Add(_locItemUnitOfMeasure.UOM.Code);
                        }
                    }

                    IEnumerable<string> _stringArrayUOMDistinct = _stringUOM.Distinct();
                    string[] _stringArrayUOMList = _stringArrayUOMDistinct.ToArray();
                    if (_stringArrayUOMList.Length == 1)
                    {
                        for (int i = 0; i < _stringArrayUOMList.Length; i++)
                        {
                            UnitOfMeasure _locUOM = Session.FindObject<UnitOfMeasure>(new BinaryOperator("Code", _stringArrayUOMList[i]));
                            if (_locUOM != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locUOM.Code + "'";
                                }
                            }
                        }
                    }
                    else if (_stringArrayUOMList.Length > 1)
                    {
                        for (int i = 0; i < _stringArrayUOMList.Length; i++)
                        {
                            UnitOfMeasure _locUOM = Session.FindObject<UnitOfMeasure>(new BinaryOperator("Code", _stringArrayUOMList[i]));
                            if (_locUOM != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locUOM.Code + "'";
                                }
                                else
                                {
                                    _endString = _endString + " OR [Code]=='" + _locUOM.Code + "'";
                                }
                            }
                        }
                    }
                    _fullString = _beginString + _endString;

                    if (_fullString != null)
                    {
                        _availableUnitOfMeasure = new XPCollection<UnitOfMeasure>(Session, CriteriaOperator.Parse(_fullString));
                    }

                }
                return _availableUnitOfMeasure;

            }
        }

        #region MaxDefaultQty

        [Appearance("CreditMemoLineMxDQtyClose", Enabled = false)]
        [ImmediatePostData()]
        public double MxDQty
        {
            get { return _mxDQTY; }
            set
            {
                SetPropertyValue("MxDQty", ref _mxDQTY, value);
                if (!IsLoading)
                {
                    SetMaxTotalQty();
                }
            }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        [Appearance("CreditMemoLineMxDUOMClose", Enabled = false)]
        public UnitOfMeasure MxDUOM
        {
            get { return _mxDUom; }
            set
            {
                SetPropertyValue("MxDUOM", ref _mxDUom, value);
                if (!IsLoading)
                {
                    SetMaxTotalQty();
                }
            }
        }

        [ImmediatePostData()]
        [Appearance("CreditMemoLineMxQtyClose", Enabled = false)]
        public double MxQty
        {
            get { return _mxQty; }
            set
            {
                SetPropertyValue("MxQty", ref _mxQty, value);
                if (!IsLoading)
                {
                    SetMaxTotalQty();
                }
            }
        }

        [DataSourceProperty("AvailableUnitOfMeasure", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        [Appearance("CreditMemoLineMxUOMClose", Enabled = false)]
        public UnitOfMeasure MxUOM
        {
            get { return _mxUom; }
            set
            {
                SetPropertyValue("MxUOM", ref _mxUom, value);
                if (!IsLoading)
                {
                    SetMaxTotalQty();
                }
            }
        }

        [Appearance("CreditMemoLineMxTQtyClose", Enabled = false)]
        public double MxTQty
        {
            get { return _mxTQty; }
            set { SetPropertyValue("MxTQty", ref _mxTQty, value); }
        }

        [Appearance("CreditMemoLineMxUAmountClose", Enabled = false)]
        public double MxUAmount
        {
            get { return _mxUAmount; }
            set { SetPropertyValue("MxUAmount", ref _mxUAmount, value); }
        }

        [Appearance("CreditMemoLineMxTUAmountClose", Enabled = false)]
        public double MxTUAmount
        {
            get { return _mxTUAmount; }
            set { SetPropertyValue("MxTUAmount", ref _mxTUAmount, value); }
        }

        #endregion MaxDefaultQty

        #region DefaultQty

        [Appearance("CreditMemoLineDQtyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double DQty
        {
            get { return _dQty; }
            set
            {
                SetPropertyValue("DQty", ref _dQty, value);
                if (!IsLoading)
                {
                    SetMxDQty();
                    SetTotalQty();
                    SetTotalUnitAmount();
                    SetTaxAmount();
                    SetDiscAmount();
                    SetTotalAmount();
                }
            }
        }

        [Appearance("CreditMemoLineDUOMClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure DUOM
        {
            get { return _dUom; }
            set
            {
                SetPropertyValue("DUOM", ref _dUom, value);
                if (!IsLoading)
                {
                    SetMxDUOM();
                    SetTotalQty();
                    SetTotalUnitAmount();
                    SetTaxAmount();
                    SetDiscAmount();
                    SetTotalAmount();
                }
            }
        }

        [Appearance("CreditMemoLineQtyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double Qty
        {
            get { return _qty; }
            set
            {
                SetPropertyValue("Qty", ref _qty, value);
                if (!IsLoading)
                {
                    SetMxQty();
                    SetTotalQty();
                    SetTotalUnitAmount();
                    SetTaxAmount();
                    SetDiscAmount();
                    SetTotalAmount();
                }
            }
        }

        [Appearance("CreditMemoLineUOMClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceProperty("AvailableUnitOfMeasure", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure UOM
        {
            get { return _uom; }
            set
            {
                SetPropertyValue("UOM", ref _uom, value);
                if (!IsLoading)
                {
                    SetMxUOM();
                    SetTotalQty();
                    SetTotalUnitAmount();
                    SetTaxAmount();
                    SetDiscAmount();
                    SetTotalAmount();
                }
            }
        }

        [Appearance("CreditMemoLineTotalQtyEnabled", Enabled = false)]
        public double TQty
        {
            get { return _tQty; }
            set { SetPropertyValue("TotalQty", ref _tQty, value); }
        }

        #endregion DefaultQty

        #region RemainQty

        [Appearance("CreditMemoLineRmDQtyClose", Enabled = false)]
        public double RmDQty
        {
            get { return _rmDQty; }
            set { SetPropertyValue("RmDQty", ref _rmDQty, value); }
        }

        [Appearance("CreditMemoLineRmQtyClose", Enabled = false)]
        public double RmQty
        {
            get { return _rmQty; }
            set { SetPropertyValue("RmQty", ref _rmQty, value); }
        }

        [Appearance("CreditMemoLineRmTQtyClose", Enabled = false)]
        public double RmTQty
        {
            get { return _rmTQty; }
            set { SetPropertyValue("RmTQty", ref _rmTQty, value); }
        }

        #endregion RemainQty

        #region PostingQty

        [Appearance("CreditMemoLinePDQtyClose", Enabled = false)]
        public double PDQty
        {
            get { return _pDQty; }
            set
            {
                SetPropertyValue("PDQty", ref _pDQty, value);
                if (!IsLoading)
                {
                    SetTotalPQty();
                }
            }
        }

        [Appearance("CreditMemoLinePDUOMClose", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure PDUOM
        {
            get { return _pDUom; }
            set
            {
                SetPropertyValue("PDUOM", ref _pDUom, value);
                if (!IsLoading)
                {
                    SetTotalPQty();
                }
            }
        }

        [Appearance("CreditMemoLinePQtyToClose", Enabled = false)]
        public double PQty
        {
            get { return _pQty; }
            set
            {
                SetPropertyValue("PQty", ref _pQty, value);
                if (!IsLoading)
                {
                    SetTotalPQty();
                }
            }
        }

        [Appearance("CreditMemoLinePUOMClose", Enabled = false)]
        [DataSourceProperty("AvailableUnitOfMeasure", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure PUOM
        {
            get { return _pUom; }
            set
            {
                SetPropertyValue("PUOM", ref _pUom, value);
                if (!IsLoading)
                {
                    SetTotalPQty();
                }
            }
        }

        [Appearance("CreditMemoLinePTQtyEnabled", Enabled = false)]
        public double PTQty
        {
            get { return _pTQty; }
            set { SetPropertyValue("PTQty", ref _pTQty, value); }
        }

        #endregion PostingQty

        #region Amount
        [Appearance("CreditMemoLineCurrencyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Currency Currency
        {
            get { return _currency; }
            set { SetPropertyValue("Currency", ref _currency, value); }
        }

        [Appearance("CreditMemoLineUAmountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double UAmount
        {
            get { return _uAmount; }
            set
            {
                SetPropertyValue("UAmount", ref _uAmount, value);
                if (!IsLoading)
                {
                    _globFunc = new GlobalFunction();
                    if (this._uAmount > 0)
                    {
                        if (_globFunc.GetRoundingList(Session, ObjectList.CreditMemoLine, FieldName.UAmount) == true)
                        {
                            this._uAmount = _globFunc.GetRoundUp(Session, this._uAmount, ObjectList.CreditMemoLine, FieldName.UAmount);
                        }
                    }
                    SetTotalUnitAmount();
                    SetTaxAmount();
                    SetDiscAmount();
                    SetTotalAmount();
                }
            }
        }

        [Appearance("CreditMemoLineTUAmountEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double TUAmount
        {
            get { return _tUAmount; }
            set { SetPropertyValue("TUAmount", ref _tUAmount, value); }
        }

        [Appearance("CreditMemoLineMultiTaxClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public bool MultiTax
        {
            get { return _multiTax; }
            set
            {
                SetPropertyValue("MultiTax", ref _multiTax, value);
                if (!IsLoading)
                {
                    SetNormalTax();
                }
            }
        }

        [Appearance("CreditMemoLineTaxClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Appearance("CreditMemoLineTaxEnabled", Criteria = "MultiTax = true", Enabled = false)]
        [ImmediatePostData]
        [DataSourceCriteria("Active = true")]
        public Tax Tax
        {
            get { return _tax; }
            set
            {
                SetPropertyValue("Tax", ref _tax, value);
                if (!IsLoading)
                {
                    if (this._tax != null)
                    {
                        this.TxValue = this._tax.Value;
                        SetTaxAmount();
                        SetTotalAmount();
                    }
                    else
                    {
                        SetNormalTax();
                    }
                }
            }
        }

        [Appearance("CreditMemoLineTxValueClose", Enabled = false)]
        [ImmediatePostData()]
        public double TxValue
        {
            get { return _txValue; }
            set
            {
                SetPropertyValue("TxValue", ref _txValue, value);
                if (!IsLoading)
                {
                    SetTaxAmount();
                    SetTotalAmount();
                }
            }
        }

        [Appearance("CreditMemoLineTxAmountEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double TxAmount
        {
            get { return _txAmount; }
            set
            {
                SetPropertyValue("TxAmount", ref _txAmount, value);
                if (!IsLoading)
                {
                    _globFunc = new GlobalFunction();
                    if (this._txAmount > 0)
                    {
                        if (_globFunc.GetRoundingList(Session, ObjectList.CreditMemoLine, FieldName.TxAmount) == true)
                        {
                            this._txAmount = _globFunc.GetRoundUp(Session, this._txAmount, ObjectList.CreditMemoLine, FieldName.TxAmount);
                        }
                    }

                    SetTotalAmount();
                }
            }
        }

        [Appearance("CreditMemoLineMultiDiscountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public bool MultiDiscount
        {
            get { return _multiDiscount; }
            set
            {
                SetPropertyValue("MultiDiscount", ref _multiDiscount, value);
                if (!IsLoading)
                {
                    SetNormalDiscount();
                }
            }
        }

        [Appearance("CreditMemoLineDiscountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Appearance("CreditMemoLineDiscountEnabled", Criteria = "MultiDiscount = true", Enabled = false)]
        [ImmediatePostData()]
        public Discount Discount
        {
            get { return _discount; }
            set
            {
                SetPropertyValue("Discount", ref _discount, value);
                if (!IsLoading)
                {
                    if (this._discount != null)
                    {
                        this.Disc = this._discount.Value;
                    }
                    else
                    {
                        SetNormalDiscount();
                    }
                }
            }
        }

        [Appearance("CreditMemoLineDiscClose", Enabled = false)]
        [Appearance("CreditMemoLineDiscEnabled", Criteria = "MultiDiscount = true", Enabled = false)]
        [ImmediatePostData()]
        public double Disc
        {
            get { return _disc; }
            set
            {
                SetPropertyValue("Disc", ref _disc, value);
                if (!IsLoading)
                {
                    SetDiscAmount();
                    SetTotalAmount();
                }
            }
        }

        [Appearance("CreditMemoLineDiscAmountEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double DiscAmount
        {
            get { return _discAmount; }
            set
            {
                SetPropertyValue("DiscAmount", ref _discAmount, value);
                if (!IsLoading)
                {
                    _globFunc = new GlobalFunction();
                    if (this._discAmount > 0)
                    {
                        if (_globFunc.GetRoundingList(Session, ObjectList.CreditMemoLine, FieldName.DiscAmount) == true)
                        {
                            this._discAmount = _globFunc.GetRoundUp(Session, this._discAmount, ObjectList.CreditMemoLine, FieldName.DiscAmount);
                        }
                    }
                }
            }
        }

        [Appearance("CreditMemoLineTPriceEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double TAmount
        {
            get { return _tAmount; }
            set
            {
                SetPropertyValue("TAmount", ref _tAmount, value);
                if (!IsLoading)
                {
                    _globFunc = new GlobalFunction();
                    if (this._tAmount > 0)
                    {
                        if (_globFunc.GetRoundingList(Session, ObjectList.CreditMemoLine, FieldName.TAmount) == true)
                        {
                            this._tAmount = _globFunc.GetRoundUp(Session, this._tAmount, ObjectList.CreditMemoLine, FieldName.TAmount);
                        }
                    }
                }
            }
        }
        #endregion Amount

        [Appearance("CreditMemoLineEstimationDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime EstimatedDate
        {
            get { return _estimatedDate; }
            set { SetPropertyValue("EstimatedDate", ref _estimatedDate, value); }
        }

        [Appearance("CreditMemoLineReturnTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public ReturnType ReturnType
        {
            get { return _returnType; }
            set { SetPropertyValue("ReturnType", ref _returnType, value); }
        }

        [Appearance("CreditMemoLineStatusEnabled", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("CreditMemoLineStatusDateEnabled", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("CreditMemoLinePostedCountEnabled", Enabled = false)]
        public int PostedCount
        {
            get { return _postedCount; }
            set { SetPropertyValue("PostedCount", ref _postedCount, value); }
        }

        [Appearance("CreditMemoLineSignCodeClose", Enabled = false)]
        public string SignCode
        {
            get { return _signCode; }
            set { SetPropertyValue("SignCode", ref _signCode, value); }
        }

        [Browsable(false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Browsable(false)]
        public PurchaseReturnMonitoring PurchaseReturnMonitoring
        {
            get { return _purchaseReturnMonitoring; }
            set { SetPropertyValue("PurchaseReturnMonitoring", ref _purchaseReturnMonitoring, value); }
        }

        [Appearance("CreditMemoLineCreditMemoEnabled", Enabled = false)]
        [Association("CreditMemo-CreditMemoLines")]
        public CreditMemo CreditMemo
        {
            get { return _creditMemo; }
            set
            {
                SetPropertyValue("CreditMemo", ref _creditMemo, value);
                if (!IsLoading)
                {
                    if (this._creditMemo != null)
                    {
                        if (this._creditMemo.Company != null)
                        {
                            this.Company = this._creditMemo.Company;
                        }
                    }
                }
            }
        }

        #region Clm

        //[Browsable(false)]
        //public bool HideSumTotalItem
        //{
        //    get { return _hideSumTotalItem; }
        //    set { SetPropertyValue("HideSumTotalItem", ref _hideSumTotalItem, value); }
        //}

        //[Appearance("CreditMemoLineEnabled", Criteria = "HideSumTotalItem = false", Visibility = ViewItemVisibility.Hide)]
        //[Persistent("TotalCML")]
        //public double TotalCML
        //{
        //    get
        //    {
        //        double _result = 0;
        //        if (!IsLoading)
        //        {
        //            if (GetTotalCML() > 0)
        //            {
        //                _result = UpdateTotalCML(true);
        //            }
        //        }
        //        return _result;
        //    }
        //}

        //[Browsable(false)]
        //[Appearance("CreditMemoLineTAmountCMLEnabled", Enabled = false)]
        //[ImmediatePostData()]
        //public double TAmountCML
        //{
        //    get
        //    {
        //        if (this._tUAmount > 0)
        //        {
        //            return GetTotalAmountCML();
        //        }
        //        else
        //        {
        //            return 0;
        //        }
        //    }
        //}

        //[Browsable(false)]
        //[Appearance("CreditMemoLineTGrandAmountCMLEnabled", Enabled = false)]
        //[ImmediatePostData()]
        //public double TGrandAmountCML
        //{
        //    get
        //    {
        //        if (this.TAmountCML > 0)
        //        {
        //            return GetTotalGrandAmountCML();
        //        }
        //        else
        //        {
        //            return 0;
        //        }
        //    }
        //}

        #endregion Clm

        #endregion Field

        //===== Code Only =====

        #region No

        public void UpdateNo()
        {
            try
            {
                if (!IsLoading && Session.IsNewObject(this))
                {
                    if (this.CreditMemo != null)
                    {
                        object _makRecord = Session.Evaluate<CreditMemoLine>(CriteriaOperator.Parse("Max(No)"), CriteriaOperator.Parse("CreditMemo=?", this.CreditMemo));
                        this.No = Convert.ToInt32(_makRecord) + 1;
                        this.Save();
                        RecoveryUpdateNo();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CreditMemoLine " + ex.ToString());
            }
        }

        public void RecoveryUpdateNo()
        {
            try
            {
                if (this.CreditMemo != null)
                {
                    CreditMemo _numHeader = Session.FindObject<CreditMemo>
                                                (new BinaryOperator("Code", this.CreditMemo.Code));

                    XPCollection<CreditMemoLine> _numLines = new XPCollection<CreditMemoLine>
                                                (Session, new BinaryOperator("CreditMemo", _numHeader),
                                                new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i1 = 0;
                        foreach (CreditMemoLine _numLine in _numLines)
                        {
                            i1 += 1;
                            _numLine.No = i1;
                            _numLine.Save();
                        }
                        i1 = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CreditMemoLine " + ex.ToString());
            }
        }

        public void RecoveryDeleteNo()
        {
            try
            {
                if (this.CreditMemo != null)
                {
                    CreditMemo _numHeader = Session.FindObject<CreditMemo>
                                                (new BinaryOperator("Code", this.CreditMemo.Code));

                    XPCollection<CreditMemoLine> _numLines = new XPCollection<CreditMemoLine>
                                                (Session, new GroupOperator(GroupOperatorType.And,
                                                 new BinaryOperator("This", this, BinaryOperatorType.NotEqual),
                                                 new BinaryOperator("CreditMemo", _numHeader)),
                                                 new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i = 0;
                        foreach (CreditMemoLine _numLine in _numLines)
                        {
                            i += 1;
                            _numLine.No = i;
                            _numLine.Save();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CreditMemoLine " + ex.ToString());
            }
        }

        #endregion No

        #region Description

        public string GetDescriptions()
        {
            string _result = "";
            try
            {
                if (this._item != null)
                {
                    string _result1 = null;
                    string _result2 = null;
                    if (this._item.Name != null)
                    {
                        _result1 = this._item.Name;
                    }
                    if (this._item.Description != null)
                    {
                        _result2 = this._item.Description;
                    }
                    _result = _result1 + " " + _result2;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CreditMemoLine " + ex.ToString());
            }
            return _result;
        }

        #endregion Description

        #region Set

        //Done1
        private void SetTotalUnitAmount()
        {
            try
            {
                _globFunc = new GlobalFunction();

                if (_tQty >= 0 & _uAmount >= 0)
                {
                    if (_globFunc.GetRoundingList(Session, ObjectList.CreditMemoLine, FieldName.TUAmount) == true)
                    {
                        this.TUAmount = _globFunc.GetRoundUp(Session, (this.TQty * this.UAmount), ObjectList.CreditMemoLine, FieldName.TUAmount);
                    }
                    else
                    {
                        this.TUAmount = this.TQty * this.UAmount;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CreditMemoLine " + ex.ToString());
            }
        }

        //Done1
        private void SetTotalAmount()
        {
            try
            {
                _globFunc = new GlobalFunction();

                if (_uAmount >= 0 && _txAmount >= 0 && _discAmount >= 0)
                {
                    if (!IsLoading)
                    {
                        if (this.Tax != null)
                        {
                            if (this.Tax.TaxNature == TaxNature.Increase)
                            {
                                if (_globFunc.GetRoundingList(Session, ObjectList.CreditMemoLine, FieldName.TAmount) == true)
                                {
                                    this.TAmount = _globFunc.GetRoundUp(Session, (this.TUAmount + this.TxAmount - this.DiscAmount), ObjectList.CreditMemoLine, FieldName.TAmount);
                                }
                                else
                                {
                                    this.TAmount = this.TUAmount + this.TxAmount - this.DiscAmount;
                                }
                            }
                            else if (this.Tax.TaxNature == TaxNature.Decrease)
                            {
                                if (_globFunc.GetRoundingList(Session, ObjectList.CreditMemoLine, FieldName.TAmount) == true)
                                {
                                    this.TAmount = _globFunc.GetRoundUp(Session, (this.TUAmount - this.TxAmount - this.DiscAmount), ObjectList.CreditMemoLine, FieldName.TAmount);
                                }
                                else
                                {
                                    this.TAmount = this.TUAmount - this.TxAmount - this.DiscAmount;
                                }
                            }
                            else if (this.Tax.TaxNature == TaxNature.None)
                            {
                                if (_globFunc.GetRoundingList(Session, ObjectList.CreditMemoLine, FieldName.TAmount) == true)
                                {
                                    this.TAmount = _globFunc.GetRoundUp(Session, (this.TUAmount - this.DiscAmount), ObjectList.CreditMemoLine, FieldName.TAmount);
                                }
                                else
                                {
                                    this.TAmount = this.TUAmount - this.DiscAmount;
                                }
                            }
                            else
                            {
                                if (_globFunc.GetRoundingList(Session, ObjectList.CreditMemoLine, FieldName.TAmount) == true)
                                {
                                    this.TAmount = _globFunc.GetRoundUp(Session, (this.TUAmount + this.TxAmount - this.DiscAmount), ObjectList.CreditMemoLine, FieldName.TAmount);
                                }
                                else
                                {
                                    this.TAmount = this.TUAmount + this.TxAmount - this.DiscAmount;
                                }
                            }
                        }
                        //clm
                        else
                        {
                            this.TAmount = this.TUAmount + this.TxAmount - this.DiscAmount;
                        }
                    }
                }
                //clm
                else
                {
                    this.TAmount = this.TUAmount + this.TxAmount - this.DiscAmount;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CreditMemoLine " + ex.ToString());
            }
        }

        //Done1
        private void SetTotalQty()
        {
            try
            {
                double _locInvLineTotal = 0;
                DateTime now = DateTime.Now;
                if (this.CreditMemo != null)
                {
                    if (this.Item != null && this.UOM != null && this.DUOM != null)
                    {
                        ItemUnitOfMeasure _locItemUOM = Session.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", this.Item),
                                                         new BinaryOperator("UOM", this.UOM),
                                                         new BinaryOperator("DefaultUOM", this.DUOM),
                                                         new BinaryOperator("Active", true)));
                        if (_locItemUOM != null)
                        {
                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.Qty * _locItemUOM.DefaultConversion + this.DQty;
                            }
                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.Qty / _locItemUOM.Conversion + this.DQty;
                            }
                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.Qty + this.DQty;
                            }

                            this.TQty = _locInvLineTotal;
                        }
                    }
                    else
                    {
                        _locInvLineTotal = this.Qty + this.DQty;
                        this.TQty = _locInvLineTotal;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CreditMemoLine " + ex.ToString());
            }
        }

        //Done1
        private void SetDiscAmount()
        {
            try
            {
                _globFunc = new GlobalFunction();

                if (_uAmount >= 0 && _discAmount >= 0)
                {
                    if (_globFunc.GetRoundingList(Session, ObjectList.CreditMemoLine, FieldName.DiscAmount) == true)
                    {
                        this.DiscAmount = _globFunc.GetRoundUp(Session, (this.TUAmount * this.Disc / 100), ObjectList.CreditMemoLine, FieldName.DiscAmount);
                    }
                    else
                    {
                        this.DiscAmount = this.TUAmount * this.Disc / 100;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CreditMemoLine " + ex.ToString());
            }
        }

        //Done1
        private void SetTaxAmount()
        {
            try
            {
                _globFunc = new GlobalFunction();

                if (_uAmount >= 0 && Tax.Value >= 0)
                {
                    if (_globFunc.GetRoundingList(Session, ObjectList.CreditMemoLine, FieldName.TxAmount) == true)
                    {
                        this.TxAmount = _globFunc.GetRoundUp(Session, (this.TUAmount * this.TxValue / 100), ObjectList.CreditMemoLine, FieldName.TxAmount);
                    }
                    else
                    {
                        this.TxAmount = this.TUAmount * this.TxValue / 100;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CreditMemoLine " + ex.ToString());
            }
        }

        //Done1
        private void SetMaxTotalQty()
        {
            try
            {
                double _locInvLineTotal = 0;
                DateTime now = DateTime.Now;
                if (this.CreditMemo != null)
                {
                    if (this.Item != null && this.MxUOM != null && this.MxDUOM != null)
                    {
                        ItemUnitOfMeasure _locItemUOM = Session.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", this.Item),
                                                         new BinaryOperator("UOM", this.MxUOM),
                                                         new BinaryOperator("DefaultUOM", this.MxDUOM),
                                                         new BinaryOperator("Active", true)));
                        if (_locItemUOM != null)
                        {
                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.MxQty * _locItemUOM.DefaultConversion + this.MxDQty;
                            }
                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.MxQty / _locItemUOM.Conversion + this.MxDQty;
                            }
                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.MxQty + this.MxDQty;
                            }

                            this.MxTQty = _locInvLineTotal;
                        }
                    }
                    else
                    {
                        _locInvLineTotal = this.MxQty + this.MxDQty;
                        this.MxTQty = _locInvLineTotal;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CreditMemoLine " + ex.ToString());
            }
        }

        //Done1
        private void SetMaxTotalUnitAmount()
        {
            try
            {
                double _locInvLineTotal = 0;
                DateTime now = DateTime.Now;
                if (this.CreditMemo != null)
                {
                    if (this._mxTQty >= 0 && this._mxUAmount >= 0)
                    {
                        this.MxTUAmount = this._mxTQty * this.MxUAmount;
                    }
                    else
                    {
                        _locInvLineTotal = this._mxTQty + this.MxDQty;
                        this.MxTQty = _locInvLineTotal;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CreditMemoLine " + ex.ToString());
            }
        }

        //Done1
        private void SetTotalPQty()
        {
            try
            {
                double _locInvLineTotal = 0;
                DateTime now = DateTime.Now;
                if (this.CreditMemo != null)
                {
                    if (this.Item != null && this.UOM != null && this.DUOM != null)
                    {
                        ItemUnitOfMeasure _locItemUOM = Session.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", this.Item),
                                                         new BinaryOperator("UOM", this.UOM),
                                                         new BinaryOperator("DefaultUOM", this.DUOM),
                                                         new BinaryOperator("Active", true)));
                        if (_locItemUOM != null)
                        {
                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.PQty * _locItemUOM.DefaultConversion + this.PDQty;
                            }
                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.PQty / _locItemUOM.Conversion + this.PDQty;
                            }
                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.PQty + this.PDQty;
                            }

                            this.PTQty = _locInvLineTotal;
                        }
                    }
                    else
                    {
                        _locInvLineTotal = this.PQty + this.PDQty;
                        this.PTQty = _locInvLineTotal;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CreditMemoLine " + ex.ToString());
            }
        }

        #region SetMaxInQty

        //Done1
        private void SetMxDQty()
        {
            try
            {
                if (this.CreditMemo != null)
                {

                    XPCollection<ApprovalLine> _locApprovalLineXPOs = new XPCollection<ApprovalLine>
                                                                   (Session, new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("CreditMemo", this.CreditMemo)));

                    if (_locApprovalLineXPOs != null && _locApprovalLineXPOs.Count() > 0)
                    {
                        if (this.Status == Status.Posted || this.PostedCount > 0)
                        {
                            if (this.RmDQty > 0)
                            {
                                if (this._dQty > this.RmDQty)
                                {
                                    this._dQty = this.RmDQty;
                                }
                            }
                        }
                        else if (this.Status == Status.Close || this.PostedCount > 0)
                        {
                            this._dQty = 0;
                        }
                    }
                    else
                    {
                        if ((this.Status == Status.Open || this.Status == Status.Progress) && this.PostedCount <= 0)
                        {
                            if (this._dQty > 0)
                            {
                                this.MxDQty = this._dQty;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CreditMemoLine " + ex.ToString());
            }
        }

        //Done1
        private void SetMxDUOM()
        {
            try
            {
                if (this.CreditMemo != null)
                {
                    ApprovalLine _locApprovalLineXPO = Session.FindObject<ApprovalLine>
                                                                   (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("CreditMemo", this.CreditMemo)));
                    if (_locApprovalLineXPO == null)
                    {
                        if ((this.Status == Status.Open || this.Status == Status.Progress)
                            && this.PostedCount <= 0)
                        {
                            this.MxDUOM = this.DUOM;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CreditMemoLine " + ex.ToString());
            }
        }

        //Done1
        private void SetMxQty()
        {
            try
            {
                if (this.CreditMemo != null)
                {
                    XPCollection<ApprovalLine> _locApprovalLineXPOs = new XPCollection<ApprovalLine>
                                                                   (Session, new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("CreditMemo", this.CreditMemo)));
                    if (_locApprovalLineXPOs != null && _locApprovalLineXPOs.Count() > 0)
                    {
                        if (this.Status == Status.Posted || this.PostedCount > 0)
                        {
                            if (this.RmQty > 0)
                            {
                                if (this._qty > this.RmQty)
                                {
                                    this._qty = this.RmQty;
                                }
                            }
                        }
                        else if (this.Status == Status.Close || this.PostedCount > 0)
                        {
                            this._qty = 0;
                        }
                    }
                    else
                    {
                        if ((this.Status == Status.Open || this.Status == Status.Progress) && this.PostedCount <= 0)
                        {
                            if (this._qty > 0)
                            {
                                this.MxQty = this._qty;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CreditMemoLine " + ex.ToString());
            }
        }

        //Done1
        private void SetMxUOM()
        {
            try
            {
                if (this.CreditMemo != null)
                {
                    ApprovalLine _locApprovalLineXPO = Session.FindObject<ApprovalLine>
                                                                   (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("CreditMemo", this.CreditMemo)));
                    if (_locApprovalLineXPO == null)
                    {
                        if ((this.Status == Status.Open || this.Status == Status.Progress)
                            && this.PostedCount <= 0)
                        {
                            this.MxUOM = this.UOM;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CreditMemoLine " + ex.ToString());
            }
        }

        #endregion SetMaxInQty

        #endregion Set

        #region Tax

        [Association("CreditMemoLine-TaxLines")]
        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        [Appearance("CreditMemoLineTaxLineClose", Criteria = "MultiTax = false", Visibility = ViewItemVisibility.Hide, Context = "DetailView")]
        [Appearance("CreditMemoLineTaxLineEnabled", Criteria = "MultiTax = true", Visibility = ViewItemVisibility.Show, Context = "DetailView")]
        public XPCollection<TaxLine> TaxLines
        {
            get { return GetCollection<TaxLine>("TaxLines"); }
        }

        //Done1
        private void SetNormalTax()
        {
            try
            {
                if (_tax != null && _txValue > 0 && _txAmount > 0)
                {
                    this.Tax = this.Tax;
                    this.TxValue = this.TxValue;
                    this.TxAmount = this.TxAmount;
                }
                else
                {
                    this.TxValue = 0;
                    this.TxAmount = 0;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CreditMemoLine " + ex.ToString());
            }
        }

        //Done
        public Tax UpdateTaxLine(bool forceChangeEvents)
        {
            Tax _result = null;
            try
            {
                Tax _locTax = null;
                XPCollection<TaxLine> _locTaxLines = new XPCollection<TaxLine>(Session,
                                                     new GroupOperator(GroupOperatorType.And,
                                                     new BinaryOperator("CreditMemoLine", this)),
                                                     new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));

                if (_locTaxLines != null && _locTaxLines.Count > 0)
                {
                    if (_locTaxLines.Count == 1)
                    {
                        foreach (TaxLine _locTaxLinerLine in _locTaxLines)
                        {
                            if (forceChangeEvents)
                            {
                                //OnChanged("CurrencyLine", oldCurrencyLine, _locSalesOrderLine.Currency);
                                _result = _locTaxLinerLine.Tax;
                            }
                        }
                    }
                    else
                    {
                        foreach (TaxLine _locTaxLine in _locTaxLines)
                        {
                            if (_locTaxLine.No == 1)
                            {
                                _locTax = _locTaxLine.Tax;
                            }
                            else
                            {
                                if (_locTaxLine.Tax != _locTax)
                                {
                                    _globFunc = new GlobalFunction();
                                    //OnChanged("CurrencyLine", oldCurrencyLine, _globFunc.GetDefaultCurrency(Session));
                                    _result = _globFunc.GetDefaultTax(Session, TaxMode.Purchase);
                                    break;
                                }
                                else
                                {
                                    _result = _locTaxLine.Tax;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = CreditMemoLine ", ex.ToString());
            }
            return _result;
        }

        #endregion Tax

        #region Discount

        [Association("CreditMemoLine-DiscountLines")]
        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        [Appearance("CreditMemoLineDiscountLineClose", Criteria = "MultiDiscount = false", Visibility = ViewItemVisibility.Hide, Context = "DetailView")]
        [Appearance("CreditMemoLineDiscountLineEnabled", Criteria = "MultiDiscount = true", Visibility = ViewItemVisibility.Show, Context = "DetailView")]
        public XPCollection<DiscountLine> DiscountLines
        {
            get { return GetCollection<DiscountLine>("DiscountLines"); }
        }

        //Done1
        private void SetNormalDiscount()
        {
            try
            {
                if (_discount != null)
                {
                    this.Disc = this.Disc;
                    this.DiscAmount = this.DiscAmount;
                }
                else
                {
                    this.Disc = 0;
                    this.DiscAmount = 0;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CreditMemoLine " + ex.ToString());
            }
        }

        //Done1
        public Discount UpdateDiscountLine(bool forceChangeEvents)
        {
            Discount _result = null;
            try
            {
                Discount _locDiscount = null;
                XPCollection<DiscountLine> _locDiscountLines = new XPCollection<DiscountLine>(Session,
                                                               new GroupOperator(GroupOperatorType.And,
                                                               new BinaryOperator("CreditMemoLine", this)),
                                                               new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));

                if (_locDiscountLines != null && _locDiscountLines.Count > 0)
                {
                    if (_locDiscountLines.Count == 1)
                    {
                        foreach (DiscountLine _locDiscountLinerLine in _locDiscountLines)
                        {
                            if (forceChangeEvents)
                            {
                                //OnChanged("CurrencyLine", oldCurrencyLine, _locSalesOrderLine.Currency);
                                _result = _locDiscountLinerLine.Discount;
                            }
                        }
                    }
                    else
                    {
                        foreach (DiscountLine _locDiscountLine in _locDiscountLines)
                        {
                            if (_locDiscountLine.No == 1)
                            {
                                _locDiscount = _locDiscountLine.Discount;
                            }
                            else
                            {
                                if (_locDiscountLine.Discount != _locDiscount)
                                {
                                    _globFunc = new GlobalFunction();
                                    //OnChanged("CurrencyLine", oldCurrencyLine, _globFunc.GetDefaultCurrency(Session));
                                    _result = _globFunc.GetDefaultDiscount(Session, DiscountMode.Purchase);
                                    break;
                                }
                                else
                                {
                                    _result = _locDiscountLine.Discount;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = CreditMemoLine ", ex.ToString());
            }
            return _result;
        }

        #endregion Discount

        #region Clm

        //private double GetTotalAmountCML()
        //{
        //    double _result = 0;
        //    try
        //    {
        //        double _locTotalTaxCml = 0;
        //        if (!IsLoading)
        //        {
        //            XPCollection<CreditMemoLine> _locCreditMemoLines = new XPCollection<CreditMemoLine>(Session,
        //                                                               new BinaryOperator("CreditMemo", this.CreditMemo));

        //            if (_locCreditMemoLines.Count() >= 0)
        //            {
        //                foreach (CreditMemoLine _locCreditMemoLine in _locCreditMemoLines)
        //                {
        //                    _locTotalTaxCml = _locTotalTaxCml + _locCreditMemoLine.TUAmount;
        //                }
        //                _result = _locTotalTaxCml;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Tracing.Tracer.LogError("BusinessObject = CreditMemoLine" + ex.ToString());
        //    }

        //    return _result;
        //}

        ////grand
        //public int GetTotalCML()
        //{
        //    int _result = 0;
        //    try
        //    {
        //        XPCollection<CreditMemoLine> _locCreditMemoLines = new XPCollection<CreditMemoLine>(Session, new GroupOperator(GroupOperatorType.And,
        //                                                           new BinaryOperator("CreditMemoLine", this)));

        //        if (_locCreditMemoLines != null && _locCreditMemoLines.Count > 0)
        //        {
        //            _result = _locCreditMemoLines.Count();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Tracing.Tracer.LogError("Business Object = CreditMemoLine", ex.ToString());
        //    }
        //    return _result;
        //}

        //public double UpdateTotalCML(bool forceChangeEvents)
        //{
        //    double _result = 0;
        //    try
        //    {
        //        double _locTotalAmount = 0;
        //        if (!IsLoading)
        //        {
        //            XPCollection<CreditMemoLine> _locCreditMemoLines = new XPCollection<CreditMemoLine>(Session, new GroupOperator(GroupOperatorType.And,
        //                                                               new BinaryOperator("CreditMemoLine", this)));

        //            if (_locCreditMemoLines != null && _locCreditMemoLines.Count() > 0)
        //            {
        //                foreach (CreditMemoLine _locCreditMemoLine in _locCreditMemoLines)
        //                {
        //                    _locTotalAmount = _locTotalAmount + _locCreditMemoLine.TAmountCML;
        //                }
        //                _result = _locTotalAmount;
        //            }

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Tracing.Tracer.LogError(" BusinessObject = ProjectLine " + ex.ToString());
        //    }
        //    return _result;
        //}

        //private double GetTotalGrandAmountCML()
        //{
        //    double _result = 0;
        //    try
        //    {
        //        double _locTGrandAmountCML = 0;

        //        if (!IsLoading)
        //        {
        //            if (_creditMemo != null)
        //            {
        //                XPCollection<CreditMemoLine> _locCreditMemoLines = new XPCollection<CreditMemoLine>(Session, new GroupOperator(GroupOperatorType.And,
        //                                                                   new BinaryOperator("CreditMemo", this.CreditMemo)));

        //                if (_locCreditMemoLines != null && _locCreditMemoLines.Count > 0)
        //                {
        //                    foreach (CreditMemoLine _locCreditMemoLine in _locCreditMemoLines)
        //                    {
        //                        _locTGrandAmountCML = _locCreditMemoLine.TAmountCML;
        //                    }
        //                }
        //                _result = _locTGrandAmountCML;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Tracing.Tracer.LogError("Business Object = CreditMemo", ex.ToString());
        //    }
        //    return _result;

        //}

        #endregion Clm
    }
}