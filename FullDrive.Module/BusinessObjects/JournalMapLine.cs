﻿#region Default

using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

#endregion Default

using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    #region Default

    [DefaultClassOptions]
    [DefaultProperty("Name")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("JournalMapLineRuleUnique", DefaultContexts.Save, "Code")]

    #endregion Default

    // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
    public class JournalMapLine : FullDriveSysBaseObject
    {
        #region Default

        private int _no;
        private string _code;
        private string _name;
        private AccountMap _accountMap;
        private JournalMap _journalMap;
        private GlobalFunction _globFunc;

        public JournalMapLine(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.JournalMapLine);
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        //No
        protected override void OnSaving()
        {
            base.OnSaving();
            if (!IsLoading && IsSaving)
            {
                UpdateNo();
            }
        }
        protected override void OnDeleting()
        {
            base.OnDeleting();
            if (!IsLoading)
            {
                RecoveryDeleteNo();
            }
        }

        #endregion Default

        #region Field

        [Appearance("JournalMapLineNoClose", Enabled = false)]
        public int No
        {
            get { return _no; }
            set { SetPropertyValue("No", ref _no, value); }
        }

        [Appearance("JournalMapLineCodeClose", Enabled = false)]
        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        //[DataSourceCriteria("Active = true")]
        public AccountMap AccountMap
        {
            get { return _accountMap; }
            set { SetPropertyValue("AccountMap", ref _accountMap, value); }
        }

        [Appearance("JournalMapLineJournalMapClose", Enabled = false)]
        [Association("JournalMap-JournalMapLines")]
        public JournalMap JournalMap
        {
            get { return _journalMap; }
            set { SetPropertyValue("JournalMap", ref _journalMap, value); }
        }

        #endregion Field

        //==== Code Only ====

        #region No

        public void UpdateNo()
        {
            try
            {
                if (!IsLoading && Session.IsNewObject(this))
                {
                    if (this.JournalMap != null)
                    {
                        object _makRecord = Session.Evaluate<JournalMapLine>(CriteriaOperator.Parse("Max(No)"), CriteriaOperator.Parse("JournalMap=?", this.JournalMap));
                        this.No = Convert.ToInt32(_makRecord) + 1;
                        this.Save();
                        RecoveryUpdateNo();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = JournalMapLine " + ex.ToString());
            }
        }

        public void RecoveryUpdateNo()
        {
            try
            {
                if (this.JournalMap != null)
                {
                    JournalMap _numHeader = Session.FindObject<JournalMap>
                                                (new BinaryOperator("Code", this.JournalMap.Code));

                    XPCollection<JournalMapLine> _numLines = new XPCollection<JournalMapLine>
                                                (Session, new BinaryOperator("JournalMap", _numHeader),
                                                new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i1 = 0;
                        foreach (JournalMapLine _numLine in _numLines)
                        {
                            i1 += 1;
                            _numLine.No = i1;
                            _numLine.Save();
                        }
                        i1 = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = JournalMapLine " + ex.ToString());
            }
        }

        public void RecoveryDeleteNo()
        {
            try
            {
                if (this.JournalMap != null)
                {
                    JournalMap _numHeader = Session.FindObject<JournalMap>
                                                (new BinaryOperator("Code", this.JournalMap.Code));

                    XPCollection<JournalMapLine> _numLines = new XPCollection<JournalMapLine>
                                                (Session, new GroupOperator(GroupOperatorType.And,
                                                 new BinaryOperator("This", this, BinaryOperatorType.NotEqual),
                                                 new BinaryOperator("JournalMap", _numHeader)),
                                                 new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i = 0;
                        foreach (JournalMapLine _numLine in _numLines)
                        {
                            i += 1;
                            _numLine.No = i;
                            _numLine.Save();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = JournalMapLine " + ex.ToString());
            }
        }

        #endregion No

    }
}