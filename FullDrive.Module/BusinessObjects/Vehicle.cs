﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.ExpressApp.Editors;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [NavigationItem("Transportation")]
    [DefaultProperty("VehicleNo")]
    [RuleCombinationOfPropertiesIsUnique("VehicleRuleUnique", DefaultContexts.Save, "Code, VehicleNo")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class Vehicle : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).

        private string _code;
        private ServiceModa _serviceModa;
        private VehicleMode _vehicleMode;
        private VehicleType _vehicleType;
        private string _name;
        private string _vehicleNo;
        private BusinessPartner _businessPartner;
        private Double _capacity;
        private UnitOfMeasure _uom;
        private DateTime _yearVehicle;
        private DateTime _keur;
        private DateTime _licence;
        private DateTime _emissionTest;
        private DateTime _sipa_kiu;
        private DateTime _ibm;
        private XPCollection<Driver> _availableDriver;
        private Driver _driver;
        private string _remark;
        private bool _active;
        private GlobalFunction _globFunc;

        public Vehicle(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.Vehicle);
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        public ServiceModa ServiceModa
        {
            get { return _serviceModa; }
            set { SetPropertyValue("ServiceModa", ref _serviceModa, value); }
        }

        public VehicleMode VehicleMode
        {
            get { return _vehicleMode; }
            set { SetPropertyValue("VehicleMode", ref _vehicleMode, value); }
        }

        public VehicleType VehicleType
        {
            get { return _vehicleType; }
            set { SetPropertyValue("VehicleType", ref _vehicleType, value); }
        }

        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        public string VehicleNo
        {
            get { return _vehicleNo; }
            set { SetPropertyValue("VehicleNo", ref _vehicleNo, value); }
        }

        [ImmediatePostData()]
        public BusinessPartner BusinessPartner
        {
            get { return _businessPartner; }
            set { SetPropertyValue("BusinessPartner", ref _businessPartner, value); }
        }

        public Double Capacity
        {
            get { return _capacity; }
            set { SetPropertyValue("Capacity", ref _capacity, value); }
        }

        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        public UnitOfMeasure UOM
        {
            get { return _uom; }
            set { SetPropertyValue("UOM", ref _uom, value); }
        }

        public string VehicleCapacity
        {
            get
            {
                if (this.Capacity > 0 && this.UOM != null)
                {
                    return String.Format("{0} ({1})", this.Capacity.ToString(), this.UOM.Code);
                }
                return null;
            }
        }

        public DateTime YearVehicle
        {
            get { return _yearVehicle; }
            set { SetPropertyValue("YearVehicle", ref _yearVehicle, value); }
        }

        public DateTime Keur
        {
            get { return _keur; }
            set { SetPropertyValue("Keur", ref _keur, value); }
        }

        public DateTime Licence
        {
            get { return _licence; }
            set { SetPropertyValue("Licence", ref _licence, value); }
        }

        public DateTime EmissionTest
        {
            get { return _emissionTest; }
            set { SetPropertyValue("EmissionTest", ref _emissionTest, value); }
        }

        public DateTime SIPA_KIU
        {
            get { return _sipa_kiu; }
            set { SetPropertyValue("SIPA_KIU", ref _sipa_kiu, value); }
        }

        public DateTime IBM
        {
            get { return _ibm; }
            set { SetPropertyValue("IBM", ref _ibm, value); }
        }

        [Browsable(false)]
        public XPCollection<Driver> AvailableDriver
        {
            get
            {
                if (BusinessPartner == null)
                {
                    _availableDriver = new XPCollection<Driver>(Session);
                }
                else
                {
                    _availableDriver = new XPCollection<Driver>(Session,
                                                new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("BusinessPartner", this.BusinessPartner),
                                                    new BinaryOperator("Active", true)));
                }
                return _availableDriver;
            }
        }

        [DataSourceProperty("AvailableDriver", DataSourcePropertyIsNullMode.SelectAll)]
        public Driver Driver
        {
            get { return _driver; }
            set { SetPropertyValue("Driver", ref _driver, value); }
        }

        [Size(512)]
        public string Remark
        {
            get { return _remark; }
            set { SetPropertyValue("Remark", ref _remark, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue("Active", ref _active, value); }
        }

        #endregion Field
    }
}