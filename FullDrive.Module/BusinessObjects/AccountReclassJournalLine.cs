﻿#region Default

using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

#endregion Default

using DevExpress.ExpressApp.Editors;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    #region Default

    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("AccountReclassJournalLineRuleUnique", DefaultContexts.Save, "Code")]

    #endregion Default

    // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
    public class AccountReclassJournalLine : FullDriveSysBaseObject
    {
        #region Default

        private bool _activationPosting;
        private int _no;
        private string _code;
        private string _name;
        private DateTime _postingDate;
        private PostingType _postingType;
        private PostingMethod _postingMethod;
        private PostingMethodType _postingMethodType;
        private BusinessPartner _customer;
        private string _description;
        private AccountGroup _accountGroup;
        private XPCollection<ChartOfAccount> _availableChartOfAccount;
        private ChartOfAccount _account;
        private bool _openDebit;
        private double _debit;
        private bool _openCredit;
        private double _credit;
        private Status _status;
        private DateTime _statusDate;
        private DateTime _postJournalDate;
        private Company _company;
        private Division _division;
        private Department _department;
        private Section _section;
        private AccountReclassJournal _accountReclassJournal;
        private string _userAccess;
        private GlobalFunction _globFunc;

        public AccountReclassJournalLine(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
            if (!IsLoading)
            {
                DateTime now = DateTime.Now;
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.AccountReclassJournalLine);
                this.Status = Status.Open;
                this.StatusDate = now;

                _userAccess = SecuritySystem.CurrentUserName;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                if (Session.IsNewObject(this))
                {
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null)
                            {
                                Company = _locUserAccess.Employee.Company;                                
                            }
                            if(_locUserAccess.Employee.Division != null)
                            {
                                Division = _locUserAccess.Employee.Division;
                            }
                            if (_locUserAccess.Employee.Department != null)
                            {
                                Department = _locUserAccess.Employee.Department;
                            }
                            if (_locUserAccess.Employee.Section != null)
                            {
                                Section = _locUserAccess.Employee.Section;
                            }
                        }
                    }
                }
            }
        }

        #endregion Default

        #region Field

        protected override void OnSaving()
        {
            base.OnSaving();
            if (!IsLoading && IsSaving)
            {
                UpdateNo();
            }
        }

        protected override void OnDeleting()
        {
            base.OnDeleting();
            if (!IsLoading)
            {
                RecoveryDeleteNo();
            }
        }

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [Appearance("AccountReclassJournalLineNoEnabled", Enabled = false)]
        public int No
        {
            get { return _no; }
            set { SetPropertyValue("No", ref _no, value); }
        }

        [Appearance("AccountReclassJournalLineCodeEnabled", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("AccountReclassJournalLineNameClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }        

        [Appearance("AccountReclassJournalLinePostingTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PostingType PostingType
        {
            get { return _postingType; }
            set { SetPropertyValue("PostingType", ref _postingType, value); }
        }

        [Appearance("AccountReclassJournalLinePostingMethodClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PostingMethod PostingMethod
        {
            get { return _postingMethod; }
            set { SetPropertyValue("PostingMethod", ref _postingMethod, value); }
        }

        [Appearance("AccountReclassJournalLinePostingMethodTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PostingMethodType PostingMethodType
        {
            get { return _postingMethodType; }
            set { SetPropertyValue("PostingMethodType", ref _postingMethodType, value); }
        } 

        [Appearance("AccountReclassJournalLineCostumerClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BusinessPartner Customer
        {
            get { return _customer; }
            set { SetPropertyValue("Customer", ref _customer, value); }
        }

        [Size(512)]
        [Appearance("AccountReclassJournalLineDescriptionClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        [ImmediatePostData]
        [Appearance("AccountReclassJournalLineAccountGroupClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public AccountGroup AccountGroup
        {
            get { return _accountGroup; }
            set { SetPropertyValue("AccountGroup", ref _accountGroup, value); }
        }

        [Browsable(false)]
        public XPCollection<ChartOfAccount> AvailableChartOfAccount
        {
            get
            {
                
                if (AccountGroup != null)
                {
                    _availableChartOfAccount = new XPCollection<ChartOfAccount>
                                               (Session, new GroupOperator(GroupOperatorType.And,
                                                new BinaryOperator("AccountGroup", this.AccountGroup),
                                                new BinaryOperator("Active", true)));
                }
                else
                {
                    _availableChartOfAccount = new XPCollection<ChartOfAccount>(Session);

                }
                return _availableChartOfAccount;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableChartOfAccount", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("AccountReclassJournalLineAccountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public ChartOfAccount Account
        {
            get { return _account; }
            set { SetPropertyValue("Account", ref _account, value); }
        }

        [Appearance("AccountReclassJournalLineOpenDebitClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public bool OpenDebit
        {
            get { return _openDebit; }
            set {
                SetPropertyValue("CloseDebit", ref _openDebit, value);
                if(!IsLoading)
                {
                    if(this._openDebit == true)
                    {
                        this.OpenCredit = false;
                        this.Credit = 0;
                    }
                    else
                    {
                        this.Debit = 0;
                    }
                }
            }
        }

        [Appearance("AccountReclassJournalLineDebitClose1", Criteria = "OpenDebit = false", Enabled = false)]
        [Appearance("AccountReclassJournalLineDebitClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double Debit
        {
            get { return _debit; }
            set { SetPropertyValue("Debit", ref _debit, value); }
        }

        [Appearance("AccountReclassJournalLineOpenCreditClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public bool OpenCredit
        {
            get { return _openCredit; }
            set {
                SetPropertyValue("OpenCredit", ref _openCredit, value);
                if (!IsLoading)
                {
                    if (this._openCredit == true)
                    {
                        this.OpenDebit = false;
                        this.Debit = 0;
                    }
                    else
                    {
                        this.Credit = 0;
                    }
                }
            }
        }

        [Appearance("AccountReclassJournalLineCreditClose1", Criteria = "OpenCredit = false", Enabled = false)]
        [Appearance("AccountReclassJournalLineCreditClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double Credit
        {
            get { return _credit; }
            set { SetPropertyValue("Credit", ref _credit, value); }
        }

        [Appearance("AccountReclassJournalLineStatusEnabled", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("AccountReclassJournalLineStatusDateEnabled", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("AccountReclassJournalLineCompanyEnabled", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Appearance("AccountReclassJournalLineDivisionEnabled", Enabled = false)]
        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        [Appearance("AccountReclassJournalLineDepartmentEnabled", Enabled = false)]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        [Appearance("AccountReclassJournalLineSectionEnabled", Enabled = false)]
        public Section Section
        {
            get { return _section; }
            set { SetPropertyValue("Section", ref _section, value); }
        }

        [ImmediatePostData()]
        [Association("AccountReclassJournal-AccountReclassJournalLines")]
        [Appearance("AccountReclassJournalLineAccountReclassJournalEnabled", Enabled = false)]
        public AccountReclassJournal AccountReclassJournal
        {
            get { return _accountReclassJournal; }
            set {
                SetPropertyValue("AccountReclassJournal", ref _accountReclassJournal, value);
                if (!IsLoading)
                {
                    if (this._accountReclassJournal != null)
                    {
                        this.PostingType = this._accountReclassJournal.PostingType;
                        this.PostingMethod = this._accountReclassJournal.PostingMethod;
                        this.PostingMethodType = this._accountReclassJournal.PostingMethodType;
                        this.Company = this._accountReclassJournal.CompanyDefault;
                    }
                }
            }
        }

        #endregion Field

        //===== Code Only ====

        #region No

        public void UpdateNo()
        {
            try
            {
                if (!IsLoading && Session.IsNewObject(this))
                {
                    if (this.AccountReclassJournal != null)
                    {
                        object _makRecord = Session.Evaluate<AccountReclassJournalLine>(CriteriaOperator.Parse("Max(No)"), CriteriaOperator.Parse("AccountReclassJournal=?", this.AccountReclassJournal));
                        this.No = Convert.ToInt32(_makRecord) + 1;
                        this.Save();
                        RecoveryUpdateNo();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = AccountReclassJournalLine " + ex.ToString());
            }
        }

        public void RecoveryUpdateNo()
        {
            try
            {
                if (this.AccountReclassJournal != null)
                {
                    AccountReclassJournal _numHeader = Session.FindObject<AccountReclassJournal>
                                                (new BinaryOperator("Code", this.AccountReclassJournal.Code));

                    XPCollection<AccountReclassJournalLine> _numLines = new XPCollection<AccountReclassJournalLine>
                                                (Session, new BinaryOperator("AccountReclassJournal", _numHeader),
                                                new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i1 = 0;
                        foreach (AccountReclassJournalLine _numLine in _numLines)
                        {
                            i1 += 1;
                            _numLine.No = i1;
                            _numLine.Save();
                        }
                        i1 = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = AccountReclassJournalLine " + ex.ToString());
            }
        }

        public void RecoveryDeleteNo()
        {
            try
            {
                if (this.AccountReclassJournal != null)
                {
                    AccountReclassJournal _numHeader = Session.FindObject<AccountReclassJournal>
                                                (new BinaryOperator("Code", this.AccountReclassJournal.Code));

                    XPCollection<AccountReclassJournalLine> _numLines = new XPCollection<AccountReclassJournalLine>
                                                (Session, new GroupOperator(GroupOperatorType.And,
                                                 new BinaryOperator("This", this, BinaryOperatorType.NotEqual),
                                                 new BinaryOperator("AccountReclassJournal", _numHeader)),
                                                 new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i = 0;
                        foreach (AccountReclassJournalLine _numLine in _numLines)
                        {
                            i += 1;
                            _numLine.No = i;
                            _numLine.Save();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = AccountReclassJournalLine " + ex.ToString());
            }
        }

        #endregion No

    }
}