﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Purchase")]
    [RuleCombinationOfPropertiesIsUnique("PurchaseRequisitionCollectionRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class PurchaseRequisitionCollection : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private bool _activationPosting;
        private string _code;
        private XPCollection<PurchaseRequisition> _availablePurchaseRequisition;
        private PurchaseRequisition _purchaseRequisition;
        private PurchaseOrder _purchaseOrder;
        private Status _status;
        private DateTime _statusDate;
        private GlobalFunction _globFunc;

        public PurchaseRequisitionCollection(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.PurchaseRequisitionCollection);
                DateTime now = DateTime.Now;
                this.Status = Status.Open;
                this.StatusDate = now;
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("PurchaseRequisitionCollectionCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Browsable(false)]
        public XPCollection<PurchaseRequisition> AvailablePurchaseRequisition
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;

                if (!IsLoading)
                {
                    if (this.PurchaseOrder != null)
                    {
                        XPQuery<PurchaseRequisitionMonitoring> _purchaseRequisitionMonitoringsQuery = new XPQuery<PurchaseRequisitionMonitoring>(Session);

                        var _purchaseRequisitionMonitorings = from prm in _purchaseRequisitionMonitoringsQuery
                                                              where (prm.Status == Status.Open && prm.PostedCount == 0
                                                              && prm.PurchaseOrder == null)
                                                              group prm by prm.PurchaseRequisition into g
                                                              select new { PurchaseRequisition = g.Key };

                        if (_purchaseRequisitionMonitorings != null && _purchaseRequisitionMonitorings.Count() > 0)
                        {
                            List<string> _stringPRM = new List<string>();

                            foreach (var _purchaseRequisitionMonitoring in _purchaseRequisitionMonitorings)
                            {
                                if (_purchaseRequisitionMonitoring != null)
                                {
                                    _stringPRM.Add(_purchaseRequisitionMonitoring.PurchaseRequisition.Code);
                                }
                            }

                            IEnumerable<string> _stringArrayPRMDistinct = _stringPRM.Distinct();
                            string[] _stringArrayPRMList = _stringArrayPRMDistinct.ToArray();
                            if (_stringArrayPRMList.Length == 1)
                            {
                                for (int i = 0; i < _stringArrayPRMList.Length; i++)
                                {
                                    if (i == 0)
                                    {
                                        _beginString = "[Code]=='" + _stringArrayPRMList[i] + "'";
                                    }
                                }
                            }
                            else if (_stringArrayPRMList.Length > 1)
                            {
                                for (int i = 0; i < _stringArrayPRMList.Length; i++)
                                {
                                    if (i == 0)
                                    {
                                        _beginString = "[Code]=='" + _stringArrayPRMList[i] + "'";
                                    }
                                    else
                                    {
                                        _endString = _endString + " OR [Code]=='" + _stringArrayPRMList[i] + "'";
                                    }
                                }
                            }
                            _fullString = _beginString + _endString;

                            if (_fullString != null)
                            {
                                _availablePurchaseRequisition = new XPCollection<PurchaseRequisition>(Session, CriteriaOperator.Parse(_fullString));
                            }
                        }
                    }
                }
                return _availablePurchaseRequisition;
            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailablePurchaseRequisition", DataSourcePropertyIsNullMode.SelectNothing)]
        public PurchaseRequisition PurchaseRequisition
        {
            get { return _purchaseRequisition; }
            set { SetPropertyValue("PurchaseRequisition", ref _purchaseRequisition, value); }
        }

        [ImmediatePostData()]
        [Appearance("PurchaseRequisitionCollectionPurchaseOrderClose", Enabled = false)]
        [Association("PurchaseOrder-PurchaseRequisitionCollections")]
        public PurchaseOrder PurchaseOrder
        {
            get { return _purchaseOrder; }
            set { SetPropertyValue("PurchaseOrder", ref _purchaseOrder, value); }
        }

        [Appearance("PurchaseRequisitionCollectionStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("PurchaseRequisitionCollectionStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        //private string _PersistentProperty;
        //[XafDisplayName("My display name"), ToolTip("My hint message")]
        //[ModelDefault("EditMask", "(000)-00"), Index(0), VisibleInListView(false)]
        //[Persistent("DatabaseColumnName"), RuleRequiredField(DefaultContexts.Save)]
        //public string PersistentProperty {
        //    get { return _PersistentProperty; }
        //    set { SetPropertyValue("PersistentProperty", ref _PersistentProperty, value); }
        //}

        //[Action(Caption = "My UI Action", ConfirmationMessage = "Are you sure?", ImageName = "Attention", AutoCommit = true)]
        //public void ActionMethod() {
        //    // Trigger a custom business logic for the current record in the UI (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112619.aspx).
        //    this.PersistentProperty = "Paid";
        //}
    }
}