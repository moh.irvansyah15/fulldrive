﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using DevExpress.ExpressApp.Editors;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Shipment")]
    [RuleCombinationOfPropertiesIsUnique("FreightForwardingOriginRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class FreightForwardingOrigin : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).

        private bool _activationPosting;
        private bool _activeApproved1;
        private bool _activeApproved2;
        private bool _activeApproved3;
        private string _code;
        private ShipmentType _shipmentType;
        private TransportType _transportType;
        private Country _countryFrom;
        private City _cityFrom;
        private XPCollection<BusinessPartner> _availableShipper;
        private BusinessPartner _shipper;
        private string _shipperContact;
        private string _shipperAddress;
        private Country _countryTo;
        private City _cityTo;
        private XPCollection<BusinessPartner> _availableConsignee;
        private BusinessPartner _consignee;
        private string _consigneeContact;
        private string _consigneeAddress;
        private Status _status;
        private DateTime _statusDate;
        private string _userAccess;
        private GlobalFunction _globFunc;

        public FreightForwardingOrigin(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            DateTime now = DateTime.Now;
            _globFunc = new GlobalFunction();
            this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.FreightForwardingOrigin);
            this.Status = Status.Open;
            this.StatusDate = now;

            #region UserAccess
            _userAccess = SecuritySystem.CurrentUserName;
            UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
            if (Session.IsNewObject(this))
            {
                if (_locUserAccess != null)
                {
                    if (_locUserAccess.Employee != null)
                    {
                        OrganizationSetupDetail _locOrganizationSetupDetail = Session.FindObject<OrganizationSetupDetail>
                                                                              (new GroupOperator(GroupOperatorType.And,
                                                                               new BinaryOperator("UserAccess", _locUserAccess),
                                                                               new BinaryOperator("ObjectList", CustomProcess.ObjectList.FreightForwardingOrigin),
                                                                               new BinaryOperator("Active", true)));
                        if (_locOrganizationSetupDetail != null)
                        {
                            if (_locOrganizationSetupDetail.ShipmentType == ShipmentType.Export || _locOrganizationSetupDetail.ShipmentType == ShipmentType.ExportAndImport )
                            {
                                this.CountryFrom = _locOrganizationSetupDetail.Country;
                                this.ShipmentType = ShipmentType.Export;
                            }

                            if(_locOrganizationSetupDetail.ShipmentType == ShipmentType.DomesticDelivery)
                            {
                                this.CountryFrom = _locOrganizationSetupDetail.Country;
                                this.ShipmentType = ShipmentType.DomesticDelivery;
                                this.CountryTo = _locOrganizationSetupDetail.Country;
                            }
                        }
                    }
                }
            }
            #endregion UserAccess
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        #region ApprovalColor

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [Browsable(false)]
        public bool ActiveApproved1
        {
            get { return _activeApproved1; }
            set { SetPropertyValue("ActiveApproved1", ref _activeApproved1, value); }
        }

        [Browsable(false)]
        public bool ActiveApproved2
        {
            get { return _activeApproved2; }
            set { SetPropertyValue("ActiveApproved2", ref _activeApproved2, value); }
        }

        [Browsable(false)]
        public bool ActiveApproved3
        {
            get { return _activeApproved3; }
            set { SetPropertyValue("ActiveApproved3", ref _activeApproved3, value); }
        }

        #endregion ApprovalColor

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("FreightForwardingOriginRedColor", Criteria = "ActiveApproved1 = true", BackColor = "#fd79a8")]
        [Appearance("FreightForwardingOriginYellowColor", Criteria = "ActiveApproved2 = true", BackColor = "#ffeaa7")]
        [Appearance("FreightForwardingOriginGreenOColor", Criteria = "ActiveApproved3 = true", BackColor = "#81ecec")]
        [Appearance("FreightForwardingOriginCodeEnabled", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("FreightForwardingOriginShipmentTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public ShipmentType ShipmentType
        {
            get { return _shipmentType; }
            set { SetPropertyValue("ShipmentType", ref _shipmentType, value); }
        }

        [Appearance("FreightForwardingOriginServiceModaClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public TransportType TransportType
        {
            get { return _transportType; }
            set { SetPropertyValue("TransportType", ref _transportType, value); }
        }

        [Appearance("FreightForwardingOriginCountryFromClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Country CountryFrom
        {
            get { return _countryFrom; }
            set { SetPropertyValue("CountryFrom", ref _countryFrom, value); }
        }

        [Appearance("FreightForwardingOriginCityFromClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public City CityFrom
        {
            get { return _cityFrom; }
            set { SetPropertyValue("CityFrom", ref _cityFrom, value); }
        }

        [Browsable(false)]
        public XPCollection<BusinessPartner> AvailableShipper
        {
            get
            {
                if (this.CountryFrom != null)
                {
                    if (this.CityFrom != null)
                    {
                        _availableShipper = new XPCollection<BusinessPartner>
                                            (Session, new GroupOperator(GroupOperatorType.And,
                                            new BinaryOperator("Country", this.CountryFrom),
                                            new BinaryOperator("City", this.CityFrom)));
                    }
                    else
                    {
                        _availableShipper = new XPCollection<BusinessPartner>
                                            (Session, new GroupOperator(GroupOperatorType.And,
                                            new BinaryOperator("Country", this.CountryFrom)));
                    }
                }
                else
                {
                    _availableShipper = new XPCollection<BusinessPartner>(Session);
                }


                return _availableShipper;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableShipper", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("FreightForwardingOriginShipperClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BusinessPartner Shipper
        {
            get { return _shipper; }
            set
            {
                SetPropertyValue("Shipper", ref _shipper, value);
                if (!IsLoading)
                {
                    if (this._shipper != null)
                    {
                        this.ShipperContact = this._shipper.Contact;
                        this.ShipperAddress = this._shipper.Address;
                    }
                }
            }
        }

        [Appearance("FreightForwardingOriginShipperContactClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string ShipperContact
        {
            get { return _shipperContact; }
            set { SetPropertyValue("ShipperContact", ref _shipperContact, value); }
        }

        [Size(512)]
        [Appearance("FreightForwardingOriginShipperAddressClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string ShipperAddress
        {
            get { return _shipperAddress; }
            set { SetPropertyValue("ShipperAddress", ref _shipperAddress, value); }
        }

        [Appearance("FreightForwardingOriginCountryToClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Country CountryTo
        {
            get { return _countryTo; }
            set { SetPropertyValue("CountryTo", ref _countryTo, value); }
        }

        [Appearance("FreightForwardingOriginCityToClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public City CityTo
        {
            get { return _cityTo; }
            set { SetPropertyValue("CityTo", ref _cityTo, value); }
        }

        [Browsable(false)]
        public XPCollection<BusinessPartner> AvailableConsignee
        {
            get
            {
                if (this.CountryFrom != null)
                {
                    if (this.CityFrom != null)
                    {
                        _availableConsignee = new XPCollection<BusinessPartner>
                                            (Session, new GroupOperator(GroupOperatorType.And,
                                            new BinaryOperator("Country", this.CountryTo),
                                            new BinaryOperator("City", this.CityTo)));
                    }
                    else
                    {
                        _availableConsignee = new XPCollection<BusinessPartner>
                                            (Session, new GroupOperator(GroupOperatorType.And,
                                            new BinaryOperator("Country", this.CountryTo)));
                    }
                }
                else
                {
                    _availableConsignee = new XPCollection<BusinessPartner>(Session);
                }


                return _availableConsignee;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableConsignee", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("FreightForwardingOriginConsigneeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BusinessPartner Consignee
        {
            get { return _consignee; }
            set
            {
                SetPropertyValue("Consignee", ref _consignee, value);
                if (!IsLoading)
                {
                    if (this._consignee != null)
                    {
                        this.ConsigneeContact = this._consignee.Contact;
                        this.ConsigneeAddress = this._consignee.Address;
                    }
                }
            }
        }

        [Appearance("FreightForwardingOriginConsigneeContactClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string ConsigneeContact
        {
            get { return _consigneeContact; }
            set { SetPropertyValue("ConsigneeContact", ref _consigneeContact, value); }
        }

        [Size(512)]
        [Appearance("FreightForwardingOriginConsigneeAddressClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string ConsigneeAddress
        {
            get { return _consigneeAddress; }
            set { SetPropertyValue("ConsigneeAddress", ref _consigneeAddress, value); }
        }

        [Appearance("FreightForwardingOriginStatusEnabled", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("FreightForwardingOriginStatusDateEnabled", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Association("FreightForwardingOrigin-FreightForwardingOriginLines")]
        public XPCollection<FreightForwardingOriginLine> FreightForwardingOriginLines
        {
            get { return GetCollection<FreightForwardingOriginLine>("FreightForwardingOriginLines"); }
        }

        [Association("FreightForwardingOrigin-ShipmentApprovalLines")]
        public XPCollection<ShipmentApprovalLine> ShipmentApprovalLines
        {
            get { return GetCollection<ShipmentApprovalLine>("ShipmentApprovalLines"); }
        }

        [Association("FreightForwardingOrigin-ShipmentBookingOriginCollections")]
        public XPCollection<ShipmentBookingOriginCollection> ShipmentBookingOriginCollections
        {
            get { return GetCollection<ShipmentBookingOriginCollection>("ShipmentBookingOriginCollections"); }
        }

        #endregion Field

    }
}